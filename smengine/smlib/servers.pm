# Server functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::servers
# Server handling functions
package smlib::servers;


use strict;
use warnings;


use smlib::constants;
use smlib::config;
use smlib::logging;
use awitpt::db::dblayer;
use smlib::util;

use Data::Dumper;
use SOAP::Lite;
use Storable qw( thaw freeze );
use MIME::Base64 qw( decode_base64 encode_base64 );

# Internal flag to make sure we don't try a remote call
my $_localCallsOnly = 0;

# Internal structure holding the current server group / server attributes
my $_attributes = {};


## @fn RPC_AUTOLOADER($function,@params)
# Redirects calls locally or remotely
#
# @param function Function name to call (or load, whatever)
# @param params Parameters to pass to function
#
# @return Result from function call
sub RPC_AUTOLOADER
{
	my ($function,$serverGroupID,@params) = @_;


	# Rip out the function & module names
	my @components = split(/::/,$function);
	my $origFunction = pop(@components);
	my $module = join('::',@components);
	my $realFunction = join('::',$module,"_$origFunction");

	# Check function exists first
	eval "
		if (!defined(\&$realFunction)) {
			die 'No such function: $realFunction';
		}
	";
	if ($@) {
		smlib::logging::log(LOG_ERR,"Internal API function '$realFunction' does not exist: $@");
		die("Internal API function '$realFunction' does not exist: $@");
	}

	# Grab server group
	my $serverGroup = getServerGroup($serverGroupID);
	if (ref($serverGroup) ne "HASH") {
		smlib::logging::log(LOG_ERR,"Failed to get information on server group ID '$serverGroupID'");
		setError("Failed to locate remote server group");
		Auth::sessionSetData('_smlib_server_group',"ERROR/NO RESULT: Server group ID '$serverGroupID'");
		return ERR_UNKNOWN;
	}

	# Check if we doing a RPC or if its local
	my @res = RPC($serverGroup,$realFunction,@params);
	# We're local...
	if (@res < 1 || !defined($res[0])) {
		eval "
			setAttributes(\$serverGroup->{'Attributes'});
			\@res = $realFunction(\@params);
		";
		if ($@) {
			smlib::logging::log(LOG_ERR,"Error calling function '$realFunction': $@");
			setError("Failed to access internal API");
			Auth::sessionSetData('_smlib_server_group',"ERROR/INTERNAL_API: Server group ID '$serverGroupID'");
			return ERR_UNKNOWN;
		}
	}
	if (@res == 1) {
		return $res[0];
	}

	return @res;
}


## @fn getServerGroups($groupInfo,$search)
# Returns list of server groups
#
# @param groupInfo Server group info hash ref
# @li AgentID - Optional agent ID
# @li UserID - Optional user ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Server group ID
# @li Description - Server group description
sub getServerGroups
{
	my ($serverGroupInfo,$search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Check for agent ID restriction
	my $extraTables = "";
	my $extraSQL = "";
	if (defined($serverGroupInfo)) {
		# Make sure we're a hash
		if (!isHash($serverGroupInfo)) {
			setError("Parameter 'serverGroupInfo' is not a HASH");
			return ERR_PARAM;
		}

		# If we have an agent ID, use it
		if (defined($serverGroupInfo->{'AgentID'})) {
			if (!defined($serverGroupInfo->{'AgentID'} = isNumber($serverGroupInfo->{'AgentID'}))) {
				setError("Parameter 'serverGroupInfo' element 'AgentID'' is invalid");
				return ERR_PARAM;
			}
			$extraTables .= ", soap_server_group_to_agent";
			$extraSQL .= " AND soap_server_group_to_agent.AgentID = ".DBQuote($serverGroupInfo->{'AgentID'});
			$extraSQL .= " AND soap_server_group_to_agent.ServerGroupID = soap_server_groups.ID";
		}

		# If we have an user ID, use it
		if (defined($serverGroupInfo->{'UserID'})) {
			if (!defined($serverGroupInfo->{'UserID'} = isNumber($serverGroupInfo->{'UserID'}))) {
				setError("Parameter 'serverGroupInfo' element 'UserID'' is invalid");
				return ERR_PARAM;
			}
			$extraTables .= ", soap_users, soap_user_to_group, soap_server_group_to_group";
			$extraSQL .= " AND soap_users.ID = ".DBQuote($serverGroupInfo->{'UserID'});
			$extraSQL .= " AND soap_user_to_group.UserID = soap_users.ID";
			$extraSQL .= " AND soap_server_group_to_group.UserGroupID = soap_user_to_group.UserGroupID";
			$extraSQL .= " AND soap_server_group_to_group.ServerGroupID = soap_server_groups.ID";

		}

		if (defined($serverGroupInfo->{'Capabilities'})) {
			if (!isVariable($serverGroupInfo->{"Capabilities"})) {
				setError("Parameter 'serverGroupInfo' element 'Capabilities' is invalid");
				return ERR_PARAM;
			}
			if ($serverGroupInfo->{"Capabilities"} eq "") {
				setError("Parameter 'serverGroupInfo' element 'Capabilities' is blank");
				return ERR_PARAM;
			}
		}
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'soap_server_groups.ID',
		'Name' => 'soap_server_groups.Name',
		'Description' => 'soap_server_groups.Description'
	};

	# Select groups and their attributes
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				soap_server_groups.ID,
				soap_server_groups.Name,
				soap_server_groups.Description,
				soap_server_group_attributes.Attribute
			FROM
				soap_server_groups,
				soap_server_group_attributes
				$extraTables
			WHERE
				soap_server_group_attributes.ServerGroupID = soap_server_groups.ID
				$extraSQL
				",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Pull in groups and attributes
	my %serverGroupHash;
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Name Description Attribute ))) {
		# Setup group
		$serverGroupHash{$row->{'ID'}}->{'Name'} = $row->{'Name'};
		$serverGroupHash{$row->{'ID'}}->{'Description'} = $row->{'Name'};

		# Check if attribute is clean
		if ($row->{'Attribute'} =~ /^([a-z\d]+)=(\S+)$/i) {
			my ($name, $value) = ($1, $2);

			$serverGroupHash{$row->{'ID'}}->{'Attributes'}->{$name} = $value;
		# Else its invalid
		} else {
			smlib::logging::log(LOG_WARN,"Server group '".$row->{'Name'}."' has an invalid attribute '".
					$row->{'Attribute'}."'");
		}
	}
	DBFreeRes($sth);

	# Loop with server groups and sort them out
	my @serverGroups = ();
	foreach my $serverGroupID (keys %serverGroupHash) {

		# Check if we have are missing capabilites
		if (!defined($serverGroupHash{$serverGroupID}->{'Attributes'}->{'Capabilities'})) {
			smlib::logging::log(LOG_WARN,"Server group '".$serverGroupHash{$serverGroupID}->{'Name'}.
					"' has no 'Capabilities' attribute defined, ignoring");
			next;
		}

		# Split off capabilities & loop
		my $match = 0;
		foreach my $capability (split(/,/,$serverGroupHash{$serverGroupID}->{'Attributes'}->{'Capabilities'})) {
			# Check for match
			if ($capability eq $serverGroupInfo->{'Capabilities'}) {
				$match = 1;
			}
		}

		# Skip if no match
		if (!$match) {
			next;
		}

		# Add server to list
		my $serverGroup;
		$serverGroup->{'ID'} = $serverGroupID;
		$serverGroup->{'Name'} = $serverGroupHash{$serverGroupID}->{'Name'};
		$serverGroup->{'Description'} = $serverGroupHash{$serverGroupID}->{'Description'};

		push(@serverGroups,$serverGroup);
	}

	return (\@serverGroups,$numResults);
}



## @fn canAccessServerGroup($agentID,$serverGroupID)
# Check if a agent can access a serverGroup
#
# @param agentID Agent ID Agent ID making the request
# @param serverGroupID ServerGroup ID ServerGroup ID being accessed
#
# @return 1 on success, 0 on failure
sub canAccessServerGroup
{
	my ($agentID,$serverGroupID) = @_;
	my $res = 0;


	# Check params
	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($serverGroupID)) {
		setError("Parameter 'serverGroupID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		setError("Parameter 'serverGroupID' is invalid");
		return ERR_PARAM;
	}

#	# Limit to radius servers
#	my $serverGroupData;
#	$serverGroupData->{'Capabilities'} = 'Radius';
	
	# Query server group
	my $num_results = DBSelectNumResults("
			FROM
				soap_server_group_to_agent	
			WHERE
				soap_server_group_to_agent.ServerGroupID = ".DBQuote($serverGroupID)."
				AND soap_server_group_to_agent.AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}



## @fn isRemote($servers,$calledFunction)
# Check if the server is remote or not
#
# @param servers Server group hash
# @param calledFunction Function parameters
#
# @return 1 if remote, 0 if local
sub isRemote
{
	my ($serverGroup, $calledFunction) = @_;
	# Everything is by default local
	my $res = 0;


	# Check for forced flag
	if ($_localCallsOnly) {
		return $res;
	}

	# Check for RedirectFunction attribute, if we have one, then the list is
	# static. If we don't have one, then we default to just saying everything
	# is remote.
	if (defined($serverGroup->{'Attributes'}->{'RedirectFunction'})) {

		# Loop with attributes
		foreach my $redirectFunction (@{$serverGroup->{'Attributes'}->{'RedirectFunction'}}) {

			# Check for match
			if ($calledFunction eq $redirectFunction || $redirectFunction eq "*") {
				$res = 1;
				last;
			}
		}
	}

	return $res;
}


## @fn RPC($serverGroup,$function,@params)
# Call remote server, wherever it may be 
#
# @param serverGroup Server group hash returned by @getServerGroup
# @param function Function name call
# @param params Array of parameters to pass to function
#
# @return result array, or undef if the function is not remote
sub RPC
{
	my ($serverGroup,$function,@params) = @_;


	# Verify all params is ok
	if (!defined($serverGroup)) {
		setError("Parameter 'serverGroup' is not defined");
		return ERR_PARAM;
	}
	if (!isHash($serverGroup)) {
		setError("Parameter 'serverGroup' is invalid");
		return ERR_PARAM;
	}

	# Check if this is a remote call or not
	if (!isRemote($serverGroup,$function)) {
		return;
	}

	# Set the server group we using
	# Default server name to N/A incase we get err'd
	Auth::sessionSetData('_smlib_server_group',$serverGroup->{'Name'});
	Auth::sessionSetData('_smlib_server_name',"ERROR: NONE AVAILBLE");

	# Freeze params, base64 it into text. We do this here so we only do it once
	my $frozenParams = encode_base64( freeze(\@params) );

	# Loop with and sort priorities
	my ($wasConnected, $rslt, $serverAddress);
PR:	foreach my $priority (reverse sort keys %{$serverGroup->{'ByPriority'}}) {

		# Randomize the array using a perl trick
		my @serverList =  sort { (-1,1)[rand 2] } @{$serverGroup->{'ByPriority'}->{$priority}};
			

		# Loop through servers, try to connect
		foreach my $server (@serverList) {

			# Address to send to
			$serverAddress = $server->{'Attributes'}->{'URI'};

			smlib::logging::log(LOG_NOTICE,"Outbound RPC call to '$serverAddress' for '$function'");

			# FIXME - configurable timeout

			# Freeze server attributes, base64 it into text
			my $attributes = mergeAttributes($serverGroup,$server);
			my $frozenAttributes = encode_base64( freeze( $attributes ) );

			# Make remote call and store result, SOAP is a piece of shit and likes to die()
			my $soap;
			eval {
				# Get SOAP instance
				$soap = SOAP::Lite->uri('BackendRPC')->proxy($serverAddress, 'timeout' => 30);
				# And do call
				$rslt = $soap->call("remoteCall" => ($function,$frozenParams,$frozenAttributes));
			};

			# Check if the eval succeeded
			if ($@) {
				smlib::logging::log(LOG_ERR,"Failed outbound RPC call to '$serverAddress' for '$function' during eval: $@");
				next;
			}

			$wasConnected = 1;

			# Set the server name we used
			Auth::sessionSetData('_smlib_server_name',$server->{'Name'});

			# Check for result
			if (!defined($rslt)) {
				smlib::logging::log(LOG_ERR,
						"Failed outbound RPC call to '$serverAddress' for '$function', no result returned");
				setError("No result returned from remote server");
				Auth::sessionSetData('_smlib_server_name',"ERROR/NO RESULT: ".$server->{'Name'});
				return ERR_UNKNOWN;
			}

			# Check for subsystem fault
			if ($rslt->fault) {
				smlib::logging::log(LOG_ERR,
						"Failed outbound RPC call to '$serverAddress' for '$function', lowlevel SOAP error: \n".
						join(', ',$rslt->faultcode,$rslt->faultstring,$rslt->faultdetail));
				setError("Fault detected while contacting remote server");
				Auth::sessionSetData('_smlib_server_name',"ERROR/FAULT: ".$server->{'Name'});
				return ERR_UNKNOWN;
			}

			# Check for result data
			if (!defined($rslt->result)) {
				smlib::logging::log(LOG_ERR,
						"Failed outbound RPC call to '$serverAddress' for '$function', no result data");
				setError("No result data returned from remote server");
				Auth::sessionSetData('_smlib_server_name',"ERROR/RESULT DATA: ".$server->{'Name'});
				return ERR_UNKNOWN;
			}

			# Check for success attribute
			if (!defined($rslt->result->{'success'})) {
				smlib::logging::log(LOG_ERR,
						"Failed outbound RPC call to '$serverAddress' for '$function', data format problem: \n".
						Dumper($rslt->result));
				setError("No data returned from remote server");
				Auth::sessionSetData('_smlib_server_name',"ERROR/DATA FORMAT: ".$server->{'Name'});
				return ERR_UNKNOWN;
			}

			# If we do its good, check if its an error
			if (!$rslt->result->{'success'}) {
				# Check for undefined 
				if (!defined($rslt->result->{'data'}) || !defined($rslt->result->{'data'}->{'ErrorReason'}) ||
						!defined($rslt->result->{'data'}->{'ErrorReason'})) {
					smlib::logging::log(LOG_ERR,
							"Failed outbound RPC call to '$serverAddress' for '$function', data format problem: \n".
							Dumper($rslt->result));
					setError("Failed communicating with remote server, bad data format");
				Auth::sessionSetData('_smlib_server_name',"ERROR/BAD COMMS: ".$server->{'Name'});
					return ERR_UNKNOWN;
				}

				# If all is ok, just return the error
				setError($rslt->result->{'data'}->{'ErrorReason'});
				return $rslt->result->{'data'}->{'ErrorCode'};
			}

			# This should be the last one we try? we made it this far didn't we?
			last PR;
		} # foreach my $server (@serverList) {
	}

	# Check we were connected and check for result, we will trigger this if we 
	# run out of servers to try or if we couldn't connect to any of them
	if (!$wasConnected || !defined($rslt)) {
		smlib::logging::log(LOG_ERR,"Failed outbound RPC call for '$function', no more servers to try");
		setError("No RPC server(s) available, please try again later");
		return ERR_UNKNOWN;
	}

	# The result should be a base64'd Storable
	if (!defined($rslt->result->{'data'}->{'B64Storable'})) {
		smlib::logging::log(LOG_ERR,"Failed outbound RPC call to '$serverAddress' for '$function', ".
				"B64Storable not found: \n".Dumper($rslt->result));
		setError("Failed communicating with remote server, no data");
		return ERR_UNKNOWN;
	}

	my $result = decode_base64($rslt->result->{'data'}->{'B64Storable'});
	if (!defined($result)) {
		smlib::logging::log(LOG_ERR,"Failed outbound RPC call to '$serverAddress' for '$function', invalid base64 data: \n".
				Dumper($rslt->result));
		setError("Failed communicating with remote server, unable to decode data");
		return ERR_UNKNOWN;
	}

	# Eval thaw so we can catch the die() if it occurs
	eval { $result = thaw($result); };
	if ($@) {
		smlib::logging::log(LOG_ERR,"Failed outbound RPC call to '$serverAddress' for '$function', invalid data: \n".
				Dumper($rslt->result));
		setError("Failed communicating with remote server, invalid data");
		return ERR_UNKNOWN;
	}

	return @{$result};
}


## @internal
# @fn getServerGroup($serverGroupID)
# Return list of servers & priorities for the server group specified
#
# @param serverGroupID Server group ID
#
# @return Array ref of hash refs
# @li Servers - Hash of servers indexed by server name
# @li ByPriorities - Hash of priorities, containing an array ref of servers
sub getServerGroup
{
	my $serverGroupID = shift;
	my $res;


	# Query server group attributes
	my $sth = DBSelect("
			SELECT
				soap_server_groups.Name
			FROM
				soap_server_groups
			WHERE
				soap_server_groups.ID = ".DBQuote($serverGroupID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	# Grab row
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Name ));
	DBFreeRes($sth);
	# Set group name
	$res->{'Name'} = $row->{'Name'};

	# Query server group attributes
	$sth = DBSelect("
			SELECT
				soap_server_group_attributes.Attribute
			FROM
				soap_server_group_attributes	
			WHERE
				soap_server_group_attributes.ServerGroupID = ".DBQuote($serverGroupID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	# Loop at set group attributes
	while ($row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Attribute ))) {
		# Check for undefined attribute
		if (!defined($row->{'Attribute'})) {
			smlib::logging::log(LOG_WARN,"Server group '".$res->{'ServerGroupName'}."' has an undefined attribute");
			next;
		}
		# Check if attribute is clean
		if ($row->{'Attribute'} =~ /^([a-z\d]+)=(\S+)$/i) {
			my ($name, $value) = ($1, $2);

			# If there is a redirect function, push it onto array
			if ($name eq "RedirectFunction") {
				push(@{$res->{'Attributes'}->{$name}}, $value);

			# Else, assign as normal
			} else {
				$res->{'Attributes'}->{$name} = $value;
			}
		# Else its invalid
		} else {
			smlib::logging::log(LOG_WARN,"Server group '".$res->{'ServerGroupName'}."' has an invalid attribute '".$row->{'Attribute'}."'");
		}
	}
	DBFreeRes($sth);

	# Query server attributes
	$sth = DBSelect("
			SELECT
				soap_server_groups.Name AS ServerGroupName,
				soap_servers.Name AS ServerName,
				soap_server_attributes.Attribute
			FROM
				soap_server_groups,
				soap_servers,
				soap_server_to_server_group,
				soap_server_attributes
			WHERE
				soap_server_groups.ID = ".DBQuote($serverGroupID)."
				AND soap_server_to_server_group.ServerGroupID = soap_server_groups.ID
				AND soap_server_to_server_group.ServerID = soap_servers.ID
				AND soap_servers.ID = soap_server_attributes.ServerID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	# Loop at set server attributes
	while ($row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ServerName Attribute ))) {
		# Set server name
		$res->{'Servers'}->{$row->{'ServerName'}}->{'Name'} = $row->{'ServerName'};

		# Check for undefined attribute
		if (!defined($row->{'Attribute'})) {
			smlib::logging::log(LOG_WARN,"Server group '".$row->{'ServerGroupName'}."' has a server '".$row->{'ServerName'}.
					"' with an undefined attribute");
			next;
		}
		# Check if attribute is clean
		if ($row->{'Attribute'} =~ /^([a-z\d]+)=(\S+)$/i) {
			my ($name, $value) = ($1, $2);
			
			# Check for invalid attributes
			if ($name eq "RedirectFunction") {
				smlib::logging::log(LOG_WARN,"Server group '".$row->{'ServerGroupName'}."' has a server '".$row->{'ServerName'}.
						"' with an invalid attribute 'RedirectFunction'");
				next;
			}

			$res->{'Servers'}->{$row->{'ServerName'}}->{'Attributes'}->{$name} = $value;
		# Else its invalid
		} else {
			smlib::logging::log(LOG_WARN,"Server group '".$row->{'ServerGroupName'}."' has a server '".$row->{'ServerName'}.
					"' with an invalid attribute '".$row->{'Attribute'}."'");
		}
	}
	DBFreeRes($sth);

	# Loop with servers, set defaults and build priority hash
	foreach my $server (keys %{$res->{'Servers'}}) {
		# Default priority is 50
		if (!(defined($res->{'Servers'}->{$server}->{'Attributes'}->{'Priority'}))) {
			$res->{'Servers'}->{$server}->{'Attributes'}->{'Priority'} = 50;
		}

		# This priority level
		my $priority = $res->{'Servers'}->{$server}->{'Attributes'}->{'Priority'};
		
		# Push this server onto array
		push(@{$res->{'ByPriority'}->{$priority}}, $res->{'Servers'}->{$server});
	}
	return $res;
}



# @fn getAttribute($name)
# This function returns an attribute
#
# @param name Name of attribute to return
#
sub getAttribute
{
	my $name = shift;

	return $_attributes->{$name};
}


## @internal
# @fn setAttributes($attributes)
# This function sets the $_attributes variable
#
# @param attributes Attributes structure
#
sub setAttributes
{
	my $attributes = shift;

	$_attributes = $attributes;
}


## @internal
# @fn setLocalCallsOnly()
# This function is used internally by BackendRPC to make sure a call never
# goes remote.
#
sub setLocalCallsOnly
{
	$_localCallsOnly = 1;
}


## @internal
# @fn mergeAttributes($serverGroup,$server)
# This function runs through the group attributes, adds them a hashref
# then runs through the server attributes and overwrites and adds
#
# @param serverGroup Server group hash from @getServerGroup
# @param server Server hash from @getServerGroup
#
# @return Hashref of the above combined attributes
sub mergeAttributes
{
	my ($serverGroup,$server) = @_;
	my $res;


	# Bring in group attributes
	foreach my $name (keys %{$serverGroup->{'Attributes'}}) {
		$res->{$name} = $serverGroup->{'Attributes'}->{$name};
	}

	# And bring in server attributes
	foreach my $name (keys %{$server->{'Attributes'}}) {
		$res->{$name} = $server->{'Attributes'}->{$name};
	}

	return $res;
}


## @fn getUserAttributeWithServerGroup($serverGroupID,$attribute)
# Return attribute of currently logged in user
#
# @param serverGroupID Server Group ID
# @param attr Attribute name
#
# @return Attribute or undef if it doesn't exist
sub getUserAttributeWithServerGroup
{
	my ($serverGroupID,$attribute) = @_;


	# internal processing function
	sub matches
	{
		my ($sgid,$attr) = @_;

#	use Data::Dumper; print STDERR "getUserAttributeWithServerGroup: match: ".Dumper($sgid,$attr);
		# If a normal variable
		if (isVariable($attr)) {
			# And if we have a sgid
			if ((my ($tsgid,$value) = ($attr =~ /^([0-9]+):(.*)/))) {
				if ($tsgid == $sgid) {
					return $value;
				}
			}
		}

		return undef;
	}


	my $ret;

	my $uAttribute = Auth::getUserAttribute($attribute);

	# Check if this is an array ref
	if (ref($uAttribute) eq "ARRAY") {
		my @newList;

		# Loop with array
		foreach my $aitem (@{$uAttribute}) {
#	use Data::Dumper; print STDERR "getUserAttributeWithServerGroup: match call: ".Dumper($serverGroupID,$aitem);
			# And add if they match
			if (defined(my $value = matches($serverGroupID,$aitem))) {
				push(@newList,$value);
			}
		}

		$ret = \@newList;
	
	} else {
		# Check if we matched and return value if we did
		if (defined(my $value = matches($serverGroupID,$uAttribute))) {
			$ret = $value;
		}
	}

#	use Data::Dumper; print STDERR "getUserAttributeWithServerGroup: ".Dumper($ret);

	return $ret;
}




1;
# vim: ts=4
