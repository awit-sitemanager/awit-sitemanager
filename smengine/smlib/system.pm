# System functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::system
# System functions
package smlib::system;


use strict;
use warnings;


use smlib::logging;


# Exporter stuff
require Exporter;
our (@ISA,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT_OK = qw(
	runCommand
	reloadHttpd
	hashPassword

	indentExpand
);


use smlib::constants;

use Digest::MD5;


## @fn runCommand($command)
# Run a command and return the output
#
# @param command Command to run in shell
#
# @return Output of command
sub runCommand
{
	my $command = shift;
	my $response = "";
	my $res;

	open(COMMAND,"$command 2>&1 |");
	# Save response
	while (my $line = <COMMAND>) {
		$response .= "$line";
	}
	# Check for exit result
	close(COMMAND);

	if (($res = $? >> 8) != 0) {
		# Throw error
		setError($response);
	}

	return $res;
}



## @fn reloadHttpd
# Reload httpd config
#
# @return 0 on success, < 0 on error
sub reloadHttpd
{
	# FIXME
	#  - Add proper error returns
	#  - Check config first
	system("/sbin/service httpd reload > /dev/null 2>&1");

	return RES_OK;
}



## @fn hashPassword($plain)
# Hash a plaintext password
#
# @param plain Plain text password
#
# @return MD5 encoded password
sub hashPassword
{
	my $plain = shift;


	my $ctx = Digest::MD5->new();

	$ctx->add($plain);

	# Prefix '{MD5}' digest name and suffix with '==' to pad on 4 byte boundary
	return "{MD5}".$ctx->b64digest()."==";
}



1;
# vim: ts=4
