# Agent Functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::Agent
# Backend functions for handling agents
package smlib::Agent;

use strict;
use warnings;

use smlib::config;
use awitpt::db::dblayer;
use smlib::system qw(hashPassword);

use smlib::constants;
use smlib::logging;
use smlib::util;

use File::Path qw(mkpath);

## @method createAgent($agentInfo)
# Create a agent
#
# @param agentInfo Agent info hash ref
# @li Name - Agent Name
# @li ContactPerson - Contact person
# @li ContactTel1 - Contact number 1
# @li ContactTel2 - Optional contact number 2
# @li ContactEmail - Contact email address
# @li SiteQuota - Website allocation quota
# @li MailboxQuota - Mailbox allocation quota
# @li MinSiteSize - Minimum site size
# @li MinMailboxSize - Minimum mailbox size
#
# @return Agent ID
sub createAgent
{
	my $agentInfo = shift;

	# Validatate hash
	if (!defined($agentInfo)) {
		setError("Parameter 'agentInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($agentInfo)) {
		setError("Parameter 'agentInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($agentInfo->{"Name"})) {
		setError("Parameter 'agentInfo' has no element 'Name'");
		return ERR_PARAM;
	}
	if (!isVariable($agentInfo->{"Name"})) {
		setError("Parameter 'agentInfo' element 'Name' is invalid");
		return ERR_PARAM;
	}
	if ($agentInfo->{"Name"} eq "") {
		setError("Parameter 'agentInfo' element 'Name' is blank");
		return ERR_PARAM;
	}

	if (!defined($agentInfo->{"ContactPerson"})) {
		setError("Parameter 'agentInfo' has no element 'ContactPerson'");
		return ERR_PARAM;
	}
	if (!isVariable($agentInfo->{"ContactPerson"})) {
		setError("Parameter 'agentInfo' element 'ContactPerson' is invalid");
		return ERR_PARAM;
	}
	if ($agentInfo->{"ContactPerson"} eq "") {
		setError("Parameter 'agentInfo' element 'ContactPerson' is blank");
		return ERR_PARAM;
	}

	if (!defined($agentInfo->{"ContactTel1"})) {
		setError("Parameter 'agentInfo' has no element 'ContactTel1'");
		return ERR_PARAM;
	}
	if (!isVariable($agentInfo->{"ContactTel1"})) {
		setError("Parameter 'agentInfo' element 'ContactTel1' is invalid");
		return ERR_PARAM;
	}
	if ($agentInfo->{"ContactTel1"} eq "") {
		setError("Parameter 'agentInfo' element 'ContactTel1' is blank");
		return ERR_PARAM;
	}

	my $extraSQL = "";
	my $extraParams = "";
	if (defined($agentInfo->{"ContactTel2"})) {
		if (!isVariable($agentInfo->{"ContactTel2"})) {
			setError("Parameter 'agentInfo' element 'ContactTel2' is invalid");
			return ERR_PARAM;
		}
		$extraSQL = "ContactTel2,";
		$extraParams = DBQuote($agentInfo->{'ContactTel2'}).",";
	}

	if (!defined($agentInfo->{"ContactEmail"})) {
		setError("Parameter 'agentInfo' has no element 'ContactEmail'");
		return ERR_PARAM;
	}
	if (!isVariable($agentInfo->{"ContactEmail"})) {
		setError("Parameter 'agentInfo' element 'ContactEmail' is invalid");
		return ERR_PARAM;
	}
	if ($agentInfo->{"ContactEmail"} eq "") {
		setError("Parameter 'agentInfo' element 'ContactEmail' is blank");
		return ERR_PARAM;
	}

	if (!defined($agentInfo->{"SiteQuota"})) {
		setError("Parameter 'agentInfo' has no element 'SiteQuota'");
		return ERR_PARAM;
	}
	if (!defined($agentInfo->{"SiteQuota"} = isNumber($agentInfo->{"SiteQuota"},ISNUMBER_ALLOW_ZERO))) {
		setError("Parameter 'agentInfo' element 'SiteQuota' must be > 0");
		return ERR_PARAM;
	}
	if (!defined($agentInfo->{"MailboxQuota"})) {
		setError("Parameter 'agentInfo' has no element 'MailboxQuota'");
		return ERR_PARAM;
	}
	if (!defined($agentInfo->{"MailboxQuota"} = isNumber($agentInfo->{"MailboxQuota"},ISNUMBER_ALLOW_ZERO))) {
		setError("Parameter 'agentInfo' element 'MailboxQuota' must be > 0");
		return ERR_PARAM;
	}
	if (!defined($agentInfo->{"MinSiteSize"})) {
		setError("Parameter 'agentInfo' has no element 'MinSiteSize'");
		return ERR_PARAM;
	}
	if (!defined($agentInfo->{"MinSiteSize"} = isNumber($agentInfo->{"MinSiteSize"}))) {
		setError("Parameter 'agentInfo' element 'MinSiteSize' must be > 0");
		return ERR_PARAM;
	}
	if (!defined($agentInfo->{"MinMailboxSize"})) {
		setError("Parameter 'agentInfo' has no element 'MinMailboxSize'");
		return ERR_PARAM;
	}
	if (!defined($agentInfo->{"MinMailboxSize"} = isNumber($agentInfo->{"MinMailboxSize"}))) {
		setError("Parameter 'agentInfo' element 'MinMailboxSize' must be > 0");
		return ERR_PARAM;
	}

	# Check if agent exists
	my $num_results = DBSelectNumResults("FROM agents WHERE Name = ".DBQuote($agentInfo->{'Name'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Agent '".$agentInfo->{'Name'}."' already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert agent
	my $sth = DBDo("
			INSERT INTO agents
				(Name, ContactPerson, ContactTel1,$extraSQL ContactEmail,
					SiteQuota, MailboxQuota, MinSiteSize, MinMailboxSize)
			VALUES (".
					DBQuote($agentInfo->{'Name'}).",".
					DBQuote($agentInfo->{"ContactPerson"}).",".
					DBQuote($agentInfo->{"ContactTel1"}).",".
					$extraParams.
					DBQuote($agentInfo->{"ContactEmail"}).",".
					DBQuote($agentInfo->{"SiteQuota"}).",".
					DBQuote($agentInfo->{"MailboxQuota"}).",".
					DBQuote($agentInfo->{"MinSiteSize"}).",".
					DBQuote($agentInfo->{"MinMailboxSize"}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID =DBLastInsertID('agents','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Set agent web dir
	my $webDir = "/var/www/sites/$ID";
	my $confDir = "/etc/httpd/conf.d/agents/$ID";

	# Update homedir as we only know the paths now
	$sth = DBDo("
			UPDATE agents
			SET
				ConfDirectory = ".DBQuote($confDir).",
				WebDirectory = ".DBQuote($webDir)."
			WHERE
				ID = ".DBQuote($ID)
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

# FIXME - do we need this if we delegate to a webserver??
  	# Create website storage directory
	mkpath($webDir,0,0775);
#	chown(0,$agentGID,$webDir);

  	# Create apache config directory & config file
	mkpath($confDir,0,0755);
	open(FILE,"> /etc/httpd/conf.d/50-agent-$ID.conf");
	print(FILE "include conf.d/agents/$ID/*.conf\n");
	close(FILE);

	DBCommit();

	return $ID;
}



## @method removeAgent($agentID)
# Create a agent
#
# @param agentID Agent ID
#
# @return 0 on success, < 0 on error
sub removeAgent
{
	my $agentID = shift;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	# Check if we got results & finish off
	if ((my $items = isAgentUsed($agentID)) > 0) {
		setError("Agent is still linked to $items item(s)");
		return ERR_HASCHILDREN;
	}

	# Grab agent to see if he exists
	my $agentInfo = getAgent($agentID);
	# We already have error set, so return
	if (!isHash($agentInfo)) {
		return $agentInfo;
	}

	# Grab result
	my $webDirectory = $agentInfo->{'WebDirectory'};
	my $confDirectory = $agentInfo->{'ConfDirectory'};

	DBBegin();

	# Remove agent from database
	my $sth = DBDo("DELETE FROM agents WHERE ID = ".DBQuote($agentID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

  	# Remove configs
#	unlink("/etc/httpd/conf.d/50-agent-$agentID.conf");
#	rmdir($confDirectory);

	# Remove web directory
#	rmdir($webDirectory);

	# Remove mail dir
#	my $res = smlib::mail::removeMailstore($agentID);
#	if ($res != 0) {
#		setError(smlib::mail::Error());
#		DBRollback();
#		return $res;
#	}

	DBCommit();

	return RES_OK;
}



## @method updateAgent($agentInfo)
# Update agent
#
# @param agentInfo Agent info hash ref
# @li Name - Optional name
# @li ContactPerson - Optional contact person
# @li ContactTel1 - Optional contact person telephone 1
# @li ContactTel2 - Optional contact person telephone 2
# @li ContactEmail - Optional contact person email address
# @li SiteQuota - Optional site allocation quota
# @li MailboxQuota - Optional mailbox allocation quota
# @li MinSiteSize - Optional minimum site size
# @li MinMailboxSize - Optional minimum mailbox size
#
# @return 0 on success, < 0 on error
sub updateAgent
{
	my $agentInfo = shift;

	if (!defined($agentInfo)) {
		setError("Parameter 'agentInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($agentInfo)) {
		setError("Parameter 'agentInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($agentInfo->{'ID'})) {
		setError("Parameter 'agentInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($agentInfo->{'ID'} = isNumber($agentInfo->{'ID'}))) {
		setError("Parameter 'agentInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	my @updates = ();

	if (defined($agentInfo->{'Name'})) {
		if (!isVariable($agentInfo->{"Name"})) {
			setError("Parameter 'agentInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
		push(@updates,"Name = ".DBQuote($agentInfo->{'Name'}));
	}

	if (defined($agentInfo->{'ContactPerson'})) {
		if (!isVariable($agentInfo->{"ContactPerson"})) {
			setError("Parameter 'agentInfo' element 'ContactPerson' is invalid");
			return ERR_PARAM;
		}
		if ($agentInfo->{"ContactPerson"} eq "") {
			setError("Parameter 'agentInfo' element 'ContactPerson' is blank");
			return ERR_PARAM;
		}
		push(@updates,"ContactPerson = ".DBQuote($agentInfo->{'ContactPerson'}));
	}

	if (defined($agentInfo->{'ContactTel1'})) {
		if (!isVariable($agentInfo->{"ContactTel1"})) {
			setError("Parameter 'agentInfo' element 'ContactTel1' is invalid");
			return ERR_PARAM;
		}
		if ($agentInfo->{"ContactTel1"} eq "") {
			setError("Parameter 'agentInfo' element 'ContactTel1' is blank");
			return ERR_PARAM;
		}
		push(@updates,"ContactTel1 = ".DBQuote($agentInfo->{'ContactTel1'}));
	}

	if (defined($agentInfo->{'ContactTel2'})) {
		if (!isVariable($agentInfo->{"ContactTel2"})) {
			setError("Parameter 'agentInfo' element 'ContactTel2' is invalid");
			return ERR_PARAM;
		}
		if ($agentInfo->{"ContactTel2"} eq "") {
			setError("Parameter 'agentInfo' element 'ContactTel2' is blank");
			return ERR_PARAM;
		}
		# Treat space " " as setting to null
		if ($agentInfo->{"ContactTel2"} eq " ") {
			push(@updates,"ContactTel2 = NULL");
		} else {
			push(@updates,"ContactTel2 = ".DBQuote($agentInfo->{'ContactTel2'}));
		}
	}

	if (defined($agentInfo->{'ContactEmail'})) {
		if (!isVariable($agentInfo->{"ContactEmail"})) {
			setError("Parameter 'agentInfo' element 'ContactEmail' is invalid");
			return ERR_PARAM;
		}
		if ($agentInfo->{"ContactEmail"} eq "") {
			setError("Parameter 'agentInfo' element 'ContactEmail' is blank");
			return ERR_PARAM;
		}
		push(@updates,"ContactEmail = ".DBQuote($agentInfo->{'ContactEmail'}));
	}

	if (defined($agentInfo->{'SiteQuota'})) {
		if (!defined($agentInfo->{"SiteQuota"} = isNumber($agentInfo->{"SiteQuota"},ISNUMBER_ALLOW_ZERO))) {
			setError("Parameter 'agentInfo' element 'SiteQuota' must be > 0");
			return ERR_PARAM;
		}
		push(@updates,"SiteQuota = ".DBQuote($agentInfo->{'SiteQuota'}));
	}
	if (defined($agentInfo->{'MailboxQuota'})) {
		if (!defined($agentInfo->{"MailboxQuota"} = isNumber($agentInfo->{"MailboxQuota"},ISNUMBER_ALLOW_ZERO))) {
			setError("Parameter 'agentInfo' element 'MailboxQuota' must be > 0");
			return ERR_PARAM;
		}
		push(@updates,"MailboxQuota = ".DBQuote($agentInfo->{'MailboxQuota'}));
	}
	if (defined($agentInfo->{'MinSiteSize'})) {
		if (!defined($agentInfo->{"MinSiteSize"} = isNumber($agentInfo->{"MinSiteSize"}))) {
			setError("Parameter 'agentInfo' element 'MinSiteSize' must be > 0");
			return ERR_PARAM;
		}
		push(@updates,"MinSiteSize = ".DBQuote($agentInfo->{'MinSiteSize'}));
	}
	if (defined($agentInfo->{'MinMailboxSize'})) {
		if (!defined($agentInfo->{"MinMailboxSize"} = isNumber($agentInfo->{"MinMailboxSize"}))) {
			setError("Parameter 'agentInfo' element 'MinMailboxSize' must be > 0");
			return ERR_PARAM;
		}
		push(@updates,"MinMailboxSize = ".DBQuote($agentInfo->{'MinMailboxSize'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for agent");
		return ERR_USAGE;
	}

	# Grab agent to see if he exists
	my $agent = getAgent($agentInfo->{'ID'});
	# We already have error set, so return
	if (!isHash($agent)) {
		return $agent;
	}

	DBBegin();

	# Update quotas
	my $sth = DBDo("
			UPDATE
				agents
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($agentInfo->{'ID'})
	);

	# Check for errors
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method getAgentQuotaUsage($agentID)
# Get agent quota usage
#
# @param agentID
#
# @return hash ref
# @li SiteQuotaAllocated - Amount of site quota allocated
# @li SiteQuotaUsage - Amount of site quota used
# @li MailQuotaAllocated - Amount of mailbox quota allocated
sub getAgentQuotaUsage
{
	my $agentID = shift;
	my $res;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	# Total up quota's allocated and used by sites
	my $sth = DBSelect("
			SELECT
				SUM(DiskQuota) AS SiteQuotaAllocated,
				SUM(DiskUsage) AS SiteQuotaUsage
			FROM
				sites
			WHERE
				AgentID = ".DBQuote($agentID)
	);
	# Check for execution error
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows == 0) {
		setError("No data returned from database for allocated site quota ");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( SiteQuotaAllocated SiteQuotaUsage ));

	if (defined($row->{'SiteQuotaAllocated'})) {
		$res->{'SiteQuotaAllocated'} = $row->{'SiteQuotaAllocated'};
	} else {
		$res->{'SiteQuotaAllocated'} = 0;
	}

	if (defined($row->{'SiteQuotaUsage'})) {
		$res->{'SiteQuotaUsage'} = $row->{'SiteQuotaUsage'};
	} else {
		$res->{'SiteQuotaUsage'} = 0;
	}

	# We done with this result set
	DBFreeRes($sth);


	# Total up quota's allocated and used by mailboxes
	$sth = DBSelect("
			SELECT SUM(mail_mailboxes.Quota) AS MailboxQuotaAllocated
				FROM mail_mailboxes, mail_transport
			WHERE
				mail_transport.AgentID = ".DBQuote($agentID)."
				AND mail_mailboxes.TransportID = mail_transport.ID"
	);
	# Check for execution error
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows == 0) {
		setError("No data returned from database for allocated mailbox quota ");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	$row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( MailboxQuotaAllocated ));

	if (defined($row->{'MailboxQuotaAllocated'})) {
		$res->{'MailboxQuotaAllocated'} = $row->{'MailboxQuotaAllocated'};
	} else {
		$res->{'MailboxQuotaAllocated'} = 0;
	}

	# We done with this result set
	DBFreeRes($sth);


	return $res;
}



## @method getAgents($agentInfo,$search)
# Return list of agents
#
# @param agentInfo Agent info hash ref
# @li AgentID - Optional agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Agent ID
# @li Name - Agent name
sub getAgents
{
	my ($agentInfo,$search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Check for agent ID restriction
	my $extraSQL = "";
	if (defined($agentInfo)) {
		# Make sure we're a hash
		if (!isHash($agentInfo)) {
			setError("Parameter 'agentInfo' is not a HASH");
			return ERR_PARAM;
		}
# FIXME   OR AgentID = $agentInfo->{'AgentID'}   (sub account)
		# If we have an agent ID, use it
		if (defined($agentInfo->{'AgentID'})) {
			if (!defined($agentInfo->{'AgentID'} = isNumber($agentInfo->{'AgentID'}))) {
				setError("Parameter 'agentInfo' element 'AgentID'' is invalid");
				return ERR_PARAM;
			}
			$extraSQL = "WHERE ID = ".DBQuote($agentInfo->{'AgentID'});
		}
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Name' => 'Name',
	};

	# Select all agents
	my ($sth,$numResults) = DBSelectSearch("
			SELECT 
				ID,
				Name
			FROM 
				agents
			$extraSQL
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Loop with results
	my @agents = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Name ))) {
		my $item;
		$item->{'ID'} = $row->{'ID'};
		$item->{'Name'} = $row->{'Name'};
		push(@agents,$item);
	}

	DBFreeRes($sth);

	return (\@agents,$numResults);
}



## @method getAgent($agentID)
# Return agent info hash
#
# @param agentID Agent ID
#
# @return Agent info hash ref
# @li Name - Agent Name
# @li ContactPerson - Contact person
# @li ContactTel1 - Contact number 1
# @li ContactTel2 - Contact number 2
# @li ContactEmail - Contact email address
# @li SiteQuota - Website allocation quota
# @li MailboxQuota - Mailbox allocation quota
# @li MinSiteSize - Minimum site size
# @li MinMailboxSize - Minimum mailbox size
sub getAgent
{
	my $agentID = shift;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	# Query site
	my $sth = DBSelect("
		SELECT
			ID, Name,
			ContactPerson, ContactTel1, ContactTel2, ContactEmail,
			SiteQuota, MailboxQuota,
			MinSiteSize, MinMailboxSize,
			ConfDirectory, WebDirectory
		FROM
			agents
		WHERE
			ID = ".DBQuote($agentID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw(
				ID Name ContactPerson ContactTel1 ContactTel2 ContactEmail
				SiteQuota MailboxQuota MinSiteSize MinMailboxSize
				ConfDirectory WebDirectory
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Name'} = $row->{'Name'};
	$res->{'ContactPerson'} = $row->{'ContactPerson'};
	$res->{'ContactTel1'} = $row->{'ContactTel1'};
	$res->{'ContactTel2'} = $row->{'ContactTel2'};
	$res->{'ContactEmail'} = $row->{'ContactEmail'};
	$res->{'SiteQuota'} = $row->{'SiteQuota'};
	$res->{'MailboxQuota'} = $row->{'MailboxQuota'};
	$res->{'MinSiteSize'} = $row->{'MinSiteSize'};
	$res->{'MinMailboxSize'} = $row->{'MinMailboxSize'};
	# FIXME - do away with this
	$res->{'ConfDirectory'} = $row->{'ConfDirectory'};
	$res->{'WebDirectory'} = $row->{'WebDirectory'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}


## @fn resolveAgentID($data)
# Resolve the AgentID's in an array of hashes to AgentName
#
# @param data Array of hashe refs or hash ref containing AgentID
#
# @return Ref contaiing resolved AgentID's
sub resolveAgentID
{
	my $data = shift;


	# Select all agents
	my $sth = DBSelect("
			SELECT 
				ID,
				Name
			FROM 
				agents
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Loop with results & build hash
	my %agents = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Name ))) {
		$agents{$row->{'ID'}} = $row->{'Name'};
	}

	DBFreeRes($sth);


	# Check if this is a hash or a array
	if (ref($data) eq "ARRAY") {
		# Loop with entries
		foreach my $item (@{$data}) {
			# Check that we do infact have an AgentID in the hash
			if (defined($item->{'AgentID'})) {
				if (defined($agents{$item->{'AgentID'}})) {
					$item->{'AgentName'} = $agents{$item->{'AgentID'}};
				} else {
					$item->{'AgentName'} = "-- UNKNOWN AGENT --";
				}
			}
		}

	# We also take hashes...
	} elsif (ref($data) eq "HASH") {
			# Check that we do infact have an AgentID in the hash
			if (defined($data->{'AgentID'})) {
				if (defined($agents{$data->{'AgentID'}})) {
					$data->{'AgentName'} = $agents{$data->{'AgentID'}};
				} else {
					$data->{'AgentName'} = "-- UNKNOWN AGENT --";
				}
			}
	}

	return $data;
}



## @fn isAgentUsed($agentID)
# Return if an agent is being used by something
#
# @param agentID Agent ID
#
# @return Number of things using this agent, < 0 if error
sub isAgentUsed
{
	my $agentID = shift;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	# Count sites agent is linked to
	my $num_results = DBSelectNumResults("FROM sites WHERE AgentID = ".DBQuote($agentID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results;
}


## @fn isAgentValid($agentID)
# Return if an agent is valid or not
#
# @param agentID Agent ID
#
# @return 1 if agent is valid, 0 if not
sub isAgentValid
{
	my $agentID = shift;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	# If we select the number of results for a agent limiting to ID we should
	# get 1 back, always
	my $num_results = DBSelectNumResults("FROM agents WHERE ID = ".DBQuote($agentID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}


## @fn canAgentAccessAgent($callerAgentID,$victimAgentID)
# Return if an agent is valid or not
#
# @param agentID Agent ID making the request
# @param victimAgentID Agent ID of the victim we want to access
#
# @return 1 if agent is allowed, 0 if not
sub canAgentAccessAgent
{
	my ($callerAgentID,$victimAgentID) = @_;
	my $res = 0;


	if (!defined($callerAgentID)) {
		setError("Parameter 'callerAgentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($callerAgentID = isNumber($callerAgentID))) {
		setError("Parameter 'callerAgentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($victimAgentID)) {
		setError("Parameter 'victimAgentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($victimAgentID = isNumber($victimAgentID))) {
		setError("Parameter 'victimAgentID' is invalid");
		return ERR_PARAM;
	}

	# FIXME - do query to see if we the owner of this agent, or if we are this agent
	# Workaround for now:
	return $callerAgentID == $victimAgentID ? 1 : 0;
}


## @fn Agent_auth($username,$password)
# Returns agent ID of the agent user that authenticated
#
# @param username Username
# @param password Password
#
# @return > 0 with agent ID, or < 0 with error
sub Agent_auth
{
	my ($username,$password) = @_;
	my $res = -1;


	if (!defined($username)) {
		setError("Parameter 'username' not defined");
		return ERR_PARAM;
	}
	if (!isVariable($username)) {
		setError("Parameter 'username' is invalid");
		return ERR_PARAM;
	}
	if ($username eq "") {
		setError("Parameter 'username' is invalid");
		return ERR_PARAM;
	}

	if (!defined($password)) {
		setError("Parameter 'password' not defined");
		return ERR_PARAM;
	}
	if (!isVariable($password)) {
		setError("Parameter 'password' is invalid");
		return ERR_PARAM;
	}
	if ($password eq "") {
		setError("Parameter 'password' is invalid");
		return ERR_PARAM;
	}

	# Select user details from database
	my $sth = DBSelect("
			SELECT
				Password, AgentID
			FROM
				agentUsers
			WHERE
				Username = ".DBQuote($username)."
				AND Disabled = 0
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# If we have an invalid number of rows, return negative response
	if ($sth->rows != 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( Password AgentID ));

	# Lower both
	my $cpass = lc(hashPassword($password));
	my $dpass = lc($row->{'Password'});

	# Compare
	if ($cpass eq $dpass) {
		$res = $row->{'AgentID'};
	} else {
		setError("User data does not match");
	}

	DBFreeRes($sth);

	return $res;
}




1;
# vim: ts=4
