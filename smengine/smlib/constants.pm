# SMEngine Constants
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::constants
# SMEngine constants package
package smlib::constants;

use strict;
use warnings;

# Exporter stuff
require Exporter;
our (@ISA,@EXPORT,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT = qw(
	ERR_UNKNOWN


	ERR_SRVPARAM
	ERR_SRVFILE
	ERR_SRVEXEC
	ERR_SRVCACHE


	ERR_DB


	ERR_PARAM
	ERR_S_PARAM
	ERR_C_PARAM

	ERR_USAGE
	ERR_S_USAGE
	ERR_C_USAGE


	ERR_NOTFOUND
	ERR_CONFLICT
	ERR_EXISTS
	ERR_HASCHILDREN
	ERR_NOACCESS

	ERR_INSUFFICIENTQUOTA



	RES_OK
	RES_ERROR
);
@EXPORT_OK = ();


use constant {
	ERR_UNKNOWN	=>	-1,


	ERR_SRVPARAM	=>	-501, # -8
	ERR_SRVFILE		=>	-511,
	ERR_SRVEXEC		=>	-512,
	ERR_SRVCACHE	=>	-590,


	ERR_DB		=>	-2001, # -2


	ERR_PARAM	=>	-3001, # -3
	ERR_S_PARAM	=>	-103001, # -101

	ERR_USAGE	=>	-3002, # -5
	ERR_S_USAGE	=>	-103002, # -5


	ERR_NOTFOUND	=>	-4001, # -4
	ERR_CONFLICT	=>	-4010, # -10
	ERR_EXISTS	=>	-4020,
	ERR_HASCHILDREN	=>	-4030,
	ERR_NOACCESS	=>	-4101, # -9

	ERR_INSUFFICIENTQUOTA	=>	-4201,


	RES_OK			=> 0,
	RES_ERROR		=> -1,

};



1;
# vim: ts=4
