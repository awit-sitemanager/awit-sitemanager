# Delivery Logs functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::MailHosting::DeliveryLogs
# Delivery logs handling class
package smlib::MailHosting::DeliveryLogs;


use strict;
use warnings;

use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;

use smlib::MailHosting;

# Use the smlib servers autoloader so we can get remote calls working nicely
sub AUTOLOAD { our $AUTOLOAD; return smlib::servers::RPC_AUTOLOADER($AUTOLOAD,@_); }


## @method getMailboxLogs($spec,$search)
# Retrieve mailbox delivery logs
#
# @param spec Mailbox specification
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Delivery log ID
# @li Timestamp - Delivery log timestamp
# @li Cluster - Server cluster used
# @li Server - Server used
# @li SendingServerName - Sending server name
# @li SendingServerIP - Sending server IP
# @li QueueID - Queue ID
# @li MessageID - Message ID
# @li EnvelopeFrom - Envelope from
# @li EnvelopeTo - Envelope to
# @li Size - Size of message
# @li Destination - Destination of message
# @li Status - Status of message
sub _getMailboxDeliveryLogs
{
	my ($spec,$search) = @_;


	# Check params
	if (!defined($spec)) {
		setError("Parameter 'spec' is not defined");
		return ERR_PARAM;
	}
	if (!isVariable($spec)) {
		setError("Parameter 'spec' is invalid");
		return ERR_PARAM;
	}
	if ($spec eq "") {
		setError("Parameter 'spec' is blank");
		return ERR_PARAM;
	}

	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Timestamp' => 'Timestamp',
		'Cluster' => 'Cluster',
		'Server' => 'Server',
		'SendingServerName' => 'SendingServerName',
		'SendingServerIP' => 'SendingServerIP',
		'QueueID' => 'QueueID',
		'MessageID' => 'MessageID',
		'EnvelopeFrom' => 'EnvelopeFrom',
		'EnvelopeTo' => 'EnvelopeTo',
		'Size' => 'Size',
		'Destination' => 'Destination',
		'Status' => 'Status'
	};

	# Query delivery logs
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				Timestamp,
				Cluster,
				Server,
				SendingServerName,
				SendingServerIP,
				QueueID,
				MessageID,
				EnvelopeFrom,
				EnvelopeTo,
				Size,
				Destination,
				Status
			FROM
				mail_logs
			WHERE
				EnvelopeTo = ".DBQuote($spec)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}
	# Fetch rows
	my @logs = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw( ID Timestamp Cluster Server SendingServerName SendingServerIP
						QueueID MessageID EnvelopeFrom EnvelopeTo Size Destination Status )
			)
	) {
		my $entry;

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'Timestamp'} = $row->{'Timestamp'};
		$entry->{'Cluster'} = $row->{'Cluster'};
		$entry->{'Server'} = $row->{'Server'};
		$entry->{'SendingServerName'} = $row->{'SendingServerName'};
		$entry->{'SendingServerIP'} = $row->{'SendingServerIP'};
		$entry->{'QueueID'} = $row->{'QueueID'};
		$entry->{'MessageID'} = $row->{'MessageID'};
		$entry->{'EnvelopeFrom'} = $row->{'EnvelopeFrom'};
		$entry->{'EnvelopeTo'} = $row->{'EnvelopeTo'};
		$entry->{'Size'} = $row->{'Size'};
		$entry->{'Destination'} = $row->{'Destination'};
		$entry->{'Status'} = $row->{'Status'};

		push(@logs,$entry);
	}

	DBFreeRes($sth);

	return (\@logs,$numResults);
}


## @method getTransportDeliveryLogs($spec,$search)
# Retrieve transport delivery logs
#
# @param spec Transport specification
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Delivery log ID
# @li Timestamp - Delivery log timestamp
# @li Cluster - Server cluster used
# @li Server - Server used
# @li SendingServerName - Sending server name
# @li SendingServerIP - Sending server IP
# @li QueueID - Queue ID
# @li MessageID - Message ID
# @li EnvelopeFrom - Envelope from
# @li EnvelopeTo - Envelope to
# @li Size - Size of message
# @li Destination - Destination of message
# @li Status - Status of message
sub _getTransportDeliveryLogs
{
	my ($spec,$search) = @_;


	# Check params
	if (!defined($spec)) {
		setError("Parameter 'spec' is not defined");
		return ERR_PARAM;
	}
	if (!isVariable($spec)) {
		setError("Parameter 'spec' is invalid");
		return ERR_PARAM;
	}
	if ($spec eq "") {
		setError("Parameter 'spec' is blank");
		return ERR_PARAM;
	}

	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Timestamp' => 'Timestamp',
		'Cluster' => 'Cluster',
		'Server' => 'Server',
		'SendingServerName' => 'SendingServerName',
		'SendingServerIP' => 'SendingServerIP',
		'QueueID' => 'QueueID',
		'MessageID' => 'MessageID',
		'EnvelopeFrom' => 'EnvelopeFrom',
		'EnvelopeTo' => 'EnvelopeTo',
		'Size' => 'Size',
		'Destination' => 'Destination',
		'Status' => 'Status'
	};

	# Query delivery logs
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				Timestamp,
				Cluster,
				Server,
				SendingServerName,
				SendingServerIP,
				QueueID,
				MessageID,
				EnvelopeFrom,
				EnvelopeTo,
				Size,
				Destination,
				Status
			FROM
				mail_logs
			WHERE
				EnvelopeTo LIKE ".DBQuote("%".$spec)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}
	# Fetch rows
	my @logs = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw( ID Timestamp Cluster Server SendingServerName SendingServerIP
						QueueID MessageID EnvelopeFrom EnvelopeTo Size Destination Status )
			)
	) {
		my $entry;

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'Timestamp'} = $row->{'Timestamp'};
		$entry->{'Cluster'} = $row->{'Cluster'};
		$entry->{'Server'} = $row->{'Server'};
		$entry->{'SendingServerName'} = $row->{'SendingServerName'};
		$entry->{'SendingServerIP'} = $row->{'SendingServerIP'};
		$entry->{'QueueID'} = $row->{'QueueID'};
		$entry->{'MessageID'} = $row->{'MessageID'};
		$entry->{'EnvelopeFrom'} = $row->{'EnvelopeFrom'};
		$entry->{'EnvelopeTo'} = $row->{'EnvelopeTo'};
		$entry->{'Size'} = $row->{'Size'};
		$entry->{'Destination'} = $row->{'Destination'};
		$entry->{'Status'} = $row->{'Status'};

		push(@logs,$entry);
	}

	DBFreeRes($sth);

	return (\@logs,$numResults);
}



1;
# vim: ts=4
