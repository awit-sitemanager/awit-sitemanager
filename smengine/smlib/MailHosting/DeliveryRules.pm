# Delivery Rules functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::MailHosting::DeliveryRules
# Delivery rules handling class
package smlib::MailHosting::DeliveryRules;


use strict;
use warnings;

use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;

# Use the smlib servers autoloader so we can get remote calls working nicely
sub AUTOLOAD { our $AUTOLOAD; return smlib::servers::RPC_AUTOLOADER($AUTOLOAD,@_); }


#
# Mail Transports
# 

## @method createTransportDeliveryRule($deliveryRuleInfo)
# Add a transport delivery rule
#
# @param deliveryRuleInfo Transport delivery rule info hash ref
# @li TransportID - Transport ID
# @li SenderSpec - Optional sender specification
# @li RecipientSpec - Optional recipient specification
# @li Action - Optional action to perform
# @li Detail - Optional message detail
# @li ValidFrom - Optional valid from date
# @li ValidTo - Optional valid to date
#
# @return transport delivery rule ID
sub _createTransportDeliveryRule
{
	my $deliveryRuleInfo = shift;


	# Check parameters
	if (!defined($deliveryRuleInfo)) {
		setError("Parameter 'deliveryRuleInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($deliveryRuleInfo)) {
		setError("Parameter 'deliveryRuleInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($deliveryRuleInfo->{'TransportID'})) {
		setError("Parameter 'deliveryRuleInfo' element 'TransportID' not defined");
		return ERR_PARAM;
	}
	if (!defined($deliveryRuleInfo->{'TransportID'} = isNumber($deliveryRuleInfo->{'TransportID'}))) {
		setError("Parameter 'deliveryRuleInfo' element 'TransportID' is invalid");
		return ERR_PARAM;
	}

	if (defined($deliveryRuleInfo->{"SenderSpec"})) {
		if (!isVariable($deliveryRuleInfo->{"SenderSpec"})) {
			setError("Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'SenderSpec'} eq "") {
			delete($deliveryRuleInfo->{'SenderSpec'});
		} elsif (!defined($deliveryRuleInfo->{"SenderSpec"} = isEmailSpec($deliveryRuleInfo->{'SenderSpec'}))) {
			setError("Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($deliveryRuleInfo->{"RecipientSpec"})) {
		if (!isVariable($deliveryRuleInfo->{"RecipientSpec"})) {
			setError("Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'RecipientSpec'} eq "") {
			delete($deliveryRuleInfo->{'RecipientSpec'});
		} elsif (!defined($deliveryRuleInfo->{'RecipientSpec'} = isEmailSpec($deliveryRuleInfo->{'RecipientSpec'}))) {
			setError("Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($deliveryRuleInfo->{"Action"})) {
		if (!isVariable($deliveryRuleInfo->{"Action"})) {
			setError("Parameter 'deliveryRuleInfo' element 'Action' is invalid");
			return ERR_PARAM;
		}
		$deliveryRuleInfo->{'Action'} = lc($deliveryRuleInfo->{'Action'});
		if ($deliveryRuleInfo->{'Action'} eq "") {
			delete($deliveryRuleInfo->{'Action'});
		} elsif (!($deliveryRuleInfo->{'Action'} =~ /^(?:autoreply|whitelist|blacklist)$/)) {
			setError("Parameter 'deliveryRuleInfo' element 'Action' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		if (!isVariable($deliveryRuleInfo->{"Detail"})) {
			setError("Parameter 'deliveryRuleInfo' element 'Detail' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'Detail'} eq "") {
			delete($deliveryRuleInfo->{'Detail'});
		}
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		if (!defined($deliveryRuleInfo->{'ValidFrom'} = isDate($deliveryRuleInfo->{'ValidFrom'},ISDATE_UNIX))) {
			setError("Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		if (!defined($deliveryRuleInfo->{'ValidTo'} = isDate($deliveryRuleInfo->{'ValidTo'},ISDATE_UNIX))) {
			setError("Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
			return ERR_PARAM;
		}
	}

	# Check what should be added
	my (@params,@values);
	if (defined($deliveryRuleInfo->{'SenderSpec'})) {
		push(@params,"SenderSpec");
		push(@values,DBQuote($deliveryRuleInfo->{'SenderSpec'}));
	}

	if (defined($deliveryRuleInfo->{'RecipientSpec'})) {
		push(@params,"RecipientSpec");
		push(@values,DBQuote($deliveryRuleInfo->{'RecipientSpec'}));
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		push(@params,"Detail");
		push(@values,DBQuote($deliveryRuleInfo->{'Detail'}));
	}

	if (defined($deliveryRuleInfo->{'Action'})) {
		push(@params,"Action");
		push(@values,DBQuote($deliveryRuleInfo->{'Action'}));
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		push(@params,"ValidFrom");
		push(@values,DBQuote($deliveryRuleInfo->{'ValidFrom'}));
	}

	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		push(@params,"ValidTo");
		push(@values,DBQuote($deliveryRuleInfo->{'ValidTo'}));
	}

	# Create and check params and values strings
	my $paramsStr = join(",",@params);
	my $valuesStr = join(",",@values);
	if ($paramsStr eq "") {
		setError("No parameters specified for transport delivery rule");
		return ERR_USAGE;
	}

	# Begin transaction
	DBBegin();

	# Insert delivery rule
	my $sth = DBDo("
			INSERT INTO mail_TransportDeliveryRules
				( TransportID, $paramsStr )
			VALUES
				( ".DBQuote($deliveryRuleInfo->{'TransportID'}).", $valuesStr )
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID('mail_TransportDeliveryRules','id');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}


## @method updateTransportDeliveryRule($deliveryRuleInfo)
# Update a transport delivery rule
#
# @param deliveryRuleInfo Transport delivery rule info hash ref
# @li ID - Transport delivery rule ID
# @li SenderSpec - Optinal sender spec
# @li RecipientSpec - Optinal recipient spec
# @li Action - Optional action to perform
# @li Detail - Optional message detail
# @li ValidFrom - Optional valid from date
# @li ValidTo - Optional valid to date
#
# @return 0 on success or < 0 on error
sub _updateTransportDeliveryRule
{
	my $deliveryRuleInfo = shift;


	# Check parameters
	if (!defined($deliveryRuleInfo)) {
		setError("Parameter 'deliveryRuleInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($deliveryRuleInfo)) {
		setError("Parameter 'deliveryRuleInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($deliveryRuleInfo->{"ID"})) {
		setError("Parameter 'deliveryRuleInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($deliveryRuleInfo->{'ID'} = isNumber($deliveryRuleInfo->{'ID'}))) {
		setError("Parameter 'deliveryRuleInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	if (defined($deliveryRuleInfo->{'SenderSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"SenderSpec"})) {
			setError("Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
			return ERR_PARAM;
		}
		if (!($deliveryRuleInfo->{'SenderSpec'} eq " ")) {
			if ($deliveryRuleInfo->{'SenderSpec'} eq "") {
				delete($deliveryRuleInfo->{'SenderSpec'});
			} elsif (!defined($deliveryRuleInfo->{'SenderSpec'} = isEmailSpec($deliveryRuleInfo->{'SenderSpec'}))) {
				setError("Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
				return ERR_PARAM;
			}
		}
	}

	if (defined($deliveryRuleInfo->{'RecipientSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"RecipientSpec"})) {
			setError("Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
			return ERR_PARAM;
		}
		if (!($deliveryRuleInfo->{'RecipientSpec'} eq " ")) {
			if ($deliveryRuleInfo->{'RecipientSpec'} eq "") {
				delete($deliveryRuleInfo->{'RecipientSpec'});
			} elsif (!defined($deliveryRuleInfo->{'RecipientSpec'} = isEmailSpec($deliveryRuleInfo->{'RecipientSpec'}))) {
				setError("Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
				return ERR_PARAM;
			}
		}
	}

	if (defined($deliveryRuleInfo->{'Action'})) {
		if (!isVariable($deliveryRuleInfo->{"Action"})) {
			setError("Parameter 'deliveryRuleInfo' element 'Action' is invalid");
			return ERR_PARAM;
		}
		$deliveryRuleInfo->{'Action'} = lc($deliveryRuleInfo->{'Action'});
		if ($deliveryRuleInfo->{'Action'} eq "") {
			delete($deliveryRuleInfo->{'Action'});
		} elsif (!($deliveryRuleInfo->{'Action'} =~ /^(?:autoreply|whitelist|blacklist)$/)) {
			if (!($deliveryRuleInfo->{'Action'} eq " ")) {
				setError("Parameter 'deliveryRuleInfo' element 'Action' is invalid");
				return ERR_PARAM;
			}
		}
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		if (!isVariable($deliveryRuleInfo->{"Detail"})) {
			setError("Parameter 'deliveryRuleInfo' element 'Detail' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'Detail'} eq "") {
			delete($deliveryRuleInfo->{'Detail'});
		}
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		if (!isVariable($deliveryRuleInfo->{"ValidFrom"})) {
			setError("Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'ValidFrom'} eq "") {
			delete($deliveryRuleInfo->{'ValidFrom'});
		}
	}
	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		if (!isVariable($deliveryRuleInfo->{"ValidTo"})) {
			setError("Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'ValidTo'} eq "") {
			delete($deliveryRuleInfo->{'ValidTo'});
		}
	}

	# Updates
	my @updates;

	# Pull in changes
	if (defined($deliveryRuleInfo->{'SenderSpec'})) {
		if ($deliveryRuleInfo->{'SenderSpec'} eq " ") {
			push(@updates,"SenderSpec = NULL");
		} else {
			push(@updates,"SenderSpec = ".DBQuote($deliveryRuleInfo->{'SenderSpec'}));
		}
	}

	if (defined($deliveryRuleInfo->{'RecipientSpec'})) {
		if ($deliveryRuleInfo->{'RecipientSpec'} eq " ") {
			push(@updates,"RecipientSpec = NULL");
		} else {
			push(@updates,"RecipientSpec = ".DBQuote($deliveryRuleInfo->{'RecipientSpec'}));
		}
	}

	if (defined($deliveryRuleInfo->{'Action'})) {
		if ($deliveryRuleInfo->{'Action'} eq " ") {
			push(@updates,"Action = NULL");
		} else {
			push(@updates,"Action = ".DBQuote($deliveryRuleInfo->{'Action'}));
		}
		push(@updates,"Action = ".DBQuote($deliveryRuleInfo->{'Action'}));
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		if ($deliveryRuleInfo->{'Detail'} eq " ") {
			push(@updates,"Detail = NULL");
		} else {
			push(@updates,"Detail = ".DBQuote($deliveryRuleInfo->{'Detail'}));
		}
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		if ($deliveryRuleInfo->{'ValidFrom'} eq " ") {
			push(@updates,"ValidFrom = NULL");
		} elsif (defined($deliveryRuleInfo->{'ValidFrom'} = isDate($deliveryRuleInfo->{'ValidFrom'},ISDATE_UNIX))) {
			push(@updates,"ValidFrom = ".DBQuote($deliveryRuleInfo->{'ValidFrom'}));
		} else {
			setError("Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		if ($deliveryRuleInfo->{'ValidTo'} eq " ") {
			push(@updates,"ValidTo = NULL");
		} elsif (defined($deliveryRuleInfo->{'ValidTo'} = isDate($deliveryRuleInfo->{'ValidTo'},ISDATE_UNIX))) {
			push(@updates,"ValidTo = ".DBQuote($deliveryRuleInfo->{'ValidTo'}));
		} else {
			setError("Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
			return ERR_PARAM;
		}
	}

	# Grab transport delivery rule to see if it exists
	my $transportDeliveryRule = _getTransportDeliveryRule($deliveryRuleInfo->{'ID'});
	if (!isHash($transportDeliveryRule)) {
		return $transportDeliveryRule;
	}

	# Create and check update string
	my $updateStr = join(",",@updates);
	if ($updateStr eq "") {
		setError("No updates specified for transport delivery rule");
		return ERR_USAGE;
	}

	# Begin our update
	DBBegin();

	# Update delivery rule
	my $sth = DBDo("
			UPDATE
				mail_TransportDeliveryRules
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($transportDeliveryRule->{'ID'})
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}


## @method removeTransportDeliveryRule($deliveryRuleID)
# Remove a transport delivery rule
#
# @param deliveryRuleID Transport delivery rule ID
#
# @return 0 on success, < 0 on error
sub _removeTransportDeliveryRule
{
	my $deliveryRuleID = shift;


	if (!defined($deliveryRuleID)) {
		setError("Parameter 'deliveryRuleID' not defined");
		return ERR_PARAM;
	}
	if (!defined($deliveryRuleID = isNumber($deliveryRuleID))) {
		setError("Parameter 'deliveryRuleID' is invalid");
		return ERR_PARAM;
	}

	# Check if delivery rule exists
	my $num_results = DBSelectNumResults("FROM mail_TransportDeliveryRules WHERE ID = ".DBQuote($deliveryRuleID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("Transport delivery rule ID '$deliveryRuleID' does not exist");
		return ERR_EXISTS;
	}

	# Grab transport delivery rule to see if it exists
	my $transportDeliveryRule = _getTransportDeliveryRule($deliveryRuleID);
	if (!isHash($transportDeliveryRule)) {
		return $transportDeliveryRule;
	}

	# Delete transport delivery rule from database
	my $sth = DBDo("DELETE FROM mail_TransportDeliveryRules WHERE ID = ".DBQuote($transportDeliveryRule->{'ID'}));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return RES_OK;
}


## @method getTransportDeliveryRules($transportID,$search)
# Return an array of transport delivery rules
#
# @param transportID Transport ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Transport delivery rule ID
# @li TransportID - Transport ID
# @li SenderSpec - Sender specification
# @li RecipientSpec - Recipient specification
# @li Action - Action to perform
# @li Detail - Message detail
# @li ValidFrom - Valid from date
# @li ValidTo - Valid to date
sub _getTransportDeliveryRules
{
	my ($transportID,$search) = @_;


	# Check params
	if (!defined($transportID)) {
		setError("Parameter 'transportID' not defined");
		return ERR_PARAM;
	}
	if (!defined($transportID = isNumber($transportID))) {
		setError("Parameter 'transportID' is invalid");
		return ERR_PARAM;
	}

	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'TransportID' => 'TransportID',
		'SenderSpec' => 'SenderSpec',
		'RecipientSpec' => 'RecipientSpec',
		'Action' => 'Action',
		'ValidFrom' => 'ValidFrom',
		'ValidTo' => 'ValidTo'
	};

	# Select transport delivery rules
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				TransportID,
				SenderSpec,
				RecipientSpec,
				Action,
				Detail,
				ValidFrom,
				ValidTo
			FROM
				mail_TransportDeliveryRules
			WHERE
				TransportID = ".DBQuote($transportID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}
	# Fetch rows
	my @transportDeliveryRules = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(	ID TransportID SenderSpec RecipientSpec Action Detail ValidFrom ValidTo )
			)
	) {
		my $entry;

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'TransportID'} = $row->{'TransportID'};
		$entry->{'SenderSpec'} = $row->{'SenderSpec'};
		$entry->{'RecipientSpec'} = $row->{'RecipientSpec'};
		$entry->{'Action'} = $row->{'Action'};
		$entry->{'Detail'} = $row->{'Detail'};
		$entry->{'ValidFrom'} = $row->{'ValidFrom'};
		$entry->{'ValidTo'} = $row->{'ValidTo'};

		push(@transportDeliveryRules,$entry);
	}

	DBFreeRes($sth);

	return (\@transportDeliveryRules,$numResults);
}


## @method getTransportDeliveryRule($deliveryRuleID)
# Return an hash for the relevant transport delivery rule
#
# @param deliveryRuleID Transport delivery rule ID
#
# @return Hash ref containing delivery rule info
# @li ID - Delivery rule ID
# @li TransportID - Transport ID
# @li SenderSpec - Sender specification
# @li RecipientSpec - Recipient specification
# @li Action - Action to perform
# @li Detail - Message detail
# @li ValidFrom - Valid from date
# @li ValidTo - Valid to date
sub _getTransportDeliveryRule
{
	my $deliveryRuleID = shift;


	if (!defined($deliveryRuleID)) {
		setError("Parameter 'deliveryRuleID' not defined");
		return ERR_PARAM;
	}
	if (!defined($deliveryRuleID = isNumber($deliveryRuleID))) {
		setError("Parameter 'deliveryRuleID' is invalid");
		return ERR_PARAM;
	}

	# Grab transport delivery rule from database
	my $sth = DBSelect("
		SELECT
			ID,
			TransportID,
			SenderSpec,
			RecipientSpec,
			Action,
			Detail,
			ValidFrom,
			ValidTo
		FROM
			mail_TransportDeliveryRules
		WHERE
			ID = ".DBQuote($deliveryRuleID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results & finish off
	if ($sth->rows < 1) {
		setError("Failed to find transport delivery rule ID '$deliveryRuleID'");
		return ERR_NOTFOUND;
	}

	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw( ID TransportID SenderSpec RecipientSpec Action Detail ValidFrom ValidTo )
	);
	DBFreeRes($sth);

	my %deliveryRuleInfo = ();

	# Pull out info
	$deliveryRuleInfo{'ID'} = $row->{'ID'};
	$deliveryRuleInfo{'TransportID'} = $row->{'TransportID'};
	$deliveryRuleInfo{'SenderSpec'} = $row->{'SenderSpec'};
	$deliveryRuleInfo{'RecipientSpec'} = $row->{'RecipientSpec'};
	$deliveryRuleInfo{'Action'} = $row->{'Action'};
	$deliveryRuleInfo{'Detail'} = $row->{'Detail'};
	$deliveryRuleInfo{'ValidFrom'} = $row->{'ValidFrom'};
	$deliveryRuleInfo{'ValidTo'} = $row->{'ValidTo'};

	return \%deliveryRuleInfo;
}


#
# Mailboxes
#

## @method createMailboxDeliveryRule($deliveryRuleInfo)
# Add a mailbox delivery rule
#
# @param deliveryRuleInfo Mailbox delivery rule info hash ref
# @li MailboxID - Mailbox ID
# @li SenderSpec - Optional sender specification
# @li RecipientSpec - Optional recipient specification
# @li Action - Optional action to perform
# @li Detail - Optional message detail
# @li ValidFrom - Optional valid from date
# @li ValidTo - Optional valid to date
#
# @return mailbox delivery rule ID
sub _createMailboxDeliveryRule
{
	my $deliveryRuleInfo = shift;


	# Check parameters
	if (!defined($deliveryRuleInfo)) {
		setError("Parameter 'deliveryRuleInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($deliveryRuleInfo)) {
		setError("Parameter 'deliveryRuleInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($deliveryRuleInfo->{'MailboxID'})) {
		setError("Parameter 'deliveryRuleInfo' element 'MailboxID' not defined");
		return ERR_PARAM;
	}
	if (!defined($deliveryRuleInfo->{'MailboxID'} = isNumber($deliveryRuleInfo->{'MailboxID'}))) {
		setError("Parameter 'deliveryRuleInfo' element 'MailboxID' is invalid");
		return ERR_PARAM;
	}

	if (defined($deliveryRuleInfo->{"SenderSpec"})) {
		if (!isVariable($deliveryRuleInfo->{"SenderSpec"})) {
			setError("Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'SenderSpec'} eq "") {
			delete($deliveryRuleInfo->{'SenderSpec'});
		} elsif (!defined($deliveryRuleInfo->{'SenderSpec'} = isEmailSpec($deliveryRuleInfo->{'SenderSpec'}))) {
			setError("Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($deliveryRuleInfo->{"RecipientSpec"})) {
		if (!isVariable($deliveryRuleInfo->{"RecipientSpec"})) {
			setError("Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'RecipientSpec'} eq "") {
			delete($deliveryRuleInfo->{'RecipientSpec'});
		} elsif (!defined($deliveryRuleInfo->{'RecipientSpec'} = isEmailSpec($deliveryRuleInfo->{'RecipientSpec'}))) {
			setError("Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($deliveryRuleInfo->{"Action"})) {
		if (!isVariable($deliveryRuleInfo->{"Action"})) {
			setError("Parameter 'deliveryRuleInfo' element 'Action' is invalid");
			return ERR_PARAM;
		}
		$deliveryRuleInfo->{'Action'} = lc($deliveryRuleInfo->{'Action'});
		if ($deliveryRuleInfo->{'Action'} eq "") {
			delete($deliveryRuleInfo->{'Action'});
		} elsif (!($deliveryRuleInfo->{'Action'} =~ /^(?:autoreply|whitelist|blacklist)$/)) {
			setError("Parameter 'deliveryRuleInfo' element 'Action' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		if (!isVariable($deliveryRuleInfo->{"Detail"})) {
			setError("Parameter 'deliveryRuleInfo' element 'Detail' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'Detail'} eq "") {
			delete($deliveryRuleInfo->{'Detail'});
		}
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		if (!defined($deliveryRuleInfo->{'ValidFrom'} = isDate($deliveryRuleInfo->{'ValidFrom'},ISDATE_UNIX))) {
			setError("Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		if (!defined($deliveryRuleInfo->{'ValidTo'} = isDate($deliveryRuleInfo->{'ValidTo'},ISDATE_UNIX))) {
			setError("Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
			return ERR_PARAM;
		}
	}

	# Check what should be added
	my (@params,@values);
	if (defined($deliveryRuleInfo->{'SenderSpec'})) {
		push(@params,"SenderSpec");
		push(@values,DBQuote($deliveryRuleInfo->{'SenderSpec'}));
	}

	if (defined($deliveryRuleInfo->{'RecipientSpec'})) {
		push(@params,"RecipientSpec");
		push(@values,DBQuote($deliveryRuleInfo->{'RecipientSpec'}));
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		push(@params,"Detail");
		push(@values,DBQuote($deliveryRuleInfo->{'Detail'}));
	}

	if (defined($deliveryRuleInfo->{'Action'})) {
		push(@params,"Action");
		push(@values,DBQuote($deliveryRuleInfo->{'Action'}));
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		push(@params,"ValidFrom");
		push(@values,DBQuote($deliveryRuleInfo->{'ValidFrom'}));
	}

	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		push(@params,"ValidTo");
		push(@values,DBQuote($deliveryRuleInfo->{'ValidTo'}));
	}

	# Create and check params and values strings
	my $paramsStr = join(",",@params);
	my $valuesStr = join(",",@values);
	if ($paramsStr eq "") {
		setError("No parameters specified for mailbox delivery rule");
		return ERR_USAGE;
	}

	# Begin transaction
	DBBegin();

	# Insert delivery rule
	my $sth = DBDo("
			INSERT INTO mail_MailboxDeliveryRules
				( MailboxID, $paramsStr )
			VALUES
				( ".DBQuote($deliveryRuleInfo->{'MailboxID'}).", $valuesStr )
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID('mail_MailboxDeliveryRules','id');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}


## @method updateMailboxDeliveryRule($deliveryRuleInfo)
# Update a mailbox delivery rule
#
# @param deliveryRuleInfo Mailbox delivery rule info hash ref
# @li ID - Mailbox delivery rule ID
# @li SenderSpec - Optinal sender spec
# @li RecipientSpec - Optinal sender spec
# @li Action - Optional action to perform
# @li Detail - Optional message detail
# @li ValidFrom - Optional valid from date
# @li ValidTo - Optional valid to date
#
# @return 0 on success or < 0 on error
sub _updateMailboxDeliveryRule
{
	my $deliveryRuleInfo = shift;


	# Check parameters
	if (!defined($deliveryRuleInfo)) {
		setError("Parameter 'deliveryRuleInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($deliveryRuleInfo)) {
		setError("Parameter 'deliveryRuleInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($deliveryRuleInfo->{"ID"})) {
		setError("Parameter 'deliveryRuleInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($deliveryRuleInfo->{'ID'} = isNumber($deliveryRuleInfo->{'ID'}))) {
		setError("Parameter 'deliveryRuleInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	if (defined($deliveryRuleInfo->{'SenderSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"SenderSpec"})) {
			setError("Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
			return ERR_PARAM;
		}
		if (!($deliveryRuleInfo->{'SenderSpec'} eq " ")) {
			if ($deliveryRuleInfo->{'SenderSpec'} eq "") {
				delete($deliveryRuleInfo->{'SenderSpec'});
			} elsif (!defined($deliveryRuleInfo->{'SenderSpec'} = isEmailSpec($deliveryRuleInfo->{'SenderSpec'}))) {
				setError("Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
				return ERR_PARAM;
			}
		}
	}

	if (defined($deliveryRuleInfo->{'RecipientSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"RecipientSpec"})) {
			setError("Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
			return ERR_PARAM;
		}
		if (!($deliveryRuleInfo->{'RecipientSpec'} eq " ")) {
			if ($deliveryRuleInfo->{'RecipientSpec'} eq "") {
				delete($deliveryRuleInfo->{'RecipientSpec'});
			} elsif (!defined($deliveryRuleInfo->{'RecipientSpec'} = isEmailSpec($deliveryRuleInfo->{'RecipientSpec'}))) {
				setError("Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
				return ERR_PARAM;
			}
		}
	}

	if (defined($deliveryRuleInfo->{'Action'})) {
		if (!isVariable($deliveryRuleInfo->{"Action"})) {
			setError("Parameter 'deliveryRuleInfo' element 'Action' is invalid");
			return ERR_PARAM;
		}
		$deliveryRuleInfo->{'Action'} = lc($deliveryRuleInfo->{'Action'});
		if ($deliveryRuleInfo->{'Action'} eq "") {
			delete($deliveryRuleInfo->{'Action'});
		} elsif (!($deliveryRuleInfo->{'Action'} =~ /^(?:autoreply|whitelist|blacklist)$/)) {
			if (!($deliveryRuleInfo->{'Action'} eq " ")) {
				setError("Parameter 'deliveryRuleInfo' element 'Action' is invalid");
				return ERR_PARAM;
			}
		}
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		if (!isVariable($deliveryRuleInfo->{"Detail"})) {
			setError("Parameter 'deliveryRuleInfo' element 'Detail' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'Detail'} eq "") {
			delete($deliveryRuleInfo->{'Detail'});
		}
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		if (!isVariable($deliveryRuleInfo->{"ValidFrom"})) {
			setError("Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'ValidFrom'} eq "") {
			delete($deliveryRuleInfo->{'ValidFrom'});
		}
	}
	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		if (!isVariable($deliveryRuleInfo->{"ValidTo"})) {
			setError("Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
			return ERR_PARAM;
		}
		if ($deliveryRuleInfo->{'ValidTo'} eq "") {
			delete($deliveryRuleInfo->{'ValidTo'});
		}
	}

	# Updates
	my @updates;

	# Pull in changes
	if (defined($deliveryRuleInfo->{'SenderSpec'})) {
		if ($deliveryRuleInfo->{'SenderSpec'} eq " ") {
			push(@updates,"SenderSpec = NULL");
		} else {
			push(@updates,"SenderSpec = ".DBQuote($deliveryRuleInfo->{'SenderSpec'}));
		}
	}

	if (defined($deliveryRuleInfo->{'RecipientSpec'})) {
		if ($deliveryRuleInfo->{'RecipientSpec'} eq " ") {
			push(@updates,"RecipientSpec = NULL");
		} else {
			push(@updates,"RecipientSpec = ".DBQuote($deliveryRuleInfo->{'RecipientSpec'}));
		}
	}

	if (defined($deliveryRuleInfo->{'Action'})) {
		if ($deliveryRuleInfo->{'Action'} eq " ") {
			push(@updates,"Action = NULL");
		} else {
			push(@updates,"Action = ".DBQuote($deliveryRuleInfo->{'Action'}));
		}
		push(@updates,"Action = ".DBQuote($deliveryRuleInfo->{'Action'}));
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		if ($deliveryRuleInfo->{'Detail'} eq " ") {
			push(@updates,"Detail = NULL");
		} else {
			push(@updates,"Detail = ".DBQuote($deliveryRuleInfo->{'Detail'}));
		}
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		if ($deliveryRuleInfo->{'ValidFrom'} eq " ") {
			push(@updates,"ValidFrom = NULL");
		} elsif (defined($deliveryRuleInfo->{'ValidFrom'} = isDate($deliveryRuleInfo->{'ValidFrom'},ISDATE_UNIX))) {
			push(@updates,"ValidFrom = ".DBQuote($deliveryRuleInfo->{'ValidFrom'}));
		} else {
			setError("Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		if ($deliveryRuleInfo->{'ValidTo'} eq " ") {
			push(@updates,"ValidTo = NULL");
		} elsif (defined($deliveryRuleInfo->{'ValidTo'} = isDate($deliveryRuleInfo->{'ValidTo'},ISDATE_UNIX))) {
			push(@updates,"ValidTo = ".DBQuote($deliveryRuleInfo->{'ValidTo'}));
		} else {
			setError("Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
			return ERR_PARAM;
		}
	}

	# Grab mailbox delivery rule to see if it exists
	my $mailboxDeliveryRule = _getMailboxDeliveryRule($deliveryRuleInfo->{'ID'});
	if (!isHash($mailboxDeliveryRule)) {
		return $mailboxDeliveryRule;
	}

	# Create and check update string
	my $updateStr = join(",",@updates);
	if ($updateStr eq "") {
		setError("No updates specified for mailbox delivery rule");
		return ERR_USAGE;
	}

	# Begin our update
	DBBegin();

	# Update delivery rule
	my $sth = DBDo("
			UPDATE
				mail_MailboxDeliveryRules
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($mailboxDeliveryRule->{'ID'})
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}


## @method removeMailboxDeliveryRule($deliveryRuleID)
# Remove a mailbox delivery rule
#
# @param deliveryRuleID Mailbox delivery rule ID
#
# @return 0 on success, < 0 on error
sub _removeMailboxDeliveryRule
{
	my $deliveryRuleID = shift;


	if (!defined($deliveryRuleID)) {
		setError("Parameter 'deliveryRuleID' not defined");
		return ERR_PARAM;
	}
	if (!defined($deliveryRuleID = isNumber($deliveryRuleID))) {
		setError("Parameter 'deliveryRuleID' is invalid");
		return ERR_PARAM;
	}

	# Check if delivery rule exists
	my $num_results = DBSelectNumResults("FROM mail_MailboxDeliveryRules WHERE ID = ".DBQuote($deliveryRuleID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("Mailbox delivery rule ID '$deliveryRuleID' does not exist");
		return ERR_EXISTS;
	}

	# Grab mailbox delivery rule to see if it exists
	my $mailboxDeliveryRule = _getMailboxDeliveryRule($deliveryRuleID);
	if (!isHash($mailboxDeliveryRule)) {
		return $mailboxDeliveryRule;
	}

	# Delete mailbox delivery rule from database
	my $sth = DBDo("DELETE FROM mail_MailboxDeliveryRules WHERE ID = ".DBQuote($mailboxDeliveryRule->{'ID'}));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return RES_OK;
}


## @method getMailboxDeliveryRules($mailboxID,$search)
# Return an array of mailbox delivery rules
#
# @param mailboxID Mailbox ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Mailbox delivery rule ID
# @li MailboxID - Mailbox ID
# @li SenderSpec - Sender specification
# @li RecipientSpec - Recipient specification
# @li Action - Action to perform
# @li Detail - Message detail
# @li ValidFrom - Valid from date
# @li ValidTo - Valid to date
sub _getMailboxDeliveryRules
{
	my ($mailboxID,$search) = @_;


	# Check params
	if (!defined($mailboxID)) {
		setError("Parameter 'mailboxID' not defined");
		return ERR_PARAM;
	}
	if (!defined($mailboxID = isNumber($mailboxID))) {
		setError("Parameter 'mailboxID' is invalid");
		return ERR_PARAM;
	}

	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'MailboxID' => 'MailboxID',
		'SenderSpec' => 'SenderSpec',
		'RecipientSpec' => 'RecipientSpec',
		'Action' => 'Action',
		'ValidFrom' => 'ValidFrom',
		'ValidTo' => 'ValidTo'
	};

	# Select mailbox delivery rules
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				MailboxID,
				SenderSpec,
				RecipientSpec,
				Action,
				Detail,
				ValidFrom,
				ValidTo
			FROM
				mail_MailboxDeliveryRules
			WHERE
				MailboxID = ".DBQuote($mailboxID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}
	# Fetch rows
	my @mailboxDeliveryRules = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(	ID TransportID SenderSpec RecipientSpec Action Detail ValidFrom ValidTo )
			)
	) {
		my $entry;

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'MailboxID'} = $row->{'MailboxID'};
		$entry->{'SenderSpec'} = $row->{'SenderSpec'};
		$entry->{'RecipientSpec'} = $row->{'RecipientSpec'};
		$entry->{'Action'} = $row->{'Action'};
		$entry->{'Detail'} = $row->{'Detail'};
		$entry->{'ValidFrom'} = $row->{'ValidFrom'};
		$entry->{'ValidTo'} = $row->{'ValidTo'};

		push(@mailboxDeliveryRules,$entry);
	}

	DBFreeRes($sth);

	return (\@mailboxDeliveryRules,$numResults);
}


## @method getMailboxDeliveryRule($deliveryRuleID)
# Return an hash for the relevant mailbox delivery rule
#
# @param deliveryRuleID Mailbox delivery rule ID
#
# @return Hash ref containing delivery rule info
# @li ID - Delivery rule ID
# @li MailboxID - Mailbox ID
# @li SenderSpec - Sender specification
# @li RecipientSpec - Recipient specification
# @li Action - Action to perform
# @li Detail - Message detail
# @li ValidFrom - Valid from date
# @li ValidTo - Valid to date
sub _getMailboxDeliveryRule
{
	my $deliveryRuleID = shift;


	if (!defined($deliveryRuleID)) {
		setError("Parameter 'deliveryRuleID' not defined");
		return ERR_PARAM;
	}
	if (!defined($deliveryRuleID = isNumber($deliveryRuleID))) {
		setError("Parameter 'deliveryRuleID' is invalid");
		return ERR_PARAM;
	}

	# Grab mailbox delivery rule from database
	my $sth = DBSelect("
		SELECT
			ID,
			MailboxID,
			SenderSpec,
			RecipientSpec,
			Action,
			Detail,
			ValidFrom,
			ValidTo
		FROM
			mail_MailboxDeliveryRules
		WHERE
			ID = ".DBQuote($deliveryRuleID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results & finish off
	if ($sth->rows < 1) {
		setError("Failed to find mailbox delivery rule ID '$deliveryRuleID'");
		return ERR_NOTFOUND;
	}

	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw( ID TransportID SenderSpec RecipientSpec Action Detail ValidFrom ValidTo )
	);
	DBFreeRes($sth);

	my %deliveryRuleInfo = ();

	# Pull out info
	$deliveryRuleInfo{'ID'} = $row->{'ID'};
	$deliveryRuleInfo{'MailboxID'} = $row->{'MailboxID'};
	$deliveryRuleInfo{'SenderSpec'} = $row->{'SenderSpec'};
	$deliveryRuleInfo{'RecipientSpec'} = $row->{'RecipientSpec'};
	$deliveryRuleInfo{'Action'} = $row->{'Action'};
	$deliveryRuleInfo{'Detail'} = $row->{'Detail'};
	$deliveryRuleInfo{'ValidFrom'} = $row->{'ValidFrom'};
	$deliveryRuleInfo{'ValidTo'} = $row->{'ValidTo'};

	return \%deliveryRuleInfo;
}


## @fn getActionList()
# Returns list of actions to perform
#
# @return Array ref
# @li AutoReply
# @li Whitelist
# @li Blacklist
sub _getActionList
{
	my @res;

	my $item1;
	$item1->{'Action'} = "AutoReply";
	push(@res,$item1);
	my $item2;
	$item2->{'Action'} = "Whitelist";
	push(@res,$item2);
	my $item3;
	$item3->{'Action'} = "Blacklist";
	push(@res,$item3);

	return \@res;
}


1;
# vim: ts=4
