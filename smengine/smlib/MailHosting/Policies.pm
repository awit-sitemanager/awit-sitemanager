# Email policy handing functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::MailHosting::Policies
# Class of mailbox handling functions
package smlib::MailHosting::Policies;


use strict;
use warnings;

use smlib::config;
use smlib::constants;
use smlib::logging;
use awitpt::db::dblayer;
use smlib::system qw(hashPassword);
use smlib::util;



## @method getMailPolicies()
# Get mail policies
#
# @return Array ref of hash refs
# @li ID - Policy ID
# @li PolicyName - Policy name
sub getMailPolicies
{
	my @policies = ();


	# Return list of mail policies
	my $sth = DBSelect("SELECT ID, PolicyName FROM mail_policies WHERE PolicyType = 2");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = $sth->fetchrow_hashref()) {
		my $policy;

		$policy->{'ID'} = $row->{'ID'};
		$policy->{'PolicyName'} = $row->{'PolicyName'};

		push(@policies,$policy);
	}

	DBFreeRes($sth);

	return \@policies;
}



## @fn parseInMailboxPolicy($row)
# Generate policy hash from database row
#
# @param row Raw database row
#
# @return Hash ref
# @li PolicyName - Policy name
# @li Greylisting - Greylisting flag
# @li Blacklisting - Blacklisting flag, 1 = premium, 2 = premium_safebl, 3 = premium_nobl
# @li BypassVirusChecks - Bypass virus check flag
# @li BypassSpamChecks - Bypass spam check flag
# @li BypassBannedChecks - Bypass banned file check
# @li BypassHeaderChecks - Bypass bad header check
# @li SpamModifiesSubject - Modify subject when we detect spam
# @li SpamTagLevel - First tag level
# @li SpamTag2Level - Second tag level
# @li SpamKillLevel - Spam kill level
# @li SpamSubjectTag - Spam subject tag
# @li SpamSubjectTag2 - Spam subject tag2
# @li MessageSizeLimit - Message size limit
sub parseInMailboxPolicy
{
	my $row = shift;


	if (!defined($row)) {
		setError("Parameter 'row' is not defined");
		return ERR_PARAM;
	}

	my $res;

	$res->{'PolicyName'} = $row->{'PolicyName'};

	$res->{'Greylisting'} = $row->{'greylisting'} eq "Y" ? 1 : 0;

	# None
	$res->{'Blacklisting'} = 0;
	# See if we can redefine
	if (defined($row->{'SMTPClass'})) {
		# High
		if ($row->{'SMTPClass'} eq "premium") {
			$res->{'Blacklisting'} = 1;
		# Med
		} elsif ($row->{'SMTPClass'} eq "premium_safebl") {
			$res->{'Blacklisting'} = 2;
		# Low
		} elsif ($row->{'SMTPClass'} eq "premium_nobl") {
			$res->{'Blacklisting'} = 3
		}
	}

	$res->{'BypassVirusChecks'} = $row->{'bypass_virus_checks'} eq "Y" ? 1 : 0;
	$res->{'BypassSpamChecks'} = $row->{'bypass_spam_checks'} eq "Y" ? 1 : 0;
	$res->{'BypassBannedChecks'} = $row->{'bypass_banned_checks'} eq "Y" ? 1 : 0;
	$res->{'BypassHeaderChecks'} = $row->{'bypass_header_checks'} eq "Y" ? 1 : 0;

	$res->{'SpamModifiesSubject'} = $row->{'spam_modifies_subj'} eq "Y" ? 1 : 0;

	$res->{'SpamTagLevel'} = $row->{'spam_tag_level'};
	$res->{'SpamTag2Level'} = $row->{'spam_tag2_level'};
	$res->{'SpamKillLevel'} = $row->{'spam_kill_level'};

	$res->{'SpamSubjectTag'} = $row->{'spam_subject_tag'};
	$res->{'SpamSubjectTag2'} = $row->{'spam_subject_tag2'};

	$res->{'MessageSizeLimit'} = defined($row->{'message_size_limit'})
			&& $row->{'message_size_limit'} > 0 ? $row->{'message_size_limit'} / 1024 : 0;

	return $res;
}


## @fn parseOutMailboxPolicy($policy)
# Fucntion to generate a hash of policy settings and values quoted for DB insert/update
#
# @param policy Policy hash ref
# @li PolicyName - Policy name
# @li Greylisting - Greylisting flag
# @li Blacklisting - Blacklisting flag, 1 = premium, 2 = premium_safebl, 3 = premium_nobl
# @li BypassVirusChecks - Bypass virus check flag
# @li BypassSpamChecks - Bypass spam check flag
# @li BypassBannedChecks - Bypass banned file check
# @li BypassHeaderChecks - Bypass bad header check
# @li SpamModifiesSubject - Modify subject when we detect spam
# @li SpamTagLevel - First tag level
# @li SpamTag2Level - Second tag level
# @li SpamKillLevel - Spam kill level
# @li SpamSubjectTag - Spam subject tag
# @li SpamSubjectTag2 - Spam subject tag2
# @li MessageSizeLimit - Message size limit
#
# @return Database row hash ref
# @li greylisting - Greylisting flag
# @li SMTPClass - SMTP class
# @li bypass_virus_checks - Bypass virus checks
# @li bypass_spam_checks - Bypass spam checks
# @li bypass_banned_checks - Bypass banned file checks
# @li bypass_header_checks - Bypass bad header checks
# @li spam_modifies_subj - Spam modifies subject flag
# @li spam_tag_level - Spam tag level
# @li spam_tag2_level - Spam tag2 level
# @li spam_kill_level - Spam kill level
# @li spam_subject_tag - Spam subject tag
# @li spam_subject_tag2 - Spam subject tag2
# @li message_size_limit - Maximum message size
sub parseOutMailboxPolicy
{
	my $policy = shift;
	my %res;


	if (!defined($policy)) {
		setError("Parameter 'policy' is not defined");
		return ERR_PARAM;
	}

	# Parse values
	if (defined($policy->{'Greylisting'})) {
		$res{'greylisting'} = DBQuote($policy->{'Greylisting'} ? 'Y' : 'N');
	}

	if (defined($policy->{'Blacklisting'})) {
		my $SMTPClass = "";

		if ($policy->{'Blacklisting'} == 1) {
			$SMTPClass = "premium";
		} elsif ($policy->{'Blacklisting'} == 2) {
			$SMTPClass = "premium_safebl";
		} elsif ($policy->{'Blacklisting'} == 3) {
			$SMTPClass = "premium_nobl";
		}
		if ($SMTPClass ne "") {
			$res{'SMTPClass'} = DBQuote($SMTPClass);
		} else {
			$res{'SMTPClass'} = "NULL";
		}
	}

	if (defined($policy->{'BypassVirusChecks'})) {
		$res{'bypass_virus_checks'} = DBQuote($policy->{'BypassVirusChecks'} ? 'Y' : 'N');
	}

	if (defined($policy->{'BypassSpamChecks'})) {
		$res{'bypass_spam_checks'} = DBQuote($policy->{'BypassSpamChecks'} ? 'Y' : 'N');
	}

	if (defined($policy->{'BypassBannedChecks'})) {
		$res{'bypass_banned_checks'} = DBQuote($policy->{'BypassBannedChecks'} ? 'Y' : 'N');
	}

	if (defined($policy->{'BypassHeaderChecks'})) {
		$res{'bypass_header_checks'} = DBQuote($policy->{'BypassHeaderChecks'} ? 'Y' : 'N');
	}

	if (defined($policy->{'SpamModifiesSubject'})) {
		$res{'spam_modifies_subj'} = DBQuote($policy->{'SpamModifiesSubject'} ? 'Y' : 'N');
	}

	if (defined($policy->{'SpamTagLevel'})) {
		$res{'spam_tag_level'} =
				$policy->{'SpamTagLevel'} eq "" ? "NULL" : DBQuote($policy->{'SpamTagLevel'});
	}

	if (defined($policy->{'SpamTag2Level'})) {
		$res{'spam_tag2_level'} =
			 $policy->{'SpamTag2Level'} eq "" ? "NULL" : DBQuote($policy->{'SpamTag2Level'});
	}

	if (defined($policy->{'SpamKillLevel'})) {
		$res{'spam_kill_level'} =
			$policy->{'SpamKillLevel'} eq "" ? "NULL" : DBQuote($policy->{'SpamKillLevel'});
	}

	if (defined($policy->{'SpamSubjectTag'})) {
		$res{'spam_subject_tag'} = DBQuote($policy->{'SpamSubjectTag'});
	}

	if (defined($policy->{'SpamSubjectTag2'})) {
		$res{'spam_subject_tag2'} = DBQuote($policy->{'SpamSubjectTag2'});
	}

	if (defined($policy->{'MessageSizeLimit'})) {
		# If we have a size limit
		if ($policy->{'MessageSizeLimit'} > 0) {
			# Sanatize it
			$res{'message_size_limit'} = DBQuote($policy->{'MessageSizeLimit'} * 1024);
		} else {
			$res{'message_size_limit'} = DBQuote("0");
		}
	}

	return \%res;
}



## @method getMailPolicy($policyID)
# Return hash describing a mailbox policy
#
# @param policyID Policy ID
#
# @return Parsed maibox policy, @see parseInMailboxPolicy
sub getMailPolicy
{
	my $policyID = shift;


	if (!defined($policyID)) {
		setError("Parameter 'policyID' is not defined");
		return ERR_PARAM;
	}
	if ($policyID < 1) {
		setError("Parameter 'policyID' is invalid");
		return ERR_PARAM;
	}

	# Return list of mail policies
	my $sth = DBSelect("SELECT * FROM mail_policies WHERE ID = ".DBQuote($policyID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Return undef on no records
	if ($sth->rows < 1) {
		return ERR_NOTFOUND;
	}

	# Grab result
	my $res = parseInMailboxPolicy($sth->fetchrow_hashref());


	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @fn getEffectiveMailboxPolicy($mailboxID)
# Return hash describing the effective mailbox policy
#
# @param mailboxID Mailbox ID
#
# @return Mailbox policy, @see parseInMailboxPolicy or undef on no policy
sub getEffectiveMailboxPolicy
{
	my $mailboxID = shift;


	if (!defined($mailboxID)) {
		setError("Parameter 'mailboxID' is not defined");
		return ERR_PARAM;
	}
	if ($mailboxID < 1) {
		setError("Parameter 'mailboxID' is invalid");
		return ERR_PARAM;
	}

	# See if we have a custom policy
	my $sth = DBSelect("
			SELECT
				*
			FROM
				mail_policies
			WHERE
				PolicyType = 3
				AND PolicyRefID = ".DBQuote($mailboxID)."

	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	my $policyType = "custom";

	# Try for default mailbox ID
	if ($sth->rows == 0) {
		DBFreeRes($sth);

		# Fallback to default mailbox policy
		$sth = DBSelect("
				SELECT
					mail_policies.*
				FROM
					mail_policies, mail_mailboxes
				WHERE
					mail_mailboxes.ID = ".DBQuote($mailboxID)."
					AND mail_policies.ID = mail_mailboxes.PolicyID
		");
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}

		$policyType = "system";
	}

	# Try for default transport ID
	if ($sth->rows == 0) {
		DBFreeRes($sth);

		# Fallback to default mailbox policy
		$sth = DBSelect("
				SELECT
					mail_policies.*
				FROM
					mail_policies, mail_transport, mail_mailboxes
				WHERE
					mail_mailboxes.ID = ".DBQuote($mailboxID)."
					AND mail_transport.ID = mail_mailboxes.TransportID
					AND mail_policies.ID = mail_transport.PolicyID
		");
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}

		$policyType = "domain";
	}

	# Return undef on no records
	if ($sth->rows != 1) {
		DBFreeRes($sth);
		return undef;
	}

	# Grab result
	my $res = parseInMailboxPolicy($sth->fetchrow_hashref());

	$res->{'PolicyType'} = $policyType;

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @fn setClientMailboxPolicy($mailboxID,$policy)
# Function to set a clients custom policy
#
# @param mailboxID Mailbox ID
# @param policy Policy hash, @see parseOutMailboxPolicy
#
# @return 0 on success, < 0 on error
sub setClientMailboxPolicy {
	my ($mailboxID,$policy) = @_;


	if (!defined($mailboxID)) {
		setError("Parameter 'mailboxID' is not defined");
		return ERR_PARAM;
	}
	if ($mailboxID < 1) {
		setError("Parameter 'mailboxID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($policy)) {
		setError("Parameter 'policy' is not defined");
		return ERR_PARAM;
	}

	my $mailboxInfo = getMailbox($mailboxID);
	if (ref $mailboxInfo ne "HASH") {
		return $mailboxInfo;
	}

	# Can only set policy for paying clients
	if (!$mailboxInfo->{'PremiumPolicy'}) {
		setError("Cannot set a mailbox policy without 'PremiumPolicy' flag being set");
		return ERR_USAGE
	}

	# See if we have a custom policy
	my $sth = DBSelect("
			SELECT
				*
			FROM
				mail_policies
			WHERE
				PolicyType = 3
				AND PolicyRefID = ".DBQuote($mailboxID)."

	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# If we have a record we must update it
	my $policyID = 0;
	if ($sth->rows == 1) {
		my $row = $sth->fetchrow_hashref();

		$policyID = $row->{'ID'};
	}

	DBFreeRes($sth);

	# Check if we must update
	if ($policyID > 0) {
		my @updates;
		my $updateHash = parseOutMailboxPolicy($policy);


		# Generate updates
		foreach my $record (keys %{$updateHash}) {
			push(@updates,"$record = ".$updateHash->{$record});
		}

		# Create and check update string
		my $updateStr = join(",",@updates);

		if ($updateStr eq "") {
			setError("No updates specified for policy");
			return ERR_USAGE;
		}

		DBBegin();

		# Update
		$sth = DBDo("
				UPDATE
					mail_policies
				SET
					$updateStr
				WHERE
					ID = ".DBQuote($policyID)."
		");
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			DBRollback();
			return ERR_DB;
		}

		DBCommit();

	# Insert new record
	} else {
		my @columns;
		my @values;
		my $updateHash = parseOutMailboxPolicy($policy);


		# Generate insert information
		foreach my $record (keys %{$updateHash}) {
			push(@columns,$record);
			push(@values,$updateHash->{$record});
		}

		# Create and check insert strings
		my $insertColumns = join(",",@columns);
		my $insertValues = join(",",@values);

		if ($insertColumns eq "" || $insertValues eq "") {
			setError("No updates specified for policy");
			return ERR_USAGE;
		}

		DBBegin();

		# Insert
		$sth = DBDo("
				INSERT INTO mail_policies
					(PolicyName,PolicyType,PolicyRefID,spam_dsn_cutoff_level,$insertColumns)
				VALUES
					("
						.DBQuote("Client Custom Policy - $mailboxID").","
						."3,"
						.DBQuote($mailboxID).",
						9,
						$insertValues
					)
		");
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			DBRollback();
			return ERR_DB;
		}

		DBCommit();
	}

	return RES_OK;
}



1;
# vim: ts=4
