# Apache config parser, section directive management 
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



# Directive inside of a section
package smlib::Apache::Config::Section::Directive;


use strict;
use warnings;


use smlib::logging;


# Args: [<name> [value]]
sub new {
	my ($class,$name,$value) = @_;

	# Create object
	my $self = {};
	bless ($self,$class);

	# Set name and value of variable
	$self->{name} = $name;
	$self->{value} = $value;
	$self->{omments} = undef;

	return $self;
}

# Return value of directive
# Args: none
sub value {
	my ($self) = @_;

	return $self->{value};
}

# Return name of directive
# Args: none
sub name {
	my ($self) = @_;

	return $self->{name};
}

# Set value of directive
# Args: [value]
sub setValue {
	my ($self,$value) = @_;

	$self->{value} = $value; 
}

# Set name of directive 
# Args: [name]
sub setName {
	my ($self,$name) = @_;

	# Check if name is defined
	if (!defined($name) && $name ne "") {
		$self->{name} = $name; 
	} else {
		print(STDERR "Warning: No name given to setName function in smlib::Apache::Config::Section::Directive\n");
	}
}

# Add comments to this directive
# Args: <array of comments>
sub addComment {
	my $self = shift;

	push(@{$self->{comments}},@_);
}


1;
# vim: ts=4
