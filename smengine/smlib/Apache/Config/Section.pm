# Apache config parser, section directive management
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package smlib::Apache::Config::Section;


use strict;
use warnings;

use smlib::util qw(indentExpand);
use smlib::Apache::Config::Section::Directive;
use smlib::logging;


# Args: <file handle> [<section> [<section_param>]]
sub new {
	my ($class,$section,$section_param) = @_;

	# Create object
	my $self = {};
	bless ($self,$class);

	# Setup our variables
	$self->{section}{name} = $section;
	$self->{section}{param} = $section_param;
	$self->{section}{comments} = undef;
	$self->{directives} = ();
	$self->{children} = undef;

	return $self;
}


# Parse data from a filehandle
# Args: <filehandle>
sub parse {
	my ($self,$fh) = @_;
	my @comments = ();


	# Check if we have a filehandle
	if (!defined($fh)) {
		print(STDERR "Warning: filehandle not defined in parse function of smlib::Apache::Config::Section\n");
		return -1;
	}

	# Parse
	while (my $line = <$fh>) {
		# Look for comments first
		if ($line =~ /\s*(#.*)/) {
			push(@comments,$1);
		# Return if we hit the end of our section
		} elsif ($line =~ /\s*<\/(\S+)>/) {
			return 0 if ($self->{section}{name} ne "root");
		# Look for sections
		} elsif ($line =~ /\s*<(\S+)(?: (\S+))?>/) {
			# Process the tag we just found
			my $section = smlib::Apache::Config::Section->new($1,$2);
			$section->parse($fh);
			$section->addComment(@comments);
			@comments = ();
			push(@{$self->{children}},$section);
		# Look for directives
		} elsif ($line =~ /\s*(\S+)\s*(.*)/) {
			# Save variable
			my $directive = smlib::Apache::Config::Section::Directive->new($1,$2);
			$directive->addComment(@comments);
			@comments = ();
			push(@{$self->{directives}},$directive);
		}
	}
	# Return if we hit the end of the file
	return 0;
}


# Return section name
# Args: none
sub name {
	my ($self) = @_;

	return $self->{section}{name};
};


# Return section value
# Args: none
sub param {
	my ($self) = @_;

	return $self->{section}{param};
};


# Return children of this section
# Args: none
sub children {
	my ($self) = @_;

	# If we have children, return them, else empty list
	if (defined($self->{children})) {
		return @{$self->{children}};
	} else {
		return ();
	}
}


# Return a list of directives matching a certain name
# Args: <directive name>
sub getDirective {
	my ($self,$name) = @_;
	my @results;

	if (!defined($name)) {
		print(STDERR "Warning: No name passed to getDirective in smlib::Apache::Config::Section\n");
		return ();
	}

	# Loop and find directives
	foreach my $var (@{$self->{directives}}) {
		push(@results,$var) if ($var->name eq $name);
	}

	return @results;
}


# Add a directive to this section
# Args: <directive name> <directive value or list of values>
sub addDirective {
	my ($self,$name,@values) = @_;

	foreach my $value (@values) {
		push(@{$self->{directives}},smlib::Apache::Config::Section::Directive->new($name,$value));
	}
}


# Remove a directive from this section
# Args: <directive name> [directive value]
sub removeDirective {
	my ($self,$name,$value) = @_;
	my @newDirectives = ();

	# Loop and find directives
	foreach my $var (@{$self->{directives}}) {
		push(@newDirectives,$var) if (!($var->name eq $name && (!defined($value) || defined($value) && $var->value eq $value)));
	}
	# Replace with new directive list excluding the ones we don't want
	$self->{directives} = \@newDirectives;
}


# Replace a directive in this section
# Args: <name> <optional value or undef> <list of new values>
sub replaceDirective {
	my ($self,$name,$value,@newValues) = @_;

	$self->removeDirective($name,$value);
	$self->addDirective($name,@newValues);
}


# Add and return a section
# Args: <name> [param]
sub addSection {
	my ($self,$name,$param) = @_;

	# Create & add section
	my $newSection = smlib::Apache::Config::Section->new($name,$param);
	push(@{$self->{children}},$newSection);

	return $newSection;
}


# Return a list of sections matching either name or name+param
# Args: [[name] [[param]]]
sub getSection {
	my ($self,$name,$param) = @_;


	# Grab results
	my @results = ();
	foreach my $item ($self->children) {
		# If we get a match, push
		if (!defined($name) || ($item->name eq $name && (!defined($param) || (defined($item->param) && $item->param eq $param)))) {
			push (@results,$item)
		}
	}

	return @results;
}


# Add comments to this directive
# Args: <array of comments>
sub addComment {
	my $self = shift;

	push(@{$self->{comments}},@_);
}


# Dump out configuration
# Args: <filehandle>
sub write {
	my ($self,$dest,$indent) = @_;

	# If no content, don't output
	return if ((!defined($self->{directives}) || @{$self->{directives}} == 0) && (!defined($self->{children}) || @{$self->{children}} == 0));

	print($dest "\n");

	# Loop and dump comments
	foreach my $comment (@{$self->{comments}}) {
		printf($dest "\%s\%s\n", indentExpand("\t", $indent), $comment);
	}

	my $newIndent = $indent;
	# Increase indentation if we not root
	if ($self->{section}{name} ne "root") {
		# Beginning tag of block
		printf($dest "\%s<\%s\%s>\n",
				indentExpand("\t", $indent),
				$self->{section}{name},
				$self->{section}{param} ? " ".$self->{section}{param} : "");
		$newIndent++;
	}

	# Loop and find directives
	foreach my $var (@{$self->{directives}}) {
		printf($dest "\%s\%s \%s\n", indentExpand("\t",$newIndent), $var->name, $var->value);
	}

	# Loop and dump children
	foreach my $child (@{$self->{children}}) {
		$child->write($dest,$newIndent);
	}

	# End tag of block
	printf($dest "\%s</\%s>\n", indentExpand("\t", $indent), $self->{section}{name}) if ($self->{section}{name} ne "root")
}



1;
# vim: ts=4
