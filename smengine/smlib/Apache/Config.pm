# Apache config parser, section directive management
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.





package smlib::Apache::Config;


use strict;
use warnings;

use FileHandle;
use smlib::Apache::Config::Section;
use smlib::logging;



sub new {
	my $class = shift;

	# Create object
	my $self = {};
	bless ($self,$class);

	# Root configuration node
	$self->{root} = undef;

	return $self;
}


# Parse file or handle
# Example:
# 	parse(\*STDIN)  or  parse("filename.conf");
# Usage: <filename or handle>
sub parse {
	my ($self,$source) = @_;
	my $fh;

	# Check if we getting passed a handle or a filename
	if (ref $source eq 'GLOB') {
		$fh = $source;
	} else {
		# Try open file or return an error
		if (!($fh = new FileHandle("< $source"))) {
			setError("Failed to open '$source' for reading: $!");
			return -1;
		}
	}

	# Start with root node
	my $root = $self->{root} = smlib::Apache::Config::Section->new('root');
	$root->parse($fh);

	# Close if we opened above
	close($fh) if (ref $source ne 'GLOB');

	return 0;
}


# Get root configuration token
# Args: none
sub getRoot {
	my ($self) = @_;

	return $self->{root};
}


# Create root configuration token
# Args: none
sub create {
	my ($self) = @_;

	$self->{root} = smlib::Apache::Config::Section->new('root');

	return $self->{root};
}


# Dump out configuration
# Args: <filehandle> | <filename>
sub write {
	my ($self,$dest) = @_;
	my $fh;


	# Check if we getting passed a handle or a filename
	if (ref $dest eq 'GLOB') {
		$fh = $dest;
	} else {
		# Try open file or return an error
		if (!($fh = new FileHandle("> $dest"))) {
			setError("Failed to open '$dest' for writing: $!");
			return -1;
		}
	}

	# Get & check root config item
	if (my $root = $self->{root}) {
		$root->write($fh,0);
	}

	# Close if we opened above
	close($fh) if (ref $dest ne 'GLOB');

	return 0;
}


1;
# vim: ts=4
