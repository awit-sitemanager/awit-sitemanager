# Database service functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::WebHosting::Database
# Database service handling functions
package smlib::WebHosting::Database;


use strict;
use warnings;


use smlib::constants;
use smlib::logging;
use awitpt::db::dblayer;
use smlib::util;


## @method createDatabase($databaseInfo)
# Add a database to website
#
# @param databaseInfo Database info hash with the following elements...
# @li WebsiteID - Website ID
# @li SystemDatabaseID - System database ID
# @li DatabaseName - Database name to create
# @li Username - Username for this database
# @li Password - Password for this database
#
sub createDatabase
{
	my ($databaseInfo) = @_;


	if (!defined($databaseInfo)) {
		setError("Parameter 'databaseInfo' not defined");
		return ERR_PARAM;
	}
	if (!defined($databaseInfo->{"WebsiteID"})) {
		setError("Parameter 'databaseInfo' has no element 'WebsiteID'");
		return ERR_PARAM;
	}
	if ($databaseInfo->{"WebsiteID"} < 1) {
		setError("Parameter 'databaseInfo' element 'WebsiteID' is blank");
		return ERR_PARAM;
	}
	if (!defined($databaseInfo->{"SystemDatabaseID"})) {
		setError("Parameter 'databaseInfo' has no element 'SystemDatabaseID'");
		return ERR_PARAM;
	}
	if ($databaseInfo->{"SystemDatabaseID"} < 1) {
		setError("Parameter 'databaseInfo' element 'SystemDatabaseID' is blank");
		return ERR_PARAM;
	}
	if (!defined($databaseInfo->{"DatabaseName"})) {
		setError("Parameter 'databaseInfo' has no element 'DatabaseName'");
		return ERR_PARAM;
	}
	if ($databaseInfo->{"DatabaseName"} eq "") {
		setError("Parameter 'databaseInfo' element 'DatabaseName' is blank");
		return ERR_PARAM;
	}
	if (!defined($databaseInfo->{"Username"})) {
		setError("Parameter 'databaseInfo' has no element 'Username'");
		return ERR_PARAM;
	}
	if ($databaseInfo->{"Username"} eq "") {
		setError("Parameter 'databaseInfo' element 'Username' is blank");
		return ERR_PARAM;
	}
	if (!defined($databaseInfo->{"Password"})) {
		setError("Parameter 'databaseInfo' has no element 'Password'");
		return ERR_PARAM;
	}
	if ($databaseInfo->{"Password"} eq "") {
		setError("Parameter 'databaseInfo' element 'Password' is blank");
		return ERR_PARAM;
	}

	# Make things a bit easier
	my $systemDatabaseID = $databaseInfo->{'SystemDatabaseID'};
	my $websiteID = $databaseInfo->{'WebsiteID'};
	my $dbName = $databaseInfo->{'DatabaseName'};
	my $username = $databaseInfo->{'Username'};
	my $password = $databaseInfo->{'Password'};


	# Check if database exists
	my $num_results = DBSelectNumResults("FROM siteDatabases WHERE Name = ".DBQuote($dbName));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Database '$dbName' already exists");
		return ERR_EXISTS;
	}

	# Grab database server info
	my $sth = DBSelect("
			SELECT
				Type, DSN, AdminUsername, AdminPassword
			FROM
				systemDatabases WHERE ID = ".DBQuote($systemDatabaseID)
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results & finish off
	if ($sth->rows < 1) {
		setError("System database does not exist");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( Type DSN AdminUsername AdminPassword ));

	my %adminDetails;
	$adminDetails{'Type'} = $row->{'Type'};
	$adminDetails{'DSN'} = $row->{'DSN'};
	$adminDetails{'Username'} = $row->{'AdminUsername'};
	$adminDetails{'Password'} = $row->{'AdminPassword'};

	DBFreeRes($sth);

	# Connect to database
	my $newDBH = DBI->connect($adminDetails{'DSN'}, $adminDetails{'Username'}, $adminDetails{'Password'}, { 'PrintError' => 0 } );
	if (!defined($newDBH)) {
		smlib::logging::log(LOG_ERR,"Error connecting to system database '%s': %s",$adminDetails{'DSN'}, $DBI::errstr);
		setError("System database error");
		return ERR_UNKNOWN;
	}

	my $databasePath;

	# Create database
	if ($adminDetails{'Type'} eq "PostgreSQL") {
		$sth = $newDBH->do("CREATE USER ".$newDBH->quote_identifier($username)." WITH PASSWORD ".$newDBH->quote($password));
		# Check for errors
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,"Error creating database user '%s' for '%s' on '%s': %s",$username, $dbName, $adminDetails{'DSN'},
					$newDBH->errstr);
			$newDBH->disconnect();
			setError("System database error");
			return ERR_UNKNOWN;
		}

		$sth = $newDBH->do("CREATE DATABASE ".$newDBH->quote_identifier($dbName)." WITH OWNER = ".$newDBH->quote_identifier($username));
		# Check for errors
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,"Error creating database '%s' on '%s': %s", $dbName, $adminDetails{'DSN'},
					$newDBH->errstr);
			$newDBH->do("DROP USER ".$newDBH->quote_identifier($username));
			$newDBH->disconnect();
			setError("System database error");
			return ERR_UNKNOWN;
		}

		# Get OID which we need to set perms below
		$sth = $newDBH->prepare("
			SELECT
				oid
			FROM
				pg_database
			WHERE
				datname = ".$newDBH->quote($dbName)
		);
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,"Error preparing oid select for '%s' on '%s': %s", $dbName, $adminDetails{'DSN'},
					$newDBH->errstr);
			$newDBH->do("DROP DATABASE ".$newDBH->quote_identifier($dbName));
			$newDBH->do("DROP USER ".$newDBH->quote_identifier($username));
			$newDBH->disconnect();
			setError("System database error");
			return ERR_UNKNOWN;
		}
		# Check for execution error
		if (!$sth->execute()) {
			smlib::logging::log(LOG_ERR,"Error executing oid select for '%s' on '%s': %s", $dbName, $adminDetails{'DSN'},
					$newDBH->errstr);
			$newDBH->do("DROP DATABASE ".$newDBH->quote_identifier($dbName));
			$newDBH->do("DROP USER ".$newDBH->quote_identifier($username));
			$newDBH->disconnect();
			setError("System database error");
			return ERR_UNKNOWN;
		}

		if ($sth->rows != 1) {
			smlib::logging::log(LOG_ERR,"Error finding oid for '%s' on '%s'",$dbName, $adminDetails{'DSN'});
			$newDBH->do("DROP DATABASE ".$newDBH->quote_identifier($dbName));
			$newDBH->do("DROP USER ".$newDBH->quote_identifier($username));
			$newDBH->disconnect();
			setError("System database error");
			return ERR_UNKNOWN;
		}

		# Fetch row
		my $row = $sth->fetchrow_hashref();
		$databasePath = "/var/lib/pgsql/data/base/".$row->{'oid'};

		# Finish off
		$sth->finish();

	} elsif ($adminDetails{'Type'} eq "MySQL") {
		$sth = $newDBH->do("GRANT USAGE ON *.* TO ".$newDBH->quote($username)."\@".$newDBH->quote("localhost")." IDENTIFIED BY ".$newDBH->quote($password));
		# Check for errors
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,"Error creating database user '%s' for '%s' on '%s': %s",$username, $dbName, $adminDetails{'DSN'},
					$newDBH->errstr);
			$newDBH->disconnect();
			setError("System database error");
			return ERR_UNKNOWN;
		}

		$sth = $newDBH->do("CREATE DATABASE ".$newDBH->quote_identifier($dbName));
		# Check for errors
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,"Error creating database '%s' on '%s': %s", $dbName, $adminDetails{'DSN'},$newDBH->errstr);
			$newDBH->do("DROP USER ".$newDBH->quote_identifier($username));
			$newDBH->disconnect();
			setError("System database error");
			return ERR_UNKNOWN;
		}

		$sth = $newDBH->do("
			GRANT ALL ON ".$newDBH->quote_identifier($dbName).".*
				TO ".$newDBH->quote($username)."\@".$newDBH->quote("localhost")
		);
		# Check for errors
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,"Error granting user '%s' access for '%s' on '%s': %s", $username, $dbName, $adminDetails{'DSN'},
					$newDBH->errstr);
			$newDBH->do("DROP DATABASE ".$newDBH->quote_identifier($dbName));
			$newDBH->do("DROP USER ".$newDBH->quote_identifier($username));
			$newDBH->disconnect();
			setError("System database error");
			return ERR_UNKNOWN;
		}

		$newDBH->do("FLUSH PRIVILEGES");

		$databasePath = "/var/lib/mysql/$dbName";

	} else {
		smlib::logging::log(LOG_ERR,"Database type is unknown '%s', DSN '%s'", $adminDetails{'Type'}, $adminDetails{'DSN'});
		setError("System database error");
		return ERR_UNKNOWN;
	}

	DBBegin();

	# Insert zone data into database
	$sth = DBDo("
			INSERT INTO siteDatabases
				(SiteID,SystemDBID,Name,Path)
			VALUES (".
					DBQuote($websiteID).", ".
					DBQuote($systemDatabaseID).", ".
					DBQuote($dbName).", ".
					DBQuote($databasePath).
				")"
	);
	if (!defined($sth)) {
		# Destroy database and user
		$newDBH->do("DROP DATABASE ".$newDBH->quote_identifier($dbName));
		$newDBH->do("DROP USER ".$newDBH->quote_identifier($username));
		$newDBH->disconnect();

		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	my $databaseID = DBLastInsertID("databases","ID");
	if (!defined($databaseID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Insert database username into database users table
	$sth = DBDo("
			INSERT INTO siteDatabaseUsers
				(DatabaseID,Username)
			VALUES (".
					DBQuote($databaseID).", ".
					DBQuote($username).
				")"
	);
	if (!defined($sth)) {
		# Destroy database and user
		$newDBH->do("DROP DATABASE ".$newDBH->quote_identifier($dbName));
		$newDBH->do("DROP USER ".$newDBH->quote_identifier($username));
		$newDBH->disconnect();

		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	$newDBH->disconnect();

	DBCommit();

	return $databaseID;
}



## @method updateDatabase(databaseInfo)
# Update a database
#
# @param databaseInfo Detail hash
# @li ID - Database user ID
# @li AgentDisabled - Optional, flag to indicate agent has disabled this database (NOT IMPLEMENTED
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
#
# @return 0 on success, < 0 on error
sub updateDatabase
{
	my $databaseInfo = shift;


	if (!defined($databaseInfo)) {
		setError("Parameter 'databaseInfo' not defined");
		return ERR_PARAM;
	}
	if (!defined($databaseInfo->{"ID"})) {
		setError("Parameter 'databaseInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($databaseInfo->{'ID'} = isNumber($databaseInfo->{'ID'}))) {
		setError("Parameter 'databaseInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	my $database = getDatabase($databaseInfo->{'ID'});
	if (!isHash($database)) {
		return $database;
	}

	my @updates = ();

	# FIXME - add things to update

	# Create and check update string
	my $updateStr = join(",",@updates);
	if ($updateStr eq "") {
		setError("No updates specified for database");
		return ERR_USAGE;
	}

	DBBegin();

	# Update
	my $sth = DBDo("
			UPDATE
				siteDatabases
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($databaseInfo->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}


## @method removeDatabase($databaseID)
# Remove a database
#
# @param databaseID Database ID
#
# @return 0 on success, < 0 on error
sub removeDatabase
{
	my $databaseID = shift;



	if (!defined($databaseID)) {
		setError("Parameter 'databaseID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($databaseID = isNumber($databaseID))) {
		setError("Parameter 'databaseID' is invalid");
		return ERR_PARAM;
	}

	# Query to see if database actually exists
	my $sth = DBSelect("SELECT SystemDBID, Name FROM siteDatabases WHERE ID = ".DBQuote($databaseID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results & finish off
	if ($sth->rows < 1) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database not found");
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( SystemDBID Name ));
	my $serverID = $row->{'SystemDBID'};
	my $dbName = $row->{'Name'};

	DBFreeRes($sth);


	# Grab database server info
	$sth = DBSelect("
			SELECT
				Type, DSN, AdminUsername, AdminPassword
			FROM
				systemDatabases
			WHERE
				ID = ".DBQuote($serverID)
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results & finish off
	if ($sth->rows < 1) {
		setError("System database does not exist");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	$row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( Type DSN Username Password ));

	my %adminDetails;
	$adminDetails{'Type'} = $row->{'Type'};
	$adminDetails{'DSN'} = $row->{'DSN'};
	$adminDetails{'Username'} = $row->{'AdminUsername'};
	$adminDetails{'Password'} = $row->{'AdminPassword'};

	DBFreeRes($sth);


	# Grab database users
	$sth = DBSelect("SELECT Username FROM siteDatabaseUsers WHERE DatabaseID = ".DBQuote($databaseID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Save database users
	my @databaseUsers;
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Username ))) {
		push(@databaseUsers,$row->{'Username'});
	}

	DBFreeRes($sth);


	# Connect to database
	my $newDBH = DBI->connect($adminDetails{'DSN'}, $adminDetails{'Username'}, $adminDetails{'Password'}, { 'PrintError' => 0 } );
	if (!defined($newDBH)) {
		smlib::logging::log(LOG_ERR,"Error connecting to system database '%s': %s",$adminDetails{'DSN'}, $DBI::errstr);
		setError("System database error");
		return ERR_UNKNOWN;
	}


	# Remove database
	if ($adminDetails{'Type'} eq "PostgreSQL") {
		$sth = $newDBH->do("DROP DATABASE ".$newDBH->quote_identifier($dbName));
		# Check for errors
		if (!defined($sth)) {
			$newDBH->disconnect();
			setError("System database error");
			return ERR_UNKNOWN;
		}

		# Loop with database users
		foreach my $username (@databaseUsers) {
			$sth = $newDBH->do("DROP USER ".$newDBH->quote_identifier($username));
			# Check for errors
			if (!defined($sth)) {
				smlib::logging::log(LOG_ERR,"Error removing database user '%s' on '%s': %s", $username, $adminDetails{'DSN'},
						$newDBH->errstr);
				$newDBH->do("DROP USER ".$newDBH->quote_identifier($username));
				$newDBH->disconnect();
				setError("System database error");
				return ERR_UNKNOWN;
			}
		}

		$newDBH->disconnect();

	} elsif ($adminDetails{'Type'} eq "MySQL") {
		$sth = $newDBH->do("DROP DATABASE ".$newDBH->quote_identifier($dbName));
		# Check for errors
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,"Error removing database '%s' from '%s': %s", $dbName, $adminDetails{'DSN'},
					$newDBH->errstr);
			$newDBH->disconnect();
			setError("System database error");
			return ERR_UNKNOWN;
		}

		# Loop with database users
		foreach my $username (@databaseUsers) {
			$sth = $newDBH->do("DELETE FROM mysql.user WHERE Host = 'localhost' AND User = ".$newDBH->quote($username));
			# Check for errors
			if (!defined($sth)) {
				smlib::logging::log(LOG_ERR,"Error removing database user '%s' on '%s': %s", $username, $adminDetails{'DSN'},
						$newDBH->errstr);
				$newDBH->disconnect();
				setError("System database error");
				return ERR_UNKNOWN;
			}
		}

		$newDBH->do("FLUSH PRIVILEGES");

		$newDBH->disconnect();

	} else {
		smlib::logging::log(LOG_ERR,"Database type is unknown '%s', DSN '%s'", $adminDetails{'Type'}, $adminDetails{'DSN'});
		setError("System database error");
		return ERR_UNKNOWN;
	}

	DBBegin();

	# Remove database users from database
	$sth = DBDo("DELETE FROM siteDatabaseUsers WHERE DatabaseID = ".DBQuote($databaseID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove database from database
	$sth = DBDo("DELETE FROM siteDatabases WHERE ID = ".DBQuote($databaseID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method getDatabases($websiteID,$search)
# Return a hash of databases which belong to a site
#
# @param databaseInfo Database info
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Database ID
# @li SystemDatabaseID - System databae ID
# @li SystemDatabaseName - System databae name
# @li Type - Database type
# @li DatabaseName - Database name
# @li AgentDisabled - Optional, flag to indicate agent has disabled this database (NOT IMPLEMENTED
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
sub getDatabases
{
	my ($websiteID,$search) = @_;


	if (!defined($websiteID)) {
		setError("Parameter 'siteID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		setError("Parameter 'siteID' is invalid");
		return ERR_PARAM;
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'siteDatabases.ID',
		'DatabaseName' => 'siteDatabases.DatabaseName',
	};

	# Select mailboxes
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				siteDatabases.ID,  siteDatabases.Name,
				systemDatabases.ID AS SystemDatabaseID, systemDatabases.Type, systemDatabases.Server
			FROM
				siteDatabases, systemDatabases, siteDatabaseUsers
			WHERE
				siteDatabases.SiteID = ".DBQuote($websiteID)."
				AND systemDatabases.ID = siteDatabases.SystemDBID
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @databases = ();
	while (my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw(
				ID Name Type Server SystemDatabaseID
			)
		)
	) {
		my $item;
		$item->{'ID'} = $row->{'ID'};
		$item->{'SystemDatabaseID'} = $row->{'SystemDatabaseID'};
		$item->{'SystemDatabaseName'} = $row->{'Server'} . " (" . $row->{'Type'} . ")";
		$item->{'Type'} = $row->{'Type'};
		$item->{'DatabaseName'} = $row->{'Name'};
		push(@databases,$item);
	}

	DBFreeRes($sth);

	return (\@databases,$numResults);
}



## @method getDatabase($databaseID)
# Return database record hash
#
# @param databaseID %Database ID
#
# @return Hash ref
# @li ID - Database ID
# @li SystemDatabaseID - System databae ID
# @li SystemDatabaseName - System databae name
# @li Type - Database type
# @li DatabaseName - Database name
# @li AgentDisabled - Optional, flag to indicate agent has disabled this database (NOT IMPLEMENTED
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
sub getDatabase
{
	my $databaseID = shift;


	if (!defined($databaseID)) {
		setError("Parameter 'databaseID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($databaseID = isNumber($databaseID))) {
		setError("Parameter 'databaseID' is invalid");
		return ERR_PARAM;
	}

	# Query database
	my $sth = DBSelect("
			SELECT
				siteDatabases.ID, siteDatabases.Name,
				systemDatabases.ID AS SystemDatabaseID, systemDatabases.Type, systemDatabases.Server
			FROM
				siteDatabases, systemDatabases
			WHERE
				siteDatabases.ID = ".DBQuote($databaseID)."
				AND systemDatabases.ID = siteDatabases.SystemDBID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		$error = "No database data for database ID $databaseID";
		DBFreeRes($sth);
		return undef;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID Name SystemDatabaseID Type Server ));
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'SystemDatabaseID'} = $row->{'SystemDatabaseID'};
	$res->{'SystemDatabaseName'} = $row->{'Server'} . " (" . $row->{'Type'} . ")";
	$res->{'Type'} = $row->{'Type'};
	$res->{'DatabaseName'} = $row->{'Name'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}





## @method getDatabaseUsers($databaseID)
# Return a list of users which belong to a specific database
#
# @param databaseID Database ID
#
# @return Array ref of hash refs
# @li ID - Database user ID
# @li Username - Database user username
sub getDatabaseUsers
{
	my $databaseID = shift;


	if (!defined($databaseID)) {
		setError("Parameter 'databaseID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($databaseID = isNumber($databaseID))) {
		setError("Parameter 'databaseID' is invalid");
		return ERR_PARAM;
	}

	my $database = getDatabase($databaseID);
	if (!isHash($database)) {
		return $database;
	}

	# Return list of users which belong to a specific database
	my $sth = DBSelect("SELECT ID, Username FROM siteDatabaseUsers WHERE DatabaseID = ".DBQuote($database->{'ID'}));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Fetch rows
	my @users = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Username ))) {
		my $item;
		$item->{'ID'} = $row->{'ID'};
		$item->{'Username'} = $row->{'Username'};
		push(@users,$item);
	}

	DBFreeRes($sth);

	return (\@users,scalar(@users));
}



## @method getSystemDatabases($search)
# Return list of systemDatabases
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - System database ID
# @li SystemDatabaseName - Pretty system database name
# @li Type - System database type
# @li Server - Server hosting the system database
sub getSystemDatabases
{
	my $search = shift;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'SystemDatabaseName' => 'Server',
		'Username' => 'Username',
	};

	# Select all agents
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID, Type, Server
			FROM
				systemDatabases
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Loop with results
	my @databases = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Type Server ))) {
		my $item;
		$item->{'ID'} = $row->{'ID'};
		$item->{'SystemDatabaseName'} = $row->{'Server'} . " (" . $row->{'Type'} . ")";
		$item->{'Type'} = $row->{'Type'};
		$item->{'Server'} = $row->{'Server'};
		push(@databases,$item);
	}

	DBFreeRes($sth);
	return (\@databases,$numResults);
}



## @fn cascadeRemove($siteID)
# Cascade on site remove and remove databases
#
# @param siteID Site ID
#
# @return 0 on success, < 0 on error
sub cascadeRemove
{
	my $siteID = shift;


	if (!defined($siteID)) {
		setError("Parameter 'siteID' is not defined");
		return ERR_PARAM;
	}
	if ($siteID < 1) {
		setError("Parameter 'siteID' is invalid");
		return ERR_PARAM;
	}

	my $databases = getDatabases($siteID);
	if (ref $databases ne "HASH") {
		 return $databases;
	}

	# Loop and remove
	foreach my $dbID (keys %{$databases}) {
		my $res = removeDatabase($dbID);
		if ($res != RES_OK) {
			return $res;
		}
	}

	return RES_OK;
}






1;
# vim: ts=4
