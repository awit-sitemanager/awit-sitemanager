# Server Side Includes
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::WebHosting::SSI
# SSI support for sites
package smlib::WebHosting::SSI;


use strict;
use warnings;


use smlib::logging;


## @method getSSI($siteID)
# Return SSI information hash
#
# @param siteID Site ID
#
# @return Hash ref
# @li Enabled - SSI is enabled
sub getSSI
{
	my $siteID = shift;


	if (!defined($siteID)) {
		setError("Parameter 'siteID' is not defined");
		return ERR_PARAM;
	}
	if ($siteID < 1) {
		setError("Parameter 'siteID' is invalid");
		return ERR_PARAM;
	}

	# Get frontpage ext info
	my $sth = DBSelect("
			SELECT
				Type
			FROM
				siteSSIExt
			WHERE
				SiteID = ".DBQuote($siteID)
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	my $res;
	if ($sth->rows == 0) {
		$res->{'Enabled'} = 0;
	} else {
		$res->{'Enabled'} = 1;
		my $row = $sth->fetchrow_hashref();
		$res->{'Type'} = $row->{'Type'};
	}

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method addSiteSSI($siteID,$type)
# Add SSI
#
# @param siteID Site ID
# @param type SSI type to add
#
# @return 0 on success, < 0 on error
sub addSiteSSI
{
	my ($siteID,$type) = @_;


	if (!defined($siteID)) {
		setError("Parameter 'siteID' is not defined");
		return ERR_PARAM;
	}
	if ($siteID < 1) {
		setError("Parameter 'siteID' is invalid");
		return ERR_PARAM;
	}

	my $siteInfo = smlib::sites::getSiteRecord($siteID);
	if (ref $siteInfo ne "HASH") {
		return $siteInfo;
	}

	# Check if its enabled
	my $num_results = DBSelectNumResults("FROM siteSSIExt WHERE SiteID = ".DBQuote($siteID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	# Check if we already installed
	if ($num_results > 0) {
		setError("Site ".$siteInfo->{'DomainName'}." already has SSI enabled");
		DBFreeRes($sth);
		return -1;
	}

	DBBegin();

	# Insert SSI data into database
	$sth = DBDo("
			INSERT INTO siteSSIExt
				(SiteID,Type)
			VALUES
				(".
					DBQuote($siteID).",".
					DBQuote($type)."
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	$ID = DBLastInsertID("siteSSI","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Parse http config
	my $httpdConf = smlib::Apache::Config->new();
	if ($httpdConf->parse($siteInfo->{'HttpdConfFile'})) {
# FIXME - add error
		smlib::logging::log(smlib::Apache::Config::Error());
		setError("Configuration file error");
		DBRollback();
		return ERR_UNKNOWN;
	}

	# Add SSI
	my $confRoot = $httpdConf->getRoot();
	my ($dirConf) = $confRoot->getSection("Directory",$siteInfo->{'HomeDirectory'}."/htdocs");
	# If we don't find it, add one
	$dirConf = $confRoot->addSection("Directory",$siteInfo->{'HomeDirectory'}."/htdocs") if (!defined($dirConf));

	my @directives = $dirConf->getDirective("Options");
	my $options = "";

	# Combine options
	foreach my $directive (@directives) {
		$options .= $directive->value . " ";
	}

	# Replace options
	$dirConf->replaceDirective("Options",undef,"$options +IncludesNoExec");

	@directives = $dirConf->getDirective("php_value");
	foreach my $directive (@directives) {
		# Remove zlib compression directives
		if ($directive->value =~ /^zlib\.output_compression/) {
			$dirConf->removeDirective("php_value",$directive->value);
		}
	}

	# Disable zlib compression
	$dirConf->addDirective("php_value",'zlib.output_compression "off"');

	# If we must add with xbithack
	if ($type eq "xbithack") {
		# If we doing it the xbithack way, add directive
		$dirConf->addDirective("XBitHack","on");
	}

	# Write out config file
	if ($httpdConf->write($siteInfo->{'HttpdConfFile'})) {
# FIXME - add error
		smlib::logging::log(smlib::Apache::Config::Error());
		setError("Configuration file error");
		DBRollback();
		return ERR_UNKNOWN;
	}

	DBCommit();

	return $ID;
}



## @method removeSiteSSI($siteID)
# Remove site SSI
#
# @param siteID Site ID
#
# @return 0 on success, < 0 on error
sub removeSiteSSI
{
	my $siteID = shift;


	if (!defined($siteID)) {
		setError("Parameter 'siteID' is not defined");
		return ERR_PARAM;
	}
	if ($siteID < 1) {
		setError("Parameter 'siteID' is invalid");
		return ERR_PARAM;
	}

	my $SSIInfo = getSSIRecord($siteID);
	if (ref $SSIInfo ne "HASH") {
		return $SSIInfo;
	}

	my $siteInfo = smlib::sites::getSiteRecord($siteID);
	if (ref $siteInfo ne "HASH") {
		return $siteInfo;
	}


	DBBegin();

	# Insert site data into database
	my $sth = DBDo("DELETE FROM siteSSIExt WHERE SiteID = ".DBQuote($siteID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Parse http config
	my $httpdConf = smlib::Apache::Config->new();
	if ($httpdConf->parse($siteInfo->{'HttpdConfFile'})) {
# FIXME - add error
		smlib::logging::log(smlib::Apache::Config::Error());
		setError("Configuration file error");
		return ERR_UNKNOWN;
	}

	# Add SSI
	my $confRoot = $httpdConf->getRoot();
	foreach my $vhost ($confRoot->getSection("Directory",$siteInfo->{'HomeDirectory'}."/htdocs")) {
		my @directives = $vhost->getDirective("Options");
		my $options = "";

		# Combine options
		foreach my $directive (@directives) {
			$options .= $directive->value . " ";
		}

		$options =~ s/\+Includes(NoExec|)//;

		# Check if options are now blank, if so remove, else replace
		if ($options =~ /^\s*$/) {
			$vhost->removeDirective("Options");
		} else {
			$vhost->replaceDirective("Options",undef,$options);
		}

		# Remove zlib compression directive
		$vhost->removeDirective("php_value",'zlib.output_compression "off"');

		# If xbithack was on, remove
		if ($SSIInfo->{'Type'} eq "xbithack") {
			# If we doing it the xbithack way, remove directive
			$vhost->removeDirective("XBitHack","on");
		}
	}

	# Write out config file
	if ($httpdConf->write($siteInfo->{'HttpdConfFile'})) {
# FIXME - add error
		smlib::logging::log(smlib::Apache::Config::Error());
		setError("Configuration file error");
		DBRollback();
		return ERR_UNKNOWN;
	}

	DBCommit();

	return RES_OK;
}



## @method getSSIRecord($siteID)
# Return SSI record hash
#
# @param siteID Site ID
#
# @return Hash ref
# @li ID - SSI ID
# @li Type - SSI Type
sub getSSIRecord
{
	my $siteID = shift;


	if (!defined($siteID)) {
		setError("Parameter 'siteID' is not defined");
		return ERR_PARAM;
	}
	if ($siteID < 1) {
		setError("Parameter 'siteID' is invalid");
		return ERR_PARAM;
	}

	# Query site
	my $sth = DBSelect("
			SELECT
				ID, Type
			FROM
				siteSSIExt
			WHERE
				SiteID = ".DBQuote($siteID)
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows != 1) {
		setError("Failed to find SSI extensions for site '$siteID'");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID Type ));
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Type'} = $row->{'Type'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



1;
# vim: ts=4
