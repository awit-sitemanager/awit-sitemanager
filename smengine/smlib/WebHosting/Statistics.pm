# Site stats
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::WebHosting::Statistics
# Stats support for sites module
package smlib::WebHosting::Statistics;


use strict;
use warnings;

use smlib::constants;
use smlib::logging;
use awitpt::db::dblayer;
use smlib::sites ();

use File::Path qw(mkpath rmtree);



## @method getStats($siteID)
# Return stats information hash
#
# @param siteID Site ID
#
# @return Hash ref
# @li Enabled - 1 if stats are enabled
sub getStats
{
	my $siteID = shift;


	# Get site stats info
	my $num_results = DBSelectNumResults("FROM siteStats WHERE SiteID = ".DBQuote($siteID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	my $res;
	if ($num_results == 0) {
		$res->{'Enabled'} = 0;
	} else {
		$res->{'Enabled'} = 1;
	}

	return $res;
}



## @method addSiteStats($siteID)
# Add stats generation to a site
#
# @param siteID Site ID
#
# @return Site stats ID on success, < 0 on error
sub addSiteStats
{
	my $siteID = shift;


	if (!defined($siteID)) {
		setError("Parameter 'siteID' is not defined");
		return ERR_PARAM;
	}
	if ($siteID < 1) {
		setError("Parameter 'siteID' is invalid");
		return ERR_PARAM;
	}

	my $siteInfo = smlib::sites::getSiteRecord($siteID);
	if (ref $siteInfo ne "HASH") {
		return $siteInfo;
	}

	# Check if its enabled
	my $num_results = DBSelectNumResults("FROM siteStats WHERE SiteID = ".DBQuote($siteID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	# Check if we already installed
	if ($num_results > 0) {
		setError("Site ".$siteInfo->{'DomainName'}." already has stats enabled");
		return ERR_CONFLICT;
	}

	DBBegin();

	# Formulate stats dir
	my $statsDir = $siteInfo->{'HomeDirectory'}."/stats";

	# Create stats dir, chown and chmod
	mkpath($statsDir,0,0755);
	chown(0,0,$statsDir);  # Chown to root
	chmod(0755,$statsDir);

	# Insert site data into database
	my $sth = DBDo("
			INSERT INTO siteStats
				(SiteID,StatsEngine,StatsDir)
			VALUES (".
				DBQuote($siteID).",".
				DBQuote("webdruid").",".
				DBQuote($statsDir)."
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	my $ID = DBLastInsertID("siteStats","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	return $ID;
}



## @fn cascadeRemove($siteID)
# Cascade on site remove and remove stats
#
# @param siteID Site ID
#
# @return 0 on success, < 0 on error
sub cascadeRemove
{
	my $siteID = shift;


	if (!defined($siteID)) {
		setError("Parameter 'siteID' is not defined");
		return ERR_PARAM;
	}
	if ($siteID < 1) {
		setError("Parameter 'siteID' is invalid");
		return ERR_PARAM;
	}

	my $statsInfo = getStats($siteID);
	if (ref $statsInfo ne "HASH") {
		return $statsInfo;
	}

	# If enabled remove
	if ($statsInfo->{'Enabled'} == 1) {
		return removeSiteStats($siteID);
	}

	return RES_OK;
}



## @method removeSiteStats($siteID)
# Remove site stats
#
# @param siteID Site ID
#
# @return 0 on success, < 0 on error
sub removeSiteStats
{
	my $siteID = shift;


	if (!defined($siteID)) {
		setError("Parameter 'siteID' is not defined");
		return ERR_PARAM;
	}
	if ($siteID < 1) {
		setError("Parameter 'siteID' is invalid");
		return ERR_PARAM;
	}

	my $statsInfo = getStatsRecord($siteID);
	if (ref $statsInfo ne "HASH") {
		return $statsInfo;
	}

	my $siteInfo = smlib::sites::getSiteRecord($siteID);
	if (ref $siteInfo ne "HASH") {
		return $siteInfo;
	}

	DBBegin();

	# Insert site data into database
	my $sth = DBDo("DELETE FROM siteStats WHERE SiteID = ".DBQuote($siteID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	# Remove stats dir
	rmtree($statsInfo->{'StatsDir'},0,0);

	return RES_OK;
}



## @method getStatsRecord($siteID)
# Return stats record hash
#
# @param siteID Site ID
#
# @return Hash ref with stats info
# @li StatsEngine - Stats engine to use
# @li StatsDir - Stats directory
sub getStatsRecord
{
	my $siteID = shift;


	if (!defined($siteID)) {
		setError("Parameter 'siteID' is not defined");
		return ERR_PARAM;
	}
	if ($siteID < 1) {
		setError("Parameter 'siteID' is invalid");
		return ERR_PARAM;
	}

	# Query site
	my $sth = DBSelect("
			SELECT
				StatsEngine,
				StatsDir
			FROM
				siteStats
			WHERE
				SiteID = ".DBQuote($siteID)
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	# Check if we got results
	if ($sth->rows != 1) {
		setError("No stats data for site ID '$siteID'");
		DBFreeRes($sth);
		return undef;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( StatsEngine StatsDir ));
	my $res;

	$res->{'StatsEngine'} = $row->{'StatsEngine'};
	$res->{'StatsDir'} = $row->{'StatsDir'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}





1;
# vim: ts=4
