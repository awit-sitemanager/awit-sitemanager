# WebHosting FTP functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::WebHosting::FTP
# FTP handling for sites module
package smlib::WebHosting::FTP;


use strict;
use warnings;

use smlib::constants;
use smlib::logging;
use awitpt::db::dblayer;
use smlib::system qw(hashPassword);
use smlib::util;
use smlib::WebHosting ();


## @method createFTPUser($ftpUserInfo)
# Add a ftp user to a website
#
# @param ftpUserInfo
# @li WebsiteID - Website ID
# @li Username - FTP username
# @li Password - FTP user password
# @li AgentDisabled - Optional, flag to indicate agent has disabled this site (NOT IMPLEMENTED
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
#
# @return FTPUser ID
sub createFTPUser
{
	my $ftpUserInfo = shift;


	# Check params
	if (!defined($ftpUserInfo)) {
		setError("Parameter 'ftpUserInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($ftpUserInfo)) {
		setError("Parameter 'ftpUserInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($ftpUserInfo->{'WebsiteID'})) {
		setError("Parameter 'ftpUserInfo' has no element 'WebsiteID'");
		return ERR_PARAM;
	}
	if (!defined($ftpUserInfo->{'WebsiteID'} = isNumber($ftpUserInfo->{'WebsiteID'}))) {
		setError("Parameter 'ftpUserInfo' element 'WebsiteID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($ftpUserInfo->{"Username"})) {
		setError("Parameter 'ftpUserInfo' has no element 'Username'");
		return ERR_PARAM;
	}
	if (!defined($ftpUserInfo->{'Username'} = isUsername($ftpUserInfo->{'Username'}))) {
		setError("Parameter 'ftpUserInfo' element 'Username' is invalid");
		return ERR_PARAM;
	}
	if (!defined($ftpUserInfo->{"Password"})) {
		setError("Parameter 'ftpUserInfo' has no element 'Password'");
		return ERR_PARAM;
	}

	my $website = smlib::WebHosting::getWebsite($ftpUserInfo->{'WebsiteID'});
	if (!isHash($website)) {
		return $website;
	}

	# Check if agent exists
	my $num_results = DBSelectNumResults("FROM ftpUsers WHERE Username = ".DBQuote($ftpUserInfo->{'Username'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("FTP username already exists");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert FTP user into database
	my $sth = DBDo("
			INSERT INTO ftpUsers
				(SiteID, Username, Password, UserID, GroupID, HomeDirectory)
			VALUES
				(".
					DBQuote($website->{'ID'}).",".
					DBQuote($ftpUserInfo->{'Username'}).",".
					DBQuote(hashPassword($ftpUserInfo->{'Password'})).",".
					DBQuote($website->{'SiteUID'}).",".
					DBQuote($website->{'SiteGID'}).",".
					DBQuote($website->{'HomeDirectory'}).
				")"
	);
	# Check for errors
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	my $ID = DBLastInsertID("ftpUsers","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateFTPUser(ftpUserInfo)
# Update a FTP user
#
# @param ftpUserInfo Detail hash
# @li ID - FTP user ID
# @li Password - FTP user password
# @li AgentDisabled - Optional, flag to indicate agent has disabled this site (NOT IMPLEMENTED
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
#
# @return 0 on success, < 0 on error
sub updateFTPUser
{
	my $ftpUserInfo = shift;


	if (!defined($ftpUserInfo)) {
		setError("Parameter 'ftpUserInfo' not defined");
		return ERR_PARAM;
	}
	if (!defined($ftpUserInfo->{"ID"})) {
		setError("Parameter 'ftpUserInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($ftpUserInfo->{'ID'} = isNumber($ftpUserInfo->{'ID'}))) {
		setError("Parameter 'ftpUserInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	my $ftpUser = getFTPUser($ftpUserInfo->{'ID'});
	if (!isHash($ftpUser)) {
		return $ftpUser;
	}

	my @updates = ();

	if (defined($ftpUserInfo->{'Password'})) {
		push(@updates,"Password = ".DBQuote(hashPassword($ftpUserInfo->{'Password'})));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);
	if ($updateStr eq "") {
		setError("No updates specified for FTP user");
		return ERR_USAGE;
	}

	DBBegin();

	# Update
	my $sth = DBDo("
			UPDATE
				ftpUsers
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($ftpUserInfo->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}


## @method removeFTPUser($ftpUserID)
# Remove FTP user
#
# @param ftpUserID FTP user ID
#
# @return 0 on success, < 0 on error
sub removeFTPUser
{
	my $ftpUserID = shift;


	if (!defined($ftpUserID)) {
		setError("Parameter 'ftpUserID' not defined");
		return ERR_PARAM;
	}
	if (!defined($ftpUserID = isNumber($ftpUserID))) {
		setError("Parameter 'ftpUserID' is invalid");
		return ERR_PARAM;
	}

	my $ftpUser = getFTPUser($ftpUserID);
	if (!isHash($ftpUser)) {
		return $ftpUser;
	}

	DBBegin();

	# Remove from database
	my $sth = DBDo("DELETE FROM ftpUsers WHERE ID = ".DBQuote($ftpUser->{'ID'}));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method getFTPUsers($websiteID,$search)
# Return an array of FTP users
#
# @param websiteID Website ID
# @param search Search criteria
#
# @return Array ref of hash refs
# @li ID - FTP user ID
# @li Username - FTP user username
# @li HomeDirectory - FTP user home directory
# @li AgentDisabled - Set if the agent has disabled this user (NOT IMPLEMENTED)
# @li Disabled - True if FTP user is disabled (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getFTPUsers
{
	my ($websiteID,$search) = @_;


	if (!defined($websiteID)) {
		setError("Parameter 'websiteID' not defined");
		return ERR_PARAM;
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		setError("Parameter 'websiteID' is invalid");
		return ERR_PARAM;
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ftpUsers.ID',
		'Username' => 'ftpUsers.Username',
# FIXME - implement
#		'AgentRef' => 'ftpUsers.AgentRef',
	};

	# Select FTP users
# FIXME - implement
#				ftpUsers.AgentRef, ftpUsers.AgentDisabled
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ftpUsers.ID, ftpUsers.Username, ftpUsers.HomeDirectory
			FROM
				ftpUsers, sites
			WHERE
				sites.ID = ".DBQuote($websiteID)."
				AND ftpUsers.SiteID = sites.ID
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @users = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Username HomeDirectory ))) {
		my $user;

		$user->{'ID'} = $row->{'ID'};
		$user->{'Username'} = $row->{'Username'};
		$user->{'HomeDirectory'} = $row->{'HomeDirectory'};
#		$user->{'AgentRef'} = $row->{'AgentRef'};
#		$user->{'AgentDisabled'} = $row->{'AgentDisabled'};

		push(@users,$user);
	}

	DBFreeRes($sth);

	return (\@users,$numResults);
}



## @method getFTPUser($ftpUserID)
# Return FTP user hash
#
# @param ftpUserID FTP user ID
#
# @return Hash ref
# @li ID - FTP user ID
# @li Username - FTP user username
# @li HomeDirectory - FTP user home directory
# @li AgentDisabled - Set if the agent has disabled this user (NOT IMPLEMENTED)
# @li Disabled - True if FTP user is disabled (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getFTPUser
{
	my $ftpUserID = shift;


	if (!defined($ftpUserID)) {
		setError("Parameter 'ftpUserID' not defined");
		return ERR_PARAM;
	}
	if (!defined($ftpUserID = isNumber($ftpUserID))) {
		setError("Parameter 'ftpUserID' is invalid");
		return ERR_PARAM;
	}

	# Query ftp user
	my $sth = DBSelect("
			SELECT
				ID, Username, HomeDirectory
			FROM
				ftpUsers
			WHERE
				ID = ".DBQuote($ftpUserID)
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("FTP user not found");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID Username HomeDirectory ));
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Username'} = $row->{'Username'};
	$res->{'HomeDirectory'} = $row->{'HomeDirectory'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @fn cascadeRemove($websiteID)
# Cascade on site remove and remove ftp users
#
# @param websiteID Site ID
#
# @return 0 on success, < 0 on error
sub cascadeRemove
{
	my $websiteID = shift;


	if (!defined($websiteID)) {
		setError("Parameter 'websiteID' not defined");
		return ERR_PARAM;
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		setError("Parameter 'websiteID' is invalid");
		return ERR_PARAM;
	}

	return removeFTPUsers($websiteID);
}



## @method removeFTPUsers($websiteID)
# Remove FTP users from a site
#
# @param websiteID Site ID
sub removeFTPUsers
{
	my $websiteID = shift;


	if (!defined($websiteID)) {
		setError("Parameter 'websiteID' not defined");
		return ERR_PARAM;
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		setError("Parameter 'websiteID' is invalid");
		return ERR_PARAM;
	}

	my $website = smlib::WebHosting::getWebsite($websiteID);
	if (!isHash($website)) {
		return $website;
	}

	# Remove all site ftp users
	my $sth = DBDo("DELETE FROM ftpUsers WHERE SiteID = ".DBQuote($website->{'ID'}));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	return RES_OK;
}





1;
# vim: ts=4
