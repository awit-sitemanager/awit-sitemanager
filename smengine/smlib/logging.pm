# Logging constants
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::logging
# Logging functions & constants
package smlib::logging;

use strict;
use warnings;


# Exporter stuff
require Exporter;
our (@ISA,@EXPORT,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT = qw(
	LOG_ERR
	LOG_WARN
	LOG_NOTICE
	LOG_INFO
	LOG_DEBUG

	setError
	getError
);
@EXPORT_OK = qw(
);


use constant {
	LOG_ERR		=> 0,
	LOG_WARN	=> 1,
	LOG_NOTICE	=> 2,
	LOG_INFO	=> 3,
	LOG_DEBUG	=> 4
};


# Server object, we only use the ->log function though
my $server;
# Our current error message
my $error;



## @fn setLogger($logger)
# Set the logger to use
#
# @param logger Logging function
sub setLogger
{
	$server = shift;
}


## @fn log($level,$msg,@params)
# Log something
#
# @param level Logging level
# @param msg Message to log
# @param params If $msg is a printf-style string, these are the params
sub log
{
	my ($level,$msg,@params) = @_;


	# If no error is given, display the last one
	if (!defined($msg)) {
		$msg = getError();
	}

	$server->log($level,"[BACKEND] $msg",@params);
}


## @internal
# @fn setError($err)
# This function is used to set the last error for this class
#
# @param err Error message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = {
		'File' => $filename,
		'Line' => $line,
		'Subroutine' => $subroutine,
		'Message' => $err
	};
}

## @internal
# @fn Error
# Return current error message
#
# @return Last error message
sub getError
{
	my $err = $error;

	# Reset error
	$error = undef;

	if (defined($err)) {
		return $err->{'Message'};
	} else {
		return undef;
	}
}






1;
# vim: ts=4
