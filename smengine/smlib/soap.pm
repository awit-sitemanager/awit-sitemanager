# SOAP functions we need
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::soap
# SOAP functions we need
package smlib::soap;

use strict;
use warnings;


require Exporter;
our (@ISA,@EXPORT);
@ISA = qw(Exporter);
@EXPORT = qw(
	SOAPResponse
	SOAP_Param_toStr

	SOAPError
	SOAPSuccess
);

use SOAP::Lite;
use smlib::constants;


## @fn SOAPResponse($res,$data,$extra)
# Build soap response
#
# @param res Response code
# RES_ERROR - Error
# RES_OK - Success
#
# @param data Data for this response code
# Either the data portion and return value of a RES_OK or the
# error code in the case of RES_ERROR.
#
# @param extra Any extra data
# Normally used with RES_ERROR, this is a verbose description of what
# error occured.
#
# @return Hash ref
# @li Result - Result code
# @li Data - Data for this response
# @li Info - Additional info
sub SOAPResponse
{
	my ($res,$data,$extra) = @_;


	my %ret;
	$ret{'Result'} = $res;
	$ret{'Data'} = $data;
	$ret{'Info'} = $extra;

	return \%ret;
}


## @fn SOAP_Param_toStr($string)
# Force a param given to SOAP into a string
#
# @param string String to return a soap data type on
#
# @return SOAP string and datatype
sub SOAP_Param_toStr
{
	my $string = shift;


	return SOAP::Data->type(string => $string);
}



## @fn SOAPError($code,$reason)
# Build soap error response
#
# @param code Error code
#
# @param reason Error reason
#
# @return SOAP response
sub SOAPError
{
	my ($code,$reason) = @_;

	# Build response
	my $response = smlib::soap::response->new();
	$response->setStatus(RES_ERROR);
	$response->addField('ErrorCode','int');
	$response->addField('ErrorReason','string');
	$response->parseHashRef({
		'ErrorCode' => $code,
		'ErrorReason' => $reason
	});

	return $response->export();
}



## @fn SOAPSuccess($value,$type,$name)
# Build soap success response
#
# @param value Value to return, defaults to 0
#
# @param type Type of value to return, defaults to 'int'
#
# @param name Name of variable, defaults to 'Result'
#
# @return SOAP response, @see smlib::soap::response
sub SOAPSuccess
{
	my ($value,$type,$name) = @_;


	# Reasonable defaults...
	$value = 0 if (!defined($value));
	$type = 'int' if (!defined($type));
	$name = 'Result' if (!defined($name));

	# Build response
	my $response = smlib::soap::response->new();
	# Add data
	$response->addField($name,$type);
	$response->parseHashRef({
		$name => $value,
	});
	# Export
	return $response->export();
}




## @class smlib::soap::response
package smlib::soap::response;


use smlib::constants;
use SOAP::Lite;


## @internal
# @fn new
# Get new class instance
sub new
{
	my $class = shift;
	my $self = {
		_fields => undef,
		_id => undef,
		_results => undef,
		_datasetSize => undef,
		_status => undef
	};
	bless $self, $class;
	return $self;
}



## @fn setID($id)
# Set ID column
#
# @param id ID column name
sub setID
{
	my ($self,$id) = @_;

	$self->{'_id'} = $id;
}



## @fn setStatus($status)
# Set response status
#
# @param status Either RES_OK (default) or RES_ERROR
sub setStatus
{
	my ($self,$status) = @_;

	$self->{'_status'} = $status;
}



## @fn addField($name,$type)
# Add a field to our return results
#
# @param name Field name
# @param type Field type, 'int', 'string', 'float', 'boolean', 'date'
sub addField
{
	my ($self,$name,$type) = @_;


	# Build field
	my $field = {
		'name' => $name,
		'type' => $type,
	};

	# Set ISO date format
	if ($type eq "date") {
		$field->{'dateFormat'} = 'Y-m-d';
	}

	# Add field to list
	push(@{$self->{'_fields'}},$field);
}



## @fn setDatasetSize($size)
# Set how many records are returned in the dataset
#
# @param size Dataset size
sub setDatasetSize
{
	my ($self,$size) = @_;

	$self->{'_datasetSize'} = $size;
}



## @fn parseArrayRef($arrayref)
# Parse in the array of results and fix it up
#
# @param arrayref Array ref containing the results
sub parseArrayRef
{
	my ($self,$arrayref) = @_;

	# Start off with blank results
	@{$self->{'_results'}} = ();

	# Loop with array items
	foreach my $aitem (@{$arrayref}) {
		my $item;

		# Loop with fields we want
		foreach my $field (@{$self->{'_fields'}}) {
			# Set correct datatype
			$item->{$field->{'name'}} = SOAP::Data->type(
				$field->{'type'} => $aitem->{$field->{'name'}}
			);
		}

		push(@{$self->{'_results'}},$item);
	}
}



## @fn parseHashRef($hashref)
# Parse in the hash of results and fix it up
#
# @param hashref Hash ref containing the results
sub parseHashRef
{
	my ($self,$hashref) = @_;

	# Start off with blank results
	%{$self->{'_results'}} = ();

	# Loop with fields we want
	foreach my $field (@{$self->{'_fields'}}) {
		# Set correct datatype
		$self->{'_results'}->{$field->{'name'}} = SOAP::Data->type(
			$field->{'type'} => $hashref->{$field->{'name'}}
		);
	}
}



## @fn export
# Export response into something we return via SOAP
#
# @return Hash ref
# @li result - Result code/status
# @li data - Ref containing results
# @li metaData - Metadata containing info about the results being returned
# Metadata contains properties..
# - root: root element, which is always 'data'
# - fields: Optional field description, arrayref of hash refs, name = 'name', type 'type' and 'dateFormat' = 'Y-m-d'
# - id: Optional ID field name
# - totalProperty: Optional property name containing the number of records, always 'datasetSize'
# @li datasetSize Optional, number of records we're rturning
sub export
{
	my $self = shift;

	# Build result
	my $ret = {

		'result' => $self->{'_status'},

		# Additional stuff for other interfaces to SOAP
		'success' => SOAP::Data->type(boolean => !$self->{'_status'} ? 1 : 0),
		'metaData' => {
			'successProperty' => 'success'
		},
	};

	# If we have results, add them
	if (defined($self->{'_results'})) {
		$ret->{'data'} = $self->{'_results'};
		$ret->{'metaData'}->{'root'} = 'data';

		# If we have fields, set them up
		if (defined($self->{'_fields'})) {
			$ret->{'metaData'}->{'fields'} = $self->{'_fields'};
		}
	}

	# Check if we have an ID set
	if (defined($self->{'_id'})) {
		$ret->{'metaData'}->{'id'} = $self->{'_id'};
	}

	# Check if we must return the dataset size
	if (defined($self->{'_datasetSize'})) {
		$ret->{'metaData'}->{'totalProperty'} = 'datasetSize';
		$ret->{'datasetSize'} = $self->{'_datasetSize'};
	}

	return $ret;
}



1;
# vim: ts=4
