# Dynamic DNS functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::DNSHosting::DDNS
# Dynamic DNS handling class
package smlib::DNSHosting::DDNS;


use strict;
use warnings;

use smlib::constants;
use smlib::logging;
use awitpt::db::dblayer;
use smlib::system qw(hashPassword);
use smlib::util;



# Use the smlib servers autoloader so we can get remote calls working nicely
sub AUTOLOAD { our $AUTOLOAD; return smlib::servers::RPC_AUTOLOADER($AUTOLOAD,@_); }


## @method createDDNSUser($userInfo)
# Create a DDNS user
#
# @param userInfo User info hash
# @li AgentID - Agent ID
# @li Username - Dynamic DNS user username
# @li Password - Dynamic DNS user password
# @li Disabled - Disabled flag (NOT IMPLEMENTED)
# @li AgentRef - Agent reference
#
# @return Dynamic DNS user ID
sub _createDDNSUser
{
	my $userInfo = shift;


	if (!defined($userInfo)) {
		setError("Parameter 'userInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($userInfo)) {
		setError("Parameter 'userInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{'AgentID'})) {
		setError("Parameter 'userInfo' element 'AgentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{'AgentID'} = isNumber($userInfo->{'AgentID'}))) {
		setError("Parameter 'userInfo' element 'AgentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{"Username"})) {
		setError("Parameter 'userInfo' has no element 'Username'");
		return ERR_PARAM;
	}
	if (!isVariable($userInfo->{"Username"})) {
		setError("Parameter 'userInfo' element 'Username' is invalid");
		return ERR_PARAM;
	}
	if ($userInfo->{"Username"} eq "") {
		setError("Parameter 'userInfo' element 'Username' is blank");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{'Username'} = isUsername($userInfo->{'Username'}))) {
		setError("Parameter 'userInfo' element 'Username' is not a valid username");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{"Password"})) {
		setError("Parameter 'userInfo' has no element 'Password'");
		return ERR_PARAM;
	}
	if (!isVariable($userInfo->{"Password"})) {
		setError("Parameter 'userInfo' element 'Password' is invalid");
		return ERR_PARAM;
	}
	if ($userInfo->{"Password"} eq "") {
		setError("Parameter 'userInfo' element 'Password' is blank");
		return ERR_PARAM;
	}
	
	if (defined($userInfo->{'AgentRef'})) {
		if (!isVariable($userInfo->{"AgentRef"})) {
			setError("Parameter 'userInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}

	my $num_results = DBSelectNumResults("FROM dynaDNSUsers WHERE Username = ".DBQuote($userInfo->{'Username'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("DDNS username '".$userInfo->{'Username'}."' already exists");
		return ERR_EXISTS;
	}

	my $extraColumns = "";
	my $extraValues = "";

	if (defined($userInfo->{'AgentRef'})) {
		$extraColumns .= ",AgentRef";
		$extraValues .= ",".DBQuote($userInfo->{'AgentRef'});
	}

	DBBegin();

	# Insert dyna dns users
	my $sth = DBDo("
			INSERT INTO dynaDNSUsers
				(AgentID,Username,Password$extraColumns)
			VALUES (".
					DBQuote($userInfo->{'AgentID'}).",".
					DBQuote($userInfo->{'Username'}).",".
					DBQuote(hashPassword($userInfo->{'Password'})).
					$extraValues."
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	my $ID = DBLastInsertID("dynaDNSUsers","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateDDNSUser($userInfo)
# Update a user
#
# @param userInfo User info hash
# @li ID - DDNS user ID
# @li AgentID - Agent ID
# @li Username - Optional username
# @li Password - Optional Password
# @li AgentRef - Optional agent reference 
sub _updateDDNSUser
{
	my $userInfo = shift;


	if (!defined($userInfo)) {
		setError("Parameter 'userInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($userInfo)) {
		setError("Parameter 'userInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{"ID"})) {
		setError("Parameter 'userInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{'ID'} = isNumber($userInfo->{'ID'}))) {
		setError("Parameter 'userInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}
	if (defined($userInfo->{'AgentID'})) {
		if (!defined($userInfo->{'AgentID'} = isNumber($userInfo->{'AgentID'}))) {
			setError("Parameter 'userInfo' element 'AgentID' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($userInfo->{'Username'})) {
		if (!isVariable($userInfo->{"Username"})) {
			setError("Parameter 'userInfo' element 'Username' is invalid");
			return ERR_PARAM;
		}
		if ($userInfo->{"Username"} eq "") {
			setError("Parameter 'userInfo' element 'Username' is blank");
			return ERR_PARAM;
		}
		if (!defined($userInfo->{'Username'} = isUsername($userInfo->{'Username'}))) {
			setError("Parameter 'userInfo' element 'Username' is not a valid username");
			return ERR_PARAM;
		}
	}
	if (defined($userInfo->{'Password'})) {
		if (!isVariable($userInfo->{"Password"})) {
			setError("Parameter 'userInfo' element 'Password' is invalid");
			return ERR_PARAM;
		}
		if ($userInfo->{"Password"} eq "") {
			setError("Parameter 'userInfo' element 'Password' is blank");
			return ERR_PARAM;
		}
	}
	if (defined($userInfo->{'AgentRef'})) {
		if (!isVariable($userInfo->{"AgentRef"})) {
			setError("Parameter 'userInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}

	# Updates
	my @updates;

	# Pull in changes
	if (defined($userInfo->{'AgentID'})) {
		push(@updates,"AgentID = ".DBQuote($userInfo->{'AgentID'}));
	}

	if (defined($userInfo->{'Username'})) {
		push(@updates,"Username = ".DBQuote($userInfo->{'Username'}));
	}

	if (defined($userInfo->{'Password'})) {
		push(@updates,"Password = ".DBQuote(hashPassword($userInfo->{'Password'})));
	}

	if (defined($userInfo->{'AgentRef'})) {
		push(@updates,"AgentRef = ".DBQuote($userInfo->{'AgentRef'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);
	if ($updateStr eq "") {
		setError("No updates specified for user");
		return ERR_USAGE;
	}

	my $user = _getDDNSUser($userInfo->{'ID'});
	if (!isHash($user)) {
		return $user;
	}

	# Begin our update
	DBBegin();

	# Update user
	my $sth = DBDo("
			UPDATE
				dynaDNSUsers
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($userInfo->{'ID'})
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}


## @method removeDDNSUser($userID)
# Remove a user
#
# @param userID DDNS user ID
#
# @return 0 on success, < 0 on error
sub _removeDDNSUser
{
	my $userID = shift;


	if (!defined($userID)) {
		setError("Parameter 'userID' not defined");
		return ERR_PARAM;
	}
	if (!defined($userID = isNumber($userID))) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Check if user exists
	my $num_results = DBSelectNumResults("FROM dynaDNSUsers WHERE id = ".DBQuote($userID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("DDNS user ID '$userID' does not exist");
		return ERR_EXISTS;
	}

	# Check if user is linked to entries
	my $sth = DBSelect("
		SELECT
			zones.origin, zoneEntries.name
		FROM
			zones, zoneEntries
		WHERE
			zoneEntries.dynaDNSUser = ".DBQuote($userID)."
			AND zones.id = zoneEntries.zone
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	# If we have results, ERR
	if ($sth->rows > 0) {
		my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw( Origin Name )
		);
		DBFreeRes($sth);
		setError("DDNS user '$userID' is still linked to '".$row->{'Name'}.$row->{'Origin'}."'");
		return ERR_NOTFOUND;
	}
	DBFreeRes($sth);

	# Grab DDNS user to see if it exists
	my $user = _getDDNSUser($userID);
	if (!isHash($user)) {
		return $user;
	}

	DBBegin();

	# Remove user from database
	$sth = DBDo("DELETE FROM dynaDNSUsers WHERE ID = ".DBQuote($userID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}


## @method getDDNSUsers($DDNSUserInfo,$search)
# Return a list of dynamic DNS users
#
# @param DDNSUserInfo User info hash
# @li AgentID - Optional agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Dynamic DNS user ID
# @li AgentID - AgentID
# @li Username - Dynamic DNS username
# @li LastUpdate - Last time this user updated something
# @li AgentRef - Agent reference
sub _getDDNSUsers
{
	my ($DDNSUserInfo,$search) = @_;


	my $extraSQL = "";

	if (defined($DDNSUserInfo)) {
		# Make sure we're a hash
		if (!isHash($DDNSUserInfo)) {
			setError("Parameter 'DDNSUserInfo' is not a HASH");
			return ERR_PARAM;
		}
		# If we have an agent ID, use it
		if (defined($DDNSUserInfo->{'AgentID'})) {
			if (!defined($DDNSUserInfo->{'AgentID'} = isNumber($DDNSUserInfo->{'AgentID'}))) {
				setError("Parameter 'DDNSUserInfo' element 'AgentID'' is invalid");
				return ERR_PARAM;
			}
			# Check this agent can access the AgentID they specificed
			if (smlib::Agent::isAgentValid($DDNSUserInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentID' has an invalid value");
			}
			$extraSQL = "AND dynaDNSUsers.AgentID = ".DBQuote($DDNSUserInfo->{'AgentID'});
		}
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'dynaDNSUsers.ID',
		'AgentID' => 'dynaDNSUsers.AgentID',
		'Username' => 'dynaDNSUsers.Username',
		'AgentRef' => 'dynaDNSUsers.AgentRef'
	};

	# Select users
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				dynaDNSUsers.ID,
				dynaDNSUsers.AgentID,
				agents.Name AS AgentName,
				dynaDNSUsers.Username,
				dynaDNSUsers.LastUpdate,
				dynaDNSUsers.AgentRef
			FROM
				dynaDNSUsers, agents
			WHERE
				agents.ID = dynaDNSUsers.AgentID
				$extraSQL
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}
	# Fetch rows
	my @users = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw( ID AgentID Username LastUpdate AgentName AgentRef )
			)
	) {
		my $entry;

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'AgentID'} = $row->{'AgentID'};
		$entry->{'AgentName'} = $row->{'AgentName'};
		$entry->{'Username'} = $row->{'Username'};
		$entry->{'LastUpdate'} = $row->{'LastUpdate'};
		$entry->{'AgentRef'} = $row->{'AgentRef'};

		push(@users,$entry);

	}

	DBFreeRes($sth);

	return (\@users,$numResults);
}


## @method getDDNSUser($userID)
# Return a hash for the relevant user
#
# @param userID DDNS user ID
#
# @return Hash ref containing user info
# @li ID - DDNS user ID
# @li AgentID - Agent ID
# @li Username - Username
# @li LastUpdate - Last time the user updated a record
# @li AgentRef - Agent reference
sub _getDDNSUser
{
	my $userID = shift;


	if (!defined($userID)) {
		setError("Parameter 'userID' not defined");
		return ERR_PARAM;
	}
	if (!defined($userID = isNumber($userID))) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Grab user from database
	my $sth = DBSelect("
		SELECT
			ID, AgentID, Username, LastUpdate, AgentRef
		FROM
			dynaDNSUsers
		WHERE
			ID = ".DBQuote($userID)
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results & finish off
	if ($sth->rows < 1) {
		setError("Failed to find user '$userID'");
		return ERR_NOTFOUND;
	}

	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw( ID AgentID Username LastUpdate AgentRef )
	);
	DBFreeRes($sth);

	my %userInfo = ();

	# Pull out info
	$userInfo{'ID'} = $row->{'ID'};

	$userInfo{'AgentID'} = $row->{'AgentID'};

	$userInfo{'Username'} = $row->{'Username'};
	$userInfo{'LastUpate'} = $row->{'LastUpdate'};

	$userInfo{'AgentRef'} = $row->{'AgentRef'};

	return \%userInfo;
}


## @fn canAccessDDNSUser($agentID,$DDNSUserID)
# Check to see if agent can access this DDNS user
#
# @param agentID Agent ID
# @param DDNSUserID DDNS user ID
#
# @return 1 if agent can access, 0 if not
sub canAccessDDNSUser
{
	my ($agentID,$DDNSUserID) = @_;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($DDNSUserID)) {
		setError("Parameter 'DDNSUserID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($DDNSUserID = isNumber($DDNSUserID))) {
		setError("Parameter 'DDNSUserID' is invalid");
		return ERR_PARAM;
	}

	# Query to see if we associated with this zone
	my $num_results = DBSelectNumResults("
			FROM
				dynaDNSUsers
			WHERE
				ID = ".DBQuote($DDNSUserID)."
				AND AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}





#----------------- HERE --------------------------

## @fn DynaDNS_auth($username,$password)
# Fucntion to authenticate a dynamic DNS user
#
# @param username Username
# @param password Password
#
# @return Dynamic DNS user ID on success, < 0 on error
sub DynaDNS_auth
{
	my ($username,$password) = @_;
	my $res = -1;


	if (!defined($username)) {
		smlib::logging::log(LOG_ERR,"Parameter 'username' is not defined");
		return ERR_UNKNOWN;
	}
	if ($username eq "") {
		smlib::logging::log(LOG_ERR,"Parameter 'username' is blank");
		return ERR_UNKNOWN;
	}

	if (!defined($password)) {
		smlib::logging::log(LOG_ERR,"Parameter 'password' is not defined");
		return ERR_UNKNOWN;
	}
	if ($password eq "") {
		smlib::logging::log(LOG_ERR,"Parameter 'password' is blank");
		return ERR_UNKNOWN;
	}

	# Select user details from database
	my $sth = DBSelect("
			SELECT
				ID, Password
			FROM
				dynaDNSUsers
			WHERE
				Username = ".DBQuote($username)."
				AND Status = 0
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# If we have an invalid number of rows, return negative response
	if ($sth->rows != 1) {
		DBFreeRes($sth);
		setError("Authentication failed");
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID Password ));

	# Lower both
	my $cpass = lc(hashPassword($password));
	my $dpass = lc($row->{'Password'});

	# Compare
	if ($cpass eq $dpass) {
		$res = $row->{'ID'};
	} else {
		setError("Authentication failed");
	}

	DBFreeRes($sth);

	return $res;
}



## @method DynaDNS_update($dynaDNSID,$hostname,$ip)
# Fucntion to update a dynamic dns hostname
#
# @param dynaDNSID Dynamic DNS ID
# @param hostname Hostname
# @param ip IP address
#
# @return Dynamic dns code
#	0		Success
#	-1		Parameter problem
#	-2		Authentication failure
#	-3		System Error
#	-4		Change not prohibited
#	-5		Too many updates
sub DynaDNS_update
{
	my ($dynaDNSID,$hostname,$ip) = @_;


	if (!defined($dynaDNSID)) {
		setError("Parameter 'dynaDNSID' not defined");
		return ERR_PARAM;
	}
	if ($dynaDNSID < 1) {
		setError("Parameter 'dynaDNSID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($hostname)) {
		setError("Parameter 'hostname' is undefined");
		return ERR_PARAM;
	}
	if ($hostname eq "") {
		setError("Parameter 'hostname' is blank");
		return ERR_PARAM;
	}
	if (!defined($ip)) {
		setError("Parameter 'ip' is undefined");
		return ERR_PARAM;
	}
	if ($ip eq "") {
		setError("Parameter 'ip' is blank");
		return ERR_PARAM;
	}

	# Select user details from database
	my $sth = DBSelect("
			SELECT
				zones.origin, zoneEntries.id, zoneEntries.name, dynaDNSUsers.LastUpdate,
				dynaDNSUsers.UpdateInterval, dynaDNSUsers.Status
			FROM
				zones, zoneEntries, dynaDNSUsers
			WHERE
				dynaDNSUsers.ID = ".DBQuote($dynaDNSID)."
				AND zoneEntries.DynaDNSUser = dynaDNSUsers.ID
				AND zoneEntries.type = 'A'
				AND zones.id = zoneEntries.zone
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	# Check for invalid
	if ($sth->rows != 1) {
		DBFreeRes($sth);
		setError("Authentication failed");
		return ERR_NOTFOUND;
	}

	# Look through the hostname we can modify
	my $zoneInfo = undef;

	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw( Origin ID Name LastUpdate UpdateInterval Status )
			)
	) {
		(my $origin = $row->{'origin'}) =~ s/\.$//;

		# Create hostname
		my $db_hostname = $row->{'name'}.".$origin";

		# Compare
		if ($hostname eq $db_hostname) {
			my $now = time();

			$zoneInfo->{'ID'} = $row->{'id'};
			$zoneInfo->{'UpdateInterval'} = ($row->{'UpdateInterval'} + ($now - $row->{'LastUpdate'})) / 2;
			$zoneInfo->{'LastUpdate'} = $now;

			if ($row->{'Status'} == 1) {
				$zoneInfo->{'Disabled'} = 1;
			} else {
				$zoneInfo->{'Disabled'} = 0;
			}

			last;
		}
	}

	DBFreeRes($sth);

	# If we got a record, process it
	if ($zoneInfo) {
		# Check if we've been disabled, if so change not prohibited
		if ($zoneInfo->{'Disabled'} == 1) {
			return -4;
		}

		# 15 min average update period
		if ($zoneInfo->{'UpdateInterval'} < 900) {
			return -5;
		}

		DBBegin();

		# Update entry
		$sth = DBDo("UPDATE zoneEntries SET data = ".DBQuote($ip)." WHERE id = ".DBQuote($zoneInfo->{'ID'}));
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			DBRollback();
			return -3;
		}

		# Update accounting
		$sth = DBDo("
				UPDATE dynaDNSUsers
					SET
						UpdateInterval = ".DBQuote($zoneInfo->{'UpdateInterval'}).",
						LastUpdate = ".DBQuote($zoneInfo->{'LastUpdate'})."
					 WHERE
						ID = ".DBQuote($dynaDNSID)
		);
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			DBRollback();
			return -3;
		}

		DBCommit();
	} else {
		# Not found, prohibit change
		return -4;
	}

	return RES_OK;
}


## @fn canAccessDDNSServerGroup($agentID,$serverGroupID)
# Check if a agent can access a dnshosting serverGroup
#
# @param agentID Agent ID Agent ID making the request
# @param serverGroupID ServerGroup ID ServerGroup ID being accessed
#
# @return 1 on success, 0 on failure
sub canAccessDDNSServerGroup
{
	my ($agentID,$serverGroupID) = @_;
	my $res = 0;


	# Check params
	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($serverGroupID)) {
		setError("Parameter 'serverGroupID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		setError("Parameter 'serverGroupID' is invalid");
		return ERR_PARAM;
	}

# FIX ME !!!!!! check capabilities aswell

	# Limit to dnshosting servers
#	my $serverGroupData;
#	$serverGroupData->{'Capabilities'} = 'DDNS';
	
#	my $res = smlib::servers::canAccessServerGroup($agentID,$serverGroupID);


#XXX: TEMPORARY HACK
	# Query agent
	my $num_results = DBSelectNumResults("
			FROM
				soap_server_group_to_agent
			WHERE
				ServerGroupID = ".DBQuote($serverGroupID)."
				AND AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}


## @fn getDDNSServerGroups($serverGroupInfo,$search)
# INTERNAL FUNCTION CALLED FROM smlib::DDNS ONLY
# Returns list of ddns server groups
#
# @param servrGroupInfo DDNS serverGroup info hash ref
# @li AgentID - Optional agent ID
#
# @param search Search hash ref, @see smlib::getServerGroups
#
# @return Array ref of hash refs
# @li ID - DDNS server group ID
# @li Name - DDNS server group name
sub getDDNSServerGroups
{
	my ($serverGroupInfo,$search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Data to pass to smlib::servers
	my $serverGroupData;

	# Make sure AgentID is ok
	if (defined($serverGroupData->{'AgentID'})) {
		if (!defined($serverGroupData->{'AgentID'} = isNumber($serverGroupInfo->{'AgentID'}))) {
			setError("Parameter 'serverGroupInfo' element 'AgentID' is invalid");
			return ERR_PARAM;
		}
	}

	# Limit to ddns servers
	$serverGroupData->{'Capabilities'} = 'DDNS';

	# Grab results and see if they ok, if not just pass the error through
	my ($results,$num_results) = smlib::servers::getServerGroups($serverGroupData,$search);
	if (ref($results) ne "ARRAY") {
		return $results;
	}

	return ($results,$num_results);
}



1;
# vim: ts=4
