# Radius functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Radius
# Radius handling functions
package smlib::Radius;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::servers;
use smlib::system qw( hashPassword );
use smlib::util;

use Math::BigFloat;
use Math::BigInt;
use POSIX qw(
	strftime
);


# Use the smlib servers autoloader so we can get remote calls working nicely
sub AUTOLOAD { our $AUTOLOAD; return smlib::servers::RPC_AUTOLOADER($AUTOLOAD,@_); }


#
# RADIUS USER FUNCTIONS
#

## @method createRadiusUser($userInfo)
# Add a radius user
#
# @param userInfo User info hash ref
# @li AgentID - Agent ID
# @li Username - Radius username, without the @@realm part
# @li Password - Optional radius user password
# @li ClassID - Radius user class ID
# @li UsageCap - Optional usage cap in Mbyte
# @li NotifyMethod - Optional notification method
# @li AgentDisabled - Optionally set if agent wants to disable this user
# @li AgentRef - Agent reference for this user
#
# @return Radius user ID, or < 0 on error
sub _createRadiusUser
{
	my $userInfo = shift;


	if (!defined($userInfo)) {
		setError("Parameter 'userInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($userInfo)) {
		setError("Parameter 'userInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{'AgentID'})) {
		setError("Parameter 'userInfo' element 'AgentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{'AgentID'} = isNumber($userInfo->{'AgentID'}))) {
		setError("Parameter 'userInfo' element 'AgentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{"Username"})) {
		setError("Parameter 'userInfo' has no element 'Username'");
		return ERR_PARAM;
	}
	if (!isUsername($userInfo->{"Username"})) {
		setError("Parameter 'userInfo' element 'Username' is invalid");
		return ERR_PARAM;
	}
	if ($userInfo->{"Username"} eq "") {
		setError("Parameter 'userInfo' element 'Username' is blank");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{'Password'})) {
		setError("Parameter 'userInfo' has no element 'Password'");
		return ERR_PARAM;
	}
	if (!isVariable($userInfo->{'Password'})) {
		setError("Parameter 'userInfo' element 'Password' is invalid");
		return ERR_PARAM;
	}
	if ($userInfo->{"Password"} eq "") {
		setError("Parameter 'userInfo' element 'Password' is blank");
		return ERR_PARAM;
	}
	if ((my $error = isPasswordInsecure($userInfo->{"Password"},[$userInfo->{'Username'}]))) {
		setError("Parameter 'UserInfo' element 'Password' is INSECURE: $error");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{"ClassID"})) {
		setError("Parameter 'userInfo' has no element 'ClassID'");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{'ClassID'} = isNumber($userInfo->{'ClassID'}))) {
		setError("Parameter 'userInfo' element 'ClassID' is invalid");
		return ERR_PARAM;
	}

	# If we have a usage cap, make sure its valid
	if (defined($userInfo->{"UsageCap"})) {
		# Check usage cap is zero (prepaid)
		if (!defined($userInfo->{'UsageCap'} = isNumber($userInfo->{'UsageCap'},ISNUMBER_ALLOW_ZERO))) {
			setError("Parameter 'userInfo' element 'UsageCap' is invalid");
			return ERR_PARAM;
		}
	}
	
	if (defined($userInfo->{"NotifyMethod"})) {
		if (!isVariable($userInfo->{"NotifyMethod"})) {
			setError("Parameter 'userInfo' element 'NotifyMethod' is invalid");
			return ERR_PARAM;
		}
	}
	
	if (defined($userInfo->{"AgentDisabled"})) {
		if (!defined($userInfo->{'AgentDisabled'} = isBoolean($userInfo->{"AgentDisabled"}))) {
			setError("Parameter 'userInfo' element 'AgentDisabled' has an invalid value");
			return ERR_PARAM;
		}
	}

	if (defined($userInfo->{"AgentRef"})) {
		if (!isVariable($userInfo->{"AgentRef"})) {
			setError("Parameter 'userInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}

	# Pull in classinfo
	my $classInfo = _getRadiusClass($userInfo->{'ClassID'});
	# We already have error set, so return
	if (!isHash($classInfo)) {
		return $classInfo;
	}

	# Make proper username including realm
	my $username .= lc($userInfo->{'Username'}) . "@" . $classInfo->{'Domain'};

	# Check if user exists
	my $num_results = DBSelectNumResults("FROM radiusUsers WHERE Username = ".DBQuote($username));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Radius user '$username' already exists in database");
		return ERR_EXISTS;
	}

	my $extraColumns = "";
	my $extraValues = "";

	# Check if we have extra columns
	if (defined($userInfo->{'Password'})) {
		$extraColumns .= ",Password";
		$extraValues .= ",".DBQuote($userInfo->{'Password'});
	}

	if (defined($userInfo->{'UsageCap'})) {
		$extraColumns .= ",UsageCap";
		$extraValues .= ",".DBQuote($userInfo->{'UsageCap'});
	}

	if (defined($userInfo->{'NotifyMethod'})) {
		$extraColumns .= ",NotifyMethod";
		$extraValues .= ",".DBQuote($userInfo->{'NotifyMethod'});
	}

	if (defined($userInfo->{'AgentRef'})) {
		if ($userInfo->{'AgentRef'} eq " ") {
			$extraColumns .= ",AgentRef";
			$extraValues .= ",NULL";
		} else {
			$extraColumns .= ",AgentRef";
			$extraValues .= ",".DBQuote($userInfo->{'AgentRef'});
		}
	}

	if (defined($userInfo->{'AgentDisabled'})) {
		$extraColumns .= ",AgentDisabled";
		$extraValues .= ",".DBQuote($userInfo->{'AgentDisabled'});
	}

	DBBegin();

	# Insert user data
	my $sth = DBDo("
			INSERT INTO radiusUsers
				(AgentID,Username,RadiusClassID$extraColumns)
			VALUES (".
					DBQuote($userInfo->{'AgentID'}).",".
					DBQuote($username).",".
					DBQuote($classInfo->{'ID'})."
					$extraValues
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	my $ID = DBLastInsertID("radiusUsers","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateRadiusUser($userInfo)
# Update a radius user
#
# @param userInfo User info hash ref
# @li ID - Radius user ID
# @li Password - Optional radius user password
# @li UsageCap - Optional radius usage cap
# @li NotifyMethod - Optional notification method
# @li AgentDisabled - Optional flag agent can set to disable this radius user 
# @li AgentRef - Optional agent reference
#
# @return 0 on success or < 0 on error
sub _updateRadiusUser
{
	my $userInfo = shift;


	# Check params
	if (!defined($userInfo)) {
		setError("Parameter 'userInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($userInfo)) {
		setError("Parameter 'userInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{'ID'})) {
		setError("Parameter 'userInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{'ID'} = isNumber($userInfo->{'ID'}))) {
		setError("Parameter 'userInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}
	if (defined($userInfo->{'Password'})) {
		if (!isVariable($userInfo->{'Password'})) {
			setError("Parameter 'userInfo' element 'Password' is invalid");
			return ERR_PARAM;
		}
		if ($userInfo->{'Password'} eq "") {
			delete($userInfo->{'Password'});
		}
	}
	if (defined($userInfo->{"UsageCap"})) {
		# Check usage cap is zero (prepaid)
		if (!defined($userInfo->{'UsageCap'} = isNumber($userInfo->{'UsageCap'},ISNUMBER_ALLOW_ZERO))) {
			setError("Parameter 'userInfo' element 'UsageCap' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($userInfo->{"NotifyMethod"})) {
		if (!isVariable($userInfo->{"NotifyMethod"})) {
			setError("Parameter 'userInfo' element 'NotifyMethod' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($userInfo->{"AgentDisabled"})) {
		if (!defined($userInfo->{'AgentDisabled'} = isBoolean($userInfo->{"AgentDisabled"}))) {
			setError("Parameter 'userInfo' element 'AgentDisabled' has an invalid value");
			return ERR_PARAM;
		}
	}
	if (defined($userInfo->{"AgentRef"})) {
		if (!isVariable($userInfo->{"AgentRef"})) {
			setError("Parameter 'userInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}

	# Grab ourselves, so we can check a few things below
	my $radiusUser = _getRadiusUser($userInfo->{'ID'});
	if (!isHash($radiusUser)) {
		return $radiusUser;
	}

	# We do this here cause we don't have user info above
	if (defined($userInfo->{'Password'})) {
		# Make sure password is secure
		if ((my $error = isPasswordInsecure($userInfo->{'Password'},[ $radiusUser->{'Username'} ]))) {
			setError("Parameter 'userInfo' element 'Password' is INSECURE: $error");
			return ERR_PARAM;
		}
	}

	# Updates we need to do
	my @updates = ();

	if (defined($userInfo->{'Password'})) {
		push(@updates,"Password = ".DBQuote($userInfo->{'Password'}));
	}

	# If we have a usage cap, make sure its valid
	if (defined($userInfo->{"UsageCap"})) {
		push(@updates,"UsageCap = ".DBQuote($userInfo->{'UsageCap'}));
	}

	if (defined($userInfo->{"NotifyMethod"})) {
		push(@updates,"NotifyMethod = ".DBQuote($userInfo->{'NotifyMethod'}));
	}
	
	if (defined($userInfo->{"AgentDisabled"})) {
		push(@updates,"AgentDisabled = ".DBQuote($userInfo->{'AgentDisabled'}));
	}

	if (defined($userInfo->{"AgentRef"})) {
		if ($userInfo->{'AgentRef'} eq " ") {
			push(@updates,"AgentRef = NULL");
		} else {
			push(@updates,"AgentRef = ".DBQuote($userInfo->{'AgentRef'}));
		}
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for radius user");
		return ERR_USAGE;
	}

	# Grab user info
	my $user = _getRadiusUser($userInfo->{'ID'});
	if (!isHash($user)) {
		return $user;
	}

	DBBegin();

	# Update
	my $sth = DBDo("
			UPDATE
				radiusUsers
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($userInfo->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeRadiusUser($userID)
# Remove a radius user
#
# @param userID Radius User ID
#
# @return 0 on success, < 0 on error
sub _removeRadiusUser
{
	my $userID = shift;


	if (!defined($userID)) {
		setError("Parameter 'userID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($userID = isNumber($userID))) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Grab user info
	my $user = _getRadiusUser($userID);
	if (!isHash($user)) {
		return $user;
	}

	DBBegin();

	# Remove port locks from database
	my $sth = DBDo("DELETE FROM radiusPortLocks WHERE RadiusUserID = ".DBQuote($userID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove topups
	$sth = DBDo("DELETE FROM radiusTopups WHERE RadiusUserID = ".DBQuote($userID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove user from database
	$sth = DBDo("DELETE FROM radiusUsers WHERE ID = ".DBQuote($userID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method getRadiusUsers($userInfo,$search)
# Return list of user records for Agent
#
# @param userInfo Radius user info hash ref
# @li AgentID - Optional agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Radius user ID
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li Username - Radius username
# @li UsageCap - Radius usage cap in Mbyte
# @li NotifyMethod - Notification method
# @li AgentRef - Refernce for agent
# @li AgentDisabled - Set if the agent disabled this entry
# @li ClassID - Class ID
# @li ClassDescription - Class description
# @li RealmDescription - Realm description
# @li Disabled - Set if user is disabled
# @li Domain - Radius domain
# @li Service - Radius service
sub _getRadiusUsers 
{
	my ($userInfo,$search) = @_;

	my $extraSQL = "";

	# Check if we have an AgentID defined that its valid
	if (defined($userInfo)) {
		# Make sure we're a hash
		if (!isHash($userInfo)) {
			setError("Parameter 'userInfo' is not a HASH");
			return ERR_PARAM;
		}
		# If we have an agent ID, use it
		if (defined($userInfo->{'AgentID'})) {
			if (!defined($userInfo->{'AgentID'} = isNumber($userInfo->{'AgentID'}))) {
				setError("Parameter 'userInfo' element 'AgentID'' is invalid");
				return ERR_PARAM;
			}
			$extraSQL = "AND radiusUsers.AgentID = ".DBQuote($userInfo->{'AgentID'});
		}
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'radiusUsers.ID',
		'Username' => 'radiusUsers.Username',
		'AgentRef' => 'radiusUsers.AgentRef',
	};

	# Query agent for user list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				radiusUsers.ID,
				radiusUsers.AgentID,
				radiusUsers.Username,
				radiusUsers.UsageCap,
				radiusUsers.NotifyMethod,
				radiusUsers.AgentRef,
				radiusUsers.AgentDisabled,
				radiusClasses.ID AS ClassID,
				radiusClasses.Description AS ClassDescription,
				radiusRealms.Description AS RealmDescription,
				radiusRealms.Domain
			FROM
				radiusUsers, radiusClasses, radiusRealms
			WHERE
				radiusClasses.ID = radiusUsers.RadiusClassID
				AND radiusRealms.ID = radiusClasses.RadiusRealmID
				$extraSQL
		",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @rusers = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID AgentID Username UsageCap NotifyMethod AgentRef AgentDisabled 
						ClassID ClassDescription
						RealmDescription Domain
				)
			)
	) {
		my $ruser;

		$ruser->{'ID'} = $row->{'ID'};
		$ruser->{'AgentID'} = $row->{'AgentID'};
		$ruser->{'Username'} = $row->{'Username'};
		$ruser->{'UsageCap'} = $row->{'UsageCap'};
		$ruser->{'NotifyMethod'} = $row->{'NotifyMethod'};
		$ruser->{'AgentRef'} = $row->{'AgentRef'};
		$ruser->{'AgentDisabled'} = booleanize($row->{'AgentDisabled'});
		$ruser->{'Disabled'} = $ruser->{'AgentDisabled'};
#		$ruser->{'AdminDisabled'} = booleanize($row->{'AdminDisabled'});
#		$ruser->{'Disabled'} = booleanize($ruser->{'AgentDisabled'} || $ruser->{'AdminDisabled'});
		$ruser->{'ClassID'} = $row->{'ClassID'};
		$ruser->{'ClassDescription'} = $row->{'ClassDescription'};
		$ruser->{'RealmDescription'} = $row->{'RealmDescription'};
		$ruser->{'Domain'} = $row->{'Domain'};
		$ruser->{'Service'} = $row->{'RealmDescription'} . " / " . $row->{'ClassDescription'};

		push(@rusers,$ruser);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@rusers,$numResults);
}



## @method getRadiusUser($userID)
# Return radius user record hash
#
# @param userID Radius user ID
#
# @return User info hash ref
# @li ID - Radius user ID
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li Username - Radius username
# @li UsageCap - Radius usage cap in Mbyte
# @li NotifyMethod - Notification method
# @li AgentRef - Refernce for agent
# @li AgentDisabled - Set if the agent disabled this entry
# @li ClassID - Class ID
# @li ClassDescription - Class description
# @li RealmDescription - Realm description
# @li Disabled - Set if user is disabled
# @li Domain - Radius domain
# @li Service - Radius service
sub _getRadiusUser
{
	my $userID = shift;


	if (!defined($userID)) {
		setError("Parameter 'userID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($userID = isNumber($userID))) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Query user
	my $sth = DBSelect("
			SELECT
				radiusUsers.ID,
				radiusUsers.AgentID,
				radiusUsers.Username,
				radiusUsers.UsageCap,
				radiusUsers.NotifyMethod,
				radiusUsers.AgentRef,
				radiusUsers.AgentDisabled,
				radiusClasses.ID AS ClassID,
				radiusClasses.Description AS ClassDescription,
				radiusRealms.Description AS RealmDescription,
				radiusRealms.Domain
			FROM
				radiusUsers, radiusClasses, radiusRealms
			WHERE
				radiusUsers.ID = ".DBQuote($userID)."
				AND radiusClasses.ID = radiusUsers.RadiusClassID
				AND radiusRealms.ID = radiusClasses.RadiusRealmID
		");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows != 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw(
					ID AgentID Username UsageCap NotifyMethod AgentRef AgentDisabled 
					ClassID ClassDescription
					RealmDescription Domain
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'AgentID'} = $row->{'AgentID'};
	$res->{'Username'} = $row->{'Username'};
	$res->{'UsageCap'} = $row->{'UsageCap'};
	$res->{'NotifyMethod'} = $row->{'NotifyMethod'};
	$res->{'AgentRef'} = $row->{'AgentRef'};
	$res->{'AgentDisabled'} = booleanize($row->{'AgentDisabled'});
	$res->{'Disabled'} = $res->{'AgentDisabled'};
	$res->{'ClassID'} = $row->{'ClassID'};
	$res->{'ClassDescription'} = $row->{'ClassDescription'};
	$res->{'RealmDescription'} = $row->{'RealmDescription'};
	$res->{'Domain'} = $row->{'Domain'};
	$res->{'Service'} = $row->{'RealmDescription'} . " / " . $row->{'ClassDescription'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method getRadiusClasses($classInfo,$search)
# Returns list of radius classes
#
# @param classInfo Radius class info hash ref
# @li AgentID - Optional agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Radius class ID
# @li Service - Radius service string
sub _getRadiusClasses
{
	my ($classInfo,$search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Check for agent ID restriction
	my $extraTables = "";
	my $extraSQL = "";
	if (defined($classInfo)) {
		# Make sure we're a hash
		if (!isHash($classInfo)) {
			setError("Parameter 'classInfo' is not a HASH");
			return ERR_PARAM;
		}
# FIXME   OR AgentID = $classInfo->{'AgentID'}   (sub account)
		# If we have an agent ID, use it
		if (defined($classInfo->{'AgentID'})) {
			if (!defined($classInfo->{'AgentID'} = isNumber($classInfo->{'AgentID'}))) {
				setError("Parameter 'classInfo' element 'AgentID'' is invalid");
				return ERR_PARAM;
			}
			$extraTables .= ", radiusClassAgentMap";
			$extraSQL .= "AND radiusClassAgentMap.AgentID = ".DBQuote($classInfo->{'AgentID'});
			$extraSQL .= "AND radiusClasses.ID = radiusClassAgentMap.RadiusClassID";
		}
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'radiusClasses.ID',
		'Description' => 'radiusRealms.Description, radiusClasses.Description'
	};

	# Select mailboxes
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				radiusClasses.ID,
				radiusRealms.Domain AS RadiusDomain,
				radiusClasses.Description AS ClassDesc,
				radiusRealms.Description AS RealmDesc
			FROM
				radiusClasses, radiusRealms $extraTables
			WHERE
				radiusRealms.ID = radiusClasses.RadiusRealmID
				$extraSQL
				",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @classes = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID RadiusDomain ClassDesc RealmDesc ))) {
		my $class;

		$class->{'ID'} = $row->{'ID'};
		$class->{'Service'} = $row->{'RealmDesc'} . " / " . $row->{'ClassDesc'};

		push(@classes,$class);
	}

	DBFreeRes($sth);

	return (\@classes,$numResults);
}



## @method getRadiusUserLogs($userInfo,$search)
# Retrieve radius logs
#
# @param userInfo Radius user info hash ref
# @li UserID - Radius user ID
# @li AgentID - Return only logs for this agent ID
# @li Username - Radius username, can be used instead of UserID & AgentID
# @li Precision - Precision to use in return numbers
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Radius log ID
# @li Username - Radius username
# @li Status - Record status
# @li Timestamp - Record timestamp
# @li AcctSessionID - Session ID
# @li AcctSessionTime - Session time, time the session has been active
# @li NASIPAddress - NAS IP address
# @li NASPortType - NAS port type
# @li NASPort - NAS port
# @li CalledStationID - Called station ID
# @li CallingStationID - Calling station ID, could be caller ID
# @li NASTransmitRate - Transmit rate reported by NAS
# @li NASReceiveRate - Receive rate reported by NAS
# @li FramedIPAddress - IP address of this session
# @li AcctInputOctets - Input bytes
# @li AcctOutputOctets - Output bytes
# @li AcctInputGigawords - Number of times the AcctInputOctets counter overflowed
# @li AcctOutputGigawords - Number of times the AcctOutputOctets counter overflowed
# @li LastAcctUpdate - Last time this record was updated
# @li ConnectTermReason - Connection termination reason
sub _getRadiusUserLogs
{
	my ($userInfo,$search) = @_;


	if (!defined($userInfo)) {
		setError("Parameter 'userInfo' is not defined");
		return ERR_PARAM;
	}
	if (!isHash($userInfo)) {
		setError("Parameter 'userInfo' is not a HASH");
		return ERR_PARAM;
	}

	# Check the two types of options we can have
	if (defined($userInfo->{'Username'})) {

		# Check Username
		if (!defined($userInfo->{'Username'} = isUsername($userInfo->{'Username'},ISUSERNAME_ALLOW_ATSIGN))) {
			setError("Parameter 'userInfo' element 'Username' is invalid");
			return ERR_PARAM;
		}
	} else {

		# Check UserID
		if (!defined($userInfo->{'UserID'})) {
			setError("Parameter 'userInfo' has no element 'UserID'");
			return ERR_PARAM;
		}
		if (!defined($userInfo->{'UserID'} = isNumber($userInfo->{'UserID'}))) {
			setError("Parameter 'userInfo' element 'UserID' is invalid");
			return ERR_PARAM;
		}

		# Grab user info
		my $user = _getRadiusUser($userInfo->{'UserID'});
		if (!isHash($user)) {
			return $user;
		}

		# Assign Username and AgentID
		$userInfo->{'Username'} = $user->{'Username'};
		$userInfo->{'AgentID'} = $user->{'AgentID'};
 	}

	# Check precision and set default if necessary
	if (defined($userInfo->{'Precision'})) {
		if (!defined($userInfo->{'Precision'} = isNumber($userInfo->{'Precision'},ISNUMBER_ALLOW_NEGATIVE))) {
			setError("Parameter 'precision' is invalid");
			return ERR_PARAM;
		}
		$userInfo->{'Precision'} = Math::BigInt->new($userInfo->{'Precision'})->babs()->bmul(-1);
	} else {
		# Default precision to -2
		$userInfo->{'Precision'} = -2;
	}
	# Positive precision,  -1 * -X = X
	my $precisionP = $userInfo->{'Precision'} * -1;

	# Check if we have an AgentID
	my $extraSQL = "";
	if (defined($userInfo->{'AgentID'})) {
		if (!defined($userInfo->{'AgentID'} = isNumber($userInfo->{'AgentID'}))) {
			setError("Parameter 'userInfo' element 'AgentID' is invalid");
			return ERR_PARAM;
		}

		# If we do, use it
		$extraSQL .= "AND AgentID = ".DBQuote($userInfo->{'AgentID'});
 	}

	# Check search params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}


	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'radiusLogs.ID',
		'Username' => 'radiusLogs.Username',
		'Timestamp' => 'radiusLogs.Timestamp',
		'AcctSessionID' => 'radiusLogs.AcctSessionID',
		'AcctSessionTime' => 'radiusLogs.AcctSessionTime',
		'NASIPAddress' => 'radiusLogs.NASIPAddress',
		'NASPortType' => 'radiusLogs.NASPortType',
		'NASPort' => 'radiusLogs.NASPort',
		'CalledStationID' => 'radiusLogs.CalledStationID',
		'CallingStationID' => 'radiusLogs.CallingStationID',
		'NASTransmitRate' => 'radiusLogs.NASTransmitRate',
		'NASReceiveRate' => 'radiusLogs.NASReceiveRate',
		'FramedIPAddress' => 'radiusLogs.FramedIPAddress',
		'LastAcctUpdate' => 'radiusLogs.LastAcctUpdate',
		'ConnectTermReason' => 'radiusLogs.ConnectTermReason'
	};

	# Query agent for user list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				Username, Status, Timestamp,
				AcctSessionID, AcctSessionTime, AcctDelayTime,
				NASIPAddress, NASPortType, NASPort, CalledStationID, CallingStationID,
				NASTransmitRate, NASReceiveRate,
				ConnectInfo, ServiceType, Class,
				FramedIPAddress,
				AcctInputOctets, AcctOutputOctets, AcctInputGigawords, AcctOutputGigawords,
				LastAccUpdate,
				ConnectTermReason
			FROM
				radiusLogs
			WHERE
				Status > 0
				AND Username = ".DBQuote($userInfo->{'Username'})."
				$extraSQL
		",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @rlogs = ();
	while (my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw(
				ID Username Status Timestamp AcctSessionID AcctSessionTime AcctDelayTime NASIPAddress NASPortType
				NASPort CalledStationID CallingStationID NASTransmitRate NASReceiveRate ConnectInfo ServiceType
				Class FramedIPAddress AcctInputOctets AcctOutputOctets AcctInputGigawords AcctOutputGigawords
				LastAcctUpdate ConnectTermReason
			)
		)
	) {
		my $rlog;

		$rlog->{'ID'} = $row->{'ID'};
		$rlog->{'Username'} = $row->{'Username'};
		$rlog->{'Status'} = $row->{'Status'};
		$rlog->{'Timestamp'} = $row->{'Timestamp'};

		$rlog->{'AcctSessionID'} = $row->{'AcctSessionID'};
		$rlog->{'AcctSessionTime'} = $row->{'AcctSessionTime'};

		$rlog->{'NASIPAddress'} = $row->{'NASIPAddress'};
		$rlog->{'NASPortType'} = $row->{'NASPortType'};
		$rlog->{'NASPort'} = $row->{'NASPort'};
		$rlog->{'CalledStationID'} = $row->{'CalledStationID'};
		$rlog->{'CallingStationID'} = $row->{'CallingStationID'};

		$rlog->{'NASTransmitRate'} = $row->{'NASTransmitRate'};
		$rlog->{'NASReceiveRate'} = $row->{'NASReceiveRate'};

		$rlog->{'FramedIPAddress'} = $row->{'FramedIPAddress'};

		$rlog->{'AcctInputOctets'} = defined($row->{'AcctInputOctets'}) ? $row->{'AcctInputOctets'} : 0;
		$rlog->{'AcctOutputOctets'} = defined($row->{'AcctOutputOctets'}) ? $row->{'AcctOutputOctets'} : 0;
		$rlog->{'AcctInputGigawords'} = defined($row->{'AcctInputGigawords'}) ? $row->{'AcctInputGigawords'} : 0;
		$rlog->{'AcctOutputGigawords'} = defined($row->{'AcctOutputGigawords'}) ? $row->{'AcctOutputGigawords'} : 0;

		# Add up gigawords multiplied by 4096
		my $inputMbyte2 = new Math::BigFloat($rlog->{'AcctInputGigawords'});
		$inputMbyte2->bmul(4096);
		# Initialize the octet counter
		my $inputMbyte = new Math::BigFloat(0);
		$inputMbyte->precision($userInfo->{'Precision'});
		# Check if its > 0 & add while dividing by Mbyte
		if ($rlog->{'AcctInputOctets'} > 0) {
			$inputMbyte->badd($rlog->{'AcctInputOctets'});
			$inputMbyte->bdiv(1024)->bdiv(1024);
		}
		$inputMbyte->badd($inputMbyte2);
		# Round to 2 places
		$rlog->{'AcctInputMbyte'} = sprintf('%.'.$precisionP.'f',$inputMbyte->bstr());

		# Add up gigawords multiplied by 4096
		my $outputMbyte2 = new Math::BigFloat($rlog->{'AcctOutputGigawords'});
		$outputMbyte2->bmul(4096);
		# Initialize the octet counter
		my $outputMbyte = new Math::BigFloat(0);
		$outputMbyte->precision($userInfo->{'Precision'});
		# Check if its > 0 & add while dividing by Mbyte
		if ($rlog->{'AcctOutputOctets'} > 0) {
			$outputMbyte->badd($rlog->{'AcctOutputOctets'});
			$outputMbyte->bdiv(1024)->bdiv(1024);
		}
		$outputMbyte->badd($outputMbyte2);
		# Round to 2 places
		$rlog->{'AcctOutputMbyte'} = sprintf('%.'.$precisionP.'f',$outputMbyte->bstr());


		$rlog->{'LastAcctUpdate'} = $row->{'LastAccUpdate'};

		$rlog->{'ConnectTermReasonCode'} = $row->{'ConnectTermReason'};
		# Make the reason a bit more pretty...
		if (defined($rlog->{'ConnectTermReasonCode'})) {
			my $reason = "";

			# Still logged in
			if ($rlog->{'ConnectTermReasonCode'} == 0) {
				$reason = "Still logged in";

			# User request
			} elsif ($rlog->{'ConnectTermReasonCode'} == 1) {
				$reason = "User request";

			# Carrier loss
			} elsif ($rlog->{'ConnectTermReasonCode'} == 2) {
				$reason = "User request";

			# POD?
			} elsif ($rlog->{'ConnectTermReasonCode'} == 5) {
				$reason = "Session timeout";

			# Admin reset
			} elsif ($rlog->{'ConnectTermReasonCode'} == 6) {
				$reason = "Router reset/reboot";

			# Port error
			} elsif ($rlog->{'ConnectTermReasonCode'} == 8) {
				$reason = "Port error";

			# NAS request
			} elsif ($rlog->{'ConnectTermReasonCode'} == 10) {
				$reason = "Router reset/reboot";
			# NAS reboot
			} elsif ($rlog->{'ConnectTermReasonCode'} == 11) {
				$reason = "Router reset/reboot";

			# Unknown
			} elsif ($rlog->{'ConnectTermReasonCode'} == 45) {
				$reason = "Unknown";

			# Unknown
			} elsif ($rlog->{'ConnectTermReasonCode'} == 46) {
				$reason = "Unknown";

			# Unknown
			} elsif ($rlog->{'ConnectTermReasonCode'} == 63) {
				$reason = "Unknown";

			# Unknown
			} elsif ($rlog->{'ConnectTermReasonCode'} == 180) {
				$reason = "Local hangup";

			# NAS request? unknown
			} elsif ($rlog->{'ConnectTermReasonCode'} == 827) {
				$reason = "Service unavailable";

			# NAS request? unknown
			} elsif ($rlog->{'ConnectTermReasonCode'} == 831) {
				$reason = "Router reset/reboot";

			# NAS request? unknown
			} elsif ($rlog->{'ConnectTermReasonCode'} == 841) {
				$reason = "Router reset/reboot";

			# Admin reset
			} elsif ($rlog->{'ConnectTermReasonCode'} == 816) {
				$reason = "Connection dropped";
			}
			$rlog->{'ConnectTermReason'} = $reason;
		}

		push(@rlogs,$rlog);

	}

	# We done with this result set
	DBFreeRes($sth);
	return (\@rlogs,$numResults);
}



## @method getRadiusUserLogsSummary($logsInfo)
# Return radius user logs summary record hash
#
# @param logsInfo Radius user logs info hash
# @li UserID - Radius user ID
# @li AgentID - Agent ID
# @li Username - Radius user ID
# @li Period - Optional period, returns current period if omitted
#
# @return logs info hash ref
# @li UsageTotal - Radius usage for selected period
# @li LastFramedIPAddress - Last connected IP,  - if none
sub _getRadiusUserLogsSummary
{
	my $logsInfo = shift;


	# Check params
	if (!defined($logsInfo)) {
		setError("Parameter 'logsInfo' is not defined");
		return ERR_PARAM;
	}
	if (!isHash($logsInfo)) {
		setError("Parameter 'logsInfo' is not a HASH");
		return ERR_PARAM;
	}

	# Check the two types of options we can have
	if (defined($logsInfo->{'Username'})) {

		# Check Username
		if (!defined($logsInfo->{'Username'} = isUsername($logsInfo->{'Username'},ISUSERNAME_ALLOW_ATSIGN))) {
			setError("Parameter 'logsInfo' element 'Username' is invalid");
			return ERR_PARAM;
		}
	} else {

		# Check UserID
		if (!defined($logsInfo->{'UserID'})) {
			setError("Parameter 'logsInfo' has no element 'UserID'");
			return ERR_PARAM;
		}
		if (!defined($logsInfo->{'UserID'} = isNumber($logsInfo->{'UserID'}))) {
			setError("Parameter 'logsInfo' element 'UserID' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($logsInfo->{'AgentID'})) {
		if (!defined($logsInfo->{'AgentID'} = isNumber($logsInfo->{'AgentID'}))) {
			setError("Parameter 'userInfo' element 'AgentID' is invalid");
			return ERR_PARAM;
		}
	}

	# Set periodYear and periodMonth from period
	my $periodBase;
	if (defined($logsInfo->{'Period'})) {
		my ($periodYear,$periodMonth) = isDate($logsInfo->{'Period'},ISDATE_YEAR | ISDATE_MONTH);
		# If periodYear is not defined, then we failed
		if (!defined($periodYear)) {
			setError("Parameter 'logsInfo' element 'Period' is invalid");
			return ERR_PARAM;
		}
		# Setup period base
		$periodBase = DateTime->new(
			year => $periodYear,
			month => $periodMonth,
			day => 1
		);
	} else {
		# Setup period base, today, but with the day as 1
		$periodBase = DateTime->now();
		$periodBase->set_day(1);
	}

	# Build search criteria
	my $search;
	@{$search->{'Filter'}} = (
		{
			'data' => {
				'comparison' => 'gt',
				'value' => $periodBase->ymd(),
				'type' => 'date'
			},
			'field' => 'Timestamp'
		},
		{
			'data' => {
				'comparison' => 'lt',
				'value' => $periodBase->add( months => 1 )->ymd(),
				'type' => 'date'
			},
			'field' => 'Timestamp'
		}
	);
	$search->{'Sort'} = "ID";
	$search->{'SortDirection'} = "ASC";

	# Send only the info we need
	my $i;
	$i->{'UserID'} = $logsInfo->{'UserID'} if (defined($logsInfo->{'UserID'}));
	$i->{'Username'} = $logsInfo->{'Username'} if (defined($logsInfo->{'Username'}));
	$i->{'Precision'} = -6;

	# Grab data
	my ($rawData,$numResults) = _getRadiusUserLogs($i,$search);
	if (ref($rawData) ne "ARRAY") {
		smlib::logging::log(LOG_ERR,getError());
		setError("Internal error");
		return ERR_UNKNOWN;
	}

	my $result;
	my $usage = Math::BigFloat->new(0);
	foreach my $log (@{$rawData}) {

		my $item;
		$item->{'AcctOutputMbyte'} = defined($log->{'AcctOutputMbyte'}) ? $log->{'AcctOutputMbyte'} : 0;
		$item->{'AcctInputMbyte'} = defined($log->{'AcctInputMbyte'}) ? $log->{'AcctInputMbyte'} : 0;

		# Add to our usage
		$usage->badd($item->{'AcctOutputMbyte'});
		$usage->badd($item->{'AcctInputMbyte'});

		# Check status and override last IP
		$item->{'Status'} = $log->{'Status'};
		if ($item->{'Status'} == 2) {
			$result->{'LastFramedIPAddress'} = undef;
		} else {
			# Set LastIP, this will be reset to "" if it's not the last open session
			$result->{'LastFramedIPAddress'} = $log->{'FramedIPAddress'};
		}
	}
	$result->{'LastFramedIPAddress'} = "-" if (!defined($result->{'LastFramedIPAddress'}));

	$usage->precision(-2);
	$result->{'UsageTotal'} = sprintf('%.2f',$usage->bstr());

	return $result;
}


## @fn resolveRadiusUsername($userID)
# Resolve the UserID to Radius Username
#
# @param userID UserID of radius user
#
# @return Username or undef on error
sub resolveRadiusUsername
{
	my $userID = shift;

	# Check ID
	if (!defined($userID)) {
		setError("Parameter 'userID' is not defined");
		return undef;
	}
	if (!defined($userID = isNumber($userID))) {
		setError("Parameter 'userID' is invalid");
		return undef;
	}

	# Select ID
	my $sth = DBSelect("
			SELECT 
				Username
			FROM 
				radiusUsers
			WHERE
				ID = ".DBQuote($userID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return undef;
	}

	# Check if we got results
	if ($sth->rows != 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return undef;
	}

	# Fetch ID
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( Username ));
	DBFreeRes($sth);

	return $row->{'Username'};
}



## @method getRadiusClass($classID)
# Return radius class
#
# @param classID Radius class ID
#
# @return Hash ref with below items
# @li ID - Radius class ID
# @li Desc - Class description
# @li Domain - Radius realm/domain
# @li RealmDesc - Radius realm description
# @li ClassDesc - Radius class description
# @li Service - Service string, combination of realm & class description
sub _getRadiusClass
{
	my $classID = shift;


	if (!defined($classID)) {
		setError("Parameter 'classID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($classID = isNumber($classID))) {
		setError("Parameter 'classID' is invalid");
		return ERR_PARAM;
	}

	# Query user
	my $sth = DBSelect("
			SELECT
				radiusClasses.ID, radiusClasses.Description AS ClassDesc,
				radiusRealms.Domain AS RadiusDomain, radiusRealms.Description AS RealmDesc
			FROM
				radiusClasses, radiusRealms
			WHERE
				radiusClasses.ID = ".DBQuote($classID)."
				AND radiusRealms.ID = radiusClasses.RadiusRealmID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows != 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID ClassDesc RadiusDomain RealmDesc ));
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Desc'} = $row->{'ClassDesc'};
	$res->{'Domain'} = $row->{'RadiusDomain'};
	$res->{'RealmDesc'} = $row->{'RealmDesc'};
	$res->{'ClassDesc'} = $row->{'ClassDesc'};
	$res->{'Service'} = sprintf('%s / %s',$row->{'RealmDesc'},$row->{'ClassDesc'});

	# We done with this result set
	DBFreeRes($sth);
	return $res;
}



## @fn canAccessRadiusUser($agentID,$userID)
# Check if a agent can access a radius user
#
# @param agentID Agent ID
# @param userID Radius user ID
#
# @return 1 on success, 0 on failure
sub canAccessRadiusUser
{
	my ($agentID,$userID) = @_;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($userID)) {
		setError("Parameter 'userID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($userID = isNumber($userID))) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Query user
	my $num_results = DBSelectNumResults("
			FROM
				radiusUsers
			WHERE
				radiusUsers.ID = ".DBQuote($userID)."
				AND radiusUsers.AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}



## @fn canAccessRadiusClass($agentID,$classID)
# Check if a agent can access a radius class
#
# @param agentID Agent ID Agent ID making the request
# @param classID Class ID Class ID being accessed
#
# @return 1 on success, 0 on failure
sub canAccessRadiusClass
{
	my ($agentID,$classID) = @_;
	my $res = 0;


	# Check params
	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($classID)) {
		setError("Parameter 'classID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($classID = isNumber($classID))) {
		setError("Parameter 'classID' is invalid");
		return ERR_PARAM;
	}

	# Query user
	my $num_results = DBSelectNumResults("
			FROM
				radiusClassAgentMap
			WHERE
				radiusClassAgentMap.RadiusClassID = ".DBQuote($classID)."
				AND radiusClassAgentMap.AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}



## @fn getRadiusServerGroups($serverGroupInfo,$search)
# INTERNAL FUNCTION CALLED FROM smlib::Radius ONLY
# Returns list of radius server groups
#
# @param servrGroupInfo Radius serverGroup info hash ref
# @li AgentID - Optional agent ID
#
# @param search Search hash ref, @see smlib::getServerGroups
#
# @return Array ref of hash refs
# @li ID - Radius server group ID
# @li Name - Radius server group name
sub getRadiusServerGroups
{
	my ($serverGroupInfo,$search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Data to pass to smlib::servers
	my $serverGroupData;

	# Make sure AgentID is ok
	if (defined($serverGroupData->{'AgentID'})) {
		if (!defined($serverGroupData->{'AgentID'} = isNumber($serverGroupInfo->{'AgentID'}))) {
			setError("Parameter 'serverGroupInfo' element 'AgentID' is invalid");
			return ERR_PARAM;
		}
	}

	# Limit to radius servers
	$serverGroupData->{'Capabilities'} = 'Radius';

	# Grab results and see if they ok, if not just pass the error through
	my ($results,$num_results) = smlib::servers::getServerGroups($serverGroupData,$search);
	if (ref($results) ne "ARRAY") {
		return $results;
	}

	return ($results,$num_results);
}



## @fn invalidateRadiusUserNotifyTracking($userInfo)
# Remove user notify tracking info
#
# @param userInfo - Radius user hash
# @li UserID - Radius user ID
# @li Username - Radius Username
#
# @return 0 on success, < 0 on error
sub _invalidateRadiusUserNotifyTracking
{
	my $userInfo = shift;


	if (!defined($userInfo)) {
		setError("Parameter 'userInfo' is not defined");
		return ERR_PARAM;
	}
	if (!isHash($userInfo)) {
		setError("Parameter 'userInfo' is not a HASH");
		return ERR_PARAM;
	}

	# Fetch Username
	if (defined($userInfo->{'Username'})) {

		# Check UserID
		if (!defined($userInfo->{'Username'} = isUsername($userInfo->{'Username'},ISUSERNAME_ALLOW_ATSIGN))) {
			setError("Parameter 'userInfo' element 'Username' is invalid");
			return ERR_PARAM;
		}
	} else {

		# Check UserID
		if (!defined($userInfo->{'UserID'})) {
			setError("Parameter 'userInfo' has no element 'UserID'");
			return ERR_PARAM;
		}
		if (!defined($userInfo->{'UserID'} = isNumber($userInfo->{'UserID'}))) {
			setError("Parameter 'userInfo' element 'UserID' is invalid");
			return ERR_PARAM;
		}

		# Grab user info
		my $user = _getRadiusUser($userInfo->{'UserID'});
		if (!isHash($user)) {
			return $user;
		}

		# Assign Username
		$userInfo->{'Username'} = $user->{'Username'};
 	}

	# Remove radius notification tracking
	my $sth = DBDo("
			DELETE FROM
				radiusNotifyTrack
			WHERE
				Username = ".DBQuote($userInfo->{'Username'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return RES_OK;
}



## @fn invalidateRadiusUserLogs($userInfo)
# Mark logs as history
#
# @param userInfo - Radius user info hash
# @li UserID - Radius user ID
# @li Username - Radius Username
#
# @return 0 on success, < 0 on error
sub _invalidateRadiusUserLogs
{
	my $userInfo = shift;


	if (!defined($userInfo)) {
		setError("Parameter 'userInfo' is not defined");
		return ERR_PARAM;
	}
	if (!isHash($userInfo)) {
		setError("Parameter 'userInfo' is not a HASH");
		return ERR_PARAM;
	}

	# Fetch Username
	if (defined($userInfo->{'Username'})) {

		# Check UserID
		if (!defined($userInfo->{'Username'} = isUsername($userInfo->{'Username'},ISUSERNAME_ALLOW_ATSIGN))) {
			setError("Parameter 'userInfo' element 'Username' is invalid");
			return ERR_PARAM;
		}
	} else {

		# Check UserID
		if (!defined($userInfo->{'UserID'})) {
			setError("Parameter 'userInfo' has no element 'UserID'");
			return ERR_PARAM;
		}
		if (!defined($userInfo->{'UserID'} = isNumber($userInfo->{'UserID'}))) {
			setError("Parameter 'userInfo' element 'UserID' is invalid");
			return ERR_PARAM;
		}

		# Grab user info
		my $user = _getRadiusUser($userInfo->{'UserID'});
		if (!isHash($user)) {
			return $user;
		}

		# Assign Username
		$userInfo->{'Username'} = $user->{'Username'};
 	}

	# Query to invalidate logs
	my $sth = DBDo("
			UPDATE
				radiusLogs
			SET
				Status = -1
			WHERE
				Username = ".DBQuote($userInfo->{'Username'})."
				AND Status > 0
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return RES_OK;
}



## @fn Radius_Auth($username,$password)
# Function to authenticate a radius user
#
# @param username Username (radius username in this case)
# @param password Password
#
# @return > 0 with radius user ID, or < 0 with error
sub _Radius_Auth
{
	my $userInfo = shift;

	if (!defined($userInfo)) {
		setError("Parameter 'userInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($userInfo)) {
		setError("Parameter 'userInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{"Username"})) {
		setError("Parameter 'userInfo' has no element 'Username'");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{'Username'} = isUsername($userInfo->{'Username'},ISUSERNAME_ALLOW_ATSIGN))) {
		setError("Parameter 'userInfo' element 'Username' is invalid");
		return ERR_PARAM;
	}
	if ($userInfo->{"Username"} eq "") {
		setError("Parameter 'userInfo' element 'Username' is blank");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{"Password"})) {
		setError("Parameter 'userInfo' has no element 'Password'");
		return ERR_PARAM;
	}
	if (!isVariable($userInfo->{"Password"})) {
		setError("Parameter 'userInfo' element 'Password' is invalid");
		return ERR_PARAM;
	}
	if ($userInfo->{"Password"} eq "") {
		setError("Parameter 'userInfo' element 'Password' is blank");
		return ERR_PARAM;
	}


	# Select user details from database
	my $sth = DBSelect("
			SELECT
				ID, Password
			FROM
				radiusUsers
			WHERE
				Username = ".DBQuote($userInfo->{'Username'})."
				AND AgentDisabled = 0
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# If we have an invalid number of rows, return negative response
	if ($sth->rows != 1) {
		DBFreeRes($sth);
		setError("Authentication failed");
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID Password ));

	# Lazy hack
	my $cpass = $userInfo->{'Password'};
	my $dpass = $row->{'Password'};

	# Compare
	my $res = RES_ERROR;
	if ($cpass eq $dpass) {
		$res = $row->{'ID'};
	} else {
		setError("Authentication failed");
	}

	DBFreeRes($sth);

	return $res;
}




1;
# vim: ts=4
