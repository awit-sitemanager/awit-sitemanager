# Radius port locking functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Radius::PortLocking
# Radius handling functions
package smlib::Radius::PortLocking;


use strict;
use warnings;

use smlib::config;
use smlib::constants;
use smlib::logging;
use awitpt::db::dblayer;
use smlib::util;


# Use the smlib servers autoloader so we can get remote calls working nicely
sub AUTOLOAD { our $AUTOLOAD; return smlib::servers::RPC_AUTOLOADER($AUTOLOAD,@_); }


#
# RADIUS PORT LOCKING FUNCTIONS
#

## @method createRadiusUserPortLock($portLockInfo)
# Add a radius port lock
#
# @param portLockInfo Port lock info Hash ref 
# @li UserID - Radius user ID
# @li NASPort - NAS port
# @li AgentRef - Optional agent reference
# @li AgentDisabled - Optional agent disabled flag
#
# @return Returns radius user port lock ID
sub _createRadiusUserPortLock
{
	my $portLockInfo = shift;


	if (!defined($portLockInfo)) {
		setError("Parameter 'portLockInfo' is not defined");
		return ERR_PARAM;
	}
	if (!isHash($portLockInfo)) {
		setError("Parameter 'portLockInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($portLockInfo->{'UserID'})) {
		setError("Parameter 'portLockInfo' element 'UserID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($portLockInfo->{'UserID'} = isNumber($portLockInfo->{'UserID'}))) {
		setError("Parameter 'portLockInfo' element 'UserID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($portLockInfo->{'NASPort'})) {
		setError("Parameter 'portLockInfo' element 'NASPort' is not defined");
		return ERR_PARAM;
	}
	if (!isVariable($portLockInfo->{"NASPort"})) {
		setError("Parameter 'portLockInfo' element 'NASPort' is invalid");
		return ERR_PARAM;
	}
	if ($portLockInfo->{'NASPort'} eq "") {
		setError("Parameter 'portLockInfo' element 'NASPort' is invalid");
		return ERR_PARAM;
	}
	if (defined($portLockInfo->{'AgentRef'})) {
		if (!isVariable($portLockInfo->{"AgentRef"})) {
			setError("Parameter 'portLockInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($portLockInfo->{"AgentDisabled"})) {
		# Check for non-variable
		if (!isVariable($portLockInfo->{'AgentDisabled'})) {
			setError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
		if (!defined($portLockInfo->{'AgentDisabled'} = isBoolean($portLockInfo->{"AgentDisabled"}))) {
			setError("Parameter 'portLockInfo' element 'AgentDisabled' has an invalid value");
			return ERR_PARAM;
		}
	}

	# Prepare query to see if the port lock exists
	my $num_results = DBSelectNumResults("
			FROM
				radiusPortLocks
			WHERE
				RadiusUserID = ".DBQuote($portLockInfo->{'UserID'})."
				AND NASPort = ".DBQuote($portLockInfo->{'NASPort'})
	);
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Port lock '".$portLockInfo->{'NASPort'}."' already exists in database");
		return ERR_EXISTS;
	}

	my $extraColumns = "";
	my $extraValues = "";

	# Check if we have extra columns
	if (defined($portLockInfo->{'AgentRef'})) {
		if ($portLockInfo->{'AgentRef'} eq " ") {
			$extraColumns .= ",AgentRef";
			$extraValues .= ",NULL";
		} else {
			$extraColumns .= ",AgentRef";
			$extraValues .= ",".DBQuote($portLockInfo->{'AgentRef'});
		}
	}

	if (defined($portLockInfo->{'AgentDisabled'})) {
		$extraColumns .= ",AgentDisabled";
		$extraValues .= ",".DBQuote($portLockInfo->{'AgentDisabled'});
	}

	DBBegin();

	# Insert user data
	my $sth = DBDo("
			INSERT INTO radiusPortLocks
				(
					RadiusUserID,
					NASPort
					$extraColumns
				)
			VALUES (".
					DBQuote($portLockInfo->{'UserID'}).",".
					DBQuote($portLockInfo->{'NASPort'})."
					$extraValues
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID("radiusPortLocks","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateRadiusUserPortLock($portLockInfo)
# Update radius user port lock
#
# @param portLockInfo Port lock info hash ref
# @li UserID - Radius user ID
# @li NASPort - NAS port
# @li AgentRef - Optional agent reference
# @li AgentDisabled - Optional agent disabled flag
#
# @return 0 on success or < 0 on error
sub _updateRadiusUserPortLock
{
	my $portLockInfo = shift;


	# Check params
	if (!defined($portLockInfo)) {
		setError("Parameter 'portLockInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($portLockInfo)) {
		setError("Parameter 'portLockInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($portLockInfo->{'ID'})) {
		setError("Parameter 'portLockInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($portLockInfo->{'ID'} = isNumber($portLockInfo->{'ID'}))) {
		setError("Parameter 'portLockInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}
	if (defined($portLockInfo->{'UserID'})) {
		if (!defined($portLockInfo->{'UserID'} = isNumber($portLockInfo->{'UserID'}))) {
			setError("Parameter 'portLockInfo' element 'UserID' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($portLockInfo->{'NASPort'})) {
		if (!isVariable($portLockInfo->{"NASPort"})) {
			setError("Parameter 'portLockInfo' element 'NASPort' is invalid");
			return ERR_PARAM;
		}
		if ($portLockInfo->{"NASPort"} eq "") {
			delete($portLockInfo->{"NASPort"});
		}
	}
	if (defined($portLockInfo->{'AgentRef'})) {
		if (!isVariable($portLockInfo->{"AgentRef"})) {
			setError("Parameter 'portLockInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($portLockInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($portLockInfo->{'AgentDisabled'})) {
			setError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
		if (!defined($portLockInfo->{'AgentDisabled'} = isBoolean($portLockInfo->{"AgentDisabled"}))) {
			setError("Parameter 'portLockInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
	}

	# Updates we need to do
	my @updates = ();

	if (defined($portLockInfo->{'UserID'})) {
		push(@updates,"UserID = ".DBQuote($portLockInfo->{'UserID'}));
	}

	if (defined($portLockInfo->{'NASPort'}) && $portLockInfo->{'NASPort'} ne "") {
		push(@updates,"NASPort = ".DBQuote($portLockInfo->{'NASPort'}));
	}

	if (defined($portLockInfo->{'AgentRef'})) {
		if ($portLockInfo->{'AgentRef'} eq " ") {
			push(@updates,"AgentRef = NULL");
		} else {
			push(@updates,"AgentRef = ".DBQuote($portLockInfo->{'AgentRef'}));
		}
	}

	if (defined($portLockInfo->{'AgentDisabled'})) {
		push(@updates,"AgentDisabled = ".DBQuote($portLockInfo->{'AgentDisabled'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);
	if ($updateStr eq "") {
		setError("No updates specified for port lock");
		return ERR_USAGE;
	}

	my $portLock = _getRadiusUserPortLock($portLockInfo->{'ID'});
	if (!isHash($portLock)) {
		return $portLock;
	}

	DBBegin();

	# Update
	my $sth = DBDo("
			UPDATE
				radiusPortLocks
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($portLockInfo->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeRadiusUserPortLock($portLockID)
# Remove a radius user port lock
#
# @param portLockID Port lock ID
sub _removeRadiusUserPortLock
{
	my $portLockID = shift;


	# Check params
	if (!defined($portLockID)) {
		setError("Parameter 'portLockID' not defined");
		return ERR_PARAM;
	}
	if (!defined($portLockID = isNumber($portLockID))) {
		setError("Parameter 'portLockID' is invalid");
		return ERR_PARAM;
	}

	# Check if port lock exists
	my $num_results = DBSelectNumResults("FROM radiusPortLocks WHERE ID = ".DBQuote($portLockID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("Port lock ID '$portLockID' does not exist");
		return ERR_EXISTS;
	}

	my $portLock = _getRadiusUserPortLock($portLockID);
	if (!isHash($portLock)) {
		return $portLock;
	}

	DBBegin();

	# Remove port lock from database
	my $sth = DBDo("DELETE FROM radiusPortLocks WHERE ID = ".DBQuote($portLockID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method getRadiusUserPortLocks($portLockInfo,$search)
# Return radius user port locks
#
# @param portLockInfo Port lock info hash ref
# @li UserID - Radius user ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Port lock ID
# @li NASPort - NAS prot
# @li AgentDisabled - Set if agent has disabled this port
# @li AgentRef - Reference for agent
sub _getRadiusUserPortLocks
{
	my ($portLockInfo,$search) = @_;


	my $extraSQL = "";

	if (defined($portLockInfo)) {
		# Make sure we're a hash
		if (!isHash($portLockInfo)) {
			setError("Parameter 'portLockInfo' is not a HASH");
			return ERR_PARAM;
		}
		# If we have an user ID, use it
		if (defined($portLockInfo->{'UserID'})) {
			if (!defined($portLockInfo->{'UserID'} = isNumber($portLockInfo->{'UserID'}))) {
				setError("Parameter 'portLockInfo' element 'UserID'' is invalid");
				return ERR_PARAM;
			}
			$extraSQL .= "AND RadiusUserID = ".DBQuote($portLockInfo->{'UserID'});
		}
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'NASPort' => 'NASPort',
		'AgentRef' => 'AgentRef',
	};

	# Select port locks
	my ($sth,$numResults) = DBSelectSearch("
		SELECT
			ID, NASPort, AgentDisabled, AgentRef
		FROM
			radiusPortLocks
		WHERE
			1 = 1
			$extraSQL
	");
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @portLocks = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID NASPort AgentDisabled AgentRef ))) {
		my $entry;

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'NASPort'} = $row->{'NASPort'};
		$entry->{'AgentDisabled'} = booleanize($row->{'AgentDisabled'});
		$entry->{'AgentRef'} = $row->{'AgentRef'};
		$entry->{'Disabled'} = $row->{'AgentDisabled'};

		push(@portLocks,$entry);
	}

	DBFreeRes($sth);

	return (\@portLocks,$numResults);
}



## @method getRadiusUserPortLock($portLockID)
# Return port lock record
#
# @param portLockID Port lock ID
#
# @return Hash ref
# @li ID - Radius port lock ID
# @li NASPort - NAS prot
# @li AgentDisabled - Set if agent has disabled this port
# @li AgentRef - Reference for agent
sub _getRadiusUserPortLock
{
	my $portLockID = shift;


	if (!defined($portLockID)) {
		setError("Parameter 'portLockID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($portLockID = isNumber($portLockID))) {
		setError("Parameter 'portLockID' is invalid");
		return ERR_PARAM;
	}

	# Query radius user port lock
	my $sth = DBSelect("
			SELECT
				ID, NASPort, AgentDisabled, AgentRef
			FROM
				radiusPortLocks
			WHERE
				radiusPortLocks.ID = ".DBQuote($portLockID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("Failed to find port lock ID '$portLockID'");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID NASPort AgentDisabled AgentRef ));
	DBFreeRes($sth);

	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'NASPort'} = $row->{'NASPort'};
	$res->{'AgentDisabled'} = booleanize($row->{'AgentDisabled'});
	$res->{'AgentRef'} = $row->{'AgentRef'};
	$res->{'Disabled'} = $row->{'AgentDisabled'};

	return $res;
}



## @fn canAccessRadiusUserPortLock($agentID,$radiusUserPortLockID)
# Check if a agent can access a radius user port lock
#
# @param agentID Agent ID
# @param radiusUserPortLockID Radius user ID
#
# @return 1 on success, 0 on failure
sub canAccessRadiusUserPortLock
{
	my ($agentID,$radiusUserPortLockID) = @_;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($radiusUserPortLockID)) {
		setError("Parameter 'radiusUserPortLockID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($radiusUserPortLockID = isNumber($radiusUserPortLockID))) {
		setError("Parameter 'radiusUserPortLockID' is invalid");
		return ERR_PARAM;
	}

	# Query user
	my $num_results = DBSelectNumResults("
			FROM
				radiusUserPortLocks,
				radiusUsers
			WHERE
				radiusUserPortLocks.ID = ".DBQuote($radiusUserPortLockID)."
				AND radiusUsers.ID = radiusUserPortLocks.radiusUserID
				AND radiusUsers.AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}



## @fn getRadiusUserIDFromPortLockID($portLockID)
# Return radius user ID from port lock ID
#
# @param portLockID Radius user port lock ID
#
# @return User ID on success, or < 0 on error
sub _getRadiusUserIDFromPortLockID
{
	my $portLockID = shift;


	if (!defined($portLockID)) {
		setError("Parameter 'portLockID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($portLockID = isNumber($portLockID))) {
		setError("Parameter 'portLockID' is invalid");
		return ERR_PARAM;
	}

	# Query user
	my $sth = DBSelect("
			SELECT
				RadiusUserID
			FROM
				radiusPortLocks
			WHERE
				ID = ".DBQuote($portLockID)."
	");
	if (!$sth) {
		smlib::logging::log(LOG_ERR,awit::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows != 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw( RadiusUserID )
	);

	my $res;
	$res = $row->{'RadiusUserID'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}




1;
# vim: ts=4
