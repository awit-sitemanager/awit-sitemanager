# Radius topup functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Radius::Topups
# Radius handling functions
package smlib::Radius::Topups;


use strict;
use warnings;

use smlib::config;
use smlib::constants;
use smlib::logging;
use awitpt::db::dblayer;
use smlib::util;


# Use the smlib servers autoloader so we can get remote calls working nicely
sub AUTOLOAD { our $AUTOLOAD; return smlib::servers::RPC_AUTOLOADER($AUTOLOAD,@_); }



#
# RADIUS TOPUP FUNCTIONS
#

## @method createRadiusUserTopup($topupInfo)
# Create a radius topup
#
# @param topupInfo Topup info hash ref
# @li UserID - Radius user ID
# @li Bandwidth - Topup bandwidth in Mbyte
# @li ValidFrom - Valid from date in format of YYYY-MM-DD
# @li ValidTo - Valid to date in format of YYYY-MM-DD
# @li AgentRef - Optional agent reference
#
# @return Returns radius user port lock ID
sub _createRadiusUserTopup
{
	my $topupInfo = shift;


	if (!defined($topupInfo)) {
		setError("Parameter 'topupInfo' is not defined");
		return ERR_PARAM;
	}
	if (!isHash($topupInfo)) {
		setError("Parameter 'topupInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($topupInfo->{'UserID'})) {
		setError("Parameter 'topupInfo' element 'UserID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($topupInfo->{'UserID'} = isNumber($topupInfo->{'UserID'}))) {
		setError("Parameter 'topupInfo' element 'UserID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($topupInfo->{'Bandwidth'})) {
		setError("Parameter 'topupInfo' element 'Bandwidth' is not defined");
		return ERR_PARAM;
	}
	if (!defined($topupInfo->{'Bandwidth'} = isNumber($topupInfo->{'Bandwidth'}))) {
		setError("Parameter 'topupInfo' element 'Bandwidth' is invalid");
		return ERR_PARAM;
	}
	if (!defined($topupInfo->{'ValidFrom'})) {
		setError("Parameter 'topupInfo' element 'ValidFrom' is not defined");
		return ERR_PARAM;
	}
	if (!isVariable($topupInfo->{"ValidFrom"})) {
		setError("Parameter 'topupInfo' element 'ValidFrom' is invalid");
		return ERR_PARAM;
	}
	# FIXME - proper date format check
	if (!($topupInfo->{'ValidFrom'} =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
		setError("Parameter 'topupInfo' element 'ValidFrom' is not an ISO standard date 'YYYY-MM-DD'");
		return ERR_PARAM;
	}
	if (!defined($topupInfo->{'ValidTo'})) {
		setError("Parameter 'topupInfo' element 'ValidTo' is not defined");
		return ERR_PARAM;
	}
	if (!isVariable($topupInfo->{"ValidTo"})) {
		setError("Parameter 'topupInfo' element 'ValidTo' is invalid");
		return ERR_PARAM;
	}
	if (!($topupInfo->{'ValidTo'} =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
		setError("Parameter 'topupInfo' element 'ValidTo' is not an ISO standard date 'YYYY-MM-DD'");
		return ERR_PARAM;
	}
	if (defined($topupInfo->{'AgentRef'})) {
		if (!isVariable($topupInfo->{"AgentRef"})) {
			setError("Parameter 'topupInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}

	my $extraColumns = "";
	my $extraValues = "";

	# Check if we have extra columns
	if (defined($topupInfo->{'AgentRef'})) {
		if ($topupInfo->{'AgentRef'} eq " ") {
			$extraColumns .= ",AgentRef";
			$extraValues .= ",NULL";
		} else {
			$extraColumns .= ",AgentRef";
			$extraValues .= ",".DBQuote($topupInfo->{'AgentRef'});
		}
	}

	DBBegin();

	# Insert data
	my $sth = DBDo("
			INSERT INTO radiusTopups
				(RadiusUserID,Timestamp,Bandwidth,ValidFrom,ValidTo$extraColumns)
			VALUES (".
					DBQuote($topupInfo->{'UserID'}).",".
					"now(),".
					DBQuote($topupInfo->{'Bandwidth'}).",".
					DBQuote($topupInfo->{'ValidFrom'}).",".
					DBQuote($topupInfo->{'ValidTo'})."
					$extraValues
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	my $ID = DBLastInsertID("radiusTopups","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateRadiusUserTopup($topupInfo)
# Update radius topup
#
# @param topupInfo Topup info hash ref 
# @li ID - Topup ID
# @li UserID - Radius user ID
# @li Bandwidth - Optional topup bandwidth in Mbyte
# @li ValidFrom - Optional from date in format of YYYY-MM-DD
# @li ValidTo - Optional to date in format of YYYY-MM-DD
# @li AgentRef - Optional agent reference
#
# @return 0 on success or < 0 on error
sub _updateRadiusUserTopup
{
	my $topupInfo = shift;


	# Check params
	if (!defined($topupInfo)) {
		setError("Parameter 'topupInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($topupInfo)) {
		setError("Parameter 'topupInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($topupInfo->{'ID'})) {
		setError("Parameter 'topupInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($topupInfo->{'ID'} = isNumber($topupInfo->{'ID'}))) {
		setError("Parameter 'topupInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	if (defined($topupInfo->{'UserID'})) {
		if (!defined($topupInfo->{'UserID'} = isNumber($topupInfo->{'UserID'}))) {
			setError("Parameter 'topupInfo' element 'UserID' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($topupInfo->{'Bandwidth'})) {
		if (!defined($topupInfo->{'Bandwidth'} = isNumber($topupInfo->{'Bandwidth'}))) {
			setError("Parameter 'topupInfo' element 'Bandwidth' is invalid");
			return ERR_PARAM;
		}
	}
	# FIXME: proper date check
	if (defined($topupInfo->{'ValidFrom'})) {
		if (!($topupInfo->{'ValidFrom'} =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
			setError("Parameter 'topupInfo' element 'ValidFrom' is not an ISO standard date 'YYYY-MM-DD'");
			return ERR_PARAM;
		}
	}
	if (defined($topupInfo->{'ValidTo'})) {
		if (!($topupInfo->{'ValidTo'} =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
			setError("Parameter 'topupInfo' element 'ValidTo' is not an ISO standard date 'YYYY-MM-DD'");
			return ERR_PARAM;
		}
	}
	if (defined($topupInfo->{'AgentRef'})) {
		if (!isVariable($topupInfo->{"AgentRef"})) {
			setError("Parameter 'topupInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}
	

	# Updates we need to do
	my @updates = ();

	if (defined($topupInfo->{'UserID'})) {
		push(@updates,"UserID = ".DBQuote($topupInfo->{'UserID'}));
	}
	if (defined($topupInfo->{'Bandwidth'})) {
		push(@updates,"Bandwidth = ".DBQuote($topupInfo->{'Bandwidth'}));
	}
	# FIXME: proper date check
	if (defined($topupInfo->{'ValidFrom'})) {
		push(@updates,"ValidFrom = ".DBQuote($topupInfo->{'ValidFrom'}));
	}
	if (defined($topupInfo->{'ValidTo'})) {
		push(@updates,"ValidTo = ".DBQuote($topupInfo->{'ValidTo'}));
	}
	if (defined($topupInfo->{'AgentRef'})) {
		if ($topupInfo->{'AgentRef'} eq " ") {
			push(@updates,"AgentRef = NULL");
		} else {
			push(@updates,"AgentRef = ".DBQuote($topupInfo->{'AgentRef'}));
		}
	}

	# Create and check update string
	my $updateStr = join(",",@updates);
	if ($updateStr eq "") {
		setError("No updates specified for topup");
		return ERR_USAGE;
	}

	# Check radius user topup exists
	my $topup = _getRadiusUserTopup($topupInfo->{'ID'});
	if (!isHash($topup)) {
		return $topup;
	}

	DBBegin();

	# Update
	my $sth = DBDo("
			UPDATE
				radiusTopups
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($topup->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeRadiusUserTopup($topupID)
# Remove a radius user topup
#
# @param topupID Topup ID
#
# @return 0 on success, < 0 on error
sub _removeRadiusUserTopup
{
	my $topupID = shift;


	# Check params
	if (!defined($topupID)) {
		setError("Parameter 'topupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($topupID = isNumber($topupID))) {
		setError("Parameter 'topupID' is invalid");
		return ERR_PARAM;
	}

	# Check if topup exists
	my $num_results = DBSelectNumResults("FROM radiusTopups WHERE id = ".DBQuote($topupID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("Topup ID '$topupID' does not exist");
		return ERR_EXISTS;
	}

	# Check radius user topup exists
	my $topup = _getRadiusUserTopup($topupID);
	if (!isHash($topup)) {
		return $topup;
	}

	DBBegin();

	# Remove topup from database
	my $sth = DBDo("DELETE FROM radiusTopups WHERE ID = ".DBQuote($topupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method getRadiusUserTopups($topupInfo,$search)
# Return an array of topups for a user
#
# @param topupInfo Topup info hash
# @li UserID - Radius user ID
#
# @param search Search criteria
#
# @return Array ref of hash refs
# @li ID - Radius topup ID
# @li UserID - Radius user ID
# @li Bandwidth - Topup bandwidth in Mbyte
# @li Timestamp - Timestamp topup was added
# @li ValidFrom - Date topup is valid from in YYYY-MM-DD
# @li ValidTo - Date topup is valid to in YYYY-MM-DD
# @li AgentRef - Reference for agent
sub _getRadiusUserTopups
{
	my ($topupInfo,$search) = @_;


	my $extraSQL = "";

	if (defined($topupInfo)) {
		# Make sure we're a hash
		if (!isHash($topupInfo)) {
			setError("Parameter 'topupInfo' is not a HASH");
			return ERR_PARAM;
		}
		# If we have an user ID, use it
		if (defined($topupInfo->{'UserID'})) {
			if (!defined($topupInfo->{'UserID'} = isNumber($topupInfo->{'UserID'}))) {
				setError("Parameter 'topupInfo' element 'UserID'' is invalid");
				return ERR_PARAM;
			}
			$extraSQL .= "AND RadiusUserID = ".DBQuote($topupInfo->{'UserID'});
		}
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'UserID' => 'RadiusUserID',
		'Bandwidth' => 'Bandwidth',
		'Timestamp' => 'Timestamp',
		'ValidFrom' => 'ValidFrom',
		'ValidTo' => 'ValidTo',
		'AgentRef' => 'AgentRef',
	};

	# Query agent for user list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID, RadiusUserID, Bandwidth, Timestamp, ValidFrom, ValidTo, AgentRef
			FROM
				radiusTopups
			WHERE
				1 = 1
				$extraSQL
		",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @topups = ();
	while (my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw( ID UserID Bandwidth Timestamp ValidFrom ValidTo AgentRef )
		)
	) {
		my $entry;

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'UserID'} = $row->{'RadiusUserID'};
		$entry->{'Bandwidth'} = $row->{'Bandwidth'};
		$entry->{'Timestamp'} = $row->{'Timestamp'};
		$entry->{'ValidFrom'} = $row->{'ValidFrom'};
		$entry->{'ValidTo'} = $row->{'ValidTo'};
		$entry->{'AgentRef'} = $row->{'AgentRef'};

		push(@topups,$entry);
	}

	DBFreeRes($sth);

	return (\@topups,$numResults);
}


## @method getRadiusUserTopup($topupID)
# Return radius user topup
#
# @param topupID Radius user topup ID
#
# @return Hash ref
# @li ID - Radius topup ID
# @li UserID - Radius user ID
# @li Bandwidth - Topup bandwidth in Mbyte
# @li Timestamp - Timestamp topup was added
# @li ValidFrom - Date topup is valid from in YYYY-MM-DD
# @li ValidTo - Date topup is valid to in YYYY-MM-DD
# @li AgentRef - Reference for agent
sub _getRadiusUserTopup
{
	my $topupID = shift;


	if (!defined($topupID)) {
		setError("Parameter 'topupID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($topupID = isNumber($topupID))) {
		setError("Parameter 'topupID' is invalid");
		return ERR_PARAM;
	}

	# Query radius user topup
	my $sth = DBSelect("
			SELECT
				ID, RadiusUserID, Bandwidth, Timestamp, ValidFrom, ValidTo, AgentRef
			FROM
				radiusTopups
			WHERE
				radiusTopups.ID = ".DBQuote($topupID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("Failed to find radius topup ID '$topupID'");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw( ID RadiusUserID Bandwidth Timestamp ValidFrom ValidTo AgentRef )
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'UserID'} = $row->{'RadiusUserID'};
	$res->{'Bandwidth'} = $row->{'Bandwidth'};
	$res->{'Timestamp'} = $row->{'Timestamp'};
	$res->{'ValidFrom'} = $row->{'ValidFrom'};
	$res->{'ValidTo'} = $row->{'ValidTo'};
	$res->{'AgentRef'} = $row->{'AgentRef'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}


## @method getRadiusUserTopupsSummary($topupsInfo)
# Return radius user topup summary record hash
#
# @param topupsInfo Radius user topups info hash
# @li UserID - Radius user ID
# @li Period - Optional period, returns current period if omitted
#
# @return topup info hash ref
# @li TopupTotal
sub _getRadiusUserTopupsSummary
{
	my $topupsInfo = shift;


	# Check params
	if (!defined($topupsInfo)) {
		setError("Parameter 'topupsInfo' is not defined");
		return ERR_PARAM;
	}
	if (!isHash($topupsInfo)) {
		setError("Parameter 'topupsInfo' is not a HASH");
		return ERR_PARAM;
	}

	# Check UserID
	if (!defined($topupsInfo->{'UserID'})) {
		setError("Parameter 'topupsInfo' has no element 'UserID'");
		return ERR_PARAM;
	}
	if (!defined($topupsInfo->{'UserID'} = isNumber($topupsInfo->{'UserID'}))) {
		setError("Parameter 'topupsInfo' element 'UserID' is invalid");
		return ERR_PARAM;
	}

	# Set periodYear and periodMonth from period
	my $periodBase;
	if (defined($topupsInfo->{'Period'})) {
		my ($periodYear,$periodMonth) = isDate($topupsInfo->{'Period'},ISDATE_YEAR | ISDATE_MONTH);
		# If periodYear is not defined, then we failed
		if (!defined($periodYear)) {
			setError("Parameter 'logsInfo' element 'Period' is invalid");
			return ERR_PARAM;
		}
		# Setup period base
		$periodBase = DateTime->new(
			year => $periodYear,
			month => $periodMonth,
			day => 1
		);
	} else {
		# Setup period base, today, but with the day as 1
		$periodBase = DateTime->now();
		$periodBase->set_day(1);
	}

	# Build search criteria
	my $search;
	@{$search->{'Filter'}} = (
		{
			'data' => {
				'comparison' => 'ge',
				'value' => $periodBase->ymd(),
				'type' => 'date'
			},
			'field' => 'ValidFrom'
		},
		{
			'data' => {
				'comparison' => 'le',
				'value' => $periodBase->add( months => 1 )->ymd(),
				'type' => 'date'
			},
			'field' => 'ValidTo'
		}
	);

	# Send only the info we need
	my $i;
	$i->{'UserID'} = $topupsInfo->{'UserID'};

	# Grab data
	my ($rawData,$numResults) = _getRadiusUserTopups($i,$search);
	if (ref($rawData) ne "ARRAY") {
		smlib::logging::log(LOG_ERR,getError());
		setError("Internal error");
		return ERR_UNKNOWN;
	}

	# Loop through topups and add to our result
	my $topupTotal = Math::BigInt->new(0);
	foreach my $topup (@{$rawData}) {
		# Add this topup amount
		$topupTotal->badd($topup->{'Bandwidth'});
	}

	# Build result
	my $result;
	$result->{'TopupTotal'} = $topupTotal->bstr();

	return $result;
}


## @fn canAccessRadiusUserTopup($agentID,$topupID)
# Check if a agent can access a radius user topup
#
# @param agentID Agent ID
# @param topupID Radius user ID
#
# @return 1 on success, 0 on failure
sub canAccessRadiusUserTopup
{
	my ($agentID,$topupID) = @_;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($topupID)) {
		setError("Parameter 'topupID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($topupID = isNumber($topupID))) {
		setError("Parameter 'topupID' is invalid");
		return ERR_PARAM;
	}

	# Query user
	my $num_results = DBSelectNumResults("
			FROM
				radiusTopups,
				radiusUsers
			WHERE
				radiusTopups.ID = ".DBQuote($topupID)."
				AND radiusUsers.ID = radiusTopups.radiusUserID
				AND radiusUsers.AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}



## @fn getRadiusUserIDFromTopupID($topupID)
# Return radius user ID from topup ID
#
# @param topupID Radius user topup ID
#
# @return User ID on success, or < 0 on error
sub getRadiusUserIDFromTopupID
{
	my $topupID = shift;


	if (!defined($topupID)) {
		setError("Parameter 'topupID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($topupID = isNumber($topupID))) {
		setError("Parameter 'topupID' is invalid");
		return ERR_PARAM;
	}

	# Query user
	my $sth = DBSelect("
			SELECT
				RadiusUserID
			FROM
				radiusTopups
			WHERE
				ID = ".DBQuote($topupID)."
	");
	if (!$sth) {
		smlib::logging::log(LOG_ERR,awit::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows != 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw( RadiusUserID )
	);
	
	my $res;
	$res = $row->{'RadiusUserID'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



1;
# vim: ts=4
