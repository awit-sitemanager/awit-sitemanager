# Site functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::WebHosting
# Website management library
package smlib::WebHosting;


use strict;
use warnings;

use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::system qw(reloadHttpd);
use smlib::util;

use smlib::Apache::Config;

use File::Path qw(mkpath rmtree);
use File::Temp qw(tempfile);


## @method createWebsite($websiteInfo)
# Add a web site
#
# @param websiteInfo Website info hash ref
# @li AgentID - Agent ID
# @li DomainName - Domain name
# @li DomainAliases - Optional, array ref to domain aliases
# @li DiskQuota - Disk quota for website
# @li NoReloadHTPTD - Optional, don't reload httpd
# @li AgentDisabled - Optional, flag to indicate agent has disabled this site (NOT IMPLEMENTED
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
#
# @return Website ID
sub createWebsite
{
	my $websiteInfo = shift;


	if (!defined($websiteInfo)) {
		setError("Parameter 'websiteInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($websiteInfo)) {
		setError("Parameter 'websiteInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($websiteInfo->{'AgentID'})) {
		setError("Parameter 'websiteInfo' element 'AgentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($websiteInfo->{'AgentID'} = isNumber($websiteInfo->{'AgentID'}))) {
		setError("Parameter 'websiteInfo' element 'AgentID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($websiteInfo->{"DomainName"})) {
		setError("Parameter 'websiteInfo' has no element 'DomainName'");
		return ERR_PARAM;
	}
	if (!defined($websiteInfo->{'DomainName'} = isDomain($websiteInfo->{'DomainName'}))) {
		setError("Parameter 'websiteInfo' element 'DomainName' is invalid");
		return ERR_PARAM;
	}
	my $domainName = $websiteInfo->{'DomainName'};

	my @domainAliases = ();
	if (defined($websiteInfo->{'DomainAliases'})) {
		if (ref($websiteInfo->{'DomainAliases'}) ne "ARRAY") {
			setError("Parameter 'websiteInfo' element 'DomainAliases' is not an ARRAY");
			return ERR_PARAM;
		}
		# Chop off blank aliases
		foreach my $alias (@{$websiteInfo->{'DomainAliases'}}) {
			# Fold all whitespaces
			(my $cleanAlias = $alias) =~ s/\s+//g;

			# Skip blank
			next if ($cleanAlias eq "");

			# Verify
			if (!defined($cleanAlias = isDomain($cleanAlias))) {
				setError("Parameter 'websiteInfo' element 'DomainName' is invalid");
				return ERR_PARAM;
			}

			push(@domainAliases,$cleanAlias);
		}
	}

	my $clashRes;
	my @clashDomains = (@domainAliases, $domainName);

	# Check for clashes
	$clashRes = checkSiteClash(\@clashDomains);
	if ($clashRes < 0) {
		smlib::logging::log(Error());
		setError("Internal error in checkSiteClash()");
		return $clashRes;
	} elsif ($clashRes > 0) {
		# FIXME - same as function above, needs fixing
		setError("Parameter 'websiteInfo' element 'DomainAliases' has a member that conflicts with a current website");
		return ERR_CONFLICT;
	}

	$clashRes = checkAliasClash(undef,\@clashDomains);
	if ($clashRes < 0) {
		smlib::logging::log(Error());
		setError("Internal error in checkAliasClash()");
		return $clashRes;
	} elsif ($clashRes > 0) {
		# FIXME - same as function above, needs fixing
		setError("Parameter 'websiteInfo' element 'DomainAliases' has a member that conflicts with a current alias");
		return ERR_CONFLICT;
	}

	if (!defined($websiteInfo->{"DiskQuota"})) {
		setError("Parameter 'websiteInfo' has no element 'DiskQuota'");
		return ERR_PARAM;
	}
	if (!defined($websiteInfo->{'DiskQuota'} = isNumber($websiteInfo->{'DiskQuota'}))) {
		setError("Parameter 'websiteInfo' element 'DiskQuota' is invalid");
		return ERR_PARAM;
	}
	my $diskQuota = $websiteInfo->{'DiskQuota'};

	my $noReloadHTTPD = $websiteInfo->{'NoReloadHTTPD'};

	# Select out agents web directory
	my $sth = DBSelect("
			SELECT
				WebDirectory, ConfDirectory
			FROM
				agents
			WHERE
				ID = ".DBQuote($websiteInfo->{'AgentID'})
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results & finish off
	if ($sth->rows < 1) {
		setError("Configuration for agent ID '".$websiteInfo->{'agentID'}."' not found");
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( WebDirectory ConfDirectory ));
	my $webDirectory = $row->{'WebDirectory'};
	my $confDirectory = $row->{'ConfDirectory'};

	DBFreeRes($sth);


	DBBegin();

	# Insert site data into database
	$sth = DBDo("
			INSERT INTO sites
				(AgentID,DomainName,DomainAliases,DiskQuota)
			VALUES (".
					DBQuote($websiteInfo->{'AgentID'}).",".
					DBQuote($domainName).",".
					DBQuote(join(',',@domainAliases)).",".
					DBQuote($diskQuota)."
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	my $ID = DBLastInsertID("sites","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Set some vars...
	my $homeDir = "$webDirectory/$ID";
	my $htdocDir = "$homeDir/htdocs";
	my $httpdConfFile = "$confDirectory/$ID.conf";
	my $siteUID = $ID + 10000;
	my $siteGID = 10000;

	# Update homedir as we only know the ID now
	$sth = DBDo("
			UPDATE sites
			SET
				HomeDirectory = ".DBQuote($homeDir).",
				HttpdConfFile = ".DBQuote($httpdConfFile).",
				SiteUID = ".DBQuote($siteUID).",
				SiteGID = ".DBQuote($siteGID)."
			WHERE
				ID = ".DBQuote($ID)
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Create htdoc path, set owner & group, set perms
	mkpath($htdocDir,0,0755);
	chown($siteUID,$siteGID,$htdocDir);
	chmod(0755,$htdocDir);

	# Create config directory
	mkpath($confDirectory,0,0755);

	# Generate http config
	my $httpdConf = smlib::Apache::Config->new();

	my $confRoot = $httpdConf->create();

	my $confVhost = $confRoot->addSection("VirtualHost","*:80");
	$confVhost->addDirective("ServerName",$domainName);
	$confVhost->addDirective("ServerAlias",@domainAliases);
	$confVhost->addDirective("DocumentRoot",$htdocDir);
	$confVhost->addDirective("php_admin_value","open_basedir /usr/share/php:/var/www/mysite:/var/run/httpd/phptemp:$homeDir");

	# Write out config file
	if ($httpdConf->write($httpdConfFile) != RES_OK) {
# FIXME - add error here
		smlib::logging::log(LOG_ERR,smlib::Apache::Config::Error());
		setError("Configuration file error");
		DBRollback();
		return ERR_UNKNOWN;
	}

	DBCommit();

	# Reload config
	reloadHttpd() if ($noReloadHTTPD && $noReloadHTTPD ne "");

	return $ID;
}




## @method removeWebsite($websiteID)
# Remove a site
#
# @param websiteID Website ID
#
# @return 0 on success, < 0 on error
sub removeWebsite
{
	my $websiteID = shift;


	if (!defined($websiteID)) {
		setError("Parameter 'websiteID' not defined");
		return ERR_PARAM;
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		setError("Parameter 'websiteID' is invalid");
		return ERR_PARAM;
	}

	my $website = getWebsite($websiteID);
	if (!isHash($website)) {
		return $website;
	}

	my $agentID = $website->{'AgentID'};
	my $homeDir = $website->{'HomeDirectory'};
	my $httpdConfFile = $website->{'HttpdConfFile'};

#	# Remove all stuff associated with a site
#	my $res = smlib::sites::stats::cascadeRemove($websiteID);
#	if ($res != RES_OK) {
#		setError(smlib::sites::stats::Error());
#		return $res;
#	}
#
#	$res = smlib::sites::frontpage::cascadeRemove($websiteID);
#	if ($res != RES_OK) {
#		setError(smlib::sites::stats::Error());
#		return $res;
#	}
#
#	$res = smlib::databases::cascadeRemove($websiteID);
#	if ($res != RES_OK) {
#		setError(smlib::sites::stats::Error());
#		return $res;
#	}
#
#	$res = smlib::sites::ftp::cascadeRemove($websiteID);
#	if ($res != RES_OK) {
#		setError(smlib::sites::stats::Error());
#		return $res;
#	}

	DBBegin();

	# Remove traffic logs
	my $sth = DBDo("DELETE FROM siteTrafficUsage WHERE SiteID = ".DBQuote($websiteID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove from database
	$sth = DBDo("DELETE FROM sites WHERE ID = ".DBQuote($websiteID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

  	# Remove config file
	unlink($httpdConfFile);

	# Remove home
	rmtree($homeDir,0,0);

	DBCommit();

	# Reload config
	reloadHttpd();

	return RES_OK;
}



## @method getWebsites($websiteInfo,$search)
# Return a hash of sites which belong to an agent
#
# @param websiteInfo Website info hash ref
# @li AgentID - Limit returned results to this agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Website ID on the system
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li DomainName - Domain name of website
# @li DomainAliases - Array ref of domain aliases
# @li HomeDirectory - Home directory of website
# @li HttpdConfFile - HTTPD configuration file
# @li SiteUID - Site user ID
# @li SiteGID - Site group ID
# @li AgentDisabled - Set if the agent has disabled this website (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getWebsites
{
	my ($websiteInfo,$search) = @_;


	my $extraSQL = "";

	# Check if we have an AgentID defined that its valid
	if (defined($websiteInfo)) {
		# Make sure we're a hash
		if (!isHash($websiteInfo)) {
			setError("Parameter 'websiteInfo' is not a HASH");
			return ERR_PARAM;
		}
		# If we have an agent ID, use it
		if (defined($websiteInfo->{'AgentID'})) {
			if (!defined($websiteInfo->{'AgentID'} = isNumber($websiteInfo->{'AgentID'}))) {
				setError("Parameter 'websiteInfo' element 'AgentID'' is invalid");
				return ERR_PARAM;
			}
			$extraSQL = "AND sites.AgentID = ".DBQuote($websiteInfo->{'AgentID'});
		}
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'sites.ID',
		'AgentID' => 'sites.AgentID',
		'DomainName' => 'sites.DomainName',
	};

	# Select websites
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				sites.ID,
				sites.AgentID,
				sites.DomainName,
				sites.DomainAliases,
				sites.DiskQuota,
				sites.DiskUsage,
				sites.HomeDirectory,
				sites.HttpdConfFile,
				sites.SiteUID,
				sites.SiteGID,
				agents.Name AS AgentName
			FROM
				sites, agents
			WHERE
				agents.ID = sites.AgentID
				$extraSQL"
			,$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @sites = ();
	while (my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw(
				ID AgentID DomainName DomainAliases DiskQuota DiskUsage HomeDirectory HttpdConfFile SiteUID
				SiteGID AgentName
			)
		)
	) {
		my $entry;

		# Pull off the domain aliases
		my @domainAliases = ();
		if (defined($row->{'DomainAliases'} && $row->{'DomainAliases'} ne "" )) {
			@domainAliases = split(',',$row->{'DomainAliases'});
		}

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'AgentID'} = $row->{'AgentID'};
		$entry->{'AgentName'} = $row->{'AgentName'};

		$entry->{'DomainName'} = $row->{'DomainName'};
		$entry->{'DomainAliases'} = \@domainAliases;
		$entry->{'DiskQuota'} = $row->{'DiskQuota'};

		if (defined($row->{'DiskUsage'}) && $row->{'DiskUsage'} > 0) {
			$entry->{'DiskUsage'} = $row->{'DiskUsage'};
		} else {
			$entry->{'DiskUsage'} = 0;
		}

		$entry->{'HomeDirectory'} = $row->{'HomeDirectory'};

		$entry->{'HttpdConfFile'} = $row->{'HttpdConfFile'};

		$entry->{'SiteUID'} = $row->{'SiteUID'};
		$entry->{'SiteGID'} = $row->{'SiteGID'};

		push(@sites,$entry);
	}

	DBFreeRes($sth);

	return (\@sites,$numResults);
}


## @method getWebsite($websiteID)
# Return a hash containing the info for this website
#
# @param websiteID Website ID
#
# @return Hash ref containing website info
# @li ID - Website ID on the system
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li DomainName - Domain name of website
# @li HomeDirectory - Home directory of website
# @li HttpdConfFile - HTTPD configuration file
# @li SiteUID - Site user ID
# @li SiteGID - Site group ID
# @li AgentDisabled - Set if the agent has disabled this website  (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getWebsite
{
	my $websiteID = shift;


	if (!defined($websiteID)) {
		setError("Parameter 'websiteID' not defined");
		return ERR_PARAM;
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		setError("Parameter 'websiteID' is invalid");
		return ERR_PARAM;
	}

	# Query website
	my $sth = DBSelect("
			SELECT
				sites.ID,
				sites.AgentID,
				sites.DomainName,
				sites.DomainAliases,
				sites.DiskQuota,
				sites.DiskUsage,
				sites.HttpdConfFile,
				sites.HomeDirectory,
				sites.SiteUID,
				sites.SiteGID,
				agents.Name AS AgentName
			FROM
				sites, agents
			WHERE
				sites.ID = ".DBQuote($websiteID)."
				AND agents.ID = sites.AgentID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("Website ID '$websiteID' not found");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw(
				ID AgentID DomainName DomainAliases DiskQuota DiskUsage HttpdConfFile HomeDirectory SiteUID
				SiteGID AgentName
			)
	);
	my $res;

	my @domainAliases = ();
	if (defined($row->{'DomainAliases'} && $row->{'DomainAliases'} ne "" )) {
		@domainAliases = split(',',$row->{'DomainAliases'});
	}

	$res->{'ID'} = $row->{'ID'};
	$res->{'AgentID'} = $row->{'AgentID'};
	$res->{'AgentName'} = $row->{'AgentName'};
	$res->{'DomainName'} = $row->{'DomainName'};
	$res->{'DomainAliases'} = \@domainAliases;

	$res->{'DiskQuota'} = $row->{'DiskQuota'};

	if (defined($row->{'DiskUsage'}) && $row->{'DiskUsage'} > 0) {
		$res->{'DiskUsage'} = $row->{'DiskUsage'};
	} else {
		$res->{'DiskUsage'} = 0;
	}

	$res->{'HomeDirectory'} = $row->{'HomeDirectory'};
	$res->{'HttpdConfFile'} = $row->{'HttpdConfFile'};

	$res->{'SiteUID'} = $row->{'SiteUID'};
	$res->{'SiteGID'} = $row->{'SiteGID'};

#	$res->{'AgentDisabled'} = $row->{'AgentDisabled'};
#	$res->{'AgentRef'} = $row->{'AgentRef'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method updateWebsite($websiteInfo)
# Update a website
#
# @param websiteInfo Website info hash ref
# @li ID - ID of website
# @li DomainAliases - Array ref of domain aliases
# @li AgentRef - Optional agent reference
# @li AgentDisabled - Optional flag to disable this website
#
# @return 0 on success, < 0 on error
sub updateWebsite
{
	my $websiteInfo = shift;


	if (!defined($websiteInfo)) {
		setError("Parameter 'websiteInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($websiteInfo)) {
		setError("Parameter 'websiteInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($websiteInfo->{"ID"})) {
		setError("Parameter 'websiteInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($websiteInfo->{'ID'} = isNumber($websiteInfo->{'ID'}))) {
		setError("Parameter 'websiteInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	my $website = getWebsite($websiteInfo->{'ID'});
	if (!isHash($website)) {
		return $website;
	}

	# Updates we need to do
	my @updates = ();

	my $domainAliases;
	if (defined($websiteInfo->{'DomainAliases'})) {
		if (ref($websiteInfo->{'DomainAliases'}) ne "ARRAY") {
			setError("Parameter 'websiteInfo' element 'DomainAliases' is not an ARRAY");
			return ERR_PARAM;
		}
		# Chop off blank aliases
		foreach my $alias (@{$websiteInfo->{'DomainAliases'}}) {
			# Fold whitespaces
			(my $cleanAlias = $alias) =~ s/\s+//g;

			# Skip blank
			next if ($cleanAlias eq "");

			# Verify
			if (!defined($cleanAlias = isDomain($cleanAlias))) {
				setError("Parameter 'websiteInfo' element 'DomainName' is invalid");
				return ERR_PARAM;
			}

			push(@{$domainAliases},$cleanAlias);
		}

		my $clashRes;

		# Check for clashes
		$clashRes = checkSiteClash($domainAliases);
		if ($clashRes < 0) {
			smlib::logging::log(LOG_ERR,Error());
			setError("Internal error in checkSiteClash()");
			return $clashRes;
		} elsif ($clashRes > 0) {
			# FIXME - same as function above, needs fixing
			setError("Parameter 'websiteInfo' element 'DomainAliases' has a member that conflicts with a current website");
			return ERR_CONFLICT;
		}

		$clashRes = checkAliasClash($websiteInfo->{'ID'},$domainAliases);
		if ($clashRes < 0) {
			smlib::logging::log(LOG_ERR,Error());
			setError("Internal error in checkAliasClash()");
			return $clashRes;
		} elsif ($clashRes > 0) {
			# FIXME - same as function above, needs fixing
			setError("Parameter 'websiteInfo' element 'DomainAliases' has a member that conflicts with a current alias");
			return ERR_CONFLICT;
		}


		push(@updates,"DomainAliases = ".DBQuote(join(',',@{$domainAliases})));
	}

	if (defined($websiteInfo->{"DiskQuota"})) {
		if (!defined($websiteInfo->{'DiskQuota'} = isNumber($websiteInfo->{'DiskQuota'}))) {
			setError("Parameter 'websiteInfo' element 'DiskQuota' is invalid");
			return ERR_PARAM;
		}
		push(@updates,"DiskQuota = ".DBQuote($websiteInfo->{'DiskQuota'}));
	}

	if (defined($websiteInfo->{'AgentRef'})) {
		push(@updates,"AgentRef = ".DBQuote($websiteInfo->{'AgentRef'}));
	}

	if (defined($websiteInfo->{'AgentDisabled'})) {
		push(@updates,"AgentDisabled = ".DBQuote($websiteInfo->{'AgentDisabled'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for website");
		return ERR_USAGE;
	}

	DBBegin();

	# Update
	my $sth = DBDo("
			UPDATE
				sites
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($websiteInfo->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Parse http config
	my $httpdConf = smlib::Apache::Config->new();
	if ($httpdConf->parse($website->{'HttpdConfFile'}) != RES_OK) {
# FIXME - add error here
		smlib::logging::log(LOG_ERR,smlib::Apache::Config::Error());
		setError("Configuration file error");
		DBRollback();
		return ERR_UNKNOWN;
	}

	# Update aliases
	my $confRoot = $httpdConf->getRoot();
	if (defined($domainAliases)) {
		foreach my $vhost ($confRoot->getSection("VirtualHost")) {
			$vhost->replaceDirective("ServerAlias",undef,@{$domainAliases});
		}
	}

	# Write out config file
	if ($httpdConf->write($website->{'HttpdConfFile'}) != RES_OK) {
# FIXME - add error here
		smlib::logging::log(LOG_ERR,smlib::Apache::Config::Error());
		setError("Configuration file error");
		DBRollback();
		return ERR_UNKNOWN;
	}

	DBCommit();

	return RES_OK;
}



#FIXME -wtf?
## @fn isSiteUsed($siteID)
# Return x > 0 if site is being used, 0 otherwise
#
# @param siteID Site ID
#
# @return > 0 if the site is being used, 0 otherwise
sub isSiteUsed
{
	my $res = 0;

	return $res;
}



## @fn chownSiteContent($websiteiD)
# Chown site content to correct user and group
#
# @param websiteID Website ID
#
# @return 0 on success, < 0 on error
sub chownSiteContent
{
	my $websiteID = shift;


	if (!defined($websiteID)) {
		setError("Parameter 'websiteID' is not defined");
		return ERR_PARAM;
	}

	if (!defined($websiteID = isNumber($websiteID))) {
		setError("Parameter 'websiteID' is invalid");
		return ERR_PARAM;
	}

	my $website = getWebsite($websiteID);
	if (!isHash($website)) {
		return $website;
	}

	# Grab result
	my $htdocDir = $website->{'HomeDirectory'}."/htdocs";
	my $siteUID = $website->{'SiteUID'};
	my $siteGID = $website->{'SiteGID'};

# FIXME - use perl function?
	my $res = system("chown -R $siteUID.$siteGID \"$htdocDir\"");
	$res >>= 8;

	return $res;
}



## @fn checkSiteClash(@domains)
# Function to check if a list of domain names clash with an alias
#
# @param domains Array ref of domains
#
# @return 1 if exists, 0 if not exists, 0 < if error
sub checkSiteClash
{
	my $domains = shift;


	if (!defined($domains)) {
		setError("Parameter 'domains' is not defined");
		return ERR_PARAM;
	}

	if (ref($domains) ne "ARRAY") {
		setError("Parameter 'domains' is not an ARRAY");
		return ERR_PARAM;
	}

	my @cleanDomains;

	# Chop off blank aliases
	foreach my $domain (@{$domains}) {
		if (!defined($domain = isDomain($domain))) {
			setError("Parameter 'websiteInfo' element 'DomainName' is invalid");
			return ERR_PARAM;
		}

		push(@cleanDomains,$domain);
	}

	# Prepare query to see if aliases exist as a site
	my $sth = DBSelect("
			SELECT
				DomainName
			FROM
				sites
			WHERE
				DomainName IN ('".(join "','", @cleanDomains)."')
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results & finish off
	if ($sth->rows > 0) {
		my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( DomainName ));
		setError("Domain '".$row->{'DomainName'}."' conflicts with an existing website");
		DBFreeRes($sth);
		return 1;
	}
	DBFreeRes($sth);

	return RES_OK;
}



## @fn checkAliasClash($websiteID,$aliases)
# Function to check if a list of domain names clash with an alias
#
# @param siteID Site ID
# @param aliases Array ref of aliases
#
# @return 1 if exists, 0 if not exists, 0 < if error
sub checkAliasClash
{
	my ($websiteID,$aliases) = @_;


	if (defined($websiteID)) {
		if (!defined($websiteID = isNumber($websiteID))) {
			setError("Parameter 'websiteID' is invalid");
			return ERR_PARAM;
		}
	}

	if (!defined($aliases)) {
		setError("Parameter 'aliases' is not defined");
		return ERR_PARAM;
	}

	if (ref($aliases) ne "ARRAY") {
		setError("Parameter 'aliases' is not an ARRAY");
		return ERR_PARAM;
	}


	# Create an expr to exclude a site ID if specified
	my $siteIDExpr = "";
	if (defined($websiteID)) {
		$siteIDExpr = "ID != ".DBQuote($websiteID)." AND ";
	}

	# Loop and check
	foreach my $talias (@{$aliases}) {
		if (!defined($talias = isDomain($talias))) {
			setError("Parameter 'aliases' has invalid member '$talias'");
			return ERR_PARAM;
		}

		(my $alias = $talias) =~ s/\./\\./g;

		# Check if there is a current alias which exists
		my $sth = DBSelect("
				SELECT
					DomainName
				FROM
					sites
				WHERE
					$siteIDExpr
					DomainAliases REGEXP '(^|,)".$alias."(\$|,)'
		");
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}

		# Check if we got results & finish off
		if ($sth->rows > 0) {
			my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( DomainName ));
			setError("Domain alias '".$row->{'DomainName'}."' conflicts with an existing website");
			DBFreeRes($sth);
			return 1;
		}

		DBFreeRes($sth);
	}

	return RES_OK;
}




1;
# vim: ts=4
