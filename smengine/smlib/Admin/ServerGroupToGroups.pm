# Server Groups to Groups functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::ServerGroupToGroups
# Server group to group functions
package smlib::Admin::ServerGroupToGroups;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getServerGroupToGroups($serverGroupID,$search)
# Return list of server groups to groups
#
# @param serverGroupID User group ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Server group to groups ID
# @li ServerGroupID - Server group ID
# @li UserGroupID - User Group ID
# @li UserGroupName - User group name
# @li UserGroupDescription - User group description
sub getServerGroupToGroups 
{
	my ($serverGroupID,$search) = @_;


	if (!defined($serverGroupID)) {
		setError("Parameter 'serverGroupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		setError("Parameter 'serverGroupID' is invalid");
		return ERR_PARAM;
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'ServerGroupID' => 'ServerGroupID',
		'UserGroupID' => 'UserGroupID',
		'UserGroupName' => 'UserGroupName',
		'UserGroupDescription' => 'UserGroupDescription',
	};

	# Query server group for group list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				soap_server_group_to_group.ID,
				soap_server_group_to_group.ServerGroupID,
				soap_server_group_to_group.UserGroupID,
				soap_groups.GroupName AS UserGroupName,
				soap_groups.Description AS UserGroupDescription
			FROM
				soap_server_group_to_group, soap_groups
			WHERE
				soap_groups.ID = soap_server_group_to_group.UserGroupID
				AND soap_server_group_to_group.ServerGroupID = ".DBQuote($serverGroupID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @userGroups = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID ServerGroupID UserGroupID UserGroupName UserGroupDescription
				)
			)
	) {
		my $userGroup;

		$userGroup->{'ID'} = $row->{'ID'};
		$userGroup->{'ServerGroupID'} = $row->{'ServerGroupID'};
		$userGroup->{'UserGroupID'} = $row->{'UserGroupID'};
		$userGroup->{'UserGroupName'} = $row->{'UserGroupName'};
		$userGroup->{'UserGroupDescription'} = $row->{'UserGroupDescription'};

		push(@userGroups,$userGroup);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@userGroups,$numResults);
}



## @method addServerGroupToGroup($serverGroupToGroupInfo)
# Add server group to group
#
# @param groupsToServerGroupInfo Server groups to groups info hash ref
# @li ServerGroupID - Server Group ID
# @li UserGroupID - User Group ID
#
# @return Server group to group ID
sub addServerGroupToGroup
{
	my $serverGroupToGroupInfo = shift;

	# Validatate hash
	if (!defined($serverGroupToGroupInfo)) {
		setError("Parameter 'serverGroupToGroupInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($serverGroupToGroupInfo)) {
		setError("Parameter 'serverGroupToGroupInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($serverGroupToGroupInfo->{"ServerGroupID"})) {
		setError("Parameter 'serverGroupToGroupInfo' has no element 'ServerGroupID'");
		return ERR_PARAM;
	}
	if (!defined($serverGroupToGroupInfo->{'ServerGroupID'} = isNumber($serverGroupToGroupInfo->{'ServerGroupID'}))) {
		setError("Parameter 'serverGroupToGroupInfo' element 'ServerGroupID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($serverGroupToGroupInfo->{"UserGroupID"})) {
		setError("Parameter 'serverGroupToGroupInfo' has no element 'UserGroupID'");
		return ERR_PARAM;
	}
	if (!defined($serverGroupToGroupInfo->{'UserGroupID'} = isNumber($serverGroupToGroupInfo->{'UserGroupID'}))) {
		setError("Parameter 'serverGroupToGroupInfo' element 'UserGroupID' is invalid");
		return ERR_PARAM;
	}

	# Check if server group to group association exists
	my $num_results = DBSelectNumResults("FROM soap_server_group_to_group WHERE UserGroupID = ".DBQuote($serverGroupToGroupInfo->{'UserGroupID'})." AND ServerGroupID = ".DBQuote($serverGroupToGroupInfo->{'ServerGroupID'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Group '".$serverGroupToGroupInfo->{'UserGroupID'}."' to server group ".DBQuote($serverGroupToGroupInfo->{'ServerGroupID'})." association already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert group
	my $sth = DBDo("
			INSERT INTO soap_server_group_to_group
				(ServerGroupID,UserGroupID)
			VALUES (".
					DBQuote($serverGroupToGroupInfo->{'ServerGroupID'}).",".
					DBQuote($serverGroupToGroupInfo->{'UserGroupID'}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID('soap_server_group_to_group','serverGroupToGroupsID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method removeServerGroupToGroup($serverGroupToGroupsID)
# Remove a group from an server group
#
# @param serverGroupToGroupsID Server group to group ID
#
# @return 0 on success, < 0 on error
sub removeServerGroupToGroup
{
	my $serverGroupToGroupsID = shift;


	# Check params
	if (!defined($serverGroupToGroupsID)) {
		setError("Parameter 'serverGroupToGroupsID' not defined");
		return ERR_PARAM;
	}
	if (!defined($serverGroupToGroupsID = isNumber($serverGroupToGroupsID))) {
		setError("Parameter 'serverGroupToGroupsID' is invalid");
		return ERR_PARAM;
	}

	DBBegin();

	# Remove group from server group
	my $sth = DBDo("DELETE FROM soap_server_group_to_group WHERE ID = ".DBQuote($serverGroupToGroupsID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
