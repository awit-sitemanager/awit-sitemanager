# Groups to API groups management functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::GroupsToAPIGroups
# Group to API group handling functions
package smlib::Admin::GroupsToAPIGroups;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getGroupsToAPIGroups($groupID,$search)
# Return list of groups to API groups
#
# @param groupID Group ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Groups to API Groups ID
# @li GroupID - Group ID
# @li APIGroupID - API Group ID
# @li APIGroupName - API Group name
# @li APIGroupDescription - API Group description
sub getGroupsToAPIGroups 
{
	my ($groupID,$search) = @_;


	if (!defined($groupID)) {
		setError("Parameter 'groupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($groupID = isNumber($groupID))) {
		setError("Parameter 'groupID' is invalid");
		return ERR_PARAM;
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'GroupID' => 'GroupID',
		'APIGroupID' => 'APIGroupID',
		'APIGroupName' => 'APIGroupName',
		'APIGroupDescription' => 'APIGroupDescription',
	};

	# Query group for group list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				soap_group_to_api_group.ID,
				soap_group_to_api_group.UserGroupID AS GroupID,
				soap_group_to_api_group.APIGroupID,
				soap_api_groups.GroupName AS APIGroupName,
				soap_api_groups.Description AS APIGroupDescription
			FROM
				soap_api_groups, soap_group_to_api_group
			WHERE
				soap_api_groups.ID = soap_group_to_api_group.APIGroupID
				AND soap_group_to_api_group.UserGroupID = ".DBQuote($groupID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @APIGroups = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID GroupID APIGroupID APIGroupName APIGroupDescription
				)
			)
	) {
		my $APIGroup;

		$APIGroup->{'ID'} = $row->{'ID'};
		$APIGroup->{'GroupID'} = $row->{'GroupID'};
		$APIGroup->{'APIGroupID'} = $row->{'APIGroupID'};
		$APIGroup->{'APIGroupName'} = $row->{'APIGroupName'};
		$APIGroup->{'APIGroupDescription'} = $row->{'APIGroupDescription'};

		push(@APIGroups,$APIGroup);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@APIGroups,$numResults);
}



## @method addGroupToAPIGroup($groupToAPIGroupInfo)
# Add group to API group
#
# @param groupsToAPIGroupInfo Groups to API groups info hash ref
# @li GroupID - Group ID
# @li APIGroupID - API Group ID
#
# @return Group to API group ID
sub addGroupToAPIGroup
{
	my $groupToAPIGroupInfo = shift;

	# Validatate hash
	if (!defined($groupToAPIGroupInfo)) {
		setError("Parameter 'groupToAPIGroupInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($groupToAPIGroupInfo)) {
		setError("Parameter 'groupToAPIGroupInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($groupToAPIGroupInfo->{"GroupID"})) {
		setError("Parameter 'groupToAPIGroupInfo' has no element 'GroupID'");
		return ERR_PARAM;
	}
	if (!defined($groupToAPIGroupInfo->{'GroupID'} = isNumber($groupToAPIGroupInfo->{'GroupID'}))) {
		setError("Parameter 'groupToAPIGroupInfo' element 'GroupID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($groupToAPIGroupInfo->{"APIGroupID"})) {
		setError("Parameter 'groupToAPIGroupInfo' has no element 'APIGroupID'");
		return ERR_PARAM;
	}
	if (!defined($groupToAPIGroupInfo->{'APIGroupID'} = isNumber($groupToAPIGroupInfo->{'APIGroupID'}))) {
		setError("Parameter 'groupToAPIGroupInfo' element 'APIGroupID' is invalid");
		return ERR_PARAM;
	}

	# Check if group to API group association exists
	my $num_results = DBSelectNumResults("FROM soap_group_to_api_group WHERE UserGroupID = ".DBQuote($groupToAPIGroupInfo->{'GroupID'})." AND APIGroupID = ".DBQuote($groupToAPIGroupInfo->{'APIGroupID'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Group '".$groupToAPIGroupInfo->{'GroupID'}."' to API group ".DBQuote($groupToAPIGroupInfo->{'APIGroupID'})." association already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert group
	my $sth = DBDo("
			INSERT INTO soap_group_to_api_group
				(UserGroupID, APIGroupID)
			VALUES (".
					DBQuote($groupToAPIGroupInfo->{'GroupID'}).",".
					DBQuote($groupToAPIGroupInfo->{'APIGroupID'}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID =DBLastInsertID('soap_group_to_api_group','groupsToAPIGroupsID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method removeGroupToAPIGroup($groupsToAPIGroupsID)
# Remove a group from an API group
#
# @param groupsToAPIGroupsID Group to API group ID
#
# @return 0 on success, < 0 on error
sub removeGroupToAPIGroup
{
	my $groupsToAPIGroupsID = shift;


	# Check params
	if (!defined($groupsToAPIGroupsID)) {
		setError("Parameter 'groupsToAPIGroupsID' not defined");
		return ERR_PARAM;
	}
	if (!defined($groupsToAPIGroupsID = isNumber($groupsToAPIGroupsID))) {
		setError("Parameter 'groupsToAPIGroupsID' is invalid");
		return ERR_PARAM;
	}

	DBBegin();

	# Remove group from API group
	my $sth = DBDo("DELETE FROM soap_group_to_api_group WHERE ID = ".DBQuote($groupsToAPIGroupsID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
