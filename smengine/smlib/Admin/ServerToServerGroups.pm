# Server To Server Groups functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::ServerToServerGroups
# Server to server groups handling functions
package smlib::Admin::ServerToServerGroups;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getServerToServerGroups($search)
# Return list of server to server groups
#
# @param ServerID server ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - server to server groups ID
# @li ServerID - server ID
# @li ServerGroupID - server group ID
# @li ServerGroupName - Group name
# @li ServerGroupDescription - Group description
sub getServerToServerGroups
{
	my ($ServerID,$search) = @_;


	if (!defined($ServerID)) {
		setError("Parameter 'ServerID' not defined");
		return ERR_PARAM;
	}
	if (!defined($ServerID = isNumber($ServerID))) {
		setError("Parameter 'ServerID' is invalid");
		return ERR_PARAM;
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'ServerID' => 'ServerID',
		'ServerGroupID' => 'ServerGroupID',
		'ServerGroupName' => 'ServerGroupName',
		'ServerGroupDescription' => 'ServerGroupDescription',
	};

	# Query group for group list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				soap_server_to_server_group.ID,
				soap_server_to_server_group.ServerID AS ServerID,
				soap_server_to_server_group.ServerGroupID AS ServerGroupID,
				soap_server_groups.Name AS ServerGroupName,
				soap_server_groups.Description AS ServerGroupDescription
			FROM
				soap_server_groups, soap_server_to_server_group
			WHERE
				soap_server_groups.ID = soap_server_to_server_group.ServerGroupID
				AND soap_server_to_server_group.ServerID = ".DBQuote($ServerID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @sgroups = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID ServerID ServerGroupID ServerGroupName ServerGroupDescription
				)
			)
	) {
		my $sgroup;

		$sgroup->{'ID'} = $row->{'ID'};
		$sgroup->{'ServerID'} = $row->{'ServerID'};
		$sgroup->{'ServerGroupID'} = $row->{'ServerGroupID'};
		$sgroup->{'ServerGroupName'} = $row->{'ServerGroupName'};
		$sgroup->{'ServerGroupDescription'} = $row->{'ServerGroupDescription'};

		push(@sgroups,$sgroup);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@sgroups,$numResults);
}



## @method addServerToServerGroup($serverToServerGroupInfo)
# Add server to server group
#
# @param serverToServerGroupInfo server to server group info hash ref
# @li ServerID - server ID
# @li ServerGroupID - server group ID
#
# @return server to server group ID
sub addServerToServerGroup
{
	my $serverToServerGroupInfo = shift;

	# Validatate hash
	if (!defined($serverToServerGroupInfo)) {
		setError("Parameter 'serverToServerGroupInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($serverToServerGroupInfo)) {
		setError("Parameter 'serverToServerGroupInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($serverToServerGroupInfo->{"ServerID"})) {
		setError("Parameter 'serverToServerGroupInfo' has no element 'ServerID'");
		return ERR_PARAM;
	}
	if (!defined($serverToServerGroupInfo->{'ServerID'} = isNumber($serverToServerGroupInfo->{'ServerID'}))) {
		setError("Parameter 'serverToServerGroupInfo' element 'ServerID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($serverToServerGroupInfo->{"ServerGroupID"})) {
		setError("Parameter 'serverToServerGroupInfo' has no element 'ServerGroupID'");
		return ERR_PARAM;
	}
	if (!defined($serverToServerGroupInfo->{'ServerGroupID'} = isNumber($serverToServerGroupInfo->{'ServerGroupID'}))) {
		setError("Parameter 'serverToServerGroupInfo' element 'ServerGroupID' is invalid");
		return ERR_PARAM;
	}

	# Check if server to server group exists
	my $num_results = DBSelectNumResults("FROM soap_server_to_server_group WHERE ServerID = ".
			DBQuote($serverToServerGroupInfo->{'ServerID'}).
			"AND ServerGroupID = ".DBQuote($serverToServerGroupInfo->{'ServerGroupID'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Server '".$serverToServerGroupInfo->{'ServerID'}."' already added to server group '".
				$serverToServerGroupInfo->{'ServerGroupID'}."'");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert group
	my $sth = DBDo("
			INSERT INTO soap_server_to_server_group
				(ServerID, ServerGroupID)
			VALUES (".
					DBQuote($serverToServerGroupInfo->{'ServerID'}).",".
					DBQuote($serverToServerGroupInfo->{'ServerGroupID'}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID('soap_server_to_server_group','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method removeServerToServerGroup($serverToServerGroupID)
# Remove a server from a server group
#
# @param serverToServerGroupID server to server group ID
#
# @return 0 on success, < 0 on error
sub removeServerToServerGroup
{
	my $serverToServerGroupID = shift;


	# Check params
	if (!defined($serverToServerGroupID)) {
		setError("Parameter 'serverToServerGroupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($serverToServerGroupID = isNumber($serverToServerGroupID))) {
		setError("Parameter 'serverToServerGroupID' is invalid");
		return ERR_PARAM;
	}

	# Check if topup exists
	my $num_results = DBSelectNumResults("FROM soap_server_to_server_group WHERE ID = ".DBQuote($serverToServerGroupID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("Server to server group ID '$serverToServerGroupID' does not exist");
		return ERR_EXISTS;
	}

	DBBegin();

	# Remove server from server group
	my $sth = DBDo("DELETE FROM soap_server_to_server_group WHERE ID = ".DBQuote($serverToServerGroupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
