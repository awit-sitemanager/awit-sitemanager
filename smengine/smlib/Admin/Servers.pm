# Server functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::Servers
# Server handling functions
package smlib::Admin::Servers;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getServers($search)
# Return list of Servers
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Server ID
# @li Name - Server name
sub getServers 
{
	my ($search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Name' => 'Name'
	};

	# Query for server list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				Name
			FROM
				soap_servers
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @sservers = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw(	ID Name	))) {
		my $sserver;

		$sserver->{'ID'} = $row->{'ID'};
		$sserver->{'Name'} = $row->{'Name'};

		push(@sservers,$sserver);
	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@sservers,$numResults);
}



## @method getServer($serverID)
# Return server info hash
#
# @param serverID Server ID
#
# @return server info hash ref
# @li ID Server ID
# @li Name server name
sub getServer
{
	my $serverID = shift;


	if (!defined($serverID)) {
		setError("Parameter 'serverID' not defined");
		return ERR_PARAM;
	}
	if (!defined($serverID = isNumber($serverID))) {
		setError("Parameter 'serverID' is invalid");
		return ERR_PARAM;
	}

	# Query servers
	my $sth = DBSelect("
		SELECT
			ID, Name
		FROM
			soap_servers
		WHERE
			ID = ".DBQuote($serverID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw(
				ID Name
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Name'} = $row->{'Name'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method createServer($serverInfo)
# Create a server
#
# @param serverInfo Server info hash ref
# @li Name Server name
#
# @return Server ID
sub createServer
{
	my $serverInfo = shift;

	# Validatate hash
	if (!defined($serverInfo)) {
		setError("Parameter 'serverInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($serverInfo)) {
		setError("Parameter 'serverInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($serverInfo->{"Name"})) {
		setError("Parameter 'serverInfo' has no element 'Name'");
		return ERR_PARAM;
	}
	if (!isVariable($serverInfo->{"Name"})) {
		setError("Parameter 'serverInfo' element 'Name' is invalid");
		return ERR_PARAM;
	}
	if ($serverInfo->{"Name"} eq "") {
		setError("Parameter 'serverInfo' element 'Name' is blank");
		return ERR_PARAM;
	}

	# Check if server exists
	my $num_results = DBSelectNumResults("FROM soap_servers WHERE Name = ".DBQuote($serverInfo->{'Name'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Server with name '".$serverInfo->{'Name'}."' already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert server
	my $sth = DBDo("
			INSERT INTO soap_servers
				(Name)
			VALUES (".DBQuote($serverInfo->{"Name"}).")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID('soap_servers','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateServer($serverInfo)
# Update server
#
# @param serverInfo Server info hash ref
# @li ID Server ID
# @li Name Optional server name
#
# @return 0 on success, < 0 on error
sub updateServer
{
	my $serverInfo = shift;

	if (!defined($serverInfo)) {
		setError("Parameter 'serverInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($serverInfo)) {
		setError("Parameter 'serverInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($serverInfo->{'ID'})) {
		setError("Parameter 'serverInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($serverInfo->{'ID'} = isNumber($serverInfo->{'ID'}))) {
		setError("Parameter 'serverInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	my @updates = ();

	if (defined($serverInfo->{'Name'})) {
		if (!isVariable($serverInfo->{"Name"})) {
			setError("Parameter 'serverInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
		if ($serverInfo->{"Name"} eq "") {
			setError("Parameter 'serverInfo' element 'Name' is blank");
			return ERR_PARAM;
		}
		push(@updates,"Name = ".DBQuote($serverInfo->{'Name'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for server");
		return ERR_USAGE;
	}

	# Grab server to see if he exists
	my $server = getServer($serverInfo->{'ID'});
	# We already have error set, so return
	if (!isHash($server)) {
		return $server;
	}

	DBBegin();

	# Update quotas
	my $sth = DBDo("
			UPDATE
				soap_servers
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($serverInfo->{'ID'})
	);

	# Check for errors
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeServer($serverID)
# Remove a server
#
# @param serverID Server ID
#
# @return 0 on success, < 0 on error
sub removeServer
{
	my $serverID = shift;


	if (!defined($serverID)) {
		setError("Parameter 'serverID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($serverID = isNumber($serverID))) {
		setError("Parameter 'serverID' is invalid");
		return ERR_PARAM;
	}

	# Grab server info
	my $server = getServer($serverID);
	if (!isHash($server)) {
		return $server;
	}

	DBBegin();

	# Remove dependencies
	my $sth = DBDo("DELETE FROM soap_server_attributes WHERE ServerID = ".DBQuote($serverID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}
	$sth = DBDo("DELETE FROM soap_server_to_server_group WHERE ServerID = ".DBQuote($serverID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove server from database
	$sth = DBDo("DELETE FROM soap_servers WHERE ID = ".DBQuote($serverID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
