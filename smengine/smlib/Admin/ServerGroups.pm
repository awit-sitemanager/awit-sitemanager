# Server group functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::ServerGroups
# Server group handling functions
package smlib::Admin::ServerGroups;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getServerGroups($search)
# Return list of server groups
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID server group ID
# @li Name server group name
# @li Description server group description
sub getServerGroups
{
	my ($search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Name' => 'Name',
		'Description' => 'Description'
	};

	# Query for server group list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				Name,
				Description
			FROM
				soap_server_groups
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @ssgroups = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw(	ID Name	Description ))) {
		my $ssgroup;

		$ssgroup->{'ID'} = $row->{'ID'};
		$ssgroup->{'Name'} = $row->{'Name'};
		$ssgroup->{'Description'} = $row->{'Description'};

		push(@ssgroups,$ssgroup);
	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@ssgroups,$numResults);
}



## @method getServerGroup($serverGroupID)
# Return server group info hash
#
# @param serverGroupID server group ID
#
# @return server group info hash ref
# @li ID server group ID
# @li Name server group name
# @li Description server group description
sub getServerGroup
{
	my $serverGroupID = shift;


	if (!defined($serverGroupID)) {
		setError("Parameter 'serverGroupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		setError("Parameter 'serverGroupID' is invalid");
		return ERR_PARAM;
	}

	# Query server groups
	my $sth = DBSelect("
		SELECT
			ID, Name, Description
		FROM
			soap_server_groups
		WHERE
			ID = ".DBQuote($serverGroupID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw(
				ID Name Description
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Name'} = $row->{'Name'};
	$res->{'Description'} = $row->{'Description'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method createServerGroup($serverGroupInfo)
# Create a server
#
# @param serverGroupInfo server info hash ref
# @li Name server name
#
# @return server ID
sub createServerGroup
{
	my $serverGroupInfo = shift;

	# Validatate hash
	if (!defined($serverGroupInfo)) {
		setError("Parameter 'serverGroupInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($serverGroupInfo)) {
		setError("Parameter 'serverGroupInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($serverGroupInfo->{"Name"})) {
		setError("Parameter 'serverGroupInfo' has no element 'Name'");
		return ERR_PARAM;
	}
	if (!isVariable($serverGroupInfo->{"Name"})) {
		setError("Parameter 'serverGroupInfo' element 'Name' is invalid");
		return ERR_PARAM;
	}
	if ($serverGroupInfo->{"Name"} eq "") {
		setError("Parameter 'serverGroupInfo' element 'Name' is blank");
		return ERR_PARAM;
	}

	# Check if we doing advanced things...
	my $extraColumns = "";
	my $extraValues = "";

	if (defined($serverGroupInfo->{'Description'})) {
		$extraColumns .= ", Description";
		$extraValues .= ", ".DBQuote($serverGroupInfo->{'Description'});
	}

	# Check if server exists
	my $num_results = DBSelectNumResults("FROM soap_server_groups WHERE Name = ".DBQuote($serverGroupInfo->{'Name'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Server with name '".$serverGroupInfo->{'Name'}."' already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert server
	my $sth = DBDo("
			INSERT INTO soap_server_groups
				(Name$extraColumns)
			VALUES (".DBQuote($serverGroupInfo->{"Name"})."$extraValues)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID('soap_server_groups','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateServerGroup($serverGroupInfo)
# Update server
#
# @param serverGroupInfo server info hash ref
# @li ID server ID
# @li Name Optional server name
#
# @return 0 on success, < 0 on error
sub updateServerGroup
{
	my $serverGroupInfo = shift;

	if (!defined($serverGroupInfo)) {
		setError("Parameter 'serverGroupInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($serverGroupInfo)) {
		setError("Parameter 'serverGroupInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($serverGroupInfo->{'ID'})) {
		setError("Parameter 'serverGroupInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($serverGroupInfo->{'ID'} = isNumber($serverGroupInfo->{'ID'}))) {
		setError("Parameter 'serverGroupInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	my @updates = ();

	if (defined($serverGroupInfo->{'Name'})) {
		if (!isVariable($serverGroupInfo->{"Name"})) {
			setError("Parameter 'serverGroupInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
		if ($serverGroupInfo->{"Name"} eq "") {
			setError("Parameter 'serverGroupInfo' element 'Name' is blank");
			return ERR_PARAM;
		}
		push(@updates,"Name = ".DBQuote($serverGroupInfo->{'Name'}));
	}

	if (defined($serverGroupInfo->{'Description'})) {
		if (!isVariable($serverGroupInfo->{"Description"})) {
			setError("Parameter 'serverGroupInfo' element 'Description' is invalid");
			return ERR_PARAM;
		}
		if ($serverGroupInfo->{"Description"} eq "") {
			setError("Parameter 'serverGroupInfo' element 'Description' is blank");
			return ERR_PARAM;
		}
		push(@updates,"Description = ".DBQuote($serverGroupInfo->{'Description'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for server group");
		return ERR_USAGE;
	}

	# Grab server to see if he exists
	my $serverGroup = getServerGroup($serverGroupInfo->{'ID'});
	# We already have error set, so return
	if (!isHash($serverGroup)) {
		return $serverGroup;
	}

	DBBegin();

	# Update server group
	my $sth = DBDo("
			UPDATE
				soap_server_groups
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($serverGroupInfo->{'ID'})
	);

	# Check for errors
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeServerGroup($serverGroupID)
# Remove a server group
#
# @param serverGroupID server group ID
#
# @return 0 on success, < 0 on error
sub removeServerGroup
{
	my $serverGroupID = shift;


	if (!defined($serverGroupID)) {
		setError("Parameter 'serverGroupID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		setError("Parameter 'serverGroupID' is invalid");
		return ERR_PARAM;
	}

	# Grab server info
	my $serverGroup = getServerGroup($serverGroupID);
	if (!isHash($serverGroup)) {
		return $serverGroup;
	}

	DBBegin();

	# Remove dependencies
	my $sth = DBDo("DELETE FROM soap_server_group_attributes WHERE ServerGroupID = ".DBQuote($serverGroupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}
	$sth = DBDo("DELETE FROM soap_server_group_to_agent WHERE ServerGroupID = ".DBQuote($serverGroupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove server group from database
	$sth = DBDo("DELETE FROM soap_server_groups WHERE ID = ".DBQuote($serverGroupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
