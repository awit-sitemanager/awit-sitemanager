# Server group attribute functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::ServerGroupAttributes
# Server group attribute handling functions
package smlib::Admin::ServerGroupAttributes;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getServerGroupAttributes($serverGroupID,$search)
# Return list of server group attributes
#
# @param serverGroupID Server group ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID Server group attribute ID
# @li Attribute Server group attribute
sub getServerGroupAttributes
{
	my ($serverGroupID,$search) = @_;


	# Validatate hash
	if (!defined($serverGroupID)) {
		setError("Parameter 'serverGroupID' is undefined");
		return ERR_PARAM;
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		setError("Parameter 'serverGroupID' is invalid");
		return ERR_PARAM;
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Attribute' => 'Attribute'
	};

	# Query
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				Attribute
			FROM
				soap_server_group_attributes
			WHERE
				ServerGroupID = ".DBQuote($serverGroupID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @serverGroupAttributes = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID Attribute
				)
			)
	) {
		my $serverGroupAttribute;

		$serverGroupAttribute->{'ID'} = $row->{'ID'};
		$serverGroupAttribute->{'Attribute'} = $row->{'Attribute'};

		push(@serverGroupAttributes,$serverGroupAttribute);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@serverGroupAttributes,$numResults);
}



## @method getServerGroupAttribute($attributeID)
# Return server group attribute info hash
#
# @param attributeID Attribute ID
#
# @return Attribute info hash ref
# @li ID Attribute ID
# @li Attribute Attribute
sub getServerGroupAttribute
{
	my $attributeID = shift;


	if (!defined($attributeID)) {
		setError("Parameter 'attributeID' not defined");
		return ERR_PARAM;
	}
	if (!defined($attributeID = isNumber($attributeID))) {
		setError("Parameter 'attributeID' is invalid");
		return ERR_PARAM;
	}

	# Query site
	my $sth = DBSelect("
		SELECT
			ID, Attribute
		FROM
			soap_server_group_attributes
		WHERE
			ID = ".DBQuote($attributeID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw(
				ID Attribute
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Attribute'} = $row->{'Attribute'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method createServerGroupAttribute($attributeInfo)
# Create a server group attribute
#
# @param attributeInfo Attribute info hash ref
# @li ServerGroupID Server group ID
# @li Attribute Attribute
#
# @return Server Group Attribute ID
sub createServerGroupAttribute
{
	my $attributeInfo = shift;

	# Validatate hash
	if (!defined($attributeInfo)) {
		setError("Parameter 'attributeInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($attributeInfo)) {
		setError("Parameter 'attributeInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($attributeInfo->{"ServerGroupID"})) {
		setError("Parameter 'attributeInfo' has no element 'ServerGroupID'");
		return ERR_PARAM;
	}
	if (!defined($attributeInfo->{'ServerGroupID'} = isNumber($attributeInfo->{"ServerGroupID"}))) {
		setError("Parameter 'attributeInfo' element 'ServerGroupID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($attributeInfo->{"Attribute"})) {
		setError("Parameter 'attributeInfo' has no element 'Attribute'");
		return ERR_PARAM;
	}
	if (!isVariable($attributeInfo->{"Attribute"})) {
		setError("Parameter 'attributeInfo' element 'Attribute' is invalid");
		return ERR_PARAM;
	}
	if ($attributeInfo->{"Attribute"} eq "") {
		setError("Parameter 'attributeInfo' element 'Attribute' is blank");
		return ERR_PARAM;
	}

	# Check if attribute exists
	my $num_results = DBSelectNumResults("FROM soap_server_group_attributes WHERE Attribute = ".DBQuote($attributeInfo->{'Attribute'})."
			AND ServerGroupID = ".DBQuote($attributeInfo->{'ServerGroupID'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Server group attribute '".$attributeInfo->{'Attribute'}."' already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert attribute
	my $sth = DBDo("
			INSERT INTO soap_server_group_attributes
				(Attribute, ServerGroupID)
			VALUES (".
					DBQuote($attributeInfo->{'Attribute'}).",".
					DBQuote($attributeInfo->{"ServerGroupID"}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID('soap_server_group_attributes','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateServerGroupAttribute($attributeInfo)
# Update server group attribute
#
# @param attributeInfo Attribute info hash ref
# @li ID Server group attribute ID
# @li Attribute Server attribute
#
# @return 0 on success, < 0 on error
sub updateServerGroupAttribute
{
	my $attributeInfo = shift;

	if (!defined($attributeInfo)) {
		setError("Parameter 'attributeInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($attributeInfo)) {
		setError("Parameter 'attributeInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($attributeInfo->{'ID'})) {
		setError("Parameter 'attributeInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($attributeInfo->{'ID'} = isNumber($attributeInfo->{'ID'}))) {
		setError("Parameter 'attributeInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	my @updates = ();

	if (defined($attributeInfo->{'Attribute'})) {
		if (!isVariable($attributeInfo->{"Attribute"})) {
			setError("Parameter 'attributeInfo' element 'Attribute' is invalid");
			return ERR_PARAM;
		}
		if ($attributeInfo->{"Attribute"} eq "") {
			setError("Parameter 'attributeInfo' element 'Attribute' is blank");
			return ERR_PARAM;
		}
		push(@updates,"Attribute = ".DBQuote($attributeInfo->{'Attribute'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for attribute");
		return ERR_USAGE;
	}

	# Grab attribute to see if it exists
	my $attribute = getServerGroupAttribute($attributeInfo->{'ID'});
	# We already have error set, so return
	if (!isHash($attribute)) {
		return $attribute;
	}

	DBBegin();

	# Update server group attribute
	my $sth = DBDo("
			UPDATE
				soap_server_group_attributes
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($attributeInfo->{'ID'})
	);

	# Check for errors
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeServerGroupAttribute($attributeID)
# Remove a server group attribute
#
# @param attributeID Attribute ID
#
# @return 0 on success, < 0 on error
sub removeServerGroupAttribute
{
	my $attributeID = shift;


	if (!defined($attributeID)) {
		setError("Parameter 'attributeID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($attributeID = isNumber($attributeID))) {
		setError("Parameter 'attributeID' is invalid");
		return ERR_PARAM;
	}

	# Grab attribute info
	my $attribute = getServerGroupAttribute($attributeID);
	if (!isHash($attribute)) {
		return $attribute;
	}

	# Remove server attribute
	my $sth = DBDo("DELETE FROM soap_server_group_attributes WHERE ID = ".DBQuote($attributeID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	return RES_OK;
}



1;
# vim: ts=4
