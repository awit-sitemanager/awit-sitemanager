# User management functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::Users
# User handling functions
package smlib::Admin::Users;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;

use smlib::system qw(hashPassword);


## @method getUsers($search)
# Return list of SOAP users
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - SOAP User ID
# @li Username - SOAP Username
# @li Name - SOAP Realm name
# @li Disabled - Set if user is disabled (NOT IMPLEMENTED)
sub getUsers 
{
	my ($search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Username' => 'Username',
		'Name' => 'Name',
	};

	# Query user for user list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				Username,
				Name
			FROM
				soap_users
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @susers = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID Username Name
				)
			)
	) {
		my $suser;

		$suser->{'ID'} = $row->{'ID'};
		$suser->{'Username'} = $row->{'Username'};
		$suser->{'Name'} = $row->{'Name'};

		push(@susers,$suser);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@susers,$numResults);
}



## @method getUser($userID)
# Return user info hash
#
# @param userID User ID
#
# @return User info hash ref
# @li ID - User ID
# @li Username - User Name
# @li Name - Name
sub getUser
{
	my $userID = shift;


	if (!defined($userID)) {
		setError("Parameter 'userID' not defined");
		return ERR_PARAM;
	}
	if (!defined($userID = isNumber($userID))) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Query site
	my $sth = DBSelect("
		SELECT
			ID, Username, Name
		FROM
			soap_users
		WHERE
			ID = ".DBQuote($userID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw(
				ID Username Name
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Username'} = $row->{'Username'};
	$res->{'Name'} = $row->{'Name'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method createUser($userInfo)
# Create a user
#
# @param userInfo Agent info hash ref
# @li Username - User username
# @li Password - User password
# @li Name - User name
#
# @return User ID
sub createUser
{
	my $userInfo = shift;

	# Validatate hash
	if (!defined($userInfo)) {
		setError("Parameter 'userInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($userInfo)) {
		setError("Parameter 'userInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{"Username"})) {
		setError("Parameter 'userInfo' has no element 'Username'");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{"Username"} = isUsername($userInfo->{"Username"},ISUSERNAME_ALLOW_ATSIGN))) {
		setError("Parameter 'userInfo' element 'Username' is invalid");
		return ERR_PARAM;
	}
	if ($userInfo->{"Username"} eq "") {
		setError("Parameter 'userInfo' element 'Name' is blank");
		return ERR_PARAM;
	}

	if (!defined($userInfo->{"Password"})) {
		setError("Parameter 'userInfo' has no element 'Password'");
		return ERR_PARAM;
	}
	if (!isVariable($userInfo->{"Password"})) {
		setError("Parameter 'userInfo' element 'Password' is invalid");
		return ERR_PARAM;
	}
	if ($userInfo->{"Password"} eq "") {
		setError("Parameter 'userInfo' element 'Name' is blank");
		return ERR_PARAM;
	}
	if ((my $error = isPasswordInsecure($userInfo->{"Password"},[$userInfo->{'Username'}]))) {
		setError("Parameter 'userInfo' element 'Password' is INSECURE: $error");
		return ERR_PARAM;
	}


	if (!defined($userInfo->{"Name"})) {
		setError("Parameter 'userInfo' has no element 'Name'");
		return ERR_PARAM;
	}
	if (!isVariable($userInfo->{"Name"})) {
		setError("Parameter 'userInfo' element 'Name' is invalid");
		return ERR_PARAM;
	}
	if ($userInfo->{"Name"} eq "") {
		setError("Parameter 'userInfo' element 'Name' is blank");
		return ERR_PARAM;
	}

	# Check if user exists
	my $num_results = DBSelectNumResults("FROM soap_users WHERE Name = ".DBQuote($userInfo->{'Username'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("User '".$userInfo->{'Username'}."' already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert user
	my $sth = DBDo("
			INSERT INTO soap_users
				(Username, Password, Name)
			VALUES (".
					DBQuote($userInfo->{'Username'}).",".
					DBQuote(hashPassword($userInfo->{"Password"})).",".
					DBQuote($userInfo->{"Name"}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID =DBLastInsertID('soap_users','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateUser($userInfo)
# Update user
#
# @param userInfo User info hash ref
# @li ID - User ID
# @li Password - Optional password
# @li Name - Optional name
#
# @return 0 on success, < 0 on error
sub updateUser
{
	my $userInfo = shift;

	if (!defined($userInfo)) {
		setError("Parameter 'userInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($userInfo)) {
		setError("Parameter 'userInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{'ID'})) {
		setError("Parameter 'userInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($userInfo->{'ID'} = isNumber($userInfo->{'ID'}))) {
		setError("Parameter 'userInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	if (defined($userInfo->{'Password'})) {
		if (!isVariable($userInfo->{"Password"})) {
			setError("Parameter 'userInfo' element 'Password' is invalid");
			return ERR_PARAM;
		}
		if ($userInfo->{"Password"} eq "") {
			delete($userInfo->{'Password'});
		}
	}

	# Grab ourselves, so we can check a few things below
	my $soapUser = getUser($userInfo->{'ID'});
	if (!isHash($soapUser)) {
		return $soapUser;
	}

	# We do this here cause we don't have user info above
	if (defined($userInfo->{'Password'})) {
		# Make sure password is secure
		if ((my $error = isPasswordInsecure($userInfo->{'Password'},[ $soapUser->{'Username'} ]))) {
			setError("Parameter 'userInfo' element 'Password' is INSECURE: $error");
			return ERR_PARAM;
		}
	}

	my @updates = ();

	if (defined($userInfo->{'Name'})) {
		if (!isVariable($userInfo->{"Name"})) {
			setError("Parameter 'userInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
		if ($userInfo->{"Name"} eq "") {
			setError("Parameter 'userInfo' element 'Name' is blank");
			return ERR_PARAM;
		}
		push(@updates,"Name = ".DBQuote($userInfo->{'Name'}));
	}

	if (defined($userInfo->{'Password'})) {
		my $cpass = hashPassword($userInfo->{'Password'});
		push(@updates,"Password = ".DBQuote($cpass));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for user");
		return ERR_USAGE;
	}

	# Grab user to see if he exists
	my $user = getUser($userInfo->{'ID'});
	# We already have error set, so return
	if (!isHash($user)) {
		return $user;
	}

	DBBegin();

	# Update quotas
	my $sth = DBDo("
			UPDATE
				soap_users
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($userInfo->{'ID'})
	);

	# Check for errors
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeUser($userID)
# Remove a user
#
# @param userID User ID
#
# @return 0 on success, < 0 on error
sub removeUser
{
	my $userID = shift;


	if (!defined($userID)) {
		setError("Parameter 'userID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($userID = isNumber($userID))) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Grab user info
	my $user = getUser($userID);
	if (!isHash($user)) {
		return $user;
	}

	DBBegin();

	# Remove user attributes
	my $sth = DBDo("DELETE FROM soap_user_attributes WHERE UserID = ".DBQuote($userID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Unlink user from groups
	$sth = DBDo("DELETE FROM soap_user_to_group WHERE UserID = ".DBQuote($userID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Unlink user from API groups
	$sth = DBDo("DELETE FROM soap_user_to_api_group WHERE UserID = ".DBQuote($userID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove user from database
	$sth = DBDo("DELETE FROM soap_users WHERE ID = ".DBQuote($userID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
