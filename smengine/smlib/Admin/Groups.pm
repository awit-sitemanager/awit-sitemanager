# Group management functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::Groups
# Group handling functions
package smlib::Admin::Groups;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getGroups($search)
# Return list of SOAP groups
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - SOAP Group ID
# @li Name - SOAP Group name
# @li Description - Group description
sub getGroups 
{
	my ($search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Name' => 'Name',
		'Description' => 'Description',
	};

	# Query group for group list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				GroupName AS Name,
				Description
			FROM
				soap_groups
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @sgroups = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID Name Description
				)
			)
	) {
		my $sgroup;

		$sgroup->{'ID'} = $row->{'ID'};
		$sgroup->{'Name'} = $row->{'Name'};
		$sgroup->{'Description'} = $row->{'Description'};

		push(@sgroups,$sgroup);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@sgroups,$numResults);
}



## @method getGroup($groupID)
# Return group info hash
#
# @param groupID Group ID
#
# @return Group info hash ref
# @li ID - Group ID
# @li Name - Group Name
# @li Description - Group description
sub getGroup
{
	my $groupID = shift;


	if (!defined($groupID)) {
		setError("Parameter 'groupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($groupID = isNumber($groupID))) {
		setError("Parameter 'groupID' is invalid");
		return ERR_PARAM;
	}

	# Query site
	my $sth = DBSelect("
		SELECT
			ID,
			GroupName AS Name,
			Description
		FROM
			soap_groups
		WHERE
			ID = ".DBQuote($groupID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw(
				ID Name Description
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Name'} = $row->{'Name'};
	$res->{'Description'} = $row->{'Description'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method createGroup($groupInfo)
# Create a group
#
# @param groupInfo Agent info hash ref
# @li Name - Group name
# @li Description - Group description
#
# @return Group ID
sub createGroup
{
	my $groupInfo = shift;

	# Validatate hash
	if (!defined($groupInfo)) {
		setError("Parameter 'groupInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($groupInfo)) {
		setError("Parameter 'groupInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($groupInfo->{"Name"})) {
		setError("Parameter 'groupInfo' has no element 'Name'");
		return ERR_PARAM;
	}
	if (!isVariable($groupInfo->{"Name"})) {
		setError("Parameter 'groupInfo' element 'Name' is invalid");
		return ERR_PARAM;
	}
	if ($groupInfo->{"Name"} eq "") {
		setError("Parameter 'groupInfo' element 'Name' is blank");
		return ERR_PARAM;
	}

	if (defined($groupInfo->{"Description"})) {
		if (!isVariable($groupInfo->{"Description"})) {
			setError("Parameter 'groupInfo' element 'Description' is invalid");
			return ERR_PARAM;
		}
		if ($groupInfo->{"Description"} eq "") {
			setError("Parameter 'groupInfo' element 'Description' is blank");
			return ERR_PARAM;
		}
	}

	# Check if group exists
	my $num_results = DBSelectNumResults("FROM soap_groups WHERE GroupName = ".DBQuote($groupInfo->{'Name'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Group '".$groupInfo->{'Name'}."' already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert group
	my $sth = DBDo("
			INSERT INTO soap_groups
				(GroupName, Description)
			VALUES (".
					DBQuote($groupInfo->{'Name'}).",".
					DBQuote($groupInfo->{"Description"}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID =DBLastInsertID('soap_groups','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateGroup($groupInfo)
# Update group
#
# @param groupInfo Group info hash ref
# @li ID - Group ID
# @li Name - Group Name
# @li Description - Optional description
#
# @return 0 on success, < 0 on error
sub updateGroup
{
	my $groupInfo = shift;

	if (!defined($groupInfo)) {
		setError("Parameter 'groupInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($groupInfo)) {
		setError("Parameter 'groupInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($groupInfo->{'ID'})) {
		setError("Parameter 'groupInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($groupInfo->{'ID'} = isNumber($groupInfo->{'ID'}))) {
		setError("Parameter 'groupInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	my @updates = ();

	if (defined($groupInfo->{'Description'})) {
		if (!isVariable($groupInfo->{"Description"})) {
			setError("Parameter 'groupInfo' element 'Description' is invalid");
			return ERR_PARAM;
		}
		if ($groupInfo->{"Description"} eq "") {
			setError("Parameter 'groupInfo' element 'Description' is blank");
			return ERR_PARAM;
		}
		push(@updates,"Description = ".DBQuote($groupInfo->{'Description'}));
	}

	if (defined($groupInfo->{'Name'})) {
		if (!isVariable($groupInfo->{"Name"})) {
			setError("Parameter 'groupInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
		if ($groupInfo->{"Name"} eq "") {
			setError("Parameter 'groupInfo' element 'Name' is blank");
			return ERR_PARAM;
		}
		push(@updates,"GroupName = ".DBQuote($groupInfo->{'Name'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for group");
		return ERR_USAGE;
	}

	# Grab group to see if he exists
	my $group = getGroup($groupInfo->{'ID'});
	# We already have error set, so return
	if (!isHash($group)) {
		return $group;
	}

	DBBegin();

	# Update quotas
	my $sth = DBDo("
			UPDATE
				soap_groups
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($groupInfo->{'ID'})
	);

	# Check for errors
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeGroup($groupID)
# Remove an API Group
#
# @param groupID group ID
#
# @return 0 on success, < 0 on error
sub removeGroup
{
	my $groupID = shift;


	# Check params
	if (!defined($groupID)) {
		setError("Parameter 'groupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($groupID = isNumber($groupID))) {
		setError("Parameter 'groupID' is invalid");
		return ERR_PARAM;
	}

	# Check if API group exists
	my $num_results = DBSelectNumResults("FROM soap_groups WHERE ID = ".DBQuote($groupID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("Group ID '$groupID' does not exist");
		return ERR_EXISTS;
	}

	DBBegin();

	# Remove users from group
	my $sth = DBDo("DELETE FROM soap_user_to_group WHERE UserGroupID = ".DBQuote($groupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove group from api group
	$sth = DBDo("DELETE FROM soap_group_to_api_group WHERE UserGroupID = ".DBQuote($groupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove group attributes
	$sth = DBDo("DELETE FROM soap_group_attributes WHERE UserGroupID = ".DBQuote($groupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove group
	$sth = DBDo("DELETE FROM soap_groups WHERE ID = ".DBQuote($groupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
