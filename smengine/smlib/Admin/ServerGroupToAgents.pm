# Server Group To Agents functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::ServerGroupToAgents
# Server group to agent handling functions
package smlib::Admin::ServerGroupToAgents;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getServerGroupToAgents($serverGroupID,$search)
# Return list of server group to agents
#
# @param serverGroupID Server ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Server to server groups ID
# @li ServerGroupID - Server ID
# @li AgentID - Server group ID
# @li AgentName - Group name
sub getServerGroupToAgents
{
	my ($serverGroupID,$search) = @_;


	if (!defined($serverGroupID)) {
		setError("Parameter 'serverGroupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		setError("Parameter 'serverGroupID' is invalid");
		return ERR_PARAM;
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'ServerGroupID' => 'ServerGroupID',
		'AgentID' => 'AgentID',
		'AgentName' => 'AgentName'
	};

	# Query server group to agents for agents list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				soap_server_group_to_agent.ID,
				soap_server_group_to_agent.ServerGroupID,
				soap_server_group_to_agent.AgentID,
				agents.Name AS AgentName
			FROM
				agents, soap_server_group_to_agent
			WHERE
				agents.ID = soap_server_group_to_agent.AgentID
				AND soap_server_group_to_agent.ServerGroupID = ".DBQuote($serverGroupID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @sgroupAgents = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID ServerGroupID AgentID AgentName
				)
			)
	) {
		my $sgroupAgent;

		$sgroupAgent->{'ID'} = $row->{'ID'};
		$sgroupAgent->{'ServerGroupID'} = $row->{'ServerGroupID'};
		$sgroupAgent->{'AgentID'} = $row->{'AgentID'};
		$sgroupAgent->{'AgentName'} = $row->{'AgentName'};

		push(@sgroupAgents,$sgroupAgent);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@sgroupAgents,$numResults);
}



## @method addServerGroupToAgent($serverGroupToAgentInfo)
# Add server group to agent
#
# @param serverGroupToAgentInfo Server group to agent info hash ref
# @li ServerGroupID - Server group ID
# @li AgentID - Agent ID
#
# @return Server group to agent ID
sub addServerGroupToAgent
{
	my $serverGroupToAgentInfo = shift;

	# Validatate hash
	if (!defined($serverGroupToAgentInfo)) {
		setError("Parameter 'serverGroupToAgentInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($serverGroupToAgentInfo)) {
		setError("Parameter 'serverGroupToAgentInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($serverGroupToAgentInfo->{"ServerGroupID"})) {
		setError("Parameter 'serverGroupToAgentInfo' has no element 'ServerGroupID'");
		return ERR_PARAM;
	}
	if (!defined($serverGroupToAgentInfo->{'ServerGroupID'} = isNumber($serverGroupToAgentInfo->{'ServerGroupID'}))) {
		setError("Parameter 'serverGroupToAgentInfo' element 'ServerGroupID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($serverGroupToAgentInfo->{"AgentID"})) {
		setError("Parameter 'serverGroupToAgentInfo' has no element 'AgentID'");
		return ERR_PARAM;
	}
	if (!defined($serverGroupToAgentInfo->{'AgentID'} = isNumber($serverGroupToAgentInfo->{'AgentID'}))) {
		setError("Parameter 'serverGroupToAgentInfo' element 'AgentID' is invalid");
		return ERR_PARAM;
	}

	# Check if server group to agent exists
	my $num_results = DBSelectNumResults("FROM soap_server_group_to_agent WHERE ServerGroupID = ".
			DBQuote($serverGroupToAgentInfo->{'ServerGroupID'})."
			AND AgentID = ".DBQuote($serverGroupToAgentInfo->{'AgentID'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Server group '".$serverGroupToAgentInfo->{'ServerGroupID'}."' already added to agent '".
				$serverGroupToAgentInfo->{'AgentID'}."'");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert group
	my $sth = DBDo("
			INSERT INTO soap_server_group_to_agent
				(ServerGroupID, AgentID)
			VALUES (".
					DBQuote($serverGroupToAgentInfo->{'ServerGroupID'}).",".
					DBQuote($serverGroupToAgentInfo->{'AgentID'}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID('soap_server_group_to_agent','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method removeServerGroupToAgent($serverGroupToAgentID)
# Remove a server group from an agent
#
# @param serverGroupToAgentID Server group to agent ID
#
# @return 0 on success, < 0 on error
sub removeServerGroupToAgent
{
	my $serverGroupToAgentID = shift;


	# Check params
	if (!defined($serverGroupToAgentID)) {
		setError("Parameter 'serverGroupToAgentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($serverGroupToAgentID = isNumber($serverGroupToAgentID))) {
		setError("Parameter 'serverGroupToAgentID' is invalid");
		return ERR_PARAM;
	}

	# Check if topup exists
	my $num_results = DBSelectNumResults("FROM soap_server_group_to_agent WHERE ID = ".DBQuote($serverGroupToAgentID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("Server group to agent '$serverGroupToAgentID' does not exist");
		return ERR_EXISTS;
	}

	DBBegin();

	# Remove server group from agent
	my $sth = DBDo("DELETE FROM soap_server_group_to_agent WHERE ID = ".DBQuote($serverGroupToAgentID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
