# Users To API Groups functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::UsersToAPIGroups
# Users to api groups handling functions
package smlib::Admin::UsersToAPIGroups;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getUsersToAPIGroups($userID,$search)
# Return list of SOAP users to api groups
#
# @param userID User ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Users to groups ID
# @li UserID - User ID
# @li APIGroupID - API Group ID
# @li APIGroupName - API Group name
# @li APIGroupDescription - API Group description
sub getUsersToAPIGroups 
{
	my ($userID,$search) = @_;


	if (!defined($userID)) {
		setError("Parameter 'userID' not defined");
		return ERR_PARAM;
	}
	if (!defined($userID = isNumber($userID))) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'UserID' => 'UserID',
		'APIGroupID' => 'APIGroupID',
		'APIGroupName' => 'APIGroupName',
		'APIGroupDescription' => 'APIGroupDescription',
	};

	# Query group for group list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				soap_user_to_api_group.ID,
				soap_user_to_api_group.UserID,
				soap_user_to_api_group.APIGroupID,
				soap_api_groups.GroupName AS APIGroupName,
				soap_api_groups.Description AS APIGroupDescription
			FROM
				soap_api_groups, soap_user_to_api_group
			WHERE
				soap_api_groups.ID = soap_user_to_api_group.APIGroupID
				AND soap_user_to_api_group.UserID = ".DBQuote($userID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @ugroups = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID UserID APIGroupID APIGroupName APIGroupDescription
				)
			)
	) {
		my $ugroup;

		$ugroup->{'ID'} = $row->{'ID'};
		$ugroup->{'UserID'} = $row->{'UserID'};
		$ugroup->{'APIGroupID'} = $row->{'APIGroupID'};
		$ugroup->{'APIGroupName'} = $row->{'APIGroupName'};
		$ugroup->{'APIGroupDescription'} = $row->{'APIGroupDescription'};

		push(@ugroups,$ugroup);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@ugroups,$numResults);
}



## @method addUserToAPIGroup($usersToAPIGroupsInfo)
# Add user to group
#
# @param usersToAPIGroupsInfo User to group info hash ref
# @li UserID - User ID
# @li APIGroupID - APIGroup ID
#
# @return User to group ID
sub addUserToAPIGroup
{
	my $usersToAPIGroupsInfo = shift;

	# Validatate hash
	if (!defined($usersToAPIGroupsInfo)) {
		setError("Parameter 'usersToAPIGroupsInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($usersToAPIGroupsInfo)) {
		setError("Parameter 'usersToAPIGroupsInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($usersToAPIGroupsInfo->{"UserID"})) {
		setError("Parameter 'usersToAPIGroupsInfo' has no element 'UserID'");
		return ERR_PARAM;
	}
	if (!defined($usersToAPIGroupsInfo->{'UserID'} = isNumber($usersToAPIGroupsInfo->{'UserID'}))) {
		setError("Parameter 'usersToAPIGroupsInfo' element 'UserID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($usersToAPIGroupsInfo->{"APIGroupID"})) {
		setError("Parameter 'usersToAPIGroupsInfo' has no element 'APIGroupID'");
		return ERR_PARAM;
	}
	if (!defined($usersToAPIGroupsInfo->{'APIGroupID'} = isNumber($usersToAPIGroupsInfo->{'APIGroupID'}))) {
		setError("Parameter 'usersToAPIGroupsInfo' element 'APIGroupID' is invalid");
		return ERR_PARAM;
	}

	# Check if user is already added
	my $num_results = DBSelectNumResults("
			FROM
				soap_user_to_api_group
			WHERE
				UserID = ".DBQuote($usersToAPIGroupsInfo->{'UserID'})."
			AND
				APIGroupID = ".DBQuote($usersToAPIGroupsInfo->{'APIGroupID'})
	);
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("User to API group '".$usersToAPIGroupsInfo->{'APIGroupID'}."' already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert user to api group
	my $sth = DBDo("
			INSERT INTO soap_user_to_api_group
				(UserID, APIGroupID)
			VALUES (".
					DBQuote($usersToAPIGroupsInfo->{'UserID'}).",".
					DBQuote($usersToAPIGroupsInfo->{'APIGroupID'}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID =DBLastInsertID('soap_user_to_api_group','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method removeUserToAPIGroup($usersToAPIGroupsID)
# Remove a user from an API group
#
# @param usersToAPIGroupsID User group ID
#
# @return 0 on success, < 0 on error
sub removeUserToAPIGroup
{
	my $usersToAPIGroupsID = shift;


	# Check params
	if (!defined($usersToAPIGroupsID)) {
		setError("Parameter 'usersToAPIGroupsID' not defined");
		return ERR_PARAM;
	}
	if (!defined($usersToAPIGroupsID = isNumber($usersToAPIGroupsID))) {
		setError("Parameter 'usersToAPIGroupsID' is invalid");
		return ERR_PARAM;
	}

	# Check if user to api group exists
	my $num_results = DBSelectNumResults("FROM soap_user_to_api_group WHERE ID = ".DBQuote($usersToAPIGroupsID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("User to API group ID '$usersToAPIGroupsID' does not exist");
		return ERR_EXISTS;
	}

	DBBegin();

	# Remove user from api group
	my $sth = DBDo("DELETE FROM soap_user_to_api_group WHERE ID = ".DBQuote($usersToAPIGroupsID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
