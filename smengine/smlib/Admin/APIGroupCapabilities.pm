# API Group capability functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::APIGroupCapabilities
# API Group capability handling functions
package smlib::Admin::APIGroupCapabilities;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getAPIGroupCapabilities($APIGroupID,$search)
# Return list of SOAP API Group Capabilities
#
# @param APIGroupID - API Group ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - API Group Capability ID
# @li Capability - API Group Capability
sub getAPIGroupCapabilities 
{
	my ($APIGroupID,$search) = @_;


	# Check params
	if (!defined($APIGroupID)) {
		setError("Parameter 'APIGroupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($APIGroupID = isNumber($APIGroupID))) {
		setError("Parameter 'APIGroupID' is invalid");
		return ERR_PARAM;
	}
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Capability' => 'Capability'
	};

	# Query api group for group capabilities
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				Capability
			FROM
				soap_api_capabilities
			WHERE
				GroupID = ".DBQuote($APIGroupID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @APIGroupCapabilities = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID Capability
				)
			)
	) {
		my $APIGroupCapability;

		$APIGroupCapability->{'ID'} = $row->{'ID'};
		$APIGroupCapability->{'Capability'} = $row->{'Capability'};

		push(@APIGroupCapabilities,$APIGroupCapability);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@APIGroupCapabilities,$numResults);
}



## @method getAPIGroupCapability($capabilityID)
# Return API group capability info hash
#
# @param capabilityID API Group ID
#
# @return API Group Capability info hash ref
# @li ID - API Group Capability ID
# @li Capability - API Group Capability
sub getAPIGroupCapability
{
	my $capabilityID = shift;


	if (!defined($capabilityID)) {
		setError("Parameter 'capabilityID' not defined");
		return ERR_PARAM;
	}
	if (!defined($capabilityID = isNumber($capabilityID))) {
		setError("Parameter 'capabilityID' is invalid");
		return ERR_PARAM;
	}

	# Query site
	my $sth = DBSelect("
		SELECT
			ID, Capability
		FROM
			soap_api_capabilities
		WHERE
			ID = ".DBQuote($capabilityID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw(
				ID Capability
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Capability'} = $row->{'Capability'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method createAPIGroupCapability($APIGroupCapabilityInfo)
# Create a API group capability
#
# @param APIGroupCapabilityInfo Capability info hash ref
# @li APIGroupID - API Group ID
# @li Capability - API Group capability
#
# @return API Group Capability ID
sub addAPIGroupCapability
{
	my $APIGroupCapabilityInfo = shift;

	# Validatate hash
	if (!defined($APIGroupCapabilityInfo)) {
		setError("Parameter 'APIGroupCapabilityInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($APIGroupCapabilityInfo)) {
		setError("Parameter 'APIGroupCapabilityInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($APIGroupCapabilityInfo->{"APIGroupID"})) {
		setError("Parameter 'APIGroupCapabilityInfo' has no element 'APIGroupID'");
		return ERR_PARAM;
	}
	if (!defined($APIGroupCapabilityInfo->{'APIGroupID'} = isNumber($APIGroupCapabilityInfo->{"APIGroupID"}))) {
		setError("Parameter 'APIGroupCapabilityInfo' element 'APIGroupID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($APIGroupCapabilityInfo->{"Capability"})) {
		setError("Parameter 'APIGroupCapabilityInfo' has no element 'Capability'");
		return ERR_PARAM;
	}
	if (!isVariable($APIGroupCapabilityInfo->{"Capability"})) {
		setError("Parameter 'APIGroupCapabilityInfo' element 'Capability' is invalid");
		return ERR_PARAM;
	}
	if ($APIGroupCapabilityInfo->{"Capability"} eq "") {
		setError("Parameter 'APIGroupCapabilityInfo' element 'Capability' is blank");
		return ERR_PARAM;
	}

	# Check if capability exists
	my $num_results = DBSelectNumResults("FROM soap_api_capabilities WHERE GroupID = ".DBQuote($APIGroupCapabilityInfo->{'APIGroupID'})." AND Capability = ".DBQuote($APIGroupCapabilityInfo->{'Capability'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Capability '".$APIGroupCapabilityInfo->{'Capability'}."' already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert group
	my $sth = DBDo("
			INSERT INTO soap_api_capabilities
				(GroupID, Capability)
			VALUES (".
					DBQuote($APIGroupCapabilityInfo->{'APIGroupID'}).",".
					DBQuote($APIGroupCapabilityInfo->{"Capability"}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID('soap_api_capabilities','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateAPIGroupCapability($apiGroupCapabilityInfo)
# Update API Group Capability
#
# @param apiGroupCapabilityInfo API Group Capability info hash ref
# @li ID - API Group Capability ID
# @li Capability - API Group Capability
#
# @return 0 on success, < 0 on error
sub updateAPIGroupCapability
{
	my $APIGroupCapabilityInfo = shift;

	if (!defined($APIGroupCapabilityInfo)) {
		setError("Parameter 'APIGroupCapabilityInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($APIGroupCapabilityInfo)) {
		setError("Parameter 'APIGroupCapabilityInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($APIGroupCapabilityInfo->{'ID'})) {
		setError("Parameter 'APIGroupCapabilityInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($APIGroupCapabilityInfo->{'ID'} = isNumber($APIGroupCapabilityInfo->{'ID'}))) {
		setError("Parameter 'APIGroupCapabilityInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	my @updates = ();

	if (defined($APIGroupCapabilityInfo->{'Capability'})) {
		if (!isVariable($APIGroupCapabilityInfo->{"Capability"})) {
			setError("Parameter 'APIGroupCapabilityInfo' element 'Capability' is invalid");
			return ERR_PARAM;
		}
		push(@updates,"Capability = ".DBQuote($APIGroupCapabilityInfo->{'Capability'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for capability");
		return ERR_USAGE;
	}

	# Grab capability to see if it exists
	my $capability = getAPIGroupCapability($APIGroupCapabilityInfo->{'ID'});
	# We already have error set, so return
	if (!isHash($capability)) {
		return $capability;
	}

	DBBegin();

	# Update quotas
	my $sth = DBDo("
			UPDATE
				soap_api_capabilities
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($APIGroupCapabilityInfo->{'ID'})
	);

	# Check for errors
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeAPIGroupCapability($capabilityID)
# Remove a group API capability
#
# @param capabilityID Group API Capability ID
#
# @return 0 on success, < 0 on error
sub removeAPIGroupCapability
{
	my $capabilityID = shift;


	# Check params
	if (!defined($capabilityID)) {
		setError("Parameter 'capabilityID' not defined");
		return ERR_PARAM;
	}
	if (!defined($capabilityID = isNumber($capabilityID))) {
		setError("Parameter 'capabilityID' is invalid");
		return ERR_PARAM;
	}

	DBBegin();

	# Remove API group capability
	my $sth = DBDo("DELETE FROM soap_api_capabilities WHERE ID = ".DBQuote($capabilityID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}


## @method getAllCapabilities
# Return an array of zones
#
# @return Array ref of hash refs
# @li Capability - Capability
sub getAllCapabilities
{
	my @res;

	# Build result array
	my $capabilityList = Auth::aclGetCapabilities();
	# Just do a sort
	foreach my $capability (sort @{$capabilityList}) {
		my $entry;
		$entry->{'Capability'} = $capability;
		push(@res,$entry);
	}

	return (\@res,length(@res));
}



1;
# vim: ts=4
