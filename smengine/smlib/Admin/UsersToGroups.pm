# Users To Groups functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::UsersToGroups
# Users to groups handling functions
package smlib::Admin::UsersToGroups;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getUsersToGroups($userID,$search)
# Return list of SOAP users to groups
#
# @param userID User ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Users to groups ID
# @li UserID - User ID
# @li GroupID - Group ID
# @li GroupName - Group name
# @li GroupDescription - Group description
sub getUsersToGroups 
{
	my ($userID,$search) = @_;


	if (!defined($userID)) {
		setError("Parameter 'userID' not defined");
		return ERR_PARAM;
	}
	if (!defined($userID = isNumber($userID))) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'UserID' => 'UserID',
		'GroupID' => 'GroupID',
		'GroupName' => 'GroupName',
		'GroupDescription' => 'GroupDescription',
	};

	# Query group for group list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				soap_user_to_group.ID,
				soap_user_to_group.UserID,
				soap_user_to_group.UserGroupID AS GroupID,
				soap_groups.GroupName,
				soap_groups.Description
			FROM
				soap_groups, soap_user_to_group
			WHERE
				soap_groups.ID = soap_user_to_group.UserGroupID
				AND soap_user_to_group.UserID = ".DBQuote($userID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @ugroups = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID UserID GroupID GroupName GroupDescription
				)
			)
	) {
		my $ugroup;

		$ugroup->{'ID'} = $row->{'ID'};
		$ugroup->{'UserID'} = $row->{'UserID'};
		$ugroup->{'GroupID'} = $row->{'GroupID'};
		$ugroup->{'GroupName'} = $row->{'GroupName'};
		$ugroup->{'GroupDescription'} = $row->{'GroupDescription'};

		push(@ugroups,$ugroup);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@ugroups,$numResults);
}



## @method addUserToGroup($usersToGroupsInfo)
# Add user to group
#
# @param usersToGroupsInfo User to group info hash ref
# @li UserID - User ID
# @li GroupID - Group ID
#
# @return User to group ID
sub addUserToGroup
{
	my $usersToGroupsInfo = shift;

	# Validatate hash
	if (!defined($usersToGroupsInfo)) {
		setError("Parameter 'usersToGroupsInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($usersToGroupsInfo)) {
		setError("Parameter 'usersToGroupsInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($usersToGroupsInfo->{"UserID"})) {
		setError("Parameter 'usersToGroupsInfo' has no element 'UserID'");
		return ERR_PARAM;
	}
	if (!defined($usersToGroupsInfo->{'UserID'} = isNumber($usersToGroupsInfo->{'UserID'}))) {
		setError("Parameter 'usersToGroupsInfo' element 'UserID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($usersToGroupsInfo->{"GroupID"})) {
		setError("Parameter 'usersToGroupsInfo' has no element 'GroupID'");
		return ERR_PARAM;
	}
	if (!defined($usersToGroupsInfo->{'GroupID'} = isNumber($usersToGroupsInfo->{'GroupID'}))) {
		setError("Parameter 'usersToGroupsInfo' element 'GroupID' is invalid");
		return ERR_PARAM;
	}

	# Check if user to group association exists
	my $num_results = DBSelectNumResults("FROM soap_user_to_group WHERE UserID = ".DBQuote($usersToGroupsInfo->{'UserID'})." AND userGroupID = ".DBQuote($usersToGroupsInfo->{'GroupID'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("User '".$usersToGroupsInfo->{'UserID'}."' to group '".$usersToGroupsInfo->{'GroupID'}."' association already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert group
	my $sth = DBDo("
			INSERT INTO soap_user_to_group
				(UserID, userGroupID)
			VALUES (".
					DBQuote($usersToGroupsInfo->{'UserID'}).",".
					DBQuote($usersToGroupsInfo->{'GroupID'}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID =DBLastInsertID('soap_user_to_group','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method removeUserToGroup($usersToGroupsID)
# Remove a user group
#
# @param usersToGroupsID User group ID
#
# @return 0 on success, < 0 on error
sub removeUserToGroup
{
	my $usersToGroupsID = shift;


	# Check params
	if (!defined($usersToGroupsID)) {
		setError("Parameter 'usersToGroupsID' not defined");
		return ERR_PARAM;
	}
	if (!defined($usersToGroupsID = isNumber($usersToGroupsID))) {
		setError("Parameter 'usersToGroupsID' is invalid");
		return ERR_PARAM;
	}

	# Check if topup exists
	my $num_results = DBSelectNumResults("FROM soap_user_to_group WHERE ID = ".DBQuote($usersToGroupsID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("User to group ID '$usersToGroupsID' does not exist");
		return ERR_EXISTS;
	}

	DBBegin();

	# Remove user from group
	my $sth = DBDo("DELETE FROM soap_user_to_group WHERE ID = ".DBQuote($usersToGroupsID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
