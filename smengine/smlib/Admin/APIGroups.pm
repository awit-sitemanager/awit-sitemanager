# API Group management functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::Admin::APIGroups
# API Group handling functions
package smlib::Admin::APIGroups;

use strict;
use warnings;

use smlib::config;
use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::util;



## @method getAPIGroups($search)
# Return list of SOAP API Groups
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - API Group ID
# @li Name - API Group name
# @li Description - API Group description
sub getAPIGroups 
{
	my ($search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Name' => 'Name',
		'Description' => 'Description',
	};

	# Query api groups for group list
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID,
				GroupName AS Name,
				Description
			FROM
				soap_api_groups
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @APIGroups = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
						ID Name Description
				)
			)
	) {
		my $APIGroup;

		$APIGroup->{'ID'} = $row->{'ID'};
		$APIGroup->{'Name'} = $row->{'Name'};
		$APIGroup->{'Description'} = $row->{'Description'};

		push(@APIGroups,$APIGroup);

	}

	# We done with this result set
	DBFreeRes($sth);

	return (\@APIGroups,$numResults);
}



## @method getAPIGroups($APIGroupID)
# Return API group info hash
#
# @param APIGroupID API Group ID
#
# @return API Group info hash ref
# @li ID - API Group ID
# @li Name - API Group Name
# @li Description - API Group description
sub getAPIGroup
{
	my $APIGroupID = shift;


	if (!defined($APIGroupID)) {
		setError("Parameter 'APIGroupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($APIGroupID = isNumber($APIGroupID))) {
		setError("Parameter 'APIGroupID' is invalid");
		return ERR_PARAM;
	}

	# Query site
	my $sth = DBSelect("
		SELECT
			ID, GroupName AS Name, Description
		FROM
			soap_api_groups
		WHERE
			ID = ".DBQuote($APIGroupID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw(
				ID Name Description
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Name'} = $row->{'Name'};
	$res->{'Description'} = $row->{'Description'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method createAPIGroup($APIGroupInfo)
# Create a API group
#
# @param APIGroupInfo Agent info hash ref
# @li Name - API Group name
# @li Description - API Group description
#
# @return API Group ID
sub createAPIGroup
{
	my $APIGroupInfo = shift;

	# Validatate hash
	if (!defined($APIGroupInfo)) {
		setError("Parameter 'APIGroupInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($APIGroupInfo)) {
		setError("Parameter 'APIGroupInfo' is not a HASH");
		return ERR_PARAM;
	}

	if (!defined($APIGroupInfo->{"Name"})) {
		setError("Parameter 'APIGroupInfo' has no element 'Name'");
		return ERR_PARAM;
	}
	if (!isVariable($APIGroupInfo->{"Name"})) {
		setError("Parameter 'APIGroupInfo' element 'Name' is invalid");
		return ERR_PARAM;
	}
	if ($APIGroupInfo->{"Name"} eq "") {
		setError("Parameter 'APIGroupInfo' element 'Name' is blank");
		return ERR_PARAM;
	}

	if (!defined($APIGroupInfo->{"Description"})) {
		setError("Parameter 'APIGroupInfo' has no element 'Description'");
		return ERR_PARAM;
	}
	if (!isVariable($APIGroupInfo->{"Description"})) {
		setError("Parameter 'APIGroupInfo' element 'Description' is invalid");
		return ERR_PARAM;
	}
	if ($APIGroupInfo->{"Description"} eq "") {
		setError("Parameter 'APIGroupInfo' element 'Description' is blank");
		return ERR_PARAM;
	}

	# Check if group exists
	my $num_results = DBSelectNumResults("FROM soap_api_groups WHERE GroupName = ".DBQuote($APIGroupInfo->{'Name'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Group '".$APIGroupInfo->{'Name'}."' already exists in database");
		return ERR_EXISTS;
	}

	DBBegin();

	# Insert group
	my $sth = DBDo("
			INSERT INTO soap_api_groups
				(GroupName, Description)
			VALUES (".
					DBQuote($APIGroupInfo->{'Name'}).",".
					DBQuote($APIGroupInfo->{"Description"}).
				")
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID =DBLastInsertID('soap_api_groups','ID');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method updateGroup($APIGroupInfo)
# Update group
#
# @param APIGroupInfo Group info hash ref
# @li ID - API Group ID
# @li Name - API Group Name
# @li Description - Optional description
#
# @return 0 on success, < 0 on error
sub updateAPIGroup
{
	my $APIGroupInfo = shift;

	if (!defined($APIGroupInfo)) {
		setError("Parameter 'APIGroupInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($APIGroupInfo)) {
		setError("Parameter 'APIGroupInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($APIGroupInfo->{'ID'})) {
		setError("Parameter 'APIGroupInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($APIGroupInfo->{'ID'} = isNumber($APIGroupInfo->{'ID'}))) {
		setError("Parameter 'APIGroupInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	my @updates = ();

	if (defined($APIGroupInfo->{'Description'})) {
		if (!isVariable($APIGroupInfo->{"Description"})) {
			setError("Parameter 'APIGroupInfo' element 'Description' is invalid");
			return ERR_PARAM;
		}
		if ($APIGroupInfo->{"Description"} eq "") {
			setError("Parameter 'APIGroupInfo' element 'Description' is blank");
			return ERR_PARAM;
		}
		push(@updates,"Description = ".DBQuote($APIGroupInfo->{'Description'}));
	}

	if (defined($APIGroupInfo->{'Name'})) {
		if (!isVariable($APIGroupInfo->{"Name"})) {
			setError("Parameter 'APIGroupInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
		if ($APIGroupInfo->{"Name"} eq "") {
			setError("Parameter 'APIGroupInfo' element 'Name' is blank");
			return ERR_PARAM;
		}
		push(@updates,"GroupName = ".DBQuote($APIGroupInfo->{'Name'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for group");
		return ERR_USAGE;
	}

	# Grab group to see if he exists
	my $APIGroup = getAPIGroup($APIGroupInfo->{'ID'});
	# We already have error set, so return
	if (!isHash($APIGroup)) {
		return $APIGroup;
	}

	DBBegin();

	# Update quotas
	my $sth = DBDo("
			UPDATE
				soap_api_groups
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($APIGroupInfo->{'ID'})
	);

	# Check for errors
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeAPIGroup($APIGroupID)
# Remove an API Group
#
# @param APIGroupID API group ID
#
# @return 0 on success, < 0 on error
sub removeAPIGroup
{
	my $APIGroupID = shift;


	# Check params
	if (!defined($APIGroupID)) {
		setError("Parameter 'APIGroupID' not defined");
		return ERR_PARAM;
	}
	if (!defined($APIGroupID = isNumber($APIGroupID))) {
		setError("Parameter 'APIGroupID' is invalid");
		return ERR_PARAM;
	}

	# Check if API group exists
	my $num_results = DBSelectNumResults("FROM soap_api_groups WHERE ID = ".DBQuote($APIGroupID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("API Group ID '$APIGroupID' does not exist");
		return ERR_EXISTS;
	}

	DBBegin();

	# Remove users from api group
	my $sth = DBDo("DELETE FROM soap_user_to_api_group WHERE APIGroupID = ".DBQuote($APIGroupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove groups from api group
	$sth = DBDo("DELETE FROM soap_group_to_api_group WHERE APIGroupID = ".DBQuote($APIGroupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove api group capabilities
	$sth = DBDo("DELETE FROM soap_api_capabilities WHERE GroupID = ".DBQuote($APIGroupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove api group
	$sth = DBDo("DELETE FROM soap_api_groups WHERE ID = ".DBQuote($APIGroupID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



1;
# vim: ts=4
