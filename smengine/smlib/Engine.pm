# Engine functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

## @class smlib::Engine
# This is the main engine class for SMEngine
package smlib::Engine;


use strict;
use warnings;


use awitpt::db::dblayer;
use awitpt::cache;
use smlib::logging;


## @fn getCacheStats
# Return cache engine stats
#
# @return Hash ref
# @li Hits - Cache hits
# @li Misses - Cache misses
sub getCacheStats
{
	my $data;

	$data->{'Hits'} = awitpt::cache::getCacheHits();
	$data->{'Misses'} = awitpt::cache::getCacheMisses();

	return $data;
}



1;
# vim: ts=4
