# User functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::users
# User handling functions
package smlib::users;


use strict;
use warnings;


use smlib::attributes;
use smlib::constants;
use smlib::config;
use smlib::logging;
use awitpt::db::dblayer;
use smlib::system qw(hashPassword);


## @fn authenticate($username,$password)
# Function to authenticate a user
#
# @param username Username
# @param password Password
sub authenticate
{
	my ($username,$password) = @_;
	my $res = -1;


	if (!defined($username)) {
		smlib::logging::log(LOG_ERR,"Parameter 'username' is not defined");
		return ERR_PARAM;
	}
	if ($username eq "") {
		smlib::logging::log(LOG_ERR,"Parameter 'username' is blank");
		return ERR_PARAM;
	}
	if (!defined($password)) {
		smlib::logging::log(LOG_ERR,"Parameter 'password' is not defined");
		return ERR_PARAM;
	}
	if ($password eq "") {
		smlib::logging::log(LOG_ERR,"Parameter 'password' is blank");
		return ERR_PARAM;
	}

	# Select user details from database
	my $sth = DBSelect("
			SELECT ID, Password
				FROM soap_users
			WHERE
				Username = ".DBQuote($username)."
				AND Disabled = 0
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows != 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result & check if we got something
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID Password ));

	# Equal both hashes
	my $cpass = hashPassword($password);
	(my $dpass = $row->{'Password'}) =~ s/^\{(\S+)\}/{\U$1}/;

	# Compare
	if ($cpass eq $dpass) {
		$res = $row->{'ID'};
	}
#	smlib::logging::log(LOG_DEBUG,"Client password '$cpass' vs. '$dpass'");

	DBFreeRes($sth);

	return $res;
}




## @fn getAPICapabilities($userID)
# Function to get capabilities of a user
#
# @param userID User ID
#
# @return Array ref of capabilities
sub getAPICapabilities
{
	my ($userID) = @_;


	if (!defined($userID)) {
		setError("Parameter 'userID' is not defined");
		return ERR_PARAM;
	}
	if ($userID < 1) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Capability list
	my %capabilities;


	# Select group capabilities
	# soap_users => soap_user_to_api_group => soap_api_capabilities
	my $sth = DBSelect("
			SELECT soap_api_capabilities.Capability
				FROM soap_users, soap_user_to_api_group, soap_api_capabilities
			WHERE
				soap_users.ID = ".DBQuote($userID)."
				AND soap_user_to_api_group.UserID = soap_users.ID
				AND soap_api_capabilities.GroupID = soap_user_to_api_group.APIGroupID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}
	# Grab results
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Capability ))) {
		$capabilities{$row->{'Capability'}} = 1;
	}

	# Select user capabilities
	# soap_users => soap_user_to_group => soap_group_to_api_group => soap_api_capabilities
	$sth = DBSelect("
			SELECT soap_api_capabilities.Capability
				FROM soap_users, soap_user_to_group, soap_group_to_api_group, soap_api_capabilities
			WHERE
				soap_users.ID = ".DBQuote($userID)."
				AND soap_user_to_group.UserID = soap_users.ID
				AND soap_group_to_api_group.UserGroupID = soap_user_to_group.UserGroupID
				AND soap_api_capabilities.GroupID = soap_group_to_api_group.APIGroupID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}
	# Grab results
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Capability ))) {
		$capabilities{$row->{'Capability'}} = 1;
	}

	DBFreeRes($sth);

	# Generate sorted keys
	my @capabilities = sort keys %capabilities;

	return \@capabilities;
}


## @fn getUserAttributes($userID)
# Function to get usre attributes
#
# @param userID User ID
#
# @return Array ref of capabilities
sub getUserAttributes
{
	my ($userID) = @_;
	my %attributes = ();


	if (!defined($userID)) {
		setError("Parameter 'userID' is not defined");
		return ERR_PARAM;
	}
	if ($userID < 1) {
		setError("Parameter 'userID' is invalid");
		return ERR_PARAM;
	}

	# Select user attributes from database
	# soap_users => soap_groups => soap_group_attributes
	my $sth = DBSelect("
			SELECT
				soap_group_attributes.Attribute
			FROM
				soap_user_to_group, soap_group_attributes
			WHERE
				soap_user_to_group.UserID = ".DBQuote($userID)."
				AND soap_group_attributes.UserGroupID = soap_user_to_group.UserGroupID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Grab group results
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Attribute ))) {
		my ($item,$operator,$value) = ($row->{'Attribute'} =~ /^([[:alnum:]]+)([\+]?=)([[:print:]]+)$/);

		$attributes{$item} = smlib::attributes::processAttribute($attributes{$item},$operator,$value);
	}
	DBFreeRes($sth);

	# Select user attributes from database
	$sth = DBSelect("
			SELECT
				Attribute
			FROM
				soap_user_attributes
			WHERE
				UserID = ".DBQuote($userID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Grab user results
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Attribute ))) {
		my ($item,$operator,$value) = ($row->{'Attribute'} =~ /^([[:alnum:]]+)([\+]?=)([[:print:]]+)$/);

		$attributes{$item} = smlib::attributes::processAttribute($attributes{$item},$operator,$value);
	}
	DBFreeRes($sth);
	
	return \%attributes;
}




1;
# vim: ts=4
