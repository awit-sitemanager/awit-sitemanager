# Mail functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class smlib::MailHosting
# Class of mailbox handling functions
package smlib::MailHosting;


use strict;
use warnings;

use smlib::config;
use smlib::constants;
use smlib::logging;
use awitpt::db::dblayer;
use smlib::system qw(hashPassword);
use smlib::util;

use Cwd qw(abs_path);
use File::Path qw(mkpath rmtree);


# Module config
my $config = {
	'VMailDir' => '/var/vmail',
	'VMailUser' => 'vmail',
	'VMailGroup' => 'vmail'
};

# Server handle
my $logger = sub { };

# Use the smlib servers autoloader so we can get remote calls working nicely
sub AUTOLOAD { our $AUTOLOAD; return smlib::servers::RPC_AUTOLOADER($AUTOLOAD,@_); }


## @internal _init($server)
# This function initializes the module when passed with a server object
sub _init
{
	my $server = shift;


	# See if we can pull in a vmail dir setting
	if (defined(my $vmailDir = $server->{'inifile'}->{'mailhosting'}->{'vmaildir'})) {
		$server->log(LOG_NOTICE,"    => smlib/MailHosting: Setting 'VMailDir' to '$vmailDir'");
		$config->{'VMailDir'} = $vmailDir;
    } else {
		$server->log(LOG_INFO,"    => smlib/MailHosting: Using default '".$config->{'VMailDir'}."' for 'VMailDir'");
	}

	# See if we can pull in a User setting
	if (defined(my $vmailUser = $server->{'inifile'}->{'mailhosting'}->{'vmailuser'})) {
		$server->log(LOG_NOTICE,"    => smlib/MailHosting: Setting 'VMailUser' to '$vmailUser'");
		$config->{'VMailUser'} = $vmailUser;
    } else {
		$server->log(LOG_INFO,"    => smlib/MailHosting: Using default '".$config->{'VMailUser'}."' for 'VMailUser'");
	}
	# Check if the user resolves
	if (!getpwnam($config->{'VMailUser'})) {
		$server->log(LOG_WARN,"    => smlib/MailHosting: Resetting 'VMailUser' to 'nobody' because '".
				$config->{'VMailUser'}."' does NOT exist!");
		$config->{'VMailUser'} = "nobody";
	}
	
	# See if we can pull in a Group setting
	if (defined(my $vmailGroup = $server->{'inifile'}->{'mailhosting'}->{'vmailgroup'})) {
		$server->log(LOG_NOTICE,"    => smlib/MailHosting: Setting 'VMailGroup' to '$vmailGroup'");
		$config->{'VMailGroup'} = $vmailGroup;
    } else {
		$server->log(LOG_INFO,"    => smlib/MailHosting: Using default '".$config->{'VMailGroup'}."' for 'VMailGroup'");
	}
	# Check if it resolves
	if (!getgrnam($config->{'VMailGroup'})) {
		$server->log(LOG_WARN,"    => smlib/MailHosting: Resetting 'VMailGroup' to 'nobody' because '".
				$config->{'VMailGroup'}."' does NOT exist!");
		$config->{'VMailGroup'} = "nobody";
	}

	# Setup logger
	$logger = sub { $server->log(@_) };

	return RES_OK;
}


## @method createMailTransport($transportInfo)
# Add a mail transport
#
# @param transportInfo Transport info hash ref
# @li AgentID - Agent ID
# @li DomainName - Domain name
# @li Transport - Transport for this domain
# @li Detail - Optional, if this transport requires data after the :, this is it   "transport:detail". For instance smtp:smtp.whereever.com
# @li TransportDetail - Optional, if this transport requires a RHS, this is it.  "transport:" => "TransportDetail"
# @li AgentRef - Optional agent reference
# @li AgentDisabled - Optional flag to disable this transport
#
# @return Mail transport ID
sub _createMailTransport
{
	my $transportInfo = shift;


	if (!defined($transportInfo)) {
		setError("Parameter 'transportInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($transportInfo)) {
		setError("Parameter 'transportInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{'AgentID'})) {
		setError("Parameter 'transportInfo' element 'AgentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{'AgentID'} = isNumber($transportInfo->{'AgentID'}))) {
		setError("Parameter 'transportInfo' element 'AgentID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{"DomainName"})) {
		setError("Parameter 'transportInfo' has no element 'DomainName'");
		return ERR_PARAM;
	}
	if (!isVariable($transportInfo->{"DomainName"})) {
		setError("Parameter 'transportInfo' element 'DomainName' is invalid");
		return ERR_PARAM;
	}
	if ($transportInfo->{"DomainName"} eq "") {
		setError("Parameter 'transportInfo' element 'DomainName' is blank");
		return ERR_PARAM;
	}
	$transportInfo->{'DomainName'} = lc($transportInfo->{'DomainName'});
	if (!($transportInfo->{'DomainName'} =~ /^[a-z0-9_\-\.]+$/)) {
		setError("Parameter 'transportInfo' element 'DomainName' is invalid");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{"Transport"})) {
		setError("Parameter 'transportInfo' has no element 'Transport'");
		return ERR_PARAM;
	}
	if (!isVariable($transportInfo->{"Transport"})) {
		setError("Parameter 'transportInfo' element 'Transport' is invalid");
		return ERR_PARAM;
	}
	if ($transportInfo->{"Transport"} eq "") {
		setError("Parameter 'transportInfo' element 'Transport' is blank");
		return ERR_PARAM;
	}

	# Check if mail transport exists
	my $num_results = DBSelectNumResults("FROM mail_transport WHERE DomainName = ".DBQuote($transportInfo->{'DomainName'}));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Mail transport '".$transportInfo->{'DomainName'}."' already exists");
		return ERR_EXISTS;
	}

	# Check transport is right
	if ($transportInfo->{'Transport'} eq "smtp") {
		# For SMTP we use detail field for the SMTP server
		if (!defined($transportInfo->{"Detail"})) {
			setError("Parameter 'transportInfo' requires the element 'Detail' to be defined when using 'smtp' transport");
			return ERR_USAGE;
		}
		if (!isVariable($transportInfo->{"Detail"})) {
			setError("Parameter 'transportInfo' element 'Detail' is invalid");
			return ERR_PARAM;
		}
		if ($transportInfo->{"Detail"} eq "") {
			setError("Parameter 'transportInfo' requires the element 'Detail' to be set when using 'smtp' transport");
			return ERR_USAGE;
		}
	# For virtual transport we don't use the Detail field
	} elsif ($transportInfo->{'Transport'} eq "virtual") {
		# For SMTP we use detail field for the SMTP server
		if (defined($transportInfo->{"Detail"}) && $transportInfo->{"Detail"} ne "") {
			setError("Parameter 'transportInfo' must not have the element 'Detail' set when using 'virtual' transport");
			return ERR_USAGE;
		}
		# Make sure its set and blank
		$transportInfo->{"Detail"} = "";

	} else {
		setError("Mail transport '".$transportInfo->{'Transport'}."' not supported");
		return ERR_USAGE;
	}

	if (defined($transportInfo->{'AgentRef'})) {
		if (!isVariable($transportInfo->{"AgentRef"})) {
			setError("Parameter 'transportInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($transportInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($transportInfo->{'AgentDisabled'})) {
			setError("Parameter 'transportInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
		# Check disabled flag is a boolean
		if (!defined($transportInfo->{'AgentDisabled'} = isBoolean($transportInfo->{'AgentDisabled'}))) {
			setError("Parameter 'transportInfo' element 'AgentDisabled' has an invalid value");
			return ERR_PARAM;
		}
	}

	my $extraCols = "";
	my $extraVals = "";

	if (defined($transportInfo->{'AgentRef'})) {
		if ($transportInfo->{'AgentRef'} eq " ") {
			$extraCols .= "AgentRef";
			$extraVals .= ",NULL";
		} else {
			$extraCols .= ",AgentRef";
			$extraVals .= ",".DBQuote($transportInfo->{'AgentRef'});
		}
	}

	if (defined($transportInfo->{'AgentDisabled'})) {
		$extraCols .= ",AgentDisabled";
		$extraVals .= ",".DBQuote($transportInfo->{'AgentDisabled'});
	}

	my $transport = $transportInfo->{'Transport'} . ":" . $transportInfo->{'Detail'};

	DBBegin();

	# Insert transport data
	my $sth = DBDo("
			INSERT INTO mail_transport
				(AgentID,DomainName,Transport$extraCols)
			VALUES (".
					DBQuote($transportInfo->{'AgentID'}).",".
					DBQuote($transportInfo->{'DomainName'}).",".
					DBQuote($transport)."
					$extraVals
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	my $ID = DBLastInsertID("mail_transport","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}


	DBCommit();
	return $ID;
}



## @method updateMailTransport($transportInfo)
# Update a mail transport
#
# @param transportInfo Hash ref with transport info
# @li ID - ID of mail transport
# @li Detail - LHS part of the transport, for instance   smtp:DETAIL
# @li MailstoreLocation - Location of mailstore on disk (PRIVATE) - mapped to TransportDetail
# @li TransportDetail - Additional transport details (PRIVATE)
# @li AgentRef - Optional agent reference
# @li AgentDisabled - Optional flag to disable this transport
#
# @return 0 on success, < 0 on error
sub _updateMailTransport
{
	my $transportInfo = shift;


	if (!defined($transportInfo)) {
		setError("Parameter 'transportInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($transportInfo)) {
		setError("Parameter 'transportInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{"ID"})) {
		setError("Parameter 'transportInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{'ID'} = isNumber($transportInfo->{'ID'}))) {
		setError("Parameter 'transportInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}

	if (defined($transportInfo->{"Detail"})) {
		if (!isVariable($transportInfo->{"Detail"})) {
			setError("Parameter 'transportInfo' element 'Detail' is invalid");
			return ERR_PARAM;
		}
		if ($transportInfo->{"Detail"} eq "") {
			setError("Parameter 'transportInfo' element 'Detail' is blank");
			return ERR_PARAM;
		}
	}

	if (defined($transportInfo->{"MailstoreLocation"})) {
		if (!isVariable($transportInfo->{"MailstoreLocation"})) {
			setError("Parameter 'transportInfo' element 'MailstoreLocation' is invalid");
			return ERR_PARAM;
		}
		if ($transportInfo->{"MailstoreLocation"} eq "") {
			setError("Parameter 'transportInfo' element 'MailstoreLocation' is blank");
			return ERR_PARAM;
		}
	}
	if (defined($transportInfo->{"TransportDetail"})) {
		if (!isVariable($transportInfo->{"TransportDetail"})) {
			setError("Parameter 'transportInfo' element 'TransportDetail' is invalid");
			return ERR_PARAM;
		}
		if ($transportInfo->{"TransportDetail"} eq "") {
			setError("Parameter 'transportInfo' element 'TransportDetail' is blank");
			return ERR_PARAM;
		}
	}
	if (defined($transportInfo->{"MailstoreLocation"}) && 
			defined($transportInfo->{"TransportDetail"})) {
		setError("Parameter 'transportInfo' element 'MailstoreLocation' and 'TransportDetail' cannot both be specified");
		return ERR_PARAM;
	}

	if (defined($transportInfo->{'AgentRef'})) {
		if (!isVariable($transportInfo->{"AgentRef"})) {
			setError("Parameter 'transportsInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($transportInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($transportInfo->{'AgentDisabled'})) {
			setError("Parameter 'transportInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
		if (!defined($transportInfo->{'AgentDisabled'} = isBoolean($transportInfo->{"AgentDisabled"}))) {
			setError("Parameter 'transportInfo' element 'AgentDisabled' has an invalid value");
			return ERR_PARAM;
		}
	}

	my $transport = _getMailTransport($transportInfo->{'ID'});
	if (!isHash($transport)) {
		return $transport;
	}

	# Updates we need to do
	my @updates = ();

	# Process MailstoreLocation & TransportDetail
	if (defined($transportInfo->{'MailstoreLocation'})) {
		if (!defined($transport->{'MailstoreLocation'}) || $transport->{'MailstoreLocation'} eq "") {
			push(@updates,"TransportDetail = ".DBQuote($transportInfo->{'MailstoreLocation'}));
		} else {
# NK: We should never need to blank this
#			# See if we blanking it or not
#			if ($transportInfo->{"MailstoreLocation"} eq " ") {
#				push(@updates,"TransportDetail = NULL");
#			} else {
				setError("Parameter 'transportInfo' element 'MailstoreLocation' cannot be changed if already set");
#			}
		}
	}
	if (defined($transportInfo->{'TransportDetail'})) {
		if (!defined($transport->{'MailstoreLocation'})) {
			push(@updates,"TransportDetail = ".DBQuote($transportInfo->{'TransportDetail'}));
		} else {
			setError("Parameter 'transportInfo' element 'TransportDetail' cannot be changed if already set");
		}
	}

	# Special treatment of 'Detail'
	if (defined($transportInfo->{'Detail'})) {
		if ($transport->{'Transport'} eq "smtp") {
			push(@updates,"Transport = ".DBQuote($transport->{'Transport'}.":".$transportInfo->{'Detail'}));
		} else {
			setError("Parameter 'transportInfo' element 'Detail' cannot be updated for transport '".$transport->{'Detail'}."'");
			return ERR_USAGE;
		}
	}

	if (defined($transportInfo->{'AgentRef'})) {
		if ($transportInfo->{'AgentRef'} eq " ") {
			push(@updates,"AgentRef = NULL");
		} else {
			push(@updates,"AgentRef = ".DBQuote($transportInfo->{'AgentRef'}));
		}
	}

	if (defined($transportInfo->{'AgentDisabled'})) {
		push(@updates,"AgentDisabled = ".DBQuote($transportInfo->{'AgentDisabled'}));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for transport");
		return ERR_USAGE;
	}

	DBBegin();

	# Update
	my $sth = DBDo("
			UPDATE
				mail_transport
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($transportInfo->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeMailTransport($transportID)
# Remove a transport
#
# @param transportID Transport ID
#
# @return 0 on success, < 0 on error
sub _removeMailTransport
{
	my $transportID = shift;


	if (!defined($transportID)) {
		setError("Parameter 'transportID' not defined");
		return ERR_PARAM;
	}
	if (!defined($transportID = isNumber($transportID))) {
		setError("Parameter 'transportID' is invalid");
		return ERR_PARAM;
	}

	# Grab mail transport, we need to see if we virtual below so we can remove the maildirs off disk
	my $transportInfo = _getMailTransport($transportID);
	# We already have error set, so return
	if (!isHash($transportInfo)) {
		return $transportInfo;
	}

	DBBegin();

	# Remove aliases
	my $sth = DBDo("DELETE FROM mail_aliases WHERE TransportID = ".DBQuote($transportID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove mailboxes
	$sth = DBDo("DELETE FROM mail_mailboxes WHERE TransportID = ".DBQuote($transportID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove from database
	$sth = DBDo("DELETE FROM mail_transport WHERE ID = ".DBQuote($transportID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method getMailTransports($transportInfo,$search)
# Return an array of transport hashes
#
# @param transportInfo Transport info hash ref
# @li AgentID - Optional agent ID
# @li TransportID - Optional TransportID or array ref of TransportID's
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Transport ID on the system
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li DomainName - Domain name of transport
# @li Transport - Transport for this domain
# @li Detail - Detail for this transport
# @li TransportDetail - Optional, if this transport requires data on the RHS of Transport
# @li MailstoreLocation - Optional, if this is a 'virtual' transport, this will be set (PRIVATE)
# @li AgentDisabled - Set if the agent has disabled this transport
# @li Disabled - True if transport is disabled
# @li AgentRef - %Agent reference
sub _getMailTransports
{
	my ($transportInfo,$search) = @_;

	my $extraSQL = "";

	# Check if we have an AgentID defined that its valid
	if (defined($transportInfo)) {
		# Make sure we're a hash
		if (!isHash($transportInfo)) {
			setError("Parameter 'transportInfo' is not a HASH");
			return ERR_PARAM;
		}
		# If we have an agent ID, use it
		if (defined($transportInfo->{'AgentID'})) {
			if (!defined($transportInfo->{'AgentID'} = isNumber($transportInfo->{'AgentID'}))) {
				setError("Parameter 'transportInfo' element 'AgentID'' is invalid");
				return ERR_PARAM;
			}
			$extraSQL .= " AND mail_transport.AgentID = ".DBQuote($transportInfo->{'AgentID'});
		}
		# Check if we have the transport ID(s)
		if (defined($transportInfo->{'TransportID'})) {
			# Check if its an array ref
			if (ref($transportInfo->{'TransportID'}) eq "ARRAY") {
				my @transportList;
				# Verify all TransportID's
				foreach my $transportID (@{$transportInfo->{'TransportID'}}) {
					# If not it can only be a number
					if (!defined($transportID = isNumber($transportID))) {
						setError("Parameter 'transportInfo' element 'TransportID' has invalid item");
						return ERR_PARAM;
					}
					# Add to our list
					push(@transportList,DBQuote($transportID));
				}
	
				$extraSQL .= " AND mail_transport.ID IN ( ".join(',',@transportList).")";

			} else {
				# If not it can only be a number
				if (!defined($transportInfo->{'TransportID'} = isNumber($transportInfo->{'TransportID'}))) {
					setError("Parameter 'transportInfo' element 'TransportID' is invalid");
					return ERR_PARAM;
				}
				$extraSQL .= " AND mail_transport.ID = ".DBQuote($transportInfo->{'TransportID'});
			}
		}
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'mail_transport.ID',
		'AgentID' => 'mail_transport.AgentID',
		'DomainName' => 'mail_transport.DomainName',
		'AgentRef' => 'mail_transport.AgentRef',
	};

	# Select transports
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				mail_transport.ID,
				mail_transport.AgentID,
				mail_transport.DomainName, 
				mail_transport.Transport, mail_transport.TransportDetail,
				mail_transport.AgentDisabled, mail_transport.DisableDelivery,
				mail_transport.AgentRef,
				agents.Name AS AgentName
			FROM
				mail_transport, agents
			WHERE
				agents.ID = mail_transport.AgentID
				$extraSQL
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @transports = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw( ID AgentID DomainName Transport AgentDisabled DisableDelivery AgentRef AgentName )
			)
	) {
		my $transport;

		$transport->{'ID'} = $row->{'ID'};
		$transport->{'AgentID'} = $row->{'AgentID'};
		$transport->{'AgentName'} = $row->{'AgentName'};
		$transport->{'DomainName'} = $row->{'DomainName'};
		($transport->{'Transport'},$transport->{'Detail'}) = split /:/, $row->{'Transport'};
		$transport->{'AgentDisabled'} = booleanize($row->{'AgentDisabled'});
		$transport->{'Disabled'} = $transport->{'AgentDisabled'};
		$transport->{'TransportDetail'} = $row->{'TransportDetail'};
		$transport->{'MailstoreLocation'} = ($transport->{'Transport'} eq 'virtual') ? $row->{'TransportDetail'} : "";
# FIXME - standardize
#		$transport->{'DisableDelivery'} = $row->{'DisableDelivery'};
		$transport->{'AgentRef'} = $row->{'AgentRef'};

		push(@transports,$transport);
	}

	DBFreeRes($sth);

	return (\@transports,$numResults);
}



## @method getMailTransport($transportID)
# Return mail transport record hash
#
# @param transportID Transport ID
#
# @return Hash ref containing transport info
# @li ID - ID of mail transport
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li DomainName - Domain name
# @li Transport - Transport for this domain
# @li Detail - Optional, if this transport requires data after the :
# @li TransportDetail - Optional, if this transport requires data on the RHS of Transport
# @li MailstoreLocation - Optional, if this is a 'virtual' transport, this will be set (PRIVATE)
# @li AgentRef - Optional agent reference
# @li AgentDisabled - Optional flag to disable this transport
# @li Disabled - Set if transport is disabled
sub _getMailTransport
{
	my $transportID = shift;


	if (!defined($transportID)) {
		setError("Parameter 'transportID' not defined");
		return ERR_PARAM;
	}
	if (!defined($transportID = isNumber($transportID))) {
		setError("Parameter 'transportID' is invalid");
		return ERR_PARAM;
	}

	# Query mailbox
	my $sth = DBSelect("
			SELECT
				mail_transport.ID,
				mail_transport.AgentID,
				mail_transport.DomainName,
				mail_transport.Transport, mail_transport.TransportDetail,
				mail_transport.AgentRef,
				mail_transport.AgentDisabled, mail_transport.DisableDelivery,
				agents.Name AS AgentName
			FROM
				mail_transport, agents
			WHERE
				mail_transport.ID = ".DBQuote($transportID)."
				AND agents.ID = mail_transport.AgentID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("Mail transport ID '$transportID' not found");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw(
				ID AgentID AgentName DomainName Transport TransportDetail AgentRef AgentDisabled DisableDelivery
				AgentName
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'AgentID'} = $row->{'AgentID'};
	$res->{'AgentName'} = $row->{'AgentName'};
	$res->{'DomainName'} = $row->{'DomainName'};
	($res->{'Transport'},$res->{'Detail'}) = split /:/, $row->{'Transport'};
	$res->{'TransportDetail'} = $row->{'TransportDetail'};
	$res->{'MailstoreLocation'} = ($res->{'Transport'} eq 'virtual') ? $res->{'TransportDetail'} : "";
# FIXME, what do we return here?
#	$res->{'PolicyID'} = $row->{'PolicyID'};
	$res->{'AgentDisabled'} = booleanize($row->{'AgentDisabled'});
	$res->{'Disabled'} = $res->{'AgentDisabled'};
# FIXME - standardize?
#	$res->{'DisableDelivery'} = $row->{'DisableDelivery'};
	$res->{'AgentRef'} = $row->{'AgentRef'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method recreateMailbox($mailboxID)
# Recreate a mailbox
#
# @param mailboxID Mailbox ID
#
# @return Mailbox ID
sub _recreateMailbox
{
	my $mailboxID = shift;


	if (!defined($mailboxID)) {
		setError("Parameter 'mailboxID' does not exist");
		return ERR_PARAM;
	}
	if (!defined($mailboxID = isNumber($mailboxID))) {
		setError("Parameter 'mailboxID' is invalid");
		return ERR_PARAM;
	}

#	print STDERR "MAILBOX RECREATE CALLED!!!\n\n";


	return $mailboxID;
}



## @method createMailbox($mailboxInfo)
# Add a mailbox
#
# @param mailboxInfo Mailbox info hash ref
# @li TransportID - Transport ID
# @li Address - Email address, minus the @@domain part
# @li Password - Mailbox password
# @li Quota - Optional mailbox quota, defaults to 5Mb
# @li AgentDisabled - Optional, mailbox disabled by agent
# @li Name - Optional, mailbox owner name
# @li AgentRef - Optional, agent reference
#
# @return Mailbox ID
sub _createMailbox
{
	my $mailboxInfo = shift;


	if (!defined($mailboxInfo)) {
		setError("Parameter 'mailboxInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($mailboxInfo)) {
		setError("Parameter 'mailboxInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'TransportID'})) {
		setError("Parameter 'mailboxInfo' has no element 'TransportID'");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'TransportID'} = isNumber($mailboxInfo->{'TransportID'}))) {
		setError("Parameter 'mailboxInfo' element 'TransportID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{"Address"})) {
		setError("Parameter 'mailboxInfo' has no element 'Address'");
		return ERR_PARAM;
	}
	if (!isVariable($mailboxInfo->{"Address"})) {
		setError("Parameter 'mailboxInfo' element 'Address' is invalid");
		return ERR_PARAM;
	}
	if ($mailboxInfo->{"Address"} eq "") {
		setError("Parameter 'mailboxInfo' element 'Address' is blank");
		return ERR_PARAM;
	}
	$mailboxInfo->{'Address'} = lc($mailboxInfo->{'Address'});
	# Check validity
	if (!($mailboxInfo->{'Address'} =~ /^[a-z0-9_\-\.]*$/)) {
		setError("Parameter 'mailboxInfo' element 'Address' is invalid");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{"Password"})) {
		setError("Parameter 'mailboxInfo' has no element 'Password'");
		return ERR_PARAM;
	}
	if (!isVariable($mailboxInfo->{"Password"})) {
		setError("Parameter 'mailboxInfo' element 'Password' is invalid");
		return ERR_PARAM;
	}
	if ($mailboxInfo->{"Password"} eq "") {
		setError("Parameter 'mailboxInfo' element 'Password' is blank");
		return ERR_PARAM;
	}
	if ((my $error = isPasswordInsecure($mailboxInfo->{"Password"},[$mailboxInfo->{'Address'}]))) {
		setError("Parameter 'mailboxInfo' element 'Password' is INSECURE: $error");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'Quota'})) {
		setError("Parameter 'mailboxInfo' has no element 'Quota' defined");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'Quota'} = isNumber($mailboxInfo->{'Quota'},ISNUMBER_ALLOW_ZERO))) {
		setError("Parameter 'mailboxInfo' element 'Quota' is invalid");
		return ERR_PARAM;
	}
	if (defined($mailboxInfo->{'Name'})) {
		# Check for non-variable
		if (!isVariable($mailboxInfo->{'Name'})) {
			setError("Parameter 'mailboxInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($mailboxInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($mailboxInfo->{'AgentDisabled'})) {
			setError("Parameter 'mailboxInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
		# Check disabled flag is a boolean
		if (!defined($mailboxInfo->{'AgentDisabled'} = isBoolean($mailboxInfo->{"AgentDisabled"}))) {
			setError("Parameter 'mailboxInfo' element 'AgentDisabled' has an invalid value");
			return ERR_PARAM;
		}
	}
	if (defined($mailboxInfo->{'AgentRef'})) {
		# Check for non-variable
		if (!isVariable($mailboxInfo->{'AgentRef'})) {
			setError("Parameter 'mailboxInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}

	# Try get the transport...
	my $transportInfo = _getMailTransport($mailboxInfo->{'TransportID'});
	if (!isHash($transportInfo)) {
		return $transportInfo;
	}

	if ($transportInfo->{'Transport'} ne "virtual") {
		setError("Can only add a mailbox to a virtual transport");
		return ERR_USAGE;
	}

	# Check if mailbox exists
	if (my $res = mailboxExists($mailboxInfo->{'TransportID'},$mailboxInfo->{'Address'})) {
		# Check result
		if ($res < 0) {
			return $res;
		} else {
			setError("Mailbox address already exists");
			return ERR_EXISTS;
		}
	}
	# Check if mailbox exists as an alias
	if (my $res = mailboxAliasExists($mailboxInfo->{'TransportID'},$mailboxInfo->{'Address'})) {
		# Check result
		if ($res < 0) {
			return $res;
		} else {
			setError("Mailbox address already exists as an alias");
			return ERR_CONFLICT;
		}
	}

	my $extraColumns = "";
	my $extraValues = "";

	# Check if we have extra columns
	my $quota;
	if (defined($mailboxInfo->{'Quota'})) {
		$extraColumns .= ",Quota";
		$extraValues .= ",".DBQuote($mailboxInfo->{'Quota'});
		# We do double the work so we can pass the quota to the backend
		$quota = $mailboxInfo->{'Quota'};
	}

	if (defined($mailboxInfo->{'Name'})) {
		$extraColumns .= ",Name";
		$extraValues .= ",".DBQuote($mailboxInfo->{'Name'});
	}

	if (defined($mailboxInfo->{'AgentRef'})) {
		if ($mailboxInfo->{'AgentRef'} eq " ") {
			$extraColumns .= ",AgentRef";
			$extraValues .= ",NULL";
		} else {
			$extraColumns .= ",AgentRef";
			$extraValues .= ",".DBQuote($mailboxInfo->{'AgentRef'});
		}
	}

# FIXME - policies?
#	if (defined($mailboxInfo->{'PremiumPolicy'})) {
#		$extraColumns .= ",PremiumPolicy";
#		$extraValues .= ",".DBQuote($mailboxInfo->{'PremiumPolicy'});
#	}
#
#	if (defined($mailboxInfo->{'PolicyID'})) {
#		$extraColumns .= ",PolicyID";
#
#		# Check if we must change or blank the policy
#		if ($mailboxInfo->{'PolicyID'} ne "" && $mailboxInfo->{'PolicyID'}) {
#			$extraValues .= ",".DBQuote($mailboxInfo->{'PolicyID'});
#		} else {
#			$extraValues .= ",NULL";
#		}
#	}
#
#	if (defined($mailboxInfo->{'PremiumSMTP'})) {
#		$extraColumns .= ",PremiumSMTP";
#		$extraValues .= ",".DBQuote($mailboxInfo->{'PremiumSMTP'});
#	}

	if (defined($mailboxInfo->{'AgentDisabled'})) {
		$extraColumns .= ",AgentDisabled";
		$extraValues .= ",".DBQuote($mailboxInfo->{'AgentDisabled'});
	}
# FIXME - standardize?
#	if (defined($mailboxInfo->{'DisableDelivery'})) {
#		$extraColumns .= ",DisableDelivery";
#		$extraValues .= ",".DBQuote($mailboxInfo->{'DisableDelivery'});
#	}
#
#	if (defined($mailboxInfo->{'DisableLogin'})) {
#		$extraColumns .= ",DisableLogin";
#		$extraValues .= ",".DBQuote($mailboxInfo->{'DisableDelivery'});
#	}
#
#	if (defined($mailboxInfo->{'DisableSASL'})) {
#		$extraColumns .= ",DisableSASL";
#		$extraValues .= ",".DBQuote($mailboxInfo->{'DisableSASL'});
#	}

	my $username = $mailboxInfo->{'Address'} . '@' . $transportInfo->{'DomainName'};

	DBBegin();

	# Insert transport data
	my $sth = DBDo("
			INSERT INTO mail_mailboxes
				(TransportID,Address,Mailbox,Username,ClearPassword,Password$extraColumns)
			VALUES (".
					DBQuote($mailboxInfo->{'TransportID'}).",".
					DBQuote($mailboxInfo->{'Address'}).",".
					DBQuote($mailboxInfo->{'Address'} . '@' . $transportInfo->{'DomainName'}).",".
					DBQuote($username).",".
					DBQuote($mailboxInfo->{'Password'}).",".
					DBQuote(hashPassword($mailboxInfo->{'Password'})).
					$extraValues."
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	my $ID = DBLastInsertID("mail_mailboxes","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method getMailboxes(mailboxInfo,search)
# Return an array of mailbox hashes
#
# @param mailboxInfo Mailbox info hash
# @li TransportID Mailbox transport ID, number or arrayref
# @param search Search criteria
#
# @return Array ref of hash refs
# @li ID - %Mailbox ID on the system
# @li Address - Email address
# @li Name - Name of person
# @li DomainName - Domain name of the email address
# @li Quota - Quota in Mbyte
# @li MailstoreLocation - Mail store location (PRIVATE)
# @li AgentDisabled - Set if the agent has disabled this transport
# @li Disabled - True if mailbox is disabled
# @li AgentRef - %Agent reference
sub _getMailboxes
{
	my ($mailboxInfo,$search) = @_;


	my $extraSQL = "";

	# Check if we have an AgentID defined that its valid
	if (defined($mailboxInfo)) {
		# Make sure we're a hash
		if (!isHash($mailboxInfo)) {
			setError("Parameter 'mailboxInfo' is not a HASH");
			return ERR_PARAM;
		}
		# Check if we have the transport ID(s)
		if (defined($mailboxInfo->{'TransportID'})) {
			# Check if its an array ref
			if (ref($mailboxInfo->{'TransportID'}) eq "ARRAY") {
				my @transportList;
				# Verify all TransportID's
				foreach my $transportID (@{$mailboxInfo->{'TransportID'}}) {
					# If not it can only be a number
					if (!defined($transportID = isNumber($transportID))) {
						setError("Parameter 'mailboxInfo' element 'TransportID' has invalid item");
						return ERR_PARAM;
					}
					# Add to our list
					push(@transportList,DBQuote($transportID));
				}
	
				$extraSQL .= " AND mail_transport.ID IN ( ".join(',',@transportList).")";

			} else {
				# If not it can only be a number
				if (!defined($mailboxInfo->{'TransportID'} = isNumber($mailboxInfo->{'TransportID'}))) {
					setError("Parameter 'mailboxInfo' element 'TransportID' is invalid");
					return ERR_PARAM;
				}
				$extraSQL .= " AND mail_transport.ID = ".DBQuote($mailboxInfo->{'TransportID'});
			}
		}
	}


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'mail_mailboxes.ID',
		'Address' => 'mail_mailboxes.Address',
		'Name' => 'mail_mailboxes.Name',
		'AgentRef' => 'mail_mailboxes.AgentRef',
	};

	# Select mailboxes
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				mail_mailboxes.ID, mail_mailboxes.Address, mail_transport.DomainName, mail_mailboxes.Quota,
				mail_mailboxes.HomeDir, mail_mailboxes.MailDir,
				mail_mailboxes.Name, mail_mailboxes.AgentRef, mail_mailboxes.PremiumPolicy,
				mail_mailboxes.PolicyID, mail_mailboxes.PremiumSMTP,
				mail_mailboxes.AgentDisabled, mail_mailboxes.DisableLogin,
				mail_mailboxes.DisableDelivery, mail_mailboxes.DisableSASL,
				mail_transport.ID AS MailTransportID
			FROM
				mail_mailboxes, mail_transport
			WHERE
				mail_mailboxes.TransportID = mail_transport.ID
				$extraSQL
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @mailboxes = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
					ID Address DomainName Quota Name AgentRef HomeDir MailDir PremiumPolicy PolicyID PremiumSMTP
					AgentDisabled DisableLogin DisableDelivery DisableSASL
					TransportID
				)
			)
	) {
		my $mailbox;

		$mailbox->{'ID'} = $row->{'ID'};
		$mailbox->{'Address'} = $row->{'Address'};
		$mailbox->{'Name'} = $row->{'Name'};
		$mailbox->{'DomainName'} = $row->{'DomainName'};
		$mailbox->{'Quota'} = $row->{'Quota'};
		# Check the dirs are defined
		if (defined($row->{'HomeDir'}) && $row->{'HomeDir'} ne "" && defined($row->{'MailDir'})) {
			$mailbox->{'MailstoreLocation'} = $row->{'HomeDir'};
			# Only add on Maildir if its not blank and not .
			if ($row->{'MailDir'} ne "" && $row->{'MailDir'} ne ".") {
				$mailbox->{'MailstoreLocation'} .= "/" . $row->{'MailDir'};
				# If we end in / it won't pass validation test
				$mailbox->{'MailstoreLocation'} =~ s/\/$//;
			}
		} else {
			$mailbox->{'MailstoreLocation'} = undef;
		}
# FIXME
#		$mailbox->{'PolicyID'} = $row->{'PolicyID'};
#		$mailbox->{'PremiumPolicy'} = $row->{'PremiumPolicy'};
#		$mailbox->{'PremiumSMTP'} = $row->{'PremiumSMTP'};
		$mailbox->{'AgentDisabled'} = booleanize($row->{'AgentDisabled'});
		$mailbox->{'Disabled'} = $row->{'AgentDisabled'};
#		$mailbox->{'DisableLogin'} = $row->{'DisableLogin'};
#		$mailbox->{'DisableDelivery'} = $row->{'DisableDelivery'};
#		$mailbox->{'DisableSASL'} = $row->{'DisableSASL'};
		$mailbox->{'AgentRef'} = $row->{'AgentRef'};
		
		$mailbox->{'TransportID'} = $row->{'TransportID'};

		push(@mailboxes,$mailbox);
	}

	DBFreeRes($sth);

	return (\@mailboxes,$numResults);
}


## @method getMailbox($mailboxID)
# Return mailbox record hash
#
# @param mailboxID %Mailbox ID
#
# @return Hash ref containing mailbox info
# @li ID - %Mailbox ID on the system
# @li Address - Email address
# @li Name - Name of person
# @li DomainName - Domain name of the email address
# @li Quota - Quota in Mbyte
# @li MailstoreLocation - Mailstore location (PRIVATE)
# @li AgentDisabled - Set if the agent has disabled this transport
# @li Disabled - Set if mailbox is disabled
# @li AgentRef - %Agent reference
sub _getMailbox
{
	my $mailboxID = shift;


	if (!defined($mailboxID)) {
		setError("Parameter 'mailboxID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($mailboxID = isNumber($mailboxID))) {
		setError("Parameter 'mailboxID' is invalid");
		return ERR_PARAM;
	}

	# Query mailbox
	my $sth = DBSelect("
			SELECT
				mail_transport.ID as TransportID, mail_transport.DomainName, 
				mail_mailboxes.ID, mail_mailboxes.Address, mail_mailboxes.Name,
				mail_mailboxes.HomeDir, mail_mailboxes.MailDir, mail_mailboxes.Quota,
				mail_mailboxes.AgentRef, mail_mailboxes.PremiumPolicy,
				mail_mailboxes.PolicyID, mail_mailboxes.PremiumSMTP,
				mail_mailboxes.AgentDisabled, mail_mailboxes.DisableLogin,
				mail_mailboxes.DisableDelivery, mail_mailboxes.DisableSASL
			FROM
				mail_mailboxes, mail_transport
			WHERE
				mail_mailboxes.ID = ".DBQuote($mailboxID)."
				AND mail_transport.ID = mail_mailboxes.TransportID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No mailbox data for mailbox ID '".$mailboxID."'");
		DBFreeRes($sth);
		return undef;
	}

	# Grab result
	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw(
				TransportID
				ID Address Name HomeDir MailDir DomainName Quota HomeDir MailDir AgentRef PremiumPolicy 
				PolicyID PremiumSMTP AgentDisabled DisableLogin DisableDelivery DisableSASL
			)
	);
	my $res;

# FIXME - policies?
# FIXME - services attached to mailbox
	$res->{'TransportID'} = $row->{'TransportID'};
	$res->{'ID'} = $row->{'ID'};
	$res->{'Address'} = $row->{'Address'};
	$res->{'Name'} = $row->{'Name'};
	$res->{'DomainName'} = $row->{'DomainName'};
	$res->{'Quota'} = $row->{'Quota'};
	# Check the dirs are defined
	if (defined($row->{'HomeDir'}) && $row->{'HomeDir'} ne "" && defined($row->{'MailDir'})) {
		$res->{'MailstoreLocation'} = $row->{'HomeDir'};
		# Only add on Maildir if its not blank and not .
		if ($row->{'MailDir'} ne "" && $row->{'MailDir'} ne ".") {
			$res->{'MailstoreLocation'} .= "/" . $row->{'MailDir'};
			# If we end in / it won't pass validation test
			$res->{'MailstoreLocation'} =~ s/\/$//;
		}
	} else {
		$res->{'MailstoreLocation'} = undef;
	}
	$res->{'AgentRef'} = $row->{'AgentRef'};
#	$res->{'PremiumPolicy'} = $row->{'PremiumPolicy'};
#	$res->{'PolicyID'} = $row->{'PolicyID'};
#	$res->{'PremiumSMTP'} = $row->{'PremiumSMTP'};
	$res->{'AgentDisabled'} = booleanize($row->{'AgentDisabled'});
	$res->{'Disabled'} = $res->{'AgentDisabled'};
#	$res->{'DisableLogin'} = $row->{'DisableLogin'};
#	$res->{'DisableDelivery'} = $row->{'DisableDelivery'};
#	$res->{'DisableSASL'} = $row->{'DisableSASL'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}



## @method removeMailbox($mailboxID)
# Remove a mailbox
#
# @param mailboxID Mailbox ID
#
# @return 0 on success, < 0 on error
sub _removeMailbox
{
	my $mailboxID = shift;


	if (!defined($mailboxID)) {
		setError("Parameter 'mailboxID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($mailboxID = isNumber($mailboxID))) {
		setError("Parameter 'mailboxID' is invalid");
		return ERR_PARAM;
	}

	# Grab mailbox, we need to see if it exists and to work out some stuff
	my $mailbox = _getMailbox($mailboxID);
	if (!isHash($mailbox)) {
		return $mailbox;
	}

	DBBegin();

# FIXME - policies?
	my $sth = DBDo("
			DELETE FROM mail_policy_users WHERE PolicyID IN (
				SELECT ID FROM mail_policies WHERE PolicyType = 3 AND PolicyRefID = ".DBQuote($mailboxID)."
			)"
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	$sth = DBDo("DELETE FROM mail_policies WHERE PolicyType = 3 AND PolicyRefID = ".DBQuote($mailboxID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove from database
	$sth = DBDo("DELETE FROM mail_mailboxes WHERE ID = ".DBQuote($mailboxID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method updateMailbox($mailboxInfo)
# Update a mailbox
#
# @param mailboxInfo Mailbox info hash ref
# @li ID - Mailbox ID
# @li Name - Optional owner of mailbox name
# @li Password - Optional password
# @li Quota - Optional mailbox quota
# @li MailstoreLocation - Location of mailstore on disk (PRIVATE) - mapped to HomeDir
# @li AgentDisabled - Optional, mailbox disabled by agent
# @li AgentRef - Optional, agent reference
#
# @return Return 0 on success, < 0 on error
sub _updateMailbox
{
	my $mailboxInfo = shift;

	if (!defined($mailboxInfo)) {
		setError("Parameter 'mailboxInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($mailboxInfo)) {
		setError("Parameter 'mailboxInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'ID'})) {
		setError("Parameter 'mailboxInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'ID'} = isNumber($mailboxInfo->{'ID'}))) {
		setError("Parameter 'mailboxID' is invalid");
		return ERR_PARAM;
	}
	if (defined($mailboxInfo->{'Name'})) {
		if (!isVariable($mailboxInfo->{"Name"})) {
			setError("Parameter 'mailboxInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
		if ($mailboxInfo->{"Name"} eq "") {
			delete($mailboxInfo->{"Name"});
		}
	}
	if (defined($mailboxInfo->{'Password'})) {
		if (!isVariable($mailboxInfo->{"Password"})) {
			setError("Parameter 'mailboxInfo' element 'Password' is invalid");
			return ERR_PARAM;
		}
		# Just nuke the password if its blank
		if ($mailboxInfo->{"Password"} eq "") {
			delete($mailboxInfo->{"Password"});
		}
	}
	if (defined($mailboxInfo->{'Quota'})) {
		if (!defined($mailboxInfo->{'Quota'} = isNumber($mailboxInfo->{'Quota'},ISNUMBER_ALLOW_ZERO))) {
			setError("Parameter 'mailboxInfo' element 'Quota' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($mailboxInfo->{"MailstoreLocation"})) {
		if (!isVariable($mailboxInfo->{"MailstoreLocation"})) {
			setError("Parameter 'mailboxInfo' element 'MailstoreLocation' is invalid");
			return ERR_PARAM;
		}
		if ($mailboxInfo->{"MailstoreLocation"} eq "") {
			setError("Parameter 'mailboxInfo' element 'MailstoreLocation' is blank");
			return ERR_PARAM;
		}
	}

	if (defined($mailboxInfo->{"AgentDisabled"})) {
		# Check for non-variable
		if (!isVariable($mailboxInfo->{'AgentDisabled'})) {
			setError("Parameter 'mailboxInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
		# Check disabled flag is a boolean
		if (!defined($mailboxInfo->{'AgentDisabled'} = isBoolean($mailboxInfo->{'AgentDisabled'}))) {
			setError("Parameter 'mailboxInfo' element 'AgentDisabled' has an invalid value");
			return ERR_PARAM;
		}
	}
	if (defined($mailboxInfo->{'AgentRef'})) {
		# Check for non-variable
		if (!isVariable($mailboxInfo->{'AgentRef'})) {
			setError("Parameter 'mailboxInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}

	# Grab ourselves, so we can check a few things below
	my $mailbox = _getMailbox($mailboxInfo->{'ID'});
	if (!isHash($mailbox)) {
		return $mailbox;
	}

	# We do this here cause we don't have mailbox info above
	if (defined($mailboxInfo->{'Password'})) {
		# Make sure password is secure
		if ((my $error = isPasswordInsecure($mailboxInfo->{'Password'},[ $mailbox->{'Address'} ]))) {
			setError("Parameter 'mailboxInfo' element 'Password' is INSECURE: $error");
			return ERR_PARAM;
		}
	}

	# Updates we need to do
	my @updates = ();

	# If this is a RENAME remember to update Mailbox database field!!!
	# DBQuote($address . '@' . $domainName).",".

	if (defined($mailboxInfo->{'Name'})) {
		push(@updates,"Name = ".DBQuote($mailboxInfo->{'Name'}));
	}

	if (defined($mailboxInfo->{'Password'})) {
		push(@updates,"ClearPassword = ".DBQuote($mailboxInfo->{'Password'}));
		push(@updates,"Password = ".DBQuote(hashPassword($mailboxInfo->{'Password'})));
	}

	if (defined($mailboxInfo->{'Quota'})) {
		push(@updates,"Quota = ".DBQuote($mailboxInfo->{'Quota'}));
	}

	# Process MailstoreLocation 
	if (defined($mailboxInfo->{'MailstoreLocation'})) {
		if (!defined($mailbox->{'MailstoreLocation'}) || $mailbox->{'MailstoreLocation'} eq "") {
			push(@updates,"HomeDir = ".DBQuote($mailboxInfo->{'MailstoreLocation'}));
			push(@updates,"MailDir = ''");
		} else {
# NK: We should never need to blank this
#			# See if we blanking it or not
#			if ($mailboxInfo->{"MailstoreLocation"} eq " ") {
#				push(@updates,"HomeDir = NULL");
#				push(@updates,"MailDir = NULL");
#			} else {
				setError("Parameter 'mailboxInfo' element 'MailstoreLocation' cannot be changed if already set");
#			}
		}
	}

#	my $removeCustomPolicy = 0;
#	if (defined($mailboxInfo->{'PremiumPolicy'})) {
#		push(@updates,"PremiumPolicy = ".DBQuote($mailboxInfo->{'PremiumPolicy'}));
#		# Check if we should remove custom policies
#		$removeCustomPolicy = 1 if ($mailboxInfo->{'PremiumPolicy'} == 0);
#	}
#
#	if (defined($mailboxInfo->{'PolicyID'})) {
#		my $val;
#
#		# Check if we must change or blank the policy
#		if ($mailboxInfo->{'PolicyID'} ne "" && $mailboxInfo->{'PolicyID'}) {
#			$val = DBQuote($mailboxInfo->{'PolicyID'});
#		} else {
#			$val = "NULL";
#		}
#
#		push(@updates,"PolicyID = $val");
#	}
#
#	if (defined($mailboxInfo->{'PremiumSMTP'})) {
#		push(@updates,"PremiumSMTP = ".DBQuote($mailboxInfo->{'PremiumSMTP'}));
#	}

	if (defined($mailboxInfo->{"AgentDisabled"})) {
		push(@updates,"AgentDisabled = ".DBQuote($mailboxInfo->{'AgentDisabled'}));
	}

#	if (defined($mailboxInfo->{'DisableLogin'})) {
#		push(@updates,"DisableLogin = ".DBQuote($mailboxInfo->{'DisableLogin'}));
#	}
#
#	if (defined($mailboxInfo->{'DisableDelivery'})) {
#		push(@updates,"DisableDelivery = ".DBQuote($mailboxInfo->{'DisableDelivery'}));
#	}
#
#	if (defined($mailboxInfo->{'DisableSASL'})) {
#		push(@updates,"DisableSASL = ".DBQuote($mailboxInfo->{'DisableSASL'}));
#	}

	if (defined($mailboxInfo->{'AgentRef'})) {
		if ($mailboxInfo->{'AgentRef'} eq " ") {
			push(@updates,"AgentRef = NULL");
		} else {
			push(@updates,"AgentRef = ".DBQuote($mailboxInfo->{'AgentRef'}));
		}
	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for mailbox");
		return ERR_USAGE;
	}

	DBBegin();

	# Update
	my $sth = DBDo("
			UPDATE
				mail_mailboxes
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($mailboxInfo->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @fn mailboxExists($transportID,$address)
# Check if mailbox exists or not
#
# @param transportID Transport ID
# @param address Email address, first part, minus the @@domain
#
# @return 1 on exists, 0 on not exists
sub mailboxExists
{
	my ($transportID,$address) = @_;


	if (!defined($transportID)) {
		setError("Parameter 'transportID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($transportID = isNumber($transportID))) {
		setError("Parameter 'transportID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($address)) {
		setError("Parameter 'address' is not defined");
		return ERR_PARAM;
	}
	if (!isVariable($address)) {
		setError("Parameter 'address' is invalid");
		return ERR_PARAM;
	}
	if ($address eq "") {
		setError("Parameter 'address' is blank");
		return ERR_PARAM;
	}

	# Prepare query to see if the address exists
	my $num_results = DBSelectNumResults("
			FROM
				mail_mailboxes
			WHERE
				TransportID = ".DBQuote($transportID)."
				AND Address = ".DBQuote($address)
	);
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Return 1 if more than 0 results
	return $num_results > 0 ? 1 : 0;
}



## @method createMailboxAlias($mailboxAliasInfo)
# Add an alias
#
# @param mailboxAliasInfo Mailbox alias info hash ref
# @li TransportID - Transport ID
# @li Address - Address minus the @@domain part
# @li Goto - Destination for this alias
# @li AgentRef - Optional agent reference
# @li AgentDisabled - Optional flag to disable this transport
#
# @return Mailbox ID
sub _createMailboxAlias
{
	my $mailboxAliasInfo = shift;

	my $extraColumns = "";
	my $extraValues = "";


	if (!defined($mailboxAliasInfo)) {
		setError("Parameter 'mailboxAliasInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($mailboxAliasInfo)) {
		setError("Parameter 'mailboxAliasInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($mailboxAliasInfo->{'TransportID'})) {
		setError("Parameter 'mailboxAliasInfo' has no element 'TransportID'");
		return ERR_PARAM;
	}
	if (!defined($mailboxAliasInfo->{'TransportID'} = isNumber($mailboxAliasInfo->{'TransportID'}))) {
		setError("Parameter 'mailboxAliasInfo' element 'TransportID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($mailboxAliasInfo->{"Address"})) {
		setError("Parameter 'mailboxAliasInfo' has no element 'Address'");
		return ERR_PARAM;
	}
	if (!isVariable($mailboxAliasInfo->{"Address"})) {
		setError("Parameter 'mailboxAliasInfo' element 'Address' is invalid");
		return ERR_PARAM;
	}
# XXX: CATCHALL's are blank!!
#	if ($mailboxAliasInfo->{"Address"} eq "") {
#		setError("Parameter 'mailboxAliasInfo' element 'Address' is blank");
#		return ERR_PARAM;
#	}
	$mailboxAliasInfo->{'Address'} = lc($mailboxAliasInfo->{'Address'});
	# Check validity
	if (!($mailboxAliasInfo->{'Address'} =~ /^[a-z0-9_\-\.]*$/)) {
		setError("Parameter 'mailboxAliasInfo' element 'Address' is invalid");
		return ERR_PARAM;
	}
	if (!defined($mailboxAliasInfo->{"Goto"})) {
		setError("Parameter 'mailboxAliasInfo' has no element 'Goto'");
		return ERR_PARAM;
	}
	if (!isVariable($mailboxAliasInfo->{"Goto"})) {
		setError("Parameter 'mailboxAliasInfo' element 'Goto' is invalid");
		return ERR_PARAM;
	}
	if ($mailboxAliasInfo->{"Goto"} eq "") {
		setError("Parameter 'mailboxAliasInfo' element 'Goto' is blank");
		return ERR_PARAM;
	}
	$mailboxAliasInfo->{'Goto'} = lc($mailboxAliasInfo->{'Goto'});
	if (defined($mailboxAliasInfo->{'AgentRef'})) {
		# Check for non-variable
		if (!isVariable($mailboxAliasInfo->{'AgentRef'})) {
			setError("Parameter 'transportInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($mailboxAliasInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($mailboxAliasInfo->{'AgentDisabled'})) {
			setError("Parameter 'transportInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
		if (!defined($mailboxAliasInfo->{'AgentDisabled'} = isBoolean($mailboxAliasInfo->{"AgentDisabled"}))) {
			setError("Parameter 'transportInfo' element 'AgentDisabled' has an invalid value");
			return ERR_PARAM;
		}
	}

	# Grab transport
	my $transportInfo = _getMailTransport($mailboxAliasInfo->{'TransportID'});
	if (!isHash($transportInfo)) {
		return $transportInfo;
	}

	# Check if alias exists as a mailbox
	if (my $res = mailboxExists($transportInfo->{'ID'},$mailboxAliasInfo->{'Address'})) {
		# Check result
		if ($res < 0) {
			return $res;
		} else {
			setError("Alias address already exists as a mailbox");
			return ERR_CONFLICT;
		}
	}
	# Check if alias exists
	if (my $res = mailboxAliasExists($transportInfo->{'ID'},$mailboxAliasInfo->{'Address'})) {
		# Check result
		if ($res < 0) {
			return $res;
		} else {
			setError("Alias address already exists");
			return ERR_EXISTS;
		}
	}

	if (defined($mailboxAliasInfo->{'AgentRef'})) {
		if ($mailboxAliasInfo->{'AgentRef'} eq " ") {
			$extraColumns .= ",AgentRef";
			$extraValues .= ",NULL";
		} else {
			$extraColumns .= ",AgentRef";
			$extraValues .= ",".DBQuote($mailboxAliasInfo->{'AgentRef'});
		}
	}

	if (defined($mailboxAliasInfo->{'AgentDisabled'})) {
		$extraColumns .= ",AgentDisabled";
		$extraValues .= ",".DBQuote($mailboxAliasInfo->{'AgentDisabled'});
	}

# FIXME
#
#	if (defined($mailboxAliasInfo->{'DisableDelivery'})) {
#		$extraColumns .= ",DisableDelivery";
#		$extraValues .= ",".DBQuote($mailboxAliasInfo->{'DisableDelivery'});
#	}

	DBBegin();

	# Insert transport data
	my $sth = DBDo("
			INSERT INTO mail_aliases
				(TransportID,Address,Alias,Goto$extraColumns)
			VALUES (".
					DBQuote($transportInfo->{'ID'}).",".
					DBQuote($mailboxAliasInfo->{'Address'}).",".
					DBQuote($mailboxAliasInfo->{'Address'} . '@' . $transportInfo->{'DomainName'}).",".
					DBQuote($mailboxAliasInfo->{'Goto'}).
					$extraValues."
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	my $ID = DBLastInsertID("mail_aliases","ID");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}



## @method getMailboxAliases($mailboxAliasInfo,$search)
# Return an array of mailbox alias hashes
#
# @param mailboxAliasInfo Mailbox alias info hash
# @li TransportID Transport ID
# @param search Optional search criteria, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - %Mailbox ID on the system
# @li Address - Email address
# @li DomainName - Domain name of the alias address
# @li TransportID - Mail transport ID
# @li Goto - Alias destination
# @li AgentDisabled - Set if the agent has disabled this transport
# @li Disabled - True if mailbox is disabled
# @li AgentRef - %Agent reference
sub _getMailboxAliases
{
	my ($mailboxAliasInfo,$search) = @_;


	my $extraSQL = "";

	# Check if we have an AgentID defined that its valid
	if (defined($mailboxAliasInfo)) {
		# Make sure we're a hash
		if (!isHash($mailboxAliasInfo)) {
			setError("Parameter 'mailboxAliasInfo' is not a HASH");
			return ERR_PARAM;
		}
		# Check if we have the transport ID(s)
		if (defined($mailboxAliasInfo->{'TransportID'})) {
			# Check if its an array ref
			if (ref($mailboxAliasInfo->{'TransportID'}) eq "ARRAY") {
				my @transportList;
				# Verify all TransportID's
				foreach my $transportID (@{$mailboxAliasInfo->{'TransportID'}}) {
					# If not it can only be a number
					if (!defined($transportID = isNumber($transportID))) {
						setError("Parameter 'mailboxAliasInfo' element 'TransportID' has invalid item");
						return ERR_PARAM;
					}
					# Add to our list
					push(@transportList,DBQuote($transportID));
				}
	
				$extraSQL .= " AND mail_transport.ID IN ( ".join(',',@transportList).")";

			} else {
				# If not it can only be a number
				if (!defined($mailboxAliasInfo->{'TransportID'} = isNumber($mailboxAliasInfo->{'TransportID'}))) {
					setError("Parameter 'mailboxAliasInfo' element 'TransportID' is invalid");
					return ERR_PARAM;
				}
				$extraSQL .= " AND mail_transport.ID = ".DBQuote($mailboxAliasInfo->{'TransportID'});
			}
		}
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'mail_aliases.ID',
		'Address' => 'mail_aliases.Address',
		'Goto' => 'mail_aliases.Goto',
		'AgentRef' => 'mail_aliases.AgentRef'
	};

	# Select transports
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				mail_aliases.ID, mail_aliases.Address, mail_transport.DomainName, mail_aliases.Goto,
				mail_aliases.AgentRef, mail_aliases.AgentDisabled, mail_aliases.DisableDelivery,
				mail_transport.ID AS TransportID
			FROM
				mail_aliases, mail_transport
			WHERE
				mail_aliases.TransportID = mail_transport.ID
				$extraSQL
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Fetch rows
	my @mailboxAliases = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(
					ID Address DomainName Goto AgentRef AgentDisabled DisableDelivery
					TransportID
				)
			)
	) {
		my $alias;

		$alias->{'ID'} = $row->{'ID'};
		$alias->{'Address'} = $row->{'Address'};
		$alias->{'DomainName'} = $row->{'DomainName'};
		$alias->{'Goto'} = $row->{'Goto'};
		$alias->{'AgentRef'} = $row->{'AgentRef'};
		$alias->{'AgentDisabled'} = booleanize($row->{'AgentDisabled'});
# FIXME - standardize?
#		$alias->{'DisableDelivery'} = $row->{'DisableDelivery'};
		$alias->{'Disabled'} = $row->{'AgentDisabled'};
		
		$alias->{'TransportID'} = $row->{'TransportID'};

		push(@mailboxAliases,$alias);
	}

	DBFreeRes($sth);

	return (\@mailboxAliases,$numResults);
}



## @method removeMailboxAlias($mailboxAliasID)
# Remove mailbox alias
#
# @param mailboxAliasID Mailbox alias ID
#
# @return 0 on success, < 0 on error
sub _removeMailboxAlias
{
	my $mailboxAliasID = shift;


	if (!defined($mailboxAliasID)) {
		setError("Parameter 'mailboxAliasID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($mailboxAliasID = isNumber($mailboxAliasID))) {
		setError("Parameter 'mailboxAliasID' is invalid");
		return ERR_PARAM;
	}

	# Grab mailbox alias to see if it exists
	my $mailboxAliasInfo = _getMailboxAlias($mailboxAliasID);
	if (!isHash($mailboxAliasInfo)) {
		return $mailboxAliasInfo;
	}

	DBBegin();

	# Remove from database
	my $sth = DBDo("DELETE FROM mail_aliases WHERE ID = ".DBQuote($mailboxAliasID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method getMailboxAlias($mailboxAliasID)
# Return mailbox alias record hash
#
# @param mailboxAliasID %Mailbox alias ID
#
# @return Hash ref
# @li ID - %Mailbox alias ID on the system
# @li Address - Email address
# @li Goto - Alias destination
# @li AgentDisabled - Set if the agent has disabled this transport
# @li AgentRef - %Agent reference
sub _getMailboxAlias
{
	my $mailboxAliasID = shift;


	if (!defined($mailboxAliasID)) {
		setError("Parameter 'mailboxAliasID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($mailboxAliasID = isNumber($mailboxAliasID))) {
		setError("Parameter 'mailboxAliasID' is invalid");
		return ERR_PARAM;
	}

	# Query mailbox alias
	my $sth = DBSelect("
			SELECT
				mail_aliases.ID, mail_aliases.Address,
				mail_aliases.Goto, mail_transport.DomainName,
				mail_aliases.AgentRef, mail_aliases.AgentDisabled,
				mail_aliases.DisableDelivery,
				mail_transport.ID AS TransportID
			FROM
				mail_aliases, mail_transport
			WHERE
				mail_aliases.ID = ".DBQuote($mailboxAliasID)."
				AND mail_transport.ID = mail_aliases.TransportID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("Mailbox alias ID '$mailboxAliasID' not found");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw(
				ID Address Goto DomainName AgentRef AgentDisabled DisableDelivery
				TransportID
			)
	);
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Address'} = $row->{'Address'};
	$res->{'DomainName'} = $row->{'DomainName'};
	$res->{'Goto'} = $row->{'Goto'};
	$res->{'AgentDisabled'} = booleanize($row->{'AgentDisabled'});
# FIXME?
#	$res->{'DisableDelivery'} = $row->{'DisableDelivery'};
	$res->{'Disabled'} = $res->{'AgentDisabled'};
	$res->{'AgentRef'} = $row->{'AgentRef'};
	
	$res->{'TransportID'} = $row->{'TransportID'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}


## @method updateMailboxAlias($mailboxAliasInfo)
# Update mailbox alias
#
# @param mailboxAliasInfo %Mailbox alias info hash ref
# @li ID - %Mailbox alias ID
# @li Goto - Optional alias destination address
# @li AgentDisabled - Optional flag agent can set to disable this mailbox alias
# @li AgentRef - Optional agent reference
#
# @return 0 on success or < 0 on error
sub _updateMailboxAlias
{
	my $mailboxAliasInfo = shift;


	if (!defined($mailboxAliasInfo)) {
		setError("Parameter 'mailboxAliasInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($mailboxAliasInfo)) {
		setError("Parameter 'mailboxAliasInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($mailboxAliasInfo->{"ID"})) {
		setError("Parameter 'mailboxAliasInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($mailboxAliasInfo->{'ID'} = isNumber($mailboxAliasInfo->{'ID'}))) {
		setError("Parameter 'mailboxAliasInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}
	if (defined($mailboxAliasInfo->{'Goto'})) {
		if (!isVariable($mailboxAliasInfo->{"Goto"})) {
			setError("Parameter 'mailboxAliasInfo' element 'Goto' is invalid");
			return ERR_PARAM;
		}
		if ($mailboxAliasInfo->{"Goto"} eq "") {
			setError("Parameter 'mailboxAliasInfo' element 'Goto' is blank");
			return ERR_PARAM;
		}
		# Lowercase the goto
		$mailboxAliasInfo->{'Goto'} = lc($mailboxAliasInfo->{'Goto'});
	}
	if (defined($mailboxAliasInfo->{'AgentRef'})) {
		# Check for non-variable
		if (!isVariable($mailboxAliasInfo->{'AgentRef'})) {
			setError("Parameter 'mailboxAliasInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($mailboxAliasInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($mailboxAliasInfo->{'AgentDisabled'})) {
			setError("Parameter 'mailboxAliasInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
		if (!defined($mailboxAliasInfo->{'AgentDisabled'} = isBoolean($mailboxAliasInfo->{"AgentDisabled"}))) {
			setError("Parameter 'mailboxAliasInfo' element 'AgentDisabled' has an invalid value");
			return ERR_PARAM;
		}
	}

	# Updates we need to do
	my @updates = ();

	if (defined($mailboxAliasInfo->{'Goto'})) {
		push(@updates,"Goto = ".DBQuote($mailboxAliasInfo->{'Goto'}));
	}

	if (defined($mailboxAliasInfo->{'AgentRef'})) {
		if ($mailboxAliasInfo->{'AgentRef'} eq " ") {
			push(@updates,"AgentRef = NULL");
		} else {
			push(@updates,"AgentRef = ".DBQuote($mailboxAliasInfo->{'AgentRef'}));
		}
	}

	if (defined($mailboxAliasInfo->{'AgentDisabled'})) {
		push(@updates,"AgentDisabled = ".DBQuote($mailboxAliasInfo->{'AgentDisabled'}));
	}

#	if (defined($mailboxAliasInfo->{'DisableDelivery'})) {
#		push(@updates,"DisableDelivery = ".DBQuote($mailboxAliasInfo->{'DisableDelivery'}));
#	}

	# Create and check update string
	my $updateStr = join(",",@updates);

	if ($updateStr eq "") {
		setError("No updates specified for transport");
		return ERR_USAGE;
	}

	# Grab mailbox alias to see if it exists
	my $mailboxAlias = _getMailboxAlias($mailboxAliasInfo->{'ID'});
	if (!isHash($mailboxAlias)) {
		return $mailboxAlias;
	}

	DBBegin();

	# Update
	my $sth = DBDo("
			UPDATE
				mail_aliases
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($mailboxAliasInfo->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}


## @method createMailTransportMailstore
# Create mail transport mailstore
#
# @param transportInfo Transport info hash ref
# @li ID - Mail transport ID
# @li AgentID - Agent ID
# @li DomainName - Domain name
#
# @return Mail transport mailstore path
sub _createMailTransportMailstore
{
	my $transportInfo = shift;


	if (!defined($transportInfo)) {
		setError("Parameter 'transportInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($transportInfo)) {
		setError("Parameter 'transportInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{'ID'})) {
		setError("Parameter 'transportInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{'ID'} = isNumber($transportInfo->{'ID'}))) {
		setError("Parameter 'transportInfo' element 'TransportID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{'AgentID'})) {
		setError("Parameter 'transportInfo' has no element 'AgentID'");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{'AgentID'} = isNumber($transportInfo->{'AgentID'}))) {
		setError("Parameter 'transportInfo' element 'AgentID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{"DomainName"})) {
		setError("Parameter 'transportInfo' has no element 'DomainName'");
		return ERR_PARAM;
	}
	if (!isVariable($transportInfo->{"DomainName"})) {
		setError("Parameter 'transportInfo' element 'DomainName' is invalid");
		return ERR_PARAM;
	}
	if ($transportInfo->{"DomainName"} eq "") {
		setError("Parameter 'transportInfo' element 'DomainName' is blank");
		return ERR_PARAM;
	}

	# Grab server attribute
	my $mailstorePathTmpl = smlib::servers::getAttribute("MailTransportMailstoreLocation");
	# If its undefined or blank, just use the default
	if (!defined($mailstorePathTmpl) || $mailstorePathTmpl eq "") {
		$mailstorePathTmpl = '%{DOMAIN(0,1)}/%{DOMAIN(0,2)}/%{DOMAIN}';
	}

	# Parse the macro in, and generate the path we need
	my ($res,$mailstorePath) = parseMacro(
			{
				"AGENT_ID" => $transportInfo->{"AgentID"},
				"DOMAIN" => $transportInfo->{"DomainName"},
				"MAIL_TRANSPORT_ID" => $transportInfo->{"ID"}
			},
			$mailstorePathTmpl
	);
	# Make sure $res is ok
	if ($res != RES_OK) {
		setError("Backend templating system error");
		return ERR_UNKNOWN;
	}

	# Grab the absolute path
	my $mailstoreFullPath = sprintf('%s/%s',$config->{'VMailDir'},$mailstorePath);
	my $mailstoreAbsPath = sanitizePath(sprintf('%s/%s',$config->{'VMailDir'},$mailstorePath));
	my $vmailDirAbsPath = sanitizePath($config->{'VMailDir'});

	# Test its the same, if someone is trying to do something odd, it more than likely won't be
	if ($mailstoreFullPath ne $mailstoreAbsPath && $mailstoreAbsPath ne $vmailDirAbsPath) {
		setError("Backend server mail transport location failed sanity check");
		return ERR_UNKNOWN;
	}

	# Try create dir
	eval {
		# Get uid and gid
		my $uid = getpwnam($config->{'VMailUser'})
				or die "Failed to getpwnam() on '".$config->{'VMailUser'}."': $!";
		my $gid = getgrnam($config->{'VMailGroup'})
				or die "Failed to getgrnam() on '".$config->{'VMailGroup'}."': $!";

		# Create maildir	
		my @created = mkpath($mailstoreAbsPath, 0, 0750);
		# Set ownership
		chown($uid,$gid,@created);

		1;
	};
	if ($@) {
		&$logger(LOG_ERR,"[MAILHOSTING] Backend server mail transport location could not be created: $@");
		setError("Backend server mail transport location could not be created");
		return ERR_UNKNOWN;
	}

	return (0,$mailstorePath);
}


## @method removeMailTransportMailstore
# Remove mail transport mailstore
#
# @param transportInfo Transport info hash ref
# @li ID - Mail transport ID
# @li AgentID - Agent ID
# @li DomainName - Domain name
# @li MailstoreLocation - Mailstore path
#
# @return Mail transport mailstore path
sub _removeMailTransportMailstore
{
	my $transportInfo = shift;


	if (!defined($transportInfo)) {
		setError("Parameter 'transportInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($transportInfo)) {
		setError("Parameter 'transportInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{'ID'})) {
		setError("Parameter 'transportInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{'ID'} = isNumber($transportInfo->{'ID'}))) {
		setError("Parameter 'transportInfo' element 'TransportID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($transportInfo->{"MailstoreLocation"})) {
		setError("Parameter 'transportInfo' has no element 'MailstoreLocation'");
		return ERR_PARAM;
	}
	if (!isVariable($transportInfo->{"MailstoreLocation"})) {
		setError("Parameter 'transportInfo' element 'MailstoreLocation' is invalid");
		return ERR_PARAM;
	}
	if ($transportInfo->{"MailstoreLocation"} eq "") {
		setError("Parameter 'transportInfo' element 'MailstoreLocation' is blank");
		return ERR_PARAM;
	}

	# Grab the absolute path
	my $mailstoreFullPath = sprintf('%s/%s',$config->{'VMailDir'},$transportInfo->{"MailstoreLocation"});
	my $mailstoreAbsPath = sanitizePath(sprintf('%s/%s',$config->{'VMailDir'},$transportInfo->{"MailstoreLocation"}));
	my $vmailDirAbsPath = sanitizePath($config->{'VMailDir'});

	# Test its the same, if someone is trying to do something odd, it more than likely won't be
	if ($mailstoreFullPath ne $mailstoreAbsPath && $mailstoreAbsPath ne $vmailDirAbsPath) {
		setError("Backend server mail transport location failed sanity check");
		return ERR_UNKNOWN;
	}

	# Try remove dir
	eval {
		rmtree($mailstoreAbsPath);

		1;
	};
	if ($@) {
		&$logger(LOG_ERR,"[MAILHOSTING] Backend server mail transport location could not be removed: $@");
		setError("Backend server mail transport location could not be removed");
		return ERR_UNKNOWN;
	}

	# Remove trailing dirs?
	my @dirList;
	my @componentList = split(/\//,$transportInfo->{"MailstoreLocation"});
	my $pDir = "";
	# Pop off the dir we removed above ...
	pop(@componentList);
	foreach my $dir (@componentList) {
		$pDir = "$pDir/$dir";
		push(@dirList,$pDir);
	}
	# Loop with dirs in reverse
	foreach my $dir (reverse(@dirList)) {
		my $fullPath = sprintf('%s/%s',$config->{'VMailDir'},$dir);
		# If we cannot blow it away, just stop
		if (!rmdir($fullPath)) {
			last;
		}
	}

	return RES_OK;
}


## @method createMailboxMailstore
# Create mailbox mailstore
#
# @param mailboxInfo Mailbox info hash ref
# @li ID - Mailbox ID
# @li AgentID - Agent ID
# @li Address - Email address
# @li TransportID - Mail transport ID
# @li MailstoreLocation - Transport mailstore path
# @li MailstoreSize - Mailstore size
#
# @return Mail transport mailstore path
sub _createMailboxMailstore
{
	my $mailboxInfo = shift;


	if (!defined($mailboxInfo)) {
		setError("Parameter 'mailboxInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($mailboxInfo)) {
		setError("Parameter 'mailboxInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'ID'})) {
		setError("Parameter 'mailboxInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'ID'} = isNumber($mailboxInfo->{'ID'}))) {
		setError("Parameter 'mailboxInfo' element 'TransportID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'AgentID'})) {
		setError("Parameter 'mailboxInfo' has no element 'AgentID'");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'AgentID'} = isNumber($mailboxInfo->{'AgentID'}))) {
		setError("Parameter 'mailboxInfo' element 'AgentID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'TransportID'})) {
		setError("Parameter 'mailboxInfo' has no element 'TransportID'");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'TransportID'} = isNumber($mailboxInfo->{'TransportID'}))) {
		setError("Parameter 'mailboxInfo' element 'TransportTransportID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{"Address"})) {
		setError("Parameter 'mailboxInfo' has no element 'Address'");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'Address'} = isUsername($mailboxInfo->{'Address'},ISUSERNAME_ALLOW_ATSIGN))) {
		setError("Parameter 'mailboxInfo' element 'Address' is invalid");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{"TransportMailstoreLocation"})) {
		setError("Parameter 'mailboxInfo' has no element 'TransportMailstoreLocation'");
		return ERR_PARAM;
	}
	if (!isVariable($mailboxInfo->{"TransportMailstoreLocation"})) {
		setError("Parameter 'mailboxInfo' element 'TransportMailstoreLocation' is invalid");
		return ERR_PARAM;
	}
	if ($mailboxInfo->{"TransportMailstoreLocation"} eq "") {
		setError("Parameter 'mailboxInfo' element 'TransportMailstoreLocation' is blank");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'MailstoreSize'})) {
		setError("Parameter 'mailboxInfo' has no element 'MailstoreSize'");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'MailstoreSize'} = isNumber($mailboxInfo->{'MailstoreSize'},ISNUMBER_ALLOW_ZERO))) {
		setError("Parameter 'mailboxInfo' element 'MailstoreSize' is invalid");
		return ERR_PARAM;
	}

	# Split off username & domain name
	my ($userPart,$domainPart) = split(/@/,$mailboxInfo->{"Address"});

	# Grab server attribute
	my $mailstorePathTmpl = smlib::servers::getAttribute("MailboxMailstoreLocation");
	# If its undefined or blank, just use the default
	if (!defined($mailstorePathTmpl) || $mailstorePathTmpl eq "") {
		$mailstorePathTmpl = '%{USER(0,1)}/%{USER(0,2)}/%{USER}';
	}

	# Parse the macro in, and generate the path we need
	my ($res,$mailstorePath) = parseMacro(
			{
				"AGENT_ID" => $mailboxInfo->{"AgentID"},
				"EMAIL" => $mailboxInfo->{"Address"},
				"DOMAIN" => $domainPart,
				"USER" => $userPart,
				"MAIL_TRANSPORT_ID" => $mailboxInfo->{"TransportID"},
				"MAILBOX_ID" => $mailboxInfo->{"ID"}
			},
			$mailstorePathTmpl
	);
	# Make sure $res is ok
	if ($res != RES_OK) {
		setError("Backend templating system error");
		return ERR_UNKNOWN;
	}

	# Prefix the transport location
	$mailstorePath = sprintf('%s/%s',$mailboxInfo->{"TransportMailstoreLocation"},
			$mailstorePath);

	# Grab the absolute path
	my $mailstoreFullPath = sprintf('%s/%s',$config->{'VMailDir'},$mailstorePath);
	my $mailstoreAbsPath = sanitizePath(sprintf('%s/%s',$config->{'VMailDir'},
			$mailstorePath));
	my $vmailDirAbsPath = sanitizePath($config->{'VMailDir'});

	# Test its the same, if someone is trying to do something odd, it more than likely won't be
	if ($mailstoreFullPath ne $mailstoreAbsPath && $mailstoreAbsPath ne $vmailDirAbsPath) {
		setError("Backend server mailbox location failed sanity check");
		return ERR_UNKNOWN;
	}

	# Set maildir quota
	my $maildirQuota = $mailboxInfo->{"MailstoreSize"} * 1024 * 1024;

	# Try create dir
	eval {
		# Get uid and gid
		my $uid = getpwnam($config->{'VMailUser'})
				or die "Failed to getpwnam() on '".$config->{'VMailUser'}."': $!";
		my $gid = getgrnam($config->{'VMailGroup'})
				or die "Failed to getgrnam() on '".$config->{'VMailGroup'}."': $!";

		my @dirList = (
				$mailstoreAbsPath."/tmp",
				$mailstoreAbsPath."/new",
				$mailstoreAbsPath."/cur"
		);
		# Create maildir	
		my @created = mkpath(\@dirList, 0, 0750);

		# Setup mail quota size
		open(MDIRSIZE,"> $mailstoreAbsPath/maildirsize")
				or die "Failed to open '$mailstoreAbsPath/maildirsize': $!";
		print(MDIRSIZE "${maildirQuota}S\n");
		print(MDIRSIZE "          0            0");
		close(MDIRSIZE);
		chmod(0640,"$mailstoreAbsPath/maildirsize");

		# Set ownership
		chown($uid,$gid,@created,"$mailstoreAbsPath/maildirsize");

		1;
	};
	if ($@) {
		&$logger(LOG_ERR,"[MAILHOSTING] Backend server mailbox location could not be created: $@");
		setError("Backend server mailbox location could not be created");
		return ERR_UNKNOWN;
	}

	return (0,$mailstorePath);
}


## @method updateMailboxMailstore
# Update mailbox mailstore
#
# @param mailboxInfo Mailbox info hash ref
# @li ID - Mail transport ID
# @li AgentID - Agent ID
# @li DomainName - Domain name
# @li MailstoreLocation - Mailstore path
# @li MailstoreSize - Mailstore size
#
# @return 0 on success, -1 on failure
sub _updateMailboxMailstore
{
	my $mailboxInfo = shift;


	if (!defined($mailboxInfo)) {
		setError("Parameter 'mailboxInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($mailboxInfo)) {
		setError("Parameter 'mailboxInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'ID'})) {
		setError("Parameter 'mailboxInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'ID'} = isNumber($mailboxInfo->{'ID'}))) {
		setError("Parameter 'mailboxInfo' element 'TransportID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{"MailstoreLocation"})) {
		setError("Parameter 'mailboxInfo' has no element 'MailstoreLocation'");
		return ERR_PARAM;
	}
	if (!isVariable($mailboxInfo->{"MailstoreLocation"})) {
		setError("Parameter 'mailboxInfo' element 'MailstoreLocation' is invalid");
		return ERR_PARAM;
	}
	if ($mailboxInfo->{"MailstoreLocation"} eq "") {
		setError("Parameter 'mailboxInfo' element 'MailstoreLocation' is blank");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'MailstoreSize'})) {
		setError("Parameter 'mailboxInfo' has no element 'MailstoreSize'");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'MailstoreSize'} = isNumber($mailboxInfo->{'MailstoreSize'},ISNUMBER_ALLOW_ZERO))) {
		setError("Parameter 'mailboxInfo' element 'TransportMailstoreSize' is invalid");
		return ERR_PARAM;
	}

	# Grab the absolute path
	my $mailstoreFullPath = sprintf('%s/%s',$config->{'VMailDir'},$mailboxInfo->{"MailstoreLocation"});
	my $mailstoreAbsPath = sanitizePath(sprintf('%s/%s',$config->{'VMailDir'},$mailboxInfo->{"MailstoreLocation"}));
	my $vmailDirAbsPath = sanitizePath($config->{'VMailDir'});

	# Test its the same, if someone is trying to do something odd, it more than likely won't be
	if ($mailstoreFullPath ne $mailstoreAbsPath && $mailstoreAbsPath ne $vmailDirAbsPath) {
		setError("Backend server mailbox location failed sanity check");
		return ERR_UNKNOWN;
	}

	# Set maildir quota
	my $maildirQuota = $mailboxInfo->{'MailstoreSize'} * 1024 * 1024;

	# Try create dir
	eval {
		my @lines;

		# Setup mail quota size
		open(MDIRSIZE,"< $mailstoreAbsPath/maildirsize") 
				or die "Failed to open '$mailstoreAbsPath/maildirsize': $!";
		while ((my $line = <MDIRSIZE>)) {
			push(@lines,$line);
		}
		close(MDIRSIZE);
		# Now write it out
		open(MDIRSIZE,"> $mailstoreAbsPath/maildirsize")
				or die "Failed to open '$mailstoreAbsPath/maildirsize': $!";
		pop(@lines); print(MDIRSIZE "${maildirQuota}S\n");
		foreach my $line (@lines) {
			print(MDIRSIZE $line);
		}
		close(MDIRSIZE);

		1;
	};
	if ($@) {
		&$logger(LOG_ERR,"[MAILHOSTING] Backend server mailbox location could not be updated: $@");
		setError("Backend server mailbox location could not be updated");
		return ERR_UNKNOWN;
	}
	return RES_OK;
}



## @method removeMailboxMailstore
# Remove mailbox mailstore
#
# @param mailboxInfo Mailbox info hash ref
# @li ID - Mail transport ID
# @li AgentID - Agent ID
# @li DomainName - Domain name
# @li MailstoreLocation - Mailstore path
#
# @return Mail transport mailstore path
sub _removeMailboxMailstore
{
	my $mailboxInfo = shift;


	if (!defined($mailboxInfo)) {
		setError("Parameter 'mailboxInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($mailboxInfo)) {
		setError("Parameter 'mailboxInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'ID'})) {
		setError("Parameter 'mailboxInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{'ID'} = isNumber($mailboxInfo->{'ID'}))) {
		setError("Parameter 'mailboxInfo' element 'TransportID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($mailboxInfo->{"MailstoreLocation"})) {
		setError("Parameter 'mailboxInfo' has no element 'MailstoreLocation'");
		return ERR_PARAM;
	}
	if (!isVariable($mailboxInfo->{"MailstoreLocation"})) {
		setError("Parameter 'mailboxInfo' element 'MailstoreLocation' is invalid");
		return ERR_PARAM;
	}
	if ($mailboxInfo->{"MailstoreLocation"} eq "") {
		setError("Parameter 'mailboxInfo' element 'MailstoreLocation' is blank");
		return ERR_PARAM;
	}

	# Grab the absolute path
	my $mailstoreFullPath = sprintf('%s/%s',$config->{'VMailDir'},$mailboxInfo->{"MailstoreLocation"});
	my $mailstoreAbsPath = sanitizePath(sprintf('%s/%s',$config->{'VMailDir'},$mailboxInfo->{"MailstoreLocation"}));
	my $vmailDirAbsPath = sanitizePath($config->{'VMailDir'});

	# Test its the same, if someone is trying to do something odd, it more than likely won't be
	if ($mailstoreFullPath ne $mailstoreAbsPath && $mailstoreAbsPath ne $vmailDirAbsPath) {
		setError("Backend server mailbox location failed sanity check");
		return ERR_UNKNOWN;
	}

	# Try remove dir
	eval {
		rmtree($mailstoreAbsPath);
	};
	if ($@) {
		&$logger(LOG_ERR,"[MAILHOSTING] Backend server mailbox location could not be removed: $@");
		setError("Backend server mailbox location could not be removed");
		return ERR_UNKNOWN;
	}

	return RES_OK;
}






## @fn mailboxAliasExists($transportID,$address)
# Check if mailbox alias exists or not
#
# @param transportID Transport ID
# @param address Email alias, first part, minus the @@domain
#
# @return 1 on exists, 0 on not exists
sub mailboxAliasExists
{
	my ($transportID,$address) = @_;


	if (!defined($transportID)) {
		setError("Parameter 'transportID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($transportID = isNumber($transportID))) {
		setError("Parameter 'transportID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($address)) {
		setError("Parameter 'address' is not defined");
		return ERR_PARAM;
	}
	if (!isVariable($address)) {
		setError("Parameter 'address' is invalid");
		return ERR_PARAM;
	}
	if ($address eq "") {
		setError("Parameter 'address' is blank");
		return ERR_PARAM;
	}

	# Prepare query to see if the alias exists
	my $num_results = DBSelectNumResults("
			FROM
				mail_aliases
			WHERE
				TransportID = ".DBQuote($transportID)."
				AND Address = ".DBQuote($address)
	);
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results > 0 ? 1 : 0;
}



## @fn canAgentAccessMailTransport($agentID,$transportID)
# Check to see if agent can access this mail transport
#
# @param agentID Agent ID
# @param transportID Transport ID
#
# @return 1 if agent can access, 0 if not
sub canAgentAccessMailTransport
{
	my ($agentID,$transportID) = @_;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($transportID)) {
		setError("Parameter 'transportID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($transportID = isNumber($transportID))) {
		setError("Parameter 'transportID' is invalid");
		return ERR_PARAM;
	}

	# Query to see if we associated with this transport
	my $num_results = DBSelectNumResults("
			FROM
				mail_transport
			WHERE
				mail_transport.ID = ".DBQuote($transportID)."
				AND mail_transport.AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}



## @fn canAgentAccessMailbox($agentID,$mailboxID)
# Check to see if agent can access this mailbox
#
# @param agentID Agent ID
# @param mailboxID Mailbox ID
#
# @return 1 if agent can access, 0 if not
sub canAgentAccessMailbox
{
	my ($agentID,$mailboxID) = @_;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($mailboxID)) {
		setError("Parameter 'mailboxID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($mailboxID = isNumber($mailboxID))) {
		setError("Parameter 'mailboxID' is invalid");
		return ERR_PARAM;
	}

	# Query to see if we associated with this transport & mailbox
	my $num_results = DBSelectNumResults("
			FROM
				mail_mailboxes, mail_transport
			WHERE
				mail_mailboxes.ID = ".DBQuote($mailboxID)."
				AND mail_transport.ID = mail_mailboxes.TransportID
				AND mail_transport.AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}


## @fn canAgentAccessMailboxalias($agentID,$mailboxAliasID)
# Check to see if agent can access this mailbox alias
#
# @param agentID Agent ID
# @param mailboxAliasID Mailbox alias ID
#
# @return 1 if agent can access, 0 if not
sub canAgentAccessMailboxAlias
{
	my ($agentID,$mailboxAliasID) = @_;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_UNKNOWN;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_UNKNOWN;
	}

	if (!defined($mailboxAliasID)) {
		setError("Parameter 'mailboxAliasID' is not defined");
		return ERR_UNKNOWN;
	}
	if (!defined($mailboxAliasID = isNumber($mailboxAliasID))) {
		setError("Parameter 'mailboxAliasID' is invalid");
		return ERR_UNKNOWN;
	}

	# Query to see if we associated with this transport & mailbox alias
	my $num_results = DBSelectNumResults("
			FROM
				mail_aliases, mail_transport
			WHERE
				mail_aliases.ID = ".DBQuote($mailboxAliasID)."
				AND mail_transport.ID = mail_aliases.TransportID
				AND mail_transport.AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}



## @fn Mailbox_auth($username,$password)
# Function to authenticate a mailbox
#
# @param username Username, or email address in this case
# @param password Password
#
# @return MailboxID on success, -1 otherwise
sub Mailbox_auth
{
	my ($username,$password) = @_;
	my $res = -1;


	if (!defined($username)) {
		setError("Parameter 'username' is not defined");
		return ERR_PARAM;
	}
	if ($username eq "") {
		setError("Parameter 'username' is blank");
		return ERR_PARAM;
	}

	if (!defined($password)) {
		setError("Parameter 'password' is not defined");
		return ERR_PARAM;
	}
	if ($password eq "") {
		setError("Parameter 'password' is blank");
		return ERR_PARAM;
	}

	# Select user details from database
	my $sth = DBSelect("
			SELECT
				ID, Password
			FROM
				mail_mailboxes
			WHERE
				Username = ".DBQuote($username)."
				AND DisableLogin = 0
				AND AgentDisabled = 0
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# If we have an invalid number of rows, return negative response
	if ($sth->rows != 1) {
		DBFreeRes($sth);
		setError("Authentication failed");
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID Password ));

	# Lower both
	my $cpass = lc(hashPassword($password));
	my $dpass = lc($row->{'Password'});

	# Compare
	if ($cpass eq $dpass) {
		$res = $row->{'ID'};
	} else {
		setError("Authentication failed");
	}

	DBFreeRes($sth);

	return $res;
}



## @fn getMailHostingServerGroups($serverGroupInfo,$search)
# INTERNAL FUNCTION CALLED FROM smlib::MailHosting ONLY
# Returns list of mailhosting server groups
#
# @param servrGroupInfo MailHosting serverGroup info hash ref
# @li AgentID - Optional agent ID
#
# @param search Search hash ref, @see smlib::getServerGroups
#
# @return Array ref of hash refs
# @li ID - MailHosting server group ID
# @li Name - MailHosting server group name
sub getMailHostingServerGroups
{
	my ($serverGroupInfo,$search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Data to pass to smlib::servers
	my $serverGroupData;

	# Make sure params are ok
	if (defined($serverGroupInfo->{'AgentID'})) {
		if (!defined($serverGroupData->{'AgentID'} = isNumber($serverGroupInfo->{'AgentID'}))) {
			setError("Parameter 'serverGroupInfo' element 'AgentID' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($serverGroupInfo->{'UserID'})) {
		if (!defined($serverGroupData->{'UserID'} = isNumber($serverGroupInfo->{'UserID'}))) {
			setError("Parameter 'serverGroupInfo' element 'AgentID' is invalid");
			return ERR_PARAM;
		}
	}

	# Limit to mailhosting servers
	$serverGroupData->{'Capabilities'} = 'MailHosting';

	# Grab results and see if they ok, if not just pass the error through
	my ($results,$num_results) = smlib::servers::getServerGroups($serverGroupData,$search);
	if (ref($results) ne "ARRAY") {
		return $results;
	}

	return ($results,$num_results);
}



1;
# vim: ts=4
