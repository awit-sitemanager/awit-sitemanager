# Audit Trail Functions
# Copyright (C) 2011, AllWorldIT
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

## @class smlib::AuditTrails
# This is the audit trails class for SMEngine
package smlib::AuditTrails;


use strict;
use warnings;


use awitpt::db::dblayer;
use smlib::logging;
use smlib::constants;
use smlib::util;
use DateTime;



## @method createAuditTrail($auditTrailInfo)
# Log an audit trail to the database
#
# @param auditTrailInfo Audit info hash ref
# @li ClientIP - Client's IP
# @li Protocol - Protocol used
# @li AgentID - Optional AgentID of caller
# @li Username - Username that called the function
# @li ServerCluster - Server cluster used
# @li ServerName - Server used
# @li Module - Module accessed
# @li Function - Function accessed
# @li Parameters - Function parameters
# @li Result - Function result
#
# @return 0 on result, < 0 on error
sub createAuditTrail
{
	my ($auditTrailInfo) = @_;

	# Validatate hash
	if (!defined($auditTrailInfo)) {
		return "[AUDITTRAILS] Parameter 'auditTrailInfo' is not defined";
	}
	if (!isHash($auditTrailInfo)) {
		return "[AUDITTRAILS] Parameter 'auditTrailInfo' is not a hash";
	}

	# Check params
	my $extraSQL = "";
	my $extraParams = "";
	if (defined($auditTrailInfo->{'AgentID'})) {
		if (!isNumber($auditTrailInfo->{'AgentID'})) {
			return "[AUDITTRAILS] Parameter 'auditTrailInfo' element 'AgentID' is invalid";
		}
		$extraSQL .= ",AgentID";
		$extraParams .= ",".DBQuote($auditTrailInfo->{'AgentID'});
	}
	if (defined($auditTrailInfo->{'Username'})) {
		$extraSQL .= ",Username";
		$extraParams .= ",".DBQuote($auditTrailInfo->{'Username'});
	}
	if (defined($auditTrailInfo->{'ServerCluster'})) {
		$extraSQL .= ",ServerCluster";
		$extraParams .= ",".DBQuote($auditTrailInfo->{'ServerCluster'});
	}
	if (defined($auditTrailInfo->{'ServerName'})) {
		$extraSQL .= ",ServerName";
		$extraParams .= ",".DBQuote($auditTrailInfo->{'ServerName'});
	}
	if (defined($auditTrailInfo->{'Module'})) {
		$extraSQL .= ",Module";
		$extraParams .= ",".DBQuote($auditTrailInfo->{'Module'});
	}
	if (defined($auditTrailInfo->{'Function'})) {
		$extraSQL .= ",Function";
		$extraParams .= ",".DBQuote($auditTrailInfo->{'Function'});
	}
	if (defined($auditTrailInfo->{'Parameters'})) {
		$extraSQL .= ",Parameters";
		$extraParams .= ",".DBQuote($auditTrailInfo->{'Parameters'});
	}
	if (defined($auditTrailInfo->{'Result'})) {
		$extraSQL .= ",Result";
		$extraParams .= ",".DBQuote($auditTrailInfo->{'Result'});
	}

	# Current timestamp
	my $now = time();

	# Insert user data
	my $sth = DBDo("
			INSERT INTO auditTrails
				(Timestamp,ClientIP,Protocol $extraSQL)
			VALUES (".
					DBQuote($now).",".
					DBQuote($auditTrailInfo->{'ClientIP'}).",".
					DBQuote($auditTrailInfo->{'Protocol'}).
					$extraParams.
			")
	");
	if (!defined($sth)) {
		DBRollback();
		return "[AUDITTRAILS] Database error: ".awitpt::db::dblayer::Error();
	}

	my $ID = DBLastInsertID("auditTrails","ID");
	if (!defined($ID)) {
		return "[AUDITTRAILS] Database error: ".awitpt::db::dblayer::Error();
	}

	return $ID;
}


## @method getAuditTrails($auditTrailsInfo,$search)
# Return audit trail information
#
# @param auditTrailsInfo Audit Trail info hash ref
# @li AgentID - Optional agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Audit Trail ID
# @li Timestamp - Timestamp of transaction
# @li ClientIP - Client's IP
# @li Protocol - Protocol used
# @li AgentID - Agent ID
# @li Username - User name
# @li ServerCluster - Server cluster used
# @li ServerName - Server used
# @li Module - Module used 
# @li Function - Function accessed
# @li Parameters - Function parameters
# @li Result - Function result
sub getAuditTrails
{
	my ($auditTrailsInfo,$search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Check for agent ID restriction
	my $extraSQL = "";
	if (defined($auditTrailsInfo)) {
		# Make sure we're a hash
		if (!isHash($auditTrailsInfo)) {
			setError("Parameter 'auditTrailsInfo' is not a HASH");
			return ERR_PARAM;
		}
		# If we have an agent ID, use it
		if (defined($auditTrailsInfo->{'AgentID'})) {
			if (!defined($auditTrailsInfo->{'AgentID'} = isNumber($auditTrailsInfo->{'AgentID'}))) {
				setError("Parameter 'auditTrailsInfo' element 'AgentID'' is invalid");
				return ERR_PARAM;
			}
			$extraSQL = "WHERE AgentID = ".DBQuote($auditTrailsInfo->{'AgentID'});
		}
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'ID',
		'Timestamp' => 'Timestamp',
		'ClientIP' => 'ClientIP',
		'Protocol' => 'Protocol',
		'AgentID' => 'AgentID',
		'Username' => 'Username',
		'ServerCluster' => 'ServerCluster',
		'ServerName' => 'ServerName',
		'Module' => 'Module',
		'Function' => 'Function'
	};

	# Select all audit trails
	my ($sth,$numResults) = DBSelectSearch("
			SELECT 
				ID,
				Timestamp,
				ClientIP,
				Protocol,
				AgentID,
				Username,
				ServerCluster,
				ServerName,
				Module,
				Function,
				Parameters,
				Result
			FROM 
				auditTrails
			$extraSQL
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Loop with results
	my @auditTrails = ();
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID Timestamp ClientIP Protocol AgentID Username ServerCluster ServerName Module Function Parameters Result))
	) {
		my $item;
		$item->{'ID'} = $row->{'ID'};
		$item->{'Timestamp'} = $row->{'Timestamp'};
		$item->{'ClientIP'} = $row->{'ClientIP'};
		$item->{'Protocol'} = $row->{'Protocol'};
		$item->{'AgentID'} = $row->{'AgentID'};
		$item->{'Username'} = $row->{'Username'};
		$item->{'ServerCluster'} = $row->{'ServerCluster'};
		$item->{'ServerName'} = $row->{'ServerName'};
		$item->{'Module'} = $row->{'Module'};
		$item->{'Function'} = $row->{'Function'};
		$item->{'Parameters'} = $row->{'Parameters'};
		$item->{'Result'} = $row->{'Result'};
		push(@auditTrails,$item);
	}

	DBFreeRes($sth);

	return (\@auditTrails,$numResults);
}


## @method getAuditTrail($auditTrailID)
# Return audit trail info hash
#
# @param augitTrailID Audit Trail ID
#
# @return Audit Trail info hash ref
# @li ID - Audit Trail ID
# @li Timestamp - Timestamp of transaction
# @li ClientIP - Client's IP
# @li Protocol - Protocol used
# @li AgentID - Agent ID
# @li Username - User name
# @li ServerCluster - Server cluster used
# @li ServerName - Server used
# @li Module - Module used
# @li Function - Function accessed
# @li Parameters - Function parameters
# @li Result - Function result
sub getAuditTrail
{
	my $auditTrailID = shift;


	if (!defined($auditTrailID)) {
		setError("Parameter 'auditTrailID' not defined");
		return ERR_PARAM;
	}
	if (!defined($auditTrailID = isNumber($auditTrailID))) {
		setError("Parameter 'auditTrailID' is invalid");
		return ERR_PARAM;
	}

	# Query audit trail
	my $sth = DBSelect("
		SELECT
			ID, Timestamp, ClientIP, Protocol, AgentID, Username, ServerCluster, ServerName, Module, Function, Parameters, Result
		FROM
			auditTrails
		WHERE
			ID = ".DBQuote($auditTrailID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID Timestamp ClientIP Protocol AgentID Username ServerCluster ServerName 
				Module Function Parameters Result ));

	my $res;
	$res->{'ID'} = $row->{'ID'};
	$res->{'Timestamp'} = $row->{'Timestamp'};
	$res->{'ClientIP'} = $row->{'ClientIP'};
	$res->{'Protocol'} = $row->{'Protocol'};
	$res->{'AgentID'} = $row->{'AgentID'};
	$res->{'Username'} = $row->{'Username'};
	$res->{'ServerCluster'} = $row->{'ServerCluster'};
	$res->{'ServerName'} = $row->{'ServerName'};
	$res->{'Module'} = $row->{'Module'};
	$res->{'Function'} = $row->{'Function'};
	$res->{'Parameters'} = $row->{'Parameters'};
	$res->{'Result'} = $row->{'Result'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;
}


1;
# vim: ts=4
