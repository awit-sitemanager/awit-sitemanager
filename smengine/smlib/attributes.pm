# Attribute handling functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::attributes
# Attribute handling functions
package smlib::attributes;

use strict;
use warnings;

# Exporter stuff
require Exporter;
our (@ISA,@EXPORT,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT = qw(
);
@EXPORT_OK = qw(
);



## @fn processAttribute($cur,$operator,$value)
# @param curValue Current value
# @param operator Operator
# @li '=' - Set value
# @li '+=' - Add value to arrayref
#
# @return Returns new value
sub processAttribute
{
	my ($curValue,$operator,$value) = @_;


#	use Data::Dumper; print STDERR "processAttribute() => ".Dumper($curValue,$operator,$value);
	# Normal =
	if ($operator eq "=") {
		$curValue = $value;

	# Add to array
	} elsif ($operator eq "+=") {
		# If its defined
		if (defined($curValue)) {
			# If its already an arrayref
			if (ref($curValue) eq "ARRAY") {
				# Just push a value onto it
				push(@{$curValue},$value);
			# If not remake it an array ref with 2 values
			} else {
				$curValue = [ $curValue, $value ]
			}
		} else {
			# Just make an array ref
			$curValue = [ $value ];
		}
	}

	return $curValue;
}


## @fn attributeHasValue($attribute,$value)
# @param attribute Attribute to test value against
# @param value Value to test against attribute
#
# @return 0 on fail, 1 on success
sub attributeHasValue
{
	my ($attribute,$value) = @_;

	my $found = 0;


	# We cannot compare an undefined value
	if (!defined($value)) {
		return 0;
	}

	# Check if we an array
	if (ref($attribute) eq "ARRAY") {
		# If we are process and check each item
		foreach my $item (@{$attribute}) {
			if ($item eq $value) {
				$found = 1;
			}
		}
	} else {
		if ($attribute eq $value) {
			$found = 1;
		}
	}

	return $found;
}



1;
# vim: ts=4
