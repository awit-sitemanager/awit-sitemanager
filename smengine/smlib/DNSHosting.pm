# Zone functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class smlib::DNSHosting
# DNS zone handling class
package smlib::DNSHosting;


use strict;
use warnings;

use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::system qw(hashPassword);
use smlib::util;

use POSIX qw(strftime);

use smlib::DNSHosting::DDNS;


# Use the smlib servers autoloader so we can get remote calls working nicely
sub AUTOLOAD { our $AUTOLOAD; return smlib::servers::RPC_AUTOLOADER($AUTOLOAD,@_); }


## @method createDNSZone($zoneInfo)
# Add a zone
#
# @param zoneInfo Zone info hash ref
# @li AgentID - Agent ID
# @li DomainName - Domain name
# @li NameserverID - Primary nameserver ID
# @li AdminEmail - Zone admin email address
# @li Refresh - Zone SOA refresh
# @li Retry - Zone SOA retry
# @li Expire - Zone SOA expire
# @li MinTTL - Zone SOA minimum TTL
# @li TTL - Zone SOA TTL
# @li AgentDisabled - Flag to indicate if agent disabled zone
# @li AgentRef - Agent reference 
#
# @return Zone ID
sub _createDNSZone
{
	my $zoneInfo = shift;


	if (!defined($zoneInfo)) {
		setError("Parameter 'zoneInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($zoneInfo)) {
		setError("Parameter 'zoneInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($zoneInfo->{'AgentID'})) {
		setError("Parameter 'zoneInfo' element 'AgentID' not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneInfo->{'AgentID'} = isNumber($zoneInfo->{'AgentID'}))) {
		setError("Parameter 'zoneInfo' element 'AgentID' is invalid");
		return ERR_PARAM;
	}
	if (!defined($zoneInfo->{"DomainName"})) {
		setError("Parameter 'zoneInfo' has no element 'DomainName'");
		return ERR_PARAM;
	}
	if (!isVariable($zoneInfo->{"DomainName"})) {
		setError("Parameter 'zoneInfo' element 'DomainName' is invalid");
		return ERR_PARAM;
	}
	if ($zoneInfo->{"DomainName"} eq "") {
		setError("Parameter 'zoneInfo' element 'DomainName' is blank");
		return ERR_PARAM;
	}
	$zoneInfo->{'DomainName'} = lc($zoneInfo->{'DomainName'});
	if (!($zoneInfo->{'DomainName'} =~ /^[a-z0-9_\-\.]+$/)) {
		setError("Parameter 'zoneInfo' element 'DomainName' is blank");
		return ERR_PARAM;
	}
	if (!defined($zoneInfo->{"NameserverID"})) {
		setError("Parameter 'zoneInfo' has no element 'NameserverID'");
		return ERR_PARAM;
	}
	if (!defined($zoneInfo->{'NameserverID'} = isNumber($zoneInfo->{'NameserverID'}))) {
		setError("Parameter 'zoneInfo' element 'NameserverID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($zoneInfo->{'AdminEmail'})) {
		setError("Parameter 'zoneInfo' has no element 'AdminEmail'");
		return ERR_PARAM;
	}
	if (!isVariable($zoneInfo->{"AdminEmail"})) {
		setError("Parameter 'zoneInfo' element 'AdminEmail' is invalid");
		return ERR_PARAM;
	}
	if ($zoneInfo->{"AdminEmail"} eq "") {
		setError("Parameter 'zoneInfo' element 'AdminEmail' is blank");
		return ERR_PARAM;
	}
	$zoneInfo->{'AdminEmail'} = lc($zoneInfo->{'AdminEmail'});
	if (!($zoneInfo->{'AdminEmail'} =~ /^\S+@\S+$/)) {
		setError("Parameter 'zoneInfo' element 'AdminEmail' is invalid");
		return ERR_PARAM;
	}

	if (defined($zoneInfo->{'Refresh'}) && !defined($zoneInfo->{'Refresh'} = isNumber($zoneInfo->{'Refresh'}))) {
		setError("Parameter 'zoneInfo' element 'Refresh' is invalid");
		return ERR_PARAM;
	}
	if (defined($zoneInfo->{'Retry'}) && !defined($zoneInfo->{'Retry'} = isNumber($zoneInfo->{'Retry'}))) {
		setError("Parameter 'zoneInfo' element 'Retry' is invalid");
		return ERR_PARAM;
	}
	if (defined($zoneInfo->{'Expire'}) && !defined($zoneInfo->{'Expire'} = isNumber($zoneInfo->{'Expire'}))) {
		setError("Parameter 'zoneInfo' element 'Expire' is invalid");
		return ERR_PARAM;
	}
	if (defined($zoneInfo->{'MinTTL'}) && !defined($zoneInfo->{'MinTTL'} = isNumber($zoneInfo->{'MinTTL'}))) {
		setError("Parameter 'zoneInfo' element 'MinTTL' is invalid");
		return ERR_PARAM;
	}
	if (defined($zoneInfo->{'TTL'}) && !defined($zoneInfo->{'TTL'} = isNumber($zoneInfo->{'TTL'}))) {
		setError("Parameter 'zoneInfo' element 'TTL' is invalid");
		return ERR_PARAM;
	}
	if (defined($zoneInfo->{'AgentRef'}) && !isVariable($zoneInfo->{"AgentRef"})) {
		setError("Parameter 'zoneInfo' element 'AgentRef' is invalid");
		return ERR_PARAM;
	}
	if (defined($zoneInfo->{'AgentDisabled'}) && 
			!defined($zoneInfo->{'AgentDisabled'} = isBoolean($zoneInfo->{'AgentDisabled'}))) {
		setError("Parameter 'zoneInfo' element 'AgentDisabled' has an invalid value");
		return ERR_PARAM;
	}


	# Verify zone doesn't clash
	my $num_results = DBSelectNumResults("FROM zones WHERE origin = ".DBQuote($zoneInfo->{'DomainName'}."."));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results > 0) {
		setError("Zone '".$zoneInfo->{'DomainName'}."' already exists");
		return ERR_EXISTS;
	}

	# Translate between ID and HostName
	my $primaryNSName;

	my $sth = DBSelect("SELECT ID, Type, HostName FROM systemNameservers");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Find primary and secondary NS
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Type HostName ))) {
		# Check for primary match
		if ($row->{'ID'} == $zoneInfo->{'NameserverID'} && ($row->{'Type'} == 1 || $row->{'Type'} == 2)) {
			$primaryNSName = $row->{'HostName'};
			last;
		}
	}

	DBFreeRes($sth);
	
	if (!defined($primaryNSName)) {
		setError("Parameter 'zoneInfo' element 'NameserverID' not found");
		return ERR_PARAM;
	}

	# Generate initial serisl
	my $serial = strftime('%Y%m%d01',localtime(time));

	# Clean up admin email address
	(my $adminEmail = $zoneInfo->{'AdminEmail'} . ".") =~ s/@/\./;

	# Check if we doing advanced things...
	my $extraColumns = "";
	my $extraValues = "";

	if (defined($zoneInfo->{'Refresh'})) {
		$extraColumns .= ", refresh";
		$extraValues .= ", ".DBQuote($zoneInfo->{'Refresh'});
	}

	if (defined($zoneInfo->{'Retry'})) {
		$extraColumns .= ", retry";
		$extraValues .= ", ".DBQuote($zoneInfo->{'Retry'});
	}

	if (defined($zoneInfo->{'Expire'})) {
		$extraColumns .= ", expire";
		$extraValues .= ", ".DBQuote($zoneInfo->{'Expire'});
	}

	if (defined($zoneInfo->{'MinTTL'})) {
		$extraColumns .= ", minimum";
		$extraValues .= ", ".DBQuote($zoneInfo->{'MinTTL'});
	}

	if (defined($zoneInfo->{'TTL'})) {
		$extraColumns .= ", ttl";
		$extraValues .= ", ".DBQuote($zoneInfo->{'TTL'});
	}

	if (defined($zoneInfo->{'AgentRef'})) {
		$extraColumns .= ",AgentRef";
		$extraValues .= ",".DBQuote($zoneInfo->{'AgentRef'});
	}

	# XXX: HACK TO TRANSLATE DISABLED INTO ACTIVE
	if (defined($zoneInfo->{'AgentDisabled'})) {
		$extraColumns .= ",active";
		$extraValues .= ",".DBQuote($zoneInfo->{'AgentDisabled'} ? 'N' : 'Y');
	}


	# Begin our inserts
	DBBegin();

	# Insert zone
	$sth = DBDo("
			INSERT INTO zones
				(
					AgentID,
					origin, ns, mbox, serial
					$extraColumns
				)
			VALUES (".
					DBQuote($zoneInfo->{'AgentID'}).",".

					DBQuote($zoneInfo->{'DomainName'}.".").",".
					DBQuote($primaryNSName.".").",".
					DBQuote($adminEmail).",".
					DBQuote($serial)."
					$extraValues
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Get last inserted ID
	my $ID = DBLastInsertID('zones','id');
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}


## @method updateDNSZone($zoneInfo)
# Update a zone
#
# @param zoneInfo Zone info hash ref
# @li ID - Zone ID
# @li AgentID - Optional agent ID
# @li DomainName - Optional domain name
# @li NameserverID - Optional primary nameserver
# @li AdminEmail - Optional admin email address
# @li Refresh - Optional SOA refresh
# @li Retry - Optional SOA retry
# @li Expire - Optional SOA expire
# @li MinTTL - Optinal SOA minimum TTL
# @li TTL - Optinal SOA default TTL
# @li AgentDisabled - Optional flag to indicate agent disabled zone
# @li AgentRef - Optional agent reference 
#
# @return 0 on success or < 0 on error
sub _updateDNSZone
{
	my $zoneInfo = shift;


	if (!defined($zoneInfo)) {
		setError("Parameter 'zoneInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($zoneInfo)) {
		setError("Parameter 'zoneInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($zoneInfo->{"ID"})) {
		setError("Parameter 'zoneInfo' has no element 'ID'");
		return ERR_PARAM;
	}
	if (!defined($zoneInfo->{'ID'} = isNumber($zoneInfo->{'ID'}))) {
		setError("Parameter 'zoneInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}
	if (defined($zoneInfo->{'AgentID'})) {
		if (!defined($zoneInfo->{'AgentID'} = isNumber($zoneInfo->{'AgentID'}))) {
			setError("Parameter 'zoneInfo' element 'AgentID' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneInfo->{'DomainName'})) {
		if (!isVariable($zoneInfo->{"DomainName"})) {
			setError("Parameter 'zoneInfo' element 'DomainName' is invalid");
			return ERR_PARAM;
		}
		if ($zoneInfo->{"DomainName"} eq "") {
			setError("Parameter 'zoneInfo' element 'DomainName' is blank");
			return ERR_PARAM;
		}
		$zoneInfo->{'DomainName'} = lc($zoneInfo->{'DomainName'});
		if (!($zoneInfo->{'DomainName'} =~ /^[a-z0-9_\-\.]+$/)) {
			setError("Parameter 'zoneInfo' element 'DomainName' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneInfo->{'NameserverID'})) {
		if (!defined($zoneInfo->{'NameserverID'} = isNumber($zoneInfo->{'NameserverID'}))) {
			setError("Parameter 'zoneInfo' element 'NameserverID' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneInfo->{"AdminEmail"})) {
		if (!isVariable($zoneInfo->{"AdminEmail"})) {
			setError("Parameter 'zoneInfo' element 'AdminEmail' is invalid");
			return ERR_PARAM;
		}
		if ($zoneInfo->{"AdminEmail"} eq "") {
			setError("Parameter 'zoneInfo' element 'AdminEmail' is blank");
			return ERR_PARAM;
		}
		$zoneInfo->{'AdminEmail'} = lc($zoneInfo->{'AdminEmail'});
		if (!($zoneInfo->{'AdminEmail'} =~ /^\S+@\S+$/)) {
			setError("Parameter 'zoneInfo' element 'AdminEmail' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneInfo->{'Refresh'})) {
		if (!defined($zoneInfo->{'Refresh'} = isNumber($zoneInfo->{'Refresh'}))) {
			setError("Parameter 'zoneInfo' element 'Refresh' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneInfo->{'Retry'})) {
		if (!defined($zoneInfo->{'Retry'} = isNumber($zoneInfo->{'Retry'}))) {
			setError("Parameter 'zoneInfo' element 'Retry' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneInfo->{'Expire'})) {
		if (!defined($zoneInfo->{'Expire'} = isNumber($zoneInfo->{'Expire'}))) {
			setError("Parameter 'zoneInfo' element 'Expire' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneInfo->{'MinTTL'})) {
		if (!defined($zoneInfo->{'MinTTL'} = isNumber($zoneInfo->{'MinTTL'}))) {
			setError("Parameter 'zoneInfo' element 'MinTTL' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneInfo->{'TTL'})) {
		if (!defined($zoneInfo->{'TTL'} = isNumber($zoneInfo->{'TTL'}))) {
			setError("Parameter 'zoneInfo' element 'TTL' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneInfo->{'AgentRef'})) {
		if (!isVariable($zoneInfo->{"AgentRef"})) {
			setError("Parameter 'zoneInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneInfo->{"AgentDisabled"})) {
		if (!defined($zoneInfo->{'AgentDisabled'} = isBoolean($zoneInfo->{"AgentDisabled"}))) {
			setError("Parameter 'zoneInfo' element 'AgentDisabled' has an invalid value");
			return ERR_PARAM;
		}
	}

	# Updates
	my @updates;

	# Pull in changes
	if (defined($zoneInfo->{'AgentID'})) {
		push(@updates,"AgentID = ".DBQuote($zoneInfo->{'AgentID'}));
	}

	if (defined($zoneInfo->{'DomainName'})) {
		push(@updates,"origin = ".DBQuote($zoneInfo->{'DomainName'} . "."));
	}

	if (defined($zoneInfo->{'NameserverID'})) {
		# Translate between ID and HostName
		my $primaryNSName;

		my $sth = DBSelect("SELECT ID, Type, HostName FROM systemNameservers");
		if (!defined($sth)) {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}

		# Find primary
		while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Type HostName ))) {
			# Check for primary match
			if ($row->{'ID'} == $zoneInfo->{'NameserverID'} && ($row->{'Type'} == 1 || $row->{'Type'} == 2)) {
				$primaryNSName = $row->{'HostName'};
				last;
			}
		}

		DBFreeRes($sth);
	
		if (!defined($primaryNSName)) {
			setError("Parameter 'zoneInfo' element 'NameserverID' not found");
			return ERR_PARAM;
		}
		
		push(@updates,"ns = ".DBQuote($primaryNSName."."));
	}

	if (defined($zoneInfo->{'AdminEmail'})) {
		# Clean up admin email address
		(my $adminEmail = $zoneInfo->{'AdminEmail'} . ".") =~ s/@/\./;

		push(@updates,"mbox = ".DBQuote($adminEmail));
	}

	if (defined($zoneInfo->{'Refresh'})) {
		push(@updates,"refresh = ".DBQuote($zoneInfo->{'Refresh'}));
	}

	if (defined($zoneInfo->{'Retry'})) {
		push(@updates,"retry = ".DBQuote($zoneInfo->{'Retry'}));
	}

	if (defined($zoneInfo->{'Expire'})) {
		push(@updates,"expire = ".DBQuote($zoneInfo->{'Expire'}));
	}

	if (defined($zoneInfo->{'MinTTL'})) {
		push(@updates,"minimum = ".DBQuote($zoneInfo->{'MinTTL'}));
	}

	if (defined($zoneInfo->{'TTL'})) {
		push(@updates,"ttl = ".DBQuote($zoneInfo->{'TTL'}));
	}

	if (defined($zoneInfo->{'AgentRef'})) {
		push(@updates,"AgentRef = ".DBQuote($zoneInfo->{'AgentRef'}));
	}

	if (defined($zoneInfo->{"AgentDisabled"})) {
		# XXX: HACK TO TRANSLATE DISABLED INTO ACTIVE
		push(@updates,"active = ".DBQuote($zoneInfo->{'AgentDisabled'} ? 'N' : 'Y'));
	}

	# Grab zone to see if it exists, and to get the serial
	my $zone = _getDNSZone($zoneInfo->{'ID'});
	if (!isHash($zone)) {
		return $zone;
	}

	# Update serial
	push(@updates,"Serial = ".DBQuote(getNewDNSZoneSerial($zoneInfo->{'Serial'})));

	# Create and check update string
	my $updateStr = join(",",@updates);
	if ($updateStr eq "") {
		setError("No updates specified for zone");
		return ERR_USAGE;
	}

	# Begin our update
	DBBegin();

	# Update zone
	my $sth = DBDo("
			UPDATE
				zones
			SET
				$updateStr
			WHERE
				id = ".DBQuote($zone->{'ID'})
	);
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}


## @method removeDNSZone($zoneID)
# Remove a zone
#
# @param zoneID Zone ID
#
# @return 0 on success, < 0 on error
sub _removeDNSZone
{
	my $zoneID = shift;


	if (!defined($zoneID)) {
		setError("Parameter 'zoneID' not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneID = isNumber($zoneID))) {
		setError("Parameter 'zoneID' is invalid");
		return ERR_PARAM;
	}

	# Check if zone exists
	my $num_results = DBSelectNumResults("FROM zones WHERE id = ".DBQuote($zoneID));
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}
	if ($num_results < 1) {
		setError("Zone ID '$zoneID' does not exist");
		return ERR_EXISTS;
	}

	# Grab zone to see if it exists
	my $zone = _getDNSZone($zoneID);
	if (!isHash($zone)) {
		return $zone;
	}

	DBBegin();

	# Delete zone data from database
	my $sth = DBDo("DELETE FROM zoneEntries WHERE zone = ".DBQuote($zoneID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	$sth = DBDo("DELETE FROM zones WHERE id = ".DBQuote($zoneID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}


## @method getDNSZones($zoneInfo,$search)
# Return an array of zones
#
# @param zoneInfo Zone info hash ref
# @li AgentID - Optional agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Zone ID
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li DomainName - Domain name
# @li NameserverHostname - Primary nameserver
# @li AdminEmail - Zone admin email address
# @li Serial - Zone SOA serial
# @li Refresh - Zone SOA refresh
# @li Retry - Zone SOA retry
# @li Expire - Zone SOA expire
# @li MinTTL - Zone SOA minimum TTL
# @li TTL - Zone SOA TTL
# @li Disabled - Set if zone is disabled
# @li AgentDisabled - Flag to indicate if zone is disabled by agent or not
# @li AgentRef - Agent reference
sub _getDNSZones
{
	my ($zoneInfo,$search) = @_;


	my $extraSQL = "";

	if (defined($zoneInfo)) {
		# Make sure we're a hash
		if (!isHash($zoneInfo)) {
			setError("Parameter 'zoneInfo' is not a HASH");
			return ERR_PARAM;
		}
		# If we have an agent ID, use it
		if (defined($zoneInfo->{'AgentID'})) {
			if (!defined($zoneInfo->{'AgentID'} = isNumber($zoneInfo->{'AgentID'}))) {
				setError("Parameter 'zoneInfo' element 'AgentID'' is invalid");
				return ERR_PARAM;
			}
			$extraSQL = "AND zones.AgentID = ".DBQuote($zoneInfo->{'AgentID'});
		}
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'zones.ID',
		'AgentID' => 'zones.AgentID',
		'DomainName' => 'zones.origin',
		'AgentRef' => 'zones.AgentRef'
	};

	# Select zones
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				zones.id,
				zones.AgentID,
				zones.origin,
				zones.ns,
				zones.mbox,
				zones.serial,
				zones.refresh,
				zones.retry,
				zones.expire,
				zones.minimum,
				zones.ttl,
				zones.active,
				zones.xfer,
				zones.AgentRef,
				agents.Name AS AgentName
			FROM
				zones, agents
			WHERE
				agents.ID = zones.AgentID
				$extraSQL
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}
	# Fetch rows
	my @zones = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw(	ID AgentID AgentName Origin NS MBox Serial Refresh Retry Expire Minimum TTL	Active AgentRef )
			)
	) {
		my $entry;

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'AgentID'} = $row->{'AgentID'};
		$entry->{'AgentName'} = $row->{'AgentName'};
		($entry->{'DomainName'} = $row->{'Origin'}) =~ s/\.$//;
		($entry->{'NameserverHostname'} = $row->{'NS'}) =~ s/\.$//;
		($entry->{'AdminEmail'} = $row->{'MBox'}) =~ s/\./@/;
		$entry->{'AdminEmail'} =~ s/\.$//;
		
		$entry->{'AgentRef'} = $row->{'AgentRef'};

		$entry->{'Serial'} = $row->{'Serial'};
		$entry->{'Refresh'} = $row->{'Refresh'};
		$entry->{'Retry'} = $row->{'Retry'};
		$entry->{'Expire'} = $row->{'Expire'};
		$entry->{'MinTTL'} = $row->{'Minimum'};
		$entry->{'TTL'} = $row->{'TTL'};

		# XXX: Convert Active to AgentDisabled
		$entry->{'AgentDisabled'} = ($row->{'Active'} eq 'N') ? 1 : 0;
		$entry->{'Disabled'} = $entry->{'AgentDisabled'};

		push(@zones,$entry);

	}

	DBFreeRes($sth);

	# Resolve nameserver ID's
	foreach my $zone (@zones) {
		# If we get a result, record the nameserver ID
		if (isHash(my $res = getDNSServerByHostname($zone->{'NameserverHostname'}))) {
			$zone->{'NameserverID'} = $res->{'ID'};
		}
	}

	return (\@zones,$numResults);
}


## @method getDNSZone($zoneID)
# Return an hash for the relevant zone
#
# @param zoneID Zone ID
#
# @return Hash ref containing zone info
# @li ID - Zone ID
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li DomainName - Domain name
# @li NameserverID - Nameserver ID
# @li NameserverHostname - Nameserver hostname
# @li AdminEmail - Zone admin email address
# @li Serial - Zone SOA serial
# @li Refresh - Zone SOA refresh
# @li Retry - Zone SOA retry
# @li Expire - Zone SOA expire
# @li MinTTL - Zone SOA minimum TTL
# @li TTL - Zone SOA TTL
# @li Disabled - If this item is disabled
# @li AgentDisabled - If this item has been disabled by the agent
# @li AgentRef - Agent reference
sub _getDNSZone
{
	my $zoneID = shift;


	if (!defined($zoneID)) {
		setError("Parameter 'zoneID' not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneID = isNumber($zoneID))) {
		setError("Parameter 'zoneID' is invalid");
		return ERR_PARAM;
	}

	# Grab zone from database
	my $sth = DBSelect("
		SELECT
			zones.id, 
			zones.AgentID, 
			zones.origin, zones.ns, zones.mbox, 
			zones.serial, 
			zones.refresh, zones.retry, zones.expire, zones.minimum, zones.ttl, zones.active, zones.xfer, 
			zones.AgentRef,
			agents.Name AS AgentName
		FROM
			zones, agents
		WHERE
			zones.id = ".DBQuote($zoneID)."
			AND agents.ID = zones.AgentID
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results & finish off
	if ($sth->rows < 1) {
		setError("Failed to find zone ID '$zoneID'");
		return ERR_NOTFOUND;
	}

	my $row = hashifyLCtoMC(
			$sth->fetchrow_hashref(),
			qw( ID AgentID Origin NS MBox Serial Refresh Retry Expire Minimum TTL Active Xfer AgentRef )
	);
	DBFreeRes($sth);

	my %zoneInfo = ();

	# Pull out info
	$zoneInfo{'ID'} = $row->{'ID'};

	$zoneInfo{'AgentID'} = $row->{'AgentID'};

	($zoneInfo{'DomainName'} = $row->{'Origin'}) =~ s/\.$//;

	($zoneInfo{'NameserverHostname'} = $row->{'NS'}) =~ s/\.$//;
	if (isHash(my $res = getDNSServerByHostname($zoneInfo{'NameserverHostname'}))) {
		$zoneInfo{'NameserverID'} = $res->{'ID'};
	}

	# Sanitize email addy
	($zoneInfo{'AdminEmail'} = $row->{'MBox'}) =~ s/\./@/;
	$zoneInfo{'AdminEmail'} =~ s/\.$//;

	$zoneInfo{'Serial'} = $row->{'Serial'};
	$zoneInfo{'Refresh'} = $row->{'Refresh'};
	$zoneInfo{'Retry'} = $row->{'Retry'};
	$zoneInfo{'Expire'} = $row->{'Expire'};
	$zoneInfo{'MinTTL'} = $row->{'Minimum'};
	$zoneInfo{'TTL'} = $row->{'TTL'};
	
	$zoneInfo{'AgentRef'} = $row->{'AgentRef'};

	# XXX: Convert Active to AgentDisabled
	$zoneInfo{'AgentDisabled'} = ($row->{'Active'} eq 'N') ? 1 : 0;
	$zoneInfo{'Disabled'} = $zoneInfo{'AgentDisabled'};

	return \%zoneInfo;
}


## @method createDNSZoneEntry($zoneEntryInfo)
# Create a zone entry
#
# @param zoneEntryInfo Zone entry info hash ref
# @li ZoneID - Zone ID
# @li Name - Optional Name
# @li Type - Type
# @li Data - Data
# @li Aux - Optional Aux
# @li TTL - Optional TTL
# @li DDNSUserID - Dynamic DNS user ID
# @li AgentDisabled - If this item has been disabled by the agent
# @li AgentRef - Agent reference
#
# @return Zone entry ID
sub _createDNSZoneEntry
{
	my $zoneEntryInfo = shift;


	if (!defined($zoneEntryInfo)) {
		setError("Parameter 'zoneEntryInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($zoneEntryInfo)) {
		setError("Parameter 'zoneEntryInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($zoneEntryInfo->{'ZoneID'})) {
		setError("Parameter 'zoneEntryInfo' element 'ZoneID' not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneEntryInfo->{'ZoneID'} = isNumber($zoneEntryInfo->{'ZoneID'}))) {
		setError("Parameter 'zoneEntryInfo' element 'ZoneID' is invalid");
		return ERR_PARAM;
	}
	# Blank name if the commonly used @ is supplied
	if (!defined($zoneEntryInfo->{'Name'})) {
		$zoneEntryInfo->{'Name'} = "";
	} else {
		if (!isVariable($zoneEntryInfo->{"Name"})) {
			setError("Parameter 'zoneEntryInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
 		if ($zoneEntryInfo->{'Name'} eq "@") {
			$zoneEntryInfo->{'Name'} = "";
		}
	}
	# Lowercase the name
	$zoneEntryInfo->{'Name'} = lc($zoneEntryInfo->{"Name"});
	# Check validity
	if (!($zoneEntryInfo->{'Name'} =~ /^[a-z0-9_\-\.]*$/)) {
		setError("Parameter 'zoneEntryInfo' element 'Name' is invalid");
		return ERR_PARAM;
	}
	if (!defined($zoneEntryInfo->{'Type'})) {
		setError("Parameter 'zoneEntryInfo' element 'Type' is not defined");
		return ERR_PARAM;
	}
	if (!isVariable($zoneEntryInfo->{"Type"})) {
		setError("Parameter 'zoneEntryInfo' element 'Type' is invalid");
		return ERR_PARAM;
	}
	if ($zoneEntryInfo->{"Type"} eq "") {
		setError("Parameter 'zoneEntryInfo' element 'Type' is invalid");
		return ERR_PARAM;
	}
	# Uppercase type
	$zoneEntryInfo->{'Type'} = uc($zoneEntryInfo->{'Type'});
	# Check for match
	my $typeValid = 0;
	foreach my $type (('A','AAAA','CNAME','HINFO','MX','NS','PTR','RP','SPF','SRV','TXT')) {
		if ($zoneEntryInfo->{'Type'} eq $type) {
			$typeValid = 1;
			last;
		}
	}
	# Bitch if wrong
	if (!$typeValid) {
		setError("Parameter 'zoneEntryInfo' element 'Type' is invalid");
		return ERR_PARAM;
	}

	if (!defined($zoneEntryInfo->{'Data'})) {
		setError("Parameter 'zoneEntryInfo' element 'Data' is not defined");
		return ERR_PARAM;
	}
	if (!isVariable($zoneEntryInfo->{"Data"})) {
		setError("Parameter 'zoneEntryInfo' element 'Data' is invalid");
		return ERR_PARAM;
	}
	if ($zoneEntryInfo->{"Data"} eq "") {
		setError("Parameter 'zoneEntryInfo' element 'Data' is blank");
		return ERR_PARAM;
	}
	if (defined($zoneEntryInfo->{'Aux'})) {
		if (!defined($zoneEntryInfo->{'Aux'} = isNumber($zoneEntryInfo->{'Aux'},ISNUMBER_ALLOW_ZERO))) {
			setError("Parameter 'zoneEntryInfo' element 'Aux' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{'TTL'})) {
		if (!defined($zoneEntryInfo->{'TTL'} = isNumber($zoneEntryInfo->{'TTL'}))) {
			setError("Parameter 'zoneEntryInfo' element 'TTL' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{'DDNSUserID'})) {
		if (!isVariable($zoneEntryInfo->{'DDNSUserID'})) {
			setError("Parameter 'zoneEntryInfo' element 'DDNSUserID' is invalid");
			return ERR_PARAM;
		}
		# If the DDNS user ID is blank, we want to remove it
		if ($zoneEntryInfo->{'DDNSUserID'} eq "") {
			$zoneEntryInfo->{'DDNSUserID'} = 0;
		# Allow 0 aswell to remove DDNS user ID
		} elsif (!defined($zoneEntryInfo->{'DDNSUserID'} = isNumber($zoneEntryInfo->{'DDNSUserID'},ISNUMBER_ALLOW_ZERO))) {
			setError("Parameter 'zoneEntryInfo' element 'DDNSUserID' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{'AgentRef'})) {
		if (!isVariable($zoneEntryInfo->{"AgentRef"})) {
			setError("Parameter 'zoneEntryInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{'AgentDisabled'})) {
		if (!defined($zoneEntryInfo->{'AgentDisabled'} = isBoolean($zoneEntryInfo->{"AgentDisabled"}))) {
			setError("Parameter 'zoneEntryInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
	}

	my $extraColumns = "";
	my $extraValues = "";

	if (defined($zoneEntryInfo->{'Aux'})) {
		$extraColumns .= ",aux";
		$extraValues .= ",".DBQuote($zoneEntryInfo->{'Aux'});
	}

	if (defined($zoneEntryInfo->{'TTL'})) {
		$extraColumns .= ",ttl";
		$extraValues .= ",".DBQuote($zoneEntryInfo->{'TTL'});
	}

	if (defined($zoneEntryInfo->{'DDNSUserID'})) {
		$extraColumns .= ",DynaDNSUser";
		if ($zoneEntryInfo->{'DDNSUserID'} > 0) {
			$extraValues .= ",".DBQuote($zoneEntryInfo->{'DDNSUserID'});
		} else {
			$extraValues .= ",NULL";
		}
	}

	if (defined($zoneEntryInfo->{'AgentRef'})) {
		$extraColumns .= ",AgentRef";
		$extraValues .= ",".DBQuote($zoneEntryInfo->{'AgentRef'});
	}

	if (defined($zoneEntryInfo->{'AgentDisabled'})) {
		# XXX: HACK TO TRANSLATE DISABLED INTO ACTIVE
		$extraColumns .= ",active";
		$extraValues .= ",".DBQuote($zoneEntryInfo->{'AgentDisabled'} ? 'N' : 'Y');
	}

	DBBegin();

	# Insert zone entry
	my $sth = DBDo("
			INSERT INTO zoneEntries
				(zone,name,type,data$extraColumns)
			VALUES (".
					DBQuote($zoneEntryInfo->{'ZoneID'}).",".
					DBQuote($zoneEntryInfo->{'Name'}).",".
					DBQuote($zoneEntryInfo->{'Type'}).",".
					DBQuote($zoneEntryInfo->{'Data'}).
					$extraValues."
				)
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	my $ID = DBLastInsertID("zoneEntries","id");
	if (!defined($ID)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return $ID;
}


## @method updateDNSZoneEntry($zoneEntryInfo)
# Update a zone entry
#
# @param zoneEntryInfo Zone entry info hash ref
# @li ID - Entry ID
# @li Name - Optional name
# @li Type - Optional entry type
# @li Data - Optional data
# @li Aux - Optional auxillary data
# @li TTL - Optional TTL (time to live)
# @li DDNSUserID - Optional dynamic DNS user ID
# @li AgentDisabled - Optional flag agent can set to disable this DNS zone entry
# @li AgentRef - Optional agent reference
#
# @return 0 on success, < 0 on error
sub _updateDNSZoneEntry
{
	my $zoneEntryInfo = shift;


	if (!defined($zoneEntryInfo)) {
		setError("Parameter 'zoneEntryInfo' not defined");
		return ERR_PARAM;
	}
	if (!isHash($zoneEntryInfo)) {
		setError("Parameter 'zoneEntryInfo' is not a HASH");
		return ERR_PARAM;
	}
	if (!defined($zoneEntryInfo->{'ID'})) {
		setError("Parameter 'zoneEntryInfo' element 'ID' not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneEntryInfo->{'ID'} = isNumber($zoneEntryInfo->{'ID'}))) {
		setError("Parameter 'zoneEntryInfo' element 'ID' is invalid");
		return ERR_PARAM;
	}
	if (defined($zoneEntryInfo->{'Name'})) {
		# Blank name if the commonly used @ is supplied
		if (!isVariable($zoneEntryInfo->{"Name"})) {
			setError("Parameter 'zoneEntryInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
		if ($zoneEntryInfo->{'Name'} eq "@") {
			$zoneEntryInfo->{'Name'} = "";
		}
		# Lowercase the name
		$zoneEntryInfo->{'Name'} = lc($zoneEntryInfo->{"Name"});
		# Check validity
		if (!($zoneEntryInfo->{'Name'} =~ /^[a-z0-9_\-\.]*$/)) {
			setError("Parameter 'zoneEntryInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{'Type'})) {
		if (!isVariable($zoneEntryInfo->{"Type"})) {
			setError("Parameter 'zoneEntryInfo' element 'Type' is invalid");
			return ERR_PARAM;
		}
		if ($zoneEntryInfo->{"Type"} eq "") {
			setError("Parameter 'zoneEntryInfo' element 'Type' is blank");
			return ERR_PARAM;
		}
		# Uppercase type
		$zoneEntryInfo->{'Type'} = uc($zoneEntryInfo->{'Type'});
		# Check for match
		my $typeValid = 0;
		foreach my $type (('A','AAAA','CNAME','HINFO','MX','NS','PTR','RP','SPF','SRV','TXT')) {
			if ($zoneEntryInfo->{'Type'} eq $type) {
				$typeValid = 1;
				last;
			}
		}
		# Bitch if wrong
		if (!$typeValid) {
			setError("Parameter 'zoneEntryInfo' element 'Type' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{'Data'})) {
		if (!isVariable($zoneEntryInfo->{"Data"})) {
			setError("Parameter 'zoneEntryInfo' element 'Data' is invalid");
			return ERR_PARAM;
		}
		if ($zoneEntryInfo->{"Data"} eq "") {
			setError("Parameter 'zoneEntryInfo' element 'Data' is blank");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{'Aux'})) {
		if (!defined($zoneEntryInfo->{'Aux'} = isNumber($zoneEntryInfo->{'Aux'},ISNUMBER_ALLOW_ZERO))) {
			setError("Parameter 'zoneEntryInfo' element 'Aux' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{'TTL'})) {
		if (!defined($zoneEntryInfo->{'TTL'} = isNumber($zoneEntryInfo->{'TTL'}))) {
			setError("Parameter 'zoneEntryInfo' element 'TTL' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{'DDNSUserID'})) {
		if (!isVariable($zoneEntryInfo->{'DDNSUserID'})) {
			setError("Parameter 'zoneEntryInfo' element 'DDNSUserID' is invalid");
			return ERR_PARAM;
		}
		# If the DDNS user ID is blank, we want to remove it
		if ($zoneEntryInfo->{'DDNSUserID'} eq "") {
			$zoneEntryInfo->{'DDNSUserID'} = 0;
		# Allow 0 aswell to remove DDNS user ID
		} elsif (!defined($zoneEntryInfo->{'DDNSUserID'} = isNumber($zoneEntryInfo->{'DDNSUserID'},ISNUMBER_ALLOW_ZERO))) {
			setError("Parameter 'zoneEntryInfo' element 'DDNSUserID' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{'AgentRef'})) {
		if (!isVariable($zoneEntryInfo->{"AgentRef"})) {
			setError("Parameter 'zoneEntryInfo' element 'AgentRef' is invalid");
			return ERR_PARAM;
		}
	}
	if (defined($zoneEntryInfo->{"AgentDisabled"})) {
		if (!defined($zoneEntryInfo->{'AgentDisabled'} = isBoolean($zoneEntryInfo->{"AgentDisabled"}))) {
			setError("Parameter 'zoneEntryInfo' element 'AgentDisabled' is invalid");
			return ERR_PARAM;
		}
	}

	# Array of updates we will be doing
	my @updates = ();

	if (defined($zoneEntryInfo->{'Name'})) {
		push(@updates,"name = ".DBQuote($zoneEntryInfo->{'Name'}));
	}

	if (defined($zoneEntryInfo->{'Type'})) {
		push(@updates,"type = ".DBQuote($zoneEntryInfo->{'Type'}));
	}

	if (defined($zoneEntryInfo->{'Data'})) {
		push(@updates,"data = ".DBQuote($zoneEntryInfo->{'Data'}));
	}

	if (defined($zoneEntryInfo->{'Aux'})) {
		push(@updates,"aux = ".DBQuote($zoneEntryInfo->{'Aux'}));
	}

	if (defined($zoneEntryInfo->{'TTL'})) {
		push(@updates,"ttl = ".DBQuote($zoneEntryInfo->{'TTL'}));
	}

	if (defined($zoneEntryInfo->{'DDNSUserID'})) {
		if ($zoneEntryInfo->{'DDNSUserID'} > 0) {
			push(@updates,"DynaDNSUser = ".DBQuote($zoneEntryInfo->{'DDNSUserID'}));
		} else {
			push(@updates,"DynaDNSUser = NULL");
		}
	}

	if (defined($zoneEntryInfo->{'AgentRef'})) {
		push(@updates,"AgentRef = ".DBQuote($zoneEntryInfo->{'AgentRef'}));
	}

	# Check AgentDisabled, convert to active
	if (defined($zoneEntryInfo->{"AgentDisabled"})) {
		# XXX: HACK TO TRANSLATE DISABLED INTO ACTIVE
		push(@updates,"active = ".DBQuote($zoneEntryInfo->{'AgentDisabled'} ? 'N' : 'Y'));
	}

	# Create and check update string
	my $updateStr = join(",",@updates);
	if ($updateStr eq "") {
		setError("No updates specified for zone entry");
		return ERR_USAGE;
	}

	# Grab zone entry to see if it exists
	my $zoneEntry = _getDNSZoneEntry($zoneEntryInfo->{'ID'});
	if (!isHash($zoneEntry)) {
		return $zoneEntry;
	}

	# Fetch DNS Zone ID
	my $zoneID = getDNSZoneIDFromDNSZoneEntryID($zoneEntry->{'ID'});
	if (!defined($zoneID)) {
		setError("Parameter 'zoneID' not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneID = isNumber($zoneID))) {
		setError("Parameter 'zoneID' is invalid");
		return ERR_PARAM;
	}

	# Grab zone to get its serial
	my $zone = _getDNSZone($zoneID);
	if (!isHash($zone)) {
		return $zone;
	}

	# Update serial
	my $zoneSerial = getNewDNSZoneSerial($zone->{'Serial'});

	DBBegin();

	# Update zone serial
	my $sth = DBDo("
			UPDATE
				zones
			SET
				serial = ".DBQuote($zoneSerial)."
			WHERE
				id = ".DBQuote($zone->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Update entry
	$sth = DBDo("
			UPDATE
				zoneEntries
			SET
				$updateStr
			WHERE
				ID = ".DBQuote($zoneEntry->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method removeDNSZoneEntry($zoneEntryID)
# Remove a zone entry
#
# @param entryID Entry ID
#
# @return 0 on success, < 0 on error
sub _removeDNSZoneEntry
{
	my $zoneEntryID = shift;


	if (!defined($zoneEntryID)) {
		setError("Parameter 'zoneEntryID' not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneEntryID = isNumber($zoneEntryID))) {
		setError("Parameter 'zoneEntryID' is invalid");
		return ERR_PARAM;
	}

	# Grab zone entry to see if it exists
	my $zoneEntry = _getDNSZoneEntry($zoneEntryID);
	if (!isHash($zoneEntry)) {
		return $zoneEntry;
	}

	# Grab zone to see if it exists
	my $zone = _getDNSZone($zoneEntry->{'ZoneID'});
	if (!isHash($zone)) {
		return $zone;
	}

	# Update serial
	my $zoneSerial = getNewDNSZoneSerial($zone->{'Serial'});

	DBBegin();

	# Update zone serial
	my $sth = DBDo("
			UPDATE
				zones
			SET
				serial = ".DBQuote($zoneSerial)."
			WHERE
				id = ".DBQuote($zone->{'ID'})."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	# Remove from database
	$sth = DBDo("DELETE FROM zoneEntries WHERE ID = ".DBQuote($zoneEntryID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



## @method getDNSZoneEntries($zoneID,$search)
# Return a zones entries
#
# @param zoneID Zone ID
# @param search Optional search criteria, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Entry ID
# @li ZoneID - Zone ID
# @li TTL - TTL (time to live)
# @li Name - Name
# @li Type - Entry type
# @li Aux - Auxillary data
# @li Data - Data
# @li DDNSUserID - Dynamic DNS user ID
# @li Disabled - If this item is disabled
# @li AgentDisabled - If this item has been disabled by the agent
# @li AgentRef - Agent reference
sub _getDNSZoneEntries
{
	my ($zoneID,$search) = @_;


	if (!defined($zoneID)) {
		setError("Parameter 'zoneID' not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneID = isNumber($zoneID))) {
		setError("Parameter 'zoneID' is invalid");
		return ERR_PARAM;
	}
	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}


	# Filters and sorts are the same here
	my $filtersorts = {
		'ID' => 'zoneEntries.id',
		'Name' => 'zoneEntries.name',
		'Type' => 'zoneEntries.type',
		'AgentRef' => 'zoneEntries.AgentRef'
	};

	# Select zones
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				id,
				zone,
				ttl,
				name,
				type,
				aux,
				data,
				DynaDNSUser,
				active,
				AgentRef
			FROM
				zoneEntries
			WHERE
				zone = ".DBQuote($zoneID)."
			",$search,$filtersorts,$filtersorts);
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}
	# Fetch rows
	my @entries = ();
	while (my $row = hashifyLCtoMC(
				$sth->fetchrow_hashref(),
				qw( ID ZoneID TTL Name Type Aux Data DynaDNSUser Active AgentRef )
			)
	) {
		my $entry;

		# Setup our entry
		$entry->{'ID'} = $row->{'ID'};

		$entry->{'ZoneID'} = $row->{'Zone'};

		$entry->{'TTL'} = $row->{'TTL'};

		$entry->{'Name'} = $row->{'Name'};

		$entry->{'Type'} = $row->{'Type'};
		$entry->{'Aux'} = $row->{'Aux'};
		$entry->{'Data'} = $row->{'Data'};

		$entry->{'DDNSUserID'} = $row->{'DynaDNSUser'};
		
		$entry->{'AgentRef'} = $row->{'AgentRef'};

		# XXX: HACK TO TRANSLATE DISABLED INTO ACTIVE
		$entry->{'AgentDisabled'} = ($row->{'Active'} eq 'N') ? 1 : 0;
		$entry->{'Disabled'} = $entry->{'AgentDisabled'};

		push(@entries,$entry);
	}

	DBFreeRes($sth);

	return (\@entries,$numResults);
}


## @method getDNSZoneEntry($zoneEntryID)
# Return a zone zoneEntry
#
# @param zoneEntryID Zone zoneEntry ID
#
# @return Zone entry info hash ref
# @li ID - Entry ID
# @li ZoneID - Zone ID
# @li TTL - TTL (time to live)
# @li Name - Name
# @li Type - Entry type
# @li Aux - Auxillary data
# @li Data - Data
# @li DDNSUserID - Dynamic DNS user ID
# @li Disabled - If this item is disabled
# @li AgentDisabled - If this item has been disabled by the agent
# @li AgentRef - Agent reference
sub _getDNSZoneEntry
{
	my $zoneEntryID = shift;


	if (!defined($zoneEntryID)) {
		setError("Parameter 'zoneEntryID' not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneEntryID = isNumber($zoneEntryID))) {
		setError("Parameter 'zoneEntryID' is invalid");
		return ERR_PARAM;
	}

	# Get zoneEntry
	my $sth = DBSelect("
			SELECT 
				id,
				zone,
				ttl,
				name,
				type,
				aux,
				data,
				DynaDNSUser,
				active,
				AgentRef
			FROM
				zoneEntries
			WHERE
				id = ".DBQuote($zoneEntryID)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("Failed to find zoneEntry ID '$zoneEntryID'");
		return ERR_NOTFOUND;
	}

	# Loop with results
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID Zone TTL Name Type Aux Data DynaDNSUser Active AgentRef ));

	my $zoneEntry;

	# Setup our zoneEntry
	$zoneEntry->{'ID'} = $row->{'ID'};

	$zoneEntry->{'ZoneID'} = $row->{'Zone'};

	$zoneEntry->{'TTL'} = $row->{'TTL'};

	$zoneEntry->{'Name'} = $row->{'Name'};

	$zoneEntry->{'Type'} = $row->{'Type'};
	$zoneEntry->{'Aux'} = $row->{'Aux'};
	$zoneEntry->{'Data'} = $row->{'Data'};

	$zoneEntry->{'DDNSUserID'} = $row->{'DynaDNSUser'};

	# XXX: HACK TO TRANSLATE DISABLED INTO ACTIVE
	$zoneEntry->{'AgentDisabled'} = ($row->{'Active'} eq 'N') ? 1 : 0;
	$zoneEntry->{'Disabled'} = $zoneEntry->{'AgentDisabled'};
	
	$zoneEntry->{'AgentRef'} = $row->{'AgentRef'};

	DBFreeRes($sth);

	return $zoneEntry;
}


## @fn getNewDNSZoneSerial($serial)
# Function to return updated zone serial
#
# @param serial Old serial number
#
# @return New serial number
sub getNewDNSZoneSerial
{
	my $serial = shift;


	if (!defined($serial)) {
		setError("Parameter 'serial' not defined");
		return ERR_PARAM;
	}
	if (!defined($serial = isNumber($serial))) {
		setError("Parameter 'serial' is invalid");
		return ERR_PARAM;
	}

	# Get current serial
	my $tmpSerial = strftime('%Y%m%d01',localtime(time));

	# If serial is great than our date serial, add 1, else use our date serial
	return ($serial >= $tmpSerial) ? $serial + 1 : $tmpSerial;
}


## @fn getDNSZoneIDFromDNSZoneEntryID($entryID)
# Get zone ID from entry id
#
# @param entryID Zone entry ID
#
# @return Zone ID
sub getDNSZoneIDFromDNSZoneEntryID
{
	my $entryID = shift;


	if (!defined($entryID)) {
		setError("Parameter 'entryID' not defined");
		return ERR_PARAM;
	}
	if (!defined($entryID = isNumber($entryID))) {
		setError("Parameter 'entryID' is invalid");
		return ERR_PARAM;
	}

	# Get entry
	my $sth = DBSelect("SELECT zone FROM zoneEntries WHERE id = ".DBQuote($entryID));
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Loop with results
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( Zone ));

	DBFreeRes($sth);

	if (defined($row)) {
		return $row->{'Zone'};
	} else {
		return ERR_NOTFOUND;
	}
}


## @fn canAccessDNSZone($agentID,$zoneID)
# Check to see if agent can access this zone
#
# @param agentID Agent ID
# @param zoneID Zone ID
#
# @return 1 if agent can access, 0 if not
sub canAccessDNSZone
{
	my ($agentID,$zoneID) = @_;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($zoneID)) {
		setError("Parameter 'zoneID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneID = isNumber($zoneID))) {
		setError("Parameter 'zoneID' is invalid");
		return ERR_PARAM;
	}

	# Query to see if we associated with this zone
	my $num_results = DBSelectNumResults("
			FROM
				zones
			WHERE
				zones.id = ".DBQuote($zoneID)."
				AND zones.AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}


## @fn canAccessDNSZoneEntry($agentID,$zoneEntryID)
# Check to see if agent can access this zone entry
#
# @param agentID Agent ID
# @param zoneEntryID Zone entry ID
#
# @return 1 if agent can access, 0 if not
sub canAccessDNSZoneEntry
{
	my ($agentID,$zoneEntryID) = @_;
	my $res = 0;


	if (!defined($agentID)) {
		setError("Parameter 'agentID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($agentID = isNumber($agentID))) {
		setError("Parameter 'agentID' is invalid");
		return ERR_PARAM;
	}

	if (!defined($zoneEntryID)) {
		setError("Parameter 'zoneEntryID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($zoneEntryID = isNumber($zoneEntryID))) {
		setError("Parameter 'zoneEntryID' is invalid");
		return ERR_PARAM;
	}

	# Query to see if we associated with this zone entry
	my $num_results = DBSelectNumResults("
			FROM
				zones, zoneEntries
			WHERE
				zoneEntries.id = ".DBQuote($zoneEntryID)."
				AND zones.id = zoneEntries.zone
				AND zones.AgentID = ".DBQuote($agentID)."
	");
	if (!defined($num_results)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	return $num_results == 1 ? 1 : 0;
}


## @method getDNSServers
# Get a list of primary nameservers
#
# @return Array ref of hash refs
# @li ID - Primary nameserver ID
# @li Hostname - Primary nameserver hostname
sub getDNSServers
{
	# Select system nameservers
	my ($sth,$numResults) = DBSelectSearch("
			SELECT
				ID, HostName
			FROM
				systemNameservers
			");
	if (!defined($sth)) {
		# Is ok for us to return to user...
		if (defined($numResults)) {
			setError(awitpt::db::dblayer::Error());
			return $numResults;
		} else {
			smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
			setError("Database error");
			return ERR_DB;
		}
	}

	# Loop with results
	my @nameservers;
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID HostName ))) {
		my $item;
		$item->{'ID'} = $row->{'ID'};
		$item->{'Hostname'} = $row->{'HostName'};
		push(@nameservers,$item);
	}

	DBFreeRes($sth);

	return(\@nameservers,$numResults);
}



## @fn getDNSServerByHostname($hostname)
# Get nameserver
#
# @param hostname Nameserver hostname
#
# @return Array ref of hash refs
# @li ID - Primary nameserver ID
# @li Hostname - Primary nameserver hostname
sub getDNSServerByHostname
{
	my $hostname = shift;


	if (!defined($hostname)) {
		setError("Parameter 'hostname' not defined");
		return ERR_PARAM;
	}
	if (!isVariable($hostname)) {
		setError("Parameter 'hostname' is invalid");
		return ERR_PARAM;
	}
	if ($hostname eq "") {
		setError("Parameter 'hostname' is blank");
		return ERR_PARAM;
	}

	# Query site
	my $sth = DBSelect("
		SELECT
			ID, Hostname
		FROM
			systemNameservers
		WHERE
			Hostname = ".DBQuote($hostname)."
	");
	if (!defined($sth)) {
		smlib::logging::log(LOG_ERR,awitpt::db::dblayer::Error());
		setError("Database error");
		return ERR_DB;
	}

	# Check if we got results
	if ($sth->rows < 1) {
		setError("No data returned from database");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	# Grab result
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Hostname ));
	my $res;

	$res->{'ID'} = $row->{'ID'};
	$res->{'Hostname'} = $row->{'Hostname'};

	# We done with this result set
	DBFreeRes($sth);

	return $res;

}


## @fn getDNSHostingServerGroups($serverGroupInfo,$search)
# INTERNAL FUNCTION CALLED FROM smlib::DNSHosting ONLY
# Returns list of radius server groups
#
# @param servrGroupInfo DNSHosting serverGroup info hash ref
# @li AgentID - Optional agent ID
#
# @param search Search hash ref, @see smlib::getServerGroups
#
# @return Array ref of hash refs
# @li ID - DNSHosting server group ID
# @li Name - DNSHosting server group name
sub getDNSHostingServerGroups
{
	my ($serverGroupInfo,$search) = @_;


	# Check params
	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Data to pass to smlib::servers
	my $serverGroupData;

	# Make sure AgentID is ok
	if (defined($serverGroupData->{'AgentID'})) {
		if (!defined($serverGroupData->{'AgentID'} = isNumber($serverGroupInfo->{'AgentID'}))) {
			setError("Parameter 'serverGroupInfo' element 'AgentID' is invalid");
			return ERR_PARAM;
		}
	}

	# Limit to radius servers
	$serverGroupData->{'Capabilities'} = 'DNSHosting';

	# Grab results and see if they ok, if not just pass the error through
	my ($results,$num_results) = smlib::servers::getServerGroups($serverGroupData,$search);
	if (ref($results) ne "ARRAY") {
		return $results;
	}

	return ($results,$num_results);
}



1;
# vim: ts=4
