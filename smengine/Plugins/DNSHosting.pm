# DNSHosting SOAP interface
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class DNSHosting
# This is the main DNSHosting class, allowing control over DNS services
package DNSHosting;

use strict;

use Auth;

use smlib::DNSHosting;
use smlib::Agent;

use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "DNSHosting",
	Init => \&init,
};


## @internal
# Initialize module
sub init {
	my $server = shift;

	Auth::aclAdd('DNSHosting','ping','DNSHosting/Test');

	Auth::aclAdd('DNSHosting','getDNSServers','DNSHosting/Zones/Get');

	Auth::aclAdd('DNSHosting','createDNSZone','DNSHosting/Zones/Add');
	Auth::aclAdd('DNSHosting','updateDNSZone','DNSHosting/Zones/Remove');
	Auth::aclAdd('DNSHosting','removeDNSZone','DNSHosting/Zones/Update');
	Auth::aclAdd('DNSHosting','getDNSZones','DNSHosting/Zones/List');
	Auth::aclAdd('DNSHosting','getDNSZone','DNSHosting/Zones/Get');

	Auth::aclAdd('DNSHosting','createDNSZoneEntry','DNSHosting/ZoneEntries/Add');
	Auth::aclAdd('DNSHosting','updateDNSZoneEntry','DNSHosting/ZoneEntries/Update');
	Auth::aclAdd('DNSHosting','removeDNSZoneEntry','DNSHosting/ZoneEntries/Remove');
	Auth::aclAdd('DNSHosting','getDNSZoneEntries','DNSHosting/ZoneEntries/List');
	Auth::aclAdd('DNSHosting','getDNSZoneEntry','DNSHosting/ZoneEntries/Get');

	Auth::aclAdd('DNSHosting','getDNSHostingServerGroups','DNSHosting/ServerGroups/List');

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method createDNSZone($serverGroupID,$zoneInfo)
# Create a DNS zone
#
# @param serverGroupID Server group ID
#
# @param zoneInfo Zone info hash ref
# @li AgentID - Agent ID
# @li DomainName - Domain name
# @li NameserverID - Primary nameserver
# @li AdminEmail - Zone admin email address
# @li Refresh - Zone SOA refresh
# @li Retry - Zone SOA retry
# @li Expire - Zone SOA expire
# @li MinTTL - Zone SOA minimum TTL
# @li TTL - Zone SOA TTL
# @li AgentDisabled - Optional, if this item has been disabled by the agent
# @li AgentRef - Optional, agent reference
#
# @return Zone ID
sub createDNSZone
{
	my (undef,$serverGroupID,$zoneInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($zoneInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' not defined");
	}
	if (!isHash($zoneInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' is not a HASH");
	}

	if (!defined($zoneInfo->{'AgentID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentID' not defined");
	}
	if (!defined($zoneInfo->{'AgentID'} = isNumber($zoneInfo->{'AgentID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentID' is invalid");
	}

	if (!defined($zoneInfo->{'DomainName'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' has no element 'DomainName'");
	}
	if (!isVariable($zoneInfo->{"DomainName"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'DomainName' is invalid");
	}
	$zoneInfo->{'DomainName'} = lc($zoneInfo->{'DomainName'});
	if (!($zoneInfo->{'DomainName'} =~ /^[a-z0-9_\-\.]+$/)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'DomainName' is invalid");
	}

	if (!defined($zoneInfo->{'NameserverID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' has no element 'NameserverID'");
	}
	if (!defined($zoneInfo->{'NameserverID'} = isNumber($zoneInfo->{'NameserverID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'NameserverID' is invalid");
	}

	if (!defined($zoneInfo->{'AdminEmail'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' has no element 'AdminEmail'");
	}
	if (!isVariable($zoneInfo->{"AdminEmail"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AdminEmail' is invalid");
	}
	$zoneInfo->{'AdminEmail'} = lc($zoneInfo->{'AdminEmail'});
	if (!($zoneInfo->{'AdminEmail'} =~ /^\S+@\S+$/)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AdminEmail' is invalid");
	}

	if (defined($zoneInfo->{'Refresh'}) && !defined($zoneInfo->{'Refresh'} = isNumber($zoneInfo->{'Refresh'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'Refresh' is invalid");
	}
	if (defined($zoneInfo->{'Retry'}) && !defined($zoneInfo->{'Retry'} = isNumber($zoneInfo->{'Retry'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'Retry' is invalid");
	}
	if (defined($zoneInfo->{'Expire'}) && !defined($zoneInfo->{'Expire'} = isNumber($zoneInfo->{'Expire'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'Expire' is invalid");
	}
	if (defined($zoneInfo->{'MinTTL'}) && !defined($zoneInfo->{'MinTTL'} = isNumber($zoneInfo->{'MinTTL'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'MinTTL' is invalid");
	}
	if (defined($zoneInfo->{'TTL'}) && !defined($zoneInfo->{'TTL'} = isNumber($zoneInfo->{'TTL'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'TTL' is invalid");
	}
	
	if (defined($zoneInfo->{'AgentRef'})) {
		if (!isVariable($zoneInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentRef' is invalid");
		}
		# Delete element if string is empty
		if ($zoneInfo->{'AgentRef'} eq "") {
			delete($zoneInfo->{'AgentRef'});
		}
	}

	# Check AgentDisabled
	if (defined($zoneInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($zoneInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentDisabled' is invalid");
		}
		# Check for boolean value
		if (!defined($zoneInfo->{'AgentDisabled'} = isBoolean($zoneInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Check if the agent ID is valid
		if (smlib::Agent::isAgentValid($zoneInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentID' has an invalid value");
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$zoneInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Build params
	my $zoneData;
	$zoneData->{'AgentID'} = $zoneInfo->{'AgentID'};
	$zoneData->{'DomainName'} = $zoneInfo->{'DomainName'};
	$zoneData->{'NameserverID'} = $zoneInfo->{'NameserverID'};
	$zoneData->{'AdminEmail'} = $zoneInfo->{'AdminEmail'};
	$zoneData->{'Refresh'} = $zoneInfo->{'Refresh'} if (defined($zoneInfo->{'Refresh'}));
	$zoneData->{'Retry'} = $zoneInfo->{'Retry'} if (defined($zoneInfo->{'Retry'}));
	$zoneData->{'Expire'} = $zoneInfo->{'Expire'} if (defined($zoneInfo->{'Expire'}));
	$zoneData->{'MinTTL'} = $zoneInfo->{'MinTTL'} if (defined($zoneInfo->{'MinTTL'}));
	$zoneData->{'TTL'} = $zoneInfo->{'TTL'} if (defined($zoneInfo->{'TTL'}));
	$zoneData->{'AgentDisabled'} = $zoneInfo->{'AgentDisabled'} if (defined($zoneInfo->{'AgentDisabled'}));
	$zoneData->{'AgentRef'} = $zoneInfo->{'AgentRef'} if (defined($zoneInfo->{'AgentRef'}));

	my $res = smlib::DNSHosting::createDNSZone($serverGroupID,$zoneData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','ZoneID');
}



## @method updateDNSZone($serverGroupID,$zoneInfo)
# Update a DNS zone
#
# @param serverGroupID Server group ID
#
# @param zoneInfo Zone info hash ref
# @li ID - Zone ID
# @li AgentID - Agent ID
# @li DomainName - Optional domain name
# @li NameserverID - Primary nameserver
# @li AdminEmail - Optional admin email address
# @li Refresh - Optional SOA refresh
# @li Retry - Optional SOA retry
# @li Expire - Optional SOA expire
# @li MinTTL - Optinal SOA minimum TTL
# @li TTL - Optinal SOA default TTL
# @li AgentDisabled - Optional, if this item has been disabled by the agent
# @li AgentRef - Optional agent reference
#
# @return 0 on success or < 0 on error
sub updateDNSZone
{
	my (undef,$serverGroupID,$zoneInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($zoneInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' not defined");
	}
	if (!isHash($zoneInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' is not a HASH");
	}
	if (!defined($zoneInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' has no element 'ID'");
	}
	if (!defined($zoneInfo->{'ID'} = isNumber($zoneInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'ID' is invalid");
	}
	if (defined($zoneInfo->{'AgentID'}) && !defined($zoneInfo->{'AgentID'} = isNumber($zoneInfo->{'AgentID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentID' is invalid");
	}
	if (defined($zoneInfo->{'DomainName'})) {
		if (!isVariable($zoneInfo->{"DomainName"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'DomainName' is invalid");
		}
		if ($zoneInfo->{"DomainName"} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'DomainName' is blank");
		}
		$zoneInfo->{'DomainName'} = lc($zoneInfo->{'DomainName'});
		if (!($zoneInfo->{'DomainName'} =~ /^[a-z0-9_\-\.]+$/)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'DomainName' is invalid");
		}
	}
	if (defined($zoneInfo->{'NameserverID'})) {
		if (!defined($zoneInfo->{'NameserverID'} = isNumber($zoneInfo->{'NameserverID'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'NameserverID' is invalid");
		}
	}
	if (defined($zoneInfo->{'AdminEmail'})) {
		if (!isVariable($zoneInfo->{"AdminEmail"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'DomainName' is invalid");
		}
		if ($zoneInfo->{"AdminEmail"} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AdminEmail' is blank");
		}
		$zoneInfo->{'AdminEmail'} = lc($zoneInfo->{'AdminEmail'});
		if (!($zoneInfo->{'AdminEmail'} =~ /^\S+@\S+$/)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AdminEmail' is invalid");
		}
	}
	if (defined($zoneInfo->{'Refresh'}) && !defined($zoneInfo->{'Refresh'} = isNumber($zoneInfo->{'Refresh'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'Refresh' is invalid");
	}
	if (defined($zoneInfo->{'Retry'}) && !defined($zoneInfo->{'Retry'} = isNumber($zoneInfo->{'Retry'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'Retry' is invalid");
	}
	if (defined($zoneInfo->{'Expire'}) && !defined($zoneInfo->{'Expire'} = isNumber($zoneInfo->{'Expire'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'Expire' is invalid");
	}
	if (defined($zoneInfo->{'MinTTL'}) && !defined($zoneInfo->{'MinTTL'} = isNumber($zoneInfo->{'MinTTL'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'MinTTL' is invalid");
	}
	if (defined($zoneInfo->{'TTL'}) && !defined($zoneInfo->{'TTL'} = isNumber($zoneInfo->{'TTL'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'TTL' is invalid");
	}
	
	if (defined($zoneInfo->{'AgentRef'})) {
		if (!isVariable($zoneInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentRef' is invalid");
		}
	}

	# Check AgentDisabled
	if (defined($zoneInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($zoneInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentDisabled' is invalid");
		}
		# Check for boolean value
		if (!defined($zoneInfo->{'AgentDisabled'} = isBoolean($zoneInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Check if the agent ID is valid
		if (smlib::Agent::isAgentValid($zoneInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentID' has an invalid value");
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID 
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check this agent can access the zone they specificed
		if (smlib::DNSHosting::canAccessDNSZone($callerAgentID,$zoneInfo->{'ID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS zone ID '".$zoneInfo->{'ID'}."'");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the AgentID they specificed
		if (defined($zoneInfo->{'AgentID'})) {
			if (smlib::Agent::canAgentAccessAgent($callerAgentID,$zoneInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
			}
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;
	$i->{'ID'} = $zoneInfo->{'ID'};
	$i->{'AgentID'} = $zoneInfo->{'AgentID'} if (defined($zoneInfo->{'AgentID'}));
	$i->{'DomainName'} = $zoneInfo->{'DomainName'} if (defined($zoneInfo->{'DomainName'}));
	$i->{'NameserverID'} = $zoneInfo->{'NameserverID'} if (defined($zoneInfo->{'NameserverID'}));
	$i->{'AdminEmail'} = $zoneInfo->{'AdminEmail'} if (defined($zoneInfo->{'AdminEmail'}));
	$i->{'Refresh'} = $zoneInfo->{'Refresh'} if (defined($zoneInfo->{'Refresh'}));
	$i->{'Retry'} = $zoneInfo->{'Retry'} if (defined($zoneInfo->{'Retry'}));
	$i->{'Expire'} = $zoneInfo->{'Expire'} if (defined($zoneInfo->{'Expire'}));
	$i->{'MinTTL'} = $zoneInfo->{'MinTTL'} if (defined($zoneInfo->{'MinTTL'}));
	$i->{'TTL'} = $zoneInfo->{'TTL'} if (defined($zoneInfo->{'TTL'}));
	$i->{'AgentDisabled'} = $zoneInfo->{'AgentDisabled'} if (defined($zoneInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $zoneInfo->{'AgentRef'} if (defined($zoneInfo->{'AgentRef'}));

	# Do the update
	my $res = smlib::DNSHosting::updateDNSZone($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeDNSZone($serverGroupID,$zoneID)
# Remove DNS zone
#
# @param serverGroupID Server group ID
#
# @param zoneID DNS zone ID
#
# @return 0 on success or < 0 on error
sub removeDNSZone
{
	my (undef,$serverGroupID,$zoneID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($zoneID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneID' undefined");
	}
	if (!defined($zoneID = isNumber($zoneID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID 
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the zone they specificed
		if (smlib::DNSHosting::canAccessDNSZone($callerAgentID,$zoneID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS zone ID '$zoneID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $res = smlib::DNSHosting::removeDNSZone($serverGroupID,$zoneID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method getDNSZones($serverGroupID,$zoneInfo,$search)
# Return a list of all the zones an agent has
#
# @param serverGroupID Server group ID
#
# @param zoneInfo Zone info hash ref
# @li AgentID - Limit returned results to this agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Zone ID
# @li AgentID - Agent ID
# @li AgentName - Agent Name
# @li DomainName - Domain name
# @li NameserverID - Primary nameserver ID
# @li NameserverHostname - Primary nameserver hostname
# @li AdminEmail - Admin email address
# @li Refresh - SOA refresh
# @li Retry - SOA retry
# @li Expire - SOA expire
# @li MinTTL - SOA minimum TTL
# @li TTL - SOA default TTL
# @li Disabled - Set if zone is disabled
# @li AgentDisabled - If this item has been disabled by the agent
# @li AgentRef - Agent reference
sub getDNSZones
{
	my (undef,$serverGroupID,$zoneInfo,$search) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	# Check if we have zoneinfo
	if (defined($zoneInfo)) {
		# Make sure we're a hash
		if (!isHash($zoneInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($zoneInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($zoneInfo->{'AgentID'} = isNumber($zoneInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $zoneData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($zoneInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($zoneInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'AgentID' has an invalid value");
			}

			$zoneData->{'AgentID'} = $zoneInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we don't have an agent ID, use the current one
		if (!defined($zoneInfo->{'AgentID'})) {
			$zoneInfo->{'AgentID'} = $callerAgentID;
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$zoneInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		$zoneData->{'AgentID'} = $zoneInfo->{'AgentID'};

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}
	
	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::DNSHosting::getDNSZones($serverGroupID,$zoneData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('AgentID','int');
	$response->addField('AgentName','string');
	$response->addField('DomainName','string');
	$response->addField('NameserverID','int');
	$response->addField('NameserverHostname','string');
	$response->addField('AdminEmail','string');
	$response->addField('Serial','int');
	$response->addField('Refresh','int');
	$response->addField('Retry','int');
	$response->addField('Expire','int');
	$response->addField('MinTTL','int');
	$response->addField('TTL','int');
	$response->addField('AgentDisabled','boolean');
	$response->addField('Disabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getDNSZone($serverGroupID,$zoneID)
# Return a hash containing the info for this DNS zone
#
# @param serverGroupID Server group ID
#
# @param zoneID Zone ID
#
# @return DNZ zone hash ref
# @li ID - Zone ID
# @li AgentID - Agent ID
# @li AgentName - Agent Name
# @li DomainName - Domain name
# @li NameserverID - Primary nameserver ID
# @li NameserverHostname - Primary nameserver hostname
# @li AdminEmail - Admin email address
# @li Refresh - SOA refresh
# @li Retry - SOA retry
# @li Expire - SOA expire
# @li MinTTL - SOA minimum TTL
# @li TTL - SOA default TTL
# @li Disabled - Set if zone is disabled
# @li AgentDisabled - If this item has been disabled by the agent
# @li AgentRef - Agent reference
sub getDNSZone
{
	my (undef,$serverGroupID,$zoneID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($zoneID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneID' undefined");
	}
	if (!defined($zoneID = isNumber($zoneID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the zone they specificed
		if (smlib::DNSHosting::canAccessDNSZone($callerAgentID,$zoneID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS zone ID '$zoneID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::DNSHosting::getDNSZone($serverGroupID,$zoneID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('AgentID','int');
	$response->addField('DomainName','string');
	$response->addField('NameserverID','int');
	$response->addField('NameserverHostname','string');
	$response->addField('AdminEmail','string');
	$response->addField('Serial','int');
	$response->addField('Refresh','int');
	$response->addField('Retry','int');
	$response->addField('Expire','int');
	$response->addField('MinTTL','int');
	$response->addField('TTL','int');
	$response->addField('Disabled','boolean');
	$response->addField('AgentDisabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createDNSZoneEntry($serverGroupID,$zoneEntryInfo)
# Create a zone entry
#
# @param serverGroupID Server group ID
#
# @param zoneEntryInfo Zone entry info hash ref
# @li ZoneID - Zone ID
# @li Name - Name
# @li Type - Entry type
# @li Data - Data
# @li Aux - Auxillary data
# @li TTL - TTL (time to live)
# @li DDNSUserID - Dynamic DNS user ID, 0 or "" for none
# @li AgentDisabled - Optional flag agent can set to disable this zone entry
# @li AgentRef - Optional agent reference
#
# @return Returns zone entry ID
sub createDNSZoneEntry
{
	my (undef,$serverGroupID,$zoneEntryInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($zoneEntryInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' undefined");
	}
	if (!isHash($zoneEntryInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' is not a HASH");
	}
	if (!defined($zoneEntryInfo->{'ZoneID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'ZoneID' undefined");
	}
	if (!defined($zoneEntryInfo->{'ZoneID'} = isNumber($zoneEntryInfo->{'ZoneID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'ZoneID' is invalid");
	}

	# Blank name if the commonly used @ is supplied
	if (!defined($zoneEntryInfo->{'Name'}) || $zoneEntryInfo->{'Name'} eq "@") {
		$zoneEntryInfo->{'Name'} = "";
	} else {
		if (!isVariable($zoneEntryInfo->{"Name"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Name' is invalid");
		}
	}
	# Lowercase the name
	$zoneEntryInfo->{'Name'} = lc($zoneEntryInfo->{"Name"});
	# Check validity
	if (!($zoneEntryInfo->{'Name'} =~ /^[a-z0-9_\-\.]*$/)) {
		setError("Parameter 'zoneEntryInfo' element 'Name' is invalid");
		return ERR_PARAM;
	}

	if (!defined($zoneEntryInfo->{'Type'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Type' is not defined");
	}
	if (!isVariable($zoneEntryInfo->{"Type"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Type' is invalid");
	}
	# Uppercase type
	$zoneEntryInfo->{'Type'} = uc($zoneEntryInfo->{'Type'});
	# Check for match
	my $typeValid = 0;
	foreach my $type (('A','AAAA','CNAME','HINFO','MX','NS','PTR','RP','SPF','SRV','TXT')) {
		if ($zoneEntryInfo->{'Type'} eq $type) {
			$typeValid = 1;
			last;
		}
	}
	# Bitch if wrong
	if (!$typeValid) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Type' is invalid");
	}

	if (!defined($zoneEntryInfo->{'Data'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Data' is not defined");
	}
	if (!isVariable($zoneEntryInfo->{"Data"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Data' is invalid");
	}
	if ($zoneEntryInfo->{"Data"} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Data' is blank");
	}

	if (defined($zoneEntryInfo->{'Aux'}) && 
			!defined($zoneEntryInfo->{'Aux'} = isNumber($zoneEntryInfo->{'Aux'},ISNUMBER_ALLOW_ZERO))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Aux' is invalid");
	}

	if (defined($zoneEntryInfo->{'TTL'}) && !defined($zoneEntryInfo->{'TTL'} = isNumber($zoneEntryInfo->{'TTL'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'TTL' is invalid");
	}

	if (defined($zoneEntryInfo->{'DDNSUserID'})) {
		if (!isVariable($zoneEntryInfo->{'DDNSUserID'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'DDNSUserID' is invalid");
		}
		# If DDNSUserID is blank, undef it
		if ($zoneEntryInfo->{'DDNSUserID'} eq "") {
			$zoneEntryInfo->{'DDNSUserID'} = 0;

		# Or, check value
		} elsif (!defined($zoneEntryInfo->{'DDNSUserID'} = isNumber($zoneEntryInfo->{'DDNSUserID'},ISNUMBER_ALLOW_ZERO))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'DDNSUserID' is invalid");
		}
	}
	
	if (defined($zoneEntryInfo->{'AgentRef'})) {
		if (!isVariable($zoneEntryInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'AgentRef' is invalid");
		}
	}

	# Check AgentDisabled
	if (defined($zoneEntryInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($zoneEntryInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'AgentDisabled' is invalid");
		}
		# Check for boolean value
		if (!defined($zoneEntryInfo->{'AgentDisabled'} = isBoolean($zoneEntryInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the zone they specificed
		if (smlib::DNSHosting::canAccessDNSZone($callerAgentID,$zoneEntryInfo->{'ZoneID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS zone ID '".$zoneEntryInfo->{'ZoneID'}."'");
		}
		
		# Check this agent can access the DDNS user they specificed
		if (defined($zoneEntryInfo->{'DDNSUserID'}) && $zoneEntryInfo->{'DDNSUserID'} > 0) {
			if (smlib::DNSHosting::DDNS::canAccessDDNSUser($callerAgentID,$zoneEntryInfo->{'DDNSUserID'}) != 1) {
				return SOAPError(ERR_NOACCESS,"Access denied to use specified 'DDNSUserID'");
			}
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;

	$i->{'ZoneID'} = $zoneEntryInfo->{'ZoneID'};
	$i->{'Name'} = $zoneEntryInfo->{'Name'} if (defined($zoneEntryInfo->{'Name'}));
	$i->{'Type'} = $zoneEntryInfo->{'Type'};
	$i->{'Data'} = $zoneEntryInfo->{'Data'};
	$i->{'Aux'} = $zoneEntryInfo->{'Aux'} if (defined($zoneEntryInfo->{'Aux'}));
	$i->{'TTL'} = $zoneEntryInfo->{'TTL'} if (defined($zoneEntryInfo->{'TTL'}));
	$i->{'DDNSUserID'} = $zoneEntryInfo->{'DDNSUserID'} if (defined($zoneEntryInfo->{'DDNSUserID'}));
	$i->{'AgentDisabled'} = $zoneEntryInfo->{'AgentDisabled'} if (defined($zoneEntryInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $zoneEntryInfo->{'AgentRef'} if (defined($zoneEntryInfo->{'AgentRef'}));

	my $res = smlib::DNSHosting::createDNSZoneEntry($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','DNSZoneEntryID');
}



## @method updateDNSZoneEntry($serverGroupID,$zoneEntryInfo)
# Update DNS zone entry
#
# @param serverGroupID Server group ID
#
# @param zoneEntryInfo Zone entry info hash ref
# @li ID - Entry ID
# @li Name - Optional name
# @li Type - Optional entry type
# @li Data - Optional data
# @li Aux - Optional auxillary data
# @li TTL - Optional TTL (time to live)
# @li DDNSUserID - Optional dynamic DNS user ID, 0 or "" to remove
# @li AgentDisabled - If this item has been disabled by the agent
# @li AgentRef - Agent reference
#
# @return 0 on success or < 0 on error
sub updateDNSZoneEntry
{
	my (undef,$serverGroupID,$zoneEntryInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($zoneEntryInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' undefined");
	}
	if (!isHash($zoneEntryInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' is not a HASH");
	}
	if (!defined($zoneEntryInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' has no element 'ID' defined");
	}
	if (!defined($zoneEntryInfo->{'ID'} = isNumber($zoneEntryInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'ID' is invalid");
	}

	# Blank name if the commonly used @ is supplied
	if (defined($zoneEntryInfo->{'Name'})) {
		if (!isVariable($zoneEntryInfo->{"Name"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Name' is invalid");
		}
		# Lowercase the name
		$zoneEntryInfo->{'Name'} = lc($zoneEntryInfo->{"Name"});
		# Check validity
		if (!($zoneEntryInfo->{'Name'} =~ /^[a-z0-9_\-\.]*$/)) {
			setError("Parameter 'zoneEntryInfo' element 'Name' is invalid");
			return ERR_PARAM;
		}
	}

	if (defined($zoneEntryInfo->{'Type'})) {
		if (!isVariable($zoneEntryInfo->{"Type"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Type' is invalid");
		}
		# Uppercase type
		$zoneEntryInfo->{'Type'} = uc($zoneEntryInfo->{'Type'});
		# Check for match
		my $typeValid = 0;
		foreach my $type (('A','AAAA','CNAME','HINFO','MX','NS','PTR','RP','SPF','SRV','TXT')) {
			if ($zoneEntryInfo->{'Type'} eq $type) {
				$typeValid = 1;
				last;
			}
		}
		# Bitch if wrong
		if (!$typeValid) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Type' is invalid");
		}
	}

	if (!defined($zoneEntryInfo->{'Data'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Data' is not defined");
	}
	if (!isVariable($zoneEntryInfo->{"Data"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Data' is invalid");
	}
	if ($zoneEntryInfo->{"Data"} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Data' is blank");
	}

	if (defined($zoneEntryInfo->{'Aux'}) && 
			!defined($zoneEntryInfo->{'Aux'} = isNumber($zoneEntryInfo->{'Aux'},ISNUMBER_ALLOW_ZERO))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'Aux' is invalid");
	}

	if (defined($zoneEntryInfo->{'TTL'}) && !defined($zoneEntryInfo->{'TTL'} = isNumber($zoneEntryInfo->{'TTL'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'TTL' is invalid");
	}

	if (defined($zoneEntryInfo->{'DDNSUserID'})) {
		if (!isVariable($zoneEntryInfo->{'DDNSUserID'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'DDNSUserID' is invalid");
		}
		# If the DDNS user ID is blank, we want to remove it
		if ($zoneEntryInfo->{'DDNSUserID'} eq "") {
			$zoneEntryInfo->{'DDNSUserID'} = 0;
		# Allow 0 aswell to remove DDNS user ID
		} elsif (!defined($zoneEntryInfo->{'DDNSUserID'} = isNumber($zoneEntryInfo->{'DDNSUserID'},ISNUMBER_ALLOW_ZERO))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'DDNSUserID' is invalid");
		}
	}

	if (defined($zoneEntryInfo->{'AgentRef'})) {
		if (!isVariable($zoneEntryInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'AgentRef' is invalid");
		}
	}

	# Check AgentDisabled
	if (defined($zoneEntryInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($zoneEntryInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'AgentDisabled' is invalid");
		}
		# Check for boolean value
		if (!defined($zoneEntryInfo->{'AgentDisabled'} = isBoolean($zoneEntryInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID and its valid
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the zone entry they specificed
		if (smlib::DNSHosting::canAccessDNSZoneEntry($callerAgentID,$zoneEntryInfo->{'ID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS zone entry ID '".
					$zoneEntryInfo->{'ID'}."'");
		}
		
		# Check this agent can access the DDNS user they specificed
		if (defined($zoneEntryInfo->{'DDNSUserID'}) && $zoneEntryInfo->{'DDNSUserID'} > 0) {
			if (smlib::DNSHosting::DDNS::canAccessDDNSUser($callerAgentID,$zoneEntryInfo->{'DDNSUserID'}) != 1) {
				return SOAPError(ERR_NOACCESS,"Access denied to use specified 'DDNSUserID'");
			}
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}


	my $i;

	$i->{'ID'} = $zoneEntryInfo->{'ID'};
	$i->{'Name'} = $zoneEntryInfo->{'Name'} if (defined($zoneEntryInfo->{'Name'}));
	$i->{'Type'} = $zoneEntryInfo->{'Type'} if (defined($zoneEntryInfo->{'Type'}));
	$i->{'Data'} = $zoneEntryInfo->{'Data'} if (defined($zoneEntryInfo->{'Data'}));
	$i->{'Aux'} = $zoneEntryInfo->{'Aux'} if (defined($zoneEntryInfo->{'Aux'}));
	$i->{'TTL'} = $zoneEntryInfo->{'TTL'} if (defined($zoneEntryInfo->{'TTL'}));
	$i->{'DDNSUserID'} = $zoneEntryInfo->{'DDNSUserID'} if (defined($zoneEntryInfo->{'DDNSUserID'}));
	$i->{'AgentDisabled'} = $zoneEntryInfo->{'AgentDisabled'} if (defined($zoneEntryInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $zoneEntryInfo->{'AgentRef'} if (defined($zoneEntryInfo->{'AgentRef'}));

	my $res = smlib::DNSHosting::updateDNSZoneEntry($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}


## @method removeDNSZoneEntry($serverGroupID,$zoneEntryID)
# Remove zone entry
#
# @param serverGroupID Server group ID
#
# @param zoneEntryID %Zone entry ID
#
# @return 0 on success or < 0 on error
sub removeDNSZoneEntry
{
	my (undef,$serverGroupID,$zoneEntryID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($zoneEntryID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryID' undefined");
	}
	if (!defined($zoneEntryID = isNumber($zoneEntryID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the zone entry they specificed
		if (smlib::DNSHosting::canAccessDNSZoneEntry($callerAgentID,$zoneEntryID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS entry zone ID '$zoneEntryID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Remove DNS zone entry
	my $res = smlib::DNSHosting::removeDNSZoneEntry($serverGroupID,$zoneEntryID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method getDNSZoneEntries($serverGroupID,$zoneID,$search)
# Return zone entries
#
# @param serverGroupID Server group ID
#
# @param zoneID DNS zone ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Entry ID
# @li TTL - TTL (time to live)
# @li Name - Name
# @li Type - Entry type
# @li Aux - Auxillary data
# @li Data - Data
# @li DDNSUserID - Dynamic DNS user ID
# @li Disabled - Set if zone is disabled
# @li AgentDisabled - If this item has been disabled by the agent
# @li AgentRef - Agent reference
sub getDNSZoneEntries
{
	my (undef,$serverGroupID,$zoneID,$search) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($zoneID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneID' undefined");
	}
	if (!defined($zoneID = isNumber($zoneID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the zone they specificed
		if (smlib::DNSHosting::canAccessDNSZone($callerAgentID,$zoneID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS zone ID '$zoneID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::DNSHosting::getDNSZoneEntries($serverGroupID,$zoneID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build result
	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('TTL','int');
	$response->addField('Name','string');
	$response->addField('Type','string');
	$response->addField('Aux','int');
	$response->addField('Data','string');
	$response->addField('DDNSUserID','int');
	$response->addField('AgentDisabled','boolean');
	$response->addField('Disabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getDNSZoneEntry($serverGroupID,$zoneEntryID)
# Get dns zone entry information
#
# @param serverGroupID Server group ID
#
# @param zoneEntryID %Mailbox alias ID
#
# @return Zone entry hash ref
# @li ID - Entry ID
# @li TTL - TTL (time to live)
# @li Name - Name
# @li Type - Entry type
# @li Aux - Auxillary data
# @li Data - Data
# @li DDNSUserID - Dynamic DNS user ID
# @li Disabled - Set if zone is disabled
# @li AgentDisabled - If this item has been disabled by the agent
# @li AgentRef - Agent reference
sub getDNSZoneEntry
{
	my (undef,$serverGroupID,$zoneEntryID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($zoneEntryID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryID' undefined");
	}
	if (!defined($zoneEntryID = isNumber($zoneEntryID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneEntryID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the zone entry they specificed
		if (smlib::DNSHosting::canAccessDNSZoneEntry($callerAgentID,$zoneEntryID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS entry zone ID '$zoneEntryID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::DNSHosting::getDNSZoneEntry($serverGroupID,$zoneEntryID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	# Build result
	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('TTL','int');
	$response->addField('Name','string');
	$response->addField('Type','string');
	$response->addField('Aux','int');
	$response->addField('Data','string');
	$response->addField('DDNSUserID','int');
	$response->addField('Disabled','boolean');
	$response->addField('AgentDisabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);

	return $response->export();
}


## @method getDNSServers
# Get a list of nameservers
#
# @return Array ref of hash refs
# @li ID - Nameserver ID
# @li HostName - Nameserver hostname
sub getDNSServers
{
	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::DNSHosting::getDNSServers();
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Hostname','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}


## @method getDNSHostingServerGroups($serverGroupInfo,$search)
# Return dnshosting server groups
#
# @param serverGroupInfo Class info hash ref. This hash must be defined even if not used.
# @li AgentID - Optional AgentID to limit results to. If not defined, results are limited to the current 
# caller privileges
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Server group ID
# @li Description - Server description
sub getDNSHostingServerGroups
{
	my (undef,$serverGroupInfo,$search) = @_;


	# Check if we have serverGroupinfo
	if (defined($serverGroupInfo)) {
		# Make sure we're a hash
		if (!isHash($serverGroupInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($serverGroupInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($serverGroupInfo->{'AgentID'} = isNumber($serverGroupInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $serverGroupData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($serverGroupInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($serverGroupInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'AgentID' has an invalid value");
			}

			$serverGroupData->{'AgentID'} = $serverGroupInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we don't have an agent ID, use the current one
		if (!defined($serverGroupInfo->{'AgentID'})) {
			$serverGroupInfo->{'AgentID'} = $callerAgentID;
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$serverGroupInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		$serverGroupData->{'AgentID'} = $serverGroupInfo->{'AgentID'};

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}
	
	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::DNSHosting::getDNSHostingServerGroups($serverGroupData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('Description','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



1;
# vim: ts=4
