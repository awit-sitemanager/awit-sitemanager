# Admin functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class Admin
# This is the admin class
package Admin;


use strict;

use Auth;

use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin",
	Init => \&init,
};


# Admin authentication
our $authInfo = {
	Name => "Admin Authentication",
	Type => "Admin",
	Authenticate => \&Admin_Auth,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin','ping','Admin/Test');
	
	Auth::aclAdd('Admin','authenticate','Admin/Authenticate');

	Auth::aclAdd('Admin','getCacheStats','Admin/CacheEngine/Stats');

	# Add authentication plugin
	Auth::pluginAdd($authInfo);

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method authenticate
# Dummy function to allow admin authentication
#
# @return Will always return 0
sub authenticate
{
	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	return SOAPSuccess(Auth::userGetAPICapabilities(),'array','Capabilities');
}



## @method getCacheStats
# Return cache engine stats
#
# @return Hash ref
# @li Hits Cache hits
# @li Misses Cache misses
sub getCacheStats
{
	my $data;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::Engine::getCacheStats();
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('Hits','int');
	$response->addField('Misses','int');
	$response->parseHashRef($rawData);

	return $response->export();
}





## @internal
# @fn Admin_Auth($username,$password)
# Try authenticate an admin
sub Admin_Auth
{
	my ($username,$password) = @_;


	# Authenticate
	my $uid = smlib::users::authenticate($username,$password);
	if ($uid < 1) {
		return $uid;
	}

	# Get attributes
	my $userAttributes = smlib::users::getUserAttributes($uid);

	# Check we're an admin
	if (!defined($userAttributes->{'Authority'}) || $userAttributes->{'Authority'} ne "Admin") {
		return RES_ERROR;
	}

	my $apiCapabilities = smlib::users::getAPICapabilities($uid);

	# Setup return
	my $res;
	$res->{'Username'} = $username;
	$res->{'UserID'} = $uid;
	$res->{'APICapabilities'} = $apiCapabilities;
	$res->{'UserAttributes'} = $userAttributes;

	return $res;
}





1;
# vim: ts=4
