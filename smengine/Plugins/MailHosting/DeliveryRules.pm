# Delivery Rules functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class MailHosting::DeliveryRules
# Delivery rules handling class
package MailHosting::DeliveryRules;


use strict;
use warnings;

use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::soap;
use smlib::util;

use smlib::MailHosting::DeliveryRules;

# Plugin info
our $pluginInfo = {
	Name => "MailHosting: DeliveryRules",
	Init => \&init,
};



## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('MailHosting::DeliveryRules','ping','MailHosting/DeliveryRules/Test');

	Auth::aclAdd('MailHosting::DeliveryRules','createMailboxDeliveryRule','MailHosting/DeliveryRules/Mailboxes/Add');
	Auth::aclAdd('MailHosting::DeliveryRules','updateMailboxDeliveryRule','MailHosting/DeliveryRules/Mailboxes/Remove');
	Auth::aclAdd('MailHosting::DeliveryRules','removeMailboxDeliveryRule','MailHosting/DeliveryRules/Mailboxes/Update');
	Auth::aclAdd('MailHosting::DeliveryRules','getMailboxDeliveryRules','MailHosting/DeliveryRules/Mailboxes/List');
	Auth::aclAdd('MailHosting::DeliveryRules','getMailboxDeliveryRule','MailHosting/DeliveryRules/Mailboxes/Get');

	Auth::aclAdd('MailHosting::DeliveryRules','createTransportDeliveryRule','MailHosting/DeliveryRules/MailTransports/Add');
	Auth::aclAdd('MailHosting::DeliveryRules','updateTransportDeliveryRule','MailHosting/DeliveryRules/MailTransports/Remove');
	Auth::aclAdd('MailHosting::DeliveryRules','removeTransportDeliveryRule','MailHosting/DeliveryRules/MailTransports/Update');
	Auth::aclAdd('MailHosting::DeliveryRules','getTransportDeliveryRules','MailHosting/DeliveryRules/MailTransports/List');
	Auth::aclAdd('MailHosting::DeliveryRules','getTransportDeliveryRule','MailHosting/DeliveryRules/MailTransports/Get');

	Auth::aclAdd('MailHosting::DeliveryRules','getActionList','MailHosting/DeliveryRules/Action/List');

	return RES_OK;
}



## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}


#
# Mailboxes
#


## @method createMailboxDeliveryRule($serverGroupID,$deliveryRuleInfo)
# Create a mailbox delivery rule
#
# @param serverGroupID Server group ID
#
# @param deliveryRuleInfo Mailbox delivery rule info hash ref
# @li MailboxID - Mailbox ID
# @li SenderSpec - Optional sender specification
# @li RecipientSpec - Optional recipient specification
# @li Action - Optional action to perform
# @li Detail - Optional detail
# @li ValidFrom - Optional valid from date
# @li ValidTo - Optional valid to date
#
# @return mailbox delivery rule ID
sub createMailboxDeliveryRule
{
	my (undef,$serverGroupID,$deliveryRuleInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($deliveryRuleInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' not defined");
	}
	if (!isHash($deliveryRuleInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' is not a HASH");
	}

	if (!defined($deliveryRuleInfo->{'MailboxID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'MailboxID' not defined");
	}
	if (!defined($deliveryRuleInfo->{'MailboxID'} = isNumber($deliveryRuleInfo->{'MailboxID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'MailboxID' is invalid");
	}

	if (defined($deliveryRuleInfo->{'SenderSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"SenderSpec"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
		}
		if ($deliveryRuleInfo->{'SenderSpec'} eq "") {
			delete($deliveryRuleInfo->{'SenderSpec'});
		} elsif (!defined($deliveryRuleInfo->{"SenderSpec"} = isEmailSpec($deliveryRuleInfo->{'SenderSpec'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
		}
	}

	if (defined($deliveryRuleInfo->{'RecipientSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"RecipientSpec"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
		}
		if ($deliveryRuleInfo->{'RecipientSpec'} eq "") {
			delete($deliveryRuleInfo->{'RecipientSpec'});
		} elsif (!defined($deliveryRuleInfo->{'RecipientSpec'} = isEmailSpec($deliveryRuleInfo->{'RecipientSpec'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
		}
	}

	if (defined($deliveryRuleInfo->{'Action'})) {
		if (!isVariable($deliveryRuleInfo->{'Action'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'Action' is invalid");
		}
		if ($deliveryRuleInfo->{'Action'} eq "") {
			delete($deliveryRuleInfo->{'Action'});
		}
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		if (!isVariable($deliveryRuleInfo->{'Detail'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'Detail' is invalid");
		}
		if ($deliveryRuleInfo->{'Detail'} eq "") {
			delete($deliveryRuleInfo->{'Detail'});
		}
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		if (!isVariable($deliveryRuleInfo->{'ValidFrom'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
		}
		if ($deliveryRuleInfo->{'ValidFrom'} eq "") {
			delete($deliveryRuleInfo->{'ValidFrom'});
		} elsif (!defined($deliveryRuleInfo->{'ValidFrom'} = isDate($deliveryRuleInfo->{'ValidFrom'},ISDATE_UNIX))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
		}
	}
	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		if (!isVariable($deliveryRuleInfo->{'ValidTo'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
		}
		if ($deliveryRuleInfo->{'ValidTo'} eq "") {
			delete($deliveryRuleInfo->{'ValidTo'});
		} elsif (!defined($deliveryRuleInfo->{'ValidTo'} = isDate($deliveryRuleInfo->{'ValidTo'},ISDATE_UNIX))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Build params
	my $deliveryRuleData;
	$deliveryRuleData->{'MailboxID'} = $deliveryRuleInfo->{'MailboxID'};
	$deliveryRuleData->{'SenderSpec'} = $deliveryRuleInfo->{'SenderSpec'} if (defined($deliveryRuleInfo->{'SenderSpec'}));
	$deliveryRuleData->{'RecipientSpec'} = $deliveryRuleInfo->{'RecipientSpec'} if (defined($deliveryRuleInfo->{'RecipientSpec'}));
	$deliveryRuleData->{'Action'} = $deliveryRuleInfo->{'Action'} if (defined($deliveryRuleInfo->{'Action'}));
	$deliveryRuleData->{'Detail'} = $deliveryRuleInfo->{'Detail'} if (defined($deliveryRuleInfo->{'Detail'}));
	$deliveryRuleData->{'ValidFrom'} = $deliveryRuleInfo->{'ValidFrom'} if (defined($deliveryRuleInfo->{'ValidFrom'}));
	$deliveryRuleData->{'ValidTo'} = $deliveryRuleInfo->{'ValidTo'} if (defined($deliveryRuleInfo->{'ValidTo'}));

	my $res = smlib::MailHosting::DeliveryRules::createMailboxDeliveryRule($serverGroupID,$deliveryRuleData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','DeliveryRuleID');
}



## @method updateMailboxDeliveryRule($serverGroupID,$deliveryRuleInfo)
# Update a mailbox delivery rule
#
# @param serverGroupID Server group ID
#
# @param deliveryRuleInfo Mailbox delivery rule info hash ref
# @li ID - Mailbox delivery rule ID
# @li SenderSpec - Optional sender specification
# @li RecipientSpec - Optional recipient specification
# @li Action - Optional action
# @li Detail - Optional detail
# @li ValidFrom - Optional valid from date
# @li ValidTo - Optional valid to date
#
# @return 0 on success or < 0 on error
sub updateMailboxDeliveryRule
{
	my (undef,$serverGroupID,$deliveryRuleInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($deliveryRuleInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' not defined");
	}
	if (!isHash($deliveryRuleInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' is not a HASH");
	}

	if (!defined($deliveryRuleInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' has no element 'ID'");
	}
	if (!defined($deliveryRuleInfo->{'ID'} = isNumber($deliveryRuleInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ID' is invalid");
	}

	if (defined($deliveryRuleInfo->{'SenderSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"SenderSpec"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
		}
		if ($deliveryRuleInfo->{'SenderSpec'} eq "") {
			delete($deliveryRuleInfo->{'SenderSpec'});
		} elsif (!defined(my $senderSpec = isEmailSpec($deliveryRuleInfo->{'SenderSpec'}))) {
			if (!($deliveryRuleInfo->{'SenderSpec'} eq " ")) {
				return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
			}
		}
	}

	if (defined($deliveryRuleInfo->{'RecipientSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"RecipientSpec"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
		}
		if ($deliveryRuleInfo->{'RecipientSpec'} eq "") {
			delete($deliveryRuleInfo->{'RecipientSpec'});
		} elsif (!defined(my $recipientSpec = isEmailSpec($deliveryRuleInfo->{'RecipientSpec'}))) {
			if (!($deliveryRuleInfo->{'RecipientSpec'} eq " ")) {
				return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
			}
		}
	}

	if (defined($deliveryRuleInfo->{'Action'})) {
		if (!isVariable($deliveryRuleInfo->{'Action'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'Action' is invalid");
		}
		if ($deliveryRuleInfo->{'Action'} eq "") {
			delete($deliveryRuleInfo->{'Action'});
		}
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		if (!isVariable($deliveryRuleInfo->{'Detail'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'Detail' is invalid");
		}
		if ($deliveryRuleInfo->{'Detail'} eq "") {
			delete($deliveryRuleInfo->{'Detail'});
		}
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		if (!isVariable($deliveryRuleInfo->{'ValidFrom'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
		}
		if ($deliveryRuleInfo->{'ValidFrom'} eq "") {
			delete($deliveryRuleInfo->{'ValidFrom'});
		} elsif (!defined(my $validFrom = isDate($deliveryRuleInfo->{'ValidFrom'},ISDATE_UNIX))) {
			if (!($deliveryRuleInfo->{'ValidFrom'} eq " ")) {
				return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
			}
		}
	}
	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		if (!isVariable($deliveryRuleInfo->{'ValidTo'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
		}
		if ($deliveryRuleInfo->{'ValidTo'} eq "") {
			delete($deliveryRuleInfo->{'ValidTo'});
		} elsif (!defined(my $validTo = isDate($deliveryRuleInfo->{'ValidTo'},ISDATE_UNIX))) {
			if (!($deliveryRuleInfo->{'ValidTo'} eq " ")) {
				return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
			}
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;
	$i->{'ID'} = $deliveryRuleInfo->{'ID'};
	$i->{'SenderSpec'} = $deliveryRuleInfo->{'SenderSpec'} if (defined($deliveryRuleInfo->{'SenderSpec'}));
	$i->{'RecipientSpec'} = $deliveryRuleInfo->{'RecipientSpec'} if (defined($deliveryRuleInfo->{'RecipientSpec'}));
	$i->{'Action'} = $deliveryRuleInfo->{'Action'} if (defined($deliveryRuleInfo->{'Action'}));
	$i->{'Detail'} = $deliveryRuleInfo->{'Detail'} if (defined($deliveryRuleInfo->{'Detail'}));
	$i->{'ValidFrom'} = $deliveryRuleInfo->{'ValidFrom'} if (defined($deliveryRuleInfo->{'ValidFrom'}));
	$i->{'ValidTo'} = $deliveryRuleInfo->{'ValidTo'} if (defined($deliveryRuleInfo->{'ValidTo'}));

	# Do the update
	my $res = smlib::MailHosting::DeliveryRules::updateMailboxDeliveryRule($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeMailboxDeliveryRule($serverGroupID,$deliveryRuleID)
# Remove mailbox delivery rule
#
# @param serverGroupID Server group ID
#
# @param deliveryRuleID Mailbox delivery rule ID
#
# @return 0 on success or < 0 on error
sub removeMailboxDeliveryRule
{
	my (undef,$serverGroupID,$deliveryRuleID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($deliveryRuleID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleID' undefined");
	}
	if (!defined($deliveryRuleID = isNumber($deliveryRuleID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $res = smlib::MailHosting::DeliveryRules::removeMailboxDeliveryRule($serverGroupID,$deliveryRuleID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method getMailboxDeliveryRules($serverGroupID,$mailboxID,$search)
# Return a list of delivery rules a mailbox has
#
# @param serverGroupID Server group ID
#
# @param mailboxID Mailbox ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Delivery rule ID
# @li MailboxID - Mailbox ID
# @li SenderSpec - Sender specification
# @li RecipientSpec - Recipient specification
# @li Action - Action to perform
# @li Detail - Message details
# @li ValidFrom - Valid from date
# @li ValidTo - Valid to date
sub getMailboxDeliveryRules
{
	my (undef,$serverGroupID,$mailboxID,$search) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($mailboxID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxID' not defined");
	}
	if (!defined($mailboxID = isNumber($mailboxID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxID' is invalid");
	}

	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::MailHosting::DeliveryRules::getMailboxDeliveryRules($serverGroupID,$mailboxID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('MailboxID','int');
	$response->addField('SenderSpec','string');
	$response->addField('RecipientSpec','string');
	$response->addField('Action','string');
	$response->addField('Detail','string');
	$response->addField('ValidFrom','int');
	$response->addField('ValidTo','int');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getMailboxDeliveryRule($serverGroupID,$deliveryRuleID)
# Return a hash containing the info for this mailbox delivery rule
#
# @param serverGroupID Server group ID
#
# @param deliveryRuleID Delivery rule ID
#
# @return mailbox delivery rule hash ref
# @li ID - Delivery rule ID
# @li MailboxID - Mailbox ID
# @li SenderSpec - Sender specification
# @li RecipientSpec - Recipient specification
# @li Action - Action to perform
# @li Detail - Message detail
# @li ValidFrom - Valid from date
# @li ValidTo - Valid to date
sub getMailboxDeliveryRule
{
	my (undef,$serverGroupID,$deliveryRuleID) = @_;

	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($deliveryRuleID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleID' undefined");
	}
	if (!defined($deliveryRuleID = isNumber($deliveryRuleID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::MailHosting::DeliveryRules::getMailboxDeliveryRule($serverGroupID,$deliveryRuleID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('MailboxID','int');
	$response->addField('SenderSpec','string');
	$response->addField('RecipientSpec','string');
	$response->addField('Action','string');
	$response->addField('Detail','string');
	$response->addField('ValidFrom','int');
	$response->addField('ValidTo','int');
	$response->parseHashRef($rawData);

	return $response->export();
}


#
# MailTransports
#


## @method createTransportDeliveryRule($serverGroupID,$deliveryRuleInfo)
# Create a transport delivery rule
#
# @param serverGroupID Server group ID
#
# @param deliveryRuleInfo Transport delivery rule info hash ref
# @li TransportID - Transport ID
# @li SenderSpec - Optional sender specification
# @li RecipientSpec - Optional sender specification
# @li Action - Optional action to perform
# @li Detail - Optional detail
# @li ValidFrom - Optional valid from date
# @li ValidTo - Optional valid to date
#
# @return transport delivery rule ID
sub createTransportDeliveryRule
{
	my (undef,$serverGroupID,$deliveryRuleInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($deliveryRuleInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' not defined");
	}
	if (!isHash($deliveryRuleInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' is not a HASH");
	}

	if (!defined($deliveryRuleInfo->{'TransportID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'TransportID' not defined");
	}
	if (!defined($deliveryRuleInfo->{'TransportID'} = isNumber($deliveryRuleInfo->{'TransportID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'TransportID' is invalid");
	}

	if (defined($deliveryRuleInfo->{'SenderSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"SenderSpec"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
		}
		if ($deliveryRuleInfo->{'SenderSpec'} eq "") {
			delete($deliveryRuleInfo->{'SenderSpec'});
		} elsif (!defined($deliveryRuleInfo->{"SenderSpec"} = isEmailSpec($deliveryRuleInfo->{'SenderSpec'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
		}
	}

	if (defined($deliveryRuleInfo->{'RecipientSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"RecipientSpec"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
		}
		if ($deliveryRuleInfo->{'RecipientSpec'} eq "") {
			delete($deliveryRuleInfo->{'RecipientSpec'});
		} elsif (!defined($deliveryRuleInfo->{'RecipientSpec'} = isEmailSpec($deliveryRuleInfo->{'RecipientSpec'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
		}
	}

	if (defined($deliveryRuleInfo->{'Action'})) {
		if (!isVariable($deliveryRuleInfo->{'Action'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'Action' is invalid");
		}
		if ($deliveryRuleInfo->{'Action'} eq "") {
			delete($deliveryRuleInfo->{'Action'});
		}
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		if (!isVariable($deliveryRuleInfo->{'Detail'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'Detail' is invalid");
		}
		if ($deliveryRuleInfo->{'Detail'} eq "") {
			delete($deliveryRuleInfo->{'Detail'});
		}
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		if (!isVariable($deliveryRuleInfo->{'ValidFrom'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
		}
		if ($deliveryRuleInfo->{'ValidFrom'} eq "") {
			delete($deliveryRuleInfo->{'ValidFrom'});
		} elsif (!defined($deliveryRuleInfo->{'ValidFrom'} = isDate($deliveryRuleInfo->{'ValidFrom'},ISDATE_UNIX))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
		}
	}
	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		if (!isVariable($deliveryRuleInfo->{'ValidTo'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
		}
		if ($deliveryRuleInfo->{'ValidTo'} eq "") {
			delete($deliveryRuleInfo->{'ValidTo'});
		} elsif (!defined($deliveryRuleInfo->{'ValidTo'} = isDate($deliveryRuleInfo->{'ValidTo'},ISDATE_UNIX))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Build params
	my $deliveryRuleData;
	$deliveryRuleData->{'TransportID'} = $deliveryRuleInfo->{'TransportID'};
	$deliveryRuleData->{'SenderSpec'} = $deliveryRuleInfo->{'SenderSpec'} if (defined($deliveryRuleInfo->{'SenderSpec'}));
	$deliveryRuleData->{'RecipientSpec'} = $deliveryRuleInfo->{'RecipientSpec'} if (defined($deliveryRuleInfo->{'RecipientSpec'}));
	$deliveryRuleData->{'Action'} = $deliveryRuleInfo->{'Action'} if (defined($deliveryRuleInfo->{'Action'}));
	$deliveryRuleData->{'Detail'} = $deliveryRuleInfo->{'Detail'} if (defined($deliveryRuleInfo->{'Detail'}));
	$deliveryRuleData->{'ValidFrom'} = $deliveryRuleInfo->{'ValidFrom'} if (defined($deliveryRuleInfo->{'ValidFrom'}));
	$deliveryRuleData->{'ValidTo'} = $deliveryRuleInfo->{'ValidTo'} if (defined($deliveryRuleInfo->{'ValidTo'}));

	my $res = smlib::MailHosting::DeliveryRules::createTransportDeliveryRule($serverGroupID,$deliveryRuleData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','DeliveryRuleID');
}



## @method updateTransportDeliveryRule($serverGroupID,$deliveryRuleInfo)
# Update a transport delivery rule
#
# @param serverGroupID Server group ID
#
# @param deliveryRuleInfo Transport delivery rule info hash ref
# @li ID - Transport delivery rule ID
# @li SenderSpec - Optional sender specification
# @li RecipientSpec - Optional recipient specification
# @li Action - Optional action
# @li Detail - Optional detail
# @li ValidFrom - Optional valid from date
# @li ValidTo - Optional valid to date
#
# @return 0 on success or < 0 on error
sub updateTransportDeliveryRule
{
	my (undef,$serverGroupID,$deliveryRuleInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($deliveryRuleInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' not defined");
	}
	if (!isHash($deliveryRuleInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' is not a HASH");
	}

	if (!defined($deliveryRuleInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' has no element 'ID'");
	}
	if (!defined($deliveryRuleInfo->{'ID'} = isNumber($deliveryRuleInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ID' is invalid");
	}

	if (defined($deliveryRuleInfo->{'SenderSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"SenderSpec"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
		}
		if ($deliveryRuleInfo->{'SenderSpec'} eq "") {
			delete($deliveryRuleInfo->{'SenderSpec'});
		} elsif (!defined(my $senderSpec = isEmailSpec($deliveryRuleInfo->{'SenderSpec'}))) {
			if (!($deliveryRuleInfo->{'SenderSpec'} eq " ")) {
				return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'SenderSpec' is invalid");
			}
		}
	}

	if (defined($deliveryRuleInfo->{'RecipientSpec'})) {
		if (!isVariable($deliveryRuleInfo->{"RecipientSpec"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
		}
		if ($deliveryRuleInfo->{'RecipientSpec'} eq "") {
			delete($deliveryRuleInfo->{'RecipientSpec'});
		} elsif (!defined(my $recipientSpec = isEmailSpec($deliveryRuleInfo->{'RecipientSpec'}))) {
			if (!($deliveryRuleInfo->{'RecipientSpec'} eq " ")) {
				return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'RecipientSpec' is invalid");
			}
		}
	}

	if (defined($deliveryRuleInfo->{'Action'})) {
		if (!isVariable($deliveryRuleInfo->{'Action'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'Action' is invalid");
		}
		if ($deliveryRuleInfo->{'Action'} eq "") {
			delete($deliveryRuleInfo->{'Action'});
		}
	}

	if (defined($deliveryRuleInfo->{'Detail'})) {
		if (!isVariable($deliveryRuleInfo->{'Detail'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'Detail' is invalid");
		}
		if ($deliveryRuleInfo->{'Detail'} eq "") {
			delete($deliveryRuleInfo->{'Detail'});
		}
	}

	if (defined($deliveryRuleInfo->{'ValidFrom'})) {
		if (!isVariable($deliveryRuleInfo->{'ValidFrom'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
		}
		if ($deliveryRuleInfo->{'ValidFrom'} eq "") {
			delete($deliveryRuleInfo->{'ValidFrom'});
		} elsif (!defined(my $validFrom = isDate($deliveryRuleInfo->{'ValidFrom'},ISDATE_UNIX))) {
			if (!($deliveryRuleInfo->{'ValidFrom'} eq " ")) {
				return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidFrom' is invalid");
			}
		}
	}
	if (defined($deliveryRuleInfo->{'ValidTo'})) {
		if (!isVariable($deliveryRuleInfo->{'ValidTo'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
		}
		if ($deliveryRuleInfo->{'ValidTo'} eq "") {
			delete($deliveryRuleInfo->{'ValidTo'});
		} elsif (!defined(my $validTo = isDate($deliveryRuleInfo->{'ValidTo'},ISDATE_UNIX))) {
			if (!($deliveryRuleInfo->{'ValidTo'} eq " ")) {
				return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleInfo' element 'ValidTo' is invalid");
			}
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;
	$i->{'ID'} = $deliveryRuleInfo->{'ID'};
	$i->{'SenderSpec'} = $deliveryRuleInfo->{'SenderSpec'} if (defined($deliveryRuleInfo->{'SenderSpec'}));
	$i->{'RecipientSpec'} = $deliveryRuleInfo->{'RecipientSpec'} if (defined($deliveryRuleInfo->{'RecipientSpec'}));
	$i->{'Action'} = $deliveryRuleInfo->{'Action'} if (defined($deliveryRuleInfo->{'Action'}));
	$i->{'Detail'} = $deliveryRuleInfo->{'Detail'} if (defined($deliveryRuleInfo->{'Detail'}));
	$i->{'ValidFrom'} = $deliveryRuleInfo->{'ValidFrom'} if (defined($deliveryRuleInfo->{'ValidFrom'}));
	$i->{'ValidTo'} = $deliveryRuleInfo->{'ValidTo'} if (defined($deliveryRuleInfo->{'ValidTo'}));

	# Do the update
	my $res = smlib::MailHosting::DeliveryRules::updateTransportDeliveryRule($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeTransportDeliveryRule($serverGroupID,$deliveryRuleID)
# Remove transport delivery rule
#
# @param serverGroupID Server group ID
#
# @param deliveryRuleID Transport delivery rule ID
#
# @return 0 on success or < 0 on error
sub removeTransportDeliveryRule
{
	my (undef,$serverGroupID,$deliveryRuleID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($deliveryRuleID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleID' undefined");
	}
	if (!defined($deliveryRuleID = isNumber($deliveryRuleID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $res = smlib::MailHosting::DeliveryRules::removeTransportDeliveryRule($serverGroupID,$deliveryRuleID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method getTransportDeliveryRules($serverGroupID,$transportID,$search)
# Return a list of delivery rules a transport has
#
# @param serverGroupID Server group ID
#
# @param transportID Transport ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Delivery rule ID
# @li TransportID - Transport ID
# @li SenderSpec - Sender specification
# @li RecipientSpec - Recipient specification
# @li Action - Action to perform
# @li Detail - Message details
# @li ValidFrom - Valid from date
# @li ValidTo - Valid to date
sub getTransportDeliveryRules
{
	my (undef,$serverGroupID,$transportID,$search) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($transportID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportID' not defined");
	}
	if (!defined($transportID = isNumber($transportID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportID' is invalid");
	}

	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::MailHosting::DeliveryRules::getTransportDeliveryRules($serverGroupID,$transportID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('TransportID','int');
	$response->addField('SenderSpec','string');
	$response->addField('RecipientSpec','string');
	$response->addField('Action','string');
	$response->addField('Detail','string');
	$response->addField('ValidFrom','int');
	$response->addField('ValidTo','int');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getTransportDeliveryRule($serverGroupID,$deliveryRuleID)
# Return a hash containing the info for this transport delivery rule
#
# @param serverGroupID Server group ID
#
# @param deliveryRuleID Delivery rule ID
#
# @return transport delivery rule hash ref
# @li ID - Delivery rule ID
# @li TransportID - Transport ID
# @li SenderSpec - Sender specification
# @li RecipientSpec - Recipient specification
# @li Action - Action to perform
# @li Detail - Message detail
# @li ValidFrom - Valid from date
# @li ValidTo - Valid to date
sub getTransportDeliveryRule
{
	my (undef,$serverGroupID,$deliveryRuleID) = @_;

	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($deliveryRuleID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleID' undefined");
	}
	if (!defined($deliveryRuleID = isNumber($deliveryRuleID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'deliveryRuleID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::MailHosting::DeliveryRules::getTransportDeliveryRule($serverGroupID,$deliveryRuleID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('TransportID','int');
	$response->addField('SenderSpec','string');
	$response->addField('RecipientSpec','string');
	$response->addField('Action','string');
	$response->addField('Detail','string');
	$response->addField('ValidFrom','int');
	$response->addField('ValidTo','int');
	$response->parseHashRef($rawData);

	return $response->export();
}


## @method getActionList($serverGroupID)
# Return a list of delivery rule actions
#
# @param serverGroupID Server group ID
#
# @return Array ref
# @li AutoReply
# @li Whitelist
# @li Blacklist
sub getActionList
{
	my (undef,$serverGroupID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my ($rawData) = smlib::MailHosting::DeliveryRules::getActionList($serverGroupID);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->addField('Action','string');
	$response->parseArrayRef($rawData);

	return $response->export();
}


1;
# vim: ts=4
