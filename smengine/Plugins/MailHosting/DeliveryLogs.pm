# Delivery Logs functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class MailHosting::DeliveryLogs
# Delivery logs handling class
package MailHosting::DeliveryLogs;


use strict;
use warnings;

use smlib::constants;
use awitpt::db::dblayer;
use smlib::logging;
use smlib::soap;
use smlib::util;

use smlib::MailHosting::DeliveryLogs;

# Plugin info
our $pluginInfo = {
	Name => "MailHosting: DeliveryLogs",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('MailHosting::DeliveryLogs','ping','MailHosting/DeliveryLogs/Test');

	Auth::aclAdd('MailHosting::DeliveryLogs','getTransportDeliveryLogs','MailHosting/DeliveryLogs/MailTransports/List');
	Auth::aclAdd('MailHosting::DeliveryLogs','getMailboxDeliveryLogs','MailHosting/DeliveryLogs/Mailboxes/List');

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getMailboxDeliveryLogs($mailboxID,$search)
# Retrieve mailbox delivery logs
#
# @param serverGroupID Server group ID
#
# @param mailboxID Mailbox ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Delivery log ID
# @li Timestamp - Delivery log timestamp
# @li Cluster - Server cluster used
# @li Server - Server used
# @li SendingServerName - Sending server name
# @li SendingServerIP - Sending server IP
# @li QueueID - Queue ID
# @li MessageID - Message ID
# @li EnvelopeFrom - Envelope from
# @li EnvelopeTo - Envelope to
# @li Size - Size of message
# @li Destination - Destination of message
# @li Status - Status of message
sub getMailboxDeliveryLogs
{
	my (undef,$serverGroupID,$mailboxID,$search) = @_;


	# Check params
	if (!defined($mailboxID)) {
		setError("Parameter 'mailboxID' is not defined");
		return ERR_PARAM;
	}
	if (!defined($mailboxID = isNumber($mailboxID))) {
		setError("Parameter 'mailboxID' is invalid");
		return ERR_PARAM;
	}

	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID and its valid
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this mailbox
		if (smlib::MailHosting::canAgentAccessMailbox($callerAgentID,$mailboxID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox ID '$mailboxID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Get mailbox info
	my $mailbox = smlib::MailHosting::getMailbox($serverGroupID,$mailboxID);
	if (ref($mailbox) ne "HASH") {
		return SOAPError($mailbox,getError());
	}

	# Make spec
	my $spec = $mailbox->{'Address'}."@".$mailbox->{'DomainName'};

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::MailHosting::DeliveryLogs::getMailboxDeliveryLogs($serverGroupID,$spec,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Timestamp','string');
	$response->addField('Cluster','string');
	$response->addField('Server','string');
	$response->addField('SendingServerName','string');
	$response->addField('SendingServerIP','string');
	$response->addField('QueueID','string');
	$response->addField('MessageID','string');
	$response->addField('EnvelopeFrom','string');
	$response->addField('EnvelopeTo','string');
	$response->addField('Size','string');
	$response->addField('Destination','string');
	$response->addField('Status','string');

	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}


## @method getTransportDeliveryLogs($transportID,$search)
# Retrieve transport delivery logs
#
# @param serverGroupID Server group ID
#
# @param transportID Transport ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Delivery log ID
# @li Timestamp - Delivery log timestamp
# @li Cluster - Server cluster used
# @li Server - Server used
# @li SendingServerName - Sending server name
# @li SendingServerIP - Sending server IP
# @li QueueID - Queue ID
# @li MessageID - Message ID
# @li EnvelopeFrom - Envelope from
# @li EnvelopeTo - Envelope to
# @li Size - Size of message
# @li Destination - Destination of message
# @li Status - Status of message
sub getTransportDeliveryLogs
{
	my (undef,$serverGroupID,$transportID,$search) = @_;


	# Check params
	if (!defined($transportID)) {
		setError("Parameter 'transportID' is not defined");
		return ERR_PARAM;
	}
	if (!($transportID = isNumber($transportID))) {
		setError("Parameter 'transportID' is invalid");
		return ERR_PARAM;
	}

	if (defined($search) && !isHash($search)) {
		setError("Parameter 'search' is not a HASH");
		return ERR_PARAM;
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID and its valid
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this transport
		if (smlib::MailHosting::canAgentAccessMailTransport($callerAgentID,$transportID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox ID '$transportID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Get transport info
	my $transport = smlib::MailHosting::getMailTransport($serverGroupID,$transportID);
	if (ref($transport) ne "HASH") {
		return SOAPError($transport,getError());
	}

	# Make spec
	my $spec = "@".$transport->{'DomainName'};

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::MailHosting::DeliveryLogs::getTransportDeliveryLogs($serverGroupID,$spec,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Timestamp','string');
	$response->addField('Cluster','string');
	$response->addField('Server','string');
	$response->addField('SendingServerName','string');
	$response->addField('SendingServerIP','string');
	$response->addField('QueueID','string');
	$response->addField('MessageID','string');
	$response->addField('EnvelopeFrom','string');
	$response->addField('EnvelopeTo','string');
	$response->addField('Size','string');
	$response->addField('Destination','string');
	$response->addField('Status','string');

	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



1;
# vim: ts=4
