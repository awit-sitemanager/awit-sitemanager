# Radius user SOAP interface
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class Radius
# This is the radius user handling class
package Radius;

use strict;

use Auth;

use smlib::Radius;

use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;

use smlib::Agent qw( resolveAgentID );

# Plugin info
our $pluginInfo = {
	Name => "Radius",
	Init => \&init,
};
# Radius user authentication
our $authInfo = {
	Name => "Radius Authentication",
	Type => "Radius",
	Authenticate => \&Radius_Auth,
};




## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Radius','ping','Radius/Test');

	# Special case, we get this priv if we authenticate
	Auth::aclAdd('Radius','authenticate','Radius/Authenticate');

	Auth::aclAdd('Radius','createRadiusUser','Radius/Users/Add');
	Auth::aclAdd('Radius','updateRadiusUser','Radius/Users/Update');
	Auth::aclAdd('Radius','removeRadiusUser','Radius/Users/Remove');
	Auth::aclAdd('Radius','getRadiusUsers','Radius/Users/List');
	Auth::aclAdd('Radius','getRadiusUser','Radius/Users/Get');
	Auth::aclAdd('Radius','getRadiusClasses','Radius/Classes/List');
	
	Auth::aclAdd('Radius','getRadiusServerGroups','Radius/ServerGroups/List');

	Auth::aclAdd('Radius','getRadiusUserLogs','Radius/Users/Logs/List');
	Auth::aclAdd('Radius','getRadiusUserLogsSummary','Radius/Users/Logs/List');

	# Add authentication plugin
	Auth::pluginAdd($authInfo);

	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping {
	return 0;
}


## @method authenticate
# Dummy function to allow authentication
#
# @return Will always return 0
sub authenticate
{
	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Radius')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}


	return SOAPSuccess(Auth::getUserAttribute('RadiusUserID'),'int','UserID');
}




## @method createRadiusUser($serverGroupID,$userInfo)
# Create a radius user
#
# @param serverGroupID Server group ID
#
# @param userInfo User info hash ref
# @li AgentID - Agent ID
# @li Username - Radius username
# @li Password - Optional password for this account
# @li ServerGroupID - Server Group ID
# @li ClassID - Class ID
# @li UsageCap - Optional usage cap
# @li NotifyMethod - Optional notify method, comma separated
# @li AgentRef - Optional reference for the agent
# @li AgentDisabled - Optional agent disabled flag
#
# @return Radius user ID
sub createRadiusUser
{
	my (undef,$serverGroupID,$userInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' not defined");
	}
	if (!isHash($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' is not a HASH");
	}

	if (!defined($userInfo->{'AgentID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' not defined");
	}
	if (!defined($userInfo->{'AgentID'} = isNumber($userInfo->{'AgentID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' is invalid");
	}

	if (!defined($userInfo->{'Username'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' has no element 'Username'");
	}	
	if (!isVariable($userInfo->{"Username"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Username' is invalid");
	}
	if ($userInfo->{"Username"} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Username' is blank");
	}
	if (!defined($userInfo->{'Username'} = isUsername($userInfo->{'Username'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Username' is invalid");
	}

	if (defined($userInfo->{'Password'})) {
		if (!isVariable($userInfo->{"Password"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Password' is invalid");
		}
		# Delete if password is empty
		if ($userInfo->{"Password"} eq "") {
			delete($userInfo->{"Password"});
		}
	}

	if (!defined($userInfo->{'ClassID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' has no element 'ClassID' defined");
	}
	if (!defined($userInfo->{'ClassID'} = isNumber($userInfo->{'ClassID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'ClassID' is invalid");
	}

	# Check UsageCap
	if (defined($userInfo->{'UsageCap'})) {
		# Check for non-variable
		if (!isVariable($userInfo->{'UsageCap'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'UsageCap' is invalid");
		}
		# Delete element if string is empty
		if ($userInfo->{'UsageCap'} eq "") {
			delete($userInfo->{'UsageCap'});
		} elsif (!defined($userInfo->{'UsageCap'} = isNumber($userInfo->{'UsageCap'},ISNUMBER_ALLOW_ZERO))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'UsageCap' is invalid");
		}
	}

	# Check NotifyMethod
	if (defined($userInfo->{'NotifyMethod'})) {
		if (!isVariable($userInfo->{"NotifyMethod"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'NotifyMethod' is invalid");
		}
		# Delete element if string is empty
		if ($userInfo->{'NotifyMethod'} eq "") {
			delete($userInfo->{'NotifyMethod'})
		}
	}	

	if (defined($userInfo->{'AgentRef'})) {
		if (!isVariable($userInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentRef' is invalid");
		}
	}

	# Check AgentDisabled
	if (defined($userInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($userInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentDisabled' is invalid");
		}
		# Delete element if string is empty
		if (!defined($userInfo->{'AgentDisabled'} = isBoolean($userInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Check if the agent ID is valid
		if (smlib::Agent::isAgentValid($userInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' has an invalid value");
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$userInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this radius class
		if (smlib::Radius::canAccessRadiusClass($callerAgentID,$userInfo->{'ClassID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ClassID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Build params
	my $userData;
	$userData->{'AgentID'} = $userInfo->{'AgentID'};
	$userData->{'Username'} = $userInfo->{'Username'};
	$userData->{'Password'} = $userInfo->{'Password'} if (defined($userInfo->{'Password'}));
	$userData->{'ServerGroupID'} = $userInfo->{'ServerGroupID'};
	$userData->{'ClassID'} = $userInfo->{'ClassID'};
	$userData->{'UsageCap'} = $userInfo->{'UsageCap'} if (defined($userInfo->{'UsageCap'}));
	$userData->{'NotifyMethod'} = $userInfo->{'NotifyMethod'} if (defined($userInfo->{'NotifyMethod'}));
	$userData->{'AgentDisabled'} = $userInfo->{'AgentDisabled'} if (defined($userInfo->{'AgentDisabled'}));
	$userData->{'AgentRef'} = $userInfo->{'AgentRef'} if (defined($userInfo->{'AgentRef'}));

	my $res = smlib::Radius::createRadiusUser($serverGroupID,$userData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','RadiusUserID');
}



## @method updateRadiusUser($serverGroupID,$userInfo)
# Update a radius user
#
# @param serverGroupID Server group ID
#
# @param userInfo User info hash ref
# @li ID - Radius user ID
# @li Password - Optional radius user password
# @li UsageCap - Optional radius usage cap
# @li NotifyMethod - Optional notification method
# @li AgentDisabled - Optional flag agent can set to disable this radius user
# @li AgentRef - Optional agent reference
#
# @return 0 on success or < 0 on error
sub updateRadiusUser
{
	my (undef,$serverGroupID,$userInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' undefined");
	}
	if (!isHash($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' is not a HASH");
	}

	if (!defined($userInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' has no element 'ID' defined");
	}
	if (!defined($userInfo->{'ID'} = isNumber($userInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'ID' is invalid");
	}

	if (defined($userInfo->{'Password'})) {
		if (!isVariable($userInfo->{"Password"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Password' is invalid");
		}
		# Delete if password is empty
		if ($userInfo->{"Password"} eq "") {
			delete($userInfo->{'Password'});
		}
	}

	# Check UsageCap
	if (defined($userInfo->{'UsageCap'})) {
		# Check for non-variable
		if (!isVariable($userInfo->{'UsageCap'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'UsageCap' is invalid");
		}
		# Delete element if string is empty
		if ($userInfo->{'UsageCap'} eq "") {
			delete($userInfo->{'UsageCap'});
		} elsif (!defined($userInfo->{'UsageCap'} = isNumber($userInfo->{'UsageCap'},ISNUMBER_ALLOW_ZERO))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'UsageCap' is invalid");
		}
	}

	# Check NotifyMethod
	if (defined($userInfo->{'NotifyMethod'})) {
		if (!isVariable($userInfo->{"NotifyMethod"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'NotifyMethod' is invalid");
		}
	}	

	if (defined($userInfo->{'AgentRef'})) {
		if (!isVariable($userInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentRef' is invalid");
		}
	}

	# Check AgentDisabled
	if (defined($userInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($userInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentDisabled' is invalid");
		}
		# Delete element if string is empty
		if (!defined($userInfo->{'AgentDisabled'} = isBoolean($userInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this radius user
		if (smlib::Radius::canAccessRadiusUser($callerAgentID,$userInfo->{'ID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius user ID '".$userInfo->{'ID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Build params
	my $userData;
	$userData->{'ID'} = $userInfo->{'ID'};
	$userData->{'Password'} = $userInfo->{'Password'} if (defined($userInfo->{'Password'}));
	$userData->{'UsageCap'} = $userInfo->{'UsageCap'} if (defined($userInfo->{'UsageCap'}));
	$userData->{'NotifyMethod'} = $userInfo->{'NotifyMethod'} if (defined($userInfo->{'NotifyMethod'}));
	$userData->{'AgentDisabled'} = $userInfo->{'AgentDisabled'} if (defined($userInfo->{'AgentDisabled'}));
	$userData->{'AgentRef'} = $userInfo->{'AgentRef'} if (defined($userInfo->{'AgentRef'}));

	my $res = smlib::Radius::updateRadiusUser($serverGroupID,$userData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeRadiusUser($serverGroupID,$userID)
# Remove radius user
#
# @param serverGroupID Server group ID
#
# @param userID Radius user ID
#
# @return 0 on success or < 0 on error
sub removeRadiusUser
{
	my (undef,$serverGroupID,$userID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' undefined");
	}
	if (!defined($userID = isNumber($userID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this user
		my $res = smlib::Radius::canAccessRadiusUser($callerAgentID,$userID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,getError());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius user ID '$userID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Get user info
	my $user = smlib::Radius::getRadiusUser($serverGroupID,$userID);
	if (ref($user) ne "HASH") {
		return SOAPError($user,getError());
	}

	# Send only the info we need
	my $i;
	$i->{'Username'} = $user->{'Username'};

	# Invalidate user logs and user notify tracking
	if ((my $res = smlib::Radius::invalidateRadiusUserLogs($serverGroupID,$i)) < 0) {
		return SOAPError($res,getError());
	}

	if ((my $res = smlib::Radius::invalidateRadiusUserNotifyTracking($serverGroupID,$i)) < 0) {
		return SOAPError($res,getError());
	}

	# Grab and sanitize data
	my $res = smlib::Radius::removeRadiusUser($serverGroupID,$userID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method getRadiusUsers($serverGroupID,$userInfo,$search)
# Return radius users with certain search criteria
#
# @param serverGroupID Server group ID
#
# @param userInfo User info hash ref
# @li AgentID - Limit returned results to this agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Radius user ID
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li Username - Radius username
# @li UsageCap - Radius usage cap in Mbyte
# @li NotifyMethod - Notification method
# @li AgentRef - Refernce for agent
# @li AgentDisabled - Set if the agent disabled this entry
# @li ClassID - Class ID
# @li ClassDescription - Class description
# @li RealmDescription - Realm description
# @li Disabled - Set if user is disabled
# @li Domain - Radius domain
# @li Service - Radius service
sub getRadiusUsers
{
	my (undef,$serverGroupID,$userInfo,$search) = @_;


	# Check serverGroupID
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	# We normally only have userInfo if we an admin
	if (defined($userInfo)) {
		# Make sure we're a hash
		if (!isHash($userInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($userInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($userInfo->{'AgentID'} = isNumber($userInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $userData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($userInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($userInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' has an invalid value");
			}

			$userData->{'AgentID'} = $userInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we have an agent ID, use it
		if (!defined($userInfo->{'AgentID'})) {
			$userInfo->{'AgentID'} = $callerAgentID;
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$userInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		$userData->{'AgentID'} = $userInfo->{'AgentID'};

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Radius::getRadiusUsers($serverGroupID,$userData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Resolve agent names
	$rawData = smlib::Agent::resolveAgentID($rawData);

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('AgentID','int');
	$response->addField('AgentName','string');
	$response->addField('Username','string');
	$response->addField('UsageCap','int');
	$response->addField('NotifyMethod','string');
	$response->addField('AgentRef','string');
	$response->addField('AgentDisabled','boolean');
	$response->addField('ClassID','int');
	$response->addField('ClassDescription','string');
	$response->addField('RealmDescription','string');
	$response->addField('Disabled','boolean');
	$response->addField('Domain','string');
	$response->addField('Service','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getRadiusUser($serverGroupID,$userID)
# Return radius user details
#
# @param serverGroupID Server group ID
#
# @param userID Radius user ID
#
# @return User info hash ref
# @li ID - Radius user ID
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li Username - Radius username
# @li UsageCap - Radius usage cap in Mbyte
# @li NotifyMethod - Notification method
# @li AgentRef - Refernce for agent
# @li AgentDisabled - Set if the agent disabled this entry
# @li ClassID - Class ID
# @li ClassDescription - Class description
# @li RealmDescription - Realm description
# @li Disabled - Set if user is disabled
# @li Domain - Radius domain
# @li Service - Radius service
sub getRadiusUser
{
	my (undef,$serverGroupID,$userID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' undefined");
	}
	if (!defined($userID = isNumber($userID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this radius user
		my $res = smlib::Radius::canAccessRadiusUser($callerAgentID,$userID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,getError());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius user ID '$userID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::Radius::getRadiusUser($serverGroupID,$userID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	# Resolve agent names
	$rawData = smlib::Agent::resolveAgentID($rawData);

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('AgentID','int');
	$response->addField('AgentName','string');
	$response->addField('Username','string');
	$response->addField('UsageCap','int');
	$response->addField('NotifyMethod','string');
	$response->addField('AgentRef','string');
	$response->addField('AgentDisabled','boolean');
	$response->addField('ClassID','int');
	$response->addField('ClassDescription','string');
	$response->addField('RealmDescription','string');
	$response->addField('Disabled','boolean');
	$response->addField('Domain','string');
	$response->addField('Service','string');

	$response->parseHashRef($rawData);

	return $response->export();
}



## @method getRadiusClasses($serverGroupID,$classInfo,$search)
# Return radius users with certain info
#
# @param serverGroupID Server group ID
#
# @param classInfo Class info hash ref. This hash must be defined even if not used.
# @li AgentID - Optional AgentID to limit results to. If not defined, results are limited to the current 
# caller privileges
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Radius class ID
# @li Service - Radius service
sub getRadiusClasses
{
	my (undef,$serverGroupID,$classInfo,$search) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	# Check if we have classinfo
	if (defined($classInfo)) {
		# Make sure we're a hash
		if (!isHash($classInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'classInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($classInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($classInfo->{'AgentID'} = isNumber($classInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'classInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $classData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($classInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($classInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'classInfo' element 'AgentID' has an invalid value");
			}

			$classData->{'AgentID'} = $classInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we don't have an agent ID, use the current one
		if (!defined($classInfo->{'AgentID'})) {
			$classInfo->{'AgentID'} = $callerAgentID;
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$classInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		$classData->{'AgentID'} = $classInfo->{'AgentID'};

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}
	
	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Radius::getRadiusClasses($serverGroupID,$classData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Service','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getRadiusUserLogs($serverGroupID,$userInfo,$search)
# Return radius user logs
#
# @param serverGroupID Server group ID
#
# @param userInfo User info hash ref
# @li UserID - Radius user ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Radius log ID
# @li Username - Radius username
# @li Status - Record status
# @li Timestamp - Record timestamp
# @li AcctSessionID - Session ID
# @li AcctSessionTime - Session time, time the session has been active
# @li NASIPAddress - NAS IP address
# @li NASPortType - NAS port type
# @li NASPort - NAS port
# @li CalledStationID - Called station ID
# @li CallingStationID - Calling station ID, could be caller ID
# @li NASTransmitRate - Transmit rate reported by NAS
# @li NASReceiveRate - Receive rate reported by NAS
# @li FramedIPAddress - IP address of this session
# @li AcctInputOctets - Input bytes
# @li AcctOutputOctets - Output bytes
# @li AcctInputGigawords - Number of times the AcctInputOctets counter overflowed
# @li AcctOutputGigawords - Number of times the AcctOutputOctets counter overflowed
# @li AcctInputMbyte - Input mbytes
# @li AcctOutputMbyte - Output mbytes
# @li LastAcctUpdate - Last time this record was updated
# @li ConnectTermReason - Connection termination reason
sub getRadiusUserLogs
{
	my (undef,$serverGroupID,$userInfo,$search) = @_;


	# Check params
	if (!defined($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' undefined");
	}
	if (!isHash($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' is not a HASH");
	}

	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userInfo->{'UserID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'UserID' undefined");
	}
	if (!defined($userInfo->{'UserID'} = isNumber($userInfo->{'UserID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'UserID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this radius user
		my $res = smlib::Radius::canAccessRadiusUser($callerAgentID,$userInfo->{'UserID'});
		# Check return code
		if ($res < 0) {
			return SOAPError($res,getError());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius user ID '".$userInfo->{'UserID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Get user info
	my $user = smlib::Radius::getRadiusUser($serverGroupID,$userInfo->{'UserID'});
	if (ref($user) ne "HASH") {
		return SOAPError($user,getError());
	}

	# Send only the info we need
	my $i;
	$i->{'AgentID'} = $user->{'AgentID'};
	$i->{'Username'} = $user->{'Username'};

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Radius::getRadiusUserLogs($serverGroupID,$i,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Username','string');
	$response->addField('Status','int');
	$response->addField('Timestamp','string');

	$response->addField('AcctSessionID','string');
	$response->addField('AcctSessionTime','string');

	$response->addField('NASIPAddress','string');
	$response->addField('NASPortType','string');
	$response->addField('NASPort','string');
	$response->addField('CalledStationID','string');
	$response->addField('CallingStationID','string');

	$response->addField('NASTransmitRate','string');
	$response->addField('NASReceiveRate','string');

	$response->addField('FramedIPAddress','string');

	$response->addField('AcctInputOctets','string');
	$response->addField('AcctOutputOctets','string');
	$response->addField('AcctInputGigawords','string');
	$response->addField('AcctOutputGigawords','string');

	$response->addField('AcctInputMbyte','string');
	$response->addField('AcctOutputMbyte','string');

	$response->addField('LastAcctUpdate','string');

	$response->addField('ConnectTermReason','string');

	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}


## @method getRadiusUserLogsSummary($serverGroupID,$logsInfo)
# Return radius user logs summary
#
# @param serverGroupID Server group ID
#
# @param logsInfo Logs info hash ref
# @li UserID - Radius user ID
# @li Period - User logs period
#
# @return Array ref of hash refs
# @li UsageTotal - Total usage for Period
# @li LastFramedIPAddress - Last IP if session is still open
sub getRadiusUserLogsSummary
{
	my (undef,$serverGroupID,$logsInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}
	if (!defined($logsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'logsInfo' undefined");
	}
	if (!isHash($logsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'logsInfo' is not a HASH");
	}
	if (!defined($logsInfo->{'UserID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'logsInfo' element 'UserID' undefined");
	}
	if (!defined($logsInfo->{'UserID'} = isNumber($logsInfo->{'UserID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'logsInfo' element 'UserID' is invalid");
	}
	# Set year and periodMonth from period
	if (defined($logsInfo->{'Period'})) {
		my ($periodYear,$periodMonth) = isDate($logsInfo->{'Period'},ISDATE_YEAR | ISDATE_MONTH);
		# If periodYear is not defined, then we failed
		if (!defined($periodYear)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'logsInfo' element 'Period' is invalid");
		}
		$logsInfo->{'Period'} = "$periodYear-$periodMonth";
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this radius user
		my $res = smlib::Radius::canAccessRadiusUser($callerAgentID,$logsInfo->{'UserID'});
		# Check return code
		if ($res < 0) {
			return SOAPError($res,getError());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius user ID '".$logsInfo->{'UserID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Get user info
	my $user = smlib::Radius::getRadiusUser($serverGroupID,$logsInfo->{'UserID'});
	if (ref($user) ne "HASH") {
		return SOAPError($user,getError());
	}

	# Send only the info we need
	my $i;
	$i->{'Username'} = $user->{'Username'};
	$i->{'Period'} = $logsInfo->{'Period'};

	# Grab data
	my ($rawLogSummaryData,$numResults) = smlib::Radius::getRadiusUserLogsSummary($serverGroupID,$i);
	if (ref($rawLogSummaryData) ne "HASH") {
		return SOAPError($rawLogSummaryData,getError());
	}

	my $result;
	$result->{'UsageTotal'} = $rawLogSummaryData->{'UsageTotal'};
	$result->{'LastFramedIPAddress'} = $rawLogSummaryData->{'LastFramedIPAddress'};

	# Build response
	my $response = smlib::soap::response->new();
	$response->addField('UsageTotal','string');
	$response->addField('LastFramedIPAddress','string');
	$response->parseHashRef($result);

	return $response->export();
}


## @method getRadiusServerGroups($serverGroupInfo,$search)
# Return radius server groups
#
# @param serverGroupInfo Class info hash ref. This hash must be defined even if not used.
# @li AgentID - Optional AgentID to limit results to. If not defined, results are limited to the current 
# caller privileges
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Server group ID
# @li Description - Server description
sub getRadiusServerGroups
{
	my (undef,$serverGroupInfo,$search) = @_;


	# Check if we have serverGroupinfo
	if (defined($serverGroupInfo)) {
		# Make sure we're a hash
		if (!isHash($serverGroupInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($serverGroupInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($serverGroupInfo->{'AgentID'} = isNumber($serverGroupInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $serverGroupData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($serverGroupInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($serverGroupInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'AgentID' has an invalid value");
			}

			$serverGroupData->{'AgentID'} = $serverGroupInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we don't have an agent ID, use the current one
		if (!defined($serverGroupInfo->{'AgentID'})) {
			$serverGroupInfo->{'AgentID'} = $callerAgentID;
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$serverGroupInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		$serverGroupData->{'AgentID'} = $serverGroupInfo->{'AgentID'};

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}
	
	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Radius::getRadiusServerGroups($serverGroupData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('Description','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}


## @internal
# @fn Radius_Auth($username,$password)
# Try authenticate an admin
sub Radius_Auth
{
	my ($username,$password) = @_;


	# Pull off serverGroupID
	my ($serverGroupID,$sUsername) = ($username =~ /^([0-9]+)\/(.*)$/);
	if (!defined($serverGroupID)) {
		return RES_ERROR;
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return RES_ERROR;
	}
	if (!defined($sUsername = isUsername($sUsername,ISUSERNAME_ALLOW_ATSIGN))) {
		return RES_ERROR;
	}

	# Only include the info we need
	my $i;
	$i->{'Username'} = $sUsername;
	$i->{'Password'} = $password;
	# Attempt authentication
	my $uid = smlib::Radius::Radius_Auth($serverGroupID,$i);
	if ($uid < 1) {
		return $uid;
	}

	# Setup return
	my $res;
	$res->{'Username'} = $username;
	$res->{'UserID'} = undef;
	$res->{'APICapabilities'} = [ 'Radius/Authenticate' ];
	$res->{'UserAttributes'} = { 'RadiusUserID' => $uid, 'Authority' => 'Radius' };

	return $res;
}




1;
# vim: ts=4
