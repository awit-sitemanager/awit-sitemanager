# SOAP backend RPC interface
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class BackendRPC
# Test functions for soap implementation
package BackendRPC;

use strict;
use warnings;

use Auth;

use smlib::constants;
use smlib::servers;
use smlib::soap;
use smlib::logging;
use smlib::util;


use Storable qw( thaw freeze );
use MIME::Base64 qw( decode_base64 encode_base64 );


# Plugin info
our $pluginInfo = {
	Name => "SOAP Bakend RPC",
	Init => \&init,
};


# Admin authentication
our $authInfo = {
	Name => "Backend RPC Authentication",
	Type => "BackendRPC",
	Authenticate => \&BackendRPC_Auth,
};


# Our config
my %config;


## @internal
# Initialize module
sub init 
{
	my $server = shift;
	my %ini;


	# Parse in config
	if (defined($server->{'inifile'}->{'backendrpc'})) {
		foreach my $key (keys %{$server->{'inifile'}->{'backendrpc'}}) {
			$ini{$key} = $server->{'inifile'}->{'backendrpc'}->{$key};
		}
	}

	# Check if we have a username and password
	if (!defined($ini{'username'}) || !defined($ini{'password'}) || $ini{'username'} eq "" || 
			$ini{'password'} eq ""
	) {
		$server->log(LOG_ERR,"  => BackendRPC: Disabling, no username or password configured");
		return RES_ERROR;
	}
	$config{'Username'} = $ini{'username'};
	$config{'Password'} = $ini{'password'};

	# Check if we have a access list
	if (!defined($ini{'allowfunctions'})) {
		$server->log(LOG_ERR,"  => BackendRPC: Disabling, AllowFunctions not set");
		return RES_ERROR;
	}
	# Split off modules
	if (ref($ini{'allowfunctions'}) eq "ARRAY") {
		foreach my $function (@{$ini{'allowfunctions'}}) {
			$function =~ s/\s+//g;
			# Skip comments
			next if ($function =~ /^#/);
			$config{'allowed_functions'}{$function} = 1;
		}
	} else {
		my @functionList = split(/\s+/,$ini{'allowfunctions'});
		foreach my $function (@functionList) {
			# Skip comments
			next if ($function =~ /^#/);
			$config{'allowed_functions'}{$function} = 1;
		}
	}

	$server->log(LOG_INFO,"  => BackendRPC: Library load started...");

	# Split off modules
	my @libraryList;
	# Check if we have a config variable
	if (defined($ini{'libraries'})) {
		if (ref($ini{'libraries'}) eq "ARRAY") {
			foreach my $library (@{$ini{'libraries'}}) {
				$library =~ s/\s+//g;
				push(@libraryList,$library);
			}
		} else {
			my @tmpLibraryList = split(/\s+/,$ini{'libraries'});
			foreach my $library (@tmpLibraryList) {
				# Skip comments
				next if ($library =~ /^#/);
				push(@libraryList,$library);
			}
		}
	} else {
		$server->log(LOG_WARN,"  => BackendRPC: No libraries to load, odd??");
	}

	# Load libraries
	foreach my $library (@libraryList) {
		# Load plugin
		my $res = eval("
			use smlib::$library;
			if (smlib::${library}->can('_init')) {
				smlib::${library}::_init(\$server);
			}
		");
		if (!$@) {
			if (defined($res) && $res != RES_OK) {
				$server->log(LOG_WARN,"  => BackendRPC: Library '$library' NOT LOADED");
			} else {
				$server->log(LOG_INFO,"  => BackendRPC: Library '$library' loaded");
			}
		} else {
			$server->log(LOG_WARN,"  => BackendRPC: Error loading plugin $library ($@)");
		}
	}

	$server->log(LOG_INFO,"  => BackendRPC: Library load done");


	Auth::aclAdd('BackendRPC','remoteCall','SOAP/BackendRPC');

	# Add authentication plugin
	Auth::pluginAdd($authInfo);

	return RES_OK;
}


## @method remoteCall($subroutine,$frozenParams,$frozenAttributes)
# Function which accepts remote function calls
#
# @param subroutine Subroutine to call
# @param frozenParams Frozen, base64'd params
# @param frozenAttributes Frozen backend attributes
#
# @return Result of the called function
sub remoteCall
{
	my (undef,$subroutine,$frozenParams,$frozenAttributes) = @_;


	# Check we are allowed to access this function first of all
	if (!defined($config{'allowed_functions'}{$subroutine})) {
		smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', not in allowed function list");
		return SOAPError(ERR_UNKNOWN,"Backend server rejected RPC call, access denied");
	}

	# Split off the components of the subroutine
	my @components = split(/::/,$subroutine);
	if (@components < 3) {
		smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', incorrect number of components");
		return SOAPError(ERR_UNKNOWN,"Backend server rejected RPC call");
	}

	# Rip off smlib::
	if (shift(@components) ne "smlib") {
		smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', base class is not smlib");
		return SOAPError(ERR_UNKNOWN,"Backend server rejected RPC call");
	}

	# Get function name
	my $function = pop(@components);
	# Check first char of the function that it is _
	if (!($function =~ /^_[a-zA-Z]+$/)) {
		smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', invalid characters in function name");
		return SOAPError(ERR_UNKNOWN,"Backend server rejected RPC call");
	}

	# Loop with components and check they contain only valid characters, beginning with a _
	foreach my $component (@components) {
		if (!($component =~ /^[a-zA-Z]+$/)) {
			smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', component(s) contain invalid characters");
			return SOAPError(ERR_UNKNOWN,"Backend server rejected RPC call");
		}
	}
	# One last check, all module start with an uppercase
	my $moduleName = $components[0];
	if (!defined($moduleName) || !($moduleName =~ /^[A-Z]/)) {
		smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', module name contains invalid characters");
		return SOAPError(ERR_UNKNOWN,"Backend server rejected RPC call");
	}

	# Grab module
	my $module = "smlib::".join("::",@components);

	# Check if its been loaded
	eval("defined(%${module}::) || die;");
	if ($@) {
		smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', library '$module' not loaded");
		return SOAPError(ERR_UNKNOWN,"Backend server library not present");
	}

	# Make sure we don't go for a remote call again
	smlib::servers::setLocalCallsOnly();

	# Thaw and decode params
	my $params = decode_base64($frozenParams);
	if (!defined($params)) {
		smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', decode_base64() failed on frozen parameters");
		return SOAPError(ERR_UNKNOWN,"Backend server failed to decode the request parameters");
	}
	# Eval thaw so we can catch the die() if it occurs
	eval { $params = thaw($params); };
	if ($@) {
		smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', thaw failed on frozen parameters: $@");
		return SOAPError(ERR_UNKNOWN,"Backend server received invalid data in the request parameters");
	}

	# Decode the frozen attributes
	$frozenAttributes = decode_base64($frozenAttributes);
	if (!defined($frozenAttributes)) {
		smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', decode_base64() failed on frozen attributes");
		return SOAPError(ERR_UNKNOWN,"Backend server failed to decode the request attributes");
	}
	# Now we going to thaw them out
	# Eval thaw so we can catch the die() if it occurs
	my $attributes;
	eval { $attributes = thaw($frozenAttributes); };
	if ($@) {
		smlib::logging::log(LOG_ERR,"Rejected call to '$subroutine', thaw failed on frozen attributes: $@");
		return SOAPError(ERR_UNKNOWN,"Backend server received invalid data in the request attributes");
	}
	# Setup attributes
	smlib::servers::setAttributes($attributes);

	# Try local call
	smlib::logging::log(LOG_NOTICE,"Call to '$subroutine'");
	my (@res, $errorCode, $error);
	eval("
		# Do call
		\@res = ${module}::${function}(\@{\$params});

		# If its 1 param
		if (\@res == 1) {

			# It should be a result code
			if (defined(my \$res0 = isNumber(\$res[0],ISNUMBER_ALLOW_NEGATIVE))) {

				# If its negative its probably a problem?
				if (\$res0 < 0) {
					\$errorCode = \$res0;
					\$error = getError();
				}
			}

		}
	");
	if ($@) {
		smlib::logging::log(LOG_ERR,"BackendRPC eval failure for '$function': $@");
		return SOAPError(ERR_UNKNOWN,"Backend server code execution failure");
	}

	if ($errorCode)  {
		smlib::logging::log(LOG_ERR,"BackendRPC failure for '$function': errorCode = $errorCode, error = '$error'");
		return SOAPError($errorCode,$error);
	}

	# Freeze results, base64 it into text
	my $ret = encode_base64( freeze(\@res) );

	# And return as string
	return SOAPSuccess($ret,'string','B64Storable');
}


## @internal
# @fn BackendRPC_Auth($username,$password)
# Try authenticate an admin
sub BackendRPC_Auth
{
	my ($username,$password) = @_;


	# Authenticate
	if ($config{'Username'} ne $username || $config{'Password'} ne $password) {
		return RES_ERROR;
	}

	# Get attributes

	# Setup return
	my $res;
	$res->{'Username'} = $username;
#	$res->{'UserID'} = $uid;
	$res->{'APICapabilities'} = [ 'SOAP/BackendRPC' ];
#	$res->{'UserAttributes'} = $userAttributes;

	return $res;
}







1;
# vim: ts=4
