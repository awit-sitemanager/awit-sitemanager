# Agent functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class Agent
# This is the agent class
package Agent;


use strict;

use Auth;

use smlib::Agent;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Agent",
	Init => \&init,
};


# Agent authentication
our $authInfo = {
	Name => "Agent Authentication",
	Type => "Agent",
	Authenticate => \&Agent_Auth,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Agent','ping','Agent/Test');
	
	Auth::aclAdd('Agent','authenticate','Agent/Authenticate');

	Auth::aclAdd('Agent','createAgent','Agent/Add');
	Auth::aclAdd('Agent','removeAgent','Agent/Remove');
	Auth::aclAdd('Agent','updateAgent','Agent/Update');
	Auth::aclAdd('Agent','getAgents','Agent/List');
	Auth::aclAdd('Agent','getAgent','Agent/Get');
	Auth::aclAdd('Agent','getAgentQuotaUsage','Agent/Get');

	# Add authentication plugin
	Auth::pluginAdd($authInfo);

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method authenticate
# Dummy function to allow admin authentication
#
# @return Will always return 0
sub authenticate
{
	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Agent')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	return SOAPSuccess(Auth::userGetAPICapabilities(),'array','Capabilities');
}



## @method createAgent($agentInfo)
# Create an agent
#
# @param agentInfo Agent info hash
# @li Name %Agent Name
# @li ContactPerson Contact person
# @li ContactTel1 Contact number 1
# @li ContactTel2 Optional contact number 2
# @li ContactEmail Contact email address
# @li SiteQuota Website allocation quota
# @li MailboxQuota %Mailbox allocation quota
# @li MinSiteSize Minimum site size
# @li MinMailboxSize Minimum mailbox size
#
# @return Agent ID
sub createAgent
{
	my (undef,$agentInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($agentInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' not defined");
	}
	if (!isHash($agentInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' is not a HASH");
	}

	if (!defined($agentInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' has no element 'Name' defined");
	}
	if (!isVariable($agentInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'Name' is invalid");
	}
	if ($agentInfo->{'Name'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'Name' is blank");
	}

	if (!defined($agentInfo->{'ContactPerson'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' has no element 'ContactPerson' defined");
	}
	if (!isVariable($agentInfo->{'ContactPerson'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactPerson' is invalid");
	}
	if ($agentInfo->{'ContactPerson'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactPerson' is blank");
	}

	if (!defined($agentInfo->{'ContactTel1'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' has no element 'ContactTel1' defined");
	}
	if (!isVariable($agentInfo->{'ContactTel1'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactTel1' is invalid");
	}
	if ($agentInfo->{'ContactTel1'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactTel1' is blank");
	}

	if (defined($agentInfo->{'ContactTel2'})) {
		if (!isVariable($agentInfo->{'ContactTel2'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactTel2' is invalid");
		}
		# If its blank just delete it
		if ($agentInfo->{'ContactTel2'} eq "") {
			delete($agentInfo->{'ContactTel2'});
		}
	}

	if (!defined($agentInfo->{'ContactEmail'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' has no element 'ContactEmail' defined");
	}
	if (!isVariable($agentInfo->{'ContactEmail'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactEmail' is invalid");
	}
	if ($agentInfo->{'ContactEmail'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactEmail' is blank");
	}

	if (!defined($agentInfo->{'SiteQuota'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' has no element 'SiteQuota' defined");
	}
	if (!defined($agentInfo->{'SiteQuota'} = isNumber($agentInfo->{'SiteQuota'},ISNUMBER_ALLOW_ZERO))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'SiteQuota' must be > 0");
	}
	if (!defined($agentInfo->{'MailboxQuota'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' has no element 'MailboxQuota' defined");
	}
	if (!defined($agentInfo->{'MailboxQuota'} = isNumber($agentInfo->{'MailboxQuota'},ISNUMBER_ALLOW_ZERO))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'MailboxQuota' must be > 0");
	}
	if (!defined($agentInfo->{'MinSiteSize'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' has no element 'MinSiteSize' defined");
	}
	if (!defined($agentInfo->{'MinSiteSize'} = isNumber($agentInfo->{'MinSiteSize'},ISNUMBER_ALLOW_ZERO))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'MinSiteSize' must be > 0");
	}
	if (!defined($agentInfo->{'MinMailboxSize'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' has no element 'MinMailboxSize' defined");
	}
	if (!defined($agentInfo->{'MinMailboxSize'} = isNumber($agentInfo->{'MinMailboxSize'},ISNUMBER_ALLOW_ZERO))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'MinMailboxSize' must be > 0");
	}

	# Build result
	my $agentData;
	$agentData->{'Name'} = $agentInfo->{'Name'};
	$agentData->{'ContactPerson'} = $agentInfo->{'ContactPerson'};
	$agentData->{'ContactTel1'} = $agentInfo->{'ContactTel1'};
	$agentData->{'ContactTel2'} = $agentInfo->{'ContactTel2'} if (defined($agentInfo->{'ContactTel2'}));
	$agentData->{'ContactEmail'} = $agentInfo->{'ContactEmail'};
	$agentData->{'SiteQuota'} = $agentInfo->{'SiteQuota'};
	$agentData->{'MailboxQuota'} = $agentInfo->{'MailboxQuota'};
	$agentData->{'MinSiteSize'} = $agentInfo->{'MinSiteSize'};
	$agentData->{'MinMailboxSize'} = $agentInfo->{'MinMailboxSize'};

	my $res = smlib::Agent::createAgent($agentData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','AgentID');
}



## @method updateAgent($agentInfo)
# Update agent
#
# @param agentInfo %Agent info hash ref
# @li ID %Agent ID
# @li Name - Optional name
# @li ContactPerson - Optional contact person
# @li ContactTel1 - Optional contact person telephone 1
# @li ContactTel2 - Optional contact person telephone 2
# @li ContactEmail - Optional contact person email address
# @li SiteQuota - Optional site allocation quota
# @li MailboxQuota - Optional mailbox allocation quota
# @li MinSiteSize - Optional minimum site size
# @li MinMailboxSize - Optional minimum mailbox size
#
# @return 0 on success, < 0 on error
sub updateAgent
{
	my (undef,$agentInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($agentInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' undefined");
	}
	if (!isHash($agentInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' is not a HASH");
	}

	if (!defined($agentInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' has no element 'ID'");
	}
	if (!defined($agentInfo->{'ID'} = isNumber($agentInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ID' must be > 0");
	}

	if (defined($agentInfo->{'ContactPerson'})) {
		if (!isVariable($agentInfo->{'ContactPerson'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactPerson' is invalid");
		}
		if ($agentInfo->{'ContactPerson'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactPerson' is blank");
		}
	}

	if (defined($agentInfo->{'ContactTel1'})) {
		if (!isVariable($agentInfo->{'ContactTel1'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactTel1' is invalid");
		}
		if ($agentInfo->{'ContactTel1'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactTel1' is blank");
		}
	}

	if (defined($agentInfo->{'ContactTel2'})) {
		if (!isVariable($agentInfo->{'ContactTel2'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactTel2' is invalid");
		}
		if ($agentInfo->{'ContactTel2'} eq "") {
			delete($agentInfo->{'ContactTel1'});
		}
	}

	if (defined($agentInfo->{'ContactEmail'})) {
		if (!isVariable($agentInfo->{'ContactEmail'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactEmail' is invalid");
		}
		if ($agentInfo->{'ContactEmail'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'ContactEmail' is blank");
		}
	}

	if (defined($agentInfo->{'SiteQuota'}) && !defined($agentInfo->{"SiteQuota"} = isNumber($agentInfo->{"SiteQuota"},ISNUMBER_ALLOW_ZERO))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'SiteQuota' must be > 0");
	}
	if (defined($agentInfo->{'MailboxQuota'}) && !defined($agentInfo->{"MailboxQuota"} = isNumber($agentInfo->{"MailboxQuota"},ISNUMBER_ALLOW_ZERO))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'MailboxQuota' must be > 0");
	}
	if (defined($agentInfo->{'MinSiteSize'}) && !defined($agentInfo->{"MinSiteSize"} = isNumber($agentInfo->{"MinSiteSize"}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'MinSiteSize' must be > 0");
	}
	if (defined($agentInfo->{'MinMailboxSize'}) && !defined($agentInfo->{"MinMailboxSize"} = isNumber($agentInfo->{"MinMailboxSize"}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'MinMailboxSize' must be > 0");
	}

	my $i;

	$i->{'ID'} = $agentInfo->{'ID'};
	$i->{'Name'} = $agentInfo->{'Name'};
	$i->{'ContactPerson'} = $agentInfo->{'ContactPerson'} if (defined($agentInfo->{'ContactPerson'}));
	$i->{'ContactTel1'} = $agentInfo->{'ContactTel1'} if (defined($agentInfo->{'ContactTel1'}));
	$i->{'ContactTel2'} = $agentInfo->{'ContactTel2'} if (defined($agentInfo->{'ContactTel2'}));
	$i->{'ContactEmail'} = $agentInfo->{'ContactEmail'} if (defined($agentInfo->{'ContactEmail'}));
	$i->{'SiteQuota'} = $agentInfo->{'SiteQuota'} if (defined($agentInfo->{'SiteQuota'}));
	$i->{'MailboxQuota'} = $agentInfo->{'MailboxQuota'} if (defined($agentInfo->{'MailboxQuota'}));
	$i->{'MinSiteSize'} = $agentInfo->{'MinSiteSize'} if (defined($agentInfo->{'MinSiteSize'}));
	$i->{'MinMailboxSize'} = $agentInfo->{'MinMailboxSize'} if (defined($agentInfo->{'MinMailboxSize'}));

	my $res = smlib::Agent::updateAgent($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}


## @method removeAgent($agentID)
# Remove agent
#
# @param agentID %Agent ID
#
# @return 0 on success or < 0 on error
sub removeAgent
{
	my (undef,$agentID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($agentID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentID' undefined");
	}
	if (!defined($agentID = isNumber($agentID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentID' must be > 0");
	}

	# FIXME: privs?
	my $res = smlib::Agent::removeAgent($agentID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}


## @method getAgents($agentInfo,$search)
# Return list of agents
#
# @param agentInfo Agent info hash ref
# @li AgentID - Limit returned results to this agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by agent ID with elements...
# @li Name
sub getAgents
{
	my (undef,$agentInfo,$search) = @_;


	# Check if we have agentinfo
	if (defined($agentInfo)) {
		# Make sure we're a hash
		if (!isHash($agentInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($agentInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($agentInfo->{'AgentID'} = isNumber($agentInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $agentData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($agentInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($agentInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'agentInfo' element 'AgentID' has an invalid value");
			}
		
			$agentData->{'AgentID'} = $agentInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we don't have an agent ID, use the current one
		if (!defined($agentInfo->{'AgentID'})) {
			$agentInfo->{'AgentID'} = $callerAgentID;
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$agentInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}
	
		$agentData->{'AgentID'} = $agentInfo->{'AgentID'};

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Agent::getAgents($agentData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getAgent($agentID)
# Return agent info hash
#
# @param agentID Agent ID
#
# @return Agent info hash ref
# @li ID %Agent ID
# @li Name $Agent Name
# @li ContactPerson Contact person
# @li ContactTel1 Contact number 1
# @li ContactTel2 Contact number 2
# @li ContactEmail Contact email address
# @li SiteQuota Website allocation quota
# @li MailboxQuota %Mailbox allocation quota
# @li MinSiteSize Minimum site size
# @li MinMailboxSize Minimum mailbox size
sub getAgent
{
	my (undef,$agentID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($agentID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentID' undefined");
	}
	if (!defined($agentID = isNumber($agentID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentID' must be > 0");
	}

	# Grab and sanitize data
	my $rawData = smlib::Agent::getAgent($agentID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('ContactPerson','string');
	$response->addField('ContactTel1','string');
	$response->addField('ContactTel2','string');
	$response->addField('ContactEmail','string');
	$response->addField('SiteQuota','int');
	$response->addField('MailboxQuota','int');
	$response->addField('MinSiteSize','int');
	$response->addField('MinMailboxSize','int');
	$response->parseHashRef($rawData);

	return $response->export();
}


## @method getAgentQuotaBreakdown($agentID)
# Get agent quota breakdown
#
# @return hash ref
# @li SiteQuotaAllocated - Amount of site quota allocated
# @li SiteQuotaUsage - Amount of site quota used
# @li MailQuotaAllocated - Amount of mailbox quota allocated
#
# @return 0 on success, < 0 on error
sub getAgentQuotaUsage
{
	my (undef,$agentID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($agentID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentID' undefined");
	}
	if (!defined($agentID = isNumber($agentID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'agentID' must be > 0");
	}

	# Grab and sanitize data
	my $rawData = smlib::Agent::getAgentQuotaUsage($agentID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('SiteQuotaAllocated','int');
	$response->addField('SiteQuotaUsage','int');
	$response->addField('MailQuotaAllocated','int');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @internal
# @fn Agent_Auth($username,$password)
# Try authenticate an agent
sub Agent_Auth
{
	my ($username,$password) = @_;


	# Authenticate
	my $uid = smlib::users::authenticate($username,$password);
	if ($uid < 1) {
		return $uid;
	}

	# Get attributes
	my $userAttributes = smlib::users::getUserAttributes($uid);

	# Check we're a agent
	if (!defined($userAttributes->{'Authority'}) || $userAttributes->{'Authority'} ne "Agent") {
		return RES_ERROR;
	}
	if (!defined($userAttributes->{'AgentID'}) || int($userAttributes->{'AgentID'}) < 1) {
		return RES_ERROR;
	}

	my $apiCapabilities = smlib::users::getAPICapabilities($uid);

	# Setup return
	my $res;
	$res->{'Username'} = $username;
	$res->{'UserID'} = $uid;
	$res->{'APICapabilities'} = $apiCapabilities;
	$res->{'UserAttributes'} = $userAttributes;

	return $res;
}






1;
# vim: ts=4
