# WebHosting SOAP interface
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class WebHosting
# This is the main agent class, allowing control over agent services
package WebHosting;

use strict;

use Auth;

use smlib::WebHosting;

use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "WebHosting",
	Init => \&init,
};


## @internal
# Initialize module
sub init {
	my $server = shift;

	Auth::aclAdd('WebHosting','ping','WebHosting/Test');

	Auth::aclAdd('WebHosting','createWebsite','WebHosting/Websites/Add');
	Auth::aclAdd('WebHosting','updateWebsite','WebHosting/Websites/Update');
	Auth::aclAdd('WebHosting','removeWebsite','WebHosting/Websites/Remove');
	Auth::aclAdd('WebHosting','getWebsites','WebHosting/Websites/List');
	Auth::aclAdd('WebHosting','getWebsite','WebHosting/Websites/Get');

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method createWebsite($websiteInfo)
# Create a website
#
# @param websiteInfo
# @li DomainName - Domain name
# @li DomainAliases - Optional, whitespace separated domain alias list
# @li DiskQuota - Disk quota for website
# @li AgentDisabled - Optional, flag to indicate agent has disabled this site (NOT IMPLEMENTED
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
#
# @return Website ID
sub createWebsite
{
	my (undef,$websiteInfo) = @_;


	my $websiteData;

	# Check params
	if (!defined($websiteInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' not defined");
	}
	if (!isHash($websiteInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' is not a HASH");
	}
	if (!defined($websiteInfo->{"DomainName"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' has no element 'DomainName'");
	}
	if (!defined($websiteInfo->{'DomainName'} = isDomain($websiteInfo->{'DomainName'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' element 'DomainName' is invalid");
	}
	my $domainAliases;
	if (defined($websiteInfo->{'DomainAliases'})) {
		my @aliasArray = split(/\s+/,$websiteInfo->{'DomainAliases'});
		$websiteInfo->{'DomainAliases'} = \@aliasArray;

		# Chop off blank aliases
		foreach my $alias (@{$websiteInfo->{'DomainAliases'}}) {
			# Verify
			if (!defined($alias = isDomain($alias))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' element 'DomainName' is invalid");
			}
			push(@{$domainAliases},$alias);
		}
	}
	if (!defined($websiteInfo->{"DiskQuota"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' has no element 'DiskQuota'");
	}
	if (!defined($websiteInfo->{'DiskQuota'} = isNumber($websiteInfo->{'DiskQuota'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' element 'DiskQuota' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Check we have website info
		if (defined($websiteInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($websiteData->{'AgentID'} = isNumber($websiteInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' element 'AgentID' is invalid");
			}
		} else {
			return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' has no element 'AgentID', required when accessing this function as an Admin");
		}
	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Make sure we have an agent ID and its valid
		if (!defined($websiteData->{'AgentID'} = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}
	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Build request
	$websiteData->{'DomainName'} = $websiteInfo->{'DomainName'};
	$websiteData->{'DomainAliases'} = $domainAliases;
	$websiteData->{'DiskQuota'} = $websiteInfo->{'DiskQuota'};
	$websiteData->{'AgentDisabled'} = booleanize($websiteInfo->{'AgentDisabled'}) if (defined($websiteInfo->{'AgentDisabled'}));
	$websiteData->{'AgentRef'} = $websiteInfo->{'AgentRef'};

	my $res = smlib::WebHosting::createWebsite($websiteData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','WebsiteID');
}



## @method updateWebsite($websiteInfo)
# Update a website
#
# @param websiteInfo Hash table
# @li ID - Website ID
# @li DomainAliases - Optional, whitespace separated domain alias list
# @li DiskQuota - Optional, disk quota for website
# @li AgentDisabled - Optional, if this item has been disabled by the agent (NOT IMPLEMENTED)
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
#
# @return 0 on success or < 0 on error
sub updateWebsite
{
	my (undef,$websiteInfo) = @_;


	# Check params
	if (!defined($websiteInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' not defined");
	}
	if (!isHash($websiteInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' is not a HASH");
	}
	if (!defined($websiteInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' has no element 'ID'");
	}
	if (!defined($websiteInfo->{'ID'} = isNumber($websiteInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' element 'ID' is invalid");
	}
	my $domainAliases;
	if (defined($websiteInfo->{'DomainAliases'})) {
		my @aliasArray = split(/\s+/,$websiteInfo->{'DomainAliases'});

		$websiteInfo->{'DomainAliases'} = \@aliasArray;

		# Chop off blank aliases
		foreach my $alias (@{$websiteInfo->{'DomainAliases'}}) {
			# Verify
			if (!defined($alias = isDomain($alias))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' element 'DomainName' is invalid");
			}
			push(@{$domainAliases},$alias);
		}
	}
	if (defined($websiteInfo->{"DiskQuota"})) {
		if (!defined($websiteInfo->{'DiskQuota'} = isNumber($websiteInfo->{'DiskQuota'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' element 'DiskQuota' is invalid");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this website
		my $res = smlib::WebHosting::canAccessWebsite($agentID,$websiteInfo->{'ID'});
		# Check return code
		if ($res < 0) {
			return SOAPError($res,getError());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access website ID '".$websiteInfo->{'ID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;
	$i->{'ID'} = $websiteInfo->{'ID'};
	$i->{'DomainAliases'} = $domainAliases if (defined($domainAliases));
	$i->{'DiskQuota'} = $websiteInfo->{'DiskQuota'} if (defined($websiteInfo->{'DiskQuota'}));
	$i->{'AgentDisabled'} = booleanize($websiteInfo->{'AgentDisabled'}) if (defined($websiteInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $websiteInfo->{'AgentRef'} if (defined($websiteInfo->{'AgentRef'}));

	# Do the update
	my $res = smlib::WebHosting::updateWebsite($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeWebsite($websiteID)
# Remove website
#
# @param websiteID Mail website ID
#
# @return 0 on success or < 0 on error
sub removeWebsite
{
	my (undef,$websiteID) = @_;


	# Check params
	if (!defined($websiteID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteID' undefined");
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {


	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this website
		my $res = smlib::WebHosting::canAccessWebsite($agentID,$websiteID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,getError());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access website ID '$websiteID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $res = smlib::WebHosting::removeWebsite($websiteID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method getWebsites($websiteInfo,$search)
# Return a list of all the website an agent has
#
# @param websiteInfo Hash with what we must get, this is ignored in the case of agents
# @li AgentID - Limit returned results to this agent ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Website ID on the system
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li DomainName - Domain name of website
# @li DomainAliases - Array ref of domain aliases of website
# @li AgentDisabled - Set if the agent has disabled this website (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getWebsites
{
	my (undef,$websiteInfo,$search) = @_;


	my $i;

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# We normally only have websiteInfo if we an admin
		if (defined($websiteInfo)) {
			# Make sure we're a hash
			if (!isHash($websiteInfo)) {
				return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' is not a HASH");
			}
			# If we have an agent ID, use it
			if (defined($websiteInfo->{'AgentID'})) {
				# If we do check if we have AgentID
				if (!defined($i->{'AgentID'} = isNumber($websiteInfo->{'AgentID'}))) {
					return SOAPError(ERR_S_PARAM,"Parameter 'websiteInfo' element 'AgentID' is invalid");
				}
			}
		}
	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Make sure we have an agent ID and its valid
		if (!defined($i->{'AgentID'} = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}
	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::WebHosting::getWebsites($i,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Loop and fix
	foreach my $website (@{$rawData}) {
		if (defined($website->{'DomainAliases'})) {
			$website->{'DomainAliases'} = join("\n",@{$website->{'DomainAliases'}});
		}
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('AgentID','int');
	$response->addField('AgentName','string');
	$response->addField('DomainName','string');
	$response->addField('DomainAliases','string');
	$response->addField('DiskQuota','int');
	$response->addField('DiskUsage','int');
#	$response->addField('AgentDisabled','boolean');
#	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getWebsite($websiteID)
# Return a hash containing the info for this website
#
# @param websiteID Website ID
#
# @return Hash ref containing website info
# @li ID - Website ID on the system
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li DomainName - Domain name of website
# @li DomainAliases - Array ref of domain aliases of website
# @li AgentDisabled - Set if the agent has disabled this website (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getWebsite
{
	my (undef,$websiteID) = @_;


	# Check params
	if (!defined($websiteID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteID' undefined");
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# FIXME: Admin has no restrictions

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this website
		my $res = smlib::WebHosting::canAccessWebsite($agentID,$websiteID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,getError());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access website ID '$websiteID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::WebHosting::getWebsite($websiteID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	if (defined($rawData->{'DomainAliases'})) {
		$rawData->{'DomainAliases'} = join("\n",@{$rawData->{'DomainAliases'}});
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('AgentID','int');
	$response->addField('AgentName','string');
	$response->addField('DomainName','string');
	$response->addField('DomainAliases','string');
	$response->addField('DiskQuota','int');
	$response->addField('DiskUsage','int');
#	$response->addField('AgentDisabled','boolean');
#	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);

	return $response->export();
}






1;
# vim: ts=4
