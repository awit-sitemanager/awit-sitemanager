# Client functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class Client
# This is the client class
package Client;


use strict;

use Auth;

use smlib::Agent;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Client",
	Init => \&init,
};


# Agent authentication
our $authInfo = {
	Name => "Client Authentication",
	Type => "Client",
	Authenticate => \&Client_Auth,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Client','ping','Client/Test');
	
	Auth::aclAdd('Client','authenticate','Client/Authenticate');

	# Add authentication plugin
	Auth::pluginAdd($authInfo);

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method authenticate
# Dummy function to allow admin authentication
#
# @return Will always return 0
sub authenticate
{
	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Client')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	return SOAPSuccess(Auth::userGetAPICapabilities(),'array','Capabilities');
}



## @internal
# @fn Client_Auth($username,$password)
# Try authenticate an client
sub Client_Auth
{
	my ($username,$password) = @_;


	# Authenticate
	my $uid = smlib::users::authenticate($username,$password);
	if ($uid < 1) {
		return $uid;
	}

	# Get attributes
	my $userAttributes = smlib::users::getUserAttributes($uid);

	# Check we're a client
	if (!defined($userAttributes->{'Authority'}) || $userAttributes->{'Authority'} ne "Client") {
		return RES_ERROR;
	}

	my $apiCapabilities = smlib::users::getAPICapabilities($uid);

	# Setup return
	my $res;
	$res->{'Username'} = $username;
	$res->{'UserID'} = $uid;
	$res->{'APICapabilities'} = $apiCapabilities;
	$res->{'UserAttributes'} = $userAttributes;

	return $res;
}






1;
# vim: ts=4
