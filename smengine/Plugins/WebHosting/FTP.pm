# WebHosting FTP SOAP interface
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class WebHosting::FTP
# This is the main agent class, allowing control over agent services
package WebHosting::FTP;

use strict;

use Auth;

use smlib::WebHosting;
use smlib::WebHosting::FTP;

use smlib::constants;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "WebHosting::FTP",
	Init => \&init,
};


## @internal
# Initialize module
sub init {
	my $server = shift;

	Auth::aclAdd('WebHosting::FTP','ping','WebHosting/FTP/Test');

	Auth::aclAdd('WebHosting::FTP','createFTPUser','WebHosting/FTPUsers/Add');
	Auth::aclAdd('WebHosting::FTP','updateFTPUser','WebHosting/FTPUsers/Update');
	Auth::aclAdd('WebHosting::FTP','removeFTPUser','WebHosting/FTPUsers/Remove');
	Auth::aclAdd('WebHosting::FTP','getFTPUsers','WebHosting/FTPUsers/List');
	Auth::aclAdd('WebHosting::FTP','getFTPUser','WebHosting/FTPUsers/Get');

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method createFTPUser($ftpUserInfo)
# Create a ftpUser
#
# @param ftpUserInfo FTP user info hash ref
# @li WebsiteID - Website ID
# @li Username - FTP username
# @li Password - FTP user password
# @li AgentDisabled - Optional, flag to indicate agent has disabled this site (NOT IMPLEMENTED
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
#
# @return FTPUser ID
sub createFTPUser
{
	my (undef,$ftpUserInfo) = @_;


	my $ftpUserData;

	# Check params
	if (!defined($ftpUserInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' not defined");
	}
	if (!isHash($ftpUserInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' is not a HASH");
	}
	if (!defined($ftpUserInfo->{'WebsiteID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' has no element 'WebsiteID'");
	}
	if (!defined($ftpUserInfo->{'WebsiteID'} = isNumber($ftpUserInfo->{'WebsiteID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' element 'WebsiteID' is invalid");
	}
	if (!defined($ftpUserInfo->{"Username"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' has no element 'Username'");
	}
	if (!defined($ftpUserInfo->{'Username'} = isUsername($ftpUserInfo->{'Username'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' element 'Username' is invalid");
	}
	if (!defined($ftpUserInfo->{"Password"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' has no element 'Password'");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# FIXME: Admin has no restrictions

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this website
		my $res = smlib::WebHosting::canAccessWebsite($agentID,$ftpUserInfo->{'WebsiteID'});
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::WebHosting::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access website ID '".$ftpUserInfo->{'WebsiteID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Build request
	$ftpUserData->{'WebsiteID'} = $ftpUserInfo->{'WebsiteID'};
	$ftpUserData->{'Username'} = $ftpUserInfo->{'Username'};
	$ftpUserData->{'Password'} = $ftpUserInfo->{'Password'};
	$ftpUserData->{'AgentDisabled'} = booleanize($ftpUserInfo->{'AgentDisabled'}) if (defined($ftpUserInfo->{'AgentDisabled'}));
	$ftpUserData->{'AgentRef'} = $ftpUserInfo->{'AgentRef'};

	my $res = smlib::WebHosting::FTP::createFTPUser($ftpUserData);
	if ($res < 0) {
		return SOAPError($res,smlib::WebHosting::FTP::Error());
	}

	return SOAPSuccess($res,'int','FTPUserID');
}



## @method updateFTPUser($ftpUserInfo)
# Update a ftpUser
#
# @param ftpUserInfo FTP user info hash ref
# @li ID - FTP user ID
# @li Password - FTP user password
# @li AgentDisabled - Optional, if this item has been disabled by the agent (NOT IMPLEMENTED)
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
#
# @return 0 on success or < 0 on error
sub updateFTPUser
{
	my (undef,$ftpUserInfo) = @_;


	# Check params
	if (!defined($ftpUserInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' not defined");
	}
	if (!isHash($ftpUserInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' is not a HASH");
	}
	if (!defined($ftpUserInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' has no element 'ID'");
	}
	if (!defined($ftpUserInfo->{'ID'} = isNumber($ftpUserInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserInfo' element 'ID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this ftpUser
		my $res = smlib::WebHosting::FTP::canAccessFTPUser($agentID,$ftpUserInfo->{'ID'});
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::WebHosting::FTP::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access ftpUser ID '".$ftpUserInfo->{'ID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;
	$i->{'ID'} = $ftpUserInfo->{'ID'};
	$i->{'Password'} = $ftpUserInfo->{'Password'};
	$i->{'AgentDisabled'} = booleanize($ftpUserInfo->{'AgentDisabled'}) if (defined($ftpUserInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $ftpUserInfo->{'AgentRef'} if (defined($ftpUserInfo->{'AgentRef'}));

	# Do the update
	my $res = smlib::WebHosting::FTP::updateFTPUser($i);
	if ($res < 0) {
		return SOAPError($res,smlib::WebHosting::FTP::Error());
	}

	return SOAPSuccess();
}



## @method removeFTPUser($ftpUserID)
# Remove ftpUser
#
# @param ftpUserID Mail ftpUser ID
#
# @return 0 on success or < 0 on error
sub removeFTPUser
{
	my (undef,$ftpUserID) = @_;


	# Check params
	if (!defined($ftpUserID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserID' undefined");
	}
	if (!defined($ftpUserID = isNumber($ftpUserID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {


	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this ftpUser
		my $res = smlib::WebHosting::FTP::canAccessFTPUser($agentID,$ftpUserID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::WebHosting::FTP::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access ftpUser ID '$ftpUserID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $res = smlib::WebHosting::FTP::removeFTPUser($ftpUserID);
	if ($res < 0) {
		return SOAPError($res,smlib::WebHosting::FTP::Error());
	}

	return SOAPSuccess();
}



## @method getFTPUsers($websiteID,$search)
# Return a list of all the ftpUser an agent has
#
# @param websiteID Website ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - FTPUser ID on the system
# @li Username - Domain name of ftpUser
# @li HomeDirectory - Home directory
# @li AgentDisabled - Set if the agent has disabled this ftpUser (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getFTPUsers
{
	my (undef,$websiteID,$search) = @_;


	# Check params
	if (!defined($websiteID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteID' undefined");
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# FIXME: Admin has no restrictions

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this website
		my $res = smlib::WebHosting::canAccessWebsite($agentID,$websiteID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::MailHosting::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access website ID '$websiteID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::WebHosting::FTP::getFTPUsers($websiteID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,smlib::WebHosting::FTP::Error());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Username','string');
	$response->addField('HomeDirectory','string');
#	$response->addField('AgentDisabled','boolean');
#	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getFTPUser($ftpUserID)
# Return a hash containing the info for this ftpUser
#
# @param ftpUserID FTPUser ID
#
# @return FTP user user hash ref
# @li ID - FTPUser ID on the system
# @li Username - Domain name of ftpUser
# @li HomeDirectory - Home directory
# @li AgentDisabled - Set if the agent has disabled this ftpUser (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getFTPUser
{
	my (undef,$ftpUserID) = @_;


	# Check params
	if (!defined($ftpUserID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserID' undefined");
	}
	if (!defined($ftpUserID = isNumber($ftpUserID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ftpUserID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# FIXME: Admin has no restrictions

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this ftpUser
		my $res = smlib::WebHosting::FTP::canAccessFTPUser($agentID,$ftpUserID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::WebHosting::FTP::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access ftpUser ID '$ftpUserID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::WebHosting::FTP::getFTPUser($ftpUserID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,smlib::WebHosting::FTP::Error());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Username','string');
	$response->addField('HomeDirectory','string');
#	$response->addField('AgentDisabled','boolean');
#	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);

	return $response->export();
}






1;
# vim: ts=4
