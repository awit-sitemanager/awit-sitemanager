# WebHosting Database SOAP interface
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class WebHosting::Database
# This is the main agent class, allowing control over agent services
package WebHosting::Database;

use strict;

use Auth;

use smlib::WebHosting;
use smlib::WebHosting::Database;

use smlib::constants;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "WebHosting::Database",
	Init => \&init,
};


## @internal
# Initialize module
sub init {
	my $server = shift;

	Auth::aclAdd('WebHosting::Database','ping','WebHosting/Database/Test');

	Auth::aclAdd('WebHosting::Database','createDatabase','WebHosting/Databases/Add');
	Auth::aclAdd('WebHosting::Database','updateDatabase','WebHosting/Databases/Update');
	Auth::aclAdd('WebHosting::Database','removeDatabase','WebHosting/Databases/Remove');
	Auth::aclAdd('WebHosting::Database','getDatabases','WebHosting/Databases/List');
	Auth::aclAdd('WebHosting::Database','getDatabase','WebHosting/Databases/Get');

	Auth::aclAdd('WebHosting::Database','getDatabaseUsers','WebHosting/Databases/List');

	Auth::aclAdd('WebHosting::Database','getSystemDatabases','WebHosting/Databases/List');

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method createDatabase($databaseInfo)
# Create a database
#
# @param databaseInfo Database info hash ref
# @li SystemDatabaseID - System database ID
# @li WebsiteID - Website ID
# @li Username - Database username
# @li Password - Database user password
# @li DatabaseName - Database name
# @li AgentDisabled - Optional, flag to indicate agent has disabled this site (NOT IMPLEMENTED
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
#
# @return Database ID
sub createDatabase
{
	my (undef,$databaseInfo) = @_;


	my $databaseData;

	# Check params
	if (!defined($databaseInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' not defined");
	}
	if (!isHash($databaseInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' is not a HASH");
	}
	if (!defined($databaseInfo->{'SystemDatabaseID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' has no element 'SystemDatabaseID'");
	}
	if (!defined($databaseInfo->{'SystemDatabaseID'} = isNumber($databaseInfo->{'SystemDatabaseID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' element 'SystemDatabaseID' is invalid");
	}
	if (!defined($databaseInfo->{'WebsiteID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' has no element 'WebsiteID'");
	}
	if (!defined($databaseInfo->{'WebsiteID'} = isNumber($databaseInfo->{'WebsiteID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' element 'WebsiteID' is invalid");
	}
	if (!defined($databaseInfo->{"Username"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' has no element 'Username'");
	}
	if (!defined($databaseInfo->{'Username'} = isUsername($databaseInfo->{'Username'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' element 'Username' is invalid");
	}
	if (!defined($databaseInfo->{"Password"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' has no element 'Password'");
	}
	if (!defined($databaseInfo->{"DatabaseName"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' has no element 'DatabaseName'");
	}
	if (!defined($databaseInfo->{'DatabaseName'} = isDatabaseName($databaseInfo->{'DatabaseName'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' element 'DatabaseName' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# FIXME: Admin has no restrictions

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this website
		my $res = smlib::WebHosting::Database::canAccessWebsite($agentID,$databaseInfo->{'WebsiteID'});
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::WebHosting::Database::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access website ID '".$databaseInfo->{'WebsiteID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Build request
	$databaseData->{'SystemDatabaseID'} = $databaseInfo->{'SystemDatabaseID'};
	$databaseData->{'WebsiteID'} = $databaseInfo->{'WebsiteID'};
	$databaseData->{'Username'} = $databaseInfo->{'Username'};
	$databaseData->{'Password'} = $databaseInfo->{'Password'};
	$databaseData->{'DatabaseName'} = $databaseInfo->{'DatabaseName'};
	$databaseData->{'AgentDisabled'} = booleanize($databaseInfo->{'AgentDisabled'}) if (defined($databaseInfo->{'AgentDisabled'}));
	$databaseData->{'AgentRef'} = $databaseInfo->{'AgentRef'};

	my $res = smlib::WebHosting::Database::createDatabase($databaseData);
	if ($res < 0) {
		return SOAPError($res,smlib::WebHosting::Database::Error());
	}

	return SOAPSuccess($res,'int','DatabaseID');
}



## @method updateDatabase($databaseInfo)
# Update a database
#
# @param databaseInfo Database info hash ref
# @li ID - Database ID
# @li Password - Database user password
# @li AgentDisabled - Optional, if this item has been disabled by the agent (NOT IMPLEMENTED)
# @li AgentRef - Optional, agent reference (NOT IMPLEMENTED)
#
# @return 0 on success or < 0 on error
sub updateDatabase
{
	my (undef,$databaseInfo) = @_;


	# Check params
	if (!defined($databaseInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' not defined");
	}
	if (!isHash($databaseInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' is not a HASH");
	}
	if (!defined($databaseInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' has no element 'ID'");
	}
	if (!defined($databaseInfo->{'ID'} = isNumber($databaseInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseInfo' element 'ID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this database
		my $res = smlib::WebHosting::Database::canAccessDatabase($agentID,$databaseInfo->{'ID'});
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::WebHosting::Database::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access database ID '".$databaseInfo->{'ID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;
	$i->{'ID'} = $databaseInfo->{'ID'};
	$i->{'Username'} = $databaseInfo->{'Username'};
	$i->{'Password'} = $databaseInfo->{'Password'};
	$i->{'AgentDisabled'} = booleanize($databaseInfo->{'AgentDisabled'}) if (defined($databaseInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $databaseInfo->{'AgentRef'} if (defined($databaseInfo->{'AgentRef'}));

	# Do the update
	my $res = smlib::WebHosting::Database::updateDatabase($i);
	if ($res < 0) {
		return SOAPError($res,smlib::WebHosting::Database::Error());
	}

	return SOAPSuccess();
}



## @method removeDatabase($databaseID)
# Remove database
#
# @param databaseID Mail database ID
#
# @return 0 on success or < 0 on error
sub removeDatabase
{
	my (undef,$databaseID) = @_;


	# Check params
	if (!defined($databaseID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseID' undefined");
	}
	if (!defined($databaseID = isNumber($databaseID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {


	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this database
		my $res = smlib::WebHosting::Database::canAccessDatabase($agentID,$databaseID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::WebHosting::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access database ID '$databaseID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $res = smlib::WebHosting::Database::removeDatabase($databaseID);
	if ($res < 0) {
		return SOAPError($res,smlib::WebHosting::Error());
	}

	return SOAPSuccess();
}



## @method getDatabases($websiteID,$search)
# Return a list of all the database an agent has
#
# @param websiteID Website ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Database ID on the system
# @li SystemDatabaseID - System database ID
# @li SystemDatabaseName - System database name
# @li Type - Database type
# @li DatabaseName - Database name
# @li AgentDisabled - Set if the agent has disabled this database (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getDatabases
{
	my (undef,$websiteID,$search) = @_;


	# Check params
	if (!defined($websiteID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteID' undefined");
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# FIXME: Admin has no restrictions

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this website
		my $res = smlib::WebHosting::canAccessWebsite($agentID,$websiteID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::MailHosting::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access website ID '$websiteID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::WebHosting::Database::getDatabases($websiteID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,smlib::WebHosting::Database::Error());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Username','string');
	$response->addField('SystemDatabaseID','int');
	$response->addField('SystemDatabaseName','string');
	$response->addField('Type','string');
	$response->addField('DatabaseName','string');
#	$response->addField('AgentDisabled','boolean');
#	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getDatabase($databaseID)
# Return a hash containing the info for this database
#
# @param databaseID Database ID
#
# @return Hash ref containing database info
# @li ID - Database ID on the system
# @li SystemDatabaseID - System databae ID
# @li SystemDatabaseName - System databae name
# @li Type - Database type
# @li DatabaseName - Database name
# @li AgentDisabled - Set if the agent has disabled this database (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getDatabase
{
	my (undef,$databaseID) = @_;


	# Check params
	if (!defined($databaseID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseID' undefined");
	}
	if (!defined($databaseID = isNumber($databaseID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'databaseID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# FIXME: Admin has no restrictions

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this database
		my $res = smlib::WebHosting::Database::canAccessDatabase($agentID,$databaseID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::WebHosting::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access database ID '$databaseID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::WebHosting::Database::getDatabase($databaseID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,smlib::WebHosting::Error());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('SystemDatabaseID','int');
	$response->addField('SystemDatabaseName','string');
	$response->addField('Type','string');
	$response->addField('DatabaseName','string');
#	$response->addField('AgentDisabled','boolean');
#	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);
	return $response->export();
}


## @method getDatabaseUsers($websiteID,$search)
# Return a list of all the database an agent has
#
# @param websiteID Website ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Database ID on the system
# @li SystemDatabaseID - System database ID
# @li SystemDatabaseName - System database name
# @li Type - Database type
# @li DatabaseName - Database name
# @li AgentDisabled - Set if the agent has disabled this database (NOT IMPLEMENTED)
# @li AgentRef - %Agent reference (NOT IMPLEMENTED)
sub getDatabaseUsers
{
	my (undef,$websiteID,$search) = @_;


	# Check params
	if (!defined($websiteID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteID' undefined");
	}
	if (!defined($websiteID = isNumber($websiteID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'websiteID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# FIXME: Admin has no restrictions

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		my $agentID;

		# Make sure we have an agent ID and its valid
		if (!defined($agentID = Auth::getUserAttribute('AgentID'))) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this website
		my $res = smlib::WebHosting::canAccessWebsite($agentID,$websiteID);
		# Check return code
		if ($res < 0) {
			return SOAPError($res,smlib::MailHosting::Error());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access website ID '$websiteID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::WebHosting::Database::getDatabaseUsers($websiteID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,smlib::WebHosting::Database::Error());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Username','string');
	$response->addField('SystemDatabaseID','int');
	$response->addField('SystemDatabaseName','string');
	$response->addField('Type','string');
	$response->addField('DatabaseName','string');
#	$response->addField('AgentDisabled','boolean');
#	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}


## @method getSystemDatabases($search)
# Return list of System Database Names
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - System database ID
# @li SystemDatabaseName - Pretty system database name
# @li Type - System database type
# @li Server - Server hosting the system database
sub getSystemDatabases
{
	my (undef,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData, $numResults) = smlib::WebHosting::Database::getSystemDatabases($search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,smlib::WebHosting::Database::Error());
	}

	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Username','string');
	$response->addField('SystemDatabaseName','string');
	$response->addField('Type','string');
	$response->addField('Server','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}




1;
# vim: ts=4
