# MailHosting SOAP interface
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class MailHosting
package MailHosting;

use strict;

use Auth;

use smlib::MailHosting;

use smlib::attributes;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "MailHosting",
	Init => \&init,
};


## @internal
# Initialize module
sub init {
	my $server = shift;


	# Initialize library
	if ((my $res = smlib::MailHosting::_init($server)) != RES_OK) {
		return $res;
	}

	Auth::aclAdd('Mailhosting','ping','MailHosting/Test');

	Auth::aclAdd('MailHosting','createMailTransport','MailHosting/MailTransports/Add');
	Auth::aclAdd('MailHosting','updateMailTransport','MailHosting/MailTransports/Remove');
	Auth::aclAdd('MailHosting','removeMailTransport','MailHosting/MailTransports/Update');
	Auth::aclAdd('MailHosting','getMailTransports','MailHosting/MailTransports/List');
	Auth::aclAdd('MailHosting','getMailTransport','MailHosting/MailTransports/Get');

	Auth::aclAdd('MailHosting','recreateMailbox','MailHosting/Mailboxes/Recreate');
	Auth::aclAdd('MailHosting','createMailbox','MailHosting/Mailboxes/Add');
	Auth::aclAdd('MailHosting','updateMailbox','MailHosting/Mailboxes/Update');
	Auth::aclAdd('MailHosting','removeMailbox','MailHosting/Mailboxes/Remove');
	Auth::aclAdd('MailHosting','getMailboxes','MailHosting/Mailboxes/List');
	Auth::aclAdd('MailHosting','getMailbox','MailHosting/Mailboxes/Get');

	Auth::aclAdd('MailHosting','createMailboxAlias','MailHosting/MailboxAliases/Add');
	Auth::aclAdd('MailHosting','updateMailboxAlias','MailHosting/MailboxAliases/Update');
	Auth::aclAdd('MailHosting','removeMailboxAlias','MailHosting/MailboxAliases/Remove');
	Auth::aclAdd('MailHosting','getMailboxAliases','MailHosting/MailboxAliases/List');
	Auth::aclAdd('MailHosting','getMailboxAlias','MailHosting/MailboxAliases/Get');

#	Auth::aclAdd('MailHosting','getMailPolicies','MailHosting/MailPolicies/List');

	Auth::aclAdd('MailHosting','getMailHostingServerGroups','MailHosting/ServerGroups/List');

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method createMailTransport($serverGroupID,$transportInfo)
# Create a mail transport
#
# @param serverGroupID Server group ID
#
# @param transportInfo Transport info hash ref
# @li AgentID - Agent ID
# @li DomainName - Domain name to add
# @li Transport - Transport to use
# @li Detail -  Optional, Transport detail
# @li AgentRef - Optional, Agent ref
# @li AgentDisabled - Optional, Disable this transport
#
# @return Transport ID
sub createMailTransport
{
	my (undef,$serverGroupID,$transportInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($transportInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' not defined");
	}
	if (!isHash($transportInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' is not a HASH");
	}

	if (!defined($transportInfo->{'AgentID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentID' not defined");
	}
	if (!defined($transportInfo->{'AgentID'} = isNumber($transportInfo->{'AgentID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentID' is invalid");
	}

	if (!defined($transportInfo->{'DomainName'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' has no element 'DomainName'");
	}
	if (!isVariable($transportInfo->{'DomainName'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'DomainName' is invalid");
	}
	$transportInfo->{'DomainName'} = lc($transportInfo->{'DomainName'});
	if (!($transportInfo->{'DomainName'} =~ /^[a-z0-9_\-\.]+$/)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'zoneInfo' element 'DomainName' is invalid");
	}

	if (!defined($transportInfo->{'Transport'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' has no element 'Transport' defined");
	}
	if (!isVariable($transportInfo->{'Transport'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'Transport' is invalid");
	}
	if ($transportInfo->{'Transport'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'Transport' is blank");
	}

	# Check detail
	if (defined($transportInfo->{'Detail'})) {
		# Check for non-variable
		if (!isVariable($transportInfo->{'Detail'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'Detail' is invalid");
		}
		# Delete element if string is empty
		if ($transportInfo->{'Detail'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'Detail' is blank");
		}
	}

	if (defined($transportInfo->{'AgentRef'})) {
		# Check for non variable
		if (!isVariable($transportInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentRef' is invalid");
		}
	}

	# Check AgentDisabled
	if (defined($transportInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($transportInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentDisabled' is invalid");
		}
		# Check for boolean value
		if (!defined($transportInfo->{'AgentDisabled'} = isBoolean($transportInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Check if the agent ID is valid
		if (smlib::Agent::isAgentValid($transportInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentID' has an invalid value");
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$transportInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Build request
	my $transportData;
	$transportData->{'AgentID'} = $transportInfo->{'AgentID'};
	$transportData->{'DomainName'} = $transportInfo->{'DomainName'};
	$transportData->{'Transport'} = $transportInfo->{'Transport'};
	$transportData->{'Detail'} = $transportInfo->{'Detail'} if (defined($transportInfo->{'Detail'}));
	$transportData->{'AgentRef'} = $transportInfo->{'AgentRef'} if (defined($transportInfo->{'AgentRef'}));
	$transportData->{'AgentDisabled'} = $transportInfo->{'AgentDisabled'} if (defined($transportInfo->{'AgentDisabled'}));
	# And try create transport
	my $transportID = smlib::MailHosting::createMailTransport($serverGroupID,$transportData);
	if ($transportID < 0) {
		return SOAPError($transportID,getError());
	}

	# Get transport info
	my $transport = smlib::MailHosting::getMailTransport($serverGroupID,$transportID);
	if (ref($transport) ne "HASH") {
		my $error = getError();
		smlib::MailHosting::removeMailTransport($serverGroupID,$transportID);
		return SOAPError($transport,$error);
	}

	# XXX: NK - this is a bit of a hotwire
	if ($transport->{'Transport'} eq "virtual") {
		# Create mail transport mailstore
		my $i;
		$i->{'ID'} = $transport->{'ID'};
		$i->{'AgentID'} = $transport->{'AgentID'};
		$i->{'DomainName'} = $transport->{'DomainName'};
		my ($cres,$cval) = smlib::MailHosting::createMailTransportMailstore($serverGroupID,$i);
		if ($cres < 0) {
			my $error = getError();
			smlib::MailHosting::removeMailTransport($serverGroupID,$transportID);
			return SOAPError($cres,$error);
		}
	
		# Update transport with the mailstore location
		my $j;
		$j->{'ID'} = $transport->{'ID'};
		$j->{'MailstoreLocation'} = $cval;
		if ((my $ures = smlib::MailHosting::updateMailTransport($serverGroupID,$j)) < 0) {
			my $error = getError();
			smlib::MailHosting::removeMailTransportMailstore($serverGroupID,$j);
			smlib::MailHosting::removeMailTransport($serverGroupID,$transportID);
			return SOAPError($ures,$error);
		}
	}

	return SOAPSuccess($transportID,'int','MailTransportID');
}



## @method updateMailTransport($serverGroupID,$transportInfo)
# Update a mail transport
#
# @param serverGroupID Server group ID
#
# @param transportInfo Transport info hash ref
# @li ID - Transport ID
# @li Detail - LHS part of the transport, for instance   smtp:DETAIL
# @li MailstoreLocation - unsupported via API
# @li TransportDetail - unsupported via API
# @li AgentDisabled - Optional, if this item has been disabled by the agent
# @li AgentRef - Optional, agent reference
#
# @return 0 on success or < 0 on error
sub updateMailTransport
{
	my (undef,$serverGroupID,$transportInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($transportInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' not defined");
	}
	if (!isHash($transportInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' is not a HASH");
	}
	if (!defined($transportInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' has no element 'ID'");
	}
	if (!defined($transportInfo->{'ID'} = isNumber($transportInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'ID' is invalid");
	}

	# Check detail
	if (defined($transportInfo->{'Detail'})) {
		# Check for non-variable
		if (!isVariable($transportInfo->{'Detail'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'Detail' is invalid");
		}
		# Delete element if string is empty
		if ($transportInfo->{'Detail'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'Detail' is blank");
		}
	}

	if (defined($transportInfo->{'AgentRef'})) {
		if (!isVariable($transportInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentRef' is invalid");
		}
	}

	# Check AgentDisabled
	if (defined($transportInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($transportInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentDisabled' is invalid");
		}
		# Delete element if string is empty
		if (!defined($transportInfo->{'AgentDisabled'} = isBoolean($transportInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mail transport
		if (smlib::MailHosting::canAgentAccessMailTransport($callerAgentID,$transportInfo->{'ID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '".$transportInfo->{'ID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;
	$i->{'ID'} = $transportInfo->{'ID'};
	$i->{'AgentDisabled'} = $transportInfo->{'AgentDisabled'} if (defined($transportInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $transportInfo->{'AgentRef'} if (defined($transportInfo->{'AgentRef'}));

	# Do the update
	my $res = smlib::MailHosting::updateMailTransport($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeMailTransport($serverGroupID,$transportID)
# Remove mail transport
#
# @param serverGroupID Server group ID
#
# @param transportID Mail transport ID
#
# @return 0 on success or < 0 on error
sub removeMailTransport
{
	my (undef,$serverGroupID,$transportID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($transportID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportID' undefined");
	}
	if (!defined($transportID = isNumber($transportID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mail transport
		if (smlib::MailHosting::canAgentAccessMailTransport($callerAgentID,$transportID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Get transport info
	my $transport = smlib::MailHosting::getMailTransport($serverGroupID,$transportID);
	if (ref($transport) ne "HASH") {
		my $error = getError();
		return SOAPError($transport,$error);
	}

	# XXX: NK - this is a bit of a hotwire
	if ($transport->{'Transport'} eq "virtual") {
		# NK: Hotwire, only remove if we're defined and not blank?
		if (defined($transport->{'MailstoreLocation'}) && $transport->{'MailstoreLocation'} ne "") {
			my $i;
			$i->{'ID'} = $transport->{'ID'};
			$i->{'MailstoreLocation'} = $transport->{'MailstoreLocation'};
			# Remove mail transport mailstore
			my ($cres,$cval) = smlib::MailHosting::removeMailTransportMailstore($serverGroupID,$i);
			if ($cres < 0) {
				my $error = getError();
				return SOAPError($cres,$error);
			}
		}
	}

	# Update transport with the mailstore location
# NK: We didn't succeed, so why don't we try again in future?
#	my $j;
#	$j->{'ID'} = $transport->{'ID'};
#	# Special case " ", NULL the mailstore
#	$j->{'MailstoreLocation'} = " ";
#	if ((my $ures = smlib::MailHosting::updateMailTransport($serverGroupID,$j)) < 0) {
#		my $error = getError();
#		return SOAPError($ures,$error);
#	}

	# Finally remove the transport
	my $res = smlib::MailHosting::removeMailTransport($serverGroupID,$transportID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method getMailTransports($serverGroupID,$transportInfo,$search)
# Return a list of all the mail transport an agent has
#
# @param serverGroupID Server group ID
#
# @param transportInfo Transport info hash ref
# @li AgentID - Limit returned results to this agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Transport ID on the system
# @li AgentID - Agent ID
# @li AgentName - Agent name
# @li DomainName - Domain name of transport
# @li Transport - Transport for this domain
# @li Detail - Detail for this transport
# @li AgentDisabled - Set if the agent has disabled this transport
# @li Disabled - True if transport is disabled
# @li AgentRef - %Agent reference
sub getMailTransports
{
	my (undef,$serverGroupID,$transportInfo,$search) = @_;


	# Check serverGroupID
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	# We normally only have transportInfo if we an admin
	if (defined($transportInfo)) {
		# Make sure we're a hash
		if (!isHash($transportInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($transportInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($transportInfo->{'AgentID'} = isNumber($transportInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $transportData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($transportInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($transportInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'transportInfo' element 'AgentID' has an invalid value");
			}

			$transportData->{'AgentID'} = $transportInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we have an agent ID, use it
		if (!defined($transportInfo->{'AgentID'})) {
			$transportInfo->{'AgentID'} = $callerAgentID;
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$transportInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		$transportData->{'AgentID'} = $transportInfo->{'AgentID'};

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Grab the callers mail transport ID
		my $callerMailTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Make sure we have an agent ID
		if (!defined($callerMailTransportID)) {
			return SOAPError(ERR_UNKNOWN,"No 'MailTransportID' linked to your profile");
		}

		$transportData->{'TransportID'} = $callerMailTransportID;

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::MailHosting::getMailTransports($serverGroupID,$transportData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('AgentID','int');
	$response->addField('AgentName','string');
	$response->addField('DomainName','string');
	$response->addField('Transport','string');
	$response->addField('Detail','string');
	$response->addField('AgentDisabled','boolean');
	$response->addField('Disabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getMailTransport($serverGroupID,$transportID)
# Return a hash containing the info for this mail transport
#
# @param serverGroupID Server group ID
#
# @param transportID Transport ID
#
# @return Array ref of hash refs
# @li ID - Transport ID on the system
# @li DomainName - Domain name of transport
# @li Transport - Transport for this domain
# @li Detail - Detail for this transport
# @li AgentDisabled - Set if the agent has disabled this transport
# @li AgentRef - %Agent reference
sub getMailTransport
{
	my (undef,$serverGroupID,$transportID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($transportID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportID' undefined");
	}
	if (!defined($transportID = isNumber($transportID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mail transport
		if (smlib::MailHosting::canAgentAccessMailTransport($callerAgentID,$transportID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::MailHosting::getMailTransport($serverGroupID,$transportID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('AgentID','int');
	$response->addField('AgentName','string');
	$response->addField('DomainName','string');
	$response->addField('Transport','string');
	$response->addField('Detail','string');
	$response->addField('AgentDisabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createMailbox($serverGroupID,$mailboxInfo)
# Create a mailbox
#
# @param serverGroupID Server group ID
#
# @param mailboxInfo Mailbox info hash ref
# @li TransportID - Mail transport ID
# @li Address - %Mailbox address
# @li Password - %Mailbox password
# @li Name - Optional mailbox name
# @li Quota - Quota in Mbyte
# @li AgentDisabled - Optional flag agent can set to disable this mailbox
# @li AgentRef - Optional agent reference
#
# @return Returns mailbox ID
sub createMailbox
{
	my (undef,$serverGroupID,$mailboxInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($mailboxInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' undefined");
	}
	if (!isHash($mailboxInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' is not a HASH");
	}
	if (!defined($mailboxInfo->{'TransportID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'transportID' undefined");
	}
	if (!defined($mailboxInfo->{'TransportID'} = isNumber($mailboxInfo->{'TransportID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'transportID' is invalid");
	}
	if (!defined($mailboxInfo->{'Address'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' has no element 'Address' defined");
	}
	if (!isVariable($mailboxInfo->{'Address'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Address' is invalid");
	}
	if ($mailboxInfo->{'Address'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Address' is blank");
	}
	if (!($mailboxInfo->{'Address'} =~ /^[a-z0-9_\-\.]*$/)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Address' is invalid");
	}
	if (!defined($mailboxInfo->{'Password'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' has no element 'Password' defined");
	}
	if (!isVariable($mailboxInfo->{'Password'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Password' is invalid");
	}
	if ($mailboxInfo->{'Password'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Password' is blank");
	}
	if (defined($mailboxInfo->{'Name'})) {
		if (!isVariable($mailboxInfo->{'Name'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Name' is invalid");
		}
	}
	if (!defined($mailboxInfo->{'Quota'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' has no element 'Quota' defined");
	}
	if (!defined($mailboxInfo->{'Quota'} = isNumber($mailboxInfo->{'Quota'},ISNUMBER_ALLOW_ZERO))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Quota' is invalid");
	}
	if (defined($mailboxInfo->{'AgentRef'})) {
		if (!isVariable($mailboxInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'AgentRef' is invalid");
		}
	}
	if (defined($mailboxInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($mailboxInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'AgentDisabled' is invalid");
		}
		# Delete element if string is empty
		if (!defined($mailboxInfo->{'AgentDisabled'} = isBoolean($mailboxInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mail transport
		if (smlib::MailHosting::canAgentAccessMailTransport($callerAgentID,$mailboxInfo->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '".
					$mailboxInfo->{'TransportID'}."'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$mailboxInfo->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '".$mailboxInfo->{'TransportID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;

	$i->{'TransportID'} = $mailboxInfo->{'TransportID'};
	$i->{'Address'} = $mailboxInfo->{'Address'};
	$i->{'Password'} = $mailboxInfo->{'Password'};
	$i->{'Name'} = $mailboxInfo->{'Name'} if (defined($mailboxInfo->{'Name'}));
	$i->{'Quota'} = $mailboxInfo->{'Quota'};

	$i->{'AgentDisabled'} = $mailboxInfo->{'AgentDisabled'} if (defined($mailboxInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $mailboxInfo->{'AgentRef'} if (defined($mailboxInfo->{'AgentRef'}));

	my $mailboxID = smlib::MailHosting::createMailbox($serverGroupID,$i);
	if ($mailboxID < 0) {
		return SOAPError($mailboxID,getError());
	}

	# Get mailbox info
	my $mailbox = smlib::MailHosting::getMailbox($serverGroupID,$mailboxID);
	if (ref($mailbox) ne "HASH") {
		my $error = getError();
		smlib::MailHosting::removeMailbox($serverGroupID,$mailboxID);
		return SOAPError($mailbox,$error);
	}

	# Get transport info
	my $transport = smlib::MailHosting::getMailTransport($serverGroupID,$mailbox->{'TransportID'});
	if (ref($transport) ne "HASH") {
		my $error = getError();
		smlib::MailHosting::removeMailbox($serverGroupID,$mailboxID);
		return SOAPError($transport,$error);
	}

	# XXX: NK - this is a bit of a hotwire
	if ($transport->{'Transport'} eq "virtual") {
		# Create transport mailstore
		my $i;
		$i->{'TransportID'} = $transport->{'ID'};
		$i->{'ID'} = $mailbox->{'ID'};
		$i->{'Address'} = $mailbox->{'Address'};
		$i->{'AgentID'} = $transport->{'AgentID'};
		$i->{'TransportMailstoreLocation'} = $transport->{'MailstoreLocation'};
		$i->{'MailstoreSize'} = $mailboxInfo->{'Quota'};
		# Create mail transport mailstore
		my ($cres,$cval) = smlib::MailHosting::createMailboxMailstore($serverGroupID,$i);
		if ($cres < 0) {
			my $error = getError();
			smlib::MailHosting::removeMailbox($serverGroupID,$mailboxID);
			return SOAPError($cres,$error);
		}
	
		# Update transport with the mailstore location
		my $j;
		$j->{'ID'} = $mailbox->{'ID'};
		$j->{'MailstoreLocation'} = $cval;
		if ((my $ures = smlib::MailHosting::updateMailbox($serverGroupID,$j)) < 0) {
			my $error = getError();
			smlib::MailHosting::removeMailboxMailstore($serverGroupID,$j);
			smlib::MailHosting::removeMailbox($serverGroupID,$mailboxID);
			return SOAPError($ures,$error);
		}
	}

	return SOAPSuccess($mailboxID,'int','MailboxID');
}



## @method updateMailbox($serverGroupID,$mailboxInfo)
# Update mailbox
#
# @param serverGroupID Server group ID
#
# @param mailboxInfo %Mailbox info hash ref
# @li ID - %Mailbox ID
# @li Password - Optional mailbox password
# @li Name - Optional mailbox name
# @li Quota - Quota in Mbyte
# @li AgentDisabled - Optional flag agent can set to disable this mailbox
# @li AgentRef - Optional agent reference
#
# @return Returns mailbox ID
sub updateMailbox
{
	my (undef,$serverGroupID,$mailboxInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($mailboxInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' undefined");
	}
	if (!isHash($mailboxInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' is not a HASH");
	}
	if (!defined($mailboxInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' has no element 'ID' defined");
	}
	if (!defined($mailboxInfo->{'ID'} = isNumber($mailboxInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'ID' is invalid");
	}
	if (defined($mailboxInfo->{'Password'})) {
		if (!isVariable($mailboxInfo->{'Password'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Password' is invalid");
		}
	}
	if (defined($mailboxInfo->{'Name'})) {
		if (!isVariable($mailboxInfo->{'Name'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Name' is invalid");
		}
	}
	if (defined($mailboxInfo->{'Quota'})) {
		if (!defined($mailboxInfo->{'Quota'} = isNumber($mailboxInfo->{'Quota'},ISNUMBER_ALLOW_ZERO))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Quota' is invalid");
		}
	}

	if (defined($mailboxInfo->{'AgentRef'})) {
		if (!isVariable($mailboxInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'AgentRef' is invalid");
		}
	}

	# Check AgentDisabled
	if (defined($mailboxInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($mailboxInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'AgentDisabled' is invalid");
		}
		# Delete element if string is empty
		if (!defined($mailboxInfo->{'AgentDisabled'} = isBoolean($mailboxInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	my $mailbox;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mailbox
		if (smlib::MailHosting::canAgentAccessMailbox($callerAgentID,$mailboxInfo->{'ID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox ID '".$mailboxInfo->{'ID'}."'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Get transport info from mailbox
		$mailbox = smlib::MailHosting::getMailbox($serverGroupID,$mailboxInfo->{'ID'});
		if (ref($mailbox) ne "HASH") {
			return SOAPError($mailbox,getError());
		}

		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$mailbox->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox ID '".$mailboxInfo->{'ID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# If mailbox not defined, grab it
	if (!defined($mailbox)) {
		# Get mailbox info
		$mailbox = smlib::MailHosting::getMailbox($serverGroupID,$mailboxInfo->{'ID'});
		if (ref($mailbox) ne "HASH") {
			return SOAPError($mailbox,getError());
		}
	}

	# NK: Hotwire, only update if we're defined and not blank?
	if (defined($mailbox->{'MailstoreLocation'}) && $mailbox->{'MailstoreLocation'} ne "") {
		# Remove mailbox mailstore
		my $i;
		$i->{'ID'} = $mailbox->{'ID'};
		$i->{'MailstoreLocation'} = $mailbox->{'MailstoreLocation'};
		$i->{'MailstoreSize'} = defined($mailboxInfo->{'Quota'}) ? $mailboxInfo->{'Quota'} : $mailbox->{'Quota'};
		# Remove mail transport mailstore
		my ($cres,$cval) = smlib::MailHosting::updateMailboxMailstore($serverGroupID,$i);
		if ($cres < 0) {
			my $error = getError();
			return SOAPError($cres,$error);
		}
	}
	my $i;

	$i->{'ID'} = $mailboxInfo->{'ID'};
	$i->{'Password'} = $mailboxInfo->{'Password'} if (defined($mailboxInfo->{'Password'}));
	$i->{'Name'} = $mailboxInfo->{'Name'} if (defined($mailboxInfo->{'Name'}));
	$i->{'Quota'} = $mailboxInfo->{'Quota'} if (defined($mailboxInfo->{'Quota'}));
	$i->{'AgentDisabled'} = $mailboxInfo->{'AgentDisabled'} if (defined($mailboxInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $mailboxInfo->{'AgentRef'} if (defined($mailboxInfo->{'AgentRef'}));

	my $res = smlib::MailHosting::updateMailbox($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeMailbox($serverGroupID,$mailboxID)
# Remove mailbox
#
# @param serverGroupID Server group ID
#
# @param mailboxID %Mailbox ID
#
# @return 0 on success or < 0 on error
sub removeMailbox
{
	my (undef,$serverGroupID,$mailboxID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($mailboxID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxID' undefined");
	}
	if (!defined($mailboxID = isNumber($mailboxID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mailbox
		if (smlib::MailHosting::canAgentAccessMailbox($callerAgentID,$mailboxID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox ID '$mailboxID'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Get transport info from mailbox
		my $mailbox = smlib::MailHosting::getMailbox($serverGroupID,$mailboxID);
		if (ref($mailbox) ne "HASH") {
			return SOAPError($mailbox,getError());
		}

		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$mailbox->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox ID '$mailboxID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Get mailbox info
	my $mailbox = smlib::MailHosting::getMailbox($serverGroupID,$mailboxID);
	if (ref($mailbox) ne "HASH") {
		my $error = getError();
		return SOAPError($mailbox,$error);
	}

	# NK: Hotwire, only remove if we're defined and not blank?
	if (defined($mailbox->{'MailstoreLocation'}) && $mailbox->{'MailstoreLocation'} ne "") {
		# Remove mailbox mailstore
		my $i;
		$i->{'ID'} = $mailbox->{'ID'};
		$i->{'MailstoreLocation'} = $mailbox->{'MailstoreLocation'};
		# Remove mail transport mailstore
		my ($cres,$cval) = smlib::MailHosting::removeMailboxMailstore($serverGroupID,$i);
		if ($cres < 0) {
			my $error = getError();
			return SOAPError($cres,$error);
		}
	}

	# Update mailbox with blank mailstore location
# NK: We didn't succeed, so why don't we try again in future?
#	my $j;
#	$j->{'ID'} = $mailbox->{'ID'};
#	# Special case " ", NULL the mailstore
#	$j->{'MailstoreLocation'} = " ";
#	if ((my $ures = smlib::MailHosting::updateMailbox($serverGroupID,$j)) < 0) {
#		my $error = getError();
#		return SOAPError($ures,$error);
#	}

	# Finally remove the mailbox
	my $res = smlib::MailHosting::removeMailbox($serverGroupID,$mailboxID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method recreateMailbox($serverGroupID,$mailboxID)
# Recreate a mailbox
#
# @param serverGroupID Server group ID
#
# @param mailboxID Mailbox ID
#
# @return Returns mailbox ID
sub recreateMailbox
{
	my (undef,$serverGroupID,$mailboxID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($mailboxID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxID' undefined");
	}
	if (!defined($mailboxID = isNumber($mailboxID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Get transport info from mailbox
		my $mailbox = smlib::MailHosting::getMailbox($serverGroupID,$mailboxID);
		if (ref($mailbox) ne "HASH") {
			return SOAPError($mailbox,getError());
		}

		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$mailbox->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox ID '$mailboxID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $res = smlib::MailHosting::recreateMailbox($serverGroupID,$mailboxID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','MailboxID');
}



## @method getMailboxes($serverGroupID,$transportID,$search)
# Function to return a list of mailboxes
#
# @param serverGroupID Server group ID
#
# @param transportID Mail transport ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - %Mailbox ID on the system
# @li Address - Email address
# @li Name - Name of person
# @li DomainName - Domain name of the email address
# @li Quota - Quota in Mbyte
# @li AgentDisabled - Set if the agent has disabled this transport
# @li Disabled - True if mailbox is disabled
# @li AgentRef - %Agent reference
sub getMailboxes
{
	my (undef,$serverGroupID,$transportID,$search) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($transportID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportID' undefined");
	}
	if (!defined($transportID = isNumber($transportID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mail transport
		if (smlib::MailHosting::canAgentAccessMailTransport($callerAgentID,$transportID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$transportID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Mailbox info hash
	my $mailboxData;
	$mailboxData->{'TransportID'} = $transportID;

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::MailHosting::getMailboxes($serverGroupID,$mailboxData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Address','string');
	$response->addField('Name','string');
	$response->addField('DomainName','string');
	$response->addField('Quota','int');
	$response->addField('AgentDisabled','boolean');
	$response->addField('Disabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getMailbox($serverGroupID,$mailboxID)
# Get mailbox information
#
# @param serverGroupID Server group ID
#
# @param mailboxID %Mailbox ID
#
# @return Mailbox info hash ref
# @li ID - %Mailbox ID on the system
# @li Address - Email address
# @li Name - Name of person
# @li DomainName - Domain name of the email address
# @li Quota - Quota in Mbyte
# @li AgentDisabled - Set if the agent has disabled this transport
# @li Disabled - Set if mailbox is disabled
# @li AgentRef - %Agent reference
sub getMailbox
{
	my (undef,$serverGroupID,$mailboxID) = @_;

	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($mailboxID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxID' undefined");
	}
	if (!defined($mailboxID = isNumber($mailboxID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mailbox
		if (smlib::MailHosting::canAgentAccessMailbox($callerAgentID,$mailboxID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox ID '$mailboxID'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Get transport info from mailbox
		my $mailbox = smlib::MailHosting::getMailbox($serverGroupID,$mailboxID);
		if (ref($mailbox) ne "HASH") {
			return SOAPError($mailbox,getError());
		}

		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$mailbox->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox ID '$mailboxID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::MailHosting::getMailbox($serverGroupID,$mailboxID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Address','string');
	$response->addField('Name','string');
	$response->addField('DomainName','string');
	$response->addField('Quota','int');
	$response->addField('Disabled','boolean');
	$response->addField('AgentDisabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createMailboxAlias($serverGroupID,$mailboxAliasInfo)
# Create a mailbox alias
#
# @param serverGroupID Server group ID
#
# @param mailboxAliasInfo %Mailbox alias info hash ref
# @li TransportID - Mail transport ID
# @li Address - %Mailbox alias
# @li Goto - %Mailbox alias destination
# @li AgentDisabled - Optional flag agent can set to disable this mailbox
# @li AgentRef - Optional agent reference
#
# @return Returns mailbox alias ID
sub createMailboxAlias
{
	my (undef,$serverGroupID,$mailboxAliasInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($mailboxAliasInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' undefined");
	}
	if (!isHash($mailboxAliasInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' is not a HASH");
	}
	if (!defined($mailboxAliasInfo->{'TransportID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'transportID' undefined");
	}
	if (!defined($mailboxAliasInfo->{'TransportID'} = isNumber($mailboxAliasInfo->{'TransportID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'transportID' is invalid");
	}
	if (!defined($mailboxAliasInfo->{'Address'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' has no element 'Address' defined");
	}
	if (!isVariable($mailboxAliasInfo->{"Address"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'Address' is invalid");
	}
	if ($mailboxAliasInfo->{'Address'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'Address' is blank");
	}
	if (!($mailboxAliasInfo->{'Address'} =~ /^[a-z0-9_\-\.]*$/)) {
		SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'Address' is invalid");
	}
	if (!defined($mailboxAliasInfo->{'Goto'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' has no element 'Goto' defined");
	}
	if (!isVariable($mailboxAliasInfo->{"Goto"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'Goto' is invalid");
	}
	if ($mailboxAliasInfo->{'Goto'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'Goto' is blank");
	}
	if (defined($mailboxAliasInfo->{'AgentRef'})) {
		if (!isVariable($mailboxAliasInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'AgentRef' is invalid");
		}
	}
	if (defined($mailboxAliasInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($mailboxAliasInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'AgentDisabled' is invalid");
		}
		# Check for invalid value
		if (!defined($mailboxAliasInfo->{'AgentDisabled'} = isBoolean($mailboxAliasInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mail transport
		if (smlib::MailHosting::canAgentAccessMailTransport($callerAgentID,$mailboxAliasInfo->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '".$mailboxAliasInfo->{'TransportID'}."'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$mailboxAliasInfo->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '".$mailboxAliasInfo->{'TransportID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;

	$i->{'TransportID'} = $mailboxAliasInfo->{'TransportID'};
	$i->{'Address'} = $mailboxAliasInfo->{'Address'};
	$i->{'Goto'} = $mailboxAliasInfo->{'Goto'};
	$i->{'AgentDisabled'} = $mailboxAliasInfo->{'AgentDisabled'} if (defined($mailboxAliasInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $mailboxAliasInfo->{'AgentRef'} if (defined($mailboxAliasInfo->{'AgentRef'}));

	my $res = smlib::MailHosting::createMailboxAlias($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','MailboxAliasID');
}



## @method updateMailboxAlias($serverGroupID,$mailboxAliasInfo)
# Update mailbox alias
#
# @param serverGroupID Server group ID
#
# @param mailboxAliasInfo %Mailbox alias info hash ref
# @li ID - %Mailbox alias ID
# @li Goto - Optional alias destination address
# @li AgentDisabled - Optional flag agent can set to disable this mailbox alias
# @li AgentRef - Optional agent reference
#
# @return 0 on success or < 0 on error
sub updateMailboxAlias
{
	my (undef,$serverGroupID,$mailboxAliasInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($mailboxAliasInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' undefined");
	}
	if (!isHash($mailboxAliasInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' is not a HASH");
	}
	if (!defined($mailboxAliasInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' has no element 'ID' defined");
	}
	if (!defined($mailboxAliasInfo->{'ID'} = isNumber($mailboxAliasInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'ID' is invalid");
	}
	if (defined($mailboxAliasInfo->{'Goto'})) {
		if (!isVariable($mailboxAliasInfo->{"Goto"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'Goto' is invalid");
		}
		if ($mailboxAliasInfo->{'Goto'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'Goto' is blank");
		}
	}
	if (defined($mailboxAliasInfo->{'AgentRef'})) {
		if (!isVariable($mailboxAliasInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'AgentRef' is invalid");
		}
	}
	if (defined($mailboxAliasInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($mailboxAliasInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'AgentDisabled' is invalid");
		}
		# Delete element if string is empty
		if (!defined($mailboxAliasInfo->{'AgentDisabled'} = isBoolean($mailboxAliasInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mailbox
		if (smlib::MailHosting::canAgentAccessMailboxAlias($callerAgentID,$mailboxAliasInfo->{'ID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox alias ID '".$mailboxAliasInfo->{'ID'}."'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Get transport info from mailboxAlias
		my $mailboxAlias = smlib::MailHosting::getMailboxAlias($serverGroupID,$mailboxAliasInfo->{'ID'});
		if (ref($mailboxAlias) ne "HASH") {
			return SOAPError($mailboxAlias,getError());
		}

		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$mailboxAlias->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailboxAlias ID '".$mailboxAliasInfo->{'ID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}


	my $i;

	$i->{'ID'} = $mailboxAliasInfo->{'ID'};
	$i->{'Goto'} = $mailboxAliasInfo->{'Goto'} if (defined($mailboxAliasInfo->{'Goto'}));
	$i->{'AgentDisabled'} = $mailboxAliasInfo->{'AgentDisabled'} if (defined($mailboxAliasInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $mailboxAliasInfo->{'AgentRef'} if (defined($mailboxAliasInfo->{'AgentRef'}));

	my $res = smlib::MailHosting::updateMailboxAlias($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}


## @method removeMailboxAlias($serverGroupID,$mailboxAliasID)
# Remove mailbox alias
#
# @param serverGroupID Server group ID
#
# @param mailboxAliasID %Mailbox alias ID
#
# @return 0 on success or < 0 on error
sub removeMailboxAlias
{
	my (undef,$serverGroupID,$mailboxAliasID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($mailboxAliasID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasID' undefined");
	}
	if (!defined($mailboxAliasID = isNumber($mailboxAliasID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mailbox
		if (smlib::MailHosting::canAgentAccessMailboxAlias($callerAgentID,$mailboxAliasID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox alias ID '$mailboxAliasID'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Get transport info from mailboxAlias
		my $mailboxAlias = smlib::MailHosting::getMailboxAlias($serverGroupID,$mailboxAliasID);
		if (ref($mailboxAlias) ne "HASH") {
			return SOAPError($mailboxAlias,getError());
		}

		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$mailboxAlias->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailboxAlias ID '$mailboxAliasID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $res = smlib::MailHosting::removeMailboxAlias($serverGroupID,$mailboxAliasID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method getMailboxAliases($serverGroupID,$transportID,$search)
# Return agents mailbox aliases
#
# @param serverGroupID Server group ID
#
# @param transportID Mail transport ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - %Mailbox ID on the system
# @li Address - Email address
# @li DomainName - Domain name of the alias address
# @li Goto - Alias destination
# @li AgentDisabled - Set if the agent has disabled this transport
# @li Disabled - True if mailbox is disabled
# @li AgentRef - %Agent reference
sub getMailboxAliases
{
	my (undef,$serverGroupID,$transportID,$search) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($transportID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportID' undefined");
	}
	if (!defined($transportID = isNumber($transportID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'transportID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mail transport
		if (smlib::MailHosting::canAgentAccessMailTransport($callerAgentID,$transportID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$transportID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Mailbox info hash
	my $mailboxAliasData;
	$mailboxAliasData->{'TransportID'} = $transportID;

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::MailHosting::getMailboxAliases($serverGroupID,$mailboxAliasData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build result
	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Address','string');
	$response->addField('DomainName','string');
	$response->addField('Goto','string');
	$response->addField('AgentDisabled','boolean');
	$response->addField('Disabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getMailboxAlias($serverGroupID,$mailboxAliasID)
# Get mailbox alias information
#
# @param serverGroupID Server group ID
#
# @param mailboxAliasID %Mailbox alias ID
#
# @return Mailbox alias info hash ref
# @li ID - %Mailbox alias ID on the system
# @li Address - Email address
# @li Goto - Alias destination
# @li AgentDisabled - Set if the agent has disabled this transport
# @li Disabled - Set if mailbox alias is disabled
# @li AgentRef - %Agent reference
sub getMailboxAlias
{
	my (undef,$serverGroupID,$mailboxAliasID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($mailboxAliasID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasID' undefined");
	}
	if (!defined($mailboxAliasID = isNumber($mailboxAliasID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'mailboxAliasID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this mailbox
		if (smlib::MailHosting::canAgentAccessMailboxAlias($callerAgentID,$mailboxAliasID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailbox alias ID '$mailboxAliasID'");
		}

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Get transport info from mailboxAlias
		my $mailboxAlias = smlib::MailHosting::getMailboxAlias($serverGroupID,$mailboxAliasID);
		if (ref($mailboxAlias) ne "HASH") {
			return SOAPError($mailboxAlias,getError());
		}

		# Grab the callers mail transport ID
		my $callerTransportID = smlib::servers::getUserAttributeWithServerGroup(
				$serverGroupID,'MailTransportID');

		# Check if we authorized for this mail transport
		if (smlib::attributes::attributeHasValue($callerTransportID,$mailboxAlias->{'TransportID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access mailboxAlias ID '$mailboxAliasID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::MailHosting::getMailboxAlias($serverGroupID,$mailboxAliasID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	# Build result
	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Address','string');
	$response->addField('Goto','string');
	$response->addField('AgentDisabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);

	return $response->export();
}


## @method getMailHostingServerGroups($serverGroupInfo,$search)
# Return mailhosting server groups
#
# @param serverGroupInfo Class info hash ref. This hash must be defined even if not used.
# @li AgentID - Optional AgentID to limit results to. If not defined, results are limited to the current 
# caller privileges
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Server group ID
# @li Description - Server description
sub getMailHostingServerGroups
{
	my (undef,$serverGroupInfo,$search) = @_;


	# Check if we have serverGroupinfo
	if (defined($serverGroupInfo)) {
		# Make sure we're a hash
		if (!isHash($serverGroupInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($serverGroupInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($serverGroupInfo->{'AgentID'} = isNumber($serverGroupInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $serverGroupData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($serverGroupInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($serverGroupInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'AgentID' has an invalid value");
			}

			$serverGroupData->{'AgentID'} = $serverGroupInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we don't have an agent ID, use the current one
		if (!defined($serverGroupInfo->{'AgentID'})) {
			$serverGroupInfo->{'AgentID'} = $callerAgentID;
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$serverGroupInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		$serverGroupData->{'AgentID'} = $serverGroupInfo->{'AgentID'};

	# Check if we're a client
	} elsif (Auth::userHasAuthority('Client')) {
		# Make sure we lock the results to this user
		$serverGroupData->{'UserID'} = Auth::getUserInfoItem('UserID');

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::MailHosting::getMailHostingServerGroups($serverGroupData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('Description','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



1;
# vim: ts=4
