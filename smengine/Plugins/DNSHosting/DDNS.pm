# SOAP interface for DDNS support in DNSHosting
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class DNSHosting::DDNS
# This is the main agent class, allowing control over agent services
package DNSHosting::DDNS;

use strict;

use Auth;

use smlib::DNSHosting::DDNS;
use smlib::Agent;

use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "DNSHosting: DDNS",
	Init => \&init,
};


## @internal
# Initialize module
sub init {
	my $server = shift;

	Auth::aclAdd('DNSHosting::DDNS','ping','DNSHosting/DDNS/Test');

	Auth::aclAdd('DNSHosting::DDNS','createDDNSUser','DNSHosting/DDNS/Users/Add');
	Auth::aclAdd('DNSHosting::DDNS','removeDDNSUser','DNSHosting/DDNS/Users/Remove');
	Auth::aclAdd('DNSHosting::DDNS','updateDDNSUser','DNSHosting/DDNS/Users/Update');
	Auth::aclAdd('DNSHosting::DDNS','getDDNSUsers','DNSHosting/DDNS/Users/List');
	Auth::aclAdd('DNSHosting::DDNS','getDDNSUser','DNSHosting/DDNS/Users/Get');

	Auth::aclAdd('DNSHosting::DDNS','getDDNSServerGroups','DNSHosting/DDNS/ServerGroups/List');

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method createDDNSUser($serverGroupID,$userInfo)
# Create a DNS user
#
# @param serverGroupID Server group ID
#
# @param userInfo User info hash
# @li AgentID - Agent ID
# @li Username - Username
# @li Password - Password
# @li Disabled - Disabled flag (NOT IMPLEMENTED)
# @li AgentRef - Optional, agent reference
#
# @return %DDNS user ID
sub createDDNSUser
{
	my (undef,$serverGroupID,$userInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' not defined");
	}
	if (!isHash($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' is not a HASH");
	}

	if (!defined($userInfo->{'AgentID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' not defined");
	}
	if (!defined($userInfo->{'AgentID'} = isNumber($userInfo->{'AgentID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' is invalid");
	}

	if (!defined($userInfo->{'Username'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' has no element 'Username'");
	}
	if (!isVariable($userInfo->{"Username"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Username' is invalid");
	}
	if ($userInfo->{"Username"} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Username' is blank");
	}
	if (!defined($userInfo->{'Username'} = isUsername($userInfo->{'Username'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Username' is not a valid username");
	}

	if (!defined($userInfo->{'Password'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' has no element 'Password'");
	}
	if (!isVariable($userInfo->{"Password"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Password' is invalid");
	}
	if ($userInfo->{"Password"} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Password' is blank");
	}

	if (defined($userInfo->{'AgentRef'})) {
		if (!isVariable($userInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentRef' is invalid");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Check if the agent ID is valid
		if (smlib::Agent::isAgentValid($userInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' has an invalid value");
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID 
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$userInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Build result
	my $userData;
	$userData->{'AgentID'} = $userInfo->{'AgentID'};
	$userData->{'Username'} = $userInfo->{'Username'};
	$userData->{'Password'} = $userInfo->{'Password'};
	$userData->{'AgentRef'} = $userInfo->{'AgentRef'} if (defined($userInfo->{'AgentRef'}));

	my $res = smlib::DNSHosting::DDNS::createDDNSUser($serverGroupID,$userData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','DDNSUserID');
}



## @method updateDDNSUser($serverGroupID,$userInfo)
# Update a %DDNS user
#
# @param serverGroupID Server group ID
#
# @param userInfo User info hash ref
# @li ID - %DDNS user ID
# @li AgentID - Agent ID
# @li Username - Optional username
# @li Password - Optional password, if "" then ignore
# @li AgentRef - Optional agent reference
#
# @return 0 on success or < 0 on error
sub updateDDNSUser
{
	my (undef,$serverGroupID,$userInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' not defined");
	}
	if (!isHash($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' is not a HASH");
	}

	if (!defined($userInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' has no element 'ID'");
	}
	if (!defined($userInfo->{'ID'} = isNumber($userInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'ID' is invalid");
	}

	if (defined($userInfo->{'AgentID'}) && !defined($userInfo->{'AgentID'} = isNumber($userInfo->{'AgentID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' is invalid");
	}

	if (defined($userInfo->{'Username'})) {
		if (!isVariable($userInfo->{"Username"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Username' is invalid");
		}
		if ($userInfo->{"Username"} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Username' is blank");
		}
		if (!defined($userInfo->{'Username'} = isUsername($userInfo->{'Username'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Username' is not a valid username");
		}
	}

	if (defined($userInfo->{'Password'})) {
		if (!isVariable($userInfo->{"Password"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Password' is invalid");
		}
		if ($userInfo->{"Password"} eq "") {
			$userInfo->{"Password"} = undef;
		}
	}
	
	if (defined($userInfo->{'AgentRef'})) {
		if (!isVariable($userInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentRef' is invalid");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Check if the agent ID is valid
		if (smlib::Agent::isAgentValid($userInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' has an invalid value");
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID 
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check this agent can access the user they specificed
		if (smlib::DNSHosting::DDNS::canAccessDDNSUser($callerAgentID,$userInfo->{'ID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS user ID '".$userInfo->{'ID'}."'");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the AgentID they specificed
		if (defined($userInfo->{'AgentID'})) {
			if (smlib::Agent::canAgentAccessAgent($callerAgentID,$userInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
			}
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;
	$i->{'ID'} = $userInfo->{'ID'};
	$i->{'AgentID'} = $userInfo->{'AgentID'} if (defined($userInfo->{'AgentID'}));
	$i->{'Username'} = $userInfo->{'Username'} if (defined($userInfo->{'Username'}));
	$i->{'Password'} = $userInfo->{'Password'} if (defined($userInfo->{'Password'}));
	$i->{'AgentRef'} = $userInfo->{'AgentRef'} if (defined($userInfo->{'AgentRef'}));

	# Do the update
	my $res = smlib::DNSHosting::DDNS::updateDDNSUser($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeDDNSUser($serverGroupID,$userID)
# Remove %DDNS user
#
# @param serverGroupID Server group ID
#
# @param userID %DDNS user ID
#
# @return 0 on success or < 0 on error
sub removeDDNSUser
{
	my (undef,$serverGroupID,$userID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' undefined");
	}
	if (!defined($userID = isNumber($userID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID 
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the user they specificed
		if (smlib::DNSHosting::DDNS::canAccessDDNSUser($callerAgentID,$userID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS user ID '$userID'");
		}


	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $res = smlib::DNSHosting::DDNS::removeDDNSUser($serverGroupID,$userID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method getDDNSUsers($serverGroupID,$userInfo,$search)
# Return a list of dynamic DNS users
#
# @param serverGroupID Server group ID
#
# @param userInfo User info hash ref
# @li AgentID - Limit returned results to this agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - %DDNS user ID
# @li AgentID - Agent ID
# @li AgentName - Agent Name
# @li Username - Username
# @li LastUpdate - Last time the user updated something
# @li AgentRef - Agent reference
sub getDDNSUsers
{
	my (undef,$serverGroupID,$userInfo,$search) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	# Check if we have userinfo
	if (defined($userInfo)) {
		# Make sure we're a hash
		if (!isHash($userInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($userInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($userInfo->{'AgentID'} = isNumber($userInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $userData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($userInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($userInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'AgentID' has an invalid value");
			}

			$userData->{'AgentID'} = $userInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we don't have an agent ID, use the current one
		if (!defined($userInfo->{'AgentID'})) {
			$userInfo->{'AgentID'} = $callerAgentID;
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$userInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		$userData->{'AgentID'} = $userInfo->{'AgentID'};


	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::DNSHosting::DDNS::getDDNSUsers($serverGroupID,$userData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('AgentID','int');
	$response->addField('AgentName','string');
	$response->addField('Username','string');
	$response->addField('LastUpdate','int');
	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}


## @method getDDNSUser($serverGroupID,$userID)
# Return a hash containing the info for this %DDNS user
#
# @param serverGroupID Server group ID
#
# @param userID %DDNS user ID
#
# @return Array ref of hash refs
# @li ID - Zone ID
# @li AgentID - Agent ID
# @li Username - Username
# @li AgentRef - Agent reference
sub getDDNSUser
{
	my (undef,$serverGroupID,$userID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' undefined");
	}
	if (!defined($userID = isNumber($userID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID 
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the user they specificed
		if (smlib::DNSHosting::DDNS::canAccessDDNSUser($callerAgentID,$userID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access DNS user ID '$userID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::DNSHosting::DDNS::getDDNSUser($serverGroupID,$userID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('AgentID','int');
	$response->addField('Username','string');
	$response->addField('LastUpdate','int');
	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);

	return $response->export();
}


## @method getDDNSServerGroups($serverGroupInfo,$search)
# Return ddns server groups
#
# @param serverGroupInfo Class info hash ref. This hash must be defined even if not used.
# @li AgentID - Optional AgentID to limit results to. If not defined, results are limited to the current 
# caller privileges
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li ID - Server group ID
# @li Description - Server description
sub getDDNSServerGroups
{
	my (undef,$serverGroupInfo,$search) = @_;


	# Check if we have serverGroupinfo
	if (defined($serverGroupInfo)) {
		# Make sure we're a hash
		if (!isHash($serverGroupInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($serverGroupInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($serverGroupInfo->{'AgentID'} = isNumber($serverGroupInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $serverGroupData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($serverGroupInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($serverGroupInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'AgentID' has an invalid value");
			}

			$serverGroupData->{'AgentID'} = $serverGroupInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we don't have an agent ID, use the current one
		if (!defined($serverGroupInfo->{'AgentID'})) {
			$serverGroupInfo->{'AgentID'} = $callerAgentID;
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$serverGroupInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		$serverGroupData->{'AgentID'} = $serverGroupInfo->{'AgentID'};

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}
	
	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::DNSHosting::DDNS::getDDNSServerGroups($serverGroupData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('Description','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}




1;
# vim: ts=4
