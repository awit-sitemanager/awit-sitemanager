# Audit Trails functions
# Copyright (C) 2011, AllWorldIT
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class AuditTrails
# This is the audit trails class
package AuditTrails;


use strict;

use Auth;

use smlib::AuditTrails;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "AuditTrails",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('AuditTrails','ping','AuditTrails/Test');
	
	Auth::aclAdd('AuditTrails','getAuditTrails','AuditTrails/List');
	Auth::aclAdd('AuditTrails','getAuditTrail','AuditTrails/Get');

	return RES_OK;
}




## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getAuditTrails($auditTrailsInfo,$search)
# Return a list of all the audit trails
#
# @param auditTrailsInfo Audit Trails info hash ref
# @li AgentID - Limit returned results to this agent ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li Timestamp - Audit trail timestamp
# @li ClientIP - Client's IP
# @li Protocol - Protocol used
# @li AgentID - Agent ID of the agent
# @li Username - Username used
# @li ServerCluster - Server cluster used
# @li ServerName - Server used
# @li Module - Module accessed
# @li Function - Function accessed
# @li Parameters - Function parameters
# @li Result - Function result
sub getAuditTrails
{
	my (undef,$auditTrailsInfo,$search) = @_;


	# Check if we have auditTrailsInfo
	if (defined($auditTrailsInfo)) {
		# Make sure we're a hash
		if (!isHash($auditTrailsInfo)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'auditTrailsInfo' is not a HASH");
		}
		# If we have an agent ID, use it
		if (defined($auditTrailsInfo->{'AgentID'})) {
			# If we do check if we have AgentID
			if (!defined($auditTrailsInfo->{'AgentID'} = isNumber($auditTrailsInfo->{'AgentID'}))) {
				return SOAPError(ERR_S_PARAM,"Parameter 'auditTrailsInfo' element 'AgentID' is invalid");
			}
		}
	}

	# Params
	my $auditTrailsData;

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {
		# Verify AgentID if we have it
		if (defined($auditTrailsInfo->{'AgentID'})) {
			# Check if the agent ID is valid
			if (smlib::Agent::isAgentValid($auditTrailsInfo->{'AgentID'}) != 1) {
				return SOAPError(ERR_S_PARAM,"Parameter 'auditTrailsInfo' element 'AgentID' has an invalid value");
			}

			$auditTrailsData->{'AgentID'} = $auditTrailsInfo->{'AgentID'};
		}

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# If we don't have an agent ID, use the current one
		if (!defined($auditTrailsInfo->{'AgentID'})) {
			$auditTrailsInfo->{'AgentID'} = $callerAgentID;
		}

		# Check this agent can access the AgentID they specificed
		if (smlib::Agent::canAgentAccessAgent($callerAgentID,$auditTrailsInfo->{'AgentID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'AgentID'");
		}

		$auditTrailsData->{'AgentID'} = $auditTrailsInfo->{'AgentID'};

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::AuditTrails::getAuditTrails($auditTrailsData,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Timestamp','string');
	$response->addField('ClientIP','string');
	$response->addField('Protocol','string');
	$response->addField('AgentID','string');
	$response->addField('Username','string');
	$response->addField('ServerCluster','string');
	$response->addField('ServerName','string');
	$response->addField('Module','string');
	$response->addField('Function','string');
	$response->addField('Parameters','string');
	$response->addField('Result','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getAuditTrail($auditTrailID)
# Return a hash containing the info for this audit trail
#
# @param auditTrailID Audit Trail ID
#
# @return Audit Trail hash ref
# @li ID - Audit Trail ID
# @li Timestamp - Audit trail timestamp
# @li ClientIP - Client's IP
# @li Protocol - Protocol used
# @li AgentID - Agent ID of the agent
# @li Username - Username used
# @li ServerCluster - Server cluster used
# @li ServerName - Server used
# @li Module - Module accessed
# @li Function - Function accessed
# @li Parameters - Function parameters
# @li Result - Function result
sub getAuditTrail
{
	my (undef,$auditTrailID) = @_;


	# Check params
	if (!defined($auditTrailID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'auditTrailID' undefined");
	}
	if (!defined($auditTrailID = isNumber($auditTrailID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'auditTrailID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check this agent can access the audit trail they specificed
		if (smlib::AuditTrails::canAccessAuditTrail($callerAgentID,$auditTrailID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access Audit Trail ID '$auditTrailID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::AuditTrails::getAuditTrail($auditTrailID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Timestamp','string');
	$response->addField('ClientIP','string');
	$response->addField('Protocol','string');
	$response->addField('AgentID','string');
	$response->addField('Username','string');
	$response->addField('ServerCluster','string');
	$response->addField('ServerName','string');
	$response->addField('Module','string');
	$response->addField('Function','string');
	$response->addField('Parameters','string');
	$response->addField('Result','string');
	$response->parseHashRef($rawData);

	return $response->export();
}


1;
# vim: ts=4
