# Radius user port locking SOAP interface
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class Radius::PortLocking
# This is the radius user port locking handling class
package Radius::PortLocking;

use strict;

use Auth;

use smlib::Radius::PortLocking;

use smlib::constants;
use smlib::soap;
use smlib::util;


# Plugin info
our $pluginInfo = {
	Name => "Radius",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Radius::PortLocking','ping','Radius/Users/PortLocks/Test');

	Auth::aclAdd('Radius::PortLocking','createRadiusUserPortLock','Radius/Users/PortLocks/Add');
	Auth::aclAdd('Radius::PortLocking','updateRadiusUserPortLock','Radius/Users/PortLocks/Update');
	Auth::aclAdd('Radius::PortLocking','removeRadiusUserPortLock','Radius/Users/PortLocks/Remove');
	Auth::aclAdd('Radius::PortLocking','getRadiusUserPortLocks','Radius/Users/PortLocks/List');
	Auth::aclAdd('Radius::PortLocking','getRadiusUserPortLock','Radius/Users/PortLocks/Get');

	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping {
	return 0;
}



## @method createRadiusUserPortLock($serverGroupID,$portLockInfo)
# Add a radius port lock
#
# @param serverGroupID Server group ID
#
# @param portLockInfo Port lock info hash ref
# @li UserID - Radius user ID
# @li NASPort - NAS port
# @li AgentRef - Optional agent reference
# @li AgentDisabled - Optional agent disabled flag
#
# @return Returns radius user port lock ID
sub createRadiusUserPortLock
{
	my (undef,$serverGroupID,$portLockInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($portLockInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' not defined");
	}
	if (!isHash($portLockInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' is not a HASH");
	}

	if (!defined($portLockInfo->{'UserID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' has no element 'UserID' defined");
	}
	if (!defined($portLockInfo->{'UserID'} = isNumber($portLockInfo->{'UserID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'UserID' is invalid");
	}

	if (!defined($portLockInfo->{'NASPort'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' has no element 'NASPort' defined");
	}
	if (!isVariable($portLockInfo->{"NASPort"})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'NASPort' is invalid");
	}
	if ($portLockInfo->{'NASPort'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'NASPort' is invalid");
	}
	
	if (defined($portLockInfo->{'AgentRef'})) {
		if (!isVariable($portLockInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'AgentRef' is invalid");
		}
	}

	# Check AgentDisabled
	if (defined($portLockInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($portLockInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'AgentDisabled' is invalid");
		}
		if (!defined($portLockInfo->{'AgentDisabled'} = isBoolean($portLockInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check this agent can access the user they specificed
		if (smlib::Radius::canAccessRadiusUser($callerAgentID,$portLockInfo->{'UserID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius user ID '".$portLockInfo->{'UserID'}."'");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;

	$i->{'UserID'} = $portLockInfo->{'UserID'};
	$i->{'NASPort'} = $portLockInfo->{'NASPort'};
	$i->{'AgentRef'} = $portLockInfo->{'AgentRef'} if (defined($portLockInfo->{'AgentRef'}));
	$i->{'AgentDisabled'} = $portLockInfo->{'AgentDisabled'} if (defined($portLockInfo->{'AgentDisabled'}));

	my $res = smlib::Radius::PortLocking::createRadiusUserPortLock($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,smlib::Radius::PortLocking::Error());
	}

	return SOAPSuccess($res,'int','RadiusUserPortLockID');
}



## @method updateRadiusUserPortLock($serverGroupID,$portLockInfo)
# Update a radius user port lock
#
# @param serverGroupID Server group ID
#
# @param portLockInfo Port locking info hash ref
# @li ID - Radius port lock ID
# @li UserID - Radius user ID
# @li NASPort - NAS port
# @li AgentDisabled - Optional flag agent can set to disable this radius user
# @li AgentRef - Optional agent reference
#
# @return 0 on success or < 0 on error
sub updateRadiusUserPortLock
{
	my (undef,$serverGroupID,$portLockInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($portLockInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' undefined");
	}
	if (!isHash($portLockInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' is not a HASH");
	}
	if (!defined($portLockInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' has no element 'ID' defined");
	}
	if (!defined($portLockInfo->{'ID'} = isNumber($portLockInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'ID' is invalid");
	}

	if (defined($portLockInfo->{'UserID'})) {
		if (!isVariable($portLockInfo->{'UserID'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'UserID' is invalid");
		}

		if (!defined($portLockInfo->{'UserID'} = isNumber($portLockInfo->{'UserID'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'UserID' is invalid");
		}
	}

	if (defined($portLockInfo->{'NASPort'})) {
		if (!isVariable($portLockInfo->{"NASPort"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'NASPort' is invalid");
		}
		if ($portLockInfo->{'NASPort'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'NASPort' is invalid");
		}
	}

	if (defined($portLockInfo->{'AgentRef'})) {
		if (!isVariable($portLockInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'AgentRef' is invalid");
		}
	}

	# Check AgentDisabled
	if (defined($portLockInfo->{'AgentDisabled'})) {
		# Check for non-variable
		if (!isVariable($portLockInfo->{'AgentDisabled'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'AgentDisabled' is invalid");
		}
		if (!defined($portLockInfo->{'AgentDisabled'} = isBoolean($portLockInfo->{'AgentDisabled'}))) {
			return SOAPError(ERR_S_PARAM,"Parameter 'portLockInfo' element 'AgentDisabled' has an invalid value");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID and its valid
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Grab user ID from radius port lock ID
		my $origUserID = smlib::Radius::PortLocking::getRadiusUserIDFromPortLockID($portLockInfo->{'ID'});
		if ($origUserID < 0) {
			return SOAPError($origUserID,smlib::Radius::PortLocking::Error());
		} elsif ($origUserID == 0) {
			return SOAPError(ERR_UNKNOWN,"Failed to determine radius user from port lock ID '".$portLockInfo->{'ID'}."'");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the original radius user
		if (smlib::Radius::canAccessRadiusUser($callerAgentID,$origUserID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access original radius user ID '$origUserID'");
		}

		if (defined($portLockInfo->{'UserID'})) {
			if (smlib::Radius::canAccessRadiusUser($callerAgentID,$portLockInfo->{'UserID'}) != 1) {
				return SOAPError(ERR_NOACCESS,"Insufficient privileges to access new radius user ID '".
						$portLockInfo->{'UserID'}."'");
			}
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}


	my $i;

	$i->{'ID'} = $portLockInfo->{'ID'};

	$i->{'UserID'} = $portLockInfo->{'UserID'} if (defined($portLockInfo->{'UserID'}));
	$i->{'NASPort'} = $portLockInfo->{'NASPort'} if (defined($portLockInfo->{'NASPort'}));
	$i->{'AgentRef'} = $portLockInfo->{'AgentRef'} if (defined($portLockInfo->{'AgentRef'}));
	$i->{'AgentDisabled'} = $portLockInfo->{'AgentDisabled'} if (defined($portLockInfo->{'AgentDisabled'}));

	my $res = smlib::Radius::PortLocking::updateRadiusUserPortLock($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,smlib::Radius::PortLocking::Error());
	}

	return SOAPSuccess();
}



## @method removeRadiusUserPortLock($serverGroupID,$portLockID)
# Remove radius user port lock
#
# @param serverGroupID Server group ID
#
# @param portLockID Radius user port lock ID
#
# @return 0 on success or < 0 on error
sub removeRadiusUserPortLock
{
	my (undef,$serverGroupID,$portLockID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($portLockID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockID' undefined");
	}
	if (!defined($portLockID = isNumber($portLockID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Grab user ID from radius port lock ID
		my $userID = smlib::Radius::PortLocking::getRadiusUserIDFromPortLockID($portLockID);
		if ($userID < 0) {
			return SOAPError($userID,smlib::Radius::PortLocking::Error());
		} elsif ($userID == 0) {
			return SOAPError(ERR_UNKNOWN,"Failed to determine radius user from port lock ID '$portLockID'");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the radius user they specificed
		if (smlib::Radius::canAccessRadiusUser($callerAgentID,$userID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access port lock ID '$portLockID'");
		}


	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $res = smlib::Radius::PortLocking::removeRadiusUserPortLock($serverGroupID,$portLockID);
	if ($res < 0) {
		return SOAPError($res,smlib::Radius::PortLocking::Error());
	}

	return SOAPSuccess();
}



## @method getRadiusUserPortLocks($serverGroupID,$userID,$search)
# Return radius user port locks
#
# @param serverGroupID Server group ID
#
# @param userID Radius user ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash table refs
# @li ID - Radius port lock ID
# @li NASPort - NAS port
# @li AgentRef - Optional reference for the agent
# @li AgentDisabled - Optional agent disabled flag
sub getRadiusUserPortLocks
{
	my (undef,$serverGroupID,$userID,$search) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'UserID' undefined");
	}
	if (!defined($userID = isNumber($userID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'UserID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the zone entry they specificed
		if (smlib::Radius::canAccessRadiusUser($callerAgentID,$userID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius user ID '$userID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;
	$i->{'UserID'} = $userID;

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Radius::PortLocking::getRadiusUserPortLocks($serverGroupID,$i,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,smlib::Radius::PortLocking::Error());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('UserID','int');
	$response->addField('NASPort','string');
	$response->addField('AgentDisabled','boolean');
	$response->addField('Disabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getRadiusUserPortLock($serverGroupID,$portLockID)
# Get radius user port lock information
#
# @param serverGroupID Server group ID
#
# @param portLockID Radius user port lock ID
#
# @return Hash ref
# @li ID - Radius user port lock ID
# @li NASPort - NAS port
# @li AgentRef - Optional reference for the agent
# @li AgentDisabled - Optional agent disabled flag
sub getRadiusUserPortLock
{
	my (undef,$serverGroupID,$portLockID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($portLockID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockID' undefined");
	}
	if (!defined($portLockID = isNumber($portLockID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'portLockID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Grab user ID from radius port lock ID
		my $userID = smlib::Radius::PortLocking::getRadiusUserIDFromPortLockID($portLockID);
		if ($userID < 0) {
			return SOAPError($userID,smlib::Radius::PortLocking::Error());
		} elsif ($userID == 0) {
			return SOAPError(ERR_UNKNOWN,"Failed to determine radius user from port lock ID '$portLockID'");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the radius user this port lock is for
		if (smlib::Radius::canAccessRadiusUser($callerAgentID,$userID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access port lock ID '$portLockID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::Radius::PortLocking::getRadiusUserPortLock($serverGroupID,$portLockID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,smlib::MailHosting::Error());
	}

	# Build result
	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('UserID','int');
	$response->addField('NASPort','string');
	$response->addField('AgentDisabled','boolean');
	$response->addField('Disabled','boolean');
	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



1;
# vim: ts=4
