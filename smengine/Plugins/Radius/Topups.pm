# Radius user topups SOAP interface
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class Radius::Topups
# This is the radius user topup handling class
package Radius::Topups;

use strict;

use Auth;

use smlib::Radius::Topups;

use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;


# Plugin info
our $pluginInfo = {
	Name => "Radius: Topups",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Radius::Topups','ping','Radius/Users/Topups/Test');

	Auth::aclAdd('Radius::Topups','createRadiusUserTopup','Radius/Users/Topups/Add');
	Auth::aclAdd('Radius::Topups','updateRadiusUserTopup','Radius/Users/Topups/Update');
	Auth::aclAdd('Radius::Topups','removeRadiusUserTopup','Radius/Users/Topups/Remove');

	Auth::aclAdd('Radius::Topups','getRadiusUserTopups','Radius/Users/Topups/List');
	Auth::aclAdd('Radius::Topups','getRadiusUserTopupsSummary','Radius/Users/Topups/List');

	Auth::aclAdd('Radius::Topups','getRadiusUserTopup','Radius/Users/Topups/Get');

	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping {
	return 0;
}



## @method createRadiusUserTopup($serverGroupID,$topupInfo)
# Create a radius user topup
#
# @param serverGroupID Server group ID
#
# @param topupInfo Topup info hash ref
# @li UserID - Radius user ID
# @li Bandwidth - Topup bandwidth in Mbyte
# @li ValidFrom - Valid from date in format of YYYY-MM-DD
# @li ValidTo - Valid to date in format of YYYY-MM-DD
# @li AgentRef - Optional agent reference
#
# @return Returns radius user topup ID
sub createRadiusUserTopup {
	my (undef,$serverGroupID,$topupInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($topupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' not defined");
	}
	if (!isHash($topupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' is not a HASH");
	}

	if (!defined($topupInfo->{'UserID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' has no element 'UserID' defined");
	}
	if (!defined($topupInfo->{'UserID'} = isNumber($topupInfo->{'UserID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'UserID' is invalid");
	}

	if (!defined($topupInfo->{'Bandwidth'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' has no element 'Bandwidth' defined");
	}
	if (!defined($topupInfo->{'Bandwidth'} = isNumber($topupInfo->{'Bandwidth'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'Bandwidth' is invalid");
	}

	if (!defined($topupInfo->{'ValidFrom'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ValidFrom' is not defined");
	}
	if (!isVariable($topupInfo->{'ValidFrom'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ValidFrom' is invalid");
	}
	if (!($topupInfo->{'ValidFrom'} =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ValidFrom' is not an ISO standard date 'YYYY-MM-DD'");
	}

	if (!defined($topupInfo->{'ValidTo'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ValidTo' is not defined");
	}
	if (!isVariable($topupInfo->{'ValidTo'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ValidTo' is invalid");
	}
	if (!($topupInfo->{'ValidTo'} =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ValidTo' is not an ISO standard date 'YYYY-MM-DD'");
	}

	if (defined($topupInfo->{'AgentRef'})) {
		if (!isVariable($topupInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'AgentRef' is invalid");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the user they specificed
		if (smlib::Radius::canAccessRadiusUser($callerAgentID,$topupInfo->{'UserID'}) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius user ID '".$topupInfo->{'UserID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;

	$i->{'UserID'} = $topupInfo->{'UserID'};
	$i->{'Bandwidth'} = $topupInfo->{'Bandwidth'};
	$i->{'ValidFrom'} = $topupInfo->{'ValidFrom'};
	$i->{'ValidTo'} = $topupInfo->{'ValidTo'};
	$i->{'AgentRef'} = $topupInfo->{'AgentRef'} if (defined($topupInfo->{'AgentRef'}));

	my $res = smlib::Radius::Topups::createRadiusUserTopup($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,smlib::Radius::Topups::Error());
	}

	return SOAPSuccess($res,'int','RadiusUserTopupID');
}



## @method updateRadiusUserTopup($serverGroupID,$topupInfo)
# Update a radius user port lock
#
# @param serverGroupID Server group ID
#
# @param topupInfo Topup info hash ref
# @li ID - Topup ID
# @li UserID - Radius user ID
# @li Bandwidth - Optional topup bandwidth in Mbyte
# @li ValidFrom - Optional from date in format of YYYY-MM-DD
# @li ValidTo - Optional to date in format of YYYY-MM-DD
# @li AgentRef - Optional agent reference
#
# @return 0 on success or < 0 on error
sub updateRadiusUserTopup {
	my (undef,$serverGroupID,$topupInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($topupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' undefined");
	}
	if (!isHash($topupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' is not a HASH");
	}

	if (!defined($topupInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' has no element 'ID' defined");
	}
	if (!defined($topupInfo->{'ID'} = isNumber($topupInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ID' is invalid");
	}

	if (defined($topupInfo->{'UserID'}) && !defined($topupInfo->{'UserID'} = isNumber($topupInfo->{'UserID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'UserID' is invalid");
	}

	if (defined($topupInfo->{'Bandwidth'}) && !defined($topupInfo->{'Bandwidth'} = isNumber($topupInfo->{'Bandwidth'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'Bandwidth' is invalid");
	}

	if (defined($topupInfo->{'ValidFrom'})) {
		if (!isVariable($topupInfo->{'ValidFrom'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ValidFrom' is invalid");
		}
		if (!($topupInfo->{'ValidFrom'} =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ValidFrom' is not an ISO standard date 'YYYY-MM-DD'");
		}
	}

	if (defined($topupInfo->{'ValidTo'})) {
		if (!isVariable($topupInfo->{'ValidTo'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ValidTo' is invalid");
		}
		if (!($topupInfo->{'ValidTo'} =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'ValidTo' is not an ISO standard date 'YYYY-MM-DD'");
		}
	}

	if (defined($topupInfo->{'AgentRef'})) {
		if (!isVariable($topupInfo->{"AgentRef"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'topupInfo' element 'AgentRef' is invalid");
		}
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID and its valid
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Grab user ID from radius topup ID
		my $origUserID = smlib::Radius::Topups::getRadiusUserIDFromTopupID($topupInfo->{'ID'});
		if ($origUserID < 0) {
			return SOAPError($origUserID,smlib::Radius::Topups::Error());
		} elsif ($origUserID == 0) {
			return SOAPError(ERR_UNKNOWN,"Failed to determine radius user from port lock ID '".$topupInfo->{'ID'}."'");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the original radius user
		if (smlib::Radius::canAccessRadiusUser($callerAgentID,$origUserID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access original radius user ID '$origUserID'");
		}

		if (defined($topupInfo->{'UserID'})) {
			if (smlib::Radius::canAccessRadiusUser($callerAgentID,$topupInfo->{'UserID'}) != 1) {
				return SOAPError(ERR_NOACCESS,"Insufficient privileges to access new radius user ID '".
						$topupInfo->{'UserID'}."'");
			}
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}


	my $i;

	$i->{'ID'} = $topupInfo->{'ID'};

	$i->{'UserID'} = $topupInfo->{'UserID'} if (defined($topupInfo->{'UserID'}));
	$i->{'Bandwidth'} = $topupInfo->{'Bandwidth'} if (defined($topupInfo->{'Bandwidth'}));
	$i->{'ValidFrom'} = $topupInfo->{'ValidFrom'} if (defined($topupInfo->{'ValidFrom'}));
	$i->{'ValidTo'} = $topupInfo->{'ValidTo'} if (defined($topupInfo->{'ValidTo'}));

	$i->{'AgentRef'} = $topupInfo->{'AgentRef'} if (defined($topupInfo->{'AgentRef'}));

	my $res = smlib::Radius::Topups::updateRadiusUserTopup($serverGroupID,$i);
	if ($res < 0) {
		return SOAPError($res,smlib::Radius::Topups::Error());
	}

	return SOAPSuccess();
}



## @method removeRadiusUserTopup($serverGroupID,$topupID)
# Remove radius user topup
#
# @param serverGroupID Server group ID
#
# @param topupID Radius user topup ID
#
# @return 0 on success or < 0 on error
sub removeRadiusUserTopup
{
	my (undef,$serverGroupID,$topupID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($topupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupID' undefined");
	}
	if (!defined($topupID = isNumber($topupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the topup they specificed
		if (smlib::Radius::Topups::canAccessRadiusUserTopup($callerAgentID,$topupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius topup ID '$topupID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Remove topup
	my $res = smlib::Radius::Topups::removeRadiusUserTopup($serverGroupID,$topupID);
	if ($res < 0) {
		return SOAPError($res,smlib::Radius::Topups::Error());
	}

	return SOAPSuccess();
}



## @method getRadiusUserTopups($serverGroupID,$userID,$search)
# Return radius user topups
#
# @param serverGroupID Server group ID
#
# @param userID Radius user ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash table refs
# @li ID - Radius port lock ID
# @li UserID - Radius user ID
# @li Bandwidth - Topup bandwidth in Mbyte
# @li ValidFrom - Valid from date in format of YYYY-MM-DD
# @li ValidTo - Valid to date in format of YYYY-MM-DD
# @li AgentRef - Optional agent reference
sub getRadiusUserTopups
{
	my (undef,$serverGroupID,$userID,$search) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($userID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'UserID' undefined");
	}
	if (!defined($userID = isNumber($userID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'UserID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the radius user specificed
		if (smlib::Radius::canAccessRadiusUser($callerAgentID,$userID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius user ID '$userID'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	my $i;
	$i->{'UserID'} = $userID;

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Radius::Topups::getRadiusUserTopups($serverGroupID,$i,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('UserID','int');
	$response->addField('Bandwidth','int');
	$response->addField('Timestamp','string');
	$response->addField('ValidFrom','string');
	$response->addField('ValidTo','string');
	$response->addField('AgentRef','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getRadiusUserTopup($serverGroupID,$topupID)
# Get radius user topup information
#
# @param serverGroupID Server group ID
#
# @param topupID Radius user topup ID
#
# @return Hash ref
# @li ID - Radius topup ID
# @li UserID - Radius user ID
# @li Bandwidth - Topup bandwidth in Mbyte
# @li Timestamp - Timestamp topup was added
# @li ValidFrom - Date topup is valid from in YYYY-MM-DD
# @li ValidTo - Date topup is valid to in YYYY-MM-DD
# @li AgentRef - Reference for agent
sub getRadiusUserTopup
{
	my (undef,$serverGroupID,$topupID) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (!defined($topupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupID' undefined");
	}
	if (!defined($topupID = isNumber($topupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupID' is invalid");
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check this agent can access the topup they specificed
		if (smlib::Radius::Topups::canAccessRadiusUserTopup($callerAgentID,$topupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius topup ID '$topupID'");
		}


	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Grab and sanitize data
	my $rawData = smlib::Radius::Topups::getRadiusUserTopup($serverGroupID,$topupID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,smlib::Radius::Topups::Error());
	}

	# Build result
	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('UserID','int');
	$response->addField('Bandwidth','int');
	$response->addField('Timestamp','string');
	$response->addField('ValidFrom','string');
	$response->addField('ValidTo','string');
	$response->addField('AgentRef','string');
	$response->parseHashRef($rawData);

	return $response->export();
}


## @method getRadiusUserTopupsSummary($serverGroupID,$topupsInfo)
# Return radius user topups summary
#
# @param serverGroupID Server group ID
#
# @param topupsInfo Topups info hash ref
# @li UserID - Radius user ID
# @li Period - User topups period
#
# @return Array ref of hash refs
# @li TopupTotal - Total topups added for Period
sub getRadiusUserTopupsSummary
{
	my (undef,$serverGroupID,$topupsInfo) = @_;


	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}
	if (!defined($topupsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupsInfo' undefined");
	}
	if (!isHash($topupsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupsInfo' is not a HASH");
	}
	if (!defined($topupsInfo->{'UserID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupsInfo' element 'UserID' undefined");
	}
	if (!defined($topupsInfo->{'UserID'} = isNumber($topupsInfo->{'UserID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'topupsInfo' element 'UserID' is invalid");
	}
	# Set year and periodMonth from period
	if (defined($topupsInfo->{'Period'})) {
		my ($periodYear,$periodMonth) = isDate($topupsInfo->{'Period'},ISDATE_YEAR | ISDATE_MONTH);
		# If periodYear is not defined, then we failed
		if (!defined($periodYear)) {
			return SOAPError(ERR_S_PARAM,"Parameter 'topupsInfo' element 'Period' is invalid");
		}
		$topupsInfo->{'Period'} = "$periodYear-$periodMonth";
	}

	# Check if we're an admin or not...
	if (Auth::userHasAuthority('Admin')) {

	# Check if we're an agent
	} elsif (Auth::userHasAuthority('Agent')) {
		# Grab the callers agent ID
		my $callerAgentID = Auth::getUserAttribute('AgentID');

		# Make sure we have an agent ID
		if (!defined($callerAgentID)) {
			return SOAPError(ERR_UNKNOWN,"No 'AgentID' linked to your profile");
		}

		# Check if we authorized for this server group
		if (smlib::servers::canAccessServerGroup($callerAgentID,$serverGroupID) != 1) {
			return SOAPError(ERR_NOACCESS,"Access denied to use specified 'ServerGroupID'");
		}

		# Check if we authorized for this radius user
		my $res = smlib::Radius::canAccessRadiusUser($callerAgentID,$topupsInfo->{'UserID'});
		# Check return code
		if ($res < 0) {
			return SOAPError($res,getError());
		} elsif ($res == 0) {
			return SOAPError(ERR_NOACCESS,"Insufficient privileges to access radius user ID '".$topupsInfo->{'UserID'}."'");
		}

	# Fallthrough
	} else {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Send only the info we need
	my $i;
	$i->{'UserID'} = $topupsInfo->{'UserID'};
	$i->{'Period'} = $topupsInfo->{'Period'};

	# Grab and sanitize data
	my ($rawTopupSummaryData,$numResults) = smlib::Radius::Topups::getRadiusUserTopupsSummary($serverGroupID,$i);
	if (ref($rawTopupSummaryData) ne "HASH") {
		return SOAPError($rawTopupSummaryData,getError());
	}

	my $result;
	$result->{'TopupTotal'} = $rawTopupSummaryData->{'TopupTotal'};

	# Build response
	my $response = smlib::soap::response->new();
	$response->addField('TopupTotal','string');
	$response->parseHashRef($result);

	return $response->export();
}


1;
# vim: ts=4
