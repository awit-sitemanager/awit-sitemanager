# Agent SOAP interface
# Copyright (C) 2008, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class Agent
# This is the main agent class, allowing control over agent services
package Agent;

use strict;
# Set library directory
use lib qw(../);
use Auth;

use smlib::agents qw();
use smlib::sites qw();
use smlib::mail qw();
use smlib::zones qw();

use smlib::constants;
use smlib::soap;



# Plugin info
our $pluginInfo = {
	Name => "Agent",
	Init => \&init,
};


## @internal
# Initialize module
sub init {
	my $server = shift;

	Auth::aclAdd('Agent','ping','Agent/Test');

	Auth::aclAdd('Agent','getAgentDetails','Agent/Get');

	Auth::aclAdd('Agent','getSites','Agent/Sites/Get');

	Auth::aclAdd('Agent','getMailTransports','Agent/MailTransports/List');
	Auth::aclAdd('Agent','getMailTransportInfo','Agent/MailTransports/Get');
	Auth::aclAdd('Agent','updateMailTransport','Agent/MailTransports/Update');

	Auth::aclAdd('Agent','getMailboxes','Agent/Mailboxes/List');
	Auth::aclAdd('Agent','getMailPolicies','Agent/MailPolicies/List');
	Auth::aclAdd('Agent','createMailbox','Agent/Mailboxes/Add');
	Auth::aclAdd('Agent','updateMailbox','Agent/Mailboxes/Update');
	Auth::aclAdd('Agent','removeMailbox','Agent/Mailboxes/Remove');
	Auth::aclAdd('Agent','getMailboxInfo','Agent/Mailboxes/Get');

	Auth::aclAdd('Agent','getMailboxAliases','Agent/MailboxAliases/List');
	Auth::aclAdd('Agent','getMailboxAliasInfo','Agent/MailboxAliases/Get');
	Auth::aclAdd('Agent','updateMailboxAlias','Agent/MailboxAliases/Update');
	Auth::aclAdd('Agent','removeMailboxAlias','Agent/MailboxAliases/Remove');
	Auth::aclAdd('Agent','createMailboxAlias','Agent/MailboxAliases/Add');

	Auth::aclAdd('Agent','getZones','Agent/Zones/List');
	
	Auth::aclAdd('Agent','getRadiusUserByUsername','Agent/Radius/Get');
	Auth::aclAdd('Agent','getRadiusUser','Agent/Radius/Get');
	Auth::aclAdd('Agent','getRadiusUsers','Agent/Radius/List');
	Auth::aclAdd('Agent','createRadiusUser','Agent/Radius/Add');
	Auth::aclAdd('Agent','updateRadiusUser','Agent/Radius/Update');
	Auth::aclAdd('Agent','removeRadiusUser','Agent/Radius/Remove');

	Auth::aclAdd('Agent','getRadiusClasses','Agent/RadiusClasses/List');

	Auth::aclAdd('Agent','getRadiusUserTopups','Agent/RadiusTopups/List');
	Auth::aclAdd('Agent','createRadiusUserTopup','Agent/RadiusTopups/Add');
	Auth::aclAdd('Agent','getRadiusUserCurrentTopups','Agent/RadiusTopups/Get');
	Auth::aclAdd('Agent','getRadiusUserCurrentUsage','Agent/RadiusLogs/Get');

	Auth::aclAdd('Agent','getRadiusUserPortLocks','Agent/RadiusPortLocks/List');
	Auth::aclAdd('Agent','createRadiusUserPortLock','Agent/RadiusPortLocks/Add');
	Auth::aclAdd('Agent','removeRadiusUserPortLock','Agent/RadiusPortLocks/Remove');

	Auth::aclAdd('Agent','getRadiusUserLogs','Agent/RadiusLogs/List');
}






## @method ping
# Ping function to allow one to see if they get a positive response from 
# this module
#
# @return Will always return 0
sub ping {
	return 0;
}


## @method getAgentDetails
# Return agent details of currently logged in user
#
# @return HashTable ref
# @li AgentName - %Agent name
# @li SiteQuota - Site quota
# @li MailboxQuota - %Mailbox quota
# @li SiteQuotaUsage - Site quota Usage
# @li MailboxQuotaUsage - %Mailbox quota Usage
# @li MinSiteSize - Minimum site size
# @li MinMailboxSize - Minimum mailbox size
sub getAgentDetails {
	my $authInfo = Auth::sessionGetData();

	# Grab and sanitize data
	my $rawAgentData = smlib::agents::getAgentRecord($authInfo->{'AgentID'});
	if (ref $rawAgentData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawAgentData,smlib::agents::Error());
	}

	my $rawAgentQuota  = smlib::agents::getAgentQuotaBreakdown($authInfo->{'AgentID'});
	if (ref $rawAgentQuota ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawAgentQuota,smlib::agents::Error());
	}

	# Build result
	my $agentData;
	$agentData->{'Name'} = $rawAgentData->{'Name'}; 
	$agentData->{'SiteQuota'} = $rawAgentData->{'SiteQuota'}; 
	$agentData->{'MailboxQuota'} = $rawAgentData->{'MailboxQuota'}; 

	$agentData->{'SiteQuotaUsage'} = $rawAgentQuota->{'SiteQuotaAllocated'}; 
	$agentData->{'MailboxQuotaUsage'} = $rawAgentQuota->{'MailboxQuotaAllocated'}; 

	$agentData->{'MinSiteSize'} = $rawAgentData->{'MinSiteSize'}; 
	$agentData->{'MinMailboxSize'} = $rawAgentData->{'MinMailboxSize'}; 


	return SOAPResponse(RES_OK,$agentData);
}


## @method getSites
# Return a list of all the websites the agent has
#
# @return Array ref of hash tables refs
# @li ID - Website ID on the system
# @li DomainName - Domain name of website
# @li DiskQuota - Website disk quota allowance
# @li DiskUsage - Current disk space usage
sub getSites {
	my $authInfo = Auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = smlib::sites::getSites($authInfo->{'AgentID'});
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::sites::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'DomainName'} = $item->{'DomainName'};
		$tmpItem->{'DiskQuota'} = $item->{'DiskQuota'};
		$tmpItem->{'DiskUsage'} = $item->{'DiskUsage'};

		push(@data,$tmpItem);
	} 

	return SOAPResponse(RES_OK,\@data);
}


## @method getMailTransports($search)
# Return a list of all the mail transport an agent has
#
# @param search Optional hash ref of search criteria
# @li searchDomainName - Domain name search criteria
# @li searchAgentRef - %Agent referent search criteria
# @li searchOrderBy - Order by this field
#
# @return Array ref of hash refs
# @li ID - Transport ID on the system
# @li DomainName - Domain name of transport
# @li Transport - Transport for this domain
# @li Detail - Detail for this transport
# @li AgentDisabled - Set if the agent has disabled this transport
# @li DisableDelivery - Set if delivery to this domain is disabled
# @li AgentRef - %Agent reference
sub getMailTransports {
	my (undef,$search) = @_;

	my $authInfo = Auth::sessionGetData();

	# Pull in search criteria
	my $backendInfo;
	# Check if we defined
	if (defined($search)) {
		$backendInfo->{'searchDomainName'} = $search->{'searchDomainName'};
		$backendInfo->{'searchAgentRef'} = $search->{'searchAgentRef'};
		$backendInfo->{'searchOrderBy'} = $search->{'searchOrderBy'};
	}

	# Grab and sanitize data
	my $rawData = smlib::mail::getMailTransports($authInfo->{'AgentID'},$backendInfo);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::mail::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'DomainName'} = SOAP_Param_toStr($item->{'DomainName'});
		$tmpItem->{'Transport'} = SOAP_Param_toStr($item->{'Transport'});
		$tmpItem->{'Detail'} = SOAP_Param_toStr($item->{'Detail'});
		$tmpItem->{'AgentDisabled'} = $item->{'AgentDisabled'};
		$tmpItem->{'DisableDelivery'} = $item->{'DisableDelivery'};
		$tmpItem->{'AgentRef'} = SOAP_Param_toStr($item->{'AgentRef'});

		push(@data,$tmpItem);
	}


	return SOAPResponse(RES_OK,\@data);
}


## @method getMailTransportInfo($transportID)
# Return a hash containing the info for this mail transport
#
# @param transportID Transport ID
#
# @return Array ref of hash refs
# @li ID - Transport ID on the system
# @li DomainName - Domain name of transport
# @li Transport - Transport for this domain
# @li Detail - Detail for this transport
# @li TransportDetail - Transport for this domain
# @li PolicyID - Policy for this transport
# @li AgentDisabled - Set if the agent has disabled this transport
# @li DisableDelivery - Set if delivery to this domain is disabled
# @li AgentRef - %Agent reference
sub getMailTransportInfo {
	my (undef,$transportID) = @_;

	# Check params
	if (!defined($transportID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'transportID' undefined");
	}
	if ($transportID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'transportID' invalid");
	}

	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this transport
	my $res = smlib::mail::canAccessMailTransport($authInfo->{'AgentID'},$transportID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
	}


	# Grab and sanitize data
	my $rawData = smlib::mail::getMailTransportRecord($transportID);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::mail::Error());
	}

	# Build result
	my %tmpItem;

	$tmpItem{'ID'} = $rawData->{'ID'};
	$tmpItem{'DomainName'} = SOAP_Param_toStr($rawData->{'DomainName'});
	$tmpItem{'Transport'} = SOAP_Param_toStr($rawData->{'Transport'});
	$tmpItem{'Detail'} = SOAP_Param_toStr($rawData->{'Detail'});
	$tmpItem{'TransportDetail'} = SOAP_Param_toStr($rawData->{'TransportDetail'});
	$tmpItem{'PolicyID'} = $rawData->{'PolicyID'};
	$tmpItem{'AgentDisabled'} = $rawData->{'AgentDisabled'};
	$tmpItem{'DisableDelivery'} = $rawData->{'DisableDelivery'};
	$tmpItem{'AgentRef'} = SOAP_Param_toStr($rawData->{'AgentRef'});

	return SOAPResponse(RES_OK,\%tmpItem);
}


## @method updateMailTransport($mailTransportInfo)
# Update a mail transport
#
# @param mailTransportInfo Hash table
# @li ID - Transport ID
# @li PolicyID - Optional policy ID
# @li AgentDisabled - Optional, if this item has been disabled by the agent
# @li AgentRef - Optional, agent reference
#
# @return 0 on success or < 0 on error
sub updateMailTransport {
	my (undef,$mailTransportInfo) = @_;

	# Check params
	if (!defined($mailTransportInfo)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailTransportInfo' not defined");
	}
	if (!defined($mailTransportInfo->{'ID'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailTransportInfo' has no element 'ID' defined");
	}
	if ($mailTransportInfo->{'ID'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailTransportInfo' element 'ID' invalid");
	}

	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::mail::canAccessMailTransport($authInfo->{'AgentID'},$mailTransportInfo->{'ID'});
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mail transport ID '".$mailTransportInfo->{'ID'}."'");
	}

	my $i;
	$i->{'ID'} = $mailTransportInfo->{'ID'};
	$i->{'PolicyID'} = $mailTransportInfo->{'PolicyID'} if (defined($mailTransportInfo->{'PolicyID'}));
	$i->{'AgentDisabled'} = $mailTransportInfo->{'AgentDisabled'} if (defined($mailTransportInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $mailTransportInfo->{'AgentRef'} if (defined($mailTransportInfo->{'AgentRef'}));

	# Do the update	
	$res = smlib::mail::updateMailTransport($i);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @method getMailboxes($transportID,$search)
# Function to return a list of mailboxes
#
# @param transportID Mail transport ID
#
# @param search Optional hash ref with search criteria
# @li searchAddress - Address search criteria
# @li searchAgentRef - %Agent referent search criteria
# @li searchOrderBy - Order by this field
#
# @return Array ref of hash refs
# @li ID - %Mailbox ID on the system
# @li Address - Email address
# @li Name - Name of person
# @li DomainName - Domain name of the email address
# @li Quota - Quota in Mbyte
# @li PolicyID - Policy ID of this mailbox
# @li PremiumPolicy - Premium policy service
# @li PremiumSMTP - Premium SMTP service
# @li AgentDisabled - Set if the agent has disabled this transport
# @li DisableLogin - Set if login to this mailbox is disabled
# @li DisableDelivery - Set if delivery to this mailbox is disabled
# @li DisableSASL - Set if SASL authentication is disabled (used for AuthSMTP)
# @li AgentRef - %Agent reference
sub getMailboxes {
	my (undef,$transportID,$search) = @_;

	# Check params
	if (!defined($transportID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'transportID' undefined");
	}
	if ($transportID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'transportID' invalid");
	}
	
	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::mail::canAccessMailTransport($authInfo->{'AgentID'},$transportID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
	}

	# Pull in search criteria
	my $backendInfo;
	if (defined($backendInfo)) {
		$backendInfo->{'searchAddress'} = $search->{'searchAddress'};
		$backendInfo->{'searchAgentRef'} = $search->{'searchAgentRef'};
		$backendInfo->{'searchOrderBy'} = $search->{'searchOrderBy'};
	}

	# Grab and sanitize data
	my $rawData = smlib::mail::getMailboxes($transportID,$backendInfo);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::mail::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Address'} = SOAP_Param_toStr($item->{'Address'});
		$tmpItem->{'Name'} = SOAP_Param_toStr($item->{'Name'});
		$tmpItem->{'DomainName'} = SOAP_Param_toStr($item->{'DomainName'});
		$tmpItem->{'Quota'} = $item->{'Quota'};
		$tmpItem->{'PolicyID'} = $item->{'PolicyID'};

		$tmpItem->{'PremiumPolicy'} = $item->{'PremiumPolicy'};
		$tmpItem->{'PremiumSMTP'} = $item->{'PremiumSMTP'};

		$tmpItem->{'AgentDisabled'} = $item->{'AgentDisabled'};
		$tmpItem->{'DisableLogin'} = $item->{'DisableLogin'};
		$tmpItem->{'DisableDelivery'} = $item->{'DisableDelivery'};
		$tmpItem->{'DisableSASL'} = $item->{'DisableSASL'};

		$tmpItem->{'AgentRef'} = SOAP_Param_toStr($item->{'AgentRef'});

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


## @method getMailPolicies
# Return list of mail policies we can use
#
# @return Array ref of hash refs
# @li ID - Policy ID
# @li PolicyName - Policy name
sub getMailPolicies {
	my $authInfo = Auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = smlib::mail::getMailPolicies();
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::mail::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'PolicyName'} = SOAP_Param_toStr($item->{'PolicyName'});

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


## @method createMailbox($transportID,$mailboxInfo)
# Create a mailbox
#
# @param transportID Transport ID
#
# @param mailboxInfo Hash table ref
# @li Address - %Mailbox address
# @li Password - %Mailbox password
# @li Name - Optional mailbox name
# @li Quota - Quota in Mbyte
# @li PolicyID - Optional policy ID
# @li PremiumPolicy - Optional flag to set if lient has premium policy
# @li PremiumSMTP - Optional flag to set if client has premium SMTP service
# @li AgentDisabled - Optional flag agent can set to disable this mailbox
# @li AgentRef - Optional agent reference
#
# @return Returns mailbox ID
sub createMailbox {
	my (undef,$transportID,$mailboxInfo) = @_;

	# Check params
	if (!defined($transportID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'transportID' undefined");
	}
	if ($transportID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'transportID' invalid");
	}
	if (!defined($mailboxInfo)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxInfo' undefined");
	}
	if (!defined($mailboxInfo->{'Address'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxInfo' has no element 'Address' defined");
	}
	if ($mailboxInfo->{'Address'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Address' is blank");
	}
	if (!defined($mailboxInfo->{'Password'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxInfo' has no element 'Password' defined");
	}
	if ($mailboxInfo->{'Password'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Password' is blank");
	}
	if (!defined($mailboxInfo->{'Quota'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxInfo' has no element 'Quota' defined");
	}
	if ($mailboxInfo->{'Quota'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Quota' is invalid");
	}
	
	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::mail::canAccessMailTransport($authInfo->{'AgentID'},$transportID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
	}

	my $i;

	$i->{'Address'} = $mailboxInfo->{'Address'};
	$i->{'Password'} = $mailboxInfo->{'Password'};
	$i->{'Name'} = $mailboxInfo->{'Name'} if (defined($mailboxInfo->{'Name'}));
	$i->{'Quota'} = $mailboxInfo->{'Quota'};
	$i->{'PolicyID'} = $mailboxInfo->{'PolicyID'} if (defined($mailboxInfo->{'PolicyID'}));

	$i->{'PremiumPolicy'} = $mailboxInfo->{'PremiumPolicy'} if (defined($mailboxInfo->{'PremiumPolicy'}));
	$i->{'PremiumSMTP'} = $mailboxInfo->{'PremiumSMTP'} if (defined($mailboxInfo->{'PremiumSMTP'}));

	$i->{'AgentDisabled'} = $mailboxInfo->{'AgentDisabled'} if (defined($mailboxInfo->{'AgentDisabled'}));
	
	$i->{'AgentRef'} = $mailboxInfo->{'AgentRef'} if (defined($mailboxInfo->{'AgentRef'}));
	
	$res = smlib::mail::createMailbox($transportID,$i);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @method updateMailbox($mailboxInfo)
# Update mailbox
#
# @param mailboxInfo %Mailbox info hash ref
# @li ID - %Mailbox ID
# @li Password - Optional mailbox password
# @li Name - Optional mailbox name
# @li Quota - Quota in Mbyte
# @li PolicyID - Optional policy ID
# @li PremiumPolicy - Optional flag to set if lient has premium policy
# @li PremiumSMTP - Optional flag to set if client has premium SMTP service
# @li AgentDisabled - Optional flag agent can set to disable this mailbox
# @li AgentRef - Optional agent reference
#
# @return Returns mailbox ID
sub updateMailbox {
	my (undef,$mailboxInfo) = @_;


	# Check params
	if (!defined($mailboxInfo)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxInfo' undefined");
	}
	if (!defined($mailboxInfo->{'ID'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxInfo' has no element 'ID' defined");
	}
	if (defined($mailboxInfo->{'Quota'})) {
		if ($mailboxInfo->{'Quota'} < 1) {
			return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxInfo' element 'Quota' is invalid");
		}
	}
	
	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::mail::canAccessMailbox($authInfo->{'AgentID'},$mailboxInfo->{'ID'});
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mailbox ID '".$mailboxInfo->{'ID'}."'");
	}


	my $i;

	$i->{'ID'} = $mailboxInfo->{'ID'};
	
	$i->{'Password'} = $mailboxInfo->{'Password'} if (defined($mailboxInfo->{'Password'}));
	$i->{'Name'} = $mailboxInfo->{'Name'} if (defined($mailboxInfo->{'Name'}));
	$i->{'Quota'} = $mailboxInfo->{'Quota'} if (defined($mailboxInfo->{'Quota'}));
	$i->{'PolicyID'} = $mailboxInfo->{'PolicyID'} if (defined($mailboxInfo->{'PolicyID'}));

	$i->{'PremiumPolicy'} = $mailboxInfo->{'PremiumPolicy'} if (defined($mailboxInfo->{'PremiumPolicy'}));
	$i->{'PremiumSMTP'} = $mailboxInfo->{'PremiumSMTP'} if (defined($mailboxInfo->{'PremiumSMTP'}));

	$i->{'AgentDisabled'} = $mailboxInfo->{'AgentDisabled'} if (defined($mailboxInfo->{'AgentDisabled'}));
	
	$i->{'AgentRef'} = $mailboxInfo->{'AgentRef'} if (defined($mailboxInfo->{'AgentRef'}));
	
	$res = smlib::mail::updateMailbox($i);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @method removeMailbox($mailboxID)
# Remove mailbox
#
# @param mailboxID %Mailbox ID
#
# @return 0 on success or < 0 on error
sub removeMailbox {
	my (undef,$mailboxID) = @_;


	# Check params
	if (!defined($mailboxID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxID' undefined");
	}
	if ($mailboxID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxID' is invalid");
	}
	
	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::mail::canAccessMailbox($authInfo->{'AgentID'},$mailboxID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mailbox ID '$mailboxID'");
	}
	
	$res = smlib::mail::removeMailbox($mailboxID);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @method getMailboxInfo($mailboxID)
# Get mailbox information
#
# @param mailboxID %Mailbox ID
#
# @return Hash ref
# @li ID - %Mailbox ID on the system
# @li Address - Email address
# @li Name - Name of person
# @li DomainName - Domain name of the email address
# @li Quota - Quota in Mbyte
# @li PolicyID - Policy ID of this mailbox
# @li PremiumPolicy - Premium policy service
# @li PremiumSMTP - Premium SMTP service
# @li AgentDisabled - Set if the agent has disabled this transport
# @li DisableLogin - Set if login to this mailbox is disabled
# @li DisableDelivery - Set if delivery to this mailbox is disabled
# @li DisableSASL - Set if SASL authentication is disabled (used for AuthSMTP)
# @li AgentRef - %Agent reference
sub getMailboxInfo {
	my (undef,$mailboxID) = @_;

	# Check params
	if (!defined($mailboxID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxID' undefined");
	}
	if ($mailboxID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxID' is invalid");
	}
	
	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::mail::canAccessMailbox($authInfo->{'AgentID'},$mailboxID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mailbox ID '$mailboxID'");
	}
	
	# Grab and sanitize data
	my $rawData = smlib::mail::getMailboxRecord($mailboxID);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::mail::Error());
	}

	# Build result
	my %tmpItem;

	$tmpItem{'ID'} = $rawData->{'ID'};
	$tmpItem{'Address'} = SOAP_Param_toStr($rawData->{'Address'});
	$tmpItem{'Name'} = SOAP_Param_toStr($rawData->{'Name'});
	$tmpItem{'DomainName'} = SOAP_Param_toStr($rawData->{'DomainName'});
	$tmpItem{'Quota'} = $rawData->{'Quota'};
	$tmpItem{'PolicyID'} = $rawData->{'PolicyID'};
	$tmpItem{'PremiumPolicy'} = $rawData->{'PremiumPolicy'};
	$tmpItem{'PremiumSMTP'} = $rawData->{'PremiumSMTP'};
	$tmpItem{'AgentDisabled'} = $rawData->{'AgentDisabled'};
	$tmpItem{'DisableLogin'} = $rawData->{'DisableLogin'};
	$tmpItem{'DisableDelivery'} = $rawData->{'DisableDelivery'};
	$tmpItem{'DisableSASL'} = $rawData->{'DisableSASL'};
	$tmpItem{'AgentRef'} = SOAP_Param_toStr($rawData->{'AgentRef'});

	return SOAPResponse(RES_OK,\%tmpItem);
}


## @method getMailboxAliases($transportID,$search)
# Return agents mailbox aliases
#
# @param transportID Mail transport ID
#
# @param search Optional search criteria hash ref
# @li searchAddress - Address search criteria
# @li searchAgentRef - %Agent referent search criteria
# @li searchOrderBy - Order by this field
#
# @return Array ref of hash refs
# @li ID - %Mailbox ID on the system
# @li Address - Email address
# @li DomainName - Domain name of the alias address
# @li Goto - Alias destination
# @li AgentDisabled - Set if the agent has disabled this transport
# @li DisableDelivery - Set if delivery to this mailbox is disabled
# @li AgentRef - %Agent reference
sub getMailboxAliases {
	my (undef,$transportID,$search) = @_;

	# Check params
	if (!defined($transportID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'transportID' undefined");
	}
	if ($transportID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'transportID' invalid");
	}
	
	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::mail::canAccessMailTransport($authInfo->{'AgentID'},$transportID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
	}

	# Pull in search criteria
	my $backendInfo;
	if (defined($search)) {
		$backendInfo->{'searchAddress'} = $search->{'searchAddress'};
		$backendInfo->{'searchAgentRef'} = $search->{'searchAgentRef'};
		$backendInfo->{'searchOrderBy'} = $search->{'searchOrderBy'};
	}

	# Grab and sanitize data
	my $rawData = smlib::mail::getMailboxAliases($transportID,$backendInfo);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::mail::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Address'} = SOAP_Param_toStr($item->{'Address'});
		$tmpItem->{'DomainName'} = SOAP_Param_toStr($item->{'DomainName'});
		$tmpItem->{'Goto'} = SOAP_Param_toStr($item->{'Goto'});
		$tmpItem->{'AgentDisabled'} = $item->{'AgentDisabled'};
		$tmpItem->{'DisableDelivery'} = $item->{'DisableDelivery'};
		$tmpItem->{'AgentRef'} = SOAP_Param_toStr($item->{'AgentRef'});

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


## @method getMailboxaliasInfo($mailboxAliasID)
# Get mailbox alias information
#
# @param mailboxAliasID %Mailbox alias ID
#
# @return Hash ref
# @li ID - %Mailbox alias ID on the system
# @li Address - Email address
# @li Goto - Alias destination
# @li AgentDisabled - Set if the agent has disabled this transport
# @li DisableDelivery - Set if delivery to this alias is disabled
# @li AgentRef - %Agent reference
sub getMailboxAliasInfo {
	my (undef,$mailboxAliasID) = @_;

	# Check params
	if (!defined($mailboxAliasID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxAliasID' undefined");
	}
	if ($mailboxAliasID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxAliasID' is invalid");
	}
	
	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox alias
	my $res = smlib::mail::canAccessMailboxAlias($authInfo->{'AgentID'},$mailboxAliasID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mailbox alias ID '$mailboxAliasID'");
	}
	
	# Grab and sanitize data
	my $rawData = smlib::mail::getMailboxAliasRecord($mailboxAliasID);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::mail::Error());
	}

	# Build result
	my %tmpItem;

	$tmpItem{'ID'} = $rawData->{'ID'};
	$tmpItem{'Address'} = SOAP_Param_toStr($rawData->{'Address'});
	$tmpItem{'Goto'} = SOAP_Param_toStr($rawData->{'Goto'});
	$tmpItem{'AgentDisabled'} = $rawData->{'AgentDisabled'};
	$tmpItem{'DisableDelivery'} = $rawData->{'DisableDelivery'};
	$tmpItem{'AgentRef'} = SOAP_Param_toStr($rawData->{'AgentRef'});

	return SOAPResponse(RES_OK,\%tmpItem);
}


## @method updateMailboxAlias($mailboxAliasInfo)
# Update mailbox alias
#
# @param mailboxAliasInfo %Mailbox alias info hash ref
# @li ID - %Mailbox alias ID
# @li Goto - Optional alias destination address
# @li AgentDisabled - Optional flag agent can set to disable this mailbox alias
# @li AgentRef - Optional agent reference
#
# @return 0 on success or < 0 on error
sub updateMailboxAlias {
	my (undef,$mailboxAliasInfo) = @_;

	# Check params
	if (!defined($mailboxAliasInfo)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxAliasInfo' undefined");
	}
	if (!defined($mailboxAliasInfo->{'ID'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxAliasInfo' has no element 'ID' defined");
	}
	
	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox alias
	my $res = smlib::mail::canAccessMailboxAlias($authInfo->{'AgentID'},$mailboxAliasInfo->{'ID'});
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mailbox alias ID '".$mailboxAliasInfo->{'ID'}."'");
	}

	my $i;

	$i->{'ID'} = $mailboxAliasInfo->{'ID'};
	$i->{'Goto'} = $mailboxAliasInfo->{'Goto'} if (defined($mailboxAliasInfo->{'Goto'}));
	$i->{'AgentDisabled'} = $mailboxAliasInfo->{'AgentDisabled'} if (defined($mailboxAliasInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $mailboxAliasInfo->{'AgentRef'} if (defined($mailboxAliasInfo->{'AgentRef'}));
	
	$res = smlib::mail::updateMailboxAlias($i);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @method removeMailboxAlias($mailboxAliasID)
# Remove mailbox alias
#
# @param mailboxAliasID %Mailbox alias ID
#
# @return 0 on success or < 0 on error
sub removeMailboxAlias {
	my (undef,$mailboxAliasID) = @_;

	# Check params
	if (!defined($mailboxAliasID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxAliasID' undefined");
	}
	if ($mailboxAliasID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxAliasID' is invalid");
	}
	
	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::mail::canAccessMailbox($authInfo->{'AgentID'},$mailboxAliasID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mailbox alias ID '$mailboxAliasID'");
	}
	
	$res = smlib::mail::removeMailboxAlias($mailboxAliasID);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @method createMailboxAlias($transportID,$mailboxAliasInfo)
# Create a mailbox alias
#
# @param transportID Transport ID
#
# @param mailboxAliasInfo Hash table ref
# @li Address - %Mailbox alias
# @li Goto - %Mailbox alias destination
# @li AgentDisabled - Optional flag agent can set to disable this mailbox
# @li AgentRef - Optional agent reference
#
# @return Returns mailbox alias ID
sub createMailboxAlias {
	my (undef,$transportID,$mailboxAliasInfo) = @_;

	# Check params
	if (!defined($transportID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'transportID' undefined");
	}
	if ($transportID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'transportID' invalid");
	}
	if (!defined($mailboxAliasInfo)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxAliasInfo' undefined");
	}
	if (!defined($mailboxAliasInfo->{'Goto'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxAliasInfo' has no element 'Goto' defined");
	}
	if ($mailboxAliasInfo->{'Goto'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'mailboxAliasInfo' element 'Goto' is blank");
	}
	
	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::mail::canAccessMailTransport($authInfo->{'AgentID'},$transportID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access mail transport ID '$transportID'");
	}

	my $i;

	$i->{'Address'} = $mailboxAliasInfo->{'Address'};
	$i->{'Goto'} = $mailboxAliasInfo->{'Goto'} if (defined($mailboxAliasInfo->{'Goto'}));
	$i->{'AgentDisabled'} = $mailboxAliasInfo->{'AgentDisabled'} if (defined($mailboxAliasInfo->{'AgentDisabled'}));
	$i->{'AgentRef'} = $mailboxAliasInfo->{'AgentRef'} if (defined($mailboxAliasInfo->{'AgentRef'}));
	
	$res = smlib::mail::createMailboxAlias($transportID,$i);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	}

	return SOAPResponse(RES_OK,$res);
}




#
# D N S   S H I T
#



## @method getZones
# Function to return a list of zones
#
# @return Array ref of hash refs
# @li ID - %Mailbox ID on the system
# @li DomainName - Domain name of the email address
sub getZones {
	# TODO
	# Pull in search criteria
#	my $backendInfo;
#	if (defined($backendInfo)) {
#		$backendInfo->{'searchAddress'} = $search->{'searchAddress'};
#		$backendInfo->{'searchAgentRef'} = $search->{'searchAgentRef'};
#		$backendInfo->{'searchOrderBy'} = $search->{'searchOrderBy'};
#	}

	my $authInfo = Auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = smlib::zones::getZones($authInfo->{'AgentID'});
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::zones::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'DomainName'} = SOAP_Param_toStr($item->{'DomainName'});

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}










#
# R A D I U S   S H I T
#




## @method getRadiusUserByUsername($username)
# Return radius user hash ref from radius username. This function makes use
# of the getRadiusUser() function and returns the same hash ref.
#
# @param username Radius username
#
# @return Array ref of hash refs
# @see getRadiusUser
sub getRadiusUserByUsername {
	my ($self,$username) = @_;

	# Check params
	if (!defined($username)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'username' undefined");
	}
	if ($username eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'username' is blank");
	}

	# Get user ID
	my $userID;
	if (($userID = smlib::radius::getRadiusUserIDFromUsername($username)) < 1) {
		return SOAPResponse(RES_ERROR,$userID,smlib::radius::Error());
	}

	# This will return the proper SOAP response
	return $self->getRadiusUser($userID);
}


## @method getRadiusUser($userID)
# Return radius user details from a user ID
#
# @param userID Radius user ID
#
# @return Hash table ref
# @li ID - Radius user ID
# @li Username - Radius username
# @li UsageCap - Radius usage cap in Mbyte
# @li ClassID - Class ID
# @li ClassDesc - Class description
# @li RealmDesc - Realm description
# @li Service - Radius service
# @li NotifyMethodEmail - Email addresses to notify
# @li NotifyMethodCell - Cellphone numbers to notify
# @li AgentRef - Refernce for agent
# @li AgentDisabled - Set if the agent disabled this entry
sub getRadiusUser {
	my ($self,$userID) = @_;


	# Check params
	if (!defined($userID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' undefined");
	}
	if ($userID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' invalid");
	}

	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::radius::canAccessRadiusUser($authInfo->{'AgentID'},$userID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access radius user '$userID'");
	}

	# Grab and sanitize data
	my $rawData = smlib::radius::getRadiusUser($userID);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::radius::Error());
	}

	# Build result
	my $userData;
	$userData->{'ID'} = $rawData->{'ID'};
	$userData->{'Username'} = SOAP_Param_toStr($rawData->{'Username'});
	$userData->{'UsageCap'} = $rawData->{'UsageCap'};
	$userData->{'ClassID'} = SOAP_Param_toStr($rawData->{'ClassID'});
	$userData->{'ClassDesc'} = SOAP_Param_toStr($rawData->{'ClassDesc'});
	$userData->{'RealmDesc'} = SOAP_Param_toStr($rawData->{'RealmDesc'});
	$userData->{'Service'} = SOAP_Param_toStr($rawData->{'Service'});

	# Split off email addies & cel numbers
	my @notifyMethods = split(/,/,$rawData->{'NotifyMethod'});
	my @notifyMethodsEmail;
	my @notifyMethodsCell;
	foreach my $i (@notifyMethods) {
		if ($i =~ /^\S+\@\S+$/) {
			push(@notifyMethodsEmail,$i);
		} elsif ($i =~ /^\+[0-9]+$/) {
			push(@notifyMethodsCell,$i);
		}
	}
	$userData->{'NotifyMethodEmail'} = SOAP_Param_toStr(join(',',@notifyMethodsEmail));
	$userData->{'NotifyMethodCell'} = SOAP_Param_toStr(join(',',@notifyMethodsCell));

	$userData->{'AgentRef'} = SOAP_Param_toStr($rawData->{'AgentRef'});
	$userData->{'AgentDisabled'} = $rawData->{'AgentDisabled'};

	return SOAPResponse(RES_OK,$userData);
}


## @method getRadiusClasses
# Return radius users with certain search criteria
#
# @return Array ref of hash refs
# @li ID - Class ID
# @li Domain - Radius domain
# @li ClassDesc - Class description
# @li DomainDesc - Domain descripton
# @li Service - Radius service
sub getRadiusClasses {
	my ($self) = @_;


	my $authInfo = Auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = smlib::radius::getRadiusClasses($authInfo->{'AgentID'});
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::radius::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

	        $tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Domain'} = SOAP_Param_toStr($item->{'RadiusDomain'});
		$tmpItem->{'ClassDesc'} = SOAP_Param_toStr($item->{'ClassDesc'});
		$tmpItem->{'DomainDesc'} = SOAP_Param_toStr($item->{'Description'});
		$tmpItem->{'Service'} = SOAP_Param_toStr($item->{'Service'});

		push(@data,$tmpItem);
	} 

	return SOAPResponse(RES_OK,\@data);
}


## @method getRadiusUserPortLocks($userID)
# Return radius user port locks
#
# @param userID Radius user ID
#
# @return Array ref of hash refs
# @li ID - Class ID
# @li Port - Port number
# @li AgentDisabled - Set if agent has disabled this port
# @li AgentRef - Reference for agent
sub getRadiusUserPortLocks {
	my ($self,$userID) = @_;


	# Check params
	if (!defined($userID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' undefined");
	}
	if ($userID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' invalid");
	}

	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::radius::canAccessRadiusUser($authInfo->{'AgentID'},$userID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access radius user '$userID'");
	}

	# Grab and sanitize data
	my $rawData = smlib::radius::getRadiusUserPortLocks($userID);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::radius::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

        	$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Port'} = SOAP_Param_toStr($item->{'NASPort'});
		$tmpItem->{'AgentDisabled'} = $item->{'AgentDisabled'};
		$tmpItem->{'AgentRef'} = SOAP_Param_toStr($item->{'AgentRef'});

		push(@data,$tmpItem);
	} 

	return SOAPResponse(RES_OK,\@data);
}


## @method getRadiusUserLogs($userID,$search)
# Return radius user logs
#
# @param userID Radius user ID
#
# @param search Hash ref of search criteria
# @li searchFrom - Date to search from
# @li searchTo - Date to search to
# @li searchOrderBy - Order by this, "date"
#
# @return Array ref of hash refs
# @li ID - Radius log ID
# @li Username - Radius username
# @li Status - Record status
# @li Timestamp - Record timestamp
# @li AcctSessionID - Session ID
# @li AcctSessionTime - Session time, time the session has been active
# @li NASIPAddress - NAS IP address
# @li NASPortType - NAS port type
# @li NASPort - NAS port
# @li CalledStationID - Called station ID
# @li CallingStationID - Calling station ID, could be caller ID
# @li NASTransmitRate - Transmit rate reported by NAS
# @li NASReceiveRate - Receive rate reported by NAS
# @li FramedIPAddress - IP address of this session
# @li AcctInputOctets - Input bytes
# @li AcctOutputOctets - Output bytes
# @li AcctInputGigawords - Number of times the AcctInputOctets counter overflowed
# @li AcctOutputGigawords - Number of times the AcctOutputOctets counter overflowed
# @li LastAcctUpdate - Last time this record was updated
# @li ConnectTermReason - Connection termination reason
sub getRadiusUserLogs {
	my ($self,$userID,$search) = @_;


	# Check params
	if (!defined($userID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' undefined");
	}
	if ($userID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' invalid");
	}

	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this mailbox
	my $res = smlib::radius::canAccessRadiusUser($authInfo->{'AgentID'},$userID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access radius user '$userID'");
	}

	# Pull in search criteria
	my $backendInfo;
	if (defined($search)) {
		$backendInfo->{'searchFrom'} = $search->{'searchFrom'};
		$backendInfo->{'searchTo'} = $search->{'searchTo'};
		$backendInfo->{'searchOrderBy'} = $search->{'searchOrderBy'};
	}

	# Grab and sanitize data
	my $rawData = smlib::radius::getRadiusUserLogs($userID,$backendInfo);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::radius::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Username'} = SOAP_Param_toStr($item->{'Username'});
		$tmpItem->{'Status'} = $item->{'Status'};
		$tmpItem->{'Timestamp'} = $item->{'Timestamp'};
	
		$tmpItem->{'AcctSessionID'} = SOAP_Param_toStr($item->{'AcctSessionID'});
		$tmpItem->{'AcctSessionTime'} = $item->{'AcctSessionTime'};

		$tmpItem->{'NASIPAddress'} = $item->{'NASIPAddress'};
		$tmpItem->{'NASPortType'} = $item->{'NASPortType'};
		$tmpItem->{'NASPort'} = $item->{'NASPort'};
		$tmpItem->{'CalledStationID'} = SOAP_Param_toStr($item->{'CalledStationID'});
		$tmpItem->{'CallingStationID'} = SOAP_Param_toStr($item->{'CallingStationID'});

		$tmpItem->{'NASTransmitRate'} = $item->{'NASTransmitRate'};
		$tmpItem->{'NASReceiveRate'} = $item->{'NASReceiveRate'};
	
		$tmpItem->{'FramedIPAddress'} = $item->{'FramedIPAddress'};

		$tmpItem->{'AcctInputOctets'} = SOAP_Param_toStr($item->{'AcctInputOctets'});
		$tmpItem->{'AcctOutputOctets'} = SOAP_Param_toStr($item->{'AcctOutputOctets'});
		$tmpItem->{'AcctInputGigawords'} = SOAP_Param_toStr($item->{'AcctInputGigawords'});
		$tmpItem->{'AcctOutputGigawords'} = SOAP_Param_toStr($item->{'AcctOutputGigawords'});

		$tmpItem->{'LastAcctUpdate'} = $item->{'LastAcctUpdate'};
	
		$tmpItem->{'ConnectTermReason'} = $item->{'ConnectTermReason'};

		push(@data,$tmpItem);
	} 

	return SOAPResponse(RES_OK,\@data);
}


## @method createRadiusUserPortLock($lockInfo)
# Create a radius user port lock
#
# @param lockInfo Lock info hash ref
# @li UserID - Radius user ID
# @li Port - NAS port
# @li AgentRef - Optional agent reference
# @li AgentDisabled - Optional agent disabled flag
#
# @return Returns radius user port lock ID
sub createRadiusUserPortLock {
	my ($self,$lockInfo) = @_;


	# Check params
	if (!defined($lockInfo)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'lockInfo' undefined");
	}
	if (!defined($lockInfo->{'UserID'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'lockInfo' element 'UserID' is undefined");
	}
	if ($lockInfo->{'UserID'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'lockInfo' element 'UserID' is invalid");
	}
	if (!defined($lockInfo->{'Port'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'lockInfo' element 'Port' is undefined");
	}
	if ($lockInfo->{'Port'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'lockInfo' element 'Port' is blank");
	}

	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this radius user
	my $res = smlib::radius::canAccessRadiusUser($authInfo->{'AgentID'},$lockInfo->{'UserID'});
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access radius user '".$lockInfo->{'UserID'}."'");
	}

	my $i;

	$i->{'UserID'} = $lockInfo->{'UserID'};
	$i->{'NASPort'} = $lockInfo->{'Port'};
	$i->{'AgentRef'} = $lockInfo->{'AgentRef'} if (defined($lockInfo->{'AgentRef'}));
	$i->{'AgentDisabled'} = $lockInfo->{'AgentDisabled'} if (defined($lockInfo->{'AgentDisabled'}));
	
	$res = smlib::radius::createRadiusUserPortLock($lockInfo->{'UserID'},$i);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @method removeRadiusUserPortLock($portLockID)
# Remove radius user port lock
#
# @param portLockID Radius port lock ID
#
# @return 0 on success or < 0 on error
sub removeRadiusUserPortLock {
	my ($self,$portLockID) = @_;


	if (!defined($portLockID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'portLockID' is undefined");
	}
	if ($portLockID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'portLockID' is invalid");
	}

	my $authInfo = Auth::sessionGetData();

	# Get user ID
	my $userID;
	if (($userID = smlib::radius::getRadiusUserIDFromPortLockID($portLockID)) < 1) {
		return SOAPResponse(RES_ERROR,$userID,smlib::radius::Error());
	}

	# Check if we authorized for this radius user
	my $res = smlib::radius::canAccessRadiusUser($authInfo->{'AgentID'},$userID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access radius user '$userID'");
	}

	# Grab and sanitize data
	$res = smlib::radius::removeRadiusUserPortLock($portLockID);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @method getRadiusUserTopups($userID,$search)
# Return radius user topups
#
# @param userID Radius user ID
#
# @param search Optional hash ref of search criteria
# @li searchFrom - Date to search from
# @li searchTo - Date to search to
# @li searchOrderBy - Order by this, "date"
#
# @return Array ref of hash refs
# @li ID - Radius topup ID
# @li Bandwidth - Topup bandwidth in Mbyte
# @li Timestamp - Timestamp topup was added
# @li ValidFrom - Date topup is valid from in YYYY-MM-DD
# @li ValidTo - Date topup is valid to in YYYY-MM-DD
# @li AgentRef - Reference for agent
sub getRadiusUserTopups {
	my ($self,$userID,$search) = @_;

	# Check params
	if (!defined($userID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' is undefined");
	}
	if ($userID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' is invalid");
	}

	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this radius user
	my $res = smlib::radius::canAccessRadiusUser($authInfo->{'AgentID'},$userID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access radius user '$userID'");
	}

	# Pull in search criteria
	my $backendInfo;
	if (defined($search)) {
		$backendInfo->{'searchFrom'} = $search->{'searchFrom'};
		$backendInfo->{'searchTo'} = $search->{'searchTo'};
		$backendInfo->{'searchOrderBy'} = $search->{'searchOrderBy'};
	}

	# Grab and sanitize data
	my $rawData = smlib::radius::getRadiusUserTopups($userID,$backendInfo);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::radius::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;


		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Bandwidth'} = $item->{'Bandwidth'};
		$tmpItem->{'Timestamp'} = $item->{'Timestamp'};
		$tmpItem->{'ValidFrom'} = $item->{'ValidFrom'};
		$tmpItem->{'ValidTo'} = $item->{'ValidTo'};
		$tmpItem->{'AgentRef'} = $item->{'AgentRef'};

		push(@data,$tmpItem);
	} 

	return SOAPResponse(RES_OK,\@data);
}


## @method createRadiusUserTopup($info)
# Create a radius topup
#
# @param info Topup info
# @li UserID - Radius user ID
# @li Bandwidth - Topup bandwidth in Mbyte
# @li ValidFrom - Valid from date in format of YYYY-MM-DD
# @li ValidTo - Valid to date in format of YYYY-MM-DD
# @li AgentRef - Optional agent reference
#
# @return Returns radius user port lock ID
sub createRadiusUserTopup {
	my ($self,$info) = @_;


	# Check params
	if (!defined($info)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'info' undefined");
	}
	if (!defined($info->{'UserID'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'info' element 'UserID' is undefined");
	}
	if ($info->{'UserID'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'info' element 'UserID' is invalid");
	}
	if (!defined($info->{'Bandwidth'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'info' element 'Bandwidth' is undefined");
	}
	if ($info->{'Bandwidth'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'info' element 'Bandwidth' is invalid");
	}
	if (!defined($info->{'ValidFrom'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'info' element 'ValidFrom' is undefined");
	}
	if (!($info->{'ValidFrom'} =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'info' element 'ValidFrom' is not an ISO standard date 'YYYY-MM-DD'");
	}
	if (!defined($info->{'ValidTo'})) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'info' element 'ValidTo' is undefined");
	}
	if (!($info->{'ValidFrom'} =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'info' element 'ValidTo' is not an ISO standard date 'YYYY-MM-DD'");
	}

	my $authInfo = Auth::sessionGetData();
	
	# Check if we authorized for this radius user
	my $res = smlib::radius::canAccessRadiusUser($authInfo->{'AgentID'},$info->{'UserID'});
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access radius user '".$info->{'UserID'}."'");
	}

	my $i;

	$i->{'UserID'} = $info->{'UserID'};
	$i->{'Bandwidth'} = $info->{'Bandwidth'};
	$i->{'ValidFrom'} = $info->{'ValidFrom'};
	$i->{'ValidTo'} = $info->{'ValidTo'};
	$i->{'AgentRef'} = $info->{'AgentRef'} if (defined($info->{'AgentRef'}));
	
	$res = smlib::radius::createRadiusUserTopup($i);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @method getRadiusUserCurrentTopups($userID)
# Return current radius user topups
#
# @param userID Radius user ID
#
# @return Number of Mbytes the user has been allocated in topups for the current period
sub getRadiusUserCurrentTopups {
	my ($self,$userID) = @_;


	# Check params
	if (!defined($userID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' is undefined");
	}
	if ($userID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' is invalid");
	}

	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this radius user
	my $res = smlib::radius::canAccessRadiusUser($authInfo->{'AgentID'},$userID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access radius user '$userID'");
	}

	# Grab and sanitize data
	$res = smlib::radius::getRadiusUserCurrentTopups($userID);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


## @method getRadiusUserCurrentUsage($userID)
# Return current radius usage
#
# @param userID Radius user ID
#
# @return Number of Mbytes the user has been used
sub getRadiusUserCurrentUsage {
	my ($self,$userID) = @_;


	# Check params
	if (!defined($userID)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' is undefined");
	}
	if ($userID < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'userID' is invalid");
	}

	my $authInfo = Auth::sessionGetData();

	# Check if we authorized for this radius user
	my $res = smlib::radius::canAccessRadiusUser($authInfo->{'AgentID'},$userID);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	} elsif ($res == 0) {
		return SOAPResponse(RES_ERROR,ERR_NOACCESS,"Insufficient privileges to access radius user '$userID'");
	}

	# Grab and sanitize data
	$res = smlib::radius::getRadiusUserCurrentUsage($userID);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	}

	return SOAPResponse(RES_OK,$res);
}








1;
# vim: ts=4
