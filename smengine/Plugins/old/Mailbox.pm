# Mailbox SOAP interface
# Copyright (C) 2008, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class Mailbox
# This is the user mailbox handling class
package Mailbox;

use strict;
# Set library directory
use lib qw(../);
use Auth;

use smlib::mail qw();

use smlib::constants;


# Plugin info
our $pluginInfo = {
	Name => "Mailbox",
	Init => \&init,
};


## @internal
# Initialize module
sub init {
	my $server = shift;

	Auth::aclAdd('Mailbox','ping','Mailbox/Test');
	Auth::aclAdd('Mailbox','getMailboxDetails','Mailbox/Get');
	Auth::aclAdd('Mailbox','getEffectiveMailboxPolicy','Mailbox/Policy/Get');
	Auth::aclAdd('Mailbox','setClientMailboxPolicy','Mailbox/Policy/Set');
}


## @method ping
# Ping function to allow one to see if they get a positive response from 
# this module
#
# @return Will always return 0
sub ping {
	return 0;
}



## @method getMailboxInfo
# Get mailbox information
#
# @return Hash ref
# @li Name - Name of person
# @li Address - Email address
# @li DomainName - Domain name of the email address
# @li Quota - Quota in Mbyte
# @li PremiumPolicy - Premium policy service
# @li PolicyID - Policy ID of this mailbox
# @li PremiumSMTP - Premium SMTP service
# @li DisableDelivery - Set if delivery to this mailbox is disabled
# @li DisableSASL - Set if SASL authentication is disabled (used for AuthSMTP)
# @li PolicyName - Set if we have a PolicyID set
sub getMailboxDetails {

	my $authInfo = Auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = sm::mail::getMailboxRecord($authInfo->{'MailboxID'});
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::agents::Error());
	}

	# Build result
	my $mailboxData;
	$mailboxData->{'Name'} = $rawData->{'Name'}; 
	$mailboxData->{'Address'} = $rawData->{'Address'}; 
	$mailboxData->{'DomainName'} = $rawData->{'DomainName'}; 
	$mailboxData->{'Quota'} = $rawData->{'Quota'}; 
	$mailboxData->{'PremiumPolicy'} = $rawData->{'PremiumPolicy'}; 
	$mailboxData->{'PolicyID'} = $rawData->{'PolicyID'}; 
	$mailboxData->{'PremiumSMTP'} = $rawData->{'PremiumSMTP'}; 
	$mailboxData->{'DisableDelivery'} = $rawData->{'DisableDelivery'}; 
	$mailboxData->{'DisableSASL'} = $rawData->{'DisableSASL'}; 

	# Map in extra data so we don't have to call SOAP API yet AGAIN
	if ($mailboxData->{'PolicyID'} > 0) {
		my $rawPolicyData = sm::mail::getMailPolicyRecord($rawData->{'PolicyID'});
		$mailboxData->{'PolicyName'} = $rawPolicyData->{'PolicyName'};
	}

	return SOAPResponse(RES_OK,$mailboxData);
}


## @method getEffectiveMailboxPolicy
# Return mailbox aliases
#
# @return Hash ref
# @li PolicyName - Name of policy
# @li PolicyType - Type of policy
# @li Greylisting - Use greylisting
# @li Blacklisting - Use blacklisting
# @li BypassVirusChecks - Bypass virus scanning
# @li BypassSpamChecks - Bypass spam scanning
# @li BypassBannedChecks - Bypass banned file scanning
# @li BypassHeaderhecks - Bypass header checks
# @li SpamModifiesSubject - Modify subject if we detect spam
# @li SpamTagLevel - Level at which to tag as suspicious
# @li SpamTag2Level - Level at which to tag as spam
# @li SpamKillLevel - Level at which to kill the message
# @li SpamSubjectTag - Subject tag to for suspicious email
# @li SpamSubjectTag2 - Subject tag to for spam email
# @li MessageSizeLimit - Maximum message size
sub getEffectiveMailboxPolicy {
	
	my $authInfo = Auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = sm::mail::getEffectiveMailboxPolicy($authInfo->{'MailboxID'});
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,smlib::agents::Error());
	}

	# Build result
	my $policyData;

	$policyData->{'PolicyName'} = $rawData->{'PolicyName'};
	$policyData->{'PolicyType'} = $rawData->{'PolicyType'};

	$policyData->{'Greylisting'} = $rawData->{'Greylisting'};

	$policyData->{'Blacklisting'} = $rawData->{'Blacklisting'};
	
	$policyData->{'BypassVirusChecks'} = $rawData->{'BypassVirusChecks'};
	$policyData->{'BypassSpamChecks'} = $rawData->{'BypassSpamChecks'};
	$policyData->{'BypassBannedChecks'} = $rawData->{'BypassBannedChecks'};
	$policyData->{'BypassHeaderChecks'} = $rawData->{'BypassHeaderChecks'};

	$policyData->{'SpamModifiesSubject'} = $rawData->{'SpamModifiesSubject'};

	$policyData->{'SpamTagLevel'} = $rawData->{'SpamTagLevel'};
	$policyData->{'SpamTag2Level'} = $rawData->{'SpamTag2Level'};
	$policyData->{'SpamKillLevel'} = $rawData->{'SpamKillLevel'};

	$policyData->{'SpamSubjectTag'} = $rawData->{'SpamSubjectTag'};
	$policyData->{'SpamSubjectTag2'} = $rawData->{'SpamSubjectTag2'};

	$policyData->{'MessageSizeLimit'} = $rawData->{'MessageSizeLimit'};

	return SOAPResponse(RES_OK,$policyData);
}


## @method setClientMailboxPolicy($policy)
# Return policy details
#
# @param policy Hash ref
# @li Greylisting - Use greylisting
# @li Blacklisting - Use blacklisting
# @li BypassVirusChecks - Bypass virus scanning
# @li BypassSpamChecks - Bypass spam scanning
# @li BypassBannedChecks - Bypass banned file scanning
# @li BypassHeaderhecks - Bypass header checks
# @li SpamModifiesSubject - Modify subject if we detect spam
# @li SpamTagLevel - Level at which to tag as suspicious
# @li SpamTag2Level - Level at which to tag as spam
# @li SpamKillLevel - Level at which to kill the message
# @li SpamSubjectTag - Subject tag to for suspicious email
# @li SpamSubjectTag2 - Subject tag to for spam email
# @li MessageSizeLimit - Maximum message size
#
# @return 0 on success or < 0 on error
sub setClientMailboxPolicy {
	my (undef,$policy) = @_;


	my $authInfo = Auth::sessionGetData();

	# FIXME TODO - Add parameter validation


	# Build result
	my $policyData;

	$policyData->{'Greylisting'} = $policy->{'Greylisting'};

	$policyData->{'Blacklisting'} = $policy->{'Blacklisting'};
	
	$policyData->{'BypassVirusChecks'} = $policy->{'BypassVirusChecks'};
	$policyData->{'BypassSpamChecks'} = $policy->{'BypassSpamChecks'};
	$policyData->{'BypassBannedChecks'} = $policy->{'BypassBannedChecks'};
	$policyData->{'BypassHeaderChecks'} = $policy->{'BypassHeaderChecks'};

	$policyData->{'SpamModifiesSubject'} = $policy->{'SpamModifiesSubject'};

	$policyData->{'SpamTagLevel'} = $policy->{'SpamTagLevel'};
	$policyData->{'SpamTag2Level'} = $policy->{'SpamTag2Level'};
	$policyData->{'SpamKillLevel'} = $policy->{'SpamKillLevel'};
	$policyData->{'SpamSubjectTag'} = $policy->{'SpamSubjectTag'};
	$policyData->{'SpamSubjectTag2'} = $policy->{'SpamSubjectTag2'};

	$policyData->{'MessageSizeLimit'} = $policy->{'MessageSizeLimit'};

	my $res = sm::mail::setClientMailboxPolicy($authInfo->{'MailboxID'},$policyData);
	# Check return code
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::mail::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



1;
# vim: ts=4
