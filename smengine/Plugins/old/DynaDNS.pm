# Dynamic DNS functions
# Copyright (C) 2008, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class DynaDNS
# This is the dynamic dns handling class
package DynaDNS;


use strict;
use lib qw(../);
use Auth;

use smlib::zones qw();

use smlib::constants;



# Plugin info
our $pluginInfo = {
	Name => "DynaDNS",
	Init => \&init,
};


## @internal
# Initialize module
sub init {
	my $server = shift;

	Auth::aclAdd('DynaDNS','ping','DynaDNS/Test');
	Auth::aclAdd('DynaDNS','update','DynaDNS/Update');
}




## @method ping
# Ping function to allow one to see if they get a positive response from 
# this module
#
# @return Will always return 0
sub ping {
	return 0;
}


## @method update($host,$ip)
# Update a dynamic dns entry
# 
# @param host Fully qualified DNS name to update
# @param ip Optional IP address
#
# @returns Integer describing result
# @li 0 Success
# @li -1 Parameter problem
# @li -2 Authentication failure
# @li -3 System error
# @li -4 Change prohibited
# @li -5 Too many updates
sub update {
	my (undef,$host,$ip) = @_;


	# Check params
	if (!defined($host)) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'host' is undefined");
	}
	if ($host eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'host' is invalid");
	}

	my $authInfo = Auth::sessionGetData();

	# If we weren't given and IP, take the peer addy
	if (!defined($ip) || $ip eq "") {
		$ip = $authInfo->{'Peer'};
	}

	my $res = sm::zones::DynaDNS_update($authInfo->{'DynaDNSUserID'},$host,$ip);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,smlib::radius::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



1;
# vim: ts=4
