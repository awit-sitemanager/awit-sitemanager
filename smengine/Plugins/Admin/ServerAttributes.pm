# Admin Server Attributes
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class ServerAttributes
# This is the admin server attributes class
package Admin::ServerAttributes;


use strict;

use smlib::Admin::ServerAttributes;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: ServerAttributes",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::ServerAttributes','ping','Admin/ServerAttributes/Test');

	Auth::aclAdd('Admin::ServerAttributes','getServerAttributes','Admin/ServerAttributes/List');
	Auth::aclAdd('Admin::ServerAttributes','getServerAttribute','Admin/ServerAttributes/Get');
	Auth::aclAdd('Admin::ServerAttributes','createServerAttribute','Admin/ServerAttributes/Add');
	Auth::aclAdd('Admin::ServerAttributes','updateServerAttribute','Admin/ServerAttributes/Update');
	Auth::aclAdd('Admin::ServerAttributes','removeServerAttribute','Admin/ServerAttributes/Remove');

	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getServerAttributes($serverID,$search)
# Return server attributes info hash
#
# @param serverID Server ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by user ID with elements...
# @li ID
# @li Attribute
sub getServerAttributes
{
	my (undef,$serverID,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverID' undefined");
	}
	if (!defined($serverID = isNumber($serverID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverID' is invalid");
	}

	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::ServerAttributes::getServerAttributes($serverID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Attribute','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getServerAttribute($attributeID)
# Return server attribute info hash
#
# @param attributeID Attribute ID
#
# @return server attribute info hash ref
# @li ID
# @li Attribute
sub getServerAttribute
{
	my (undef,$attributeID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' undefined");
	}
	if (!defined($attributeID = isNumber($attributeID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' is invalid");
	}

	# Grab and sanitize data
	my $rawData = smlib::Admin::ServerAttributes::getServerAttribute($attributeID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Attribute','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createServerAttribute($attributeInfo)
# Create a server attribute
#
# @param attributeInfo Attribute info hash
# @li ServerID server ID
# @li Attribute server attribute
#
# @return ID Attribute ID
sub createServerAttribute
{
	my (undef,$attributeInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' not defined");
	}
	if (!isHash($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' is not a HASH");
	}

	if (!defined($attributeInfo->{'ServerID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'Attribute' defined");
	}
	if (!defined($attributeInfo->{'ServerID'} = isNumber($attributeInfo->{'ServerID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is invalid");
	}

	if (!defined($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'Attribute' defined");
	}
	if (!isVariable($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is invalid");
	}
	if ($attributeInfo->{'Attribute'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is blank");
	}

	# Build result
	my $attributeData;
	$attributeData->{'ServerID'} = $attributeInfo->{'ServerID'};
	$attributeData->{'Attribute'} = $attributeInfo->{'Attribute'};

	my $res = smlib::Admin::ServerAttributes::createServerAttribute($attributeData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','ID');
}



## @method updateServerAttribute($attributeInfo)
# Update server attribute
#
# @param attributeInfo Attribute info hash ref
# @li ID Attribute ID
# @li Attribute - Attribute
#
# @return 0 on success, < 0 on error
sub updateServerAttribute
{
	my (undef,$attributeInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' undefined");
	}
	if (!isHash($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' is not a HASH");
	}

	if (!defined($attributeInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'ID'");
	}
	if (!defined($attributeInfo->{'ID'} = isNumber($attributeInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'ID' is invalid");
	}

	if (!defined($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'Attribute'");
	}
	if (!isVariable($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is invalid");
	}
	if ($attributeInfo->{'Attribute'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is blank");
	}

	my $i;
	$i->{'ID'} = $attributeInfo->{'ID'};
	$i->{'Attribute'} = $attributeInfo->{'Attribute'};

	my $res = smlib::Admin::ServerAttributes::updateServerAttribute($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeServerAttribute($attributeID)
# Remove server attribute
#
# @param attributeID Attribute ID
#
# @return 0 on success or < 0 on error
sub removeServerAttribute
{
	my (undef,$attributeID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' undefined");
	}
	if (!defined($attributeID = isNumber($attributeID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' is invalid");
	}

	# Grab and sanitize data
	my $res = smlib::Admin::ServerAttributes::removeServerAttribute($attributeID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



1;
# vim: ts=4
