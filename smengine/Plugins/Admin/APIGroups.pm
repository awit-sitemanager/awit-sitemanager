# Admin APIGroup functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class APIGroups
# This is the API group admin class
package Admin::APIGroups;


use strict;

use Auth;

use smlib::Admin::APIGroups;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: APIGroups",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::APIGroups','ping','Admin/Test');

	Auth::aclAdd('Admin::APIGroups','getAPIGroups','Admin/APIGroups/List');
	Auth::aclAdd('Admin::APIGroups','getAPIGroup','Admin/APIGroups/Get');
	Auth::aclAdd('Admin::APIGroups','createAPIGroup','Admin/APIGroups/Add');
	Auth::aclAdd('Admin::APIGroups','updateAPIGroup','Admin/APIGroups/Update');
	Auth::aclAdd('Admin::APIGroups','removeAPIGroup','Admin/APIGroups/Remove');
	
	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getAPIGroups($search)
# Return API group info hash 
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by API group ID with elements...
# @li ID API Group ID
# @li Name API Group Name
# @li Description API Group Description
sub getAPIGroups
{
	my (undef,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::APIGroups::getAPIGroups($search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('Description','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getAPIGroup($APIGroupID)
# Return API group info hash 
#
# @param APIGroupID APIGroup ID
#
# @return API Group info hash ref
# @li ID - API Group ID
# @li Name - API Group Name
# @li Description - API Group Description
sub getAPIGroup
{
	my (undef,$APIGroupID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($APIGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupID' undefined");
	}
	if (!defined($APIGroupID = isNumber($APIGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupID' is invalid");
	}

	# Grab and sanitize data
	my $rawData = smlib::Admin::APIGroups::getAPIGroup($APIGroupID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('Description','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createAPIGroup($APIGroupInfo)
# Create a APIGroup
#
# @param APIGroupInfo APIGroup info hash
# @li Name APIGroup name
# @li Description APIGroup description
#
# @return APIGroup ID
sub createAPIGroup
{
	my (undef,$APIGroupInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($APIGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' not defined");
	}
	if (!isHash($APIGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' is not a HASH");
	}

	if (!defined($APIGroupInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' has no element 'Name' defined");
	}
	if (!isVariable($APIGroupInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'Name' is invalid");
	}
	if ($APIGroupInfo->{'Name'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'Name' is blank");
	}

	if (defined($APIGroupInfo->{'Description'})) {
		if (!isVariable($APIGroupInfo->{'Description'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'Description' is invalid");
		}
		if ($APIGroupInfo->{'Description'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'Description' is blank");
		}
	}

	# Build result
	my $APIGroupData;
	$APIGroupData->{'Name'} = $APIGroupInfo->{'Name'};
	$APIGroupData->{'Description'} = $APIGroupInfo->{'Description'} if (defined($APIGroupInfo->{'Description'}));

	my $res = smlib::Admin::APIGroups::createAPIGroup($APIGroupData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','APIGroupID');
}



## @method updateAPIGroup($APIGroupInfo)
# Update API group
#
# @param APIGroupInfo API Group info hash ref
# @li ID API Group ID
# @li Name Optional API Group Name
# @li Description Optional description
#
# @return 0 on success, < 0 on error
sub updateAPIGroup
{
	my (undef,$APIGroupInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($APIGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' undefined");
	}
	if (!isHash($APIGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' is not a HASH");
	}

	if (!defined($APIGroupInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' has no element 'ID'");
	}
	if (!defined($APIGroupInfo->{'ID'} = isNumber($APIGroupInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'ID' is invalid");
	}

	if (defined($APIGroupInfo->{'Name'})) {
		if (!isVariable($APIGroupInfo->{'Name'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'Name' is invalid");
		}
		if ($APIGroupInfo->{'Name'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'Name' is blank");
		}
	}

	if (defined($APIGroupInfo->{'Description'})) {
		if (!isVariable($APIGroupInfo->{'Description'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'Description' is invalid");
		}
		if ($APIGroupInfo->{'Description'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'Description' is blank");
		}
	}

	my $i;

	$i->{'ID'} = $APIGroupInfo->{'ID'};
	$i->{'Name'} = $APIGroupInfo->{'Name'} if (defined($APIGroupInfo->{'Name'}));
	$i->{'Description'} = $APIGroupInfo->{'Description'} if (defined($APIGroupInfo->{'Description'}));

	my $res = smlib::Admin::APIGroups::updateAPIGroup($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeAPIGroup($APIGroupID)
# Remove an API Group 
#
# @param APIGroupID API Group ID
#
# @return 0 on success or < 0 on error
sub removeAPIGroup
{
	my (undef,$APIGroupID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($APIGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupID' not defined");
	}
	if (!defined($APIGroupID = isNumber($APIGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupID' element 'APIGroupID' is invalid");
	}

	my $res = smlib::Admin::APIGroups::removeAPIGroup($APIGroupID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}
 


1;
# vim: ts=4
