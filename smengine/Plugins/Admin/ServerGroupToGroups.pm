# Admin Server group to group functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class ServerGroupToGroups
# This is admin server group to group class
package Admin::ServerGroupToGroups;


use strict;

use Auth;

use smlib::Admin::ServerGroupToGroups;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: ServerGroupToGroups",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::ServerGroupToGroups','ping','Admin/ServerGroupToGroups/Test');

	Auth::aclAdd('Admin::ServerGroupToGroups','getServerGroupToGroups','Admin/ServerGroupToGroups/List');
	Auth::aclAdd('Admin::ServerGroupToGroups','addServerGroupToGroup','Admin/ServerGroupToGroups/Add');
	Auth::aclAdd('Admin::ServerGroupToGroups','removeServerGroupToGroup','Admin/ServerGroupToGroups/Remove');
	
	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method addServerGroupToGroup($serverGroupToGroupsInfo)
# Add a group to a server group
#
# @param serverGroupToGroupsInfo Server group to group info hash
# @li ServerGroupID Server Group ID
# @li UserGroupID User Group ID
#
# @return serverGroupToGroupsID group ID
sub addServerGroupToGroup
{
	my (undef,$serverGroupToGroupsInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupToGroupsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToGroupsInfo' not defined");
	}
	if (!isHash($serverGroupToGroupsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToGroupsInfo' is not a HASH");
	}

	if (!defined($serverGroupToGroupsInfo->{'ServerGroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToGroupsInfo' has no element 'ServerGroupID' defined");
	}
	if (!defined($serverGroupToGroupsInfo->{'ServerGroupID'} = isNumber($serverGroupToGroupsInfo->{'ServerGroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToGroupsInfo' element 'ServerGroupID' is invalid");
	}

	if (!defined($serverGroupToGroupsInfo->{'UserGroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToGroupsInfo' has no element 'UserGroupID' defined");
	}
	if (!defined($serverGroupToGroupsInfo->{'UserGroupID'} = isNumber($serverGroupToGroupsInfo->{'UserGroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToGroupsInfo' element 'UserGroupID' is invalid");
	}

	# Build result
	my $serverGroupToGroupData;
	$serverGroupToGroupData->{'ServerGroupID'} = $serverGroupToGroupsInfo->{'ServerGroupID'};
	$serverGroupToGroupData->{'UserGroupID'} = $serverGroupToGroupsInfo->{'UserGroupID'};

	my $res = smlib::Admin::ServerGroupToGroups::addServerGroupToGroup($serverGroupToGroupData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','serverGroupToGroupsID');
}



## @method removeServerGroupToGroup($serverGroupToGroupsID)
# Remove a group from a server group 
#
# @param serverGroupToGroupsID Group to server group ID
#
# @return 0 on success or < 0 on error
sub removeServerGroupToGroup
{
	my (undef,$serverGroupToGroupsID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupToGroupsID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToGroupsID' not defined");
	}
	if (!defined($serverGroupToGroupsID = isNumber($serverGroupToGroupsID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToGroupsID' element 'serverGroupToGroupsID' is invalid");
	}

	my $res = smlib::Admin::ServerGroupToGroups::removeServerGroupToGroup($serverGroupToGroupsID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}
 

## @method getServerGroupToGroups($serverGroupID,$search)
# Return server group to group info hash 
#
# @param serverGroupID Server Group ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by group to server group ID with elements...
# @li ID - Server Group to Group ID
# @li ServerGroupID - ServerGroup ID
# @li UserGroupID - User Group ID
# @li UserGroupName - User Group Name
# @li UserGroupDescription - User Group Description
sub getServerGroupToGroups
{
	my (undef,$serverGroupID,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::ServerGroupToGroups::getServerGroupToGroups($serverGroupID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('ServerGroupID','int');
	$response->addField('UserGroupID','int');
	$response->addField('UserGroupName','string');
	$response->addField('UserGroupDescription','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



# vim: ts=4
