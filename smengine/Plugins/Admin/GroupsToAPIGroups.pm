# Admin Group to API group functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class GroupsToAPIGroups
# This is admin group to API group class
package Admin::GroupsToAPIGroups;


use strict;

use Auth;

use smlib::Admin::GroupsToAPIGroups;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: GroupsToAPIGroups",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::GroupsToAPIGroups','ping','Admin/Test');

	Auth::aclAdd('Admin::GroupsToAPIGroups','getGroupsToAPIGroups','Admin/GroupsToAPIGroups/List');
	Auth::aclAdd('Admin::GroupsToAPIGroups','addGroupToAPIGroup','Admin/GroupsToAPIGroups/Add');
	Auth::aclAdd('Admin::GroupsToAPIGroups','removeGroupToAPIGroup','Admin/GroupsToAPIGroups/Remove');
	
	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method addGroupToAPIGroup($groupsToAPIGroupsInfo)
# Create a group
#
# @param groupsToAPIGroupsInfo Group to API group info hash
# @li GroupID Group ID
# @li APIGroupID API Group ID
#
# @return groupsToAPIGroupID group ID
sub addGroupToAPIGroup
{
	my (undef,$groupsToAPIGroupsInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($groupsToAPIGroupsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupsToAPIGroupsInfo' not defined");
	}
	if (!isHash($groupsToAPIGroupsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupsToAPIGroupsInfo' is not a HASH");
	}

	if (!defined($groupsToAPIGroupsInfo->{'GroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupsToAPIGroupsInfo' has no element 'GroupID' defined");
	}
	if (!defined($groupsToAPIGroupsInfo->{'GroupID'} = isNumber($groupsToAPIGroupsInfo->{'GroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupsToAPIGroupsInfo' element 'GroupID' is invalid");
	}

	if (!defined($groupsToAPIGroupsInfo->{'APIGroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupsToAPIGroupsInfo' has no element 'APIGroupID' defined");
	}
	if (!defined($groupsToAPIGroupsInfo->{'APIGroupID'} = isNumber($groupsToAPIGroupsInfo->{'APIGroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupsToAPIGroupsInfo' element 'APIGroupID' is invalid");
	}

	# Build result
	my $groupAPIGroupData;
	$groupAPIGroupData->{'GroupID'} = $groupsToAPIGroupsInfo->{'GroupID'};
	$groupAPIGroupData->{'APIGroupID'} = $groupsToAPIGroupsInfo->{'APIGroupID'};

	my $res = smlib::Admin::GroupsToAPIGroups::addGroupToAPIGroup($groupAPIGroupData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','groupsToAPIGroupID');
}



## @method removeGroupToAPIGroup($groupsToAPIGroupID)
# Remove a user from a group 
#
# @param groupsToAPIGroupID Group to API group ID
#
# @return 0 on success or < 0 on error
sub removeGroupToAPIGroup
{
	my (undef,$groupsToAPIGroupID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($groupsToAPIGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupsToAPIGroupID' not defined");
	}
	if (!defined($groupsToAPIGroupID = isNumber($groupsToAPIGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupsToAPIGroupID' element 'groupsToAPIGroupID' is invalid");
	}

	my $res = smlib::Admin::GroupsToAPIGroups::removeGroupToAPIGroup($groupsToAPIGroupID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}
 

## @method getGroupsToAPIGroups($groupID,$search)
# Return group to API group info hash 
#
# @param GroupID Group ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by user API group ID with elements...
# @li ID - Group APIGroup ID
# @li GroupID - Group ID
# @li APIGroupID - APIGroup ID
# @li APIGroupName - APIGroup Name
# @li APIGroupDescription - APIGroup Description
sub getGroupsToAPIGroups
{
	my (undef,$groupID,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($groupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupID' not defined");
	}
	if (!defined($groupID = isNumber($groupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupID' element 'groupID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::GroupsToAPIGroups::getGroupsToAPIGroups($groupID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('GroupID','int');
	$response->addField('APIGroupID','int');
	$response->addField('APIGroupName','string');
	$response->addField('APIGroupDescription','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



# vim: ts=4
