# Users to groups functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class UsersToGroups
# This is the users to groups class
package Admin::UsersToGroups;


use strict;

use Auth;

use smlib::Admin::UsersToGroups;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: UsersToGroups",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::UsersToGroups','ping','Admin/Test');

	Auth::aclAdd('Admin::UsersToGroups','getUsersToGroups','Admin/UsersToGroups/List');
	Auth::aclAdd('Admin::UsersToGroups','addUserToGroup','Admin/UsersToGroups/Add');
	Auth::aclAdd('Admin::UsersToGroups','removeUserToGroup','Admin/UsersToGroups/Remove');
	
	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method addUserToGroup($usersToGroupsInfo)
# Add a user to a group
#
# @param usersToGroupsInfo Users to groups info hash
# @li UserID User ID
# @li GroupID Group ID
#
# @return Users to groups ID
sub addUserToGroup
{
	my (undef,$usersToGroupsInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($usersToGroupsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToGroupsInfo' not defined");
	}
	if (!isHash($usersToGroupsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToGroupsInfo' is not a HASH");
	}

	if (!defined($usersToGroupsInfo->{'UserID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToGroupsInfo' has no element 'UserID' defined");
	}
	if (!defined($usersToGroupsInfo->{'UserID'} = isNumber($usersToGroupsInfo->{'UserID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToGroupsInfo' element 'UserID' is invalid");
	}

	if (!defined($usersToGroupsInfo->{'GroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToGroupsInfo' has no element 'GroupID' defined");
	}
	if (!defined($usersToGroupsInfo->{'GroupID'} = isNumber($usersToGroupsInfo->{'GroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToGroupsInfo' element 'GroupID' is invalid");
	}

	# Build result
	my $usersToGroupsData;
	$usersToGroupsData->{'UserID'} = $usersToGroupsInfo->{'UserID'};
	$usersToGroupsData->{'GroupID'} = $usersToGroupsInfo->{'GroupID'};

	my $res = smlib::Admin::UsersToGroups::addUserToGroup($usersToGroupsData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','usersToGroupsID');
}



## @method removeUserToGroup($usersToGroupsID)
# Remove a user from a group 
#
# @param usersToGroupsID Users to groups ID
#
# @return 0 on success or < 0 on error
sub removeUserToGroup
{
	my (undef,$usersToGroupsID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($usersToGroupsID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToGroupsID' not defined");
	}
	if (!defined($usersToGroupsID = isNumber($usersToGroupsID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToGroupsID' element 'usersToGroupsID' is invalid");
	}

	my $res = smlib::Admin::UsersToGroups::removeUserToGroup($usersToGroupsID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}
 

## @method getUsersToGroups()
# Return user group info hash 
#
# @param userID User ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by user group ID with elements...
# @li ID User to group ID
# @li UserID User ID
# @li GroupID Group ID
# @li GroupName Group NAme
# @li GroupDescription Group description
sub getUsersToGroups
{
	my (undef,$userID,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($userID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' not defined");
	}
	if (!defined($userID = isNumber($userID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' element 'userID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::UsersToGroups::getUsersToGroups($userID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('UserID','int');
	$response->addField('GroupID','int');
	$response->addField('GroupName','string');
	$response->addField('GroupDescription','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



# vim: ts=4
