# Admin APIGroup capabilties
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class APIGroupCapabilities
# This is the API group capabilities class
package Admin::APIGroupCapabilities;


use strict;

use Auth;

use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;

use smlib::Admin::APIGroupCapabilities;


# Plugin info
our $pluginInfo = {
	Name => "Admin: APIGroupCapabilities",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::APIGroupCapabilities','ping','Admin/APIGroups/Capabilities');

	Auth::aclAdd('Admin::APIGroupCapabilities','getAPIGroupCapability','Admin/APIGroupCapabilities/Get');
	Auth::aclAdd('Admin::APIGroupCapabilities','getAPIGroupCapabilities','Admin/APIGroupCapabilities/List');
	Auth::aclAdd('Admin::APIGroupCapabilities','addAPIGroupCapability','Admin/APIGroupCapabilities/Add');
	Auth::aclAdd('Admin::APIGroupCapabilities','updateAPIGroupCapability','Admin/APIGroupCapabilities/Update');
	Auth::aclAdd('Admin::APIGroupCapabilities','removeAPIGroupCapability','Admin/APIGroupCapabilities/Remove');

	Auth::aclAdd('Admin::APIGroupCapabilities','getAllCapabilities','Admin/APIGroupCapabilities/ListAll');
	
	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getAPIGroupCapabilites($APIGroupID,$search)
# Return API group capability info hash 
#
# @param APIGroupID - API Group ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by API group capability ID with elements...
# @li ID - API Group ID
# @li Capability - API Group Capability
sub getAPIGroupCapabilities
{
	my (undef,$APIGroupID,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($APIGroupID)) {
		return SOAPError("Parameter 'APIGroupID' not defined");
	}
	if (!defined($APIGroupID = isNumber($APIGroupID))) {
		return SOAPError("Parameter 'APIGroupID' is invalid");
	}
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::APIGroupCapabilities::getAPIGroupCapabilities($APIGroupID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Capability','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getAPIGroupCapability($capabilityID)
# Return API group capability info hash
#
# @param capabilityID APIGroup Capability ID
#
# @return API Group Capability info hash ref
# @li ID - API Group Capability ID
# @li Capability - API Group Capability
sub getAPIGroupCapability
{
	my (undef,$capabilityID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($capabilityID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'capabilityID' undefined");
	}
	if (!defined($capabilityID = isNumber($capabilityID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'capabilityID' is invalid");
	}

	# Grab and sanitize data
	my $rawData = smlib::Admin::APIGroups::getAPIGroup($capabilityID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Capability','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method addAPIGroupCapability($APIGroupCapabilityInfo)
# Create a APIGroupCapability
#
# @param APIGroupInfo APIGroup info hash
# @li APIGroupID API Group ID
# @li Capability APIGroup capability
#
# @return APIGroup ID
sub addAPIGroupCapability
{
	my (undef,$APIGroupInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($APIGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' not defined");
	}
	if (!isHash($APIGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' is not a HASH");
	}

	if (!defined($APIGroupInfo->{'APIGroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' has no element 'APIGroupID' defined");
	}
	if (!defined($APIGroupInfo->{'APIGroupID'} = isNumber($APIGroupInfo->{'APIGroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'APIGroupID' is invalid");
	}

	if (!defined($APIGroupInfo->{'Capability'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' has no element 'Capability' defined");
	}
	if (!isVariable($APIGroupInfo->{'Capability'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'Capability' is invalid");
	}
	if ($APIGroupInfo->{'Capability'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupInfo' element 'Capability' is blank");
	}

	# Build result
	my $APIGroupData;
	$APIGroupData->{'APIGroupID'} = $APIGroupInfo->{'APIGroupID'};
	$APIGroupData->{'Capability'} = $APIGroupInfo->{'Capability'};

	my $res = smlib::Admin::APIGroupCapabilities::addAPIGroupCapability($APIGroupData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','APIGroupCapabilityID');
}



## @method updateAPIGroupCapability($APIGroupCapabilityInfo)
# Update API group capability
#
# @param APIGroupCapabilityInfo API Group capability info hash ref
# @li ID API Group Capability ID
# @li Capability - API Group Capability
#
# @return 0 on success, < 0 on error
sub updateAPIGroupCapability
{
	my (undef,$APIGroupCapabilityInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($APIGroupCapabilityInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupCapabilityInfo' undefined");
	}
	if (!isHash($APIGroupCapabilityInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupCapabilityInfo' is not a HASH");
	}

	if (!defined($APIGroupCapabilityInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupCapabilityInfo' has no element 'ID'");
	}
	if (!defined($APIGroupCapabilityInfo->{'ID'} = isNumber($APIGroupCapabilityInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupCapabilityInfo' element 'ID' is invalid");
	}

	if (defined($APIGroupCapabilityInfo->{'Capability'})) {
		if (!isVariable($APIGroupCapabilityInfo->{'Capability'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupCapabilityInfo' element 'Capability' is invalid");
		}
		if ($APIGroupCapabilityInfo->{'Capability'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'APIGroupCapabilityInfo' element 'Capability' is blank");
		}
	}

	my $i;

	$i->{'ID'} = $APIGroupCapabilityInfo->{'ID'};
	$i->{'Capability'} = $APIGroupCapabilityInfo->{'Capability'} if (defined($APIGroupCapabilityInfo->{'Capability'}));

	my $res = smlib::Admin::APIGroupCapabilities::updateAPIGroupCapability($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeAPIGroupCapability($capabilityID)
# Remove an API Group Capability 
#
# @param capabilityID Capability ID
#
# @return 0 on success or < 0 on error
sub removeAPIGroupCapability
{
	my (undef,$capabilityID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($capabilityID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'capabilityID' not defined");
	}
	if (!defined($capabilityID = isNumber($capabilityID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'capabilityID' element 'capabilityID' is invalid");
	}

	my $res = smlib::Admin::APIGroupCapabilities::removeAPIGroupCapability($capabilityID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}


## @method getAllCapabilities($search)
# Return a list of all capabilities the APIGroup has access to
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Array ref of hash refs
# @li Capability - Capability
sub getAllCapabilities
{
	my (undef,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Params
	my $capabilityData;

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::APIGroupCapabilities::getAllCapabilities($search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->addField('Capability','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



1;
# vim: ts=4
