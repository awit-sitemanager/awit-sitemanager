# Servers functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class Servers
# This is the servers class
package Admin::Servers;


use strict;

use smlib::Admin::Servers;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: Servers",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::Servers','ping','Admin/Servers/Test');

	Auth::aclAdd('Admin::Servers','getServers','Admin/Servers/List');
	Auth::aclAdd('Admin::Servers','getServer','Admin/Servers/Get');
	Auth::aclAdd('Admin::Servers','createServer','Admin/Servers/Add');
	Auth::aclAdd('Admin::Servers','updateServer','Admin/Servers/Update');
	Auth::aclAdd('Admin::Servers','removeServer','Admin/Servers/Remove');

	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getServers($search)
# Return servers info hash
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by server ID with elements...
# @li ID
# @li Name
sub getServers
{
	my (undef,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::Servers::getServers($search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getServer($serverID)
# Return server info hash
#
# @param serverID Server ID
#
# @return server info hash ref
# @li ID
# @li Name
sub getServer
{
	my (undef,$serverID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverID' undefined");
	}
	if (!defined($serverID = isNumber($serverID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverID' is invalid");
	}

	# Grab and sanitize data
	my $rawData = smlib::Admin::Servers::getServer($serverID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createServer($serverInfo)
# Create a server
#
# @param serverInfo Server info hash
# @li Name server name
#
# @return Server ID
sub createServer
{
	my (undef,$serverInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' not defined");
	}
	if (!isHash($serverInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' is not a HASH");
	}

	if (!defined($serverInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' has no element 'Name' defined");
	}
	if (!isVariable($serverInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' element 'Name' is invalid");
	}
	if ($serverInfo->{'Name'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' element 'Name' is blank");
	}

	# Build result
	my $serverData;
	$serverData->{'Name'} = $serverInfo->{'Name'};

	my $res = smlib::Admin::Servers::createServer($serverData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','serverID');
}



## @method updateServer($serverInfo)
# Update server
#
# @param serverInfo server info hash ref
# @li ID server ID
# @li Name Optional server name
#
# @return 0 on success, < 0 on error
sub updateServer
{
	my (undef,$serverInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' undefined");
	}
	if (!isHash($serverInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' is not a HASH");
	}

	if (!defined($serverInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' has no element 'ID'");
	}
	if (!defined($serverInfo->{'ID'} = isNumber($serverInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' element 'ID' is invalid");
	}

	if (defined($serverInfo->{'Name'})) {
		if (!isVariable($serverInfo->{'Name'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' element 'Name' is invalid");
		}
		if ($serverInfo->{'Name'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverInfo' element 'Name' is blank");
		}
	}

	my $i;
	$i->{'ID'} = $serverInfo->{'ID'};
	$i->{'Name'} = $serverInfo->{'Name'} if (defined($serverInfo->{'Name'}));

	my $res = smlib::Admin::Servers::updateServer($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeServer($serverID)
# Remove server
#
# @param serverID server ID
#
# @return 0 on success or < 0 on error
sub removeServer
{
	my (undef,$serverID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverID' undefined");
	}
	if (!defined($serverID = isNumber($serverID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverID' is invalid");
	}

	# Grab and sanitize data
	my $res = smlib::Admin::Servers::removeServer($serverID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



1;
# vim: ts=4
