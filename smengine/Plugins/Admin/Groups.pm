# Admin Group functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class Groups
# This is the admin group class
package Admin::Groups;


use strict;

use Auth;

use smlib::Admin::Groups;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: Groups",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::Groups','ping','Admin/Test');

	Auth::aclAdd('Admin::Groups','getGroups','Admin/Groups/List');
	Auth::aclAdd('Admin::Groups','getGroup','Admin/Groups/Get');
	Auth::aclAdd('Admin::Groups','createGroup','Admin/Groups/Add');
	Auth::aclAdd('Admin::Groups','updateGroup','Admin/Groups/Update');
	Auth::aclAdd('Admin::Groups','removeGroup','Admin/Groups/Remove');
	
	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getGroups($search)
# Return group info hash 
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by group ID with elements...
# @li ID Group ID
# @li Name Group name
# @li Description Group description
sub getGroups
{
	my (undef,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::Groups::getGroups($search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('Description','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getGroup($groupID)
# Return group info hash 
#
# @param groupID Group ID
#
# @return Group info hash ref
# @li ID Group ID
# @li Name Group name
# @li Description Group description
sub getGroup
{
	my (undef,$groupID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($groupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupID' undefined");
	}
	if (!defined($groupID = isNumber($groupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupID' is invalid");
	}

	# Grab and sanitize data
	my $rawData = smlib::Admin::Groups::getGroup($groupID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('Description','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createGroup($groupInfo)
# Create a group
#
# @param groupInfo Group info hash
# @li Name Group name
# @li Description Group description
#
# @return Group ID
sub createGroup
{
	my (undef,$groupInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($groupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' not defined");
	}
	if (!isHash($groupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' is not a HASH");
	}

	if (!defined($groupInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' has no element 'Name' defined");
	}
	if (!isVariable($groupInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' element 'Name' is invalid");
	}
	if ($groupInfo->{'Name'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' element 'Name' is blank");
	}

	if (defined($groupInfo->{'Description'})) {
		if (!isVariable($groupInfo->{'Description'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' element 'Description' is invalid");
		}
		if ($groupInfo->{'Description'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' element 'Description' is blank");
		}
	}

	# Build result
	my $groupData;
	$groupData->{'Name'} = $groupInfo->{'Name'};
	$groupData->{'Description'} = $groupInfo->{'Description'} if (defined($groupInfo->{'Description'}));

	my $res = smlib::Admin::Groups::createGroup($groupData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','GroupID');
}



## @method updateGroup($groupInfo)
# Update group
#
# @param groupInfo Group info hash ref
# @li ID Group ID
# @li Name Optional Group name
# @li Description - Optional group description
#
# @return 0 on success, < 0 on error
sub updateGroup
{
	my (undef,$groupInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($groupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' undefined");
	}
	if (!isHash($groupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' is not a HASH");
	}

	if (!defined($groupInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' has no element 'ID'");
	}
	if (!defined($groupInfo->{'ID'} = isNumber($groupInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' element 'ID' is invalid");
	}

	if (defined($groupInfo->{'Name'})) {
		if (!isVariable($groupInfo->{'Name'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' element 'Name' is invalid");
		}
		if ($groupInfo->{'Name'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' element 'Name' is blank");
		}
	}

	if (defined($groupInfo->{'Description'})) {
		if (!isVariable($groupInfo->{'Description'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' element 'Description' is invalid");
		}
		if ($groupInfo->{'Description'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'groupInfo' element 'Description' is blank");
		}
	}

	my $i;

	$i->{'ID'} = $groupInfo->{'ID'};
	$i->{'Name'} = $groupInfo->{'Name'} if (defined($groupInfo->{'Name'}));
	$i->{'Description'} = $groupInfo->{'Description'} if (defined($groupInfo->{'Description'}));

	my $res = smlib::Admin::Groups::updateGroup($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeGroup($groupID)
# Remove a Group 
#
# @param groupID Group ID
#
# @return 0 on success or < 0 on error
sub removeGroup
{
	my (undef,$groupID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($groupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupID' not defined");
	}
	if (!defined($groupID = isNumber($groupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupID' element 'groupID' is invalid");
	}

	my $res = smlib::Admin::Groups::removeGroup($groupID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}
 


1;
# vim: ts=4
