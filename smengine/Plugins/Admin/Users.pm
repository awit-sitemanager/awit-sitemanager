# Admin User functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class Users
# This is the admin user class
package Admin::Users;


use strict;

use Auth;

use smlib::Admin::Users;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: Users",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::Users','ping','Admin/Test');

	Auth::aclAdd('Admin::Users','getUsers','Admin/Users/List');
	Auth::aclAdd('Admin::Users','getUser','Admin/Users/Get');
	Auth::aclAdd('Admin::Users','createUser','Admin/Users/Add');
	Auth::aclAdd('Admin::Users','updateUser','Admin/Users/Update');
	Auth::aclAdd('Admin::Users','removeUser','Admin/Users/Remove');
	
	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getUsers($search)
# Return user info hash 
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by user ID with elements...
# @li ID
# @li Username
# @li Name
sub getUsers
{
	my (undef,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::Users::getUsers($search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Username','string');
	$response->addField('Name','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getUser($userID)
# Return user info hash 
#
# @param userID User ID
#
# @return User info hash ref
# @li ID
# @li Username
# @li Name
sub getUser
{
	my (undef,$userID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($userID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' undefined");
	}
	if (!defined($userID = isNumber($userID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' is invalid");
	}

	# Grab and sanitize data
	my $rawData = smlib::Admin::Users::getUser($userID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Username','string');
	$response->addField('Name','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createUser($userInfo)
# Create a user
#
# @param userInfo User info hash
# @li Username Username
# @li Password User password
# @li Name User name/surname
#
# @return User ID
sub createUser
{
	my (undef,$userInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' not defined");
	}
	if (!isHash($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' is not a HASH");
	}

	if (!defined($userInfo->{'Username'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' has no element 'Username' defined");
	}
	if (!defined($userInfo->{'Username'} = isUsername($userInfo->{'Username'},ISUSERNAME_ALLOW_ATSIGN))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Username' is invalid");
	}

	if (!defined($userInfo->{'Password'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' has no element 'Password' defined");
	}
	if (!isVariable($userInfo->{'Password'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Password' is invalid");
	}
	if ($userInfo->{'Password'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Password' is blank");
	}

	if (!defined($userInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' has no element 'Name' defined");
	}
	if (!isVariable($userInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Name' is invalid");
	}
	if ($userInfo->{'Name'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Name' is blank");
	}

	# Build result
	my $userData;
	$userData->{'Username'} = $userInfo->{'Username'};
	$userData->{'Password'} = $userInfo->{'Password'};
	$userData->{'Name'} = $userInfo->{'Name'};

	my $res = smlib::Admin::Users::createUser($userData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','UserID');
}



## @method updateUser($userInfo)
# Update user
#
# @param userInfo User info hash ref
# @li ID User ID
# @li Password - Optional password
# @li Name - Optional name
#
# @return 0 on success, < 0 on error
sub updateUser
{
	my (undef,$userInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' undefined");
	}
	if (!isHash($userInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' is not a HASH");
	}

	if (!defined($userInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' has no element 'ID'");
	}
	if (!defined($userInfo->{'ID'} = isNumber($userInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'ID' is invalid");
	}

	if (defined($userInfo->{'Password'})) {
		if (!isVariable($userInfo->{"Password"})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Password' is invalid");
		}
		# Delete if password is empty
		if ($userInfo->{"Password"} eq "") {
			delete($userInfo->{'Password'});
		}
	}

	if (defined($userInfo->{'Name'})) {
		if (!isVariable($userInfo->{'Name'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'userInfo' element 'Name' is invalid");
		}
		if ($userInfo->{'Name'} eq "") {
			delete($userInfo->{'Password'});
		}
	}

	my $i;

	$i->{'ID'} = $userInfo->{'ID'};
	$i->{'Password'} = $userInfo->{'Password'} if (defined($userInfo->{'Password'}));
	$i->{'Name'} = $userInfo->{'Name'} if (defined($userInfo->{'Name'}));

	my $res = smlib::Admin::Users::updateUser($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeUser($userID)
# Remove user
#
# @param userID user ID
#
# @return 0 on success or < 0 on error
sub removeUser
{
	my (undef,$userID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($userID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' undefined");
	}
	if (!defined($userID = isNumber($userID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' is invalid");
	}

	# Grab and sanitize data
	my $res = smlib::Admin::Users::removeUser($userID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



1;
# vim: ts=4
