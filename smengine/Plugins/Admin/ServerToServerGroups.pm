# Server To Server Group Functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class ServerToServerGroups
# This is the server to server groups class
package Admin::ServerToServerGroups;


use strict;

use smlib::Admin::ServerToServerGroups;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: ServerToServerGroups",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::ServerToServerGroups','ping','Admin/Test');

	Auth::aclAdd('Admin::ServerToServerGroups','getServerToServerGroups','Admin/ServerToServerGroups/List');
	Auth::aclAdd('Admin::ServerToServerGroups','addServerToServerGroup','Admin/ServerToServerGroups/Add');
	Auth::aclAdd('Admin::ServerToServerGroups','removeServerToServerGroup','Admin/ServerToServerGroups/Remove');

	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method addServerToServerGroup($serverToServerGroupInfo)
# Add a server to a server group
#
# @param serverToServerGroupInfo server to server groups info hash
# @li ServerID server ID
# @li ServerGroupID server group ID
#
# @return server to server groups ID
sub addServerToServerGroup
{
	my (undef,$serverToServerGroupInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverToServerGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverToServerGroupInfo' not defined");
	}
	if (!isHash($serverToServerGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverToServerGroupInfo' is not a HASH");
	}

	if (!defined($serverToServerGroupInfo->{'ServerID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverToServerGroupInfo' has no element 'ServerID' defined");
	}
	if (!defined($serverToServerGroupInfo->{'ServerID'} = isNumber($serverToServerGroupInfo->{'ServerID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverToServerGroupInfo' element 'ServerID' is invalid");
	}

	if (!defined($serverToServerGroupInfo->{'ServerGroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverToServerGroupInfo' has no element 'ServerGroupID' defined");
	}
	if (!defined($serverToServerGroupInfo->{'ServerGroupID'} = isNumber($serverToServerGroupInfo->{'ServerGroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverToServerGroupInfo' element 'ServerGroupID' is invalid");
	}

	# Build result
	my $serverToServerGroupData;
	$serverToServerGroupData->{'ServerID'} = $serverToServerGroupInfo->{'ServerID'};
	$serverToServerGroupData->{'ServerGroupID'} = $serverToServerGroupInfo->{'ServerGroupID'};

	my $res = smlib::Admin::ServerToServerGroups::addServerToServerGroup($serverToServerGroupData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','serverToServerGroupID');
}



## @method removeServerToServerGroup($serverToServerGroupID)
# Remove a server from a server group
#
# @param serverToServerGroupID server to server group ID
#
# @return 0 on success or < 0 on error
sub removeServerToServerGroup
{
	my (undef,$serverToServerGroupID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverToServerGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverToServerGroupID' not defined");
	}
	if (!defined($serverToServerGroupID = isNumber($serverToServerGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverToServerGroupID' element 'serverToServerGroupID' is invalid");
	}

	my $res = smlib::Admin::ServerToServerGroups::removeServerToServerGroup($serverToServerGroupID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}


## @method getServerToServerGroups($serverID,$search)
# Return server to server group info hash
#
# @param serverID server ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by server group ID with elements...
# @li ID server to server group ID
# @li ServerID server ID
# @li ServerGroupID server group ID
# @li ServerGroupName server group Name
# @li ServerGroupDescription server group description
sub getServerToServerGroups
{
	my (undef,$serverID,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverID' not defined");
	}
	if (!defined($serverID = isNumber($serverID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::ServerToServerGroups::getServerToServerGroups($serverID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('ServerID','int');
	$response->addField('ServerGroupID','int');
	$response->addField('ServerGroupName','string');
	$response->addField('ServerGroupDescription','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



# vim: ts=4
