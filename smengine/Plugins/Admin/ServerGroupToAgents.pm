# Server Group To Agent Functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class ServerGroupToAgents
# This is the server group to agents class
package Admin::ServerGroupToAgents;


use strict;

use smlib::Admin::ServerGroupToAgents;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: ServerGroupToAgents",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::ServerGroupToAgents','ping','Admin/Test');

	Auth::aclAdd('Admin::ServerGroupToAgents','getServerGroupToAgents','Admin/ServerGroupToAgents/List');
	Auth::aclAdd('Admin::ServerGroupToAgents','addServerGroupToAgent','Admin/ServerGroupToAgents/Add');
	Auth::aclAdd('Admin::ServerGroupToAgents','removeServerGroupToAgent','Admin/ServerGroupToAgents/Remove');

	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method addServerGroupToAgent($serverGroupToAgentInfo)
# Add a server group to an agent
#
# @param serverGroupToAgentInfo server group to agents info hash
# @li ServerGroupID server group ID
# @li AgentID Agent ID
#
# @return server to server groups ID
sub addServerGroupToAgent
{
	my (undef,$serverGroupToAgentInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupToAgentInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToAgentInfo' not defined");
	}
	if (!isHash($serverGroupToAgentInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToAgentInfo' is not a HASH");
	}

	if (!defined($serverGroupToAgentInfo->{'ServerGroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToAgentInfo' has no element 'ServerGroupID' defined");
	}
	if (!defined($serverGroupToAgentInfo->{'ServerGroupID'} = isNumber($serverGroupToAgentInfo->{'ServerGroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToAgentInfo' element 'ServerGroupID' is invalid");
	}

	if (!defined($serverGroupToAgentInfo->{'AgentID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToAgentInfo' has no element 'AgentID' defined");
	}
	if (!defined($serverGroupToAgentInfo->{'AgentID'} = isNumber($serverGroupToAgentInfo->{'AgentID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToAgentInfo' element 'AgentID' is invalid");
	}

	# Build result
	my $serverGroupToAgentData;
	$serverGroupToAgentData->{'ServerGroupID'} = $serverGroupToAgentInfo->{'ServerGroupID'};
	$serverGroupToAgentData->{'AgentID'} = $serverGroupToAgentInfo->{'AgentID'};

	my $res = smlib::Admin::ServerGroupToAgents::addServerGroupToAgent($serverGroupToAgentData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','serverGroupToAgentID');
}



## @method removeServerGroupToAgent($serverGroupToAgentID)
# Remove a server group from an agent
#
# @param serverGroupToAgentID server group to agent ID
#
# @return 0 on success or < 0 on error
sub removeServerGroupToAgent
{
	my (undef,$serverGroupToAgentID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupToAgentID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToAgentID' not defined");
	}
	if (!defined($serverGroupToAgentID = isNumber($serverGroupToAgentID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupToAgentID' element 'serverGroupToAgentID' is invalid");
	}

	my $res = smlib::Admin::ServerGroupToAgents::removeServerGroupToAgent($serverGroupToAgentID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}


## @method getServerGroupToAgents($serverGroupID,$search)
# Return server group to agent info hash
#
# @param serverGroupID server group ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by server group ID with elements...
# @li ID server to server group ID
# @li ServerGroupID server ID
# @li AgentID server group ID
# @li AgentName server group Name
sub getServerGroupToAgents
{
	my (undef,$serverGroupID,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ServerGroupID' not defined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'ServerGroupID' element 'ServerGroupID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::ServerGroupToAgents::getServerGroupToAgents($serverGroupID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('ServerGroupID','int');
	$response->addField('AgentID','int');
	$response->addField('AgentName','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



# vim: ts=4
