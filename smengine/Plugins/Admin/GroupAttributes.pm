# Admin Group attributes functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class GroupAttributes
# This is the admin group attributes class
package Admin::GroupAttributes;


use strict;

use Auth;

use smlib::Admin::GroupAttributes;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: GroupAttributes",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::GroupAttributes','ping','Admin/Test');

	Auth::aclAdd('Admin::GroupAttributes','getGroupAttributes','Admin/GroupAttributes/List');
	Auth::aclAdd('Admin::GroupAttributes','getGroupAttribute','Admin/GroupAttributes/Get');
	Auth::aclAdd('Admin::GroupAttributes','createGroupAttribute','Admin/GroupAttributes/Add');
	Auth::aclAdd('Admin::GroupAttributes','updateGroupAttribute','Admin/GroupAttributes/Update');
	Auth::aclAdd('Admin::GroupAttributes','removeGroupAttribute','Admin/GroupAttributes/Remove');
	
	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getGroupAttributes($groupID,$search)
# Return group attributes info hash 
#
# @param groupID Group ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by user ID with elements...
# @li ID
# @li Attribute
sub getGroupAttributes
{
	my (undef,$groupID,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($groupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupID' undefined");
	}
	if (!defined($groupID = isNumber($groupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'groupID' is invalid");
	}

	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::GroupAttributes::getGroupAttributes($groupID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Attribute','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getGroupAttribute($attributeID)
# Return group attribute info hash 
#
# @param attributeID Attribute ID
#
# @return group attribute info hash ref
# @li ID
# @li Attribute
sub getGroupAttribute
{
	my (undef,$attributeID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' undefined");
	}
	if (!defined($attributeID = isNumber($attributeID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' is invalid");
	}

	# Grab and sanitize data
	my $rawData = smlib::Admin::GroupAttributes::getGroupAttribute($attributeID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Attribute','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createGroupAttribute($attributeInfo)
# Create a group attribute
#
# @param attributeInfo Attribute info hash
# @li GroupID Group ID
# @li Attribute Group attribute
#
# @return ID Attribute ID
sub createGroupAttribute
{
	my (undef,$attributeInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' not defined");
	}
	if (!isHash($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' is not a HASH");
	}

	if (!defined($attributeInfo->{'GroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'Attribute' defined");
	}
	if (!defined($attributeInfo->{'GroupID'} = isNumber($attributeInfo->{'GroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is invalid");
	}

	if (!defined($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'Attribute' defined");
	}
	if (!isVariable($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is invalid");
	}
	if ($attributeInfo->{'Attribute'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is blank");
	}

	# Build result
	my $attributeData;
	$attributeData->{'GroupID'} = $attributeInfo->{'GroupID'};
	$attributeData->{'Attribute'} = $attributeInfo->{'Attribute'};

	my $res = smlib::Admin::GroupAttributes::createGroupAttribute($attributeData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','ID');
}



## @method updateGroupAttribute($attributeInfo)
# Update group attribute
#
# @param attributeInfo Attribute info hash ref
# @li ID Attribute ID
# @li Attribute - Attribute
#
# @return 0 on success, < 0 on error
sub updateGroupAttribute
{
	my (undef,$attributeInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' undefined");
	}
	if (!isHash($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' is not a HASH");
	}

	if (!defined($attributeInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'ID'");
	}
	if (!defined($attributeInfo->{'ID'} = isNumber($attributeInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'ID' is invalid");
	}

	if (!defined($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'Attribute'");
	}
	if (!isVariable($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is invalid");
	}
	if ($attributeInfo->{'Attribute'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is blank");
	}

	my $i;
	$i->{'ID'} = $attributeInfo->{'ID'};
	$i->{'Attribute'} = $attributeInfo->{'Attribute'};

	my $res = smlib::Admin::GroupAttributes::updateGroupAttribute($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeGroupAttribute($attributeID)
# Remove group attribute
#
# @param attributeID Attribute ID
#
# @return 0 on success or < 0 on error
sub removeGroupAttribute
{
	my (undef,$attributeID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' undefined");
	}
	if (!defined($attributeID = isNumber($attributeID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' is invalid");
	}

	# Grab and sanitize data
	my $res = smlib::Admin::GroupAttributes::removeGroupAttribute($attributeID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



1;
# vim: ts=4
