# Server Groups functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class ServerGroups
# This is the server groups class
package Admin::ServerGroups;


use strict;

use smlib::Admin::ServerGroups;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: ServerGroups",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::ServerGroups','ping','Admin/ServerGroups/Test');

	Auth::aclAdd('Admin::ServerGroups','getServerGroups','Admin/ServerGroups/List');
	Auth::aclAdd('Admin::ServerGroups','getServerGroup','Admin/ServerGroups/Get');
	Auth::aclAdd('Admin::ServerGroups','createServerGroup','Admin/ServerGroups/Add');
	Auth::aclAdd('Admin::ServerGroups','updateServerGroup','Admin/ServerGroups/Update');
	Auth::aclAdd('Admin::ServerGroups','removeServerGroup','Admin/ServerGroups/Remove');

	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getServerGroups($search)
# Return server groups info hash
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by server group ID with elements...
# @li ID
# @li Name
# @li Description
sub getServerGroups
{
	my (undef,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::ServerGroups::getServerGroups($search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('Description','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getServerGroup($serverGroupID)
# Return server group info hash
#
# @param serverGroupID Server Group ID
#
# @return server group info hash ref
# @li ID
# @li Name
# @li Description
sub getServerGroup
{
	my (undef,$serverGroupID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	# Grab and sanitize data
	my $rawData = smlib::Admin::ServerGroups::getServerGroup($serverGroupID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Name','string');
	$response->addField('Description','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createServerGroup($serverGroupInfo)
# Create a server group
#
# @param serverGroupInfo Server info hash
# @li Name server group name
#
# @return Server group ID
sub createServerGroup
{
	my (undef,$serverGroupInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' not defined");
	}
	if (!isHash($serverGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' is not a HASH");
	}

	if (!defined($serverGroupInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' has no element 'Name' defined");
	}
	if (!isVariable($serverGroupInfo->{'Name'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'Name' is invalid");
	}
	if ($serverGroupInfo->{'Name'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'Name' is blank");
	}

	if (defined($serverGroupInfo->{'Description'})) {
		if (!isVariable($serverGroupInfo->{'Description'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'Description' is invalid");
		}
		if ($serverGroupInfo->{'Description'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'Description' is blank");
		}
	}

	# Build result
	my $serverGroupData;
	$serverGroupData->{'Name'} = $serverGroupInfo->{'Name'};
	$serverGroupData->{'Description'} = $serverGroupInfo->{'Description'} if defined($serverGroupInfo->{'Description'});

	my $res = smlib::Admin::ServerGroups::createServerGroup($serverGroupData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','serverGroupID');
}



## @method updateServerGroup($serverGroupInfo)
# Update server group
#
# @param serverGroupInfo server group info hash ref
# @li ID server group ID
# @li Name Optional server group name
# @li Description Optional server group description
#
# @return 0 on success, < 0 on error
sub updateServerGroup
{
	my (undef,$serverGroupInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' undefined");
	}
	if (!isHash($serverGroupInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' is not a HASH");
	}

	if (!defined($serverGroupInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' has no element 'ID'");
	}
	if (!defined($serverGroupInfo->{'ID'} = isNumber($serverGroupInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'ID' is invalid");
	}

	if (defined($serverGroupInfo->{'Name'})) {
		if (!isVariable($serverGroupInfo->{'Name'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'Name' is invalid");
		}
		if ($serverGroupInfo->{'Name'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'Name' is blank");
		}
	}

	if (defined($serverGroupInfo->{'Description'})) {
		if (!isVariable($serverGroupInfo->{'Description'})) {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'Description' is invalid");
		}
		if ($serverGroupInfo->{'Description'} eq "") {
			return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupInfo' element 'Description' is blank");
		}
	}

	my $i;
	$i->{'ID'} = $serverGroupInfo->{'ID'};
	$i->{'Name'} = $serverGroupInfo->{'Name'} if (defined($serverGroupInfo->{'Name'}));
	$i->{'Description'} = $serverGroupInfo->{'Description'} if (defined($serverGroupInfo->{'Description'}));

	my $res = smlib::Admin::ServerGroups::updateServerGroup($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeServerGroup($serverGroupID)
# Remove server group
#
# @param serverGroupID server group ID
#
# @return 0 on success or < 0 on error
sub removeServerGroup
{
	my (undef,$serverGroupID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	# Grab and sanitize data
	my $res = smlib::Admin::ServerGroups::removeServerGroup($serverGroupID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



1;
# vim: ts=4
