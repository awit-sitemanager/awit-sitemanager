# Users to API groups functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class UsersToAPIGroups
# This is the users to api groups class
package Admin::UsersToAPIGroups;


use strict;

use Auth;

use smlib::Admin::UsersToAPIGroups;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: UsersToAPIGroups",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::UsersToAPIGroups','ping','Admin/Test');

	Auth::aclAdd('Admin::UsersToAPIGroups','getUsersToAPIGroups','Admin/UsersToAPIGroups/List');
	Auth::aclAdd('Admin::UsersToAPIGroups','addUserToAPIGroup','Admin/UsersToAPIGroups/Add');
	Auth::aclAdd('Admin::UsersToAPIGroups','removeUserToAPIGroup','Admin/UsersToAPIGroups/Remove');
	
	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method addUserToAPIGroup($usersToAPIGroupsInfo)
# Add a user to a api group
#
# @param usersToAPIGroupsInfo Users to api groups info hash
# @li UserID User ID
# @li APIGroupID API Group ID
#
# @return Users to api groups ID
sub addUserToAPIGroup
{
	my (undef,$usersToAPIGroupsInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($usersToAPIGroupsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToAPIGroupsInfo' not defined");
	}
	if (!isHash($usersToAPIGroupsInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToAPIGroupsInfo' is not a HASH");
	}

	if (!defined($usersToAPIGroupsInfo->{'UserID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToAPIGroupsInfo' has no element 'UserID' defined");
	}
	if (!defined($usersToAPIGroupsInfo->{'UserID'} = isNumber($usersToAPIGroupsInfo->{'UserID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToAPIGroupsInfo' element 'UserID' is invalid");
	}

	if (!defined($usersToAPIGroupsInfo->{'APIGroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToAPIGroupsInfo' has no element 'APIGroupID' defined");
	}
	if (!defined($usersToAPIGroupsInfo->{'APIGroupID'} = isNumber($usersToAPIGroupsInfo->{'APIGroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToAPIGroupsInfo' element 'APIGroupID' is invalid");
	}

	# Build result
	my $usersToAPIGroupsData;
	$usersToAPIGroupsData->{'UserID'} = $usersToAPIGroupsInfo->{'UserID'};
	$usersToAPIGroupsData->{'APIGroupID'} = $usersToAPIGroupsInfo->{'APIGroupID'};

	my $res = smlib::Admin::UsersToAPIGroups::addUserToAPIGroup($usersToAPIGroupsData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','usersToAPIGroupsID');
}



## @method removeUserToAPIGroup($usersToAPIGroupsID)
# Remove a user from a api group 
#
# @param usersToAPIGroupsID Users to groups ID
#
# @return 0 on success or < 0 on error
sub removeUserToAPIGroup
{
	my (undef,$usersToAPIGroupsID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($usersToAPIGroupsID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToAPIGroupsID' not defined");
	}
	if (!defined($usersToAPIGroupsID = isNumber($usersToAPIGroupsID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'usersToAPIGroupsID' element 'usersToAPIGroupsID' is invalid");
	}

	my $res = smlib::Admin::UsersToAPIGroups::removeUserToAPIGroup($usersToAPIGroupsID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}
 

## @method getUsersToAPIGroups($userID,$search)
# Return user api group info hash 
#
# @param userID User ID
#
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by user group ID with elements...
# @li ID User to group ID
# @li UserID User ID
# @li APIGroupID APIGroup ID
# @li APIGroupName APIGroup NAme
# @li APIGroupDescription APIGroup description
sub getUsersToAPIGroups
{
	my (undef,$userID,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($userID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' not defined");
	}
	if (!defined($userID = isNumber($userID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'userID' element 'userID' is invalid");
	}

	# Check params
	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::UsersToAPIGroups::getUsersToAPIGroups($userID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('UserID','int');
	$response->addField('APIGroupID','int');
	$response->addField('APIGroupName','string');
	$response->addField('APIGroupDescription','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



# vim: ts=4
