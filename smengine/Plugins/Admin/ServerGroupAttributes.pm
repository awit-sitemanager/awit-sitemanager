# Admin Server Group Attributes
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



## @class ServerGroupAttributes
# This is the admin server group attributes class
package Admin::ServerGroupAttributes;


use strict;

use smlib::Admin::ServerGroupAttributes;
use smlib::constants;
use smlib::logging;
use smlib::soap;
use smlib::util;



# Plugin info
our $pluginInfo = {
	Name => "Admin: ServerGroupAttributes",
	Init => \&init,
};


## @internal
# Initialize module
sub init
{
	my $server = shift;

	Auth::aclAdd('Admin::ServerGroupAttributes','ping','Admin/ServerGroupAttributes/Test');

	Auth::aclAdd('Admin::ServerGroupAttributes','getServerGroupAttributes','Admin/ServerGroupAttributes/List');
	Auth::aclAdd('Admin::ServerGroupAttributes','getServerGroupAttribute','Admin/ServerGroupAttributes/Get');
	Auth::aclAdd('Admin::ServerGroupAttributes','createServerGroupAttribute','Admin/ServerGroupAttributes/Add');
	Auth::aclAdd('Admin::ServerGroupAttributes','updateServerGroupAttribute','Admin/ServerGroupAttributes/Update');
	Auth::aclAdd('Admin::ServerGroupAttributes','removeServerGroupAttribute','Admin/ServerGroupAttributes/Remove');

	return RES_OK;
}


## @method ping
# Ping function to allow one to see if they get a positive response from
# this module
#
# @return Will always return 0
sub ping
{
	return 0;
}



## @method getServerGroupAttributes($serverGroupID,$search)
# Return server group attributes info hash
#
# @param serverGroupID Server Group ID
# @param search Search hash ref, @see DBSelectSearch
#
# @return Hash ref indexed by user ID with elements...
# @li ID
# @li Attribute
sub getServerGroupAttributes
{
	my (undef,$serverGroupID,$search) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($serverGroupID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' undefined");
	}
	if (!defined($serverGroupID = isNumber($serverGroupID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'serverGroupID' is invalid");
	}

	if (defined($search) && !isHash($search)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'search' is not a HASH");
	}

	# Grab and sanitize data
	my ($rawData,$numResults) = smlib::Admin::ServerGroupAttributes::getServerGroupAttributes($serverGroupID,$search);
	if (ref($rawData) ne "ARRAY") {
		return SOAPError($rawData,getError());
	}

	# Build response
	my $response = smlib::soap::response->new();
	$response->setID('ID');
	$response->addField('ID','int');
	$response->addField('Attribute','string');
	$response->parseArrayRef($rawData);
	$response->setDatasetSize($numResults);

	return $response->export();
}



## @method getServerGroupAttribute($attributeID)
# Return server group attribute info hash
#
# @param attributeID Attribute ID
#
# @return server group attribute info hash ref
# @li ID
# @li Attribute
sub getServerGroupAttribute
{
	my (undef,$attributeID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' undefined");
	}
	if (!defined($attributeID = isNumber($attributeID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' is invalid");
	}

	# Grab and sanitize data
	my $rawData = smlib::Admin::ServerGroupAttributes::getServerGroupAttribute($attributeID);
	if (!isHash($rawData)) {
		return SOAPError($rawData,getError());
	}

	my $response = smlib::soap::response->new();
	$response->addField('ID','int');
	$response->addField('Attribute','string');
	$response->parseHashRef($rawData);

	return $response->export();
}



## @method createServerGroupAttribute($attributeInfo)
# Create a server group attribute
#
# @param attributeInfo Attribute info hash
# @li ServerGroupID server ID
# @li Attribute server group attribute
#
# @return ID Attribute ID
sub createServerGroupAttribute
{
	my (undef,$attributeInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' not defined");
	}
	if (!isHash($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' is not a HASH");
	}

	if (!defined($attributeInfo->{'ServerGroupID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'Attribute' defined");
	}
	if (!defined($attributeInfo->{'ServerGroupID'} = isNumber($attributeInfo->{'ServerGroupID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is invalid");
	}

	if (!defined($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'Attribute' defined");
	}
	if (!isVariable($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is invalid");
	}
	if ($attributeInfo->{'Attribute'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is blank");
	}

	# Build result
	my $attributeData;
	$attributeData->{'ServerGroupID'} = $attributeInfo->{'ServerGroupID'};
	$attributeData->{'Attribute'} = $attributeInfo->{'Attribute'};

	my $res = smlib::Admin::ServerGroupAttributes::createServerGroupAttribute($attributeData);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess($res,'int','ID');
}



## @method updateServerGroupAttribute($attributeInfo)
# Update server group attribute
#
# @param attributeInfo Attribute info hash ref
# @li ID Attribute ID
# @li Attribute - Attribute
#
# @return 0 on success, < 0 on error
sub updateServerGroupAttribute
{
	my (undef,$attributeInfo) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' undefined");
	}
	if (!isHash($attributeInfo)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' is not a HASH");
	}

	if (!defined($attributeInfo->{'ID'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'ID'");
	}
	if (!defined($attributeInfo->{'ID'} = isNumber($attributeInfo->{'ID'}))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'ID' is invalid");
	}

	if (!defined($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' has no element 'Attribute'");
	}
	if (!isVariable($attributeInfo->{'Attribute'})) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is invalid");
	}
	if ($attributeInfo->{'Attribute'} eq "") {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeInfo' element 'Attribute' is blank");
	}

	my $i;
	$i->{'ID'} = $attributeInfo->{'ID'};
	$i->{'Attribute'} = $attributeInfo->{'Attribute'};

	my $res = smlib::Admin::ServerGroupAttributes::updateServerGroupAttribute($i);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



## @method removeServerGroupAttribute($attributeID)
# Remove server group attribute
#
# @param attributeID Attribute ID
#
# @return 0 on success or < 0 on error
sub removeServerGroupAttribute
{
	my (undef,$attributeID) = @_;


	# Check if we're an admin or not...
	if (!Auth::userHasAuthority('Admin')) {
		return SOAPError(ERR_UNKNOWN,"Your authority is not supported by this function");
	}

	# Check params
	if (!defined($attributeID)) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' undefined");
	}
	if (!defined($attributeID = isNumber($attributeID))) {
		return SOAPError(ERR_S_PARAM,"Parameter 'attributeID' is invalid");
	}

	# Grab and sanitize data
	my $res = smlib::Admin::ServerGroupAttributes::removeServerGroupAttribute($attributeID);
	if ($res < 0) {
		return SOAPError($res,getError());
	}

	return SOAPSuccess();
}



1;
# vim: ts=4
