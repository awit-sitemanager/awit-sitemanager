# SOAP test functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
# Copyright (C) 2008, LinuxRulz
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


## @class SOAPTest
# Test functions for soap implementation
package SOAPTest;

use strict;
use warnings;

use Auth;

use smlib::constants;


# Plugin info
our $pluginInfo = {
	Name => "SOAP Test",
	Init => \&init,
};


## @internal
# Initialize module
sub init {
	my $server = shift;

	Auth::aclAdd('SOAPTest','ping','SOAP/Test');
	Auth::aclAdd('SOAPTest','paramTest','SOAP/Test');

	return RES_OK;
}


## @method ping()
# Function to test out your SOAP implementation
#
# @return Will return the unix time index
sub ping {

	return "Pong reply - Unix time index ".time().".";
}


## @method paramTest($a,$b,$c)
# Function to test out your SOAP implementation
#
# @param a Test parameter 1
# @param b Test parameter 1
# @param c Test parameter 1
# @return Will return a concatenation of a, b and c
sub paramTest {
	my ($m,$a,$b,$c) = @_;

	return "Params - Param 1: \"$a\", Param 2: \"$b\", Param 3: \"$c\"";
}



1;
# vim: ts=4
