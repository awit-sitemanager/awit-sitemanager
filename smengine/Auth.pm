# SOAP authentication functions
# Copyright (C) 2008-2011, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


# Predefined groups:
#	everybody
#	admins


## @internal
## @class Auth
# Authentication package
package Auth;

use strict;
use warnings;

use smlib::version;
use smlib::constants;
use smlib::logging;

use SOAP::Lite;

require Exporter;
our (@ISA,@EXPORT);
@ISA = qw(Exporter);
@EXPORT = qw(
);



# Authentication data
my $authData;

# Access control list
my @acl;

# Auth types
my @authTypes;


# Server instance
my $server;


## @fn init(server)
# Initialize ACLs, this sets our $server variable
#
# @param server Server instance
sub init {
	my $srvInstance = shift;

	$server = $srvInstance;
}



## @fn aclAdd(package,function,capability)
# Add required ACL item
#
# @param package Package name
# @param function Function name
# @param capability Capability required to access this function
sub aclAdd {
	my ($package,$function,$capability) = @_;

	# Setup item info
	my $itemInfo;
	$itemInfo->{'package'} = $package;
	$itemInfo->{'function'} = $function;
	$itemInfo->{'capability'} = $capability;
	push(@acl,$itemInfo);

	# Map function into dispatch
	$server->map_dispatch($package,$function);
}


## @fn aclGetCapabilities
# Return list of currently loaded capabilities
#
# @return Array ref of capabilities
sub aclGetCapabilities {
	my %capabilityHash;

	# Loop with access controls
	foreach my $i (@acl) {
		# Loop with uses capabilities
		foreach my $capability (@{$authData->{'UserInfo'}->{'APICapabilities'}}) {
			$capabilityHash{$capability} = 1;
		}
	}

	# Build capability list array
	my @capabilityList = sort keys %capabilityHash;

	# Return array ref
	return \@capabilityList;
}


## @fn sessionCreate($server)
# Create session
#
# @param server Server instance
# @param data UserInfo structure we generated earlier when we authenticated
sub sessionCreate {
	my $server = shift;

	# Set and reset data
	$authData = {
		'_server' => $server,
		'Peer' => $server->{'server'}->{'peeraddr'}
	};
}


## @fn sessionSetAuthInfo($server,$userData)
# Create session
#
# @param server Server instance
# @param data UserInfo structure we generated earlier when we authenticated
sub sessionSetAuthInfo {
	my ($server,$userData) = @_;

	# Set user info
	$authData->{'UserInfo'} = $userData;
}


## @fn sessionDestroy
# Destroy session data
sub sessionDestroy {
	# Destroy session
	$authData = undef;

	$server->log(LOG_DEBUG,"[AUTH] SESSION DESTROY");
}

## @fn sessionSetData($name,$value)
# Set arbitrary session data
#
# @param name Name of item to set a value for
# @param value Value to set the name to
sub sessionSetData {
	my ($name,$value) = @_;

	# Set value
	$authData->{'_data'}->{$name} = $value;
}

## @fn sessionGetData($name)
# Get arbitrary session data
#
# @param name Name of item to get value of
sub sessionGetData {
	my ($name) = @_;

	# Get value
	return $authData->{'_data'}->{$name};
}



## @fn checkAccess(uri,package,function)
# Return 0 if we authorized, -1 if not
#
# @param uri URI in request
# @param package Package access is being requested to
# @param function Function in the package the access is being requested to
#
# @return 0 if we authorized, -1 if not
sub accessCheck {
	my ($server,$uri,$rawPackage,$function) = @_;

	# SOAP::Lite not like this ...
	if ($rawPackage =~ /::/) {
		my $msg = sprintf("Access denied to '%s', the use of '::' is deprecated. Use '/' instead",$rawPackage);
		# Something odd happened?
		$server->log(LOG_WARN,"[AUTH] $msg");
		# Set logger error
		$server->soap_result_logger($server,$msg);
		die SOAP::Fault->faultcode($SOAP::Constants::FAULT_CLIENT)->faultstring("Access denied. Deprecated module specification. Use '/' not '::'.");
	}

	# Clean up package name
	my $package = $rawPackage;
	$package =~ s#/#::#g;

	# Check we have session data
	if (!defined($authData)) {
		my $msg = sprintf("Access denied to %s::%s, No active session",$package,$function);
		# Something odd happened?
		$server->log(LOG_WARN,"[AUTH] $msg");
		# Set logger error
		$server->soap_result_logger($server,$msg);
		die SOAP::Fault->faultcode($SOAP::Constants::FAULT_CLIENT)->faultstring("Access denied. No active session");
	}

	# Loop with access controls
	foreach my $i (@acl) {
		# Next if we don't match
		next if ($i->{'package'} ne $package || $i->{'function'} ne $function);

		# Loop with uses capabilities
		foreach my $capability (@{$authData->{'UserInfo'}->{'APICapabilities'}}) {
				# Check if our capability matches
				if (lc($i->{'capability'}) eq lc($capability)) {
					$server->log(LOG_INFO,"[AUTH] Function ${package}::${function} accessed by '".
							$authData->{'UserInfo'}->{'Username'}."' [".$authData->{'Peer'}."]");
					return;
				}
		}
	}

	# Setup error message
	my $msg = sprintf("Access denied to %s::%s from '%s' [%s]",$package,$function,
			$authData->{'UserInfo'}->{'Username'},$authData->{'Peer'});
	# If we got here, we didn't return, so we not authorized
	$server->log(LOG_WARN,"[AUTH] $msg");
	# Set logger error
	$server->soap_result_logger($server,$msg);

	# Return error back to SOAP
	die SOAP::Fault->faultcode($SOAP::Constants::FAULT_CLIENT)->faultstring("Access denied to ${package}::${function}");
}



## @fn pluginAdd($info)
# Add a plugin
#
# @param Name Plugin name
# @param Type Plugin type
# @param Auth Auth pointer
#
# @return RES_OK on success, RES_ERROR on failure
sub pluginAdd
{
	my $info = shift;

	if (!defined($info)) {
		$server->log(LOG_ERR,"[AUTH] No plugin info");
		return RES_ERROR;
	}
	if (!defined($info->{'Name'})) {
		$server->log(LOG_ERR,"[AUTH] No plugin info element 'Name'");
		return RES_ERROR;
	}
	if (!defined($info->{'Type'})) {
		$server->log(LOG_ERR,"[AUTH] No plugin info element 'Type' for '".$info->{'Name'}."'");
		return RES_ERROR;
	}
	if (!defined($info->{'Authenticate'})) {
		$server->log(LOG_ERR,"[AUTH] No plugin info element 'Authenticate' for '".$info->{'Name'}."'");
		return RES_ERROR;
	}

	push(@authTypes,$info);

	$server->log(LOG_DEBUG,"[AUTH] Authentication plugin '".$info->{'Name'}."' loaded");

	return RES_OK;
}


## @fn userAuthenticate($type,$username,$password)
# Try authenticate a user
#
# @param type Authentication type, eg. 'Admin' or 'Agent'
# @param username Username to authenticate
# @param password User password
#
# @return User hash
# @li plugin - Plugin pointer that authenticated this user
# @li data - Data we got back from the plugin
sub userAuthenticate
{
	my ($type,$username,$password) = @_;


	# Look to see if a plugin matches this auth type
	my $found;
	foreach my $authMod (@authTypes) {
		if (lc($type) eq lc($authMod->{'Type'})) {
			$found = $authMod;
			last;
		}
	}

	# Check if we didn't find anything
	if (!defined($found)) {
		$server->log(LOG_WARN,"[AUTH] No authentication plugin for '$type' found");
		return undef;
	}

	my $res;

	# Check if we managed to authenticate, if its not a hash its FAILED?
	my $userInfo = $found->{'Authenticate'}->($username,$password);
	if (ref($userInfo) eq 'HASH') {
		$res = $userInfo;
		$res->{'Authenticated'} = 1;
	}

	# Set plugin used to authenticate
	$res->{'Plugin'} = $found;

	return $res;
}


## @fn userHasAuthority($auth)
# Check if user has specified authority
#
# @param auth Authority to look for
#
# @return 1 if true, 0 if not
sub userHasAuthority
{
	my $auth = shift;


	# Grab authority
	if (my $authority = getUserAttribute('Authority')) {
		# Check for match
		if ($authority eq $auth) {
			return 1;
		}
	}

	return 0;
}


## @fn getUserInfoItem($item)
# Return item of currently logged in user info hash
#
# @param item Item name
#
# @return Item value or undef if it doesn't exist
sub getUserInfoItem
{
	my $item = shift;


	# Check if we have everything
	if (!defined($authData)) {
		$server->log(LOG_ERR,"[AUTH] Get attribute on undefined authData");
		return undef;
	}
	if (!defined($authData->{'UserInfo'})) {
		return undef;
	}
	# Check if we have an attr
	if (!defined($authData->{'UserInfo'}->{$item})) {
		return undef;
	}

	return $authData->{'UserInfo'}->{$item};
}


## @fn getUserAttribute($attr)
# Return attribute of currently logged in user
#
# @param attr Attribute name
#
# @return Attribute or undef if it doesn't exist
sub getUserAttribute
{
	my $attr = shift;


	# Check if we have everything
	if (!defined($authData)) {
		$server->log(LOG_ERR,"[AUTH] Get attribute on undefined authData");
		return undef;
	}
	if (!defined($authData->{'UserInfo'})) {
		return undef;
	}
	if (!defined($authData->{'UserInfo'}->{'UserAttributes'})) {
		return undef;
	}
	# Check if we have an attr
	if (!defined($authData->{'UserInfo'}->{'UserAttributes'}->{$attr})) {
		return undef;
	}

	return $authData->{'UserInfo'}->{'UserAttributes'}->{$attr};
}


## @fn userGetCapabilities()
# Return users' capabilities
#
# @return 1 if true, 0 if not
sub userGetAPICapabilities
{
	return $authData->{'UserInfo'}->{'APICapabilities'};
}




1;
# vim: ts=4
