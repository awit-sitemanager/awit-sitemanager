CREATE TABLE sites (
	ID		SERIAL,
	AgentID		BIGINT UNSIGNED NOT NULL,
	DomainName	TINYTEXT NOT NULL,
	DomainAliases	TEXT,
	HomeDirectory	TEXT NOT NULL,
	DiskQuota	MEDIUMINT UNSIGNED, /* In Mbyte */
	DiskUsage	INT UNSIGNED, /* In Kbyte */

	HttpdConfFile	TEXT NOT NULL,
	SiteUID		INT UNSIGNED,
	SiteGID		INT UNSIGNED,

	PRIMARY KEY (ID),
	FOREIGN KEY (AgentID) REFERENCES agents(ID)
) ENGINE=InnoDB;


CREATE TABLE siteTrafficUsage (
	SiteID		BIGINT UNSIGNED NOT NULL,
	TimeFrame	INT UNSIGNED NOT NULL,
	TrafficUsage	INT UNSIGNED NOT NULL,

	FOREIGN KEY (SiteID) REFERENCES sites(ID)
) ENGINE=InnoDB;


CREATE TABLE ftpUsers (
	ID		SERIAL,
	SiteID		BIGINT UNSIGNED NOT NULL,
	Username	TINYTEXT NOT NULL,
	Password	TINYTEXT NOT NULL,
	UserID		INT UNSIGNED NOT NULL,
	GroupID		INT UNSIGNED NOT NULL,
	HomeDirectory	TEXT NOT NULL,

	PRIMARY KEY (ID),
	FOREIGN KEY (SiteID) REFERENCES sites(ID)
) ENGINE=InnoDB;


CREATE TABLE ftpGroups (
	GroupID		INT UNSIGNED NOT NULL,
	GroupName	TINYTEXT NOT NULL,
	Members		TINYTEXT	
) ENGINE=InnoDB;

INSERT INTO ftpGroups (GroupID,GroupName) VALUES (10000,'webserver');


CREATE TABLE systemDatabases (
	ID		SERIAL,
	Type		TINYTEXT NOT NULL,
	Server		TINYTEXT NOT NULL,
	DSN		TINYTEXT NOT NULL, 
	AdminUsername	TINYTEXT NOT NULL, 
	AdminPassword	TINYTEXT NOT NULL, 
	ClientDetails	TEXT,

	PRIMARY KEY (ID)
) ENGINE=InnoDB;

INSERT INTO systemDatabases (Type,Server,DSN,AdminUsername,AdminPassword) 
	VALUES ('MySQL','localhost','DBI:mysql:database=mysql;host=localhost','mysql','');

INSERT INTO systemDatabases (Type,Server,DSN,AdminUsername,AdminPassword) 
	VALUES ('PostgreSQL','localhost','DBI:Pg:database=template1;host=localhost','postgres','');


CREATE TABLE siteDatabases (
	ID		SERIAL,
	SiteID		BIGINT UNSIGNED NOT NULL,
	SystemDBID	BIGINT UNSIGNED NOT NULL,
	Name		TINYTEXT NOT NULL, 
	Path		TINYTEXT, 

	PRIMARY KEY (ID),
	FOREIGN KEY (SiteID) REFERENCES sites(ID),
	FOREIGN KEY (SystemDBID) REFERENCES systemDatabases(ID)
) ENGINE=InnoDB;


CREATE TABLE siteDatabaseUsers (
	ID		SERIAL,
	DatabaseID	BIGINT UNSIGNED NOT NULL,
	Username	TINYTEXT NOT NULL,

	PRIMARY KEY (ID),
	FOREIGN KEY (DatabaseID) REFERENCES siteDatabases(ID)
) ENGINE=InnoDB;


CREATE TABLE siteFrontpageExt (
	ID		SERIAL,
	SiteID		BIGINT UNSIGNED NOT NULL,
	FakeHttpdCfg	TEXT NOT NULL,

	PRIMARY KEY (ID),
	FOREIGN KEY (SiteID) REFERENCES sites(ID)
) ENGINE=InnoDB;


CREATE TABLE siteStats (
	ID		SERIAL,
	SiteID		BIGINT UNSIGNED NOT NULL,
	StatsEngine	TINYTEXT NOT NULL,
	StatsDir	TEXT NOT NULL,

	PRIMARY KEY (ID),
	FOREIGN KEY (SiteID) REFERENCES sites(ID)
) ENGINE=InnoDB;


CREATE TABLE siteSSIExt (
	ID		SERIAL,
	SiteID		BIGINT UNSIGNED NOT NULL,
	Type		ENUM('full','xbithack'),

	PRIMARY KEY (ID),
	FOREIGN KEY (SiteID) REFERENCES sites(ID)
) ENGINE=InnoDB;



