#
# Table structure for table 'client access'
# - Done for all users
CREATE TABLE mail_access_client (
	ID SERIAL,

	Pattern VARCHAR(255) NOT NULL UNIQUE,
	Action VARCHAR(1024) NOT NULL,

	BlockID BIGINT UNSIGNED NOT NULL,


	PRIMARY KEY (ID),
	UNIQUE INDEX (Pattern)	
) ENGINE=InnoDB;


#
# Table structure for table 'premium client access'
# - Done for premium users
CREATE TABLE mail_access_client_p (
	ID SERIAL,

	Pattern VARCHAR(255) NOT NULL UNIQUE,
	Action VARCHAR(1024) NOT NULL,

	BlockID BIGINT UNSIGNED NOT NULL,


	PRIMARY KEY (ID),
	UNIQUE INDEX (Pattern)	
) ENGINE=InnoDB;



#
# Table structure for table 'sender access'
# - Done for all users
CREATE TABLE mail_access_sender (
	ID SERIAL,

	Pattern VARCHAR(255) NOT NULL UNIQUE,
	Action VARCHAR(1024) NOT NULL,

	BlockID BIGINT UNSIGNED NOT NULL,


	PRIMARY KEY (ID),
	UNIQUE INDEX (Pattern)	
) ENGINE=InnoDB;



#
# Table structure for table 'premium sender access'
# - Done for premium users
CREATE TABLE mail_access_sender_p (
	ID SERIAL,

	Pattern VARCHAR(255) NOT NULL UNIQUE,
	Action VARCHAR(1024) NOT NULL,

	BlockID BIGINT UNSIGNED NOT NULL,


	PRIMARY KEY (ID),
	UNIQUE INDEX (Pattern)	
) ENGINE=InnoDB;



#
# Table structure for table 'helo access'
# - Done for all users
CREATE TABLE mail_access_helo (
	ID SERIAL,

	Pattern VARCHAR(255) NOT NULL UNIQUE,
	Action VARCHAR(1024) NOT NULL,

	BlockID BIGINT UNSIGNED NOT NULL,


	PRIMARY KEY (ID),
	UNIQUE INDEX (Pattern)	
) ENGINE=InnoDB;



#
# Table structure for table 'premium helo access'
# - Done for premium
CREATE TABLE mail_access_helo_p (
	ID SERIAL,

	Pattern VARCHAR(255) NOT NULL UNIQUE,
	Action VARCHAR(1024) NOT NULL,

	BlockID BIGINT UNSIGNED NOT NULL,


	PRIMARY KEY (ID),
	UNIQUE INDEX (Pattern)	
) ENGINE=InnoDB;



#
# Table structure for table 'recipient access'
CREATE TABLE mail_access_recipient (
	ID SERIAL,

	Pattern VARCHAR(255) NOT NULL UNIQUE,
	Action VARCHAR(1024) NOT NULL,

	BlockID BIGINT UNSIGNED NOT NULL,


	PRIMARY KEY (ID),
	UNIQUE INDEX (Pattern)	
) ENGINE=InnoDB;




#
# Table structure for table 'etrn'
#
CREATE TABLE mail_access_etrn (
	ID SERIAL,

	Pattern VARCHAR(255) NOT NULL UNIQUE,
	Action VARCHAR(1024) NOT NULL,
	Note TEXT,

	UNIQUE INDEX (Pattern),	
	PRIMARY KEY (ID)
) ENGINE=InnoDB;


