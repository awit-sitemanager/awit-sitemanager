#
# Table structure for table 'mail mailbox delivery rules'
# - Done for all users
CREATE TABLE mail_MailboxDeliveryRules (
	ID SERIAL,

	MailboxID		BIGINT UNSIGNED NOT NULL,

	SenderSpec		VARCHAR(255) NOT NULL,

	RecipientSpec	VARCHAR(255),

	Action			VARCHAR(255) NOT NULL,

	Detail			VARCHAR(4096),

	ValidFrom		INT UNSIGNED,
	ValidTo			INT UNSIGNED,

	PRIMARY KEY (ID)
) ENGINE=InnoDB;

#
# Table structure for table 'mail transport delivery rules'
# - Done for all users
CREATE TABLE mail_TransportDeliveryRules (
	ID SERIAL,

	TransportID		BIGINT UNSIGNED NOT NULL,

	SenderSpec 		VARCHAR(255) NOT NULL,

	RecipientSpec	VARCHAR(255),

	Action			VARCHAR(255) NOT NULL,

	Detail			VARCHAR(4096),

	ValidFrom		INT UNSIGNED,
	ValidTo			INT UNSIGNED,

	PRIMARY KEY (ID)
) ENGINE=InnoDB;
