/*
 * U S E R   I N F O
 */


CREATE TABLE soap_api_groups (
	ID			SERIAL,

	GroupName		VARCHAR(255) NOT NULL,

	Description		TEXT,

	UNIQUE (GroupName)
) ENGINE=InnoDB;

INSERT INTO soap_api_groups (ID,GroupName) VALUES (1,'APITest');
INSERT INTO soap_api_groups (ID,GroupName) VALUES (2,'Administrator');
INSERT INTO soap_api_groups (ID,GroupName) VALUES (3,'Agent');
INSERT INTO soap_api_groups (ID,GroupName) VALUES (4,'Reseller');
/*
INSERT INTO soap_api_groups (ID,GroupName) VALUES (5,'Mailbox User');
INSERT INTO soap_api_groups (ID,GroupName) VALUES (6,'Radius User');
INSERT INTO soap_api_groups (ID,GroupName) VALUES (7,'DynamicDNS User');
INSERT INTO soap_api_groups (ID,GroupName) VALUES (8,'Website User');  
*/
/* If no website_user, use ftp username if 1 is found or fail */



/* API capability list for each api group */
CREATE TABLE soap_api_capabilities (
	ID			SERIAL,

	GroupID			BIGINT UNSIGNED NOT NULL,

	Capability		VARCHAR(255) NOT NULL,

/* compatability issues? */
	UNIQUE (GroupID,Capability),
	FOREIGN KEY (GroupID) REFERENCES soap_api_groups(ID)
) ENGINE=InnoDB;

/*
 * APITest
 */

/* SOAPTest */
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (1,'SOAP/Test');

/*
 * Administrator
 */

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroupCapabilities/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroupCapabilities/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroupCapabilities/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroupCapabilities/ListAll');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroupCapabilities/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroupCapabilities/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroups/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroups/Capabilities');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroups/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroups/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroups/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/APIGroups/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Authenticate');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/CacheEngine/Stats');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/GroupAttributes/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/GroupAttributes/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/GroupAttributes/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/GroupAttributes/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/GroupAttributes/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Groups/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Groups/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Groups/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Groups/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Groups/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/GroupsToAPIGroups/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/GroupsToAPIGroups/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/GroupsToAPIGroups/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerAttributes/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerAttributes/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerAttributes/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerAttributes/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerAttributes/Test');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerAttributes/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupAttributes/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupAttributes/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupAttributes/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupAttributes/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupAttributes/Test');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupAttributes/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupToAgents/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupToAgents/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupToAgents/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupToGroups/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupToGroups/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroupToGroups/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroups/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroups/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroups/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroups/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroups/Test');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerGroups/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerToServerGroups/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerToServerGroups/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/ServerToServerGroups/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Servers/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Servers/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Servers/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Servers/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Servers/Test');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Servers/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Test');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Users/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Users/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Users/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Users/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/Users/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/UsersToGroups/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/UsersToGroups/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/UsersToGroups/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/UsersToAPIGroups/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/UsersToAPIGroups/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (2,'Admin/UsersToAPIGroups/Remove');
/*
 * Agent
 */

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (3,'Agent/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (3,'Agent/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (3,'Agent/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (3,'Agent/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (3,'Agent/List');


/*
 * Reseller
 */

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Agent/Test');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Agent/Authenticate');

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/Zones/Test');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/Zones/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/Zones/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/Zones/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/Zones/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/Zones/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/ZoneEntries/Test');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/ZoneEntries/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/ZoneEntries/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/ZoneEntries/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/ZoneEntries/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/ZoneEntries/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/DDNS/Test');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/DDNS/Users/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/DDNS/Users/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/DDNS/Users/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/DDNS/Users/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'DNSHosting/DDNS/Users/List');

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'AuditTrails/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'AuditTrails/List');

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/List');
/*
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Classes/List');
*/
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/Topups/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/Topups/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/Topups/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/Topups/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/Topups/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/PortLocks/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/PortLocks/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/PortLocks/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/PortLocks/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/Users/PortLocks/List');

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'Radius/ServerGroups/List');



INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/Websites/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/Websites/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/Websites/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/Websites/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/Websites/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/FTPUsers/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/FTPUsers/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/FTPUsers/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/FTPUsers/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/FTPUsers/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/Databases/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/Databases/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/Databases/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/Databases/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'WebHosting/Databases/List');

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/Test');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/MailTransports/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/MailTransports/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/MailTransports/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/MailTransports/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/MailTransports/List');

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/Mailboxes/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/Mailboxes/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/Mailboxes/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/Mailboxes/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/Mailboxes/List');

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/MailboxAliases/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/MailboxAliases/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/MailboxAliases/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/MailboxAliases/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/MailboxAliases/List');

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryLogs/MailTransports/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryLogs/Mailboxes/List');

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/MailTransports/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/MailTransports/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/MailTransports/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/MailTransports/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/MailTransports/List');

INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/Mailboxes/Add');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/Mailboxes/Remove');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/Mailboxes/Update');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/Mailboxes/Get');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/Mailboxes/List');
INSERT INTO soap_api_capabilities (GroupID,Capability) VALUES (4,'MailHosting/DeliveryRules/Action/List');

/* SOAP users */
CREATE TABLE soap_users (
	ID			SERIAL,

	Username		VARCHAR(255) NOT NULL,
	Password		VARCHAR(255) NOT NULL,

	Disabled		SMALLINT NOT NULL DEFAULT '0',

	Name			VARCHAR(255),

	UNIQUE (Username)
) ENGINE=InnoDB;
/* user: admin / password */
INSERT INTO soap_users (ID,Username,Password) VALUES (1,'admin','{md5}X03MO1qnZdYdgyfeuILPmQ==');
INSERT INTO soap_users (ID,Username,Password) VALUES (2,'agent','{md5}X03MO1qnZdYdgyfeuILPmQ==');


/* SOAP user attributes */
CREATE TABLE soap_user_attributes (
	ID			SERIAL,

	UserID			BIGINT UNSIGNED NOT NULL,
	Attribute		VARCHAR(255),

	UNIQUE (UserID,Attribute),
	FOREIGN KEY (UserID) REFERENCES soap_users(ID)
) ENGINE=InnoDB;


/* SOAP groups */
CREATE TABLE soap_groups (
	ID			SERIAL,

	GroupName		VARCHAR(255) NOT NULL,

	Description		TEXT,

	UNIQUE (GroupName)
) ENGINE=InnoDB;

INSERT INTO soap_groups (ID,GroupName) VALUES (1,'Administrators');
INSERT INTO soap_groups (ID,GroupName) VALUES (2,'Client1');


/* Group attributes */
CREATE TABLE soap_group_attributes (
	ID			SERIAL,

	UserGroupID		BIGINT UNSIGNED NOT NULL,
	Attribute		VARCHAR(255),

	UNIQUE (UserGroupID,Attribute),
	FOREIGN KEY (UserGroupID) REFERENCES soap_group_attributes(ID)
) ENGINE=InnoDB;

INSERT INTO soap_group_attributes (UserGroupID,Attribute) VALUES (1,'Authority=Admin');
INSERT INTO soap_group_attributes (UserGroupID,Attribute) VALUES (2,'Authority=Agent');
INSERT INTO soap_group_attributes (UserGroupID,Attribute) VALUES (2,'AgentID=1');



/* SOAP user to group mappings */
CREATE TABLE soap_user_to_group (
	ID			SERIAL,

	UserID			BIGINT UNSIGNED NOT NULL,
	UserGroupID		BIGINT UNSIGNED NOT NULL,

	UNIQUE (UserID,UserGroupID),
	FOREIGN KEY (UserID) REFERENCES soap_users(ID),
	FOREIGN KEY (UserGroupID) REFERENCES soap_groups(ID)
) ENGINE=InnoDB;

INSERT INTO soap_user_to_group (UserID,UserGroupID) VALUES (1,1);
INSERT INTO soap_user_to_group (UserID,UserGroupID) VALUES (2,2);


/* SOAP user to api group */
CREATE TABLE soap_user_to_api_group (
	ID			SERIAL,

	UserID			BIGINT UNSIGNED NOT NULL,
	APIGroupID		BIGINT UNSIGNED NOT NULL,

	UNIQUE (UserID,APIGroupID),
	FOREIGN KEY (UserID) REFERENCES soap_users(ID),
	FOREIGN KEY (APIGroupID) REFERENCES soap_api_groups(ID)
) ENGINE=InnoDB;


/* SOAP group to api group */
CREATE TABLE soap_group_to_api_group (
	ID			SERIAL,

	UserGroupID		BIGINT UNSIGNED NOT NULL,
	APIGroupID		BIGINT UNSIGNED NOT NULL,

	UNIQUE (UserGroupID,APIUserGroupID),
	FOREIGN KEY (UserGroupID) REFERENCES soap_groups(ID),
	FOREIGN KEY (APIGroupID) REFERENCES soap_api_groups(ID)
) ENGINE=InnoDB;

/* Admin part of SOAPTest & Administrators */
INSERT INTO soap_group_to_api_group (UserGroupID,APIGroupID) VALUES (1,1);
INSERT INTO soap_group_to_api_group (UserGroupID,APIGroupID) VALUES (1,2);
INSERT INTO soap_group_to_api_group (UserGroupID,APIGroupID) VALUES (1,3);
INSERT INTO soap_group_to_api_group (UserGroupID,APIGroupID) VALUES (1,4);
/* Agent would be part of 3 and 4 */
INSERT INTO soap_group_to_api_group (UserGroupID,APIGroupID) VALUES (2,3);
INSERT INTO soap_group_to_api_group (UserGroupID,APIGroupID) VALUES (2,4);
/* Reseller would be only 4 */


/*
 * SOAP SERVERS
 */






/* SOAP server */
CREATE TABLE soap_servers (
	ID			SERIAL,

	Name			VARCHAR(255) NOT NULL)
) ENGINE=InnoDB;
INSERT INTO soap_servers (ID,Name) VALUES (1,'radius-0-1.virt: RPC');
INSERT INTO soap_servers (ID,Name) VALUES (2,'radius-1-0.virt: RPC');

/* Server server attributes */
CREATE TABLE soap_server_attributes (
	ID			SERIAL,

	ServerID		BIGINT UNSIGNED NOT NULL,
	Attribute		VARCHAR(2048) NOT NULL,

	UNIQUE (ServerID,Attribute),
	FOREIGN KEY (ServerID) REFERENCES soap_servers(ID)
) ENGINE=InnoDB;
/*
INSERT INTO soap_server_attributes (ServerID,Attribute) VALUES (1,'Priority=1');
*/
INSERT INTO soap_server_attributes (ServerID,Attribute) VALUES (1,'URI=SERVER_URI_HERE');
INSERT INTO soap_server_attributes (ServerID,Attribute) VALUES (2,'URI=SERVER_URI_HERE');

/* Server groups */
CREATE TABLE soap_server_groups (
	ID			SERIAL,

	Name			VARCHAR(255) NOT NULL,

	Description		TEXT,
	
	UNIQUE (Name)
) ENGINE=InnoDB;

INSERT INTO soap_server_groups (ID,Name,Description) VALUES (1,'Radius Cluster (Compat.)','Radius server cluster');

/* Server group attributes */
CREATE TABLE soap_server_group_attributes (
	ID			SERIAL,

	ServerGroupID		BIGINT UNSIGNED NOT NULL,
	Attribute		VARCHAR(2048) NOT NULL,

	UNIQUE (ServerGroupID,Attribute),
	FOREIGN KEY (ServerGroupID) REFERENCES soap_server_groups(ID)
) ENGINE=InnoDB;
/*
INSERT INTO soap_server_group_attributes (ServerGroupID,Attribute) VALUES (1,'StorageLocation=/var/vmail');
INSERT INTO soap_server_group_attributes (ServerGroupID,Attribute) VALUES (1,'LayoutStrategy=emailv1');
INSERT INTO soap_server_group_attributes (ServerGroupID,Attribute) VALUES (2,'StorageLocation=/var/www/sites');
INSERT INTO soap_server_group_attributes (ServerGroupID,Attribute) VALUES (2,'LayoutStrategy=webv1');
*/

/*ServerCapabilities=Radius,WebHosting,DNSHosting,MailHosting
RedirectFunction=getRadiusUsers:xyz  (server/my-server-name or serverGroup/ID)
*/

/* SOAP servers to SOAP server groups mappings */
CREATE TABLE soap_server_to_server_group (
	ID			SERIAL,

	ServerID		BIGINT UNSIGNED NOT NULL,
	ServerGroupID		BIGINT UNSIGNED NOT NULL,

	UNIQUE (ServerID,ServerGroupID),
	FOREIGN KEY (ServerID) REFERENCES soap_servers(ID),
	FOREIGN KEY (ServerGroupID) REFERENCES soap_server_groups(ID)
) ENGINE=InnoDB;

INSERT INTO soap_server_to_server_group (ServerID,ServerGroupID) VALUES (1,1);

/* SOAP servers groups to agent mappings */
CREATE TABLE soap_server_group_to_agent (
	ID			SERIAL,

	ServerGroupID		BIGINT UNSIGNED NOT NULL,
	AgentID			BIGINT UNSIGNED NOT NULL,

	UNIQUE (ServerGroupID,AgentID),
	FOREIGN KEY (ServerGroupID) REFERENCES soap_server_groups(ID),
	FOREIGN KEY (AgentID) REFERENCES agents(ID)
) ENGINE=InnoDB;

INSERT INTO soap_server_group_to_agent (ServerGroupID,AgentID) VALUES (1,1);

/* SOAP server group to group mappings */
CREATE TABLE soap_server_group_to_group (
	ID			SERIAL,

	ServerGroupID		BIGINT UNSIGNED NOT NULL,
	UserGroupID			BIGINT UNSIGNED NOT NULL,

	UNIQUE (ServerGroupID,UserGroupID),
	FOREIGN KEY (ServerGroupID) REFERENCES soap_server_groups(ID),
	FOREIGN KEY (UserGroupID) REFERENCES soap_groups(ID)
) ENGINE=InnoDB;
