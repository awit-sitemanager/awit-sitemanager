#
# Table structure for table 'transport'
#
CREATE TABLE mail_transport (
	ID SERIAL,

	AgentID BIGINT UNSIGNED NOT NULL,
	DomainName VARCHAR(255) NOT NULL UNIQUE,
	Transport VARCHAR(1024) NOT NULL,
	TransportDetail VARCHAR(8192),  # Used currently for the virtual: home directory

	PolicyID BIGINT UNSIGNED DEFAULT NULL,

	AgentDisabled TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	DisableDelivery TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',

	AgentRef VARCHAR(128),

	PRIMARY KEY (ID),
	UNIQUE INDEX (DomainName),
	FOREIGN KEY (AgentID) REFERENCES agents(ID),
	FOREIGN KEY (PolicyID) REFERENCES mail_policies(ID)
) ENGINE=InnoDB;

#INSERT INTO mail_transport (ID,DomainName,Transport) VALUES (1,'test.domain','virtual:');
#INSERT INTO mail_transport (ID,DomainName,Transport) VALUES (1,'admin.domain','virtual:');


#
# Table structure for table 'users'
#
CREATE TABLE mail_mailboxes (
	ID SERIAL,

	TransportID BIGINT UNSIGNED NOT NULL,

	Address VARCHAR(255) NOT NULL,

	Mailbox VARCHAR(255) NOT NULL,  # Indexed mailbox name full@domain 

	Username VARCHAR(255) NOT NULL,
	Password VARCHAR(128) NOT NULL DEFAULT '',
	ClearPassword VARCHAR(128), # Needed for extra auth mechanisms
	Name VARCHAR(128) NOT NULL DEFAULT '',
	UID SMALLINT UNSIGNED NOT NULL DEFAULT '10000',
	GID SMALLINT UNSIGNED NOT NULL DEFAULT '10001',
	HomeDir VARCHAR(8192) NOT NULL,
	Maildir VARCHAR(8192) NOT NULL,
	Quota INT UNSIGNED NOT NULL DEFAULT '5',  # In Mbyte

	# Policy stuff
	PremiumPolicy TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	PolicyID BIGINT UNSIGNED DEFAULT NULL,

	# Premium SMTP service - allows authenticated SMTP & log analysis
	PremiumSMTP TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',

	# Agents disable field
	AgentDisabled TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	# System disable fields
	DisableLogin TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	DisableDelivery TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	# Disable authenticated SMTP
	DisableSASL TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',

	AgentRef VARCHAR(128),

	PRIMARY KEY (ID),
	UNIQUE INDEX (Mailbox),
	UNIQUE INDEX (Username),
	UNIQUE INDEX (TransportID,Address),
	FOREIGN KEY (TransportID) REFERENCES mail_transport(ID),
	FOREIGN KEY (PolicyID) REFERENCES mail_policies(ID)
) ENGINE=InnoDB;

#INSERT INTO mail_mailboxes (TransportID,Address,Username,Password,Home,Maildir) VALUES (1,'user','user@test.domain',encrypt('test'),'/tmp/postfix','user@test.domain/');


#
# Table structure for table 'aliases'
#
CREATE TABLE mail_aliases (
	ID SERIAL,

	TransportID BIGINT UNSIGNED NOT NULL,

	Address VARCHAR(255) NOT NULL,

	Alias VARCHAR(255) NOT NULL,

	Goto VARCHAR(1024) NOT NULL,

	# Agents disable field
	AgentDisabled TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	# System disabled fields
	DisableDelivery TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',

	AgentRef VARCHAR(128),

	PRIMARY KEY (ID),
	UNIQUE INDEX (Alias),
	UNIQUE INDEX (TransportID,Address),
	FOREIGN KEY (TransportID) REFERENCES mail_transport(ID)
) ENGINE=InnoDB;

#INSERT INTO mail_aliases (TransportID,Address,Goto) VALUES (2,'','dropbox@test.domain');


