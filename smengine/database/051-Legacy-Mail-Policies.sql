#
# Table structure for table mail policies, used also by amavis
#
CREATE TABLE mail_policies (
	ID			SERIAL, 

	PolicyName		VARCHAR(128) NOT NULL,
	PolicyType		TINYINT NOT NULL,  # 1 = system, 2 = agent, 3 = client

	PolicyRefID		BIGINT UNSIGNED DEFAULT NULL,  # NULL if system, agentID if agent, mailboxID if client

	SMTPClass		VARCHAR(1024) DEFAULT NULL,

	greylisting		CHAR(1) DEFAULT NULL,     -- Y/N
	
	virus_lover		CHAR(1) DEFAULT NULL,     -- Y/N
	spam_lover		CHAR(1) DEFAULT NULL,     -- Y/N
	banned_files_lover	CHAR(1) DEFAULT NULL,     -- Y/N
	bad_header_lover	CHAR(1) DEFAULT NULL,     -- Y/N

	bypass_virus_checks	CHAR(1) DEFAULT NULL,     -- Y/N
	bypass_spam_checks	CHAR(1) DEFAULT NULL,     -- Y/N
	bypass_banned_checks	CHAR(1) DEFAULT NULL,     -- Y/N
	bypass_header_checks	CHAR(1) DEFAULT NULL,     -- Y/N

	spam_modifies_subj	CHAR(1) DEFAULT NULL,     -- Y/N

	virus_quarantine_to	VARCHAR(1024) DEFAULT NULL,
	spam_quarantine_to	VARCHAR(1024) DEFAULT NULL,
	banned_quarantine_to	VARCHAR(1024) DEFAULT NULL,
	bad_header_quarantine_to VARCHAR(1024) DEFAULT NULL,

	spam_tag_level		FLOAT DEFAULT NULL,  -- higher score inserts spam info headers
	spam_tag2_level		FLOAT DEFAULT NULL,  -- inserts 'declared spam' header fields
	spam_kill_level		FLOAT DEFAULT NULL,  -- higher score activates evasive actions
	spam_dsn_cutoff_level	FLOAT DEFAULT NULL,

	addr_extension_virus	VARCHAR(64) DEFAULT NULL,
	addr_extension_spam	VARCHAR(64) DEFAULT NULL,
	addr_extension_banned	VARCHAR(64) DEFAULT NULL,
	addr_extension_bad_header VARCHAR(64) DEFAULT NULL,

	warnvirusrecip		CHAR(1) DEFAULT NULL, -- Y/N
	warnbannedrecip		CHAR(1) DEFAULT NULL, -- Y/N
	warnbadhrecip		CHAR(1) DEFAULT NULL, -- Y/N

	newvirus_admin		VARCHAR(256) DEFAULT NULL,
	virus_admin		VARCHAR(256) DEFAULT NULL,
	banned_admin		VARCHAR(256) DEFAULT NULL,
	bad_header_admin	VARCHAR(256) DEFAULT NULL,
	spam_admin		VARCHAR(256) DEFAULT NULL,

	spam_subject_tag	VARCHAR(64)	DEFAULT NULL,
	spam_subject_tag2	VARCHAR(64)	DEFAULT NULL,

	message_size_limit	BIGINT UNSIGNED	DEFAULT NULL, -- max size in bytes, 0 disable

	banned_rulenames	VARCHAR(1024)	DEFAULT NULL, -- comma-separated list of ...
	-- names mapped through %banned_rules to actual banned_filename tables

	PRIMARY KEY (ID)
) ENGINE=InnoDB;

INSERT INTO mail_policies (
		ID, PolicyType, PolicyName,
		SMTPClass,
		greylisting,
		virus_lover, spam_lover, banned_files_lover, bad_header_lover,
		bypass_virus_checks, bypass_spam_checks, bypass_banned_checks, bypass_header_checks,
		spam_modifies_subj, spam_tag_level, spam_tag2_level, spam_kill_level, spam_dsn_cutoff_level
	) 
	VALUES
  	(
		1, 1, 'System - None',
		NULL,
		'N',
		'N',  'N',  'N',  'N',
		'Y',  'Y',  'Y',  'Y',
		'N', NULL, NULL, NULL, NULL 
	),
  	(
		2, 1, 'System - Admin',
		'OK',
		'N',
		'Y',  'Y',  'Y',  'Y',
		'N',  'N',  'N',  'N',
		'N', NULL, NULL, NULL, NULL 
	),
  	(
		3, 2, 'Client - Free Service',
		NULL,
		'N',
		'N',  'N',  'N',  'N',
		'N',  'Y',  'Y',  'Y',
		'N', NULL, NULL, NULL, NULL 
	),
  	(
		4, 2, 'Client - Premium Service',
		'premium',
		'Y',
		'N',  'N',  'N',  'N',
		'N',  'N',  'N',  'N',
		'N', NULL,  5.0,  7.5, 9.0
	),
  	(
		5, 2, 'Client - Premium Service (SAFE)',
		'premium_safe',
		'Y',
		'N',  'N',  'N',  'N',
		'N',  'N',  'N',  'N',
		'N', NULL,  5.0,  7.5, 9.0
	);




# PRIORITIES USED BELOW

#1. System Policy
#2.
#3. Domain Policy - recreated
#4.
#5. Mailbox Policy - recreated
#6.
#7. Client Custom Policy - recreated
#8.
#9. Special Policy
#10.

#
# Table structure for table mail policy users, used mostly by amavis
#

CREATE TABLE mail_policy_users (
	id SERIAL, -- Needed by amavis

	Priority TINYINT NOT NULL,
	PolicyID BIGINT UNSIGNED NOT NULL,

	AddrSpec VARCHAR(255) NOT NULL,

	PRIMARY KEY (ID),
	KEY AddrSpec (AddrSpec),
	UNIQUE INDEX (AddrSpec),
	FOREIGN KEY (PolicyID) REFERENCES mail_policies(ID)
) ENGINE=InnoDB;

INSERT INTO mail_policy_users (
		Priority,PolicyID,AddrSpec
	)
	VALUES
	(
		# Default system priority
		1,1,"@."
	),
	# RFC requires us not to reject mail for these
	(
		9,2,"abuse"
	),
	(
		9,2,"postmaster"
	);


#
# Table structure for table mail policy maps, used by postfix
#
CREATE TABLE mail_policy_maps (
	ID SERIAL,

	Priority TINYINT NOT NULL,

	AddrSpec VARCHAR(255) NOT NULL,
	ClassName VARCHAR(1024) NOT NULL,

	PRIMARY KEY (ID),
	KEY AddrSpec (AddrSpec),
	UNIQUE INDEX (AddrSpec)
) ENGINE=InnoDB;

INSERT INTO mail_policy_maps
		(Priority,AddrSpec,ClassName) 
	VALUES 
		(9,'postmaster@','OK'),
		(9,'abuse@','OK');

