
/* Table structure for table 'logs' */

CREATE TABLE mail_logs (
	ID SERIAL,
	Timestamp DATETIME,

	Cluster VARCHAR(255),
	Server VARCHAR(255),

	SendingServerName VARCHAR(255),
	SendingServerIP VARCHAR(255),

	QueueID VARCHAR(255),
	MessageID VARCHAR(255),

	EnvelopeFrom VARCHAR(255),
	EnvelopeTo VARCHAR(255),

	Size INT UNSIGNED,

	Destination VARCHAR(255),

	Status VARCHAR(255),

	PRIMARY KEY (ID)
) ENGINE=InnoDB;
