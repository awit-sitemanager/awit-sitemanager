DROP TABLE IF EXISTS systemNameservers;
CREATE TABLE systemNameservers (
	ID		SERIAL,
	# 1 = Primary, 2 = Secondary, 3 = both
	Type		TINYINT UNSIGNED NOT NULL, 		
	HostName	VARCHAR(255) NOT NULL,

	PRIMARY KEY (ID),
	INDEX (HostName)
) ENGINE=InnoDB;


INSERT INTO systemNameservers (Type,HostName) 
	VALUES (
		1,
		'ns1.domain.com'
	);

INSERT INTO systemNameservers (Type,HostName) 
	VALUES (
		2,
		'ns2.domain.com'
	);

INSERT INTO systemNameservers (Type,HostName) 
	VALUES (
		2,
		'ns3.domain.com'
	);


# Case differences here for mydns (soa table)
DROP TABLE IF EXISTS zones;
CREATE TABLE zones (
	id		SERIAL,
	AgentID		BIGINT UNSIGNED NOT NULL,
	origin		VARCHAR(255) NOT NULL,
	ns		VARCHAR(255) NOT NULL,
	mbox		VARCHAR(255) NOT NULL,
	serial		INT UNSIGNED NOT NULL DEFAULT '1',
	refresh		INT UNSIGNED NOT NULL DEFAULT '7200',
	retry		INT UNSIGNED NOT NULL DEFAULT '600',
	expire		INT UNSIGNED NOT NULL DEFAULT '2419200',
	minimum		INT UNSIGNED NOT NULL DEFAULT '3600',
	ttl		INT UNSIGNED NOT NULL DEFAULT '3600',
	active		ENUM('Y','N') NOT NULL DEFAULT 'Y',
	xfer		VARCHAR(255) NOT NULL DEFAULT '',

	PRIMARY KEY (ID),
	UNIQUE KEY (origin),
	FOREIGN KEY (AgentID) REFERENCES agents(ID)
) ENGINE=InnoDB;


# Dynamic DNS support
DROP TABLE IF EXISTS dynaDNSUsers;
CREATE TABLE dynaDNSUsers (
	ID		SERIAL,
	AgentID		BIGINT UNSIGNED NOT NULL,
	Username	VARCHAR(64) NOT NULL,
	Password	VARCHAR(64) NOT NULL,
	LastUpdate	INT UNSIGNED NOT NULL,
	UpdateInterval	INT UNSIGNED NOT NULL DEFAULT 86400,  # Set initial interval to 1 day
	Status		INT UNSIGNED NOT NULL DEFAULT 0,  # 0 - Active, 1 - Disabled

	PRIMARY KEY (ID),
	UNIQUE KEY (Username),
	FOREIGN KEY (AgentID) REFERENCES agents(ID)
) ENGINE=InnoDB;


# mydns rr table
DROP TABLE IF EXISTS zoneEntries;
CREATE TABLE zoneEntries (
	id		SERIAL,
	zone		BIGINT UNSIGNED NOT NULL,
	name		VARCHAR(64) NOT NULL,
	type		ENUM('A','AAAA','CNAME','HINFO','MX','NS','PTR','RP','SRV','TXT') NOT NULL,
	data		VARCHAR(255) NOT NULL,
	aux		INT UNSIGNED NOT NULL DEFAULT 0,
	ttl		INT UNSIGNED NOT NULL DEFAULT '3600',
	active		ENUM('Y','N') NOT NULL DEFAULT 'Y',
	DynaDNSUser	BIGINT UNSIGNED DEFAULT NULL, 
	
	PRIMARY KEY (id),
	UNIQUE KEY (zone,name,type,data),
	FOREIGN KEY (zone) REFERENCES zones(id),
	FOREIGN KEY (DynaDNSUser) REFERENCES dynaDNSUsers(ID)
) ENGINE=InnoDB;




