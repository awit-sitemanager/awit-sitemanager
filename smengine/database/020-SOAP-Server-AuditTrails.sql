/*
 * Audit Trails
 */

CREATE TABLE auditTrails (
	ID SERIAL,
	Timestamp BIGINT UNSIGNED NOT NULL,
	ClientIP VARCHAR(255),
	Protocol VARCHAR(255),
	AgentID BIGINT UNSIGNED,
	Username VARCHAR(255),
	ServerCluster VARCHAR(255),
	ServerName VARCHAR(255),
	Module VARCHAR(255),
	Function VARCHAR(255),
	Parameters VARCHAR(255),
	Result VARCHAR(255)
) ENGINE=InnoDB;

