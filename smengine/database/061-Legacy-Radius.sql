# Radius realms
CREATE TABLE radiusRealms (
	ID 		SERIAL,

	Domain		VARCHAR(255),
	Description	TINYTEXT NOT NULL,
	
	PRIMARY KEY (ID)
) ENGINE=InnoDB;


# Radius Classes
CREATE TABLE radiusClasses (
	ID 		SERIAL,

	RadiusRealmID		BIGINT UNSIGNED NOT NULL,
	Description		TINYTEXT NOT NULL,
	
	PODServers		TINYTEXT,  # Packet of disconnect servers,  ip:port,ip:port

	PRIMARY KEY (ID),
	FOREIGN KEY (radiusRealmID) REFERENCES radiusRealms(ID)
) ENGINE=InnoDB;



# Mappings of classes to agents, with agent specific info
CREATE TABLE radiusClassAgentMap (
	ID		SERIAL,

	AgentID		BIGINT UNSIGNED NOT NULL,
	RadiusClassID	BIGINT UNSIGNED NOT NULL,

	PRIMARY KEY (ID),
	FOREIGN KEY (AgentID) REFERENCES agents(ID),
	FOREIGN KEY (RadiusClassID) REFERENCES radiusClasses(ID)
) ENGINE=InnoDB;



# Radius users
CREATE TABLE radiusUsers (
	ID 		SERIAL,

	AgentID		BIGINT UNSIGNED NOT NULL,

	Username	VARCHAR(32) NOT NULL,
	Password	VARCHAR(64) NOT NULL,

	# ADSL specific
	UsageCap	INTEGER UNSIGNED, /* In Mbytes, NULL means uncapped, 0 means prepaid */

	RadiusClassID	BIGINT UNSIGNED NOT NULL,


	# ADSL Specific options block
	CappingType	TINYINT UNSIGNED NOT NULL DEFAULT 1,  # 1 = hard capping, 2 = auto-topup

	NotifyMethod	TINYTEXT,


	AgentDisabled	TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',

	AgentRef VARCHAR(128),


	PRIMARY KEY (ID),
	UNIQUE INDEX (Username),
	FOREIGN KEY (AgentID) REFERENCES agents(ID),
	FOREIGN KEY (RadiusClassID) REFERENCES radiusClasses(ID)
) ENGINE=InnoDB;


# Radius attribs, per user
CREATE TABLE radiusAttribs (
	ID 		SERIAL,

	RadiusUserID	BIGINT UNSIGNED NOT NULL,
	Attr		VARCHAR(32) NOT NULL,
	Value		VARCHAR(128),
	OP 		enum("=", "!=", "<", ">", "<=", ">="),


	PRIMARY KEY (ID),
	INDEX (RadiusUserID),
	FOREIGN KEY (RadiusUserID) REFERENCES radiusUsers(ID)
) ENGINE=InnoDB;



# Radius attribs
CREATE TABLE radiusClassAttribs (
	ID 		SERIAL,

	RadiusClassID	BIGINT UNSIGNED NOT NULL,
	Attr		VARCHAR(32) NOT NULL,
	Value		VARCHAR(128),
	OP 		enum("=", "!=", "<", ">", "<=", ">="),


	PRIMARY KEY (ID),
	FOREIGN KEY (RadiusClassID) REFERENCES radiusClasses(ID)
) ENGINE=InnoDB;
/* 
INSERT INTO radiusClassAttribs (RadiusClassID,Attr,Value,OP) VALUES (1,'Acct-Interim-Interval',3600,NULL);
INSERT INTO radiusClassAttribs (RadiusClassID,Attr,Value,OP) VALUES (1,'Session-Timeout',86400,NULL); 
*/






# Bandwidth topups
CREATE TABLE radiusTopups (
	ID		SERIAL,

	RadiusUserID	BIGINT UNSIGNED NOT NULL,

	Bandwidth	INTEGER UNSIGNED, /* In Mbytes */

	Timestamp	DATETIME NOT NULL,

	ValidFrom	DATETIME NOT NULL,
	ValidTo		DATETIME NOT NULL,

	AgentRef VARCHAR(128),

	PRIMARY KEY (ID),
	INDEX (RadiusUserID,ValidFrom,ValidTo),
	FOREIGN KEY (RadiusUserID) REFERENCES radiusUsers(ID)
) ENGINE=InnoDB;


# Radius port locking
CREATE TABLE radiusPortLocks (
	ID 		SERIAL,

	RadiusUserID	BIGINT UNSIGNED NOT NULL,
	NASPort		INTEGER UNSIGNED,

	AgentDisabled	TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',

	AgentRef VARCHAR(128),

	PRIMARY KEY (ID),
	INDEX (RadiusUserID),
	UNIQUE INDEX (RadiusUserID,NASPort),
	FOREIGN KEY (RadiusUserID) REFERENCES radiusUsers(ID)
) ENGINE=InnoDB;








