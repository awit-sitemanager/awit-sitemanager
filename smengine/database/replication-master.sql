# Master
#
# 1. In term 1 run    FLUSH TABLES WITH READ LOCK;
# 2. In term 2 run    cd /var/lib/mysql; tar -cvf /root/snapshot.tar ib* sitemanager
# 3. In term 1 run    SHOW MASTER STATUS;
#	- note the File and Position
# 4. Close connection to DB in term 1
# 5. Copy snapshot.tar to slave
#
#
# - Set the following in /etc/my.cnf
#
# report-host=mymaster-server
# log-bin=mysql-bin
# server-id=1


GRANT REPLICATION SLAVE ON *.*
		TO 'sitemanager-repl-1'@'' IDENTIFIED BY 'slavepass1';

#GRANT REPLICATION SLAVE ON *.*
#		TO 'sitemanager-repl-2'@'' IDENTIFIED BY 'slavepass2';





# Slave
#
# 1. Set the following in /etc/my.cnf
#
# report-host=myslave-server
# server-id=2
#
# 2. Untar above backup into /var/lib/mysql
#
# 3. Fire up mysql

CHANGE MASTER TO
	MASTER_HOST='master_host_name',
	MASTER_USER='replication_user_name',
	MASTER_PASSWORD='replication_password',
	MASTER_LOG_FILE='recorded_log_file_name',
	MASTER_LOG_POS=recorded_log_position;

START SLAVE;

# 4. Add   replicate-do-db=sitemanager  to my.cnf
