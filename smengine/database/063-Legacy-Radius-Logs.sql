# Radius failures
CREATE TABLE radiusAuthFail (
	ID 		SERIAL,

	Username	VARCHAR(32) NOT NULL,
	Timestamp	DATETIME,

	NASIPAddress	VARCHAR(17),
	NASPortType	INTEGER UNSIGNED,
	NASPort		INTEGER UNSIGNED,

	ConnectInfo		VARCHAR(32),
	ServiceType		INTEGER UNSIGNED,

	UsageCap	INTEGER UNSIGNED,
	CurUsage	INTEGER UNSIGNED,

	Reason		INT UNSIGNED,  /* 1 = deactivated, 2 = capped, 3 = password incorrect, 4 = port locked, 5 = class violation */

	PRIMARY KEY (ID),
	INDEX (Username)
) ENGINE=InnoDB;




# Radius logs
CREATE TABLE radiusLogs (
	ID 		SERIAL,

	AgentID		BIGINT UNSIGNED,  # Agent ID this user is assigned to

	Username	VARCHAR(32) NOT NULL,

	RadiusClassID	BIGINT UNSIGNED,  # User class at time of logging
	UsageCap	INTEGER UNSIGNED,  # Cap at time of logging
	Topups		INTEGER UNSIGNED,  # Topup amount at time of logging

	Status          INTEGER,  # -1 - history data, 1 - start accounting, 2 - update, 3 - stop accounting
  
	Timestamp	DATETIME NOT NULL,

	AcctSessionID	VARCHAR(32),
	AcctSessionTime INTEGER UNSIGNED,
	AcctDelayTime 	INTEGER UNSIGNED,
	
	NASIPAddress	VARCHAR(17),
	NASPortType	INTEGER UNSIGNED,
	NASPort		INTEGER UNSIGNED,
	CalledStationID		VARCHAR(32),
	CallingStationID 	VARCHAR(32),

	NASTransmitRate		INTEGER UNSIGNED,
	NASReceiveRate		INTEGER UNSIGNED,

	ConnectInfo		VARCHAR(32),
	ServiceType		INTEGER UNSIGNED,
	Class			VARCHAR(16),

        FramedIPAddress         VARCHAR(17),

	AcctInputOctets		INTEGER UNSIGNED,
	AcctOutputOctets	INTEGER UNSIGNED,
	AcctInputGigawords	INTEGER UNSIGNED,
	AcctOutputGigawords	INTEGER UNSIGNED,
	LastAccUpdate		DATETIME,

	ConnectTermReason	INTEGER,
  
	PRIMARY KEY (ID),
	INDEX (Username,Timestamp),
	INDEX (Username,AcctSessionID)
);
        

# Radius notification tracking
CREATE TABLE radiusNotifyTrack (
	ID 		SERIAL,

	Username	VARCHAR(32) NOT NULL,

	LastUpdate	INTEGER UNSIGNED NOT NULL,
	UpdateInterval	INTEGER UNSIGNED NOT NULL DEFAULT 86400,  # Set initial interval to 1 day

	LastValue	INTEGER UNSIGNED,


	PRIMARY KEY (ID),
	INDEX (Username)
) ENGINE=InnoDB;


