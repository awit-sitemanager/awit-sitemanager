# Agent users
CREATE TABLE agentUsers (
	ID		SERIAL,

	AgentID		BIGINT UNSIGNED NOT NULL,

	Username	TINYTEXT NOT NULL,
	Password	TINYTEXT NOT NULL,
	Name		TINYTEXT NOT NULL,
	ContactTel1	TINYTEXT NOT NULL,
	ContactEmail	TINYTEXT NOT NULL,

	Disabled TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',

	PRIMARY KEY (ID),
	FOREIGN KEY (AgentID) REFERENCES agents(ID)
) ENGINE=InnoDB;
