# Agents
DROP TABLE IF EXISTS agents;
CREATE TABLE agents (
	ID		SERIAL,
	Name		TINYTEXT NOT NULL,
	ContactPerson	TINYTEXT,
	ContactTel1	TINYTEXT,
	ContactTel2	TINYTEXT,
	ContactEmail	TINYTEXT,
	SiteQuota	INT UNSIGNED,
	MailboxQuota	INT UNSIGNED,
	MinSiteSize	INT UNSIGNED,
	MinMailboxSize	INT UNSIGNED,

	ConfDirectory	TEXT NOT NULL,
	WebDirectory	TEXT NOT NULL,

	PRIMARY KEY (ID)
) ENGINE=InnoDB;

