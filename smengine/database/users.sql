# Backend management functions username
GRANT USAGE 
	ON sitemanager.* 
	TO "sitemanager"@'localhost' 
		IDENTIFIED BY 'test';
GRANT ALL 
	ON sitemanager.* 
	TO "sitemanager"@'localhost';

# For mysql create database, drop database, grant ... etc
GRANT ALL 
	ON *.* 
	TO "dbadmin"@'localhost' 
		IDENTIFIED BY 'test';

# Proftpd server 
GRANT USAGE 
	ON sitemanager.* 
	TO "proftpd"@'localhost' 
		IDENTIFIED BY 'test';
GRANT SELECT 
	ON sitemanager.ftpUsers 
	TO "proftpd"@'localhost';
GRANT SELECT 
	ON sitemanager.ftpGroups 
	TO "proftpd"@'localhost';


# FormMailer CGI script
GRANT USAGE 
	ON sitemanager.* 
	TO 'FormMailer'@'localhost' 
		IDENTIFIED BY 'test';
GRANT SELECT 
	ON sitemanager.sites 
	TO "FormMailer"@'localhost';


# phpMyAdmin
GRANT USAGE 
	ON mysql.* 
	TO 'pma'@'localhost' 
		IDENTIFIED BY 'pmapass';
GRANT SELECT (Host, User, Select_priv, Insert_priv, Update_priv, Delete_priv, 
		Create_priv, Drop_priv, Reload_priv, Shutdown_priv, Process_priv, 
		File_priv, Grant_priv, References_priv, Index_priv, Alter_priv, 
		Show_db_priv, Super_priv, Create_tmp_table_priv, Lock_tables_priv,
		Execute_priv, Repl_slave_priv, Repl_client_priv) 
	ON mysql.user 
	TO 'pma'@'localhost';
GRANT SELECT 
	ON mysql.db 
	TO 'pma'@'localhost';
GRANT SELECT 
	ON mysql.host 
	TO 'pma'@'localhost';
GRANT SELECT (Host, Db, User, Table_name, Table_priv, Column_priv) 
	ON mysql.tables_priv 
	TO 'pma'@'localhost';


# MyDNS
GRANT USAGE 
	ON sitemanager.*
	TO "mydns"@'localhost' 
		IDENTIFIED BY 'test';
GRANT SELECT 
	ON sitemanager.zones 
	TO "mydns"@'localhost' ;
GRANT SELECT 
	ON sitemanager.zoneEntries 
	TO "mydns"@'localhost';


# MySite
GRANT USAGE 
	ON sitemanager.*
	TO "mysite"@'localhost' 
		IDENTIFIED BY 'test';
GRANT SELECT 
	ON sitemanager.ftpUsers 
	TO "mysite"@'localhost' ;
GRANT SELECT 
	ON sitemanager.siteStats 
	TO "mysite"@'localhost';



# # #  M A I L  # # #

# Backend management for policyd
GRANT USAGE 
	ON sitemanager_policyd.* 
	TO "sitemanager"@'localhost' 
		IDENTIFIED BY 'test';
GRANT ALL 
	ON sitemanager_policyd.* 
	TO "sitemanager"@'localhost';


# Postfix
GRANT USAGE 
	ON sitemanager.*
	TO "postfix"@'localhost' 
		IDENTIFIED BY 'test';
GRANT SELECT 
	ON sitemanager.mail_access_client 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_access_client_p 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_access_sender 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_access_sender_p 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_access_helo 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_access_helo_p 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_access_recipient 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_transport 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_mailboxes 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_aliases 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_access_etrn 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_policy_maps 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.incidents 
	TO "postfix"@'localhost' ;

GRANT SELECT 
	ON sitemanager.incident_updates
	TO "postfix"@'localhost' ;


# Amavis
GRANT USAGE 
	ON sitemanager.*
	TO "amavis"@'localhost' 
		IDENTIFIED BY 'test';
GRANT SELECT 
	ON sitemanager.mail_policy_users 
	TO "amavis"@'localhost' ;

GRANT SELECT 
	ON sitemanager.mail_policies 
	TO "amavis"@'localhost' ;


# Policyd
GRANT USAGE 
	ON sitemanager_policyd.*
	TO "policyd"@'localhost' 
		IDENTIFIED BY 'test';
GRANT ALL 
	ON sitemanager_policyd.* 
	TO "policyd"@'localhost';


# Authlib
GRANT USAGE 
	ON sitemanager.*
	TO "authlib"@'localhost' 
		IDENTIFIED BY 'test';
GRANT SELECT
	ON sitemanager.mail_transport 
	TO "authlib"@'localhost';

GRANT SELECT, UPDATE 
	ON sitemanager.mail_mailboxes 
	TO "authlib"@'localhost';


# Spamassassin
GRANT USAGE
	ON sitemanager_spamassassin.*
	TO "spamassassin"@'localhost'
		IDENTIFIED BY 'test';
GRANT ALL 
	ON sitemanager_spamassassin.* 
	TO "spamassassin"@'localhost';


# # #  R A D I U S  # # #

# Radius daemon
GRANT USAGE 
	ON sitemanager.* 
	TO "radius"@'localhost' 
		IDENTIFIED BY 'test';
GRANT SELECT 
	ON sitemanager.agents 
	TO "radius"@'localhost';
GRANT SELECT 
	ON sitemanager.radiusRealms 
	TO "radius"@'localhost';
GRANT SELECT 
	ON sitemanager.radiusClassAgentMap 
	TO "radius"@'localhost';
GRANT SELECT 
	ON sitemanager.radiusClasses 
	TO "radius"@'localhost';
GRANT SELECT 
	ON sitemanager.radiusUsers 
	TO "radius"@'localhost';
GRANT SELECT 
	ON sitemanager.radiusAttribs 
	TO "radius"@'localhost';
GRANT SELECT 
	ON sitemanager.radiusClassAttribs 
	TO "radius"@'localhost';
GRANT SELECT 
	ON sitemanager.radiusTopups 
	TO "radius"@'localhost';
GRANT SELECT 
	ON sitemanager.radiusPortLocks 
	TO "radius"@'localhost';

# Radius filters
GRANT USAGE,SELECT,INSERT,UPDATE,DELETE
        ON sitemanager_radius.*
	TO "radiuslogs"@'localhost'
		IDENTIFIED BY 'test';
GRANT INSERT
	ON sitemanager_radius.radiusAuthFail
	TO "radius"@'localhost';


