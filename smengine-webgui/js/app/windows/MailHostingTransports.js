/*
MailHosting Transport
Copyright (C) 2007-2010, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/



function showMailTransportsWindow(serverGroupID,serverGroupName) {

	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("MailHosting/MailTransports/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add mail transport',
			iconCls:'silk-book_add',
			handler: function() {
				showMailTransportEditWindow(mailTransportWindow,serverGroupID,serverGroupName);
			}
		});
		buttons.push('-'); 
	}
	if (userHasCapability("MailHosting/MailTransports/Update")) {
		buttons.push({
			text:'Edit',
			tooltip:'Edit mail transport',
			iconCls:'silk-book_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(mailTransportWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailTransportEditWindow(mailTransportWindow,serverGroupID,serverGroupName,selectedItem.data.ID,selectedItem.data.DomainName);
				} else {
					mailTransportWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mail transport selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailTransportWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/MailTransports/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove mail transport',
			iconCls:'silk-book_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(mailTransportWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailTransportRemoveWindow(mailTransportWindow,serverGroupID,selectedItem.data.ID);
				} else {
					mailTransportWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mail transport selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailTransportWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/Mailboxes/List")) {
		buttons.push({
			text:'Mailboxes',
			tooltip:'Mailboxes',
			iconCls:'silk-email',
			handler: function() {
				var selectedItem = Ext.getCmp(mailTransportWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailboxesWindow(serverGroupID,serverGroupName,selectedItem.data.ID,selectedItem.data.DomainName);
				} else {
					mailTransportWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mail transport selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailTransportWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/MailboxAliases/List")) {
		buttons.push({
			text:'Aliases',
			tooltip:'Aliases',
			iconCls:'silk-link',
			handler: function() {
				var selectedItem = Ext.getCmp(mailTransportWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailboxAliasesWindow(serverGroupID,serverGroupName,selectedItem.data.ID,selectedItem.data.DomainName);
				} else {
					mailTransportWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mail transport selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailTransportWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/DeliveryLogs/MailTransports/List")) {
		buttons.push({
			text:'Logs',
			tooltip:'Mail transport delivery logs',
			iconCls: 'silk-page_white_text',
			handler: function() {
				var selectedItem = Ext.getCmp(mailTransportWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showTransportDeliveryLogsWindow(serverGroupID,selectedItem.data.ID);
				} else {
					mailTransportWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mail transport selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailTransportWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/DeliveryRules/MailTransports/List")) {
		buttons.push({
			text:'Rules',
			tooltip:'Transport delivery rules',
			iconCls:'silk-lock',
			handler: function() {
				var selectedItem = Ext.getCmp(mailTransportWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showTransportDeliveryRulesWindow(serverGroupID,selectedItem.data.ID);
				} else {
					mailTransportWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mail transport selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailTransportWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}

	var mailTransportWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Mail Transports - "+serverGroupName,
			iconCls: 'silk-book',

			width: 600,
			height: 335,

			minWidth: 600,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "AgentID",
					sortable: true,
					hidden: true,
					dataIndex: 'AgentID'
				},
				{
					header: "Agent Name",
					sortable: false,
					dataIndex: 'AgentName'
				},
				{
					header: "Domain Name",
					sortable: true,
					dataIndex: 'DomainName'
				},
				{
					header: "Transport",
					sortable: true,
					dataIndex: 'Transport'
				},
				{
					header: "Agent Ref",
					sortable: true,
					dataIndex: 'AgentRef'
				},
				{
					header: "Status",
					renderer: renderNiceBooleanColumn,
					align: 'center',
					sortable: false,
					dataIndex: 'Disabled'
				}
			]),
			autoExpandColumn: 'DomainName'
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				SOAPModule: 'MailHosting',
				SOAPFunction: 'getMailTransports',
				SOAPParams: 'ServerGroupID,__null,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'AgentID'},
				{type: 'string',  dataIndex: 'DomainName'},
				{type: 'string',  dataIndex: 'AgentRef'}
			]
		}
	);

	mailTransportWindow.show();
}


// Display edit/add form
function showMailTransportEditWindow(mailTransportWindow,serverGroupID,serverGroupName,transportID,domainName) {

	var submitAjaxConfig;
	var editMode;
	var icon;
	var title;


	// We doing an update
	if (transportID) {
		icon = 'silk-book_edit';
		title = 'Mail Transport - '+serverGroupName+' - '+domainName;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				ID: transportID,
				SOAPFunction: 'updateMailTransport',
				SOAPParams: 
					'ServerGroupID,'+
					'1:ID,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(mailTransportWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
		editMode = true;

	// We doing an Add
	} else {
		icon = 'silk-book_add';
		title = 'Mail Transport - '+serverGroupName;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				SOAPFunction: 'createMailTransport',
				SOAPParams: 
					'ServerGroupID,'+
					'1:AgentID,'+
					'1:DomainName,'+
					'1:Transport,1:Detail,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(mailTransportWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
		editMode = false;
	}

	// Agent store
	var agentStore = new Ext.ux.JsonStore({
			sortInfo: { field: "Name", direction: "ASC" },
			baseParams: {
				SOAPModule: 'Agent',
				SOAPFunction: 'getAgents',
				SOAPParams: '__search'
			}
	});

	// Form panel ID
	var formPanelID = Ext.id();

	// Combo box ID
	var agentComboBoxID = Ext.id();
	var transportComboboxID = Ext.id();
	var transportSMTPServerID = Ext.id();

	// Create window
	var mailTransportFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 450,
			height: 410,
			minWidth: 450,
			minHeight: 410
		},
		// Form panel config
		{
			formPanelID: formPanelID,
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'MailHosting'
			},
			items: [
				{
					xtype: 'combo',
					id: agentComboBoxID,
					fieldLabel: 'Agent',
					name: 'Agent',
					allowBlank: false,
					width: 160,

					store: agentStore,
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'AgentID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false,
					disabled: editMode
				},
				{
					fieldLabel: 'Domain Name',
					name: 'DomainName',
					vtype: 'domain',
					maskRe: domainRe,
					allowBlank: false,
					
					disabled: editMode
				},
				{
					// We use an ID so we can get the box later
					id: transportComboboxID,
					xtype: 'combo',

					fieldLabel: 'Transport',
					allowBlank: false,
					width: 100,

					mode: 'local',
					store: [ [ 'virtual', 'Virtual' ]  , [ 'smtp', 'SMTP' ] ],
					hiddenName: 'Transport',

					forceSelection: false,
					triggerAction: 'all',
					editable: false,
					
					disabled: editMode
				},
				{
					// We use an ID so we can get the field later
					id: transportSMTPServerID,
					fieldLabel: 'SMTP Server',
					name: 'Detail',
//					vtype: 'domain',
					allowBlank: false,
					
					disabled: editMode
				},
				{
					fieldLabel: 'Agent Ref',
					name: 'AgentRef'
				},/*,
				{
					xtype: 'tabpanel',
					plain: 'true',
					deferredRender: false, // Load all panels!
					activeTab: 0,
					height: 100,
					defaults: {
						layout: 'form',
						bodyStyle: 'padding: 10px;'
					},
					
					items: [
						{
							title: 'Policy Settings',
							layout: 'form',
							defaultType: 'textfield',
							items: [
								{
									fieldLabel: 'Transport Policy',
									name: 'Policy',
									vtype: 'number',
									value: '1'
								}
							]
						}
					]
				}*/
				getStandardServicePanel(formPanelID)
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	// Events
	if (!transportID) {
		Ext.getCmp(transportComboboxID).on({
			select: {
				fn: function() {
					var tb = Ext.getCmp(transportSMTPServerID);

					if (this.getValue() == 'smtp') {
						tb.setValue('smtp.server.here');
						tb.enable();
					} else {
						tb.setValue('n/a');
						tb.disable();
					}
				}
			}
		});
	}

	mailTransportFormWindow.show();

	if (transportID) {
		Ext.getCmp(mailTransportFormWindow.formPanelID).load({
			params: {
				ServerGroupID: serverGroupID,
				ID: transportID,
				SOAPModule: 'MailHosting',
				SOAPFunction: 'getMailTransport',
				SOAPParams: 'ServerGroupID,ID'
			}
		});
	}

	initStandardServicePanel(formPanelID);
}


// Display remove form
function showMailTransportRemoveWindow(mailTransportWindow,serverGroupID,transportID) {
	// Mask mailTransportWindow window
	mailTransportWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this mail transport?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(mailTransportWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: transportID,
						SOAPModule: 'MailHosting',
						SOAPFunction: 'removeMailTransport',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(mailTransportWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});

			// Unmask if user answered no
			} else {
				mailTransportWindow.getEl().unmask();
			}
		}
	});
}



// vim: ts=4
