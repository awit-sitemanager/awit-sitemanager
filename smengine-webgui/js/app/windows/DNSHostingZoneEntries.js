/*
DNS Zone Entries
Copyright (C) 2007-2010, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/




function showZoneEntriesWindow(serverGroupID,serverGroupName,domainName,zoneID) {
	
	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("DNSHosting/ZoneEntries/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add entry',
			iconCls:'silk-script_add',
			handler: function() {
				showDNSZoneEntryEditWindow(DNSZoneEntryWindow,serverGroupID,serverGroupName,domainName,zoneID);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("DNSHosting/ZoneEntries/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit entry',
			iconCls:'silk-script_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(DNSZoneEntryWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showDNSZoneEntryEditWindow(DNSZoneEntryWindow,serverGroupID,serverGroupName,domainName,zoneID,selectedItem.data.ID,selectedItem.data.Name);
				} else {
					DNSZoneEntryWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No entry selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							DNSZoneEntryWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("DNSHosting/ZoneEntries/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove entry',
			iconCls:'silk-script_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(DNSZoneEntryWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showDNSZoneEntryRemoveWindow(DNSZoneEntryWindow,serverGroupID,selectedItem.data.ID);
				} else {
					DNSZoneEntryWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No entry selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							DNSZoneEntryWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var DNSZoneEntryWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Zone Entries - "+serverGroupName+" - "+domainName,
			iconCls: 'silk-script',
			
			width: 500,
			height: 335,
			minWidth: 500,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars (buttons)
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					hidden: true,
					dataIndex: 'ID'
				},
				{
					id: 'TTL',
					header: "TTL",
					hidden: true,
					dataIndex: 'TTL'
				},
				{
					id: 'Name',
					header: "Name",
					sortable: true,
					dataIndex: 'Name'
				},
				{
					header: "Type",
					sortable: true,
					dataIndex: 'Type'
				},
				{
					header: "Aux",
					dataIndex: 'Aux'
				},
				{
					header: "Data",
					dataIndex: 'Data'
				},
				{
					header: "AgentRef",
					sortable: true,
					dataIndex: 'AgentRef'
				},
				{
					header: "Status",
					renderer: renderNiceBooleanColumn,
					align: 'center',
					sortable: false,
					dataIndex: 'Disabled'
				}
			])
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				SOAPModule: 'DNSHosting',
				SOAPFunction: 'getDNSZoneEntries',
				ZoneID: zoneID,
				SOAPParams: 'ServerGroupID,ZoneID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Name'},
				{type: 'string',  dataIndex: 'Type'}
			]
		}
	);
	DNSZoneEntryWindow.show();
}

// Display edit/add form
function showDNSZoneEntryEditWindow(DNSZoneEntryWindow,serverGroupID,serverGroupName,domainName,zoneID,zoneEntryID,entryName) {

	var submitAjaxConfig;
	var icon;
	var title;


	// We doing an update
	if (zoneEntryID) {
		icon = 'silk-script_edit';
		title = "Entry - "+serverGroupName+" - "+domainName+" - "+entryName;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				ID: zoneEntryID,
				SOAPFunction: 'updateDNSZoneEntry',
				SOAPParams: 
					'ServerGroupID,'+
					'1:ID,'+
					'1:TTL,'+
					'1:Name,'+
					'1:Type,'+
					'1:Aux,'+
					'1:Data,'+
					'1:DDNSUserID,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(DNSZoneEntryWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};

	// We doing an Add
	} else {
		icon = 'silk-script_add';
		title = "Entry - "+serverGroupName+" - "+domainName;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				SOAPFunction: 'createDNSZoneEntry',
				ZoneID: zoneID,
				SOAPParams: 
					'ServerGroupID,'+
					'1:ZoneID,'+
					'1:TTL,'+
					'1:Name,'+
					'1:Type,'+
					'1:Aux,'+
					'1:Data,'+
					'1:DDNSUserID,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(DNSZoneEntryWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}
	
	// Grab dynadns user store
	var dynaDNSUserStore = new Ext.ux.JsonStore({
			ID: zoneEntryID,
			sortInfo: { field: "Username", direction: "ASC" },
			baseParams: {
				SOAPModule: 'DNSHosting/DDNS',
				SOAPFunction: 'getDDNSUsers'
			}
	});

	// Form panel ID
	var formPanelID = Ext.id();

	// Set combobox ID
	var DDNSBoxID = Ext.id();

	// Create window
	var DNSZoneEntryFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 450,
			height: 440,
			minWidth: 450,
			minHeight: 440
		},
		// Form panel config
		{
			formPanelID: formPanelID,
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'DNSHosting'
			},
			items: [
				{
					xtype: 'numberfield',
					fieldLabel: 'TTL',
					value: 3600,
					name: 'TTL'
				},
				{
					fieldLabel: 'Name',
					name: 'Name'
				},
				{
					fieldLabel: 'Type',
					name: 'Type',
					xtype: 'combo',
					allowBlank: false,
					width: 100,
					mode: 'local',
					store: [ 
							'A',
							'AAAA',
							'CNAME',
							'HINFO',
							'MX',
							'NS',
							'PTR',
							'RP',
							'SRV',
							'TXT'
						],
					Name: 'Type',	
					forceSelection: false,
					triggerAction: 'all',
					editable: false,
					valueField: 'ID'
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'Aux',
					value: 0,
					minValue: 0,
					name: 'Aux'
				},
				{
					fieldLabel: 'Data',
					name: 'Data'
				},
				{
					fieldLabel: 'AgentRef',
					name: 'AgentRef'
				},

				getStandardServicePanel(
						formPanelID,
					 	[
							{
								title: 'Dynamic DNS',
								iconCls: 'silk-user_go',
								layout: 'form',
								defaultType: 'textfield',
								buttonAlign: 'center',
								buttons: [
									{
										text: 'Remove',
										tooltip: 'Remove DDNS user',
										iconCls: 'silk-user_delete',
										handler: function() {
											var userbox = Ext.getCmp(DDNSBoxID);
											userbox.clearValue();
										}
									}
								],
								items: [
									{
										xtype: 'combo',
										id: DDNSBoxID,
										fieldLabel: 'DDNS User',
										name: 'DDNSUserID',
										allowBlank: true,

										store: dynaDNSUserStore,
										displayField: 'Username',
										valueField: 'ID',
										hiddenName: 'DDNSUserID',
										forceSelection: true,
										triggerAction: 'all',
										editable: false
									}
								]
							}
						]
				)
			]
		},
		// Submit button config
		submitAjaxConfig
	);
	
	DNSZoneEntryFormWindow.show();

	// Load DDNS users
	dynaDNSUserStore.load();

	if (zoneEntryID) {
		Ext.getCmp(DNSZoneEntryFormWindow.formPanelID).load({
			params: {
				ServerGroupID: serverGroupID,
				ID: zoneEntryID,
				SOAPModule: 'DNSHosting',
				SOAPFunction: 'getDNSZoneEntry',
				SOAPParams: 'ServerGroupID,ID'
			}
		});
	}

	initStandardServicePanel(formPanelID);
}

// Display edit/add form
function showDNSZoneEntryRemoveWindow(DNSZoneEntryWindow,serverGroupID,zoneEntryID) {
	// Mask parent window
	DNSZoneEntryWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this entry?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(DNSZoneEntryWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: zoneEntryID,
						SOAPModule: 'DNSHosting',
						SOAPFunction: 'removeDNSZoneEntry',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(DNSZoneEntryWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				DNSZoneEntryWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
