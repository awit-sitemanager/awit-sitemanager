

function showWebHostingDatabasesWindow(siteID) {

	var webHostingDatabaseWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Databases",
			width: 500,
			height: 335,
			minWidth: 500,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: [
				{
					text:'Add',
					tooltip:'Add database',
					iconCls:'add',
					handler: function() {
						showWebHostingDatabaseEditWindow(siteID,0);
					}
				}, 
				'-', 
				{
					text:'Edit',
					tooltip:'Edit database',
					iconCls:'option',
					handler: function() {
						var selectedItem = Ext.getCmp(webHostingDatabaseWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showWebHostingDatabaseEditWindow(siteID,selectedItem.data.ID);
						} else {
							webHostingDatabaseWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No database selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									webHostingDatabaseWindow.getEl().unmask();
								}
							});
						}
					}
				},
				'-',
				{
					text:'Remove',
					tooltip:'Remove database',
					iconCls:'remove',
					handler: function() {
						var selectedItem = Ext.getCmp(webHostingDatabaseWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showWebHostingDatabaseRemoveWindow(webHostingDatabaseWindow,selectedItem.data.ID);
						} else {
							webHostingDatabaseWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No database selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									webHostingDatabaseWindow.getEl().unmask();
								}
							});
						}
					}
				},
				'-',
				{
					text:'Database Users',
					tooltip:'Website Database Users',
					iconCls:'databaseUsers',
					handler: function() {
						var selectedItem = Ext.getCmp(webHostingDatabaseWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showDatabaseUsersWindow(selectedItem.data.ID);
						} else {
							webHostingDatabaseWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No database selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									webHostingDatabaseWindow.getEl().unmask();
								}
							});
						}
					}
				},
			],
			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "SystemDatabaseID",
					hidden: true,
					sortable: true,
					dataIndex: 'SystemDatabaseID'
				},
				{
					header: "SystemDatabaseName",
					//hidden: true,
					sortable: true,
					dataIndex: 'SystemDatabaseName'
				},
				{
					header: "DatabaseName",
					//hidden: true,
					sortable: true,
					dataIndex: 'DatabaseName'
				},
				{
					header: "Username",
					sortable: true,
					dataIndex: 'Username'
				},
				{
					header: "Password",
					sortable: true,
					dataIndex: 'Password'
				},
				{
					header: "AgentDisabled",
					sortable: true,
					dataIndex: 'AgentDisabled'
				}
			]),
			autoExpandColumn: 'Name'
		},
		// Store config
		{
			baseParams: {
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting/Database',
				SOAPFunction: 'getDatabases',
				WebsiteID: siteID,
				SOAPParams: 'WebsiteID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'SystemDatabaseID'},
				{type: 'string',  dataIndex: 'SystemDatabaseName'},
				{type: 'string',  dataIndex: 'DatabaseName'},
				{type: 'string',  dataIndex: 'Username'},
				{type: 'string',  dataIndex: 'Password'}
			]
		}
	);

	webHostingDatabaseWindow.show();
}


// Display edit/add form
function showWebHostingDatabaseEditWindow(siteID,id) {

	var submitAjaxConfig;
	var editMode;

	// We doing an update
	if (id) {
		submitAjaxConfig = {
			ID: id,
			SOAPFunction: 'updateDatabase',
			SOAPParams: 
				'0:ID,'+
				'0:Username,'+
				'0:Password,'+
				'0:SystemDatabaseID,'+
				'0:SystemDatabaseName,'+
				'0:DatabaseName,'+
				'0:AgentDisabled:boolean'
		};
		editMode = true;
	// We doing an Add
	} else {
		submitAjaxConfig = {
			SOAPFunction: 'createDatabase',
			WebsiteID: siteID,
			SOAPParams: 
				'0:WebsiteID,'+
				'0:Username,'+
				'0:Password,'+
				'0:SystemDatabaseID,'+
				'0:SystemDatabaseName,'+
				'0:DatabaseName,'+
				'0:AgentDisabled:boolean'
		};
		editMode = false;
	}

	// Create window
	var webHostingDatabaseFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Database Information",

			width: 320,
			height: 260,

			minWidth: 320,
			minHeight: 260
		},
		// Form panel config
		{
			labelWidth: 95,
			baseParams: {
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting/Database'
			},
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'System Database Name',
					name: 'SystemDatabaseName',
					allowBlank: false,
					width: 160,
					store: new Ext.ux.JsonStore({
						ID: id,
						sortInfo: { field: "SystemDatabaseName", direction: "ASC" },
						baseParams: {
							SOAPUsername: globalConfig.soap.username,
							SOAPPassword: globalConfig.soap.password,
							SOAPAuthType: globalConfig.soap.authtype,
							SOAPModule: 'WebHosting/Database',
							SOAPFunction: 'getSystemDatabases',
							SOAPParams: '__search'
						}
					}),
					displayField: 'SystemDatabaseName',
					valueField: 'ID',
					hiddenName: 'SystemDatabaseID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false,
					disabled: editMode
				},
				{
					fieldLabel: 'UserName',
					name: 'Username',
					vtype: 'username',
					maskRe: usernameRe,
					allowBlank: false
				},
				{
					id: 'password_field',
					fieldLabel: 'Password',
					name: 'Password',
					width: 100
				},
				{
					fieldLabel: 'Database Name',
					name: 'DatabaseName',
					allowBlank: false
				},
				{
					xtype: 'checkbox',
					fieldLabel: 'AgentDisabled',
					name: 'AgentDisabled'
				}
			],
			extrabuttons: [ 
				{
					text: 'MkPass',
					handler: function() {
						var panel = this.ownerCt;
						var win = panel.ownerCt;
						var passfield = win.findById('password_field');
						passfield.setValue(getRandomPass(8));
					}
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	webHostingDatabaseFormWindow.show();

	if (id) {
		Ext.getCmp(webHostingDatabaseFormWindow.formPanelID).load({
			params: {
				id: id,
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting/Database',
				SOAPFunction: 'getDatabase',
				SOAPParams: 'id'
			}
		});
	}
}

// Display edit/add form
function showWebHostingDatabaseRemoveWindow(parent,id) {
	// Mask parent window
	parent.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this database?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(parent,{
					params: {
						id: id,
						SOAPUsername: globalConfig.soap.username,
						SOAPPassword: globalConfig.soap.password,
						SOAPAuthType: globalConfig.soap.authtype,
						SOAPModule: 'WebHosting/Database',
						SOAPFunction: 'removeDatabase',
						SOAPParams: 'id'
					}
				});

			// Unmask if user answered no
			} else {
				parent.getEl().unmask();
			}
		}
	});
}

