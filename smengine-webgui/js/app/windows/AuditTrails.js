/*
Audit Trails
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAuditTrailsWindow() {

	// Buttons
	var buttons = [ ];

	if (userHasCapability("AuditTrails/Get")) {
		buttons.push({
			text:'Details',
			tooltip:'View details',
			iconCls:'silk-report',
			handler: function() {
				var selectedItem = Ext.getCmp(auditTrailsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAuditTrailsDetailsWindow(auditTrailsWindow,selectedItem.data.ID);
				} else {
					auditTrailsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No record selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							auditTrailsWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var auditTrailsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Audit Trails",
			iconCls: "silk-report",
			
			width: 820,
			height: 400,
		
			minWidth: 820,
			minHeight: 400
		},
		// Grid config
		{
			// Inline toolbars (buttons)
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Timestamp",
					sortable: true,
					width: 90,
					dataIndex: 'Timestamp',
					renderer: function (value) {
						return formatUnixTimestamp(value, 'Y-m-d H:i:s');
					}
				},
				{
					header: "ClientIP",
					hidden: true,
					sortable: true,
					dataIndex: 'ClientIP'
				},
				{
					header: "Protocol",
					hidden: true,
					sortable: true,
					dataIndex: 'Protocol'
				},
				{
					header: "AgentID",
					sortable: true,
					dataIndex: 'AgentID',
					width: 41
				},
				{
					header: "Username",
					sortable: true,
					dataIndex: 'Username'
				},
				{
					header: "ServerCluster",
					sortable: true,
					dataIndex: 'ServerCluster'
				},
				{
					header: "ServerName",
					hidden: true,
					sortable: true,
					dataIndex: 'ServerName'
				},
				{
					header: "Module",
					sortable: true,
					dataIndex: 'Module'
				},
				{
					header: "Function",
					sortable: true,
					dataIndex: 'Function'
				},
				{
					header: "Result",
					sortable: true,
					dataIndex: 'Result'
				}
			]),
			autoExpandColumn: 'Function'
		},
		// Store config
		{
			sortInfo: { field: "Timestamp", direction: "DESC" },
			baseParams: {
				SOAPModule: 'AuditTrails',
				SOAPFunction: 'getAuditTrails',
				SOAPParams: '__null,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Timestamp'},
				{type: 'string', dataIndex: 'ClientIP'},
				{type: 'string', dataIndex: 'Protocol'},
				{type: 'numeric',  dataIndex: 'AgentID'},
				{type: 'string',  dataIndex: 'Username'},
				{type: 'string', dataIndex: 'ServerCluster'},
				{type: 'string', dataIndex: 'ServerName'},
				{type: 'string',  dataIndex: 'Module'},
				{type: 'string',  dataIndex: 'Function'},
				{type: 'string',  dataIndex: 'Result'}
			]
		}
	);

	auditTrailsWindow.show();
}


// More detailed audit trail information
function showAuditTrailsDetailsWindow(auditTrailsWindow,auditTrailID) {

	// Timestamp ID
	var timestampID = Ext.id();

	// Create window
	var auditTrailFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Audit Trail Information",

			width: 420,
			height: 365,

			minWidth: 420,
			minHeight: 365,

			layout: 'fit'
		},
		// Form panel config
		{
			labelWidth: 95,
			buttons: [],
			baseParams: {
				SOAPModule: 'AuditTrails'
			},
			items: [
				{
					id: timestampID,
					xtype: 'displayfield',
					fieldLabel: "Timestamp",
					name: 'FormattedTimestamp'
				},
				{
					xtype: 'displayfield',
					fieldLabel: "ClientIP",
					name: 'ClientIP'
				},
				{
					xtype: 'displayfield',
					fieldLabel: "Protocol",
					name: 'Protocol'
				},
				{
					xtype: 'displayfield',
					fieldLabel: "AgentID",
					name: 'AgentID'
				},
				{
					xtype: 'displayfield',
					fieldLabel: "Username",
					name: 'Username'
				},
				{
					xtype: 'displayfield',
					fieldLabel: "ServerCluster",
					name: 'ServerCluster'
				},
				{
					xtype: 'displayfield',
					fieldLabel: "ServerName",
					name: 'ServerName'
				},
				{
					xtype: 'displayfield',
					fieldLabel: "Module",
					name: 'Module'
				},
				{
					xtype: 'displayfield',
					fieldLabel: "Function",
					name: 'Function'
				},
				{
					xtype: 'displayfield',
					fieldLabel: "Result",
					name: 'Result'
				},
				{
					xtype: 'displayfield',
					fieldLabel: "Parameters",
					name: 'Parameters',
					height: 40,
					width: 100
				}
			]
		}
	);

	auditTrailFormWindow.show();

	Ext.getCmp(auditTrailFormWindow.formPanelID).load({
		params: {
			ID: auditTrailID,
			SOAPModule: 'AuditTrails',
			SOAPFunction: 'getAuditTrail',
			SOAPParams: 'ID'
		},
		success: function (success, data) {
			// Get timestamp field
			var timestampField = Ext.getCmp(timestampID);

			// Format timestamp
			var formattedTimestamp = formatUnixTimestamp(data.result.data.Timestamp, 'Y-m-d H:i:s');

			// Set the timestampfield
			timestampField.setValue(formattedTimestamp);
		}
	});
}


// vim: ts=4
