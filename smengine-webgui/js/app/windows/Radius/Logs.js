

function showRadiusUserLogsWindow(serverGroupID,serverGroupName,username,radiusUserID) {
	// Calculate dates we going to need
	var today = new Date();
	var firstOfMonth = today.getFirstDateOfMonth();
	var firstOfNext = today.getLastDateOfMonth().add(Date.DAY, 1);

	// Our IDs
	var searchFormID = Ext.id();
	var summaryFormID = Ext.id();
	var summaryTotalID = Ext.id();

	var dateFrom = Ext.id();
	var dateTo = Ext.id();
	
	var radiusUserLogsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: 'Radius User Logs - '+serverGroupName+' - '+username,
			layout:'border',
			height: 480,
			width: 700,
			minHeight: 480,
			minWidth: 700,
			closable: true,
			plain: true,
			iconCls: 'silk-page_white_text',
			uxItems: [
				{
					xtype: 'form',
					id: searchFormID,
					title: 'Search',
					region: 'west',
					border: true,
					frame: true,
					defaultType: 'datefield',
					height: 180,
					width: 300,
					labelWidth: 100,
					items: [
						{
							id: dateFrom,
							name: 'after',
							width: 180,
							fieldLabel: 'From',
							vtype: 'daterange',
							format: 'Y-m-d',
							value: firstOfMonth,
							endDateField: dateTo
						},
						{
							id: dateTo,
							name: 'before',
							width: 180,
							fieldLabel: 'To',
							vtype: 'daterange',
							format: 'Y-m-d',
							value: firstOfNext,
							startDateField: dateFrom
						}
					],
					buttons: [
						{
							text: 'Search',
							id: 'formbtn',
							handler: function() {
								// Pull in window, grid & form	
								var grid = Ext.getCmp(radiusUserLogsWindow.gridPanelID);
								var form = Ext.getCmp(searchFormID);

								// Grab store
								var store = grid.getStore();

								// Grab timestamp filter
								var gridFilters = grid.filters;
								var timestampFilter = gridFilters.getFilter('Timestamp');

								// Grab	form fields
								var afterField = Ext.getCmp(dateFrom);
								var beforeField = Ext.getCmp(dateTo);

								// Set filter values from form
								timestampFilter.setValue({
									after: afterField.getValue(),
									before: beforeField.getValue()
								});

								// Trigger store reload
								store.reload();
							}
						}
					],
					buttonAlign: 'center'
				},
				{
					xtype: 'form',
					id: summaryFormID,
					region: 'center',
					split: true,
					border: true,
					autoScroll: true,
					defaultType: 'textarea',
					height: 180,
					width: 400,
					labelWidth: 80,
					items: [
						{
							id: summaryTotalID,
							name: 'summaryTotal',
							readOnly: true,
							height: 135,
							width: 250,
							fieldLabel: 'Summary',
							fieldClass: 'font-family: monospace; font-size: 10px;',
							value: ''
						}
					]					
				}
			]
		},
		// Grid config
		{
			region: 'south',
			width: 700,
			border: true,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					hidden: true,
					dataIndex: 'ID'
				},
				{
					header: "Username",
					hidden: true,
					dataIndex: 'Username'
				},
				{
					header: "Status",
					sortable: true,
					hidden: true,
					dataIndex: 'Status'
				},
				{
					header: "Timestamp",
					sortable: true,
					dataIndex: 'Timestamp'
				},
				{
					header: "Session ID",
					hidden: true,
					dataIndex: 'AcctSessionID'
				},
				{
					header: "Session Time",
					dataIndex: 'AcctSessionTime'
				},
				{
					header: "NAS IP",
					hidden: true,
					dataIndex: 'NASIPAddress'
				},
				{
					header: "Port Type",
					hidden: true,
					dataIndex: 'NASPortType'
				},
				{
					header: "NAS Port",
					dataIndex: 'NASPort'
				},
				{
					header: "Called Station",
					hidden: true,
					dataIndex: 'CalledStationID'
				},
				{
					header: "Calling Station",
					sortable: true,
					dataIndex: 'CallingStationID'
				},
				{
					header: "NAS Xmit Rate",
					dataIndex: 'NASTransmitRate'
				},
				{
					header: "NAS Recv Rate",
					hidden: true,
					dataIndex: 'NASReceiveRate'
				},
				{
					header: "IP Address",
					hidden: true,
					dataIndex: 'FramedIPAddress'
				},
				{
					header: "Input Mbyte",
					dataIndex: 'AcctInputMbyte'
				},
				{
					header: "Output Mbyte",
					dataIndex: 'AcctOutputMbyte'
				},
				{
					header: "Last Update",
					hidden: true,
					dataIndex: 'LastAcctUpdate'
				},
				{
					header: "Term. Reason",
					dataIndex: 'ConnectTermReason'
				}
			])
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				UserID: radiusUserID,
				SOAPModule: 'Radius',
				SOAPFunction: 'getRadiusUserLogs',
				SOAPParams: 'ServerGroupID,1:UserID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Username'},
				{type: 'numeric',  dataIndex: 'Status'},
				{
					type: 'date',  
					dataIndex: 'Timestamp', 
					value: {
						after: firstOfMonth,
						before: firstOfNext
					}
				},

				{type: 'string',  dataIndex: 'AcctSessionID'},
				{type: 'numeric',  dataIndex: 'AcctSessionTime'},

				{type: 'string',  dataIndex: 'NASIPAddress'},
				{type: 'string',  dataIndex: 'NASPortType'},
				{type: 'string',  dataIndex: 'NASPort'},
				{type: 'string',  dataIndex: 'CalledStationID'},
				{type: 'string',  dataIndex: 'CallingStationID'},

				{type: 'string',  dataIndex: 'NASTransmitRate'},
				{type: 'string',  dataIndex: 'NASReceiveRate'},

				{type: 'string',  dataIndex: 'FramedIPAddress'},

				{type: 'date',  dataIndex: 'LastAcctUpdate'},

				{type: 'string',  dataIndex: 'ConnectTermReason'}
			]
		}
	);

	// Grab store
	var store = Ext.getCmp(radiusUserLogsWindow.gridPanelID).getStore();

	// Populate summary
	store.on('load',function() {

		// Current period
		var currentPeriod = new Date().format("Y-m");

		// Mask parent window
		radiusUserLogsWindow.getEl().mask();

		// Make ajax call
		var userTopups = 0;
		uxAjaxRequest(
			radiusUserLogsWindow,
			{
				params: {
					ServerGroupID: serverGroupID,
					UserID: radiusUserID,
					Period: currentPeriod,
					SOAPModule: 'Radius/Topups',
					SOAPFunction: 'getRadiusUserTopupsSummary',
					SOAPParams: 'ServerGroupID,1:UserID,1:Period'
				},

				customSuccess: function (result) {
					var response = Ext.decode(result.responseText);
					userTopups = response.data.TopupTotal;
				},
				failure: function (result) {
					Ext.MessageBox.alert('Failed', 'Couldn\'t retrieve radius topups summary: '+result.date);
				}
			}
		);
		// Make ajax call
		uxAjaxRequest(
			radiusUserLogsWindow,
			{
				params: {
					ServerGroupID: serverGroupID,
					UserID: radiusUserID,
					Period: currentPeriod,
					SOAPModule: 'Radius',
					SOAPFunction: 'getRadiusUserLogsSummary',
					SOAPParams: 'ServerGroupID,1:UserID,1:Period'
				},

				customSuccess: function (result) {
					var response = Ext.decode(result.responseText);

					// Build string
					var summaryString = "";
					summaryString += "Period   : "+currentPeriod+"\n";
					summaryString += "Topups   : "+userTopups+" Mbyte\n";
					summaryString += "Usage    : "+response.data.UsageTotal+"\n";
					summaryString += "Last IP  : "+response.data.LastFramedIPAddress+"\n";

					// Get summary field
					var summaryField = Ext.getCmp(summaryTotalID);
					summaryField.setValue(summaryString);
				},
				failure: function (result) {
					Ext.MessageBox.alert('Failed', 'Couldn\'t retrieve radius logs summary: '+result.date);
				}
			}
		);
	});

	radiusUserLogsWindow.show();				
}
