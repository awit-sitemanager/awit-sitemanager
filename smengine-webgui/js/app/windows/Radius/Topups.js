/*
Radius Topups
Copyright (C) 2007-2009, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/






function showRadiusUserTopupsWindow(serverGroupID,serverGroupName,username,userID) {

	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Radius/Users/Topups/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add topup',
			iconCls:'silk-chart_bar_add',
			handler: function() {
				showRadiusUserTopupEditWindow(radiusUserTopupsWindow,serverGroupID,serverGroupName,username,userID,0);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Radius/Users/Topups/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit topup',
			iconCls:'silk-chart_bar_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(radiusUserTopupsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showRadiusUserTopupEditWindow(radiusUserTopupsWindow,serverGroupID,serverGroupName,username,userID,selectedItem.data.ID);
				} else {
					radiusUserTopupsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No topup selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							radiusUserTopupsWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Radius/Users/Topups/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove topup',
			iconCls:'silk-chart_bar_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(radiusUserTopupsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showRadiusUserTopupRemoveWindow(radiusUserTopupsWindow,serverGroupID,selectedItem.data.ID);
				} else {
					radiusUserTopupsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No topup selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							radiusUserTopupsWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	var radiusUserTopupsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Radius User Topups - "+serverGroupName+" - "+username,
			iconCls: 'silk-chart_bar',

			width: 500,
			height: 335,

			minWidth: 500,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					hidden: true,
					dataIndex: 'ID'
				},
				{
					header: "Bandwidth",
					sortable: true,
					dataIndex: 'Bandwidth'
				},
				{
					header: "Timestamp",
					sortable: true,
					hidden: true,
					dataIndex: 'Timestamp'
				},
				{
					header: "ValidFrom",
					sortable: true,
					dataIndex: 'ValidFrom'
				},
				{
					header: "ValidTo",
					sortable: true,
					dataIndex: 'ValidTo'
				},
				{
					header: "AgentRef",
					sortable: true,
					dataIndex: 'AgentRef'
				}
			])
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				UserID: userID,
				SOAPModule: 'Radius/Topups',
				SOAPFunction: 'getRadiusUserTopups',
				SOAPParams: 'ServerGroupID,UserID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'Bandwidth'},
				{type: 'date',  dataIndex: 'Timestamp'},
				{type: 'date',  dataIndex: 'ValidFrom'},
				{type: 'date',  dataIndex: 'ValidTo'},
				{type: 'string',  dataIndex: 'AgentRef'}
			]
		}
	);

	radiusUserTopupsWindow.show();
}


// Display edit/add form
function showRadiusUserTopupEditWindow(radiusUserTopupsWindow,serverGroupID,serverGroupName,username,userID,topupID) {
	var today = new Date();
	var firstOfMonth = today.getFirstDateOfMonth();
	var firstOfNext = today.getLastDateOfMonth().add(Date.DAY, 1);

	var submitAjaxConfig;
	var icon;

	// We doing an update
	if (topupID) {
		icon = "silk-chart_bar_edit";
		title = "Topup - "+serverGroupName+" - "+username+" - "+topupID;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				ID: topupID,
				SOAPFunction: 'updateRadiusUserTopup',
				SOAPParams: 
					'ServerGroupID,'+
					'1:ID,1:Bandwidth,'+
					'1:Timestamp,1:ValidFrom,1:ValidTo,1:AgentRef'
			},
			onSuccess: function() {
				var store = Ext.getCmp(radiusUserTopupsWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	// We doing an Add
	} else {
		icon = "silk-chart_bar_add";
		title = "Topup - "+serverGroupName+" - "+username;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				UserID: userID,
				SOAPFunction: 'createRadiusUserTopup',
				SOAPParams: 
					'ServerGroupID,'+
					'1:UserID,1:Bandwidth,'+
					'1:Timestamp,1:ValidFrom,1:ValidTo,1:AgentRef'
			},
			onSuccess: function() {
				var store = Ext.getCmp(radiusUserTopupsWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}

	// Create window
	var radiusUserTopupFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 400,
			height: 200,

			minWidth: 400,
			minHeight: 200
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Radius/Topups'
			},
			items: [
				{
					xtype: 'numberfield',
					fieldLabel: 'Bandwidth',
					name: 'Bandwidth',
					minValue: 1,
					allowBlank: false
				},
				{
					xtype: 'datefield', 
					fieldLabel: 'ValidFrom',
					name: 'ValidFrom',
					id: 'ValidFrom',
					vtype: 'daterange',
					disabledDates: ["([^1]|[1-3]1)$"],
					disabledDatesText: "Please select first day of month",
					value: firstOfMonth,
					format: 'Y-m-d',
					endDateField: 'ValidTo'

				},
				{
					xtype: 'datefield',
					fieldLabel: 'ValidTo',
					name: 'ValidTo',
					id: 'ValidTo',
					vtype: 'daterange',
					disabledDates: ["([^1]|[1-3]1)$"],
					disabledDatesText: "Please select first day of month",
					value: firstOfNext,
					format: 'Y-m-d',
					startDateField: 'ValidFrom'
				},
				{
					fieldLabel: 'AgentRef',
					name: 'AgentRef'
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	radiusUserTopupFormWindow.show();

	if (topupID) {
		Ext.getCmp(radiusUserTopupFormWindow.formPanelID).load({
			params: {
				ServerGroupID: serverGroupID,
				ID: topupID,
				SOAPModule: 'Radius/Topups',
				SOAPFunction: 'getRadiusUserTopup',
				SOAPParams: 'ServerGroupID,ID'
			}
		});
	}
}




// Display edit/add form
function showRadiusUserTopupRemoveWindow(radiusUsersTopupsWindow,serverGroupID,topupID) {
	// Mask radiusUsersTopupsWindow window
	radiusUsersTopupsWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this topup?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(radiusUsersTopupsWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: topupID,
						SOAPModule: 'Radius/Topups',
						SOAPFunction: 'removeRadiusUserTopup',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(radiusUsersTopupsWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				radiusUsersTopupsWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
