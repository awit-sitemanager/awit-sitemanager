/*
Radius port locking
Copyright (C) 2007-2009, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/




function showRadiusUserPortLocksWindow(serverGroupID,serverGroupName,username,userID) {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Radius/Users/PortLocks/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add port lock',
			iconCls:'silk-lock_add',
			handler: function() {
				showRadiusUserPortLockEditWindow(radiusUserPortLocksWindow,serverGroupID,serverGroupName,username,userID,0);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Radius/Users/PortLocks/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit port lock',
			iconCls:'silk-lock_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(radiusUserPortLocksWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showRadiusUserPortLockEditWindow(radiusUserPortLocksWindow,serverGroupID,serverGroupName,username,userID,selectedItem.data.ID);
				} else {
					radiusUserPortLocksWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No port lock selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							radiusUserPortLocksWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Radius/Users/PortLocks/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove port lock',
			iconCls:'silk-lock_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(radiusUserPortLocksWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showRadiusUserPortLockRemoveWindow(radiusUserPortLocksWindow,serverGroupID,selectedItem.data.ID);
				} else {
					radiusUserPortLocksWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No port lock selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							radiusUserPortLocksWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

				
	var radiusUserPortLocksWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Port Locks - "+serverGroupName+" - "+username,
			iconCls: "silk-lock",
			
			width: 400,
			height: 335,

			minWidth: 400,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars (buttons)
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					hidden: true,
					dataIndex: 'ID'
				},
				{
					id: 'userID',
					header: "userID",
					sortable: true,
					hidden: true,
					dataIndex: 'userID'
				},
				{
					header: "NASPort",
					sortable: true,
					dataIndex: 'NASPort'
				},
				{
					header: "AgentRef",
					sortable: true,
					dataIndex: 'AgentRef'
				},
				{
					header: "Status",
					renderer: renderNiceBooleanColumn,
					sortable: false,
					dataIndex: 'Disabled'
				}
			])
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				UserID: userID,
				SOAPModule: 'Radius/PortLocking',
				SOAPFunction: 'getRadiusUserPortLocks',
				SOAPParams: 'ServerGroupID,UserID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'userID'},
				{type: 'numeric',  dataIndex: 'NASPort'},
				{type: 'string',  dataIndex: 'AgentRef'}
			]
		}
	);

	radiusUserPortLocksWindow.show();
}


// Display edit/add form
function showRadiusUserPortLockEditWindow(radiusUserPortLocksWindow,serverGroupID,serverGroupName,username,userID,portLockID) {

	var submitAjaxConfig;
	var icon;
	var title;

	// We doing an update
	if (portLockID) {
		icon = 'silk-lock_edit';
		title = "Port Lock - "+serverGroupName+" - "+username+" - "+portLockID;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				ID: portLockID,
				SOAPFunction: 'updateRadiusUserPortLock',
				SOAPParams: 
					'ServerGroupID,'+
					'1:ID,1:NASPort,'+
					'1:AgentRef,1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(radiusUserPortLocksWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	// We doing an Add
	} else {
		icon = 'silk-lock_add';
		title = "Port Lock - "+serverGroupName+" - "+username;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				UserID: userID,
				SOAPFunction: 'createRadiusUserPortLock',
				SOAPParams: 
					'ServerGroupID,'+
					'1:UserID,1:NASPort,'+
					'1:AgentRef,1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(radiusUserPortLocksWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}

	// Create window
	var radiusUserPortLockFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 300,
			height: 175,

			minWidth: 300,
			minHeight: 175
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Radius/PortLocking'
			},
			items: [
				{
					xtype: 'numberfield',
					fieldLabel: 'NASPort',
					name: 'NASPort',
					minValue: 1,
					allowBlank: false
				},
				{
					fieldLabel: 'AgentRef',
					name: 'AgentRef'
				},
				{
					xtype: 'checkbox',
					fieldLabel: 'AgentDisabled',
					name: 'AgentDisabled'
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	radiusUserPortLockFormWindow.show();

	if (portLockID) {
		Ext.getCmp(radiusUserPortLockFormWindow.formPanelID).load({
			params: {
				ServerGroupID: serverGroupID,
				ID: portLockID,
				SOAPModule: 'Radius/PortLocking',
				SOAPFunction: 'getRadiusUserPortLock',
				SOAPParams: 'ServerGroupID,ID'
			}
		});
	}
}




// Display edit/add form
function showRadiusUserPortLockRemoveWindow(radiusUserPortLocksWindow,serverGroupID,portLockID) {
	// Mask radiusUserPortLocksWindow window
	radiusUserPortLocksWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this port lock?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(radiusUserPortLocksWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: portLockID,
						SOAPModule: 'Radius/PortLocking',
						SOAPFunction: 'removeRadiusUserPortLock',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(radiusUserPortLocksWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				radiusUserPortLocksWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
