/*
DNS Hosting Zones
Copyright (C) 2007-2010, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showDNSZonesWindow(serverGroupID,serverGroupName) {

	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("DNSHosting/Zones/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add DNS zone',
			iconCls:'silk-table_add',
			handler: function() {
				showDNSZoneEditWindow(DNSZoneWindow,serverGroupID,serverGroupName);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("DNSHosting/Zones/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit DNS zone',
			iconCls:'silk-table_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(DNSZoneWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showDNSZoneEditWindow(DNSZoneWindow,serverGroupID,serverGroupName,selectedItem.data.ID,selectedItem.data.DomainName);
				} else {
					DNSZoneWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No DNS zone selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							DNSZoneWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("DNSHosting/Zones/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove DNS zone',
			iconCls:'silk-table_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(DNSZoneWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showDNSZoneRemoveWindow(DNSZoneWindow,serverGroupID,selectedItem.data.ID);
				} else {
					DNSZoneWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No DNS zone selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							DNSZoneWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("DNSHosting/ZoneEntries/List")) {
		buttons.push({ 
			text:'Entries',
			tooltip:'Entries',
			iconCls:'silk-script',
			handler: function() {
				var selectedItem = Ext.getCmp(DNSZoneWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showZoneEntriesWindow(serverGroupID,serverGroupName,selectedItem.data.DomainName,selectedItem.data.ID);
				} else {
					DNSZoneWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No DNS zone selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							DNSZoneWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var DNSZoneWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "DNS Zones - "+serverGroupName,
			iconCls: 'silk-table',
			
			width: 600,
			height: 335,
			minWidth: 600,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars (buttons)
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "AgentID",
					sortable: true,
					hidden: true,
					dataIndex: 'AgentID'
				},
				{
					header: "AgentName",
					sortable: false,
					dataIndex: 'AgentName'
				},
				{
					header: "Domain Name",
					sortable: true,
					dataIndex: 'DomainName'
				},
				{
					header: "Primary NS ID",
					hidden: true,
					dataIndex: 'NameserverID'
				},
				{
					header: "Primary NS",
					dataIndex: 'NameserverHostname'
				},
				{
					header: "Serial",
					hidden: true,
					dataIndex: 'Serial'
				},
				{
					header: "AdminEmail",
					dataIndex: 'AdminEmail'
				},
				{
					header: "Refresh",
					hidden: true,
					dataIndex: 'Refresh'
				},
				{
					header: "Retry",
					hidden: true,
					dataIndex: 'Retry'
				},
				{
					header: "Expire",
					hidden: true,
					dataIndex: 'Expire'
				},
				{
					header: "TTL",
					hidden: true,
					dataIndex: 'TTL'
				},
				{
					header: "MinTTL",
					hidden: true,
					dataIndex: 'MinTTL'
				},
				{
					header: "AgentRef",
					sortable: true,
					dataIndex: 'AgentRef'
				},
				{
					header: "Status",
					renderer: renderNiceBooleanColumn,
					align: 'center',
					sortable: false,
					dataIndex: 'Disabled'
				}
			]),
			autoExpandColumn: 'DomainName'
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				SOAPModule: 'DNSHosting',
				SOAPFunction: 'getDNSZones',
				SOAPParams: 'ServerGroupID,__null,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'AgentID'},
				{type: 'string',  dataIndex: 'DomainName'},
				{type: 'string',  dataIndex: 'AgentRef'}
			]
		}
	);
	DNSZoneWindow.show();
}

// Display edit/add form
function showDNSZoneEditWindow(DNSZoneWindow,serverGroupID,serverGroupName,zoneID,domainName) {

	var submitAjaxConfig;
	var icon;
	var title;


	// We doing an update
	if (zoneID) {
		icon = 'silk-table_edit';
		title = "DNS Zone - "+serverGroupName+" - "+domainName;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				ID: zoneID,
				SOAPFunction: 'updateDNSZone',
				SOAPParams: 
					'ServerGroupID,'+
					'1:ID,'+
					'1:AgentID,'+
					'1:DomainName,'+
					'1:NameserverID,'+
					'1:NameserverHostname,'+
					'1:Serial,'+
					'1:AdminEmail,'+
					'1:Refresh,'+
					'1:Retry,'+
					'1:Expire,'+
					'1:MinTTL,'+
					'1:TTL,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(DNSZoneWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	// We doing an Add
	} else {
		icon = 'silk-table_add';
		title = "DNS Zone - "+serverGroupName;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				SOAPFunction: 'createDNSZone',
				SOAPParams: 
					'ServerGroupID,'+
					'1:AgentID,'+
					'1:DomainName,'+
					'1:NameserverID,'+
					'1:NameserverHostname,'+
					'1:Serial,'+
					'1:AdminEmail,'+
					'1:Refresh,'+
					'1:Retry,'+
					'1:Expire,'+
					'1:MinTTL,'+
					'1:TTL,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(DNSZoneWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}

	// Agent store
	var agentStore = new Ext.ux.JsonStore({
			ID: zoneID,
			sortInfo: { field: "Name", direction: "ASC" },
			baseParams: {
				SOAPModule: 'Agent',
				SOAPFunction: 'getAgents',
				SOAPParams: '__search'
			}
	});

	// Nameserver store
	var nameserverStore = new Ext.ux.JsonStore({
			ID: zoneID,
			sortInfo: { field: "Hostname", direction: "ASC" },
			baseParams: {
				SOAPModule: 'DNSHosting',
				SOAPFunction: 'getDNSServers'
			}
	});

	// Form panel ID
	var formPanelID = Ext.id();

	// Create window
	var DNSZoneFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 450,
			height: 556,
			minWidth: 450,
			minHeight: 556
		},
		// Form panel config
		{
			formPanelID: formPanelID,
			labelWidth: 100,
			baseParams: {
				SOAPModule: 'DNSHosting'
			},
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Agent',
					name: 'Agent',
					allowBlank: false,
					width: 160,

					store: agentStore,
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'AgentID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false
				},
				{
					fieldLabel: 'Domain Name',
					name: 'DomainName',
					vtype: 'domain',
					maskRe: domainRe,
					allowBlank: false
				},
				{
					xtype: 'combo',
					fieldLabel: 'Primary NS',
					name: 'PrimaryNS',
					allowBlank: false,
					width: 100,

					store: nameserverStore,
					displayField: 'Hostname',
					valueField: 'ID',
					hiddenName: 'NameserverID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false
				},
				{
					fieldLabel: 'Admin Email',
					vtype: 'emailAddress',
					maskRe: emailAddressRe,
					allowBlank: false,
					name: 'AdminEmail'
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'Refresh',
					name: 'Refresh',
					value: 7200
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'Retry',
					name: 'Retry',
					value: 600
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'Expire',
					name: 'Expire',
					value: 2419200
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'TTL',
					name: 'TTL',
					value: 3600
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'MinTTL',
					name: 'MinTTL',
					value: 3600
				},
				{
					fieldLabel: 'AgentRef',
					name: 'AgentRef'
				},
				getStandardServicePanel(formPanelID)
			]
		},
		// Submit button config
		submitAjaxConfig
	);
	
	DNSZoneFormWindow.show();

	if (zoneID) {
		// Load agent store to resolve the agnetID => agentName
		agentStore.load();
		// Load nameserver store to resolve the nameserverID => nameserverhostname
		nameserverStore.load();

		Ext.getCmp(DNSZoneFormWindow.formPanelID).load({
			params: {
				ServerGroupID: serverGroupID,
				ID: zoneID,
				SOAPModule: 'DNSHosting',
				SOAPFunction: 'getDNSZone',
				SOAPParams: 'ServerGroupID,ID'
			}
		});
	}

	initStandardServicePanel(formPanelID);
}

// Display edit/add form
function showDNSZoneRemoveWindow(DNSZoneWindow,serverGroupID,zoneID) {
	// Mask DNSZoneWindow window
	DNSZoneWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this DNS Zone?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(DNSZoneWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: zoneID,
						SOAPModule: 'DNSHosting',
						SOAPFunction: 'removeDNSZone',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(DNSZoneWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});

			// Unmask if user answered no
			} else {
				DNSZoneWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
