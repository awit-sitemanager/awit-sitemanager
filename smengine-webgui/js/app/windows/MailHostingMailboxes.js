/* Mailboxes
Copyright (C) 2007-2010, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/



function showMailboxesWindow(serverGroupID,serverGroupName,transportID,domainName) {
	
	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("MailHosting/Mailboxes/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add mailbox',
			iconCls:'silk-email_add',
			handler: function() {
				showMailboxEditWindow(mailboxWindow,serverGroupID,serverGroupName,domainName,transportID);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/Mailboxes/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit mailbox',
			iconCls:'silk-email_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(mailboxWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailboxEditWindow(mailboxWindow,serverGroupID,serverGroupName,domainName,transportID,selectedItem.data.ID,selectedItem.data.Address);
				} else {
					mailboxWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mailbox selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailboxWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/Mailboxes/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove mailbox',
			iconCls:'silk-email_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(mailboxWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailboxRemoveWindow(mailboxWindow,serverGroupID,selectedItem.data.ID);
				} else {
					mailboxWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mailbox selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailboxWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/Mailboxes/Recreate")) {
		buttons.push({
			text:'Recreate',
			tooltip:'Recreate mailbox',
			iconCls:'silk-arrow_rotate_clockwise',
			handler: function() {
				var selectedItem = Ext.getCmp(mailboxWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailboxRecreateWindow(mailboxWindow,serverGroupID,selectedItem.data.ID);
				} else {
					mailboxWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mailbox selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailboxWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/DeliveryLogs/Mailboxes/List")) {
		buttons.push({
			text:'Logs',
			tooltip:'Mailbox delivery logs',
			iconCls: 'silk-page_white_text',
			handler: function() {
				var selectedItem = Ext.getCmp(mailboxWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailboxDeliveryLogsWindow(serverGroupID,selectedItem.data.ID);
				} else {
					mailboxWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mailbox selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailboxWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/DeliveryRules/Mailboxes/List")) {
		buttons.push({
			text:'Rules',
			tooltip:'Mailbox delivery rules',
			iconCls:'silk-lock',
			handler: function() {
				var selectedItem = Ext.getCmp(mailboxWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailboxDeliveryRulesWindow(serverGroupID,selectedItem.data.ID);
				} else {
					mailboxWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mailbox selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailboxWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}

	// Check which buttons the client can see
	var mailboxWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Mail Mailboxes - "+serverGroupName+" - "+domainName,
			iconCls: 'silk-email',
			
			width: 500,
			height: 335,
		
			minWidth: 500,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Address",
					sortable: true,
					dataIndex: 'Address'
				},
				{
					header: "Name",
					sortable: true,
					dataIndex: 'Name'
				},
				{
					header: "Quota",
					sortable: true,
					dataIndex: 'Quota'
				},
				{
					header: "Agent Ref",
					sortable: true,
					dataIndex: 'AgentRef'
				},
				{
					header: "Status",
					renderer: renderNiceBooleanColumn,
					align: 'center',
					sortable: false,
					dataIndex: 'Disabled'
				}
			]),
			autoExpandColumn: 'Address'
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				TransportID: transportID,
				SOAPModule: 'MailHosting',
				SOAPFunction: 'getMailboxes',
				SOAPParams: 'ServerGroupID,TransportID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Address'},
				{type: 'string',  dataIndex: 'Name'},
				{type: 'numeric',  dataIndex: 'Quota'},
				{type: 'string',  dataIndex: 'AgentRef'}
			]
		}
	);

	mailboxWindow.show();
}


// Display edit/add form
function showMailboxEditWindow(mailboxWindow,serverGroupID,serverGroupName,domainName,transportID,mailboxID,mailboxAddress) {

	var submitAjaxConfig;
	var editMode;
	var icon;
	var title;


	// We doing an update
	if (mailboxID) {
		icon = "silk-email_edit";
		title = "Mailboxes - "+serverGroupName+" - "+domainName+" - "+mailboxAddress;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				ID: mailboxID,
				SOAPFunction: 'updateMailbox',
				SOAPParams: 
					'ServerGroupID,'+
					'1:ID,'+
					'1:Name,'+
					'1:Password,'+
					'1:Quota,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(mailboxWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
		editMode = true;

	// We doing an Add
	} else {
		icon = "silk-email_add";
		title = "Mailboxes - "+serverGroupName+" - "+domainName;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				TransportID: transportID,
				SOAPFunction: 'createMailbox',
				SOAPParams: 
					'ServerGroupID,'+
					'1:TransportID,'+
					'1:Address,'+
					'1:Name,'+
					'1:Password,'+
					'1:Quota,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(mailboxWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
		editMode = false;
	}

	// Form panel ID
	var formPanelID = Ext.id();

	// Create window
	var mailboxFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 475,
			height: 410,

			minWidth: 475,
			minHeight: 410
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'MailHosting'
			},
			items: [
				{
					fieldLabel: 'Address',
					name: 'Address',
					vtype: 'emailLocalPart',
					maskRe: emailLocalPartRe,
					allowBlank: false,
					
					disabled: editMode
				},
				{
					fieldLabel: 'Name',
					name: 'Name'
				},
				{
					id: 'password_field',
					fieldLabel: 'Password',
					name: 'Password',
					width: 100
				},
				{
					id: 'quotabox',
					fieldLabel: 'Quota (Mbyte)',
					name: 'Quota',
					width: 50,
					vtype: 'number',
					allowBlank: false
				},
				{
					fieldLabel: 'Agent Ref',
					name: 'AgentRef'
				},
				getStandardServicePanel(formPanelID)
			],
			extrabuttons: [ 
				{
					text: 'MkPass',
					handler: function() {
						var panel = this.ownerCt;
						var win = panel.ownerCt;
						var passfield = win.findById('password_field');
						passfield.setValue(getRandomPass(8));
					}
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	mailboxFormWindow.show();

	if (mailboxID) {
		Ext.getCmp(mailboxFormWindow.formPanelID).load({
			params: {
				ServerGroupID: serverGroupID,
				ID: mailboxID,
				SOAPModule: 'MailHosting',
				SOAPFunction: 'getMailbox',
				SOAPParams: 'ServerGroupID,ID'
			}
		});
	}

	initStandardServicePanel(formPanelID);
}


// Display recreate form
function showMailboxRecreateWindow(mailboxWindow,serverGroupID,mailboxID) {
	// Mask mailboxWindow window
	mailboxWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm recreate",
		msg: "Are you very sure you wish to recreate this mailbox?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(mailboxWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: mailboxID,
						SOAPModule: 'MailHosting',
						SOAPFunction: 'recreateMailbox',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(mailboxWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				mailboxWindow.getEl().unmask();
			}
		}
	});
}


// Display edit/add form
function showMailboxRemoveWindow(mailboxWindow,serverGroupID,mailboxID) {
	// Mask mailboxWindow window
	mailboxWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this mailbox?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(mailboxWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: mailboxID,
						SOAPModule: 'MailHosting',
						SOAPFunction: 'removeMailbox',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(mailboxWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				mailboxWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
