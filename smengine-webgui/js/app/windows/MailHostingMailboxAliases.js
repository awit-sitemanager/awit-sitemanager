/*
Mailbox Aliases
Copyright (C) 2007-2010, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/




function showMailboxAliasesWindow(serverGroupID,serverGroupName,transportID,domainName) {
	
	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("MailHosting/MailboxAliases/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add mailbox alias',
			iconCls:'silk-link_add',
			handler: function() {
				showMailboxAliasEditWindow(mailboxAliasWindow,serverGroupID,serverGroupName,domainName,transportID);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/MailboxAliases/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit mailbox alias',
			iconCls:'silk-link_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(mailboxAliasWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailboxAliasEditWindow(mailboxAliasWindow,serverGroupID,serverGroupName,domainName,transportID,selectedItem.data.ID,selectedItem.data.Address);
				} else {
					mailboxAliasWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mailbox alias selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailboxAliasWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/MailboxAliases/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove mailbox alias',
			iconCls:'silk-link_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(mailboxAliasWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showMailboxAliasRemoveWindow(mailboxAliasWindow,serverGroupID,selectedItem.data.ID);
				} else {
					mailboxAliasWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No mailbox alias selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							mailboxAliasWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	var mailboxAliasWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Mailbox Aliases - "+serverGroupName+" - "+domainName,
			iconCls: 'silk-link',
			
			width: 500,
			height: 335,
			minWidth: 500,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Address",
					sortable: true,
					dataIndex: 'Address'
				},
				{
					header: "Goto",
					sortable: true,
					dataIndex: 'Goto'
				},
				{
					header: "Agent Ref",
					sortable: true,
					dataIndex: 'AgentRef'
				},
				{
					header: "Status",
					renderer: renderNiceBooleanColumn,
					align: 'center',
					sortable: false,
					dataIndex: 'Disabled'
				}
			]),
			autoExpandColumn: 'Goto'
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				TransportID: transportID,
				SOAPModule: 'MailHosting',
				SOAPFunction: 'getMailboxAliases',
				SOAPParams: 'ServerGroupID,TransportID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Address'},
				{type: 'string',  dataIndex: 'Goto'},
				{type: 'string',  dataIndex: 'AgentRef'}
			]
		}
	);

	mailboxAliasWindow.show();
}


// Display edit/add form
function showMailboxAliasEditWindow(mailboxAliasWindow,serverGroupID,serverGroupName,domainName,transportID,aliasID,mailboxAddress) {

	var submitAjaxConfig;
	var editMode;
	var icon;
	var title;


	// We doing an update
	if (aliasID) {
		icon = 'silk-link_edit';
		title = "Alias - "+serverGroupName+" - "+domainName+" - "+mailboxAddress;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				ID: aliasID,
				SOAPFunction: 'updateMailboxAlias',
				SOAPParams: 
					'ServerGroupID,'+
					'1:ID,'+
					'1:Goto,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(mailboxAliasWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
		editMode = true;

	// We doing an Add
	} else {
		icon = 'silk-link_add';
		title = "Alias - "+serverGroupName+" - "+domainName;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				TransportID: transportID,
				SOAPFunction: 'createMailboxAlias',
				SOAPParams: 
					'ServerGroupID,'+
					'1:TransportID,'+
					'1:Address,'+
					'1:Goto,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(mailboxAliasWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
		editMode = false;
	}

	// Form panel ID
	var formPanelID = Ext.id();

	// Create window
	var mailboxAliasFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 475,
			height: 355,
			minWidth: 475,
			minHeight: 355
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'MailHosting'
			},
			items: [
				{
					fieldLabel: 'Address',
					name: 'Address',
					vtype: 'emailLocalPart',
					maskRe: emailLocalPartRe,
					allowBlank: false,
					
					disabled: editMode
				},
				{
					fieldLabel: 'Goto',
					name: 'Goto',
					vtype: 'emailAddress',
					maskRe: emailAddressRe,
					allowBlank: false
				},
				{
					fieldLabel: 'Agent Ref',
					name: 'AgentRef'
				},
				getStandardServicePanel(formPanelID)
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	mailboxAliasFormWindow.show();

	if (aliasID) {
		Ext.getCmp(mailboxAliasFormWindow.formPanelID).load({
			params: {
				ServerGroupID: serverGroupID,
				ID: aliasID,
				SOAPModule: 'MailHosting',
				SOAPFunction: 'getMailboxAlias',
				SOAPParams: 'ServerGroupID,ID'
			}
		});
	}

	initStandardServicePanel(formPanelID);
}




// Display remove form
function showMailboxAliasRemoveWindow(mailboxAliasWindow,serverGroupID,aliasID) {
	// Mask mailboxAliasWindow window
	mailboxAliasWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this alias?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(mailboxAliasWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: aliasID,
						SOAPModule: 'MailHosting',
						SOAPFunction: 'removeMailboxAlias',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(mailboxAliasWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				mailboxAliasWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
