/*
Group Selector
Copyright (C) 2007-2010, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/



function showServerGroupSelectWindow(calledFunction) {

	var customPanelID = Ext.id();
	var windowID = Ext.id();

	// Server group store
	var serverGroupStore;
	if (/dnszones/i.test(calledFunction)) {
		serverGroupStore = new Ext.ux.JsonStore({
				sortInfo: { field: "Description", direction: "ASC" },
				baseParams: {
					SOAPModule: 'DNSHosting',
					SOAPFunction: 'getDNSHostingServerGroups',
					SOAPParams: '0:__search'
				}
		});
	} else if (/ddnsusers/i.test(calledFunction)) {
		serverGroupStore = new Ext.ux.JsonStore({
				sortInfo: { field: "Description", direction: "ASC" },
				baseParams: {
					SOAPModule: 'DNSHosting/DDNS',
					SOAPFunction: 'getDDNSServerGroups',
					SOAPParams: '0:__search'
				}
		});
	} else if (/radiususer/i.test(calledFunction)) {
		serverGroupStore = new Ext.ux.JsonStore({
				sortInfo: { field: "Description", direction: "ASC" },
				baseParams: {
					SOAPModule: 'Radius',
					SOAPFunction: 'getRadiusServerGroups',
					SOAPParams: '0:__search'
				}
		});
	} else if (/mailtransport/i.test(calledFunction)) {
		serverGroupStore = new Ext.ux.JsonStore({
				sortInfo: { field: "Description", direction: "ASC" },
				baseParams: {
					SOAPModule: 'MailHosting',
					SOAPFunction: 'getMailHostingServerGroups',
					SOAPParams: '0:__search'
				}
		});
	}

	// Combo box ID
	var serverGroupComboBoxID = Ext.id();

	// Server group form panel
	var customFormPanel = new Ext.FormPanel({
			xtype: 'progressformpanel',
			id: customPanelID,


			// Space stuff a bit so it looks better
			bodyStyle: 'padding: 5px',

			// Default form item is text field
			defaultType: 'textfield',

			// Button uses formBind = true, this is undocumented
			// we may need to define an event to call  'clientvalidation'
			monitorValid: true,
			
			// Buttons for the form
			buttons: [
				{
					text: 'Submit',
					formBind: true,
					handler: function() {
						var panel = Ext.getCmp(customPanelID);
						var win = Ext.getCmp(windowID);

						// Get combobox value
						var serverGroup = Ext.getCmp(serverGroupComboBoxID);
						var serverGroupID = serverGroup.getValue();
						var serverGroupName = serverGroup.getRawValue();

						window[calledFunction](serverGroupID,serverGroupName);
						win.close();
					}
				},
				{
					text: 'Cancel',
					handler: function() {
						var win = Ext.getCmp(windowID);
						win.close();
					}
				}
			],
			items: [
				{
					xtype: 'combo',
					id: serverGroupComboBoxID,
					fieldLabel: 'Cluster',
					name: 'Cluster',
					allowBlank: false,

					store: serverGroupStore,
					displayField: 'Description',
					valueField: 'ID',
					//hiddenName: 'ServerGroupID',
					forceSelection: false,
					triggerAction: 'all',
					editable: false
				}
			],
			// Align buttons
			buttonAlign: 'center'
	});

	// Create window
	var serverGroupSelectWindow = new Ext.Window({

		// Window config
		title: 'Server Group',
		iconCls: 'silk-server',

		id: windowID,
		width: 420,
		height: 120,
		minWidth: 420,
		minHeight: 120,
		layout: 'fit',

		items: customFormPanel
	});

	serverGroupSelectWindow.show();
}


// vim: ts=4
