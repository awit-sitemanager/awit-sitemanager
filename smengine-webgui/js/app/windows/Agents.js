/*
Agents
Copyright (C) 2007-2009, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAgentsWindow() {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Agent/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add agent',
			iconCls:'silk-group_add',
			handler: function() {
				showAgentEditWindow(agentsWindow);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Agent/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit agent',
			iconCls:'silk-group_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(agentsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAgentEditWindow(agentsWindow,selectedItem.data.ID);
				} else {
					agentsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No agent selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							agentsWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Agent/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove agent',
			iconCls:'silk-group_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(agentsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAgentRemoveWindow(agentsWindow,selectedItem.data.ID);
				} else {
					agentsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No agent selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							agentsWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var agentsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Agents",
			iconCls: "silk-group",
			
			width: 400,
			height: 335,
		
			minWidth: 400,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars (buttons)
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Name",
					sortable: true,
					dataIndex: 'Name'
				}
			]),
			autoExpandColumn: 'Name'
		},
		// Store config
		{
			baseParams: {
				SOAPModule: 'Agent',
				SOAPFunction: 'getAgents',
				SOAPParams: '__null,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Name'}
			]
		}
	);

	agentsWindow.show();
}


// Display edit/add form
function showAgentEditWindow(agentsWindow,agentID) {

	var submitAjaxConfig;
	var icon;

	// We doing an update
	if (agentID) {
		icon = 'silk-group_edit';
		submitAjaxConfig = {
			params: {
				ID: agentID,
				SOAPFunction: 'updateAgent',
				SOAPParams: 
					'0:ID,0:Name,'+
					'0:ContactPerson,0:ContactTel1,0:ContactTel2,0:ContactEmail,'+
					'0:SiteQuota,0:MinSiteSize,'+
					'0:MailboxQuota,0:MinMailboxSize'
			},
			onSuccess: function() {
				var store = Ext.getCmp(agentsWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	// We doing an Add
	} else {
		icon = 'silk-group_add';
		submitAjaxConfig = {
			params: {
				SOAPFunction: 'createAgent',
				SOAPParams: 
					'0:Name,'+
					'0:ContactPerson,0:ContactTel1,0:ContactTel2,0:ContactEmail,'+
					'0:SiteQuota,0:MinSiteSize,'+
					'0:MailboxQuota,0:MinMailboxSize'
			},
			onSuccess: function() {
				var store = Ext.getCmp(agentsWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}

	// Create window
	var agentFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Agent Information",
			iconCls: icon,

			width: 320,
			height: 340,

			minWidth: 320,
			minHeight: 340
		},
		// Form panel config
		{
			labelWidth: 95,
			baseParams: {
				SOAPModule: 'Agent'
			},
			items: [
				{
					fieldLabel: 'Name',
					name: 'Name',
					allowBlank: false
				},
				{
					fieldLabel: 'Contact Person',
					name: 'ContactPerson',
					allowBlank: false
				},
				{
					fieldLabel: 'Contact Tel 1',
					name: 'ContactTel1',
					allowBlank: false
				},
				{
					fieldLabel: 'Contact Tel 2',
					name: 'ContactTel2'
				},
				{
					fieldLabel: 'Contact Email',
					name: 'ContactEmail',
					vtype: 'email',
					allowBlank: false
				},
	
				{
					xtype: 'tabpanel',
					plain: 'true',
					deferredRender: false, // Load all panels!
					activeTab: 0,
					height: 100,
					defaults: {
						layout: 'form',
						bodyStyle: 'padding: 10px;'
					},
					
					items: [
						{
							title: 'Web Hosting',
							iconCls: 'silk-page_world',
							layout: 'form',
							defaultType: 'textfield',
							items: [
								{
									fieldLabel: 'Site Quota',
									name: 'SiteQuota',
									allowBlank: false,
									vtype: 'number',
									value: '0'
								},
								{
									fieldLabel: 'Min Site Size',
									name: 'MinSiteSize',
									allowBlank: false,
									vtype: 'number',
									value: '5'
								}
							]
						},
						{
							title: 'Mail Hosting',
							iconCls: 'silk-email',
							layout: 'form',
							defaultType: 'textfield',
							labelWidth: 110,
							fieldWidth: 70,
							items: [
								{
									fieldLabel: 'Mail Quota',
									name: 'MailboxQuota',
									allowBlank: false,
									vtype: 'number',
									value: '0'
								},
								{
									fieldLabel: 'Min Mailbox Size',
									name: 'MinMailboxSize',
									allowBlank: false,
									vtype: 'number',
									value: '1'
								}
							]
						}
					]
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	agentFormWindow.show();

	if (agentID) {
		Ext.getCmp(agentFormWindow.formPanelID).load({
			params: {
				ID: agentID,
				SOAPModule: 'Agent',
				SOAPFunction: 'getAgent',
				SOAPParams: 'ID'
			}
		});
	}
}




// Display edit/add form
function showAgentRemoveWindow(agentsWindow,agentID) {
	// Mask agentsWindow window
	agentsWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this agent?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(agentsWindow,{
					params: {
						ID: agentID,
						SOAPModule: 'Agent',
						SOAPFunction: 'removeAgent',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(agentsWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				agentsWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
