

function showDatabaseUsersWindow(databaseID) {

	var databaseUserWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Database Users",
			
			width: 400,
			height: 335,
		
			minWidth: 400,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: [
				{
					text:'Add',
					tooltip:'Add database user',
					iconCls:'add',
					handler: function() {
						showDatabaseUserEditWindow(databaseID,0);
					}
				}, 
				'-', 
				{
					text:'Edit',
					tooltip:'Edit database user',
					iconCls:'option',
					handler: function() {
						var selectedItem = Ext.getCmp(databaseUserWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showDatabaseUserEditWindow(databaseID,selectedItem.data.ID);
						} else {
							databaseUserWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No database user selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									databaseUserWindow.getEl().unmask();
								}
							});
						}
					}
				},
				'-',
				{
					text:'Remove',
					tooltip:'Remove database user',
					iconCls:'remove',
					handler: function() {
						var selectedItem = Ext.getCmp(databaseUserWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showDatabaseUserRemoveWindow(databaseUserWindow,selectedItem.data.ID);
						} else {
							databaseUserWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No database user selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									databaseUserWindow.getEl().unmask();
								}
							});
						}
					}
				}
			],
			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{	
					header: "Username",
					sortable: true,
					dataIndex: 'Username'
				},
			]),
			autoExpandColumn: 'Name'
		},
		// Store config
		{
			baseParams: {
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting/Database',
				SOAPFunction: 'getDatabaseUsers',
				SOAPParams: 'DatabaseID,__search',
				DatabaseID: databaseID
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Username'},
			]
		}
	);

	databaseUserWindow.show();
}


// Display edit/add form
function showDatabaseUserEditWindow(databaseID,id) {

	var submitAjaxConfig;
	var editMode;

	// We doing an update
	if (id) {
		submitAjaxConfig = {
			ID: id,
			SOAPFunction: 'updateDatabaseUser',
			SOAPParams: 
				'0:ID,0:Username'
		};
		editMode = true;
	// We doing an Add
	} else {
		submitAjaxConfig = {
			SOAPFunction: 'createDatabaseUser',
			DatabaseID: databaseID,
			SOAPParams: 
				'0:DatabaseID,0:Username'
		};
		editMode = false;
	}

	// Create window
	var databaseUserFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Database User Information",

			width: 320,
			height: 340,

			minWidth: 320,
			minHeight: 340
		},
		// Form panel config
		{
			labelWidth: 95,
			baseParams: {
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting/Database'
			},
			items: [
				{
					fieldLabel: 'Username',
					name: 'Username',
					vtype: 'username',
					maskRe: usernameRe,
					allowBlank: false
					//disabled: editMode
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	databaseUserFormWindow.show();

	if (id) {
		Ext.getCmp(databaseUserFormWindow.formPanelID).load({
			params: {
				id: id,
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting/Database',
				SOAPFunction: 'getDatabaseUser',
				SOAPParams: 'id'
			}
		});
	}
}




// Display edit/add form
function showDatabaseUserRemoveWindow(parent,id) {
	// Mask parent window
	parent.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this database user?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(parent,{
					params: {
						id: id,
						SOAPUsername: globalConfig.soap.username,
						SOAPPassword: globalConfig.soap.password,
						SOAPAuthType: globalConfig.soap.authtype,
						SOAPModule: 'WebHosting/Database',
						SOAPFunction: 'removeDatabaseUser',
						SOAPParams: 'id'
					}
				});


			// Unmask if user answered no
			} else {
				parent.getEl().unmask();
			}
		}
	});
}

