/*
Radius Users
Copyright (C) 2007-2010, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/




function showRadiusUserWindow(serverGroupID,serverGroupName) {

	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Radius/Users/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add radius user',
			iconCls:'silk-user_add',
			handler: function() {
				showRadiusUserEditWindow(radiusUsersWindow,serverGroupID,serverGroupName);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Radius/Users/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit radius user',
			iconCls:'silk-user_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(radiusUsersWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showRadiusUserEditWindow(radiusUsersWindow,serverGroupID,serverGroupName,selectedItem.data.ID,selectedItem.data.Username);
				} else {
					radiusUsersWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No radius user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							radiusUsersWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Radius/Users/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove radius user',
			iconCls:'silk-user_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(radiusUsersWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showRadiusUserRemoveWindow(radiusUsersWindow,serverGroupID,selectedItem.data.ID);
				} else {
					radiusUsersWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No radius user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							radiusUsersWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Radius/Users/Logs/List")) {
		buttons.push({ 
			text:'Logs',
			tooltip:'Radius user logs',
			iconCls: 'silk-page_white_text',
			handler: function() {
				var selectedItem = Ext.getCmp(radiusUsersWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showRadiusUserLogsWindow(serverGroupID,serverGroupName,selectedItem.data.Username,selectedItem.data.ID);
				} else {
					radiusUsersWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No radius user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							radiusUsersWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Radius/Users/Topups/List")) {
		buttons.push({ 
			text:'Topups',
			tooltip:'Radius user topups',
			iconCls:'silk-chart_bar',
			handler: function() {
				var selectedItem = Ext.getCmp(radiusUsersWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showRadiusUserTopupsWindow(serverGroupID,serverGroupName,selectedItem.data.Username,selectedItem.data.ID);
				} else {
					radiusUsersWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No radius user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							radiusUsersWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Radius/Users/PortLocks/List")) {
		buttons.push({ 
			text:'Port Locks',
			tooltip:'Radius user port locking',
			iconCls:'silk-lock',
			handler: function() {
				var selectedItem = Ext.getCmp(radiusUsersWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showRadiusUserPortLocksWindow(serverGroupID,serverGroupName,selectedItem.data.Username,selectedItem.data.ID);
				} else {
					radiusUsersWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No radius user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							radiusUsersWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var radiusUsersWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Radius Users - "+serverGroupName,
			iconCls: 'silk-group',
			
			width: 900,
			height: 335,
		
			minWidth: 900,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					header: "ID",
					width: 10,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "AgentName",
					width: 60,
					sortable: true,
					dataIndex: 'AgentName'
				},
				{
					header: "Username",
					width: 70,
					sortable: true,
					dataIndex: 'Username'
				},
				{
					header: "Service",
					sortable: true,
					dataIndex: 'Service'
				},
				{
					header: "Usage Cap",
					width: 45,
					sortable: true,
					dataIndex: 'UsageCap'
				},
				{
					header: "Agent Ref",
					width: 60,
					sortable: true,
					dataIndex: 'AgentRef'
				},
				{
					header: "Status",
					renderer: renderNiceBooleanColumn,
					width: 45,
					align: 'center',
					sortable: false,
					dataIndex: 'Disabled'
				}
			]),
			autoExpandColumn: 'Service'
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				SOAPModule: 'Radius',
				SOAPFunction: 'getRadiusUsers',
				SOAPParams: 'ServerGroupID,__null,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'AgentName'},
				{type: 'string',  dataIndex: 'Username'},
				{type: 'string',  dataIndex: 'Service'},
				{type: 'numeric',  dataIndex: 'UsageCap'},
				{type: 'string',  dataIndex: 'AgentRef'}
			]
		}
	);

	radiusUsersWindow.show();
}


// Display edit/add form
function showRadiusUserEditWindow(radiusUsersWindow,serverGroupID,serverGroupName,userID,username) {

	var submitAjaxConfig;
	var editMode;
	var icon;
	var title;

	// Combo box ID
	var agentComboBoxID = Ext.id();
	var serviceComboBoxID = Ext.id();


	// We doing an update
	if (userID) {
		icon = "silk-user_edit";
		title = "Radius User - "+serverGroupName+" - "+username;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				ID: userID,
				SOAPFunction: 'updateRadiusUser',
				SOAPParams: 
					'ServerGroupID,'+
					'1:ID,'+
					'1:Password,'+
					'1:UsageCap,'+
					'1:NotifyMethod,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(radiusUsersWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
		editMode = true;

	// We doing an Add
	} else {
		icon = "silk-user_add";
		title = "Radius User - "+serverGroupName;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				SOAPFunction: 'createRadiusUser',
				SOAPParams: 
					'ServerGroupID,'+
					'1:AgentID,'+
					'1:ClassID,'+
					'1:Username,'+
					'1:Password,'+
					'1:UsageCap,'+
					'1:NotifyMethod,'+
					'1:AgentRef,'+
					'1:AgentDisabled:boolean'
			},
			onSuccess: function() {
				var store = Ext.getCmp(radiusUsersWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
		editMode = false;
	}
	
	// Agent store
	var agentStore = new Ext.ux.JsonStore({
			sortInfo: { field: "Name", direction: "ASC" },
			baseParams: {
				SOAPModule: 'Agent',
				SOAPFunction: 'getAgents',
				SOAPParams: '__search'
			}
	});

	// Service store
	var serviceStore = new Ext.ux.JsonStore({
//			sortInfo: { field: "Description", direction: "ASC" },
			baseParams: {
				ServerGroupID: serverGroupID,
				SOAPModule: 'Radius',
				SOAPFunction: 'getRadiusClasses',
				SOAPParams: 'ServerGroupID,1:AgentID,__search'
			}
	});

	// Form panel ID
	var formPanelID = Ext.id();

	// Password ID
	var passwordID = Ext.id();

	// Tabpanel IDs
	var autoTopupTextFieldID = Ext.id();
	var autoTopupComboBoxID = Ext.id();

	// Create window
	var radiusUserFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 475,
			height: 450,

			minWidth: 475,
			minHeight: 450
		},
		// Form panel config
		{
			formPanelID: formPanelID,
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Radius'
			},
			items: [
				{
					xtype: 'combo',
					id: agentComboBoxID,
					fieldLabel: 'Agent',
					name: 'Agent',
					allowBlank: false,
					width: 160,

					store: agentStore,
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'AgentID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false,
					disabled: editMode
				},

				{
					xtype: 'combo',
					id: serviceComboBoxID,
					fieldLabel: 'Service',
					name: 'Service',
					allowBlank: false,
					width: 340,

					store: serviceStore,
					displayField: 'Service',
					valueField: 'ID',
					hiddenName: 'ClassID',
					forceSelection: false,
					triggerAction: 'all',
					editable: false,
					disabled: editMode
				},

				{
					fieldLabel: 'Username',
					name: 'Username',
					vtype: 'usernamePart',
					maskRe: usernamePartRe,
					allowBlank: false,
					
					disabled: editMode
				},

				{
					id: passwordID,
					fieldLabel: 'Password',
					name: 'Password',
					allowBlank: true
				},

				{
					fieldLabel: 'Usage Cap',
					name: 'UsageCap'
				},

				{
					fieldLabel: 'Agent Ref',
					name: 'AgentRef',
					width: 320
				},
				getStandardServicePanel(
					formPanelID,
					[
						{
							title: 'Notifications',

							items: [
								{
									xtype: 'fieldset',
									title: 'General',
									autoHeight: true,

									items: [
										{
											layout: 'form',
											defaultType: 'textfield',
											border: false,
											height: 30,

											items: [
												{
													fieldLabel: 'Notify Method',
													name: 'NotifyMethod',
													width: 310
												}
											]
										}
									]
								}
							]
						},
						{
							title: 'Auto Topups',

							items: [
								{
									xtype: 'fieldset',
									title: 'Settings',
									autoHeight: true,

									items: [
										{
											layout: 'absolute',
											border: false,
											defaultType: 'label',
											height: 30,

											items: [
												{
													text: 'Auto Topup',
													x: 10,
													y: 0
												},
												{
													xtype: 'checkbox',
													name: 'AgentAutoTopup',
													disabled: true,
													handler: function (checkbox, checked) {
														var textField = Ext.getCmp(autoTopupTextFieldID);
														var comboBox = Ext.getCmp(autoTopupComboBoxID);

														if (checked) {
															textField.enable();
															comboBox.enable();
														} else {
															textField.disable();
															comboBox.disable();
														}
													},
													x: 90,
													y: 3
												},
												{
													text: 'when account reaches',
													x: 120,
													y: 0
												},
												{
													id: autoTopupTextFieldID,
													xtype: 'textfield',
													disabled: true,
													width: 40,
													x: 240,
													y: 0
												},
												{
													id: autoTopupComboBoxID,
													hiddenName: 'xyz',
													xtype: 'combo',
													allowBlank: false,
													disabled: true,
													x: 310,
													y: 0,
													width: 60,
													mode: 'local',
								    					store: new Ext.data.ArrayStore({
														idIndex: 0,
														fields: [
															'id', 'value', 'display'
														],
														data: [
															[1, 'Percent', 'Percent'],
															[2, 'Usage', 'Mbyte']
														]
													}),
													displayField: 'display',
													valueField: 'value',
													hiddenName: 'xyz',
													forceSelection: false,
													triggerAction: 'all',
													editable: false
												}
											]
										}
									]
								}
							]
						}
					]
				)
			],
			extrabuttons: [ 
				{
					text: 'MkPass',
					handler: function() {
						var passfield = Ext.getCmp(passwordID);
						passfield.setValue(getRandomPass(8));
					}
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	// Events
	if (!userID) {
		var agentComboBox = Ext.getCmp(agentComboBoxID);
		var serviceComboBox = Ext.getCmp(serviceComboBoxID);

		agentComboBox.on({
			select: {
				fn: function() {
					var myValue = this.getValue();
					if (myValue) {
						serviceComboBox.reset();
						serviceStore.baseParams.AgentID = myValue;
						serviceStore.reload();
						serviceComboBox.enable();
					} else {
						serviceComboBox.reset();
						serviceComboBox.disable();
					}
				}
			}
		});
	}
	radiusUserFormWindow.show();

	if (userID) {
		// Load the stores to resolve the ID to text
		agentStore.load();
		serviceStore.load();

		// Main form load
		radiusUserFormWindow.getFormPanel().load({
			params: {
				ServerGroupID: serverGroupID,
				ID: userID,
				SOAPModule: 'Radius',
				SOAPFunction: 'getRadiusUser',
				SOAPParams: 'ServerGroupID,ID'
			}
		});
	}

	initStandardServicePanel(formPanelID);
}




// Display remove form
function showRadiusUserRemoveWindow(radiusUsersWindow,serverGroupID,userID) {
	// Mask radiusUsersWindow window
	radiusUsersWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this radius user?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(radiusUsersWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: userID,
						SOAPModule: 'Radius',
						SOAPFunction: 'removeRadiusUser',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(radiusUsersWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				radiusUsersWindow.getEl().unmask();
			}
		}
	});
}




// vim: ts=4
