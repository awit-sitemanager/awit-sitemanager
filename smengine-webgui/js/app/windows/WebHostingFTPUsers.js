

function showWebHostingFTPUsersWindow(websiteID) {

	var webHostingFTPUserWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "WebHostingFTPUser",
			
			width: 400,
			height: 335,
		
			minWidth: 400,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: [
				{
					text:'Add',
					tooltip:'Add FTP User',
					iconCls:'add',
					handler: function() {
						showWebHostingFTPUserEditWindow(websiteID,0);
					}
				}, 
				'-', 
				{
					text:'Edit',
					tooltip:'Edit FTP User',
					iconCls:'option',
					handler: function() {
						var selectedItem = Ext.getCmp(webHostingFTPUserWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showWebHostingFTPUserEditWindow(websiteID,selectedItem.data.ID);
						} else {
							webHostingFTPUserWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No FTP User selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									webHostingFTPUserWindow.getEl().unmask();
								}
							});
						}
					}
				},
				'-',
				{
					text:'Remove',
					tooltip:'Remove FTP User',
					iconCls:'remove',
					handler: function() {
						var selectedItem = Ext.getCmp(webHostingFTPUserWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showWebHostingFTPUserRemoveWindow(webHostingFTPUserWindow,selectedItem.data.ID);
						} else {
							webHostingFTPUserWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No FTP User selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									webHostingFTPUserWindow.getEl().unmask();
								}
							});
						}
					}
				}
			],
			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Username",
					sortable: true,
					dataIndex: 'Username'
				},
				{
					header: "Password",
					sortable: true,
					dataIndex: 'Password'
				},
				{
					header: "HomeDirectory",
					sortable: true,
					dataIndex: 'HomeDirectory'
				},
				{
					header: "AgentRef",
					sortable: true,
					dataIndex: 'AgentRef'
				},
				{
					header: "AgentDisabled",
					sortable: true,
					dataIndex: 'AgentDisabled'
				}
			])
		},
		// Store config
		{
			baseParams: {
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting/FTP',
				SOAPFunction: 'getFTPUsers',
				SOAPParams: 'WebsiteID,__search',
				WebsiteID: websiteID
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Username'},
				{type: 'string',  dataIndex: 'Password'},
			//	{type: 'string',  dataIndex: 'HomeDirectory'},
				{type: 'string',  dataIndex: 'Agentref'}
			]
		}
	);

	webHostingFTPUserWindow.show();
}


// Display edit/add form
function showWebHostingFTPUserEditWindow(websiteID,id) {

	var submitAjaxConfig;
	var editMode;

	// We doing an update
	if (id) {
		submitAjaxConfig = {
			ID: id,
			SOAPFunction: 'updateFTPUser',
			SOAPParams: 
				'0:ID,'+
				'0:Username,'+
				'0:Password,0:HomeDirectory,0:AgentRef,'+
				'0:AgentDisabled:boolean'
		};
		editMode = true;
	// We doing an Add
	} else {
		submitAjaxConfig = {
			SOAPFunction: 'createFTPUser',
			WebsiteID: websiteID,
			SOAPParams: 
				'0:WebsiteID,'+
				'0:Username,'+
				'0:Password,0:HomeDirectory,0:AgentRef,'+
				'0:AgentDisabled:boolean'
		};
		editMode = false;
	}

	// Create window
	var webHostingFTPUserFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "FTP User Information",

			width: 320,
			height: 340,

			minWidth: 320,
			minHeight: 340
		},
		// Form panel config
		{
			labelWidth: 95,
			baseParams: {
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting/FTP'
			},
			items: [
				{
					fieldLabel: 'UserName',
					name: 'Username',
					vtype: 'username',
					maskRe: usernameRe,
					allowBlank: false
				},
				{
					id: 'password_field',
					fieldLabel: 'Password',
					name: 'Password',
					width: 100
				},
				{
					fieldLabel: 'Directory',
					name: 'HomeDirectory',
					editable: false,
					disabled: editMode,
					allowBlank: false
				},
				{
					fieldLabel: 'AgentRef',
					name: 'AgentRef'
				},
				{
					xtype: 'checkbox',
					fieldLabel: 'AgentDisabled',
					name: 'AgentDisabled'
				}
	
			],
			extrabuttons: [ 
				{
					text: 'MkPass',
					handler: function() {
						var panel = this.ownerCt;
						var win = panel.ownerCt;
						var passfield = win.findById('password_field');
						passfield.setValue(getRandomPass(8));
					}
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	webHostingFTPUserFormWindow.show();

	if (id) {
		Ext.getCmp(webHostingFTPUserFormWindow.formPanelID).load({
			params: {
				id: id,
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting/FTP',
				SOAPFunction: 'getFTPUser',
				SOAPParams: 'id'
			}
		});
	}
}

// Display edit/add form
function showWebHostingFTPUserRemoveWindow(parent,id) {
	// Mask parent window
	parent.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this FTP User?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(parent,{
					params: {
						id: id,
						SOAPUsername: globalConfig.soap.username,
						SOAPPassword: globalConfig.soap.password,
						SOAPAuthType: globalConfig.soap.authtype,
						SOAPModule: 'WebHosting/FTP',
						SOAPFunction: 'removeFTPUser',
						SOAPParams: 'id'
					}
				});


			// Unmask if user answered no
			} else {
				parent.getEl().unmask();
			}
		}
	});
}

