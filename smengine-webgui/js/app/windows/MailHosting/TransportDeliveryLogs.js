/*
Transport Delivery Logs
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/



function showTransportDeliveryLogsWindow(serverGroupID,transportID) {

	// Calculate dates we going to need
	var today = new Date();
	var firstOfMonth = today.getFirstDateOfMonth();
	var firstOfNext = today.getLastDateOfMonth().add(Date.DAY, 1);

	// Search form ID
	var searchFormID = Ext.id();

	var transportDeliveryLogsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: 'Transport Logs',
			layout:'border',
			height: 480,
			width: 900,
			minHeight: 480,
			minWidth: 900,
			closable: true,
			plain: true,
			iconCls: 'silk-page_white_text',
			uxItems: [
				{
					xtype: 'form',
					id: searchFormID,
					title: 'Search',
					region: 'center',
					border: true,
					frame: true,
					defaultType: 'datefield',
					height: 180,
					width: 300,
					labelWidth: 100,
					items: [
						{
							id: 'after',
							name: 'after',
							width: 180,
							fieldLabel: 'From',
							vtype: 'daterange',
							format: 'Y-m-d',
							value: firstOfMonth,
							endDateField: 'before'
						},
						{
							id: 'before',
							name: 'before',
							width: 180,
							fieldLabel: 'To',
							vtype: 'daterange',
							format: 'Y-m-d',
							value: firstOfNext,
							startDateField: 'after'
						}
					],
					buttons: [
						{
							text: 'Search',
							id: 'formbtn',
							handler: function() {
								// Pull in window, grid & form
								var grid = Ext.getCmp(transportDeliveryLogsWindow.gridPanelID);
								var form = Ext.getCmp(searchFormID);

								// Grab store
								var store = grid.getStore();

								// Grab timestamp filter
								var gridFilters = grid.filters;
								var timestampFilter = gridFilters.getFilter('Timestamp');

								// Grab	form fields
								var afterField = form.getForm().findField('after');
								var beforeField = form.getForm().findField('before');

								// Set filter values from form
								timestampFilter.setValue({
									after: afterField.getValue(),
									before: beforeField.getValue()
								});

								// Trigger store reload
								store.reload();
							}
						}
					],
					buttonAlign: 'center'
				}
			]
		},
		// Grid config
		{
			region: 'south',
			width: 900,
			border: true,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					hidden: true,
					dataIndex: 'ID'
				},
				{
					header: "Timestamp",
					sortable: true,
					dataIndex: 'Timestamp'
				},
				{
					header: "Cluster",
					sortable: true,
					hidden: true,
					dataIndex: 'Cluster'
				},
				{
					header: "Server",
					sortable: true,
					hidden: true,
					dataIndex: 'Server'
				},
				{
					header: "SendingServerName",
					sortable: true,
					hidden: true,
					dataIndex: 'SendingServerName'
				},
				{
					header: "SendingServerIP",
					sortable: true,
					hidden: true,
					dataIndex: 'SendingServerIP'
				},
				{
					header: "QueueID",
					sortable: true,
					dataIndex: 'QueueID'
				},
				{
					header: "MessageID",
					sortable: true,
					hidden: true,
					dataIndex: 'MessageID'
				},
				{
					header: "EnvelopeFrom",
					sortable: true,
					dataIndex: 'EnvelopeFrom'
				},
				{
					header: "EnvelopeTo",
					sortable: true,
					dataIndex: 'EnvelopeTo'
				},
				{
					header: "Size",
					sortable: true,
					dataIndex: 'Size'
				},
				{
					header: "Destination",
					sortable: true,
					dataIndex: 'Destination'
				},
				{
					header: "Status",
					sortable: true,
					dataIndex: 'Status'
				},
			])
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				TransportID: transportID,
				SOAPModule: 'MailHosting/DeliveryLogs',
				SOAPFunction: 'getTransportDeliveryLogs',
				SOAPParams: 'ServerGroupID,TransportID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{
					type: 'date',
					dataIndex: 'Timestamp',
					value: { after: firstOfMonth,before: firstOfNext }
				},
				{type: 'string', dataIndex: 'Cluster'},
				{type: 'string', dataIndex: 'Server'},
				{type: 'string', dataIndex: 'SendingServerName'},
				{type: 'string', dataIndex: 'SendingServerIP'},
				{type: 'string', dataIndex: 'QueueID'},
				{type: 'string', dataIndex: 'MessageID'},
				{type: 'string', dataIndex: 'EnvelopeFrom'},
				{type: 'string', dataIndex: 'EnvelopeTo'},
				{type: 'string', dataIndex: 'Size'},
				{type: 'string', dataIndex: 'Destination'},
				{type: 'string', dataIndex: 'Status'}
			]
		}
	);

	// Show window
	transportDeliveryLogsWindow.show();
}
