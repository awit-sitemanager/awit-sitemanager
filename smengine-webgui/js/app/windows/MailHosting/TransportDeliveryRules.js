/*
Transport Delivery Rules
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showTransportDeliveryRulesWindow(serverGroupID,transportID) {

	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("MailHosting/DeliveryRules/MailTransports/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add transport delivery rule',
			iconCls:'silk-lock_add',
			handler: function() {
				showTransportDeliveryRuleAddEditWindow(transportDeliveryRulesWindow,serverGroupID,transportID);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/DeliveryRules/MailTransports/Update")) {
		buttons.push({
			text:'Edit',
			tooltip:'Edit transport delivery rule',
			iconCls:'silk-lock_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(transportDeliveryRulesWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showTransportDeliveryRuleAddEditWindow(transportDeliveryRulesWindow,serverGroupID,transportID,selectedItem.data.ID);
				} else {
					transportDeliveryRulesWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No delivery rule selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							transportDeliveryRulesWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("MailHosting/DeliveryRules/MailTransports/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove transport delivery rule',
			iconCls:'silk-lock_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(transportDeliveryRulesWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showTransportDeliveryRuleRemoveWindow(transportDeliveryRulesWindow,serverGroupID,selectedItem.data.ID);
				} else {
					transportDeliveryRulesWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No delivery rule selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							transportDeliveryRulesWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var transportDeliveryRulesWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Transport Delivery Rules",
			iconCls: 'silk-lock',

			width: 600,
			height: 335,
			minWidth: 600,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars (buttons)
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					ID: 'ID',
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "TransportID",
					sortable: true,
					hidden: true,
					dataIndex: 'TransportID'
				},
				{
					header: "ValidFrom",
					sortable: true,
					dataIndex: 'ValidFrom',
					renderer: function (value) {
						return formatUnixTimestamp(value, 'Y-m-d H:i');
					}
				},
				{
					header: "ValidTo",
					sortable: true,
					dataIndex: 'ValidTo',
					renderer: function (value) {
						return formatUnixTimestamp(value, 'Y-m-d H:i');
					}
				},
				{
					header: "SenderSpec",
					sortable: false,
					dataIndex: 'SenderSpec'
				},
				{
					header: "RecipientSpec",
					sortable: false,
					dataIndex: 'RecipientSpec'
				},
				{
					header: "Action",
					sortable: true,
					dataIndex: 'Action'
				}
			]),
			autoExpandColumn: 'SenderSpec'
		},
		// Store config
		{
			baseParams: {
				TransportID: transportID,
				ServerGroupID: serverGroupID,
				SOAPModule: 'MailHosting/DeliveryRules',
				SOAPFunction: 'getTransportDeliveryRules',
				SOAPParams: 'ServerGroupID,TransportID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'TransportID'},
				{type: 'string',  dataIndex: 'ValidFrom'},
				{type: 'string',  dataIndex: 'ValidTo'},
				{type: 'string',  dataIndex: 'SenderSpec'},
				{type: 'string',  dataIndex: 'RecipientSpec'},
				{type: 'string',  dataIndex: 'Action'}
			]
		}
	);
	transportDeliveryRulesWindow.show();
}

// Display edit/add form
function showTransportDeliveryRuleAddEditWindow(transportDeliveryRulesWindow,serverGroupID,transportID,deliveryRuleID) {

	var submitAjaxConfig;
	var icon;
	var editMode;

	// We doing an update
	if (deliveryRuleID) {
		editMode = true;
		icon = 'silk-lock_edit';
		submitAjaxConfig = {
			params: {
				ID: deliveryRuleID,
				ServerGroupID: serverGroupID,
				SOAPFunction: 'updateTransportDeliveryRule',
				SOAPParams:
					'ServerGroupID,'+
					'1:ID,'+
					'1:ValidFrom,'+
					'1:ValidTo,'+
					'1:SenderSpec,'+
					'1:RecipientSpec,'+
					'1:Action,'+
					'1:Detail'
			},
			onSuccess: function() {
				var store = Ext.getCmp(transportDeliveryRulesWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	// We doing an Add
	} else {
		editMode = false;
		icon = 'silk-lock_add';
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				TransportID: transportID,
				SOAPFunction: 'createTransportDeliveryRule',
				SOAPParams:
					'ServerGroupID,'+
					'1:TransportID,'+
					'1:ValidFrom,'+
					'1:ValidTo,'+
					'1:SenderSpec,'+
					'1:RecipientSpec,'+
					'1:Action,'+
					'1:Detail'
			},
			onSuccess: function() {
				var store = Ext.getCmp(transportDeliveryRulesWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}

	// Form panel ID
	var formPanelID = Ext.id();

	// Actions store
	var actionStore = new Ext.ux.JsonStore({
			sortInfo: { field: "Action", direction: "ASC" },
			baseParams: {
				ServerGroupID: serverGroupID,
				SOAPModule: 'MailHosting/DeliveryRules',
				SOAPFunction: 'getActionList',
				SOAPParams: 'ServerGroupID'
			}
	});

	var validFromID = Ext.id();
	var validToID = Ext.id();

	// Create window
	var transportDeliveryRuleFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: 'Transport Delivery Rule',
			iconCls: icon,

			width: 435,
			height: 365,
			minWidth: 435,
			minHeight: 365
		},
		// Form panel config
		{
			formPanelID: formPanelID,
			labelWidth: 100,
			baseParams: {
				SOAPModule: 'MailHosting/DeliveryRules'
			},
			items: [
				{
					id: validFromID,
					xtype: 'textfield',
					fieldLabel: 'ValidFrom',
					name: 'ValidFrom',
					allowBlank: true
				},
				{
					id: validToID,
					xtype: 'textfield',
					fieldLabel: 'ValidTo',
					name: 'ValidTo',
					allowBlank: true
				},
				{
					fieldLabel: 'SenderSpec',
					name: 'SenderSpec',
					allowBlank: true
				},
				{
					fieldLabel: 'RecipientSpec',
					name: 'RecipientSpec',
					allowBlank: true
				},
				{
					xtype: 'combo',
					fieldLabel: 'Action',
					name: 'Action',
					allowBlank: true,
					width: 168,

					store: actionStore,
					displayField: 'Action',
					valueField: 'Action',
					hiddenName: 'Action',
					forceSelection: false,
					triggerAction: 'all',
					editable: editMode
				},
				{
					xtype: 'textarea',
					fieldLabel: 'Detail',
					name: 'Detail',
					allowBlank: true,

					width: 300,
					height: 150
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	transportDeliveryRuleFormWindow.show();

	var formPanel = Ext.getCmp(transportDeliveryRuleFormWindow.formPanelID);
	if (deliveryRuleID) {
		formPanel.load({
			params: {
				ServerGroupID: serverGroupID,
				ID: deliveryRuleID,
				SOAPModule: 'MailHosting/DeliveryRules',
				SOAPFunction: 'getTransportDeliveryRule',
				SOAPParams: 'ServerGroupID,ID'
			},
			success: function() {
				// Get fields
				var validFromField = Ext.getCmp(validFromID);
				var validToField = Ext.getCmp(validToID);

				// Get field values
				var validFromOld = validFromField.getRawValue();
				var validToOld = validToField.getRawValue();

				// Parse times
				var fromDate = formatUnixTimestamp(validFromOld,"Y-m-d H:i");
				var toDate = formatUnixTimestamp(validToOld,"Y-m-d H:i");

				// Finally set formatted values
				validFromField.setValue(fromDate);
				validToField.setValue(toDate);
			}
		});
	}

	// Need to parse dates to unix time before submit
	formPanel.on({
		beforeaction: { scope:this, fn:function(form, action){
			if (action.type == 'submit') {
				// Get fields
				var validFromField = Ext.getCmp(validFromID);
				var validToField = Ext.getCmp(validToID);

				// Get field values
				var fromDate = validFromField.getRawValue();
				var toDate = validToField.getRawValue();

				// Parse dates
				fromDate = form_DateToUnix(fromDate);
				toDate = form_DateToUnix(toDate);

				// Finally set formatted values
				validFromField.setValue(fromDate);
				validToField.setValue(toDate);
			}
		}}
	});
}

// Display remove form
function showTransportDeliveryRuleRemoveWindow(transportDeliveryRulesWindow,serverGroupID,deliveryRuleID) {
	// Mask transportDeliveryRulesWindow window
	transportDeliveryRulesWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this transport delivery rule?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(transportDeliveryRulesWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: deliveryRuleID,
						SOAPModule: 'MailHosting/DeliveryRules',
						SOAPFunction: 'removeTransportDeliveryRule',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(transportDeliveryRulesWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});

			// Unmask if user answered no
			} else {
				transportDeliveryRulesWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
