/*
DDNSUsers
Copyright (C) 2007-2009, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showDDNSUsersWindow(serverGroupID,serverGroupName) {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("DNSHosting/DDNS/Users/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add user',
			iconCls:'silk-group_add',
			handler: function() {
				showDDNSUserWindow(DDNSUserWindow,serverGroupID,serverGroupName);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("DNSHosting/DDNS/Users/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit user',
			iconCls:'silk-group_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(DDNSUserWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showDDNSUserWindow(DDNSUserWindow,serverGroupID,serverGroupName,selectedItem.data.ID,selectedItem.data.Username);
				} else {
					DDNSUserWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							DDNSUserWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("DNSHosting/DDNS/Users/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove user',
			iconCls:'silk-group_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(DDNSUserWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showDDNSUserRemoveWindow(DDNSUserWindow,serverGroupID,selectedItem.data.ID);
				} else {
					DDNSUserWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							DDNSUserWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var DDNSUserWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "DDNS Users - "+serverGroupName,
			iconCls: "silk-group",
			
			width: 400,
			height: 335,
		
			minWidth: 400,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars (buttons)
			tbar: buttons,
			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "AgentID",
					sortable: true,
					hidden: true,
					dataIndex: 'AgentID'
				},
				{
					header: "AgentName",
					sortable: false,
					dataIndex: 'AgentName'
				},
				{
					header: "Username",
					sortable: true,
					dataIndex: 'Username'
				},
				{
					header: "AgentRef",
					sortable: true,
					dataIndex: 'AgentRef'
				}
			]),
			autoExpandColumn: 'Username'
		},
		// Store config
		{
			baseParams: {
				ServerGroupID: serverGroupID,
				SOAPModule: 'DNSHosting/DDNS',
				SOAPFunction: 'getDDNSUsers',
				SOAPParams: 'ServerGroupID,__null,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'AgentID'},
				{type: 'string',  dataIndex: 'Username'},
				{type: 'string',  dataIndex: 'AgentRef'}
			]
		}
	);

	DDNSUserWindow.show();
}


// Display edit/add form
function showDDNSUserWindow(DDNSUserWindow,serverGroupID,serverGroupName,userID,username) {

	var submitAjaxConfig;
	var icon;
	var editMode;
	var title;

	// We doing an update
	if (userID) {
		icon = 'silk-group_edit';
		title = "DDNS User - "+serverGroupName+" - "+username;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				ID: userID,
				SOAPFunction: 'updateDDNSUser',
				SOAPParams: 
					'ServerGroupID,'+
					'1:ID,'+
					'1:AgentID,'+
					'1:Username,'+
					'1:Password,'+
					'1:AgentRef'
			},
			onSuccess: function() {
				var store = Ext.getCmp(DDNSUserWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
		editMode = true;

	// We doing an Add
	} else {
		icon = 'silk-group_add';
		title = "DDNS User - "+serverGroupName;
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				SOAPFunction: 'createDDNSUser',
				SOAPParams: 
					'ServerGroupID,'+
					'1:ID,'+
					'1:AgentID,'+
					'1:Username,'+
					'1:Password,'+
					'1:AgentRef'
			},
			onSuccess: function() {
				var store = Ext.getCmp(DDNSUserWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
		editMode = false;
	}

	var agentStore = new Ext.ux.JsonStore({
			ID: userID,
			sortInfo: { field: "Name", direction: "ASC" },
			baseParams: {
				SOAPModule: 'Agent',
				SOAPFunction: 'getAgents',
				SOAPParams: '__search'
			}
	});

	var passwordID = Ext.id();

	// Create window
	var DDNSUserFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 320,
			height: 220,

			minWidth: 320,
			minHeight: 220
		},
		// Form panel config
		{
			labelWidth: 95,
			baseParams: {
				SOAPModule: 'DNSHosting/DDNS',
			},
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Agent',
					name: 'Agent',
					allowBlank: false,
					width: 160,

					store: agentStore,
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'AgentID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false
				},
				{
					fieldLabel: 'Username',
					name: 'Username',
					allowBlank: false
				},
				{
					id: passwordID,
					fieldLabel: 'Password',
					name: 'Password',
					allowBlank: editMode
				},
				{
					fieldLabel: 'AgentRef',
					name: 'AgentRef'
				}
			],
			extrabuttons: [ 
				{
					text: 'MkPass',
					handler: function() {
						var passfield = Ext.getCmp(passwordID);
						passfield.setValue(getRandomPass(8));
					}
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	DDNSUserFormWindow.show();

	if (userID) {
		// Load agent store to resolve the agnetID => agentName
		agentStore.load();

		Ext.getCmp(DDNSUserFormWindow.formPanelID).load({
			params: {
				ServerGroupID: serverGroupID,
				ID: userID,
				SOAPModule: 'DNSHosting/DDNS',
				SOAPFunction: 'getDDNSUser',
				SOAPParams: 'ServerGroupID,ID'
			}
		});
	}
}




// Display edit/add form
function showDDNSUserRemoveWindow(DDNSUserWindow,serverGroupID,userID) {
	// Mask DDNSUserWindow window
	DDNSUserWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this DDNS user?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(DDNSUserWindow,{
					params: {
						ServerGroupID: serverGroupID,
						ID: userID,
						SOAPModule: 'DNSHosting/DDNS',
						SOAPFunction: 'removeDDNSUser',
						SOAPParams: 'ServerGroupID,ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(DDNSUserWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				DDNSUserWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
