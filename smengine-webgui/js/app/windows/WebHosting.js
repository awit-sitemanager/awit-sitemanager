

function showWebHostingSitesWindow() {

	var webHostingSitesWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Web Hosting Site",
			
			width: 480,
			height: 350,
		
			minWidth: 480,
			minHeight: 350
		},
		// Grid config
		{
			// Inline toolbars
			tbar: [
				{
					text:'Add',
					tooltip:'Add site',
					iconCls:'add',
					handler: function() {
						showWebHostingSitesEditWindow();
					}
				}, 
				'-', 
				{
					text:'Edit',
					tooltip:'Edit site',
					iconCls:'option',
					handler: function() {
						var selectedItem = Ext.getCmp(webHostingSitesWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showWebHostingSitesEditWindow(selectedItem.data.ID);
						} else {
							webHostingSitesWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No site selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									webHostingSitesWindow.getEl().unmask();
								}
							});
						}
					}
				},
				'-',
				{
					text:'Remove',
					tooltip:'Remove site',
					iconCls:'remove',
					handler: function() {
						var selectedItem = Ext.getCmp(webHostingSitesWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showWebHostingSiteRemoveWindow(webHostingSitesWindow,selectedItem.data.ID);
						} else {
							webHostingSitesWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No site selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									webHostingSitesWindow.getEl().unmask();
								}
							});
						}
					}
				},
				'-',
				{
					text:'FTP Users',
					tooltip:'Website FTP Users',
					iconCls:'ftp',
					handler: function() {
						var selectedItem = Ext.get(webHostingSitesWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showWebHostingFTPUsersWindow(selectedItem.data.ID);
						} else {
							webHostingSitesWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No site selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									webHostingSitesWindow.getEl().unmask();
								}
							});
						}
					}
				},
				'-',
				{
					text:'Databases',
					tooltip:'Website Databases',
					iconCls:'databases',
					handler: function() {
						var selectedItem = Ext.get(webHostingSitesWindow.gridPanelID).getSelectionModel().getSelected();
						// Check if we have selected item
						if (selectedItem) {
							// If so display window
							showWebHostingDatabasesWindow(selectedItem.data.ID);
						} else {
							webHostingSitesWindow.getEl().mask();

							// Display error
							Ext.Msg.show({
								title: "Nothing selected",
								msg: "No site selected",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.CANCEL,
								modal: false,
								fn: function() {
									webHostingSitesWindow.getEl().unmask();
								}
							});
						}
					}
				},
			],
			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					//hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					id: 'AgentID',
					header: "AgentID",
					hidden: true,
					sortable: true,
					dataIndex: 'AgentID'
				},
				{
					header: "AgentName",
					//hidden: true,
					sortable: true,
					dataIndex: 'AgentName'
				},
				{
					header: "DomainName",
					sortable: true,
					dataIndex: 'DomainName'
				},
				{
					header: "DomainAliases",
					sortable: true,
					dataIndex: 'DomainAliases'
				},
				{
					header: "DiskQuota",
					sortable: true,
					dataIndex: 'DiskQuota'
				},
				{
					header: "AgentRef",
					//hidden: true,
					sortable: true,
					hidden: true,
					dataIndex: 'AgentRef'
				},
				{
					header: "AgentDisabled",
					//hidden: true,
					sortable: true,
					dataIndex: 'AgentDisabled'
				}
			]),

			autoExpandColumn: 'DomainName'
		},
		// Store config
		{
			baseParams: {
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting',
				SOAPFunction: 'getWebsites',
				SOAPParams: '__null,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'AgentID'},
				{type: 'string',  dataIndex: 'AgentName'},
				{type: 'string',  dataIndex: 'DomainName'},
				{type: 'string',  dataIndex: 'DomainAliases'},
				{type: 'numeric',  dataIndex: 'DiskQuota'}
			]
		}
	);

	webHostingSitesWindow.show();
}


// Display edit/add form
function showWebHostingSitesEditWindow(id) {

	var submitAjaxConfig;
	var editMode;

	// We doing an update
	if (id) {
		submitAjaxConfig = {
			ID: id,
			SOAPFunction: 'updateWebsite',
			SOAPParams: 
				'0:ID,'+
				'0:AgentID,'+
				'0:AgentName,'+
				'0:DomainName,'+
				'0:DomainAliases,'+
				'0:DiskQuota,'+
				'0:AgentRef,'+
				'0:AgentDisabled'
		};
		editMode = true;
	// We doing an Add
	} else {
		submitAjaxConfig = {
			SOAPFunction: 'createWebsite',
			SOAPParams: 
				'0:AgentID,'+
				'0:AgentName,'+
				'0:DomainName,'+
				'0:DomainAliases,'+
				'0:DiskQuota,'+
				'0:AgentRef,'+
				'0:AgentDisabled'
		};
		editMode = false;
	}

	// Create window
	var webHostingSiteFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Site Information",

			width: 320,
			height: 250,

			minWidth: 320,
			minHeight: 250
		},
		// Form panel config
		{
			labelWidth: 95,
			baseParams: {
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting'
			},
			items: [
			/*	{
					xtype: 'numberfield',
					fieldLabel: 'Agent ID',
					name: 'AgentID',
					allowBlank: false,
					editable: false,
					disabled: editMode
				},*/
				{
					xtype: 'combo',
					//id: 'combo',
					fieldLabel: 'Agent',
					name: 'Agent',
					allowBlank: false,
					width: 160,

					store: new Ext.ux.JsonStore({
						ID: id,
						sortInfo: { field: "Name", direction: "ASC" },
						baseParams: {
							SOAPUsername: globalConfig.soap.username,
							SOAPPassword: globalConfig.soap.password,
							SOAPAuthType: globalConfig.soap.authtype,
							SOAPModule: 'Agents',
							SOAPFunction: 'getAgents',
							SOAPParams: '__search'
						}
					}),
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'AgentID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false,
					disabled: editMode
				},
				{
					fieldLabel: 'Domain Name',
					name: 'DomainName',
					vtype: 'domain',
					maskRe: domainRe,
					allowBlank: false,	
					editable: false,
					disabled: editMode
				},
				{
					xtype: 'textarea',
					id: 'DomainAliases',
					width: 130,
					fieldLabel: 'Domain Aliases',
					name: 'DomainAliases',
					allowBlank: false,
					valueField: 'ID'
				},
				{
					fieldLabel: 'Disk Quota',
					name: 'DiskQuota',
					allowBlank: false,
					xtype: 'numberfield',
					minValue: 0,
					value: 0
				},
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	webHostingSiteFormWindow.show();

	if (id) {
		Ext.getCmp(webHostingSiteFormWindow.formPanelID).load({
			params: {
				id: id,
				SOAPUsername: globalConfig.soap.username,
				SOAPPassword: globalConfig.soap.password,
				SOAPAuthType: globalConfig.soap.authtype,
				SOAPModule: 'WebHosting',
				SOAPFunction: 'getWebsite',
				SOAPParams: 'id'
			}
		});
	}
}

// Display edit/add form
function showWebHostingSiteRemoveWindow(parent,id) {
	// Mask parent window
	parent.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this web site?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(parent,{
					params: {
						id: id,
						SOAPUsername: globalConfig.soap.username,
						SOAPPassword: globalConfig.soap.password,
						SOAPAuthType: globalConfig.soap.authtype,
						SOAPModule: 'WebHosting',
						SOAPFunction: 'removeWebsite',
						SOAPParams: 'id'
					}
				});

			// Unmask if user answered no
			} else {
				parent.getEl().unmask();
			}
		}
	});
}


