/*
Admin Group Attributes
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/




function showAdminGroupAttributesWindow(groupID) {

	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/GroupAttributes/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add attribute',
			iconCls:'silk-table_add',
			handler: function() {
				showAdminGroupAttributesAddEditWindow(adminGroupAttributesWindow,groupID);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/GroupAttributes/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit attribute',
			iconCls:'silk-table_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(adminGroupAttributesWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminGroupAttributesAddEditWindow(adminGroupAttributesWindow,groupID,selectedItem.data.ID);
				} else {
					adminGroupAttributesWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No attribute selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminGroupAttributesWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/GroupAttributes/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove attribute',
			iconCls:'silk-table_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminGroupAttributesWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminGroupAttributesRemoveWindow(adminGroupAttributesWindow,selectedItem.data.ID);
				} else {
					adminGroupAttributesWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No attribute selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminGroupAttributesWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var adminGroupAttributesWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Group Attributes",
			iconCls: 'silk-group',
			
			width: 900,
			height: 335,
		
			minWidth: 900,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					header: "ID",
					width: 10,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Attribute",
					width: 60,
					sortable: true,
					dataIndex: 'Attribute'
				}
			]),
			autoExpandColumn: 'Attribute'
		},
		// Store config
		{
			sortInfo: { field: "Attribute", direction: "ASC" },
			baseParams: {
				GroupID: groupID,
				SOAPModule: 'Admin/GroupAttributes',
				SOAPFunction: 'getGroupAttributes',
				SOAPParams: 'GroupID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Attribute'}
			]
		}
	);

	adminGroupAttributesWindow.show();
}


// Display edit/add form
function showAdminGroupAttributesAddEditWindow(adminGroupAttributesWindow,groupID,attributeID) {

	var submitAjaxConfig;
	var editMode;
	var icon;
	var title;

	// We doing an update
	if (attributeID) {
		icon = "silk-table_edit";
		title = "Update Attribute";
		submitAjaxConfig = {
			params: {
				ID: attributeID,
				SOAPFunction: 'updateGroupAttribute',
				SOAPParams: 
					'0:ID,'+
					'0:Attribute'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminGroupAttributesWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};

	// We doing an Add
	} else {
		icon = "silk-table_add";
		title = "Create Attribute";
		submitAjaxConfig = {
			params: {
				GroupID: groupID,
				SOAPFunction: 'createGroupAttribute',
				SOAPParams: 
					'0:GroupID,'+
					'0:Attribute'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminGroupAttributesWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}
	
	// Create window
	var adminGroupAttributesFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 360,
			height: 120,

			minWidth: 360,
			minHeight: 120
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/GroupAttributes'
			},
			items: [
				{
					fieldLabel: 'Attribute',
					name: 'Attribute',
					allowBlank: false,
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminGroupAttributesFormWindow.show();

	if (attributeID) {

		// Main form load
		adminGroupAttributesFormWindow.getFormPanel().load({
			params: {
				ID: attributeID,
				SOAPModule: 'Admin/GroupAttributes',
				SOAPFunction: 'getGroupAttribute',
				SOAPParams: 'ID'
			}
		});
	}
}



// Display remove form
function showAdminGroupAttributesRemoveWindow(adminGroupAttributesWindow,attributeID) {
	// Mask adminGroupAttributesWindow
	adminGroupAttributesWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this attribute?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminGroupAttributesWindow,{
					params: {
						ID: attributeID,
						SOAPModule: 'Admin/GroupAttributes',
						SOAPFunction: 'removeGroupAttribute',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminGroupAttributesWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminGroupAttributesWindow.getEl().unmask();
			}
		}
	});
}




// vim: ts=4
