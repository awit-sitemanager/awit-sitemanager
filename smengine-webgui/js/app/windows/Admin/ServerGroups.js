/*
Server Groups
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminServerGroupWindow() {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/ServerGroups/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add server group',
			iconCls: 'silk-building_add',
			handler: function() {
				showAdminServerGroupAddEditWindow(adminServerGroupWindow);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerGroups/Update")) {
		buttons.push({
			text:'Edit',
			tooltip:'Edit server group',
			iconCls: 'silk-building_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerGroupAddEditWindow(adminServerGroupWindow,selectedItem.data.ID);
				} else {
					adminServerGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerGroups/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove server group',
			iconCls: 'silk-building_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerGroupRemoveWindow(adminServerGroupWindow,selectedItem.data.ID);
				} else {
					adminServerGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerGroupAttributes/List")) {
		buttons.push({
			text:'Attributes',
			tooltip:'Attributes',
			iconCls: 'silk-table',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerGroupAttributesWindow(selectedItem.data.ID);
				} else {
					adminServerGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerGroupToAgents/List")) {
		buttons.push({
			text:'Agents',
			tooltip:'Agents',
			iconCls: 'silk-group',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerGroupToAgentWindow(selectedItem.data.ID);
				} else {
					adminServerGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}
	if (userHasCapability("Admin/ServerGroupToGroups/List")) {
		buttons.push({ 
			text:'User Groups',
			tooltip:'User groups',
			iconCls:'silk-group',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerGroupToGroupsWindow(selectedItem.data.ID);
				} else {
					adminServerGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var adminServerGroupWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Server Groups",
			iconCls: 'silk-building',

			width: 355,
			height: 335,

			minWidth: 355,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars (buttons)
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Name",
					sortable: true,
					dataIndex: 'Name'
				},
				{
					header: "Description",
					sortable: true,
					dataIndex: 'Description'
				}
			]),
			autoExpandColumn: 'Name'
		},
		// Store config
		{
			sortInfo: { field: "Name", direction: "ASC" },
			baseParams: {
				SOAPModule: 'Admin/ServerGroups',
				SOAPFunction: 'getServerGroups',
				SOAPParams: '__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string', dataIndex: 'Name'},
				{type: 'string', dataIndex: 'Description'}
			]
		}
	);

	adminServerGroupWindow.show();
}


// Display edit/add form
function showAdminServerGroupAddEditWindow(adminServerGroupWindow,serverGroupID) {

	var submitAjaxConfig;
	var icon;

	// We doing an update
	if (serverGroupID) {
		icon = 'silk-building_edit';
		submitAjaxConfig = {
			params: {
				ID: serverGroupID,
				SOAPFunction: 'updateServerGroup',
				SOAPParams:
					'0:ID,0:Name,0:Description'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminServerGroupWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	// We doing an Add
	} else {
		icon = 'silk-building_add';
		submitAjaxConfig = {
			params: {
				SOAPFunction: 'createServerGroup',
				SOAPParams:
					'0:Name,0:Description'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminServerGroupWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}

	// Create window
	var adminServerGroupFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Server Group Information",
			iconCls: icon,

			width: 320,
			height: 145,

			minWidth: 320,
			minHeight: 145
		},
		// Form panel config
		{
			labelWidth: 95,
			baseParams: {
				SOAPModule: 'Admin/ServerGroups'
			},
			items: [
				{
					fieldLabel: 'Name',
					name: 'Name',
					allowBlank: false
				},
				{
					fieldLabel: 'Description',
					name: 'Description',
					allowBlank: true
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminServerGroupFormWindow.show();

	if (serverGroupID) {
		Ext.getCmp(adminServerGroupFormWindow.formPanelID).load({
			params: {
				ID: serverGroupID,
				SOAPModule: 'Admin/ServerGroups',
				SOAPFunction: 'getServerGroup',
				SOAPParams: 'ID'
			}
		});
	}
}




// Display remove form
function showAdminServerGroupRemoveWindow(adminServerGroupWindow,serverGroupID) {
	// Mask adminUser window
	adminServerGroupWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this server group?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminServerGroupWindow,{
					params: {
						ID: serverGroupID,
						SOAPModule: 'Admin/ServerGroups',
						SOAPFunction: 'removeServerGroup',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminServerGroupWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminServerGroupWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
