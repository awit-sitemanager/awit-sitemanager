/*
Admin UsersToGroups management
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminUsersToGroupsWindow(userID) {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/UsersToGroups/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add group',
			iconCls:'silk-group_add',
			handler: function() {
				showAdminUserGroupAddWindow(adminUsersToGroupsWindow,userID);
			}
		});
		buttons.push('-'); 
	}
	if (userHasCapability("Admin/UsersToGroups/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove user from group',
			iconCls:'silk-group_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminUsersToGroupsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminUserGroupRemoveWindow(adminUsersToGroupsWindow,selectedItem.data.ID);
				} else {
					adminUsersToGroupsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminUsersToGroupsWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}
	var adminUsersToGroupsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Groups",
			iconCls: 'silk-group',
			
			width: 480,
			height: 335,
		
			minWidth: 400,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "UserID",
					sortable: true,
					dataIndex: 'UserID'
				},
				{
					header: "GroupID",
					sortable: true,
					dataIndex: 'GroupID'
				},
				{
					header: "GroupName",
					sortable: true,
					dataIndex: 'GroupName'
				},
				{
					header: "GroupDescription",
					sortable: true,
					dataIndex: 'GroupDescription'
				}
			]),
			autoExpandColumn: 'Name'
		},
		// Store config
		{
			sortInfo: { field: "GroupName", direction: "ASC" },
			baseParams: {
				ID: userID,
				SOAPModule: 'Admin/UsersToGroups',
				SOAPFunction: 'getUsersToGroups',
				SOAPParams: 'ID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'UserID'},
				{type: 'numeric',  dataIndex: 'GroupID'},
				{type: 'string',  dataIndex: 'GroupName'},
				{type: 'string',  dataIndex: 'GroupDescription'}
			]
		}
	);

	adminUsersToGroupsWindow.show();
}


// Display add form
function showAdminUserGroupAddWindow(adminUsersToGroupsWindow,userID) {

	var icon = 'silk-group_add';
	var submitAjaxConfig = {
		params: {
			UserID: userID,
			SOAPFunction: 'addUserToGroup',
			SOAPParams: 
				'0:UserID,'+
				'0:GroupID'
		},
		onSuccess: function() {
			var store = Ext.getCmp(adminUsersToGroupsWindow.gridPanelID).getStore();
			store.load({
				params: {
					limit: 25
				}
			});
		}
	};
	
	// Create window
	var adminUserGroupFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Group Information",
			iconCls: icon,

			width: 310,
			height: 113,

			minWidth: 310,
			minHeight: 113
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/UsersToGroups'
			},
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Group',
					name: 'Group',
					allowBlank: false,
					width: 160,

					store: new Ext.ux.JsonStore({
						sortInfo: { field: "Name", direction: "ASC" },
						baseParams: {
							SOAPModule: 'Admin/Groups',
							SOAPFunction: 'getGroups',
							SOAPParams: '__search'
						}
					}),
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'GroupID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminUserGroupFormWindow.show();
}




// Display remove form
function showAdminUserGroupRemoveWindow(adminUsersToGroupsWindow,userGroupID) {
	// Mask adminUsersToGroupsWindow window
	adminUsersToGroupsWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to unlink this group?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminUsersToGroupsWindow,{
					params: {
						ID: userGroupID,
						SOAPModule: 'Admin/UsersToGroups',
						SOAPFunction: 'removeUserToGroup',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminUsersToGroupsWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminUsersToGroupsWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
