/*
Admin API Group interface
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminAPIGroupWindow() {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/APIGroups/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add API group',
			iconCls:'silk-application_add',
			handler: function() {
				showAdminAPIGroupAddEditWindow(adminAPIGroupWindow);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/APIGroups/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit group',
			iconCls:'silk-application_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(adminAPIGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminAPIGroupAddEditWindow(adminAPIGroupWindow,selectedItem.data.ID);
				} else {
					adminAPIGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminAPIGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/APIGroups/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove group',
			iconCls:'silk-application_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminAPIGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminAPIGroupRemoveWindow(adminAPIGroupWindow,selectedItem.data.ID);
				} else {
					adminAPIGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminAPIGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/APIGroups/Capabilities")) {
		buttons.push({ 
			text:'Capabilities',
			tooltip:'SOAP API Group Capabilties',
			iconCls:'silk-table',
			handler: function() {
				var selectedItem = Ext.getCmp(adminAPIGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminAPIGroupCapabilitiesWindow(selectedItem.data.ID);
				} else {
					adminAPIGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminAPIGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	var adminAPIGroupWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "API Groups",
			iconCls: 'silk-application',
			
			width: 600,
			height: 335,
		
			minWidth: 600,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Name",
					sortable: true,
					dataIndex: 'Name'
				},
				{
					header: "Description",
					sortable: true,
					dataIndex: 'Description'
				}
			]),
			autoExpandColumn: 'Name'
		},
		// Store config
		{
			sortInfo: { field: "Name", direction: "ASC" },
			baseParams: {
				SOAPModule: 'Admin/APIGroups',
				SOAPFunction: 'getAPIGroups',
				SOAPParams: '__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Name'},
				{type: 'string',  dataIndex: 'Description'}
			]
		}
	);

	adminAPIGroupWindow.show();
}


// Display edit/add form
function showAdminAPIGroupAddEditWindow(adminAPIGroupWindow,APIGroupID) {

	var submitAjaxConfig;
	var icon;

	// We doing an update
	if (APIGroupID) {
		icon = 'silk-application_edit';
		submitAjaxConfig = {
			params: {
				ID: APIGroupID,
				SOAPFunction: 'updateAPIGroup',
				SOAPParams: 
					'0:ID,'+
					'0:Name,'+
					'0:Description'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminAPIGroupWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};

	// We doing an Add
	} else {
		icon = 'silk-application_add';
		submitAjaxConfig = {
			params: {
				SOAPFunction: 'createAPIGroup',
				SOAPParams: 
					'0:Name,'+
					'0:Description'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminAPIGroupWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}
	
	// Create window
	var adminAPIGroupFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Group Information",
			iconCls: icon,

			width: 310,
			height: 143,

			minWidth: 310,
			minHeight: 143
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/APIGroups'
			},
			items: [
				{
					fieldLabel: 'Name',
					name: 'Name',
					allowBlank: false,
				},
				{
					fieldLabel: 'Description',
					name: 'Description',
					allowBlank: true
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminAPIGroupFormWindow.show();

	if (APIGroupID) {
		Ext.getCmp(adminAPIGroupFormWindow.formPanelID).load({
			params: {
				ID: APIGroupID,
				SOAPModule: 'Admin/APIGroups',
				SOAPFunction: 'getAPIGroup',
				SOAPParams: 'ID'
			}
		});
	}
}




// Display remove form
function showAdminAPIGroupRemoveWindow(adminAPIGroupWindow,APIGroupID) {
	// Mask APIGroup window
	adminAPIGroupWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this group?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminAPIGroupWindow,{
					params: {
						ID: APIGroupID,
						SOAPModule: 'Admin/APIGroups',
						SOAPFunction: 'removeAPIGroup',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminAPIGroupWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminAPIGroupWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
