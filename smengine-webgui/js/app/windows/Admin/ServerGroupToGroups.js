/*
Admin Server group to groups
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminServerGroupToGroupsWindow(serverGroupID) {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/ServerGroupToGroups/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add server group to group',
			iconCls:'silk-group_add',
			handler: function() {
				showAdminServerGroupToGroupsAddWindow(adminServerGroupToGroupsWindow,serverGroupID);
			}
		});
		buttons.push('-'); 
	}
	if (userHasCapability("Admin/ServerGroupToGroups/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove group from server group',
			iconCls:'silk-group_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerGroupToGroupsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerGroupToGroupsRemoveWindow(adminServerGroupToGroupsWindow,selectedItem.data.ID);
				} else {
					adminServerGroupToGroupsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerGroupToGroupsWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}

	var adminServerGroupToGroupsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "User Groups",
			iconCls: 'silk-group',
			
			width: 650,
			height: 335,
		
			minWidth: 650,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					dataIndex: 'ID',
					width: 40
				},
				{
					header: "ServerGroupID",
					sortable: true,
					dataIndex: 'ServerGroupID',
					width: 70
				},
				{
					header: "UserGroupID",
					sortable: true,
					dataIndex: 'UserGroupID',
					width: 70
				},
				{
					header: "UserGroupName",
					sortable: true,
					dataIndex: 'UserGroupName'
				},
				{
					header: "UserGroupDescription",
					sortable: true,
					dataIndex: 'UserGroupDescription'
				}
			]),
			autoExpandColumn: 'UserGroupName'
		},
		// Store config
		{
			sortInfo: { field: "UserGroupName", direction: "ASC" },
			baseParams: {
				ID: serverGroupID,
				SOAPModule: 'Admin/ServerGroupToGroups',
				SOAPFunction: 'getServerGroupToGroups',
				SOAPParams: 'ID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'ServerGroupID'},
				{type: 'numeric',  dataIndex: 'UserGroupID'},
				{type: 'string',  dataIndex: 'UserGroupName'},
				{type: 'string',  dataIndex: 'UserGroupDescription'}
			]
		}
	);

	adminServerGroupToGroupsWindow.show();
}


// Display add form
function showAdminServerGroupToGroupsAddWindow(adminServerGroupToGroupsWindow,serverGroupID) {

	var icon = 'silk-group_add';
	var submitAjaxConfig = {
		params: {
			ServerGroupID: serverGroupID,
			SOAPFunction: 'addServerGroupToGroup',
			SOAPParams: 
				'0:ServerGroupID,'+
				'0:UserGroupID'
		},
		onSuccess: function() {
			var store = Ext.getCmp(adminServerGroupToGroupsWindow.gridPanelID).getStore();
			store.load({
				params: {
					limit: 25
				}
			});
		}
	};
	
	// Create window
	var adminServerGroupToGroupsFromWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Groups to server group information",
			iconCls: icon,

			width: 390,
			height: 113,

			minWidth: 310,
			minHeight: 113
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/ServerGroupToGroups'
			},
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Group',
					name: 'Group',
					allowBlank: false,
					width: 160,

					store: new Ext.ux.JsonStore({
						sortInfo: { field: "Name", direction: "ASC" },
						baseParams: {
							SOAPModule: 'Admin/Groups',
							SOAPFunction: 'getGroups',
							SOAPParams: '__null,__search'
						}
					}),
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'UserGroupID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminServerGroupToGroupsFromWindow.show();
}




// Display remove form
function showAdminServerGroupToGroupsRemoveWindow(adminServerGroupToGroupsWindow,serverGroupToGroupID) {
	// Mask adminServerGroupToGroupsWindow window
	adminServerGroupToGroupsWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to unlink this group?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminServerGroupToGroupsWindow,{
					params: {
						ID: serverGroupToGroupID,
						SOAPModule: 'Admin/ServerGroupToGroups',
						SOAPFunction: 'removeServerGroupToGroup',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminServerGroupToGroupsWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminServerGroupToGroupsWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
