/*
Admin users
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminUserWindow() {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/Users/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add user',
			iconCls:'silk-user_add',
			handler: function() {
				showAdminUserAddEditWindow(adminUserWindow);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/Users/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit user',
			iconCls:'silk-user_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(adminUserWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminUserAddEditWindow(adminUserWindow,selectedItem.data.ID);
				} else {
					adminUserWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminUserWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/Users/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove user',
			iconCls:'silk-user_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminUserWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminUserRemoveWindow(adminUserWindow,selectedItem.data.ID);
				} else {
					adminUserWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminUserWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/UsersToGroups/List")) {
		buttons.push({ 
			text:'Groups',
			tooltip:'User groups',
			iconCls:'silk-group',
			handler: function() {
				var selectedItem = Ext.getCmp(adminUserWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminUsersToGroupsWindow(selectedItem.data.ID);
				} else {
					adminUserWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminUserWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/UsersToAPIGroups/List")) {
		buttons.push({ 
			text:'API Groups',
			tooltip:'User API groups',
			iconCls:'silk-group',
			handler: function() {
				var selectedItem = Ext.getCmp(adminUserWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminUsersToAPIGroupsWindow(selectedItem.data.ID);
				} else {
					adminUserWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No user selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminUserWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var adminUserWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Users",
			iconCls: "silk-user",
			
			width: 400,
			height: 335,
		
			minWidth: 400,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars (buttons)
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Username",
					sortable: true,
					dataIndex: 'Username'
				},
				{
					header: "Name",
					sortable: true,
					dataIndex: 'Name'
				}
			]),
			autoExpandColumn: 'Username'
		},
		// Store config
		{
			sortInfo: { field: "Username", direction: "ASC" },
			baseParams: {
				SOAPModule: 'Admin/Users',
				SOAPFunction: 'getUsers',
				SOAPParams: '__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Username'},
				{type: 'string', dataIndex: 'Name'}
			]
		}
	);

	adminUserWindow.show();
}


// Display edit/add form
function showAdminUserAddEditWindow(adminUserWindow,userID) {

	var submitAjaxConfig;
	var icon;
	var editMode;

	// We doing an update
	if (userID) {
		editMode = true;
		icon = 'silk-user_edit';
		submitAjaxConfig = {
			params: {
				ID: userID,
				SOAPFunction: 'updateUser',
				SOAPParams: 
					'0:ID,0:Password,0:Name'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminUserWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	// We doing an Add
	} else {
		editMode = false;
		icon = 'silk-user_add';
		submitAjaxConfig = {
			params: {
				SOAPFunction: 'createUser',
				SOAPParams: 
					'0:Username,0:Password,0:Name'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminUserWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}

	// Password ID
	var passwordID = Ext.id();

	// Create window
	var adminUserFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "User Information",
			iconCls: icon,

			width: 320,
			height: 175,

			minWidth: 320,
			minHeight: 175
		},
		// Form panel config
		{
			labelWidth: 95,
			baseParams: {
				SOAPModule: 'Admin/Users'
			},
			items: [
				{
					fieldLabel: 'Username',
					name: 'Username',
					allowBlank: false,
					disabled: editMode
				},
				{
					id: passwordID,
					fieldLabel: 'Password',
					name: 'Password',
					allowBlank: editMode
				},
				{
					fieldLabel: 'Name',
					name: 'Name',
					allowBlank: true
				}
			],
			extrabuttons: [
				{
					text: 'MkPass',
					handler: function() {
						var passfield = Ext.getCmp(passwordID);
						passfield.setValue(getRandomPass(8));
					}
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminUserFormWindow.show();

	if (userID) {
		Ext.getCmp(adminUserFormWindow.formPanelID).load({
			params: {
				ID: userID,
				SOAPModule: 'Admin/Users',
				SOAPFunction: 'getUser',
				SOAPParams: 'ID'
			}
		});
	}
}




// Display remove form
function showAdminUserRemoveWindow(adminUserWindow,userID) {
	// Mask adminUser window
	adminUserWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this user?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminUserWindow,{
					params: {
						ID: userID,
						SOAPModule: 'Admin/Users',
						SOAPFunction: 'removeUser',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminUserWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminUserWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
