/*
Admin Group interface
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminGroupWindow() {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/Groups/Add")) {
		buttons.push({ 
			text:'Add',
			tooltip:'Add group',
			iconCls:'silk-group_add',
			handler: function() {
				showAdminGroupAddEditWindow(adminGroupWindow);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/Groups/Update")) {
		buttons.push({ 
			text:'Edit',
			tooltip:'Edit group',
			iconCls:'silk-group_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(adminGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminGroupAddEditWindow(adminGroupWindow,selectedItem.data.ID);
				} else {
					adminGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/Groups/Remove")) {
		buttons.push({ 
			text:'Remove',
			tooltip:'Remove group',
			iconCls:'silk-group_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminGroupRemoveWindow(adminGroupWindow,selectedItem.data.ID);
				} else {
					adminGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/GroupAttributes/List")) {
		buttons.push({ 
			text:'Attributes',
			tooltip:'Group Attributes',
			iconCls:'silk-group',
			handler: function() {
				var selectedItem = Ext.getCmp(adminGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminGroupAttributesWindow(selectedItem.data.ID);
				} else {
					adminGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/GroupsToAPIGroups/List")) {
		buttons.push({ 
			text:'API Groups',
			tooltip:'API groups',
			iconCls:'silk-group',
			handler: function() {
				var selectedItem = Ext.getCmp(adminGroupWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminGroupsToAPIGroupsWindow(selectedItem.data.ID);
				} else {
					adminGroupWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminGroupWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	var adminGroupWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Groups",
			iconCls: 'silk-group',
			
			width: 600,
			height: 335,
		
			minWidth: 600,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Name",
					sortable: true,
					dataIndex: 'Name'
				},
				{
					header: "Description",
					sortable: true,
					dataIndex: 'Description'
				}
			]),
			autoExpandColumn: 'Name'
		},
		// Store config
		{
			sortInfo: { field: "Name", direction: "ASC" },
			baseParams: {
				SOAPModule: 'Admin/Groups',
				SOAPFunction: 'getGroups',
				SOAPParams: '__null,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Name'},
				{type: 'string',  dataIndex: 'Description'}
			]
		}
	);

	adminGroupWindow.show();
}


// Display edit/add form
function showAdminGroupAddEditWindow(adminGroupWindow,groupID) {

	var submitAjaxConfig;
	var icon;
	var editMode;

	// We doing an update
	if (groupID) {
		icon = 'silk-group_edit';
		submitAjaxConfig = {
			params: {
				ID: groupID,
				SOAPFunction: 'updateGroup',
				SOAPParams: 
					'0:ID,'+
					'0:Name,'+
					'0:Description'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminGroupWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};

	// We doing an Add
	} else {
		icon = 'silk-group_add';
		submitAjaxConfig = {
			params: {
				SOAPFunction: 'createGroup',
				SOAPParams: 
					'0:Name,'+
					'0:Description'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminGroupWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}
	
	// Create window
	var adminGroupFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Group Information",
			iconCls: icon,

			width: 310,
			height: 143,

			minWidth: 310,
			minHeight: 143
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/Groups'
			},
			items: [
				{
					fieldLabel: 'Name',
					name: 'Name',
					allowBlank: false,
				},
				{
					fieldLabel: 'Description',
					name: 'Description',
					allowBlank: true
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminGroupFormWindow.show();

	if (groupID) {
		Ext.getCmp(adminGroupFormWindow.formPanelID).load({
			params: {
				ID: groupID,
				SOAPModule: 'Admin/Groups',
				SOAPFunction: 'getGroup',
				SOAPParams: 'ID'
			}
		});
	}
}




// Display remove form
function showAdminGroupRemoveWindow(adminGroupWindow,groupID) {
	// Mask adminGroupWindow window
	adminGroupWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this group?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminGroupWindow,{
					params: {
						ID: groupID,
						SOAPModule: 'Admin/Groups',
						SOAPFunction: 'removeGroup',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminGroupWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminGroupWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
