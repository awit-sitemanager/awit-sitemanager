/*
Servers
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminServerWindow() {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/Servers/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add server',
			iconCls:'silk-server_add',
			handler: function() {
				showAdminServerAddEditWindow(adminServerWindow);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/Servers/Update")) {
		buttons.push({
			text:'Edit',
			tooltip:'Edit server',
			iconCls:'silk-server_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerAddEditWindow(adminServerWindow,selectedItem.data.ID);
				} else {
					adminServerWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/Servers/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove server',
			iconCls:'silk-server_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerRemoveWindow(adminServerWindow,selectedItem.data.ID);
				} else {
					adminServerWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerAttributes/List")) {
		buttons.push({
			text:'Attributes',
			tooltip:'Attributes',
			iconCls: 'silk-table',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerAttributesWindow(selectedItem.data.ID);
				} else {
					adminServerWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerToServerGroups/List")) {
		buttons.push({
			text:'Server Groups',
			tooltip:'Server Groups',
			iconCls: 'silk-building',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerToServerGroupWindow(selectedItem.data.ID);
				} else {
					adminServerWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}

	// Create window
	var adminServerWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Servers",
			iconCls: "silk-server",

			width: 415,
			height: 335,

			minWidth: 415,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars (buttons)
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					header: "ID",
					hidden: true,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Name",
					sortable: true,
					dataIndex: 'Name'
				}
			]),
			autoExpandColumn: 'Name'
		},
		// Store config
		{
			sortInfo: { field: "Name", direction: "ASC" },
			baseParams: {
				SOAPModule: 'Admin/Servers',
				SOAPFunction: 'getServers',
				SOAPParams: '__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string', dataIndex: 'Name'}
			]
		}
	);

	adminServerWindow.show();
}


// Display edit/add form
function showAdminServerAddEditWindow(adminServerWindow,serverID) {

	var submitAjaxConfig;
	var icon;
	var editMode;

	// We doing an update
	if (serverID) {
		editMode = true;
		icon = 'silk-server_edit';
		submitAjaxConfig = {
			params: {
				ID: serverID,
				SOAPFunction: 'updateServer',
				SOAPParams:
					'0:ID,0:Name'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminServerWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	// We doing an Add
	} else {
		editMode = false;
		icon = 'silk-server_add';
		submitAjaxConfig = {
			params: {
				SOAPFunction: 'createServer',
				SOAPParams:
					'0:Name'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminServerWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}

	// Create window
	var adminServerFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Server Information",
			iconCls: icon,

			width: 320,
			height: 120,

			minWidth: 320,
			minHeight: 120
		},
		// Form panel config
		{
			labelWidth: 95,
			baseParams: {
				SOAPModule: 'Admin/Servers'
			},
			items: [
				{
					fieldLabel: 'Name',
					name: 'Name',
					allowBlank: false
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminServerFormWindow.show();

	if (serverID) {
		Ext.getCmp(adminServerFormWindow.formPanelID).load({
			params: {
				ID: serverID,
				SOAPModule: 'Admin/Servers',
				SOAPFunction: 'getServer',
				SOAPParams: 'ID'
			}
		});
	}
}




// Display remove form
function showAdminServerRemoveWindow(adminServerWindow,serverID) {
	// Mask adminUser window
	adminServerWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this server?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminServerWindow,{
					params: {
						ID: serverID,
						SOAPModule: 'Admin/Servers',
						SOAPFunction: 'removeServer',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminServerWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminServerWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
