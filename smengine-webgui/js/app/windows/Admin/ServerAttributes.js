/*
Server Attributes
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/




function showAdminServerAttributesWindow(serverID) {

	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/ServerAttributes/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add attribute',
			iconCls: 'silk-table_add',
			handler: function() {
				showAdminServerAttributesAddEditWindow(adminServerAttributesWindow,serverID);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerAttributes/Update")) {
		buttons.push({
			text:'Edit',
			tooltip:'Edit attribute',
			iconCls: 'silk-table_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerAttributesWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerAttributesAddEditWindow(adminServerAttributesWindow,serverID,selectedItem.data.ID);
				} else {
					adminServerAttributesWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No attribute selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerAttributesWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerAttributes/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove attribute',
			iconCls: 'silk-table_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerAttributesWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerAttributesRemoveWindow(adminServerAttributesWindow,selectedItem.data.ID);
				} else {
					adminServerAttributesWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No attribute selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerAttributesWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var adminServerAttributesWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Server Attributes",
			iconCls: 'silk-table',

			width: 900,
			height: 335,

			minWidth: 900,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					header: "ID",
					width: 10,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Attribute",
					width: 60,
					sortable: true,
					dataIndex: 'Attribute'
				}
			]),
			autoExpandColumn: 'Attribute'
		},
		// Store config
		{
			sortInfo: { field: "Attribute", direction: "ASC" },
			baseParams: {
				ServerID: serverID,
				SOAPModule: 'Admin/ServerAttributes',
				SOAPFunction: 'getServerAttributes',
				SOAPParams: 'ServerID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Attribute'}
			]
		}
	);

	adminServerAttributesWindow.show();
}


// Display edit/add form
function showAdminServerAttributesAddEditWindow(adminServerAttributesWindow,serverID,attributeID) {

	var submitAjaxConfig;
	var editMode;
	var icon;

	// We doing an update
	if (attributeID) {
		icon = 'silk-table_edit';
		title = "Update Attribute";
		submitAjaxConfig = {
			params: {
				ID: attributeID,
				SOAPFunction: 'updateServerAttribute',
				SOAPParams:
					'0:ID,'+
					'0:Attribute'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminServerAttributesWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};

	// We doing an Add
	} else {
		icon = 'silk-table_add';
		title = "Create Attribute";
		submitAjaxConfig = {
			params: {
				ServerID: serverID,
				SOAPFunction: 'createServerAttribute',
				SOAPParams:
					'0:ServerID,'+
					'0:Attribute'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminServerAttributesWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}

	// Create window
	var adminServerAttributesFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: title,
			iconCls: icon,

			width: 360,
			height: 120,

			minWidth: 360,
			minHeight: 120
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/ServerAttributes'
			},
			items: [
				{
					fieldLabel: 'Attribute',
					name: 'Attribute',
					allowBlank: false,
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminServerAttributesFormWindow.show();

	if (attributeID) {

		// Main form load
		adminServerAttributesFormWindow.getFormPanel().load({
			params: {
				ID: attributeID,
				SOAPModule: 'Admin/ServerAttributes',
				SOAPFunction: 'getServerAttribute',
				SOAPParams: 'ID'
			}
		});
	}
}



// Display remove form
function showAdminServerAttributesRemoveWindow(adminServerAttributesWindow,attributeID) {
	// Mask adminServerAttributesWindow
	adminServerAttributesWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this attribute?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminServerAttributesWindow,{
					params: {
						ID: attributeID,
						SOAPModule: 'Admin/ServerAttributes',
						SOAPFunction: 'removeServerAttribute',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminServerAttributesWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminServerAttributesWindow.getEl().unmask();
			}
		}
	});
}



// vim: ts=4
