/*
Admin Group to API groups
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminGroupsToAPIGroupsWindow(groupID) {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/GroupsToAPIGroups/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add Group to API Group',
			iconCls:'silk-group_add',
			handler: function() {
				showAdminGroupsToAPIGroupsAddWindow(adminGroupsToAPIGroupsWindow,groupID);
			}
		});
		buttons.push('-'); 
	}
	if (userHasCapability("Admin/GroupsToAPIGroups/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove Group from API group',
			iconCls:'silk-group_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminGroupsToAPIGroupsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminGroupsToAPIGroupsRemoveWindow(adminGroupsToAPIGroupsWindow,selectedItem.data.ID);
				} else {
					adminGroupsToAPIGroupsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminGroupsToAPIGroupsWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}

	var adminGroupsToAPIGroupsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Groups",
			iconCls: 'silk-group',
			
			width: 480,
			height: 335,
		
			minWidth: 400,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "GroupID",
					sortable: true,
					dataIndex: 'GroupID'
				},
				{
					header: "APIGroupID",
					sortable: true,
					dataIndex: 'APIGroupID'
				},
				{
					header: "APIGroupName",
					sortable: true,
					dataIndex: 'APIGroupName'
				},
				{
					header: "APIGroupDescription",
					sortable: true,
					dataIndex: 'APIGroupDescription'
				}
			]),
			autoExpandColumn: 'Name'
		},
		// Store config
		{
			sortInfo: { field: "APIGroupName", direction: "ASC" },
			baseParams: {
				ID: groupID,
				SOAPModule: 'Admin/GroupsToAPIGroups',
				SOAPFunction: 'getGroupsToAPIGroups',
				SOAPParams: 'ID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'GroupID'},
				{type: 'numeric',  dataIndex: 'APIGroupID'},
				{type: 'string',  dataIndex: 'APIGroupName'},
				{type: 'string',  dataIndex: 'APIGroupDescription'}
			]
		}
	);

	adminGroupsToAPIGroupsWindow.show();
}


// Display add form
function showAdminGroupsToAPIGroupsAddWindow(adminGroupsToAPIGroupsWindow,groupID) {

	var icon = 'silk-group_add';
	var submitAjaxConfig = {
		params: {
			GroupID: groupID,
			SOAPFunction: 'addGroupToAPIGroup',
			SOAPParams: 
				'0:GroupID,'+
				'0:APIGroupID'
		},
		onSuccess: function() {
			var store = Ext.getCmp(adminGroupsToAPIGroupsWindow.gridPanelID).getStore();
			store.load({
				params: {
					limit: 25
				}
			});
		}
	};
	
	// Create window
	var adminGroupsToAPIGroupsFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Groups to API Group Information",
			iconCls: icon,

			width: 390,
			height: 113,

			minWidth: 310,
			minHeight: 113
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/GroupsToAPIGroups'
			},
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Group',
					name: 'Group',
					allowBlank: false,
					width: 160,

					store: new Ext.ux.JsonStore({
						sortInfo: { field: "Name", direction: "ASC" },
						baseParams: {
							SOAPModule: 'Admin/APIGroups',
							SOAPFunction: 'getAPIGroups',
							SOAPParams: '__null,__search'
						}
					}),
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'APIGroupID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminGroupsToAPIGroupsFormWindow.show();
}




// Display remove form
function showAdminGroupsToAPIGroupsRemoveWindow(adminGroupsToAPIGroupsWindow,groupAPIGroupID) {
	// Mask adminGroupAPIGroupWindow window
	adminGroupsToAPIGroupsWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to unlink this group?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminGroupsToAPIGroupsWindow,{
					params: {
						ID: groupAPIGroupID,
						SOAPModule: 'Admin/GroupsToAPIGroups',
						SOAPFunction: 'removeGroupToAPIGroup',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminGroupsToAPIGroupsWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminGroupsToAPIGroupsWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
