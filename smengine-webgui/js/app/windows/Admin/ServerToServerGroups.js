/*
Server to server group
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminServerToServerGroupWindow(serverID) {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/ServerToServerGroups/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add server group',
			iconCls:'silk-group_add',
			handler: function() {
				showAdminServerToServerGroupAddWindow(adminServerToServerGroupsWindow,serverID);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerToServerGroups/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove server from server group',
			iconCls:'silk-group_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerToServerGroupsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerToServerGroupRemoveWindow(adminServerToServerGroupsWindow,selectedItem.data.ID);
				} else {
					adminServerToServerGroupsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No server group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerToServerGroupsWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}
	var adminServerToServerGroupsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Server Groups",
			iconCls: 'silk-group',

			width: 650,
			height: 335,

			minWidth: 650,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					width: 20,
					dataIndex: 'ID'
				},
				{
					header: "ServerID",
					sortable: true,
					width: 60,
					dataIndex: 'ServerID'
				},
				{
					header: "ServerGroupID",
					sortable: true,
					width: 70,
					dataIndex: 'ServerGroupID'
				},
				{
					header: "ServerGroupName",
					sortable: true,
					dataIndex: 'ServerGroupName'
				},
				{
					header: "ServerGroupDescription",
					sortable: true,
					dataIndex: 'ServerGroupDescription'
				}
			]),
			autoExpandColumn: 'ServerGroupName'
		},
		// Store config
		{
			sortInfo: { field: "ServerGroupName", direction: "ASC" },
			baseParams: {
				ID: serverID,
				SOAPModule: 'Admin/ServerToServerGroups',
				SOAPFunction: 'getServerToServerGroups',
				SOAPParams: 'ID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'ServerID'},
				{type: 'numeric',  dataIndex: 'ServerGroupID'},
				{type: 'string',  dataIndex: 'ServerGroupName'},
				{type: 'string',  dataIndex: 'ServerGroupDescription'}
			]
		}
	);

	adminServerToServerGroupsWindow.show();
}


// Display add form
function showAdminServerToServerGroupAddWindow(adminServerToServerGroupsWindow,serverID) {

	var submitAjaxConfig = {
		params: {
			ServerID: serverID,
			SOAPFunction: 'addServerToServerGroup',
			SOAPParams:
				'0:ServerID,'+
				'0:ServerGroupID'
		},
		onSuccess: function() {
			var store = Ext.getCmp(adminServerToServerGroupsWindow.gridPanelID).getStore();
			store.load({
				params: {
					limit: 25
				}
			});
		}
	};

	// Create window
	var adminServerToServerGroupFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Server Group Information",

			width: 375,
			height: 115,

			minWidth: 375,
			minHeight: 115
		},
		// Form panel config
		{
			labelWidth: 120,
			baseParams: {
				SOAPModule: 'Admin/ServerToServerGroups'
			},
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'ServerGroup',
					name: 'ServerGroup',
					allowBlank: false,

					store: new Ext.ux.JsonStore({
						sortInfo: { field: "Name", direction: "ASC" },
						baseParams: {
							SOAPModule: 'Admin/ServerGroups',
							SOAPFunction: 'getServerGroups',
							SOAPParams: '__search'
						}
					}),
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'ServerGroupID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminServerToServerGroupFormWindow.show();
}




// Display remove form
function showAdminServerToServerGroupRemoveWindow(adminServerToServerGroupsWindow,serverToServerGroupID) {
	// Mask adminServerToServerGroupsWindow window
	adminServerToServerGroupsWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to unlink this server group?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminServerToServerGroupsWindow,{
					params: {
						ID: serverToServerGroupID,
						SOAPModule: 'Admin/ServerToServerGroups',
						SOAPFunction: 'removeServerToServerGroup',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminServerToServerGroupsWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminServerToServerGroupsWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
