/*
Admin UsersToAPIGroups management
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminUsersToAPIGroupsWindow(userID) {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/UsersToAPIGroups/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add API group',
			iconCls:'silk-group_add',
			handler: function() {
				showAdminUserAPIGroupAddWindow(adminUsersToAPIGroupsWindow,userID);
			}
		});
		buttons.push('-'); 
	}
	if (userHasCapability("Admin/UsersToAPIGroups/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove user from API group',
			iconCls:'silk-group_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminUsersToAPIGroupsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminUserAPIGroupRemoveWindow(adminUsersToAPIGroupsWindow,selectedItem.data.ID);
				} else {
					adminUsersToAPIGroupsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No API group selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminUsersToAPIGroupsWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}
	var adminUsersToAPIGroupsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "API Groups",
			iconCls: 'silk-group',
			
			width: 480,
			height: 335,
		
			minWidth: 400,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "UserID",
					sortable: true,
					dataIndex: 'UserID'
				},
				{
					header: "APIGroupID",
					sortable: true,
					dataIndex: 'APIGroupID'
				},
				{
					header: "APIGroupName",
					sortable: true,
					dataIndex: 'APIGroupName'
				},
				{
					header: "APIGroupDescription",
					sortable: true,
					dataIndex: 'APIGroupDescription'
				}
			]),
			autoExpandColumn: 'APIGroupName'
		},
		// Store config
		{
			sortInfo: { field: "APIGroupName", direction: "ASC" },
			baseParams: {
				ID: userID,
				SOAPModule: 'Admin/UsersToAPIGroups',
				SOAPFunction: 'getUsersToAPIGroups',
				SOAPParams: 'ID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'UserID'},
				{type: 'numeric',  dataIndex: 'APIGroupID'},
				{type: 'string',  dataIndex: 'APIGroupName'},
				{type: 'string',  dataIndex: 'APIGroupDescription'}
			]
		}
	);

	adminUsersToAPIGroupsWindow.show();
}


// Display add form
function showAdminUserAPIGroupAddWindow(adminUsersToAPIGroupsWindow,userID) {

	var icon = 'silk-group_add';
	var submitAjaxConfig = {
		params: {
			UserID: userID,
			SOAPFunction: 'addUserToAPIGroup',
			SOAPParams: 
				'0:UserID,'+
				'0:APIGroupID'
		},
		onSuccess: function() {
			var store = Ext.getCmp(adminUsersToAPIGroupsWindow.gridPanelID).getStore();
			store.load({
				params: {
					limit: 25
				}
			});
		}
	};
	
	// Create window
	var adminUserAPIGroupFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "API Group Information",
			iconCls: icon,

			width: 310,
			height: 113,

			minWidth: 310,
			minHeight: 113
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/UsersToAPIGroups'
			},
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'APIGroup',
					name: 'APIGroup',
					allowBlank: false,
					width: 160,

					store: new Ext.ux.JsonStore({
						sortInfo: { field: "Name", direction: "ASC" },
						baseParams: {
							SOAPModule: 'Admin/APIGroups',
							SOAPFunction: 'getAPIGroups',
							SOAPParams: '__search'
						}
					}),
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'APIGroupID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminUserAPIGroupFormWindow.show();
}




// Display remove form
function showAdminUserAPIGroupRemoveWindow(adminUsersToAPIGroupsWindow,userAPIGroupID) {
	// Mask adminUsersToAPIGroupsWindow window
	adminUsersToAPIGroupsWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to unlink this API group?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminUsersToAPIGroupsWindow,{
					params: {
						ID: userAPIGroupID,
						SOAPModule: 'Admin/UsersToAPIGroups',
						SOAPFunction: 'removeUserToAPIGroup',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminUsersToAPIGroupsWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminUsersToAPIGroupsWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
