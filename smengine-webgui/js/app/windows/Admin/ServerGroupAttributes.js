/*
Server Group Attributes
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/




function showAdminServerGroupAttributesWindow(serverGroupID) {

	// Window buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/ServerGroupAttributes/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add attribute',
			iconCls: 'silk-table_add',
			handler: function() {
				showAdminServerGroupAttributesAddEditWindow(adminServerGroupAttributesWindow,serverGroupID);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerGroupAttributes/Update")) {
		buttons.push({
			text:'Edit',
			tooltip:'Edit attribute',
			iconCls: 'silk-table_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerGroupAttributesWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerGroupAttributesAddEditWindow(adminServerGroupAttributesWindow,serverGroupID,selectedItem.data.ID);
				} else {
					adminServerGroupAttributesWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No attribute selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerGroupAttributesWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerGroupAttributes/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove attribute',
			iconCls: 'silk-table_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerGroupAttributesWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerGroupAttributesRemoveWindow(adminServerGroupAttributesWindow,selectedItem.data.ID);
				} else {
					adminServerGroupAttributesWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No attribute selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerGroupAttributesWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}

	// Create window
	var adminServerGroupAttributesWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Server Group Attributes",
			iconCls: 'silk-table',

			width: 900,
			height: 335,

			minWidth: 900,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					header: "ID",
					width: 10,
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Attribute",
					width: 60,
					sortable: true,
					dataIndex: 'Attribute'
				}
			]),
			autoExpandColumn: 'Attribute'
		},
		// Store config
		{
			sortInfo: { field: "Attribute", direction: "ASC" },
			baseParams: {
				ServerGroupID: serverGroupID,
				SOAPModule: 'Admin/ServerGroupAttributes',
				SOAPFunction: 'getServerGroupAttributes',
				SOAPParams: 'ServerGroupID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Attribute'}
			]
		}
	);

	adminServerGroupAttributesWindow.show();
}


// Display edit/add form
function showAdminServerGroupAttributesAddEditWindow(adminServerGroupAttributesWindow,serverGroupID,attributeID) {

	var submitAjaxConfig;
	var editMode;
	var icon;

	// We doing an update
	if (attributeID) {
		icon = 'silk-table_edit';
		title = "Update Attribute";
		submitAjaxConfig = {
			params: {
				ID: attributeID,
				SOAPFunction: 'updateServerGroupAttribute',
				SOAPParams:
					'0:ID,'+
					'0:Attribute'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminServerGroupAttributesWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};

	// We doing an Add
	} else {
		title = "Create Attribute";
		icon = 'silk-table_add';
		submitAjaxConfig = {
			params: {
				ServerGroupID: serverGroupID,
				SOAPFunction: 'createServerGroupAttribute',
				SOAPParams:
					'0:ServerGroupID,'+
					'0:Attribute'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminServerGroupAttributesWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}

	// Create window
	var adminServerGroupAttributesFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			iconCls: icon,

			width: 360,
			height: 120,

			minWidth: 360,
			minHeight: 120
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/ServerGroupAttributes'
			},
			items: [
				{
					fieldLabel: 'Attribute',
					name: 'Attribute',
					allowBlank: false,
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminServerGroupAttributesFormWindow.show();

	if (attributeID) {

		// Main form load
		adminServerGroupAttributesFormWindow.getFormPanel().load({
			params: {
				ID: attributeID,
				SOAPModule: 'Admin/ServerGroupAttributes',
				SOAPFunction: 'getServerGroupAttribute',
				SOAPParams: 'ID'
			}
		});
	}
}



// Display remove form
function showAdminServerGroupAttributesRemoveWindow(adminServerGroupAttributesWindow,attributeID) {
	// Mask adminServerGroupAttributesWindow
	adminServerGroupAttributesWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this attribute?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminServerGroupAttributesWindow,{
					params: {
						ID: attributeID,
						SOAPModule: 'Admin/ServerGroupAttributes',
						SOAPFunction: 'removeServerGroupAttribute',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminServerGroupAttributesWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminServerGroupAttributesWindow.getEl().unmask();
			}
		}
	});
}



// vim: ts=4
