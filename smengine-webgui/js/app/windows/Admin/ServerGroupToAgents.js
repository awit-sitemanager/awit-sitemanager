/*
Server group to agent
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminServerGroupToAgentWindow(serverGroupID) {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/ServerGroupToAgents/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add agent',
			iconCls:'silk-group_add',
			handler: function() {
				showAdminServerGroupToAgentAddWindow(adminServerGroupToAgentsWindow,serverGroupID);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/ServerGroupToAgents/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove server group from agent',
			iconCls:'silk-group_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminServerGroupToAgentsWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminServerGroupToAgentRemoveWindow(adminServerGroupToAgentsWindow,selectedItem.data.ID);
				} else {
					adminServerGroupToAgentsWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No agent selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminServerGroupToAgentsWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}
	var adminServerGroupToAgentsWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Server Groups To Agents",
			iconCls: 'silk-group',

			width: 480,
			height: 335,

			minWidth: 400,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "ServerGroupID",
					sortable: true,
					dataIndex: 'ServerGroupID'
				},
				{
					header: "AgentID",
					sortable: true,
					dataIndex: 'AgentID'
				},
				{
					header: "AgentName",
					sortable: true,
					dataIndex: 'AgentName'
				}
			]),
			autoExpandColumn: 'AgentName'
		},
		// Store config
		{
			sortInfo: { field: "AgentName", direction: "ASC" },
			baseParams: {
				ID: serverGroupID,
				SOAPModule: 'Admin/ServerGroupToAgents',
				SOAPFunction: 'getServerGroupToAgents',
				SOAPParams: 'ID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'numeric',  dataIndex: 'ServerGroupID'},
				{type: 'numeric',  dataIndex: 'AgentID'},
				{type: 'string',  dataIndex: 'AgentName'}
			]
		}
	);

	adminServerGroupToAgentsWindow.show();
}


// Display add form
function showAdminServerGroupToAgentAddWindow(adminServerGroupToAgentsWindow,serverGroupID) {

	var submitAjaxConfig = {
		params: {
			ServerGroupID: serverGroupID,
			SOAPFunction: 'addServerGroupToAgent',
			SOAPParams:
				'0:ServerGroupID,'+
				'0:AgentID'
		},
		onSuccess: function() {
			var store = Ext.getCmp(adminServerGroupToAgentsWindow.gridPanelID).getStore();
			store.load({
				params: {
					limit: 25
				}
			});
		}
	};

	// Create window
	var adminServerGroupToAgentFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Server Group To Agents Information",

			width: 310,
			height: 113,

			minWidth: 310,
			minHeight: 113
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/ServerGroupToAgents'
			},
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Name',
					name: 'Name',
					allowBlank: false,
					width: 160,

					store: new Ext.ux.JsonStore({
						sortInfo: { field: "Name", direction: "ASC" },
						baseParams: {
							SOAPModule: 'Agent',
							SOAPFunction: 'getAgents',
							SOAPParams: '__search'
						}
					}),
					displayField: 'Name',
					valueField: 'ID',
					hiddenName: 'AgentID',
					forceSelection: true,
					triggerAction: 'all',
					editable: false
				}
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminServerGroupToAgentFormWindow.show();
}




// Display remove form
function showAdminServerGroupToAgentRemoveWindow(adminServerGroupToAgentsWindow,serverGroupToAgentID) {
	// Mask adminServerGroupToAgentsWindow window
	adminServerGroupToAgentsWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to unlink this agent?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminServerGroupToAgentsWindow,{
					params: {
						ID: serverGroupToAgentID,
						SOAPModule: 'Admin/ServerGroupToAgents',
						SOAPFunction: 'removeServerGroupToAgent',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminServerGroupToAgentsWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminServerGroupToAgentsWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
