/*
Admin API Capabilities
Copyright (C) 2007-2011, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


function showAdminAPIGroupCapabilitiesWindow(APIGroupID) {

	// Buttons
	var buttons = [ ];

	// Check which buttons the client can see
	if (userHasCapability("Admin/APIGroupCapabilities/Add")) {
		buttons.push({
			text:'Add',
			tooltip:'Add capability',
			iconCls:'silk-table_add',
			handler: function() {
				showAdminAPIGroupCapabilitiesAddEditWindow(adminAPIGroupCapabilitiesWindow,APIGroupID);
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/APIGroupCapabilities/Update")) {
		buttons.push({
			text:'Edit',
			tooltip:'Edit capability',
			iconCls:'silk-table_edit',
			handler: function() {
				var selectedItem = Ext.getCmp(adminAPIGroupCapabilitiesWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminAPIGroupCapabilitiesAddEditWindow(adminAPIGroupCapabilitiesWindow,
							APIGroupID,selectedItem.data.ID,
							selectedItem.data.Capability
					);
				} else {
					adminAPIGroupCapabilitiesWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No capability selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminAPIGroupCapabilitiesWindow.getEl().unmask();
						}
					});
				}
			}
		});
		buttons.push('-');
	}
	if (userHasCapability("Admin/APIGroupCapabilities/Remove")) {
		buttons.push({
			text:'Remove',
			tooltip:'Remove capability',
			iconCls:'silk-table_delete',
			handler: function() {
				var selectedItem = Ext.getCmp(adminAPIGroupCapabilitiesWindow.gridPanelID).getSelectionModel().getSelected();
				// Check if we have selected item
				if (selectedItem) {
					// If so display window
					showAdminAPIGroupCapabilitiesRemoveWindow(adminAPIGroupCapabilitiesWindow,selectedItem.data.ID);
				} else {
					adminAPIGroupCapabilitiesWindow.getEl().mask();

					// Display error
					Ext.Msg.show({
						title: "Nothing selected",
						msg: "No capability selected",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.CANCEL,
						modal: false,
						fn: function() {
							adminAPIGroupCapabilitiesWindow.getEl().unmask();
						}
					});
				}
			}
		});
	}

	var adminAPIGroupCapabilitiesWindow = new Ext.ux.GenericGridWindow(
		// Window config
		{
			title: "Capabilities",
			iconCls: 'silk-table',
			
			width: 600,
			height: 335,
		
			minWidth: 600,
			minHeight: 335
		},
		// Grid config
		{
			// Inline toolbars
			tbar: buttons,

			// Column model
			colModel: new Ext.grid.ColumnModel([
				{
					id: 'ID',
					header: "ID",
					sortable: true,
					dataIndex: 'ID'
				},
				{
					header: "Capability",
					sortable: true,
					dataIndex: 'Capability'
				}
			]),
			autoExpandColumn: 'Name'
		},
		// Store config
		{
			sortInfo: { field: "Capability", direction: "ASC" },
			baseParams: {
				ID: APIGroupID,
				SOAPModule: 'Admin/APIGroupCapabilities',
				SOAPFunction: 'getAPIGroupCapabilities',
				SOAPParams: 'ID,__search'
			}
		},
		// Filter config
		{
			filters: [
				{type: 'numeric',  dataIndex: 'ID'},
				{type: 'string',  dataIndex: 'Capability'}
			]
		}
	);

	adminAPIGroupCapabilitiesWindow.show();
}


// Display add form
function showAdminAPIGroupCapabilitiesAddEditWindow(adminAPIGroupCapabilitiesWindow,APIGroupID,CapabilityID,Capability) {

	var submitAjaxConfig;
	var icon;
	var editMode;

	// Combobox ID
	var comboboxID = Ext.id();

	// Capabilities store
	var capabilitiesStore = new Ext.ux.JsonStore({
			sortInfo: { field: "Capability", direction: "ASC" },
			baseParams: {
				SOAPModule: 'Admin/APIGroupCapabilities',
				SOAPFunction: 'getAllCapabilities',
				SOAPParams: '__search'
			}
	});

	if (CapabilityID) {
		icon = 'silk-table_edit';
		submitAjaxConfig = {
			params: {
				ID: CapabilityID,
				SOAPFunction: 'updateAPIGroupCapability',
				SOAPParams:
					'0:ID,0:Capability'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminAPIGroupCapabilitiesWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	} else {
		icon = 'silk-table_add';
		submitAjaxConfig = {
			params: {
				APIGroupID: APIGroupID,
				SOAPFunction: 'addAPIGroupCapability',
				SOAPParams:
					'0:APIGroupID,0:Capability'
			},
			onSuccess: function() {
				var store = Ext.getCmp(adminAPIGroupCapabilitiesWindow.gridPanelID).getStore();
				store.load({
					params: {
						limit: 25
					}
				});
			}
		};
	}
	
	// Create window
	var adminAPIGroupCapabilityFormWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "Capability Information",
			iconCls: icon,

			width: 400,
			height: 125,

			minWidth: 400,
			minHeight: 125,

			autoScroll: true
		},
		// Form panel config
		{
			labelWidth: 85,
			baseParams: {
				SOAPModule: 'Admin/APIGroupCapabilities'
			},
			items: [
				{
					id: comboboxID,
					fieldLabel: 'Capability',
					name: 'Capability',
					xtype: 'combo',
					allowBlank: false,
					width: 225,
					store: capabilitiesStore,
					displayField: 'Capability',
					valueField: 'Capability',
					hiddenName: 'Capability',
					forceSelection: false,
					triggerAction: 'all',
					editable: true
				},
			]
		},
		// Submit button config
		submitAjaxConfig
	);

	adminAPIGroupCapabilityFormWindow.show();

	// Populate value with the current selected capability
	if (CapabilityID) {
		var combobox = Ext.getCmp(comboboxID);
		combobox.setValue(Capability);
	}
}




// Display remove form
function showAdminAPIGroupCapabilitiesRemoveWindow(adminAPIGroupCapabilitiesWindow,id) {
	// Mask adminAPIGroupCapabilitiesWindow window
	adminAPIGroupCapabilitiesWindow.getEl().mask();

	// Display remove confirm window
	Ext.Msg.show({
		title: "Confirm removal",
		msg: "Are you very sure you wish to remove this capability?",
		icon: Ext.MessageBox.ERROR,
		buttons: Ext.Msg.YESNO,
		modal: false,
		fn: function(buttonId,text) {
			// Check if user clicked on 'yes' button
			if (buttonId == 'yes') {

				// Do ajax request
				uxAjaxRequest(adminAPIGroupCapabilitiesWindow,{
					params: {
						ID: id,
						SOAPModule: 'Admin/APIGroupCapabilities',
						SOAPFunction: 'removeAPIGroupCapability',
						SOAPParams: 'ID'
					},
					customSuccess: function() {
						var store = Ext.getCmp(adminAPIGroupCapabilitiesWindow.gridPanelID).getStore();
						store.load({
							params: {
								limit: 25
							}
						});
					}
				});


			// Unmask if user answered no
			} else {
				adminAPIGroupCapabilitiesWindow.getEl().unmask();
			}
		}
	});
}


// vim: ts=4
