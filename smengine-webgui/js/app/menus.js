/*
Menus
Copyright (C) 2007-2009, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


var agentsMenu = new Ext.menu.Menu({
	items: [
		{
			text: 'Agents',
			iconCls: "silk-group",
			handler: function() {
				showAgentsWindow();
			}
		}
	]
});


var dnsHostingMenu = new Ext.menu.Menu({
	items: [
	/*	{
			text: 'DNS Servers',
			handler: function() {
			}
		},*/
		{
			text: 'Zones',
			iconCls: 'silk-table',
			handler: function() {
				showServerGroupSelectWindow('showDNSZonesWindow');
			}
		},
		{
			text: 'DDNS Users',
			iconCls: 'silk-group',
			handler: function() {
				showServerGroupSelectWindow('showDDNSUsersWindow');
			}
		}
	]
});

var webHostingMenu = new Ext.menu.Menu({
	items: [
	/*	{
			text: 'Web Servers',
			handler: function() {
			}
		},*/
		{
			text: 'Sites',
			iconCls: 'silk-layout',
			handler: function() {
				showWebHostingSitesWindow();
			}
		}
	]
});


var mailHostingMenu = new Ext.menu.Menu({
	items: [
	/*
		{
			text: 'Mail Servers',
			handler: function() {
			}
		},
	*/
		{
			text: 'Transports',
			iconCls: 'silk-book',
			handler: function() {
				showServerGroupSelectWindow('showMailTransportsWindow');
			}
		}
	]
});

var radiusMenu = new Ext.menu.Menu({
	items: [
	/*
		{
			text: 'Radius Servers',
			handler: function() {
			}
		},
		{
			text: 'Realms',
			handler: function() {
			}
		},
	*/
		{
			text: 'Users',
			iconCls: 'silk-group',
			handler: function() {
				showServerGroupSelectWindow('showRadiusUserWindow');
			}
		}
	]
});


var adminMenuUsersItem = {
	text: 'Users',
	iconCls: 'silk-user',
	handler: function() {
		showAdminUserWindow();
	}
};
var adminMenuGroupsItem = {
	text: 'Groups',
	iconCls: 'silk-group',
	handler: function() {
		showAdminGroupWindow();
	}
};
var adminMenuAPIGroupsItem = {
	text: 'API Groups',
	iconCls: 'silk-application',
	handler: function() {
		showAdminAPIGroupWindow();
	}
};
var adminMenuServersItem = {
	text: 'Servers',
	iconCls: 'silk-server',
	handler: function() {
		showAdminServerWindow();
	}
};
var adminMenuServerGroupsItem = {
	text: 'Server Groups',
	iconCls: 'silk-building',
	handler: function() {
		showAdminServerGroupWindow();
	}
};

var auditTrailsMenu = new Ext.menu.Menu({
	items: [
		{
			text: 'Audit Trails',
			iconCls: 'silk-report',
			handler: function() {
				showAuditTrailsWindow();
			}
		}
	]
});


// vim: ts=4
