/*
Standard service panels
Copyright (C) 2007-2010, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/





// Create a plugin to handle the enabling an disabling of components based 
// on the value/status of a checkbox
Ext.ux.checkboxAutoFoo = function(config){		
	Ext.apply(this, config);
};

Ext.extend(Ext.ux.checkboxAutoFoo, Ext.util.Observable, {

	// Initialize our plugin
	init: function(checkbox) {

		this.checkbox  = checkbox;

		// Install onchange handler
		this.checkbox.on('check', function(checkbox,checked) {
				var component = Ext.getCmp(this.childID);

				if (checked) {
					component.enable();
				} else {
					component.disable();
				}
			},
			this
		);

	}
});


// Function to get a nice tab panel back with standard service tabs and options
function getStandardServicePanel(formPanelID,extraTabs) {

	// Component ID's
	var AgentAutoDisableOnID = formPanelID + 'aado';
	var AgentAutoDisableID = formPanelID + 'aad';
	var AgentAutoEnableOnID = formPanelID + 'aaeo';
	var AgentAutoEnableID = formPanelID + 'aae';
	var AgentAutoRemoveOnID = formPanelID + 'aaro';
	var AgentAutoRemoveID = formPanelID + 'aar';

	// Here is our standard set of tabs
	var standardTabs = [
		{
			title: 'Service',

			items: [
				{
					xtype: 'fieldset',
					title: 'Status',
					autoHeight: true,

					items: [
						{
							layout: 'absolute',
							defaultType: 'label',
							height: 20,
							border: false,

							items: [
								{
									text: 'Admin Disabled:',
									x: 40,
									y: 0
								},
								{
									xtype: 'checkbox',
									name: 'AdminDisabled',
									disabled: true,
									x: 120,
									y: 3
								},
								{
									text: 'Agent Disabled: ',
									x: 250,
									y: 0
								},
								{
									xtype: 'checkbox',
									name: 'AgentDisabled',
									x: 330,
									y: 3
								}
							]
						}
					]
				}
			]
		},
		{
			title: 'Schedule',

			items: [
				{
					xtype: 'fieldset',
					title: 'Automatic Actions',
					autoHeight: true,

					items: [
						{
							layout: 'absolute',
							defaultType: 'label',
							height: 90,
							border: false,

							items: [
								// AutoDisable
								{
									text: 'Auto Disable',
									x: 10,
									y: 0
								},
								{
									id: AgentAutoDisableID,
									xtype: 'checkbox',
									name: 'AgentAutoDisable',
									plugins: new Ext.ux.checkboxAutoFoo({ childID: AgentAutoDisableOnID }),
									disabled: true,
									x: 90,
									y: 3
								},
								{
									text: 'on',
									x: 120,
									y: 0
								},
								{
									id: AgentAutoDisableOnID,
									xtype: 'datefield',
									name: 'AgentAutoDisableOn',
									disabled: true,
									x: 140,
									y: 0
								},
								// AutoEnable
								{
									text: 'Auto Enable',
									x: 10,
									y: 30
								},
								{
									id: AgentAutoEnableID,
									xtype: 'checkbox',
									name: 'AgentAutoEnable',
									plugins: new Ext.ux.checkboxAutoFoo({ childID: AgentAutoEnableOnID }),
									disabled: true,
									x: 90,
									y: 33
								},
								{
									text: 'on',
									x: 120,
									y: 30
								},
								{
									id: AgentAutoEnableOnID,
									xtype: 'datefield',
									name: 'AgentAutoEnableOn',
									disabled: true,
									x: 140,
									y: 30
								},
								// AutoRemove
								{
									text: 'Auto Remove',
									x: 10,
									y: 60
								},
								{
									id: AgentAutoRemoveID,
									xtype: 'checkbox',
									name: 'AgentAutoRemove',
									plugins: new Ext.ux.checkboxAutoFoo({ childID: AgentAutoRemoveOnID }),
									disabled: true,
									x: 90,
									y: 63
								},
								{
									text: 'on',
									x: 120,
									y: 60
								},
								{
									id: AgentAutoRemoveOnID,
									xtype: 'datefield',
									name: 'AgentAutoRemoveOn',
									disabled: true,
									x: 140,
									y: 60
								}
							]
						}
					]
				}
			]
		}
	];

	// Empty array if not defined
	if (!extraTabs) {
		extraTabs = [ ];
	}

	// Base panel config
	var panelConfig = {
		xtype: 'tabpanel',
		plain: 'true',
		deferredRender: false, // Load all panels!
		activeTab: 0,
		height: 175,
		defaults: {
			layout: 'form',
			bodyStyle: 'padding: 10px;'
		},
		// Pull in all our tabs
		items: standardTabs.concat(extraTabs)
	}

	return panelConfig;
}


function initStandardServicePanel(formPanelID) {
	// Component ID's
	var AgentAutoDisableOnID = formPanelID + 'aado';
	var AgentAutoDisableID = formPanelID + 'aad';
	var AgentAutoEnableOnID = formPanelID + 'aaeo';
	var AgentAutoEnableID = formPanelID + 'aae';
	var AgentAutoRemoveOnID = formPanelID + 'aaro';
	var AgentAutoRemoveID = formPanelID + 'aar';

	// Set initial values
	if (Ext.getCmp(AgentAutoDisableID).getValue()) {
		Ext.getCmp(AgentAutoDisableOnID).enable();
	}
	if (Ext.getCmp(AgentAutoEnableID).getValue()) {
		Ext.getCmp(AgentAutoEnableOnID).enable();
	}
	if (Ext.getCmp(AgentAutoEnableID).getValue()) {
		Ext.getCmp(AgentAutoEnableOnID).enable();
	}
}


// vim: ts=4
