/*
Main Layout
Copyright (C) 2007-2009, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


// Main viewport
function initViewport() {

	// Menus
	var menus = [ ];

	// Check which menus the client can see
	var adminMenuItems = [ ];
	if (userHasCapability("Admin/Users/List")) {
		adminMenuItems.push(adminMenuUsersItem);
	}
	if (userHasCapability("Admin/Groups/List")) {
		adminMenuItems.push(adminMenuGroupsItem);
	}
	if (userHasCapability("Admin/APIGroups/List")) {
		adminMenuItems.push(adminMenuAPIGroupsItem);
	}
	if (userHasCapability("Admin/Servers/List")) {
		adminMenuItems.push(adminMenuServersItem);
	}
	if (userHasCapability("Admin/ServerGroups/List")) {
		adminMenuItems.push(adminMenuServerGroupsItem);
	}
	// If we do have items
	if (adminMenuItems.length > 0) {
		// Build the menu
		var adminMenu = new Ext.menu.Menu({
			items: adminMenuItems
		});
		menus.push({ text: "Admin Panel", menu: adminMenu });
	}
	
	if (userHasCapability("Agent/List")) {
		menus.push({ text: "Agents", menu: agentsMenu });
	}
	if (userHasCapability("DNSHosting/Zones/List")) {
		menus.push({ text: "DNS Hosting", menu: dnsHostingMenu });
	}
	if (userHasCapability("WebHosting/Websites/List")) {
		menus.push({ text: "Web Hosting", menu: webHostingMenu });
	}
	if (userHasCapability("MailHosting/MailTransports/List")) {
		menus.push({ text: "Mail Hosting", menu: mailHostingMenu });
	}
	if (userHasCapability("Radius/Users/List")) {
		menus.push({ text: "Radius", menu: radiusMenu });
	}
	if (userHasCapability("AuditTrails/List")) {
		menus.push({ text: "Audit Trails", menu: auditTrailsMenu });
	}


	// Create viewport
	var viewport = new Ext.Viewport({
		layout: 'border',
		items: [
			{
				// Top bar
				region:'north',
	
				height: 30,
				border: false,
	
				items: [
					{
						// Add toolbar
						xtype: 'toolbar',
						items: menus
					}
				]
			},
			// Main content
			{
				xtype: 'panel',
				region: 'center',
				autoScroll: true,
				border: true,
				margins:'5 5 5 5',
				baseCls: 'nmsmain'
//				items: [
//					mainWindow
//				]
			},

			{
				id: 'main-statusbar',
				xtype: 'panel',
				region: 'south',
				border: true,
				height: 30,
    				bbar: new Ext.ux.StatusBar({
					id: 'my-status',

					// defaults to use when the status is cleared:
					defaultText: 'Default status text',
					defaultIconCls: 'default-icon',

					// values to set initially:
					text: 'Ready',
					iconCls: 'ready-icon'

					// any standard Toolbar items:
//					items: [{
//						text: 'A Button'
//					}, '-', 'Plain Text']
				})
			}

/*
			{
				region: 'east',
				html: '<img src="resources/custom/images/smarty_icon.gif"></img>',
				margins:'5 5 5 5',
				border: true
			}
*/
		]	
	});
}



// vim: ts=4
