/*
Main Application Bootup
Copyright (C) 2007-2009, AllWorldIT

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


// Display edit/add form
function showLoginWindow() {

	// Create window
	var loginWindow = new Ext.ux.GenericFormWindow(
		// Window config
		{
			title: "NMS System Login - v"+VERSION,
			iconCls: "silk-key",

			width: 320,
			height: 230,

			minWidth: 320,
			minHeight: 230
		},
		// Form panel config
		{
			labelWidth: 95,
			items: [
				{
					fieldLabel: 'Username',
					name: 'SOAPUsername',
					allowBlank: false
				},
				{
					fieldLabel: 'Password',
					name: 'SOAPPassword',
					allowBlank: false,
					inputType: 'password'
				},
				{
					fieldLabel: 'Authority',
					name: 'SOAPAuthType',
					xtype: 'combo',
					allowBlank: false,
					width: 100,
					mode: 'local',
					store: [ 
							'Client',
							'Agent',
							'Admin'
					],
					forceSelection: false,
					triggerAction: 'all',
					editable: false
				},
				{
					fieldLabel: 'Login As',
					name: 'SOAPModule',
					xtype: 'combo',
					allowBlank: false,
					width: 100,
					mode: 'local',
    					store: new Ext.data.ArrayStore({
						idIndex: 0,
						fields: [
							'id', 'value', 'display'
						],
						data: [
							[1, 'Client', 'Client User'],
							[2, 'Agent', 'Agent User'],
							[3, 'Admin', 'Admin User']
						]
					}),
					displayField: 'display',
					valueField: 'value',
					hiddenName: 'SOAPModule',
					forceSelection: false,
					triggerAction: 'all',
					editable: false
				},
				{
					fieldLabel: 'Account ID',
					name: 'AccountID',
					allowBlank: false
				}
			]
		},
		// Submit button config
		{
			params: {
				SOAPFunction: 'authenticate',
				SOAPParams: '0:AccountID'
			},
			submitButtonText: 'Login',
			submitMsg: 'Logging in...',
			onSuccess: function(form,action) {
				// Set our capabilities
				globalConfig.Capabilities = action.result.data.Capabilities;
				// Fire up viewport
				initViewport();
			}
		}
	);

	loginWindow.show();
}


Ext.onReady(function(){

	// Enable tips
	Ext.QuickTips.init();

	// Turn on validation errors beside the field globally
	Ext.form.Field.prototype.msgTarget = 'side';
	
	// Turn off the loading icon
	var hideMask = function () {
		Ext.get('loading').remove();
		Ext.fly('loading-mask').fadeOut({
			remove: true,
			callback: function() {
				showLoginWindow();
			}
		});
	}

	hideMask.defer(250);

});
