<?php
# JSON<=>SOAP Translator
# Copyright (C) 2008-2010, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

$VERSION = "0.2.0";

# Includes
include_once("include/config.php");
include_once("include/constants.php");
include_once("include/json.php");

# Start session, we need to do this for various internal tracking reasons
session_start();

# Set our name all nicely
header("X-JSON-Translator: SMEngine JSON<=>SOAP Translator v$VERSION");


 # Fire an exception off to Ajax
function ajaxException($msg) {
	echo jsonError(-1,$msg);
	exit;
}

# PHP exception handler
function exception_handler($exception) {
	ajaxException("Exception: ". $exception->getMessage());
}

# Set PHP exception handler
set_exception_handler('exception_handler');

# Just make sure, if this is a authenticaiton request ... wipe session
if (!empty($_REQUEST['SOAPFunction']) && $_REQUEST['SOAPFunction'] == "authenticate") {
	session_unset();
}


# Check if we have a session username
if (!empty($_SESSION['SOAPUsername'])) {
	$username = $_SESSION['SOAPUsername'];
# If not, check if one was specified
} else {
	if (empty($_REQUEST['SOAPUsername'])) {
		ajaxException("SOAPUsername undefined");
	}
	if (!is_string($_REQUEST['SOAPUsername'])) {
		ajaxException("SOAPUsername is not a string");
	}
	$username = $_REQUEST['SOAPUsername'];
}

# Check if we have a session password
if (!empty($_SESSION['SOAPPassword'])) {
	$password = $_SESSION['SOAPPassword'];
# If not, check if one was specified
} else {
	if (empty($_REQUEST['SOAPPassword'])) {
		ajaxEception("SOAPPassword undefined");
	}
	if (!is_string($_REQUEST['SOAPPassword'])) {
		ajaxException("SOAPPassword is not a string");
	}
	$password = $_REQUEST['SOAPPassword'];
}

# Check if we have a session authentication module
if (!empty($_SESSION['SOAPAuthType'])) {
	$authType = $_SESSION['SOAPAuthType'];
# if not, check if one was specified
} else {
	if (empty($_REQUEST['SOAPAuthType'])) {
		ajaxException("SOAPAuthType undefined");
	}
	if (!is_string($_REQUEST['SOAPAuthType'])) {
		ajaxException("SOAPAuthType is not a string");
	}
	$authType = $_REQUEST['SOAPAuthType'];
}


# Look if we have a module defined
if (empty($_REQUEST['SOAPModule'])) {
	ajaxException("SOAPModule undefined");
}
if (!is_string($_REQUEST['SOAPModule'])) {
	ajaxException("SOAPModule is not a string");
}
$module = $_REQUEST['SOAPModule'];

# We also need a function
if (empty($_REQUEST['SOAPFunction'])) {
	ajaxException("SOAPFunction undefined");
}
if (!is_string($_REQUEST['SOAPFunction'])) {
	ajaxException("SOAPFunction is not a string");
}
$function = $_REQUEST['SOAPFunction'];



/*
	__search
	ID,__search
	1:Name,1:Contact,1:Telephone,1:Email
*/

# Check if we have a parameter template
$soapParamTemplate = array();
if (isset($_REQUEST['SOAPParams'])) {
	$soapParamTemplate = explode(',',$_REQUEST['SOAPParams']);
}

$soapParams = array();

foreach ($soapParamTemplate as $param) {
	# Special case '__search'
	if ($param == "__search") {
		# Build hash and push into param list
		$search = array(
			'Filter' => isset($_REQUEST['filter']) ? $_REQUEST['filter'] : NULL,
			'Start' => isset($_REQUEST['start']) ? $_REQUEST['start'] : NULL,
			'Limit' => isset($_REQUEST['limit']) ? $_REQUEST['limit'] : NULL,
			'Sort' => isset($_REQUEST['sort']) ? $_REQUEST['sort'] : NULL,
			'SortDirection' => isset($_REQUEST['dir']) ? $_REQUEST['dir'] : NULL,
		);
		array_push($soapParams,$search);

	# Special case '__null'
	} elseif ($param == "__null") {
		array_push($soapParams,NULL);

	# Everything else
	} else {
		# Split off param number if we're using it
		$items = explode(':',$param);

		# We have a parameter number
		if (count($items) > 1) {
			$array_pos = $items[0];
			$array_item = $items[1];

			# Initialize array
			if (!isset($soapParams[$array_pos])) {
				$soapParams[$array_pos] = array();
			}
			# Check if we have an explicit type
			if (isset($items[2])) {
				$array_type = $items[2];
				# Check type
				if ($array_type == "boolean") {
					# Checkboxes/booleans are undefined if false
					if (isset($_REQUEST[$array_item])) {
						$item_value = 'true';
					} else {
						$item_value = 'false';
					}
				# And bitch if we invalid
				} else {
					ajaxException("Unknown AJAX=>SOAP type: '$array_type'");
				}
			} else {
				$item_value = isset($_REQUEST[$array_item]) ? $_REQUEST[$array_item] : NULL;
			}
			# Set item
			$soapParams[$array_pos][$array_item] = $item_value;

		} else {
			array_push($soapParams,$_REQUEST[$items[0]]);
		}
	}
}

# Connect via soap
$soap = new SoapClient(null,
	array(
		'location' => $SOAP_SERVER . "/?Auth=$authType",
		'uri' => $module,
		'login' => $username,
		'password' => $password
	)
);

# Try soap call
try {
	$soapRes = $soap->__call($function,$soapParams);

} catch (Exception $e) {
	# If this is a SOAP fault, its not THAT bad ...
	if (is_soap_fault($e)) {
		$msg = $e->faultstring;
		if (!empty($e->detail)) {
			$msg .= " (".$e->detail.")";
		}
	# If its not a SOAP fault, its bad
	} else {
		header($_SERVER['SERVER_PROTOCOL']." 500 Internal Server Error");
		$msg = $e->getMessage();
	}

	ajaxException($msg);
}

# Special handling if we just authenticated
if ($function == "authenticate" && ($soapRes->success !== FALSE || $soapRes->success !== 'false')) {
	$_SESSION['SOAPAuthType'] = $authType;
	$_SESSION['SOAPUsername'] = $username;
	$_SESSION['SOAPPassword'] = $password;
}

# Updated last accessed
$_SESSION['LastAccessed'] = time();
# This is how many times we've been accessed in this specific session
if (isset($_SESSION['access_count'])) {
	$_SESSION['access_count']++;
} else {
	$_SESSION['access_count'] = 1;
}

echo json_encode($soapRes);
